<?php

use Illuminate\Database\Seeder;

class AcceptAllExperienceVersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experienceVersions = \App\Models\ExperienceVersion::all();

        foreach ($experienceVersions as $experienceVersion) {
            if ($experienceVersion->experienceVersionPhotos()->count()>0) {
                $experienceVersion->status = \App\Constant::STATUS_ACCEPTED;
                $experienceVersion->save();
            }
        }
    }
}
