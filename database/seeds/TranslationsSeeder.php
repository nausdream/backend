<?php

use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use Illuminate\Database\Seeder;

class TranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $boat_types = [
            'catamaran' => 'Catamaran',
            'motorboat' => 'Motorboat',
            'sailboat' => 'Sailboat'
        ];
        $this->insertValuesOnTranslationDb($boat_types, 'boat_types');

        $boat_materials = [
            'fiberglass' => 'Fiber glass',
            'wood' => 'Wood',
            'steel' => 'Steel',
            'aluminium' => 'Aluminium'
        ];
        $this->insertValuesOnTranslationDb($boat_materials, 'boat_materials');

        $motor_types = [
            'inboard' => 'Inboard',
            'outboard' => 'Outboard',
            'sterndrive' => 'Sterndrive'
        ];

        $this->insertValuesOnTranslationDb($motor_types, 'motor_types');

        $countries = array
        (
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );

        $this->insertValuesOnTranslationDb($countries, 'countries');

        $experience_types = [
            'trips-and-tours' => 'Trips and tours',
            'luxury' => 'Luxury',
            'romantic' => 'Romantic',
            'dinners' => 'Dinners',
            'aperitif' => 'Aperitif',
            'diving' => 'Diving',
            'fishing' => 'Fishing',
            'other' => 'Other'
        ];

        $this->insertValuesOnTranslationDb($experience_types, 'experience_types');

        $fishing_partitions = [
            'skipper' => 'Skipper',
            'guests' => 'Guests',
            'guests-and-skipper' => 'Guests and skipper',
        ];

        $this->insertValuesOnTranslationDb($fishing_partitions, 'fishing_partitions');

        $fishing_types = [
            'fishing-line' => 'Fishing line',
            'pull-coast' => 'Pull coast',
            'pull-height' => 'Pull height',
            'drifting' => 'Drifting',
            'artificial-lure' => 'Artificial lure'
        ];

        $this->insertValuesOnTranslationDb($fishing_types, 'fishing_types');

        $additionalServices = [
            'cabin-for-special-events' => 'Cabin for special events',
            'welcome-aperitif' => 'Welcome aperitif',
            'aperitif-with-oysters-and-champagne' => 'Aperitif with oysters and champagne',
            'fishing-equipment' => 'Fishing equipment',
            'sport-equipment' => 'Sport equipment',
            'alcoholic-drinks' => 'Alcoholic drinks',
            'soft-drinks' => 'Soft drinks',
            'bath-linen' => 'Bath linen',
            'snorkel' => 'Snorkel',
            'scuba-tank' => 'Scuba tank',
            'diving-boots' => 'Diving boots',
            'galley' => 'Galley',
            'diving-hood' => 'Diving hood',
            'diving-computer' => 'Diving computer',
            'fuel' => 'Fuel',
            'sun-cream' => 'Sun cream',
            'breakfast' => 'Breakfast',
            'sea-cook' => 'Sea cook',
            'possible-moorings' => 'Possible moorings',
            'fresh-fruit' => 'Fresh fruit',
            'buoyancy-compensator' => 'Buoyancy compensator',
            'inflatable' => 'Inflatable',
            'diving-gloves' => 'Diving gloves',
            'tour-guide-aboard' => 'Tour guide aboard',
            'hostess' => 'Hostess',
            'interpreter' => 'Interpreter',
            'sailor' => 'Sailor',
            'diving-mask' => 'Diving mask',
            'customisable' => 'Customisable',
            'music' => 'Music',
            'diving-suite' => 'Diving suite',
            'parking-lot' => 'Parking lot',
            'vegan-meal' => 'Vegan meal',
            'vegetarian-meal' => 'Vegetarian meal',
            'surface-marker-buoy' => 'Surface marker buoy',
            'overnight-stay' => 'Overnight stay',
            'fins' => 'Fins',
            'possibility-to-bring-your-own-music' => 'Possibility to bring your own music',
            'lunch' => 'Lunch',
            'bath-products' => 'Bath products',
            'final-cleaning-aboard' => 'Final cleaning aboard',
            'daily-cleaning' => 'Daily cleaning',
            'bed-linen' => 'Bed linen',
            'dock-shower' => 'Dock shower',
            'tender' => 'Tender',
            'snack' => 'Snack',
            'candlelight-table' => 'Candlelight table',
            'beach-towel' => 'Beach towel',
            'torch' => 'Torch',
            'transfer-from-to' => 'Transfer from hotel to port',
            'use-of-the-heating' => 'Use of the heating',
            'various-moorings' => 'Various moorings',
            'ballast' => 'Ballast',
        ];

        $this->insertValuesOnTranslationDb($additionalServices, 'additional_services');
    }

    /**
     * @param $array
     * @param $pageName
     */
    protected function insertValuesOnTranslationDb($array, $pageName)
    {
        $page = Page::all()->where('name', $pageName)->first();

        if (!isset($page)) {
            $page = new Page(['name' => $pageName]);
            $page->save();
        }

        $fallback = \App\TranslationModels\Language::all()->where('language', Lang::getFallback())->first();

        // Check for no sentence duplication

        foreach ($array as $key => $element) {

            $sentence = Sentence::all()->where('name', $key)->where('page_id', $page->id)->last();

            // Create new sentence

            if (!isset($sentence)) {
                $sentence = new Sentence(['name' => $key]);
                $page->sentences()->save($sentence);
            }

            // Create sentence translation

            $sentenceTranslation = SentenceTranslation::all()
                ->where('language_id', $fallback->id)
                ->where('sentence_id', $sentence->id)
                ->last();

            if (!isset($sentenceTranslation)) {
                $sentenceTranslation = new SentenceTranslation(['text' => $element]);
                $sentenceTranslation->language()->associate($fallback);
                $sentence->sentenceTranslations()->save($sentenceTranslation);
            } else {
                $sentenceTranslation->text = $element;
                $sentenceTranslation->save();
            }

        }
    }
}
