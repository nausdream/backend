<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FakeTranslationsSeeder2 extends Seeder
{
    protected $fake;

    /**
     * FakeDataSeeder constructor.
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $giuseppeBasciu = \App\TranslationModels\User::all()->where('email', 'giuseppe@nausdream.com')->first();
        $language = \App\TranslationModels\Language::all()->where('language', 'it')->first();
        $giuseppeBasciu->is_admin = false;
        $giuseppeBasciu->languages()->attach($language->id);
        $giuseppeBasciu->save();
    }

}