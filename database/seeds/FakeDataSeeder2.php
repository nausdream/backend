<?php

use Illuminate\Database\Seeder;

class FakeDataSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ousmaneDieng = \App\Models\Administrator::all()->where('email', 'ousmane@nausdream.com')->first();
        $ousmaneDieng->phone = '+17876771455';
        $ousmaneDieng->save();
    }
}
