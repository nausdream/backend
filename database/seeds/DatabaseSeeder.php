<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FakeDataSeeder::class);
        $this->call(FakeTranslationsSeeder::class);
        $this->call(SpokenLanguageAndErrorsSeeder::class);
        $this->call(TranslationsSeeder::class);
    }
}
