<?php

use App\Models\Price;
use Illuminate\Database\Seeder;

class AddPriceWith1Person extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $periods = \App\Models\Period::all();

        foreach ($periods as $period) {
            $priceForOne = $period->prices()->where('person', 1)->first();
            if (!isset($priceForOne)) {
                // Add price to period
                $otherPrice = $period->prices()->first();
                $price = new Price(['person' => 1, 'price' => ceil($otherPrice->price/$otherPrice->person), 'currency' => $otherPrice->currency]);
                $period->prices()->save($price);
            }
        }
    }
}
