<?php
/**
 * User: Luca Puddu
 * Date: 04/11/2016
 * Time: 17:14
 */

use App\Constant;
use App\Http\Controllers\v1\SeosController;
use App\Models\Boat;
use App\Models\BoatTranslationDescription;
use App\Models\BoatTranslationIndication;
use App\Models\BoatTranslationRule;
use App\Models\BoatVersion;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Experience;
use App\Models\ExperiencePhoto;
use App\Models\ExperienceTranslationDescription;
use App\Models\ExperienceTranslationRule;
use App\Models\ExperienceTranslationTitle;
use App\Models\ExperienceVersion;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Models\FixedAdditionalService;
use App\Models\Rule;
use App\Models\User;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FakeDataSeeder extends Seeder
{
    protected $fake;

    /**
     * FakeDataSeeder constructor.
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datasetLength = 50;
        $users = $datasetLength;
        $captains = $datasetLength;
        $boats = $captains;
        $experiences = (int)($boats * 2);

        // Create administrators
        $administrators = [];
        $marcoDeiosso = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Marco',
                'last_name' => 'Deiosso',
                'email' => 'marco@nausdream.com',
                'phone' => '+393485447113',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        $ousmaneDieng = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Ousmane',
                'last_name' => 'Dieng',
                'email' => 'ousmane@nausdream.com',
                'phone' => '+393403224160',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        $lucaPuddu = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Luca',
                'last_name' => 'Puddu',
                'email' => 'luca@nausdream.com',
                'phone' => '+393495867908',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        $giuseppeBasciu = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Giuseppe',
                'last_name' => 'Basciu',
                'email' => 'giuseppe@nausdream.com',
                'phone' => '+393895999330',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        $francescaChippari = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Francesca',
                'last_name' => 'Chippari',
                'email' => 'francesca@nausdream.com',
                'phone' => '+393467066695',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        array_push($administrators, $marcoDeiosso, $ousmaneDieng, $lucaPuddu, $giuseppeBasciu, $francescaChippari);

        // Fetch authorizations
        $authorizations = \App\Models\Authorization::all();
        $authIds = [];
        foreach ($authorizations as $authorization) {
            $authIds[] = $authorization->id;
        }
        foreach ($administrators as $administrator) {
            $administrator->authorizations()->attach($authIds);
        }

        // Create normal users
        factory(User::class, $users)->states('dummy')->create(['is_captain' => false]);
        // Create captains
        factory(User::class, $captains)->states('dummy', 'captain')->create();

        // Create boats and boatVersions
        factory(Boat::class, (int)($boats / 3))->create()->each(function ($b) {
            $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'staging', 'finished_and_accepted')->make());
            $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
        });
        factory(Boat::class, (int)($boats / 3))->create()->each(function ($b) {
            $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 2)->states('dummy', 'staging', 'finished_and_accepted')->make());
            $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'staging')->make());
        });
        factory(Boat::class, (int)($boats / 3))->create()->each(function ($b) {
            $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 2)->states('dummy', 'staging', 'finished_and_accepted')->make());
            $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_REJECTED]));
        });
        factory(Boat::class, (int)($boats / 3))->create()->each(function ($b) {
            $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 2)->states('dummy', 'staging', 'finished_and_accepted')->make());
            $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]));
        });
        factory(Boat::class, (int)($boats / 3))->create()->each(function ($b) {
            $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_PENDING]));
        });

        // Boatversion photos
        $boatVersions = BoatVersion::all();
        foreach ($boatVersions as $boatVersion) {
            $boatVersion->version_internal_photo = $this->fake->numberBetween(1000000, 9999999);
            $boatVersion->version_external_photo = $this->fake->numberBetween(1000000, 9999999);
            $boatVersion->version_sea_photo = $this->fake->numberBetween(1000000, 9999999);
            $boatVersion->save();
        };

        // Create experiences and experienceVersions
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'staging', 'finished', 'accepted')->make());
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy', 'staging')->make());
        });
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy', 'staging', 'finished', 'accepted')->make());
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy', 'staging')->make());
        });
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 2)->states('dummy', 'staging', 'finished', 'accepted')->make());
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_REJECTED]));
        });
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 2)->states('dummy', 'staging', 'finished', 'accepted')->make());
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_FREEZED]));
        });
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 2)->states('dummy', 'staging', 'finished', 'accepted')->make());
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]));
        });
        factory(Experience::class, (int)($experiences / 3))->create()->each(function ($e) {
            $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'staging', 'finished')->make(['status' => Constant::STATUS_PENDING]));
        });

        // Set number of fixed rules and fixed additional services
        $fixedRules = [];
        foreach (Rule::all() as $fixedRule) {
            $fixedRules[] = $fixedRule->id;
        }
        $fixedAdditionalServices = [];
        foreach (FixedAdditionalService::all() as $fixedAdditionalService) {
            $fixedAdditionalServices[] = $fixedAdditionalService->id;
        }

        // Add fixed rules and additional services to experienceVersions
        ExperienceVersion::all()->each(function ($e) use ($fixedRules, $fixedAdditionalServices) {
            $e->rules()->attach(array_rand(array_flip($fixedRules), random_int(1, count($fixedRules))));
            // Set fixedAdditionalServices and prices
            $services = array_rand(array_flip($fixedAdditionalServices), random_int(1, count($fixedAdditionalServices)));
            $servicePrices = [];
            if (is_array($services) && count($services) > 0) {
                foreach ($services as $service) {
                    $servicePrices[$service] = [
                        'price' => random_int(0, 50),
                        'per_person' => $this->fake->boolean,
                        'currency' => Currency::all()->random()->name
                    ];
                }
            } else {
                $servicePrices[$services] = [
                    'price' => random_int(0, 50),
                    'per_person' => $this->fake->boolean,
                    'currency' => Currency::all()->random()->name
                ];
            }
            $e->fixedAdditionalServices()->attach($servicePrices);

            $e->experienceVersionPhotos()->saveMany(factory(ExperiencePhoto::class, 5)->make());
            $e->experienceVersionPhotos()->save(factory(ExperiencePhoto::class)->states('cover')->make());
        });

        // Add custom additional services
        foreach (ExperienceVersion::all() as $experienceVersion) {
            for ($i = 0; $i < $this->fake->randomNumber(2); $i++) {
                $additionalServiceData = [
                    'experience_version_id' => $experienceVersion->id
                ];
                factory(\App\Models\AdditionalService::class)->create($additionalServiceData);
            }
        }

        // Add seo
        $seoController = new SeosController();
        foreach (ExperienceVersion::all() as $experienceVersion) {
            $seoController->createOrUpdateSeo(
                $experienceVersion->title,
                $experienceVersion->description,
                $experienceVersion->experience()->first()->boat()->first()->user()->first()->language,
                $experienceVersion->id
            );
        }

        // Add non-colliding availabilities
        foreach (Experience::all() as $experience) {
            $lastAvailabilityDateEndOffset = 2;
            for ($i = 0; $i < (int)($datasetLength / random_int(10, $datasetLength)); $i++) {
                $rnd = random_int(5, 50);
                $availabilityData = [
                    'experience_id' => $experience->id,
                    'date_start' => date('Y-m-d', strtotime('+' . ($lastAvailabilityDateEndOffset) . 'days')),
                    'date_end' => date('Y-m-d', strtotime('+' . ($lastAvailabilityDateEndOffset+$rnd) . 'days'))
                ];
                $lastAvailabilityDateEndOffset += $rnd + random_int(5, 50);
                factory(\App\Models\ExperienceAvailability::class)->states('dummy')->create($availabilityData);
            }
        }

        // Add periods and prices
        // Generate random non colliding periods
        foreach (Experience::all() as $experience) {
            if ($experience->lastExperienceVersion()->is_fixed_price) {
                $periodData = [
                    'experience_id' => $experience->id,
                    'date_start' => null,
                    'date_end' => null
                ];
                factory(\App\Models\Period::class)->states('dummy')->create($periodData);
            } else {
                for ($i = 0; $i < (int)($datasetLength / random_int(1, $datasetLength)); $i++) {
                    $periodData = [
                        'experience_id' => $experience->id,
                        'date_start' => Carbon::today()->toDateString(),
                        'date_end' => date('Y-m-d', strtotime('+' . (($i + 2) * 15) . 'days'))
                    ];
                    factory(\App\Models\Period::class)->states('dummy')->create($periodData);
                }
            }
        }

        foreach (\App\Models\Period::all() as $period) {
            $rnd = (int)($datasetLength / random_int(1, $datasetLength));
            for ($i = 0; $i < $rnd; $i++) {
                if ($i == 0) {
                    $priceData = ['period_id' => $period->id, 'person' => 1, 'price' => random_int(1, 150)];
                    factory(\App\Models\Price::class)->states('dummy')->create($priceData);
                } else {
                    $numbers = range(2, $rnd + 10);
                    shuffle($numbers);
                    array_slice($numbers, 0, $rnd - 1);
                    $priceData = ['period_id' => $period->id, 'person' => $numbers[$i], 'price' => random_int(1, 150)];
                    factory(\App\Models\Price::class)->states('dummy')->create($priceData);
                }
            }
        }

        // Add bookings and conversations
        $bookings = factory(\App\Models\Booking::class, $datasetLength)->states('dummy')
            ->create()
            ->each(function ($e) {
                $e->user()->associate(User::all()->random());
                $experienceVersion = Experience::all()->random()->experienceVersions()->get()->where('is_finished', true)->last();
                $e->experienceVersion()->associate($experienceVersion);
                $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
                $conversation->experience()->associate(Experience::all()->random());
                $conversation->save();
                $e->conversation()->associate($conversation);

                $fixedServiceIds = [];
                $fixedServices = ExperienceVersionsFixedAdditionalServices::all()
                    ->where('experience_version_id', $experienceVersion->id);
                $totalSeats = $e->adults + $e->kids + $e->babies;
                $sumPriceServices = 0;
                foreach ($fixedServices as $fixedService) {
                    // Save model and id for later edits
                    $fixedServiceIds[] = $fixedService->fixed_additional_service_id;

                    if ($fixedService->per_person) {
                        $price = $fixedService->price * $totalSeats;
                    } else {
                        $price = $fixedService->price;
                    }

                    $sumPriceServices += ceil(PriceHelper::convertPrice($fixedService->currency, $e->currency, $price));
                }

                $additionalServiceIds = [];
                $additionalServices = \App\Models\AdditionalService::all()
                    ->where('experience_version_id', $experienceVersion->id);
                foreach ($additionalServices as $additionalService) {
                    // Save model and id for later edits
                    $additionalServiceIds[] = $additionalService->id;

                    if ($additionalService->per_person) {
                        $price = $additionalService->price * $totalSeats;
                    } else {
                        $price = $additionalService->price;
                    }
                    $sumPriceServices += ceil(PriceHelper::convertPrice($additionalService->currency, $e->currency, $price));
                }

                $e->price = $e->price + $sumPriceServices;

                // Associate fixed additional services
                $e->fixedAdditionalServices()->attach($fixedServiceIds);
                // Associate additional services
                $e->additionalServices()->attach($additionalServiceIds);
                $e->save();
            });

        // Add messages to conversations
        \App\Models\Conversation::all()->each(function ($c) use ($datasetLength) {
            factory(\App\Models\Message::class, $datasetLength)->states('dummy', 'unread')
                ->create()
                ->each(function ($m) use ($c) {
                    $m->user()->associate($c->user()->first());
                    $m->conversation()->associate($c);
                    $m->save();
                });
            factory(\App\Models\Message::class, $datasetLength)->states('dummy', 'unread')
                ->create()
                ->each(function ($m) use ($c) {
                    $m->user()->associate($c->captain()->first());
                    $m->conversation()->associate($c);
                    $m->save();
                });
        });
    }
}