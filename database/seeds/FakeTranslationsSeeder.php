<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FakeTranslationsSeeder extends Seeder
{
    protected $fake;

    /**
     * FakeDataSeeder constructor.
     */
    public function __construct()
    {
        $this->fake = Faker::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add pages, sentences and translations

        for ($i = 0; $i < 10; $i++) {
            $it_failed = 0;
            do {
                try {
                    $page = factory(\App\TranslationModels\Page::class)->create();
                    $it_failed = 0;
                } catch (\Exception $e) {
                    $it_failed = 1;
                }
            } while ($it_failed);

            $en = \App\TranslationModels\Language::all()->where('language', 'en')->first();
            $it = \App\TranslationModels\Language::all()->where('language', 'it')->first();

            for ($j = 0; $j < 10; $j++) {
                $it_failed = 0;
                do {
                    DB::beginTransaction();
                    try {
                        $sentence = new \App\TranslationModels\Sentence(['name' => $this->fake->word]);
                        $sentenceTranslationIt = new \App\TranslationModels\SentenceTranslation(['text' => $this->fake->text]);
                        $sentenceTranslationEn = new \App\TranslationModels\SentenceTranslation(['text' => $this->fake->text]);
                        $page->sentences()->save($sentence);
                        $sentenceTranslationIt->language()->associate($it);
                        $sentenceTranslationEn->language()->associate($en);
                        $sentence->sentenceTranslations()->save($sentenceTranslationIt);
                        $sentence->sentenceTranslations()->save($sentenceTranslationEn);
                        $it_failed = 0;
                    } catch (\Exception $e) {
                        DB::rollBack();
                        $it_failed = 1;
                    }
                } while ($it_failed);
                DB::commit();
            }
        }
    }
}
