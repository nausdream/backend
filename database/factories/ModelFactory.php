<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


use App\Constant;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => null,
        'last_name' => null,
        'docking_place' => null,
        'first_name_skipper' => null,
        'last_name_skipper' => null,
        'skippers_have_license' => null,
        'first_name_contact' => null,
        'last_name_contact' => null,
        'email' => null,
        'phone' => null,
        'password' => null,
        'is_captain' => false,
        'captain_type' => null,
        'is_skipper' => null,
        'sex' => null,
        'birth_date' => null,
        'nationality' => null,
        'currency' => 'EUR',
        'language' => 'it',
        'description' => null,
        'enterprise_name' => null,
        'vat_number' => null,
        'enterprise_address' => null,
        'enterprise_city' => null,
        'enterprise_zip_code' => null,
        'enterprise_country' => null,
        'has_card' => null,
        'default_fee' => null,
        'photo_version' => null,
        'is_partner' => false,
        'is_mail_activated' => false,
        'is_suspended' => false,
        'newsletter' => false,
        'fuid' => null,
        'is_phone_activated' => false,
        'is_set_password' => false,
        'paypal_account' => null
    ];
});

$factory->state(App\Models\User::class, 'captain', function (Faker\Generator $faker) {
    return [
        'is_captain' => true,
    ];
});

$factory->state(App\Models\User::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'docking_place' => $faker->city,
        'first_name_skipper' => $faker->firstName,
        'last_name_skipper' => $faker->lastName,
        'skippers_have_license' => $faker->boolean,
        'first_name_contact' => $faker->firstName,
        'last_name_contact' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'password' => $faker->password,
        'is_captain' => $faker->boolean,
        'captain_type' => $faker->numberBetween(1, 3),
        'is_skipper' => $faker->boolean,
        'sex' => $faker->randomElement(['m', 'f']),
        'birth_date' => $faker->date(),
        'nationality' => \App\Models\Country::inRandomOrder()->first()->name,
        'currency' => \App\Models\Currency::all()->count() > 0 ? \App\Models\Currency::all()->random()->name : 'USD',
        'language' => \App\TranslationModels\Language::all()->count() > 0 ? \App\TranslationModels\Language::all()->random()->language : 'en',
        'description' => $faker->text(100),
        'enterprise_name' => $faker->company,
        'vat_number' => str_random(20),
        'enterprise_address' => $faker->address,
        'enterprise_city' => $faker->city,
        'enterprise_zip_code' => $faker->countryCode,
        'enterprise_country' => \App\Models\Country::inRandomOrder()->first()->name,
        'has_card' => $faker->boolean,
        'default_fee' => $faker->numberBetween(0, 50),
        'photo_version' => $faker->numberBetween(1000000, 9999999),
        'is_partner' => $faker->boolean,
        'is_mail_activated' => $faker->boolean,
        'is_suspended' => $faker->boolean,
        'newsletter' => $faker->boolean,
        'fuid' => str_random(20),
        'is_phone_activated' => $faker->boolean,
        'is_set_password' => $faker->boolean,
        'paypal_account' => $faker->email
    ];
});

$factory->define(App\Models\Boat::class, function (Faker\Generator $faker) {
    return [
        'user_id' => \App\Models\User::all()->random()->id,
        'fee' => env('DEFAULT_FEE'),
    ];
});

$factory->define(App\Models\BoatVersion::class, function (Faker\Generator $faker) {
    return [
        'model' => null,
        'type' => null,
        'docking_place' => null,
        'seats' => null,
        'berths' => null,
        'cabins' => null,
        'toilets' => null,
        'production_site' => null,
        'name' => null,
        'description' => null,
        'indications' => null,
        'rules' => null,
        'construction_year' => null,
        'restoration_year' => null,
        'material' => null,
        'length' => null,
        'has_insurance' => null,
        'motors_quantity' => null,
        'motor_type' => null,
        'lat' => null,
        'lng' => null,
        'status' => null,
        'boat_id' => \App\Models\Boat::all()->random()->id,
        'admin_id' => null,
        'is_finished' => false,
        'events' => false,
    ];
});

$factory->state(\App\Models\BoatVersion::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'model' => $faker->firstNameFemale,
        'type' => \App\Models\BoatType::inRandomOrder()->first()->name,
        'docking_place' => $faker->text(Constant::DOCKING_PLACE_LENGTH),
        'seats' => $faker->numberBetween(1, 500),
        'berths' => $faker->numberBetween(0, 500),
        'cabins' => $faker->numberBetween(0, 500),
        'toilets' => $faker->numberBetween(0, 500),
        'production_site' => $faker->text(Constant::PRODUCTION_SITE_LENGTH),
        'name' => $faker->text(Constant::BOAT_NAME_LENGTH),
        'description' => $faker->text(Constant::BOAT_DESCRIPTION_LENGTH),
        'rules' => $faker->text(Constant::BOAT_RULES_LENGTH),
        'indications' => $faker->text(Constant::BOAT_INDICATIONS_LENGTH),
        'construction_year' => $faker->numberBetween(0, date('Y')),
        'restoration_year' => $faker->numberBetween(0, date('Y')),
        'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->name,
        'length' => $faker->numberBetween(1, 500),
        'has_insurance' => $faker->boolean,
        'motors_quantity' => $faker->numberBetween(1, 10),
        'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'events' => $faker->boolean(),
    ];
});

$factory->state(\App\Models\BoatVersion::class, 'finished_and_accepted', function (Faker\Generator $faker) {
    return [
        'status' => Constant::STATUS_ACCEPTED,
        'is_finished' => true
    ];
});

$factory->state(\App\Models\BoatVersion::class, 'finished', function (Faker\Generator $faker) {
    return [
        'is_finished' => true
    ];
});

$factory->state(\App\Models\BoatVersion::class, 'accepted', function (Faker\Generator $faker) {
    return [
        'status' => Constant::STATUS_ACCEPTED
    ];
});

$factory->define(App\Models\Administrator::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'currency' => App\Models\Currency::inRandomOrder()->first()->name,
        'language' => App\TranslationModels\Language::inRandomOrder()->first()->language,
        'area_id' => null
    ];
});

$factory->state(App\Models\Administrator::class, 'super_admin', function (Faker\Generator $faker) {
    $world = \App\Models\Area::all()->where('name', 'world')->first();
    if (!isset($world)){
        $world = factory(\App\Models\Area::class)->states('world')->create();
    }
    return [
        'area_id' => $world->id
    ];
});

$factory->define(\App\TranslationModels\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'is_admin' => false
    ];
});

$factory->state(\App\TranslationModels\User::class, 'admin', function (Faker\Generator $faker) {
    return [
        'is_admin' => true,
    ];
});

$factory->define(\App\TranslationModels\Page::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

$factory->define(\App\Models\Experience::class, function (Faker\Generator $faker) {
    return [
        'boat_id' => \App\Models\Boat::all()->random()->id
    ];
});

$factory->define(\App\Models\ExperienceVersion::class, function (Faker\Generator $faker) {
    return [
        'experience_id' => \App\Models\Experience::all()->random()->id,
        'is_finished' => false,
        'is_searchable' => true,
        'currency' =>  \App\Models\Currency::all()->count() > 0 ? \App\Models\Currency::all()->random()->name : 'USD'

    ];
});

$factory->state(\App\Models\ExperienceVersion::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'type' => \App\Models\ExperienceType::all()->random()->name,
        'title' => $faker->text(40),
        'description' => $faker->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
        'rules' => $faker->text(Constant::EXPERIENCE_RULES_LENGTH),
        'duration' => $faker->randomNumber(2),
        'departure_port' => $faker->text(Constant::PORT_DEPARTURE_LENGTH),
        'departure_time' => $faker->time(),
        'arrival_port' => $faker->text(Constant::PORT_RETURN_LENGTH),
        'arrival_time' => $faker->time(),
        'monday' => $faker->boolean,
        'tuesday' => $faker->boolean,
        'wednesday' => $faker->boolean,
        'thursday' => $faker->boolean,
        'friday' => $faker->boolean,
        'saturday' => $faker->boolean,
        'sunday' => $faker->boolean,
        'is_fixed_price' => $faker->boolean,
        'seats' => $faker->numberBetween(5,20),
        'is_searchable' => true,
        'departure_lat' => $faker->latitude,
        'departure_lng' => $faker->longitude,
        'destination_lat' => $faker->latitude,
        'destination_lng' => $faker->longitude,
        'road_stead' => $faker->boolean,
        'fixed_menu' => $faker->boolean,
        'starter_dish' => $faker->boolean,
        'first_dish' => $faker->boolean,
        'second_dish' => $faker->boolean,
        'side_dish' => $faker->boolean,
        'dessert_dish' => $faker->boolean,
        'bread' => $faker->boolean,
        'wines' => $faker->boolean,
        'champagne' => $faker->boolean,
        'weather' => $faker->boolean,
        'lures' => $faker->boolean,
        'fishing_pole' => $faker->boolean,
        'fresh_bag' => $faker->boolean,
        'ice' => $faker->boolean,
        'underwater_baptism' => $faker->boolean,
        'down_payment' => $faker->boolean,
        'days' => $faker->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
        'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->id,
        'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->id,
        'deposit' => $faker->randomElement([$faker->numberBetween(0,1000), null]),
        'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
        'deposit_card' => $faker->boolean,
        'deposit_check' => $faker->boolean,
        'deposit_cash' => $faker->boolean,
        'currency' =>  \App\Models\Currency::all()->count() > 0 ? \App\Models\Currency::all()->random()->name : 'USD',
        'captain_night_aboard' => $faker->boolean,
        'kids' => $faker->randomNumber(1),
        'babies' => $faker->randomNumber(1),
    ];
});

$factory->state(\App\Models\ExperienceVersion::class, 'finished_and_accepted', function (Faker\Generator $faker) {
    return [
        'status' => Constant::STATUS_ACCEPTED,
        'is_finished' => true
    ];
});

$factory->state(\App\Models\ExperienceVersion::class, 'finished', function (Faker\Generator $faker) {
    return [
        'is_finished' => true
    ];
});

$factory->state(\App\Models\ExperienceVersion::class, 'accepted', function (Faker\Generator $faker) {
    return [
        'status' => Constant::STATUS_ACCEPTED
    ];
});

$factory->define(\App\Models\Area::class, function (Faker\Generator $faker) {
    $point_a_lat = $faker->latitude;
    $point_a_lng = $faker->longitude;

    return [
        'name' => $faker->country,
        'point_a_lat' => $point_a_lat,
        'point_a_lng' => $point_a_lng,
        'point_b_lat' => $point_a_lat + $faker->randomFloat(null, 1, 5),
        'point_b_lng' => $point_a_lng + $faker->randomFloat(null, 1, 5)
    ];
});

$factory->state(\App\Models\Area::class, 'world', function (Faker\Generator $faker) {
    return [
        'name' => 'world',
        'point_a_lat' => -50000,
        'point_a_lng' => -50000,
        'point_b_lat' => 50000,
        'point_b_lng' => 50000
    ];
});

$factory->define(\App\Models\Coupon::class, function (Faker\Generator $faker) {
    return [
        'name' => str_random(Constant::COUPON_NAME_LENGTH),
        'is_percentual' => $faker->boolean,
        'amount' => $faker->numberBetween(0, 100),
        'is_consumable' => $faker->boolean
    ];
});

$factory->state(\App\Models\Coupon::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'expiration_date' => date('Y-m-d', strtotime('+' . $faker->numberBetween(0, 5) . ' months')),
        'is_consumed' => $faker->boolean,
        'currency' => \App\Models\Currency::all()->count() > 0 ? \App\Models\Currency::all()->random()->name : 'USD'
    ];
});

$factory->define(\App\Models\Step::class, function (Faker\Generator $faker) {
    return [
        'step' => $faker->numberBetween(1, 4),
        'message' => $faker->text,
        'experience_version_id' => \App\Models\ExperienceVersion::all()->random()->id
    ];
});

$factory->define(\App\Models\Booking::class, function (Faker\Generator $faker) {
    $array = [
        'experience_date' => \Carbon\Carbon::today()->addDays(15)->format('Y-m-d'),
        'request_date' => \Carbon\Carbon::today()->format('Y-m-d'),
        'status' => Constant::BOOKING_STATUS_PENDING,
        'price' => $faker->numberBetween(400,1000),
        'adults' => $faker->numberBetween(3,7),
        'kids' => 0,
        'babies' => 0,
        'currency' => \App\Models\Currency::all()->random()->name,
        'entire_boat' => $faker->boolean(),
        'fee' => $faker->numberBetween(10,20),
        'discount' => 0,
        'user_id' => \App\Models\User::all()->random()->id,
        'experience_version_id' => \App\Models\ExperienceVersion::all()->random()->id
    ];
    $array['price_to_pay'] = floor($array['price'] / 100 * $array['fee']);
    $array['price_on_board'] =  $array['price'] - $array['price_to_pay'];
    return $array;
});

$factory->state(\App\Models\Booking::class, 'dummy', function (Faker\Generator $faker) {
    $rng = $faker->numberBetween(20, 50);
    $status = array_rand(array_flip(
        [
            Constant::BOOKING_STATUS_PENDING,
            Constant::BOOKING_STATUS_ACCEPTED, Constant::BOOKING_STATUS_REJECTED,
            Constant::BOOKING_STATUS_PAID, Constant::BOOKING_STATUS_CANCELLED
        ]));
    $isExperienceDone = $status == Constant::BOOKING_STATUS_PAID;

    $price = $faker->numberBetween(50, 200);
    $fee = $faker->numberBetween(10, 20);
    $attributes = [
        'experience_date' => date('Y-m-d', strtotime('+' . $rng . ' days')),
        'request_date' => date('Y-m-d', strtotime('+' . (int)($rng / 3) . ' days')),
        'acceptance_date' => $status == Constant::BOOKING_STATUS_ACCEPTED ? date('Y-m-d', strtotime('+' . (int)($rng / 2) . ' days')) : null,
        'status' => $status,
        'price' => $price,
        'adults' => $faker->numberBetween(0, 3),
        'kids' => $faker->numberBetween(0, 3),
        'babies' => $faker->numberBetween(0, 3),
        'entire_boat' => $faker->boolean(),
        'conversation_id' => \App\Models\Conversation::all()->count() > 0 ? \App\Models\Conversation::all()->random()->id : null,
        'contact_customer_id' => \App\Models\ContactCustomer::all()->count() > 0 ? \App\Models\ContactCustomer::all()->random()->id : null,
        'currency' => \App\Models\Currency::all()->random()->name,
        'fee' => $fee,
        'discount' => $faker->numberBetween(1, 30),
        'price_on_board' => $price - floor(($price / 100) * $fee),
        'price_to_pay' => floor(($price / 100) * $fee)
    ];

    if ($isExperienceDone){
        $attributes += [
            'feedback' => $isExperienceDone ? $faker->text() : null,
            'acceptance_date' => date('Y-m-d', strtotime('+' . (int)($rng / 2) . ' days')),
            'is_feedback_approved' => $isExperienceDone ? $faker->boolean : null,
            'captain_feedback_id' => \App\Models\CaptainFeedback::all()->count() > 0 ? \App\Models\CaptainFeedback::all()->random()->id : null,
            'user_feedback_id' => \App\Models\UserFeedback::all()->count() > 0 ? \App\Models\UserFeedback::all()->random()->id : null,
            'boat_feedback_id' => \App\Models\BoatFeedback::all()->count() > 0 ? \App\Models\BoatFeedback::all()->random()->id : null,
            'experience_feedback_id' => \App\Models\ExperienceFeedback::all()->count() > 0 ? \App\Models\ExperienceFeedback::all()->random()->id : null
        ];
    }
    return $attributes;
});

$factory->state(\App\Models\Booking::class, 'passed', function (Faker\Generator $faker) {
    return [
        'feedback' => $faker->text(),
        'acceptance_date' => date('Y-m-d', strtotime('+' . $faker->numberBetween(1, 5) . ' days')),
        'status' => Constant::BOOKING_STATUS_PAID,
        'is_feedback_approved' => $faker->boolean,
        'conversation_id' => \App\Models\Conversation::all()->count() > 0 ? \App\Models\Conversation::all()->random()->id : null,
        'captain_feedback_id' => \App\Models\CaptainFeedback::all()->count() > 0 ? \App\Models\CaptainFeedback::all()->random()->id : null,
        'user_feedback_id' => \App\Models\UserFeedback::all()->count() > 0 ? \App\Models\UserFeedback::all()->random()->id : null,
        'boat_feedback_id' => \App\Models\BoatFeedback::all()->count() > 0 ? \App\Models\BoatFeedback::all()->random()->id : null,
        'experience_feedback_id' => \App\Models\ExperienceFeedback::all()->count() > 0 ? \App\Models\ExperienceFeedback::all()->random()->id : null
    ];
});

$factory->define(\App\Models\Conversation::class, function (Faker\Generator $faker) {
    $usersRandom = \App\Models\User::all()->random(2);
    return [
        'request_date' => date('Y-m-d'),
        'adults' => 0,
        'kids' => 0,
        'babies' => 0,
        'user_id' => $usersRandom->first()->id,
        'captain_id' => $usersRandom->last()->id,
        'experience_id' => \App\Models\Experience::all()->random()->id,
        'alert' => false
    ];
});

$factory->state(\App\Models\Conversation::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'request_date' => $faker->date('Y-m-d', 'now'),
        'adults' => $faker->numberBetween(0, 3),
        'kids' => $faker->numberBetween(0, 3),
        'babies' => $faker->numberBetween(0, 3),
        'alert' => $faker->boolean,
    ];
});

$factory->define(\App\Models\Message::class, function (Faker\Generator $faker) {
    return [
        'message' => '',
        'read' => false,
        'user_id' => \App\Models\User::all()->random()->id,
        'conversation_id' => \App\Models\Conversation::all()->random()->id
    ];
});

$factory->state(\App\Models\Message::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'message' => $faker->text,
        'read' => $faker->boolean,
        'user_id' => \App\Models\User::all()->random()->id,
        'conversation_id' => \App\Models\Conversation::all()->random()->id
    ];
});

$factory->state(\App\Models\Message::class, 'read', function (Faker\Generator $faker) {
    return [
        'read' => true
    ];
});

$factory->state(\App\Models\Message::class, 'unread', function (Faker\Generator $faker) {
    return [
        'read' => false
    ];
});

$factory->define(\App\Models\Period::class, function (Faker\Generator $faker) {
    return [
        'is_default' => false,
        'entire_boat' => false,
        'price' => 0,
        'currency' => 'EUR',
        'experience_id' => \App\Models\Experience::all()->random()->id
    ];
});

$factory->state(\App\Models\Period::class, 'dummy', function (Faker\Generator $faker) {
    $rng = $faker->numberBetween(20, 50);
    return [
        'date_start' => date('Y-m-d', strtotime('+' . (int)($rng/3) . ' days')),
        'date_end' => date('Y-m-d', strtotime('+' . $rng . ' days')),
        'is_default' => $faker->boolean,
        'min_person' => $faker->randomElement([1, 2]),
        'entire_boat' => $faker->boolean,
        'price' => $faker->numberBetween(1, 200),
        'currency' => \App\Models\Currency::all()->random()->name
    ];
});

$factory->define(\App\Models\Price::class, function (Faker\Generator $faker) {
    return [
        'period_id' => \App\Models\Period::all()->random()->id,
        'price' => 0,
        'currency' => 'EUR'
    ];
});

$factory->state(\App\Models\Price::class, 'dummy', function (Faker\Generator $faker) {
    return [
        'person' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(20, 100),
        'currency' => \App\Models\Currency::all()->random()->name
    ];
});

$factory->define(\App\Models\Seo::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->text(Constant::SEO_TITLE_LENGTH),
        'description' =>  $faker->text(Constant::SEO_DESCRIPTION_LENGTH),
        'language' => App\TranslationModels\Language::inRandomOrder()->first()->language,
        'slug_url' => str_slug($faker->text(Constant::SEO_TITLE_LENGTH)),
    ];
});

$factory->define(\App\Models\ExperiencePhoto::class, function (Faker\Generator $faker) {
    return [
        'is_cover' => false,
        'experience_version_id' =>  \App\Models\ExperienceVersion::all()->random()->first()->id
    ];
});
$factory->state(\App\Models\ExperiencePhoto::class, 'cover', function (Faker\Generator $faker) {
    return [
        'is_cover' => true
    ];
});

// STAGING
$factory->state(\App\Models\BoatVersion::class, 'staging', function (Faker\Generator $faker) {
    return [
        'model' => $faker->word,
        'docking_place' => $faker->address,
        'production_site' => $faker->address,
        'name' => $faker->firstNameFemale
    ];
});
$factory->state(\App\Models\ExperienceVersion::class, 'staging', function (Faker\Generator $faker) {
    return [
        'title' => $faker->realText(40),
        'description' => $faker->realText(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
        'rules' => $faker->realText(Constant::EXPERIENCE_RULES_LENGTH),
        'departure_port' => $faker->address,
        'arrival_port' => $faker->address
    ];
});

$factory->define(\App\Models\ExperienceAvailability::class, function (Faker\Generator $faker) {
    return [
        'experience_id' =>  \App\Models\Experience::all()->random()->first()->id,
        'date_start' => date('Y-m-d'),
        'date_end' => date('Y-m-d')
    ];
});

$factory->state(\App\Models\ExperienceAvailability::class, 'dummy', function (Faker\Generator $faker) {
    $rng = $faker->numberBetween(20, 50);
    return [
        'date_start' => date('Y-m-d', strtotime('+' . (int)($rng/3) . ' days')),
        'date_end' => date('Y-m-d', strtotime('+' . $rng . ' days'))
    ];
});

$factory->define(\App\Models\AdditionalService::class, function (Faker\Generator $faker) {
    return [
        'price' => $faker->numberBetween(0, 10),
        'per_person' => $faker->boolean,
        'experience_version_id' =>  \App\Models\ExperienceVersion::all()->random()->first()->id,
        'currency' => \App\Models\Currency::all()->random()->name,
        'name' => $faker->realText(Constant::ADDITIONAL_SERVICE_LENGTH)
    ];
});

$factory->define(\App\Models\OffsiteBooking::class, function (Faker\Generator $faker) {
    $array = [
        'experience_date' => \Carbon\Carbon::today()->addDays(15)->format('Y-m-d'),
        'arrival_date' => \Carbon\Carbon::today()->addDays(16)->format('Y-m-d'),
        'expiration_date' => \Carbon\Carbon::today()->addDays(10)->format('Y-m-d'),
        'experience_title' => $faker->text(Constant::EXPERIENCE_TITLE_LENGTH),
        'experience_description' => $faker->text,
        'boat_type' => \App\Models\BoatType::inRandomOrder()->first()->name,
        'departure_port' => $faker->city,
        'departure_time' => $faker->time(),
        'arrival_port' => $faker->city,
        'arrival_time' => $faker->time(),
        'adults' => $faker->numberBetween(1, 3),
        'kids' => $faker->numberBetween(0,2),
        'babies' => $faker->numberBetween(0,2),
        'included_services' => $faker->text(),
        'excluded_services' => $faker->text(),
        'drinks' => $faker->text(),
        'dishes' => $faker->text(),
        'price' => $faker->numberBetween(50, 200),
        'fee' => $faker->numberBetween(50, 200),
        'currency' => \App\Models\Currency::all()->random()->name,
        'language' => \App\TranslationModels\Language::all()->random()->language,
        'first_name_captain' => $faker->firstName,
        'last_name_captain' => $faker->lastName,
        'email_captain' => $faker->email,
        'phone_captain' => $faker->phoneNumber,
        'status' => Constant::OFFSITE_BOOKING_STATUS_PENDING
    ];
    return $array;
});

$factory->state(\App\Models\OffsiteBooking::class, 'paid', function (Faker\Generator $faker) {
    return [
        'status' => Constant::OFFSITE_BOOKING_STATUS_PAID
    ];
});
