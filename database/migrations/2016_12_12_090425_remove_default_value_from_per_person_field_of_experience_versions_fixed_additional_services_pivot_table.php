<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDefaultValueFromPerPersonFieldOfExperienceVersionsFixedAdditionalServicesPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->boolean('per_person')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->boolean('per_person')->default(false)->change();
        });
    }
}
