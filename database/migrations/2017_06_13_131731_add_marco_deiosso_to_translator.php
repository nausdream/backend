<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarcoDeiossoToTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $marcoDeiosso = factory(\App\TranslationModels\User::class)->create(
            [
                'first_name' => 'Marco',
                'last_name' => 'Deiosso',
                'email' => 'marco@nausdream.com',
                'phone' => '+393485447113',
            ]
        );

        $languageIt = \App\TranslationModels\Language::all()->where('language', 'it')->first();
        $languageEn = \App\TranslationModels\Language::all()->where('language', 'en')->first();
        $marcoDeiosso->languages()->attach($languageIt->id);
        $marcoDeiosso->languages()->attach($languageEn->id);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();
        $marcoDeiosso = \App\TranslationModels\User::all()->where('email', 'marco@nausdream.com')->last();
        $marcoDeiosso->languages()->detach();
        $marcoDeiosso->forceDelete();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }
}
