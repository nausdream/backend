<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCountryExternalKeyOnUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_enterprise_country_foreign');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('enterprise_country', Constant::COUNTRY_NAME_LENGTH)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('enterprise_country');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('enterprise_country')->unsigned()->nullable();
            $table->foreign('enterprise_country')->references('id')->on('countries');
        });
    }
}
