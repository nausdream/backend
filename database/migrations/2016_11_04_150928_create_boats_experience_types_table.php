<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatsExperienceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boats_experience_types', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('boat_id')->unsigned();
            $table->foreign('boat_id')->references('id')->on('boats');

            $table->bigInteger('experience_type_id')->unsigned();
            $table->foreign('experience_type_id')->references('id')->on('experience_types');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('boats_experience_types');
        Schema::enableForeignKeyConstraints();
    }
}
