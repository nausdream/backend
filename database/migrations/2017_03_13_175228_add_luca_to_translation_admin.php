<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLucaToTranslationAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        factory(\App\TranslationModels\User::class)->states('admin')->create(
            [
                'first_name' => 'Luca',
                'last_name' => 'Puddu',
                'email' => 'luca@nausdream.com',
                'phone' => '+393495867908',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();
        $lucaPuddu = \App\TranslationModels\User::all()->where('email', 'luca@nausdream.com')->last();
        $lucaPuddu->languages()->detach();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }
}
