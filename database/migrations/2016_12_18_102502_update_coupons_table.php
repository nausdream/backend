<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            $table->bigInteger('area_id')->unsigned()->change();
            $table->boolean('is_consumable');
            $table->string('name', Constant::COUPON_NAME_LENGTH_DB)->unique()->change();
            $table->boolean('is_consumed')->nullable()->change();

            $table->dropForeign('coupons_user_id_foreign');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('coupons', function (Blueprint $table) {
            $table->bigInteger('area_id')->unsigned()->nullable()->change();
            $table->dropColumn('is_consumable');
            $table->string('name', Constant::COUPON_NAME_LENGTH_DB)->change();
            $table->boolean('is_consumed')->change();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::enableForeignKeyConstraints();
    }
}
