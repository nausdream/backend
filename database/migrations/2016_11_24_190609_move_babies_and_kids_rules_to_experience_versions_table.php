<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveBabiesAndKidsRulesToExperienceVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->integer('kids')->nullable()->default(null);
            $table->integer('babies')->nullable()->default(null);
        });

        $babies = \App\Models\Rule::all()->where('name', 'kids')->first();
        $kids = \App\Models\Rule::all()->where('name', 'babies')->first();

        if (isset($babies)){
            $babies->experienceVersions()->detach();
            $babies->save();
            $babies->delete();
        }
        if (isset($kids)){
            $kids->experienceVersions()->detach();
            $kids->save();
            $kids->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropColumn('kids');
            $table->dropColumn('babies');
        });

        $babies = new \App\Models\Rule(['name' => 'babies']);
        $babies->save();
        $kids = new \App\Models\Rule(['name' => 'kids']);
        $kids->save();
    }
}
