<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->boolean('deposit_cash')->default(false);
            $table->boolean('deposit_card')->default(false);
            $table->boolean('deposit_check')->default(false);
            $table->dropColumn('deposit_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropColumn('deposit_cash');
            $table->dropColumn('deposit_card');
            $table->dropColumn('deposit_check');
            $table->tinyInteger('deposit_method')->nullable();
        });
    }
}
