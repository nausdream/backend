<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMailTokenTableForAdminAndTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('admin_tokens');
        Schema::connection('mysql_translation')->dropIfExists('tokens');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('admin_tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token', Constant::EMAIL_TOKEN_LENGTH)->unique();

            $table->bigInteger('administrator_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::connection('mysql_translation')->create('tokens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token', Constant::EMAIL_TOKEN_LENGTH);

            $table->bigInteger('user_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('admin_tokens', function (Blueprint $table) {
            $table->foreign('administrator_id')->references('id')->on('administrators');
        });

        Schema::connection('mysql_translation')->table('tokens', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
