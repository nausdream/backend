<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatTranslationRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boat_translation_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('boat_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boat_translation_rules');
    }
}
