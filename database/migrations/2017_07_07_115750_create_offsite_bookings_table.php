<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffsiteBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offsite_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('experience_date');
            $table->string('experience_title', Constant::EXPERIENCE_TITLE_LENGTH);
            $table->text('experience_description');
            $table->tinyInteger('boat_type');
            $table->string('departure_port', Constant::PORT_DEPARTURE_LENGTH);
            $table->time('departure_time');
            $table->string('arrival_port', Constant::PORT_RETURN_LENGTH);
            $table->time('arrival_time');
            $table->integer('days');
            $table->integer('adults');
            $table->integer('kids');
            $table->integer('babies');
            $table->text('included_services')->nullable();
            $table->text('excluded_services')->nullable();
            $table->text('drinks')->nullable();
            $table->text('dishes')->nullable();
            $table->integer('price');
            $table->integer('fee');
            $table->string('currency', Constant::CURRENCY_LENGTH);
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->string('first_name_captain', Constant::FIRST_NAME_LENGTH);
            $table->string('last_name_captain', Constant::LAST_NAME_LENGTH);
            $table->string('email_captain', Constant::EMAIL_LENGTH)->unique();
            $table->string('phone_captain', Constant::PHONE_LENGTH)->unique();
            $table->tinyInteger('status')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->dropForeign('offsite_bookings_user_id_foreign');
        });

        Schema::dropIfExists('offsite_bookings');

        Schema::enableForeignKeyConstraints();
    }
}
