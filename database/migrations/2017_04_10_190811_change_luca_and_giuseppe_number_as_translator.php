<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLucaAndGiuseppeNumberAsTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Remove Ousmane from admin
        $ousmaneDieng = \App\TranslationModels\User::all()->where('email', 'ousmane@nausdream.com')->first();
        $languages[] = \App\TranslationModels\Language::all()->where('language', 'en')->first()->id;
        $languages[] = \App\TranslationModels\Language::all()->where('language', 'it')->first()->id;
        $ousmaneDieng->languages()->attach($languages);
        $ousmaneDieng->is_admin = false;
        $ousmaneDieng->save();

        // Change number to Giuseppe and Luca
        $giuseppeBasciu = \App\TranslationModels\User::all()->where('email', 'giuseppe@nausdream.com')->first();
        $giuseppeBasciu->phone = '+17873632790';
        $giuseppeBasciu->save();

        $lucaPuddu = \App\TranslationModels\User::all()->where('email', 'luca@nausdream.com')->first();
        $lucaPuddu->phone = '+17873634036';
        $lucaPuddu->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Remove Ousmane from admin
        $ousmaneDieng = \App\TranslationModels\User::all()->where('email', 'ousmane@nausdream.com')->first();
        $ousmaneDieng->languages()->detach();
        $ousmaneDieng->is_admin = true;
        $ousmaneDieng->save();

        // Change number to Giuseppe and Luca
        $giuseppeBasciu = \App\TranslationModels\User::all()->where('email', 'giuseppe@nausdream.com')->first();
        $giuseppeBasciu->phone = '+393895999330';
        $giuseppeBasciu->save();

        $lucaPuddu = \App\TranslationModels\User::all()->where('email', 'luca@nausdream.com')->first();
        $lucaPuddu->phone = '+393495867908';
        $lucaPuddu->save();
    }
}
