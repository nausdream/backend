<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaptainNightAboardOnExperienceVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->boolean('captain_night_aboard')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropColumn('captain_night_aboard');
        });
    }
}
