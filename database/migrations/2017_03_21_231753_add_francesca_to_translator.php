<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFrancescaToTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $francescaChippari = factory(\App\TranslationModels\User::class)->create(
            [
                'first_name' => 'Francesca',
                'last_name' => 'Chippari',
                'email' => 'francesca@nausdream.com',
                'phone' => '+393467066695',
            ]
        );
        $language = \App\TranslationModels\Language::all()->where('language', 'it')->first();
        $francescaChippari->languages()->attach($language->id);
        $language = \App\TranslationModels\Language::all()->where('language', 'en')->first();
        $francescaChippari->languages()->attach($language->id);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();
        $francescaChippari = \App\TranslationModels\User::all()->where('email', 'francesca@nausdream.com')->last();
        $francescaChippari->languages()->detach();
        $francescaChippari->forceDelete();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }
}
