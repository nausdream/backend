<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('point_a_lat');
            $table->double('point_a_lng');
            $table->double('point_b_lat');
            $table->double('point_b_lng');
            $table->integer('fee');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_fees');
    }
}
