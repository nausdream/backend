<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceTypesFixedAdditionalServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_types_fixed_additional_services', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('experience_type_id')->unsigned();
            $table->bigInteger('fixed_additional_service_id')->unsigned();

            $table->foreign('experience_type_id', 'experience_type_service_type')->references('id')->on('experience_types');
            $table->foreign('fixed_additional_service_id', 'experience_type_service_serv')->references('id')->on('fixed_additional_services');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_types_fixed_additional_services');
    }
}
