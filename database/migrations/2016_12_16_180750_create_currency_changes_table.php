<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('currency_in', Constant::CURRENCY_LENGTH);
            $table->string('currency_out', Constant::CURRENCY_LENGTH);
            $table->float('change');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_changes');
    }
}
