<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArrivalDateToOffsiteBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->date('arrival_date')->nullable();
            $table->dropColumn('days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->dropColumn('arrival_date');
            $table->integer('days');
        });
    }
}
