<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatabaseSeeds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Accessories

        $accessories = [
            'hot-water',
            'air-conditioning',
            'sports-gear',
            'kitchen',
            'outdoor-shower',
            'security-equipment',
            'echo-sounder',
            'epirb',
            'fire-extinguisher',
            'freezer',
            'fridge',
            'power-unit',
            'music-centre',
            'microwave',
            'gangway',
            'wall-socket',
            'radio',
            'heating',
            'fire-prevention-system',
            'awning',
            'tender',
            'tv',
            'kitchen-tools',
            'vhf',
            'wifi'
        ];

        foreach ($accessories as $accessoryName) {
            $accessory = new \App\Models\Accessory(['name' => $accessoryName]);
            $accessory->save();
        }

        // Authorizations

        $authorizations = ['boats', 'experiences', 'users', 'captains', 'coupons', 'partners', 'bookings', 'conversations', 'feedbacks'];

        foreach ($authorizations as $authorizationName) {
            $authorization = new \App\Models\Authorization(['name' => $authorizationName]);
            $authorization->save();
        }

        // Boat lists

        $boat_types = ['catamaran', 'motorboat', 'sailboat'];
        $boat_materials = ['fiberglass', 'wood', 'steel', 'aluminium'];
        $motor_types = ['inboard', 'outboard', 'sterndrive'];

        foreach ($boat_types as $type) {
            $boatType = new \App\Models\BoatType(['name' => $type]);
            $boatType->save();
        }
        foreach ($boat_materials as $material) {
            $boatMaterial = new \App\Models\BoatMaterial(['name' => $material]);
            $boatMaterial->save();
        }
        foreach ($motor_types as $type) {
            $motorType = new \App\Models\MotorType(['name' => $type]);
            $motorType->save();
        }

        // Countries

        $countries = [
            "Afghanistan",
            "Albania",
            "Algeria",
            "Andorra",
            "Angola",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belarus",
            "Belgium",
            "Belize",
            "Benin",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Brazil",
            "Brunei",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombi",
            "Comoros",
            "Congo (Brazzaville)",
            "Congo",
            "Costa Rica",
            "Cote d'Ivoire",
            "Croatia",
            "Cuba",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "East Timor (Timor Timur)",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Fiji",
            "Finland",
            "France",
            "Gabon",
            "Gambia, The",
            "Georgia",
            "Germany",
            "Ghana",
            "Greece",
            "Grenada",
            "Guatemala",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "India",
            "Indonesia",
            "Iran",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Korea, North",
            "Korea, South",
            "Kuwait",
            "Kyrgyzstan",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Mauritania",
            "Mauritius",
            "Mexico",
            "Micronesia",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "Qatar",
            "Romania",
            "Russia",
            "Rwanda",
            "Saint Kitts and Nevis",
            "Saint Lucia",
            "Saint Vincent",
            "Samoa",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia and Montenegro",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "Spain",
            "Sri Lanka",
            "Sudan",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Syria",
            "Taiwan",
            "Tajikistan",
            "Tanzania",
            "Thailand",
            "Togo",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Turkmenistan",
            "Tuvalu",
            "Uganda",
            "Ukraine",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Vatican City",
            "Venezuela",
            "Vietnam",
            "Yemen",
            "Zambia",
            "Zimbabwe"
        ];

        foreach ($countries as $country) {
            $countryObject = new App\Models\Country(['name' => $country]);
            $countryObject->save();
        }

        // Currencies

        $currencies = ['EUR', 'USD', 'THB'];

        foreach ($currencies as $currency) {
            $currencyObject = new App\Models\Currency(['name' => $currency]);
            $currencyObject->save();
        }

        // Currency change

        \App\Services\CurrencyChanger::run();

        // Devices

        $devices = [
            [
                'name' => 'mobile',
                'width' => 360,
                'height' => 640,
            ],
            [
                'name' => 'tablet',
                'width' => 980,
                'height' => 1280,
            ],
            [
                'name' => 'desktop',
                'width' => 1366,
                'height' => 768,
            ],


        ];

        foreach ($devices as $device) {
            $deviceObject = new App\Models\Device($device);
            $deviceObject->save();
        }

        // Experience Type

        $experiences = [
            'trips-and-tours',
            'luxury',
            'romantic',
            'dinners',
            'aperitif',
            'diving',
            'fishing',
            'other'
        ];

        foreach ($experiences as $experience) {
            $experienceType = new \App\Models\ExperienceType(['name' => $experience]);
            $experienceType->save();
        }

        // Fishing partitions

        $fishing_partitions = [
            'skipper',
            'guests',
            'guests-and-skipper',
        ];

        foreach ($fishing_partitions as $fishing_partition) {
            $fishingPartition = new \App\Models\FishingPartition(['name' => $fishing_partition]);
            $fishingPartition->save();
        }

        // Fishing Types

        $fishing_types = [
            'fishing-line',
            'pull-coast',
            'pull-height',
            'drifting',
            'artificial-lure'
        ];

        foreach ($fishing_types as $fishing_type) {
            $fishingType = new \App\Models\FishingType(['name' => $fishing_type]);
            $fishingType->save();
        }

        $additionalServices = [
            'cabin-for-special-events',
            'welcome-aperitif',
            'aperitif-with-oysters-and-champagne',
            'fishing-equipment',
            'sport-equipment',
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'snorkel',
            'scuba-tank',
            'diving-boots',
            'galley',
            'diving-hood',
            'diving-computer',
            'fuel',
            'sun-cream',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'fresh-fruit',
            'buoyancy-compensator',
            'inflatable',
            'diving-gloves',
            'tour-guide-aboard',
            'hostess',
            'interpreter',
            'sailor',
            'diving-mask',
            'customisable',
            'music',
            'diving-suite',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'surface-marker-buoy',
            'overnight-stay',
            'fins',
            'possibility-to-bring-your-own-music',
            'lunch',
            'bath-products',
            'final-cleaning-aboard',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'candlelight-table',
            'beach-towel',
            'torch',
            'transfer-from-to',
            'use-of-the-heating',
            'various-moorings',
            'ballast',
        ];

        foreach ($additionalServices as $additionalServiceName) {
            $additionalService = new \App\Models\FixedAdditionalService(['name' => $additionalServiceName]);
            $additionalService->save();
        }

        // Trips and tours

        $additionalServices = [
            'welcome-aperitif',
            'sport-equipment',
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'galley',
            'fuel',
            'sun-cream',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'fresh-fruit',
            'inflatable',
            'hostess',
            'interpreter',
            'sailor',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'fins',
            'lunch',
            'bath-products',
            'final-cleaning-aboard',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'beach-towel',
            'transfer-from-to',
            'use-of-the-heating',
            'various-moorings',
            'fishing-equipment',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'trips-and-tours')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Luxury

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'hostess',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'transfer-from-to',
            'possibility-to-bring-your-own-music',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'luxury')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Romantic

        $additionalServices = [
            'cabin-for-special-events',
            'welcome-aperitif',
            'aperitif-with-oysters-and-champagne',
            'sport-equipment',
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'galley',
            'fuel',
            'sun-cream',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'fresh-fruit',
            'inflatable',
            'hostess',
            'interpreter',
            'sailor',
            'customisable',
            'music',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'fins',
            'possibility-to-bring-your-own-music',
            'lunch',
            'bath-products',
            'final-cleaning-aboard',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'candlelight-table',
            'beach-towel',
            'transfer-from-to',
            'use-of-the-heating',
            'various-moorings',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'romantic')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Dinners

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'hostess',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'transfer-from-to',
            'possibility-to-bring-your-own-music',
            'welcome-aperitif',
            'customisable',
            'music',
            'candlelight-table',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'dinners')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Aperitif

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'hostess',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'transfer-from-to',
            'possibility-to-bring-your-own-music',
            'customisable',
            'music',
            'candlelight-table',
            'beach-towel',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'aperitif')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Diving

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'transfer-from-to',
            'beach-towel',
            'welcome-aperitif',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'galley',
            'sun-cream',
            'fresh-fruit',
            'inflatable',
            'fins',
            'lunch',
            'various-moorings',
            'snorkel',
            'scuba-tank',
            'diving-boots',
            'diving-hood',
            'diving-computer',
            'buoyancy-compensator',
            'diving-gloves',
            'tour-guide-aboard',
            'diving-mask',
            'diving-suite',
            'surface-marker-buoy',
            'torch',
            'ballast',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'diving')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Fishing

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'transfer-from-to',
            'beach-towel',
            'welcome-aperitif',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'galley',
            'sun-cream',
            'fresh-fruit',
            'inflatable',
            'lunch',
            'various-moorings',
            'tour-guide-aboard',
            'sport-equipment',
            'sailor',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'fishing')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Other

        $additionalServices = [
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'fuel',
            'breakfast',
            'sea-cook',
            'possible-moorings',
            'interpreter',
            'parking-lot',
            'vegan-meal',
            'vegetarian-meal',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'transfer-from-to',
            'beach-towel',
            'welcome-aperitif',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'tender',
            'snack',
            'galley',
            'sun-cream',
            'fresh-fruit',
            'inflatable',
            'lunch',
            'various-moorings',
            'tour-guide-aboard',
            'sport-equipment',
            'sailor',
            'fins',
            'snorkel',
            'scuba-tank',
            'diving-boots',
            'diving-hood',
            'diving-computer',
            'buoyancy-compensator',
            'diving-gloves',
            'diving-mask',
            'diving-suite',
            'surface-marker-buoy',
            'torch',
            'hostess',
            'possibility-to-bring-your-own-music',
            'customisable',
            'music',
            'candlelight-table',
            'use-of-the-heating',
            'fishing-equipment',
            'cabin-for-special-events',
            'aperitif-with-oysters-and-champagne',
        ];

        $experienceType = \App\Models\ExperienceType::all()->where('name', 'other')->last();

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }

        // Languages

        $languages = ['it', 'en'];

        foreach ($languages as $language) {
            $languageObject = new \App\TranslationModels\Language(['language' => $language]);
            $languageObject->rtl = false;
            $languageObject->save();
        }

        // Rules

        $rules = [
            'animal_small',
            'animal_medium',
            'animal_large',
            'alcohol',
            'cooking'
        ];

        foreach ($rules as $ruleName) {
            $rule = new \App\Models\Rule(['name' => $ruleName]);
            $rule->save();
        }

        // Spoken Language

        $languages = [
            'mr',
            'bs',
            'ee_TG',
            'ms',
            'kam_KE',
            'mt',
            'ha',
            'es_HN',
            'ml_IN',
            'ro_MD',
            'kab_DZ',
            'he',
            'es_CO',
            'my',
            'es_PA',
            'az_Latn',
            'mer',
            'en_NZ',
            'xog_UG',
            'sg',
            'fr_GP',
            'sr_Cyrl_BA',
            'hi',
            'fil_PH',
            'lt_LT',
            'si',
            'en_MT',
            'si_LK',
            'luo_KE',
            'it_CH',
            'teo',
            'mfe',
            'sk',
            'uz_Cyrl_UZ',
            'sl',
            'rm_CH',
            'az_Cyrl_AZ',
            'fr_GQ',
            'kde',
            'sn',
            'cgg_UG',
            'so',
            'fr_RW',
            'es_SV',
            'mas_TZ',
            'en_MU',
            'sq',
            'hr',
            'sr',
            'en_PH',
            'ca',
            'hu',
            'mk_MK',
            'fr_TD',
            'nb',
            'sv',
            'kl',
            'sw',
            'nd',
            'sr_Latn',
            'el_GR',
            'hy',
            'ne',
            'el_CY',
            'es_CR',
            'fo_FO',
            'pa_Arab_PK',
            'seh',
            'ar_YE',
            'ja_JP',
            'ur_PK',
            'pa_Guru',
            'gl_ES',
            'zh_Hant_HK',
            'ar_EG',
            'nl',
            'th_TH',
            'es_PE',
            'fr_KM',
            'nn',
            'kk_Cyrl_KZ',
            'kea',
            'lv_LV',
            'kln',
            'tzm_Latn',
            'yo',
            'gsw_CH',
            'ha_Latn_GH',
            'is_IS',
            'pt_BR',
            'cs',
            'en_PK',
            'fa_IR',
            'zh_Hans_SG',
            'luo',
            'ta',
            'fr_TG',
            'kde_TZ',
            'mr_IN',
            'ar_SA',
            'ka_GE',
            'mfe_MU',
            'id',
            'fr_LU',
            'de_LU',
            'ru_MD',
            'cy',
            'zh_Hans_HK',
            'te',
            'bg_BG',
            'shi_Latn',
            'ig',
            'ses',
            'ii',
            'es_BO',
            'th',
            'ko_KR',
            'ti',
            'it',
            'shi_Latn_MA',
            'pt_MZ',
            'ff_SN',
            'haw',
            'zh_Hans',
            'so_KE',
            'bn_IN',
            'en_UM',
            'to',
            'id_ID',
            'uz_Cyrl',
            'en_GU',
            'es_EC',
            'en_US_POSIX',
            'sr_Latin_BA',
            'is',
            'luy',
            'tr',
            'en_NA',
            'it',
            'da',
            'bo_IN',
            'vun_TZ',
            'ar_SD',
            'uz_Latn_UZ',
            'az_Latn_AZ',
            'de',
            'es_GQ',
            'ta_IN',
            'de_DE',
            'fr_FR',
            'rof_TZ',
            'ar_LY',
            'en_BW',
            'asa',
            'zh',
            'ha_Latn',
            'fr_NE',
            'es_MX',
            'bem_ZM',
            'zh_Hans_CN',
            'bn_BD',
            'pt_GW',
            'om',
            'jmc',
            'de_AT',
            'kk_Cyrl',
            'sw_TZ',
            'ar_OM',
            'et_EE',
            '    or',
            'da_DK',
            'ro_RO',
            'zh_Hant',
            'bm_ML',
            'ja',
            'fr_CA',
            'naq',
            'zu',
            'en_IE',
            'ar_MA',
            'es_GT',
            'uz_Arab_AF',
            'en_AS',
            'bs_BA',
            'am_ET',
            'ar_TN',
            'haw_US',
            'ar_JO',
            'fa_AF',
            'uz_Latn',
            'en_BZ',
            'nyn_UG',
            'ebu_KE',
            'te_IN',
            'cy_GB',
            'uk',
            'nyn',
            'en_JM',
            'en_US',
            'fil',
            'ar_KW',
            'af_ZA',
            'en_CA',
            'fr_DJ',
            'ti_ER',
            'ig_NG',
            'en_AU',
            'ur',
            'fr_MC',
            'pt_PT',
            'pa',
            'es_419',
            'fr_CD',
            'en_SG',
            'bo_CN',
            'kn_IN',
            'sr_Cyrl_RS',
            'lg_UG',
            'gu_IN',
            'ee',
            'nd_ZW',
            'bem',
            'uz',
            'sw_KE',
            'sq_AL',
            'hr_HR',
            'mas_KE',
            'el',
            'ti_ET',
            'es_AR',
            'pl',
            'en',
            'eo',
            'shi',
            'kok',
            'fr_CF',
            'fr_RE',
            'mas',
            'rof',
            'ru_UA',
            'yo_NG',
            'dav_KE',
            'gv_GB',
            'pa_Arab',
            'es',
            'teo_UG',
            'ps',
            'es_PR',
            'fr_MF',
            'et',
            'pt',
            'eu',
            'ka',
            'rwk_TZ',
            'nb_NO',
            'fr_CG',
            'cgg',
            'zh_Hant_TW',
            'sr_Cyrl_ME',
            'lag',
            'ses_ML',
            'en_ZW',
            'ak_GH',
            'vi_VN',
            'sv_FI',
            'to_TO',
            'fr_MG',
            'fr_GA',
            'fr_CH',
            'de_CH',
            'es_US',
            'ki',
            'my_MM',
            'vi',
            'ar_QA',
            'ga_IE',
            'rwk',
            'bez',
            'ee_GH',
            'kk',
            'as_IN',
            'ca_ES',
            'kl',
            'fr_SN',
            'ne_IN',
            'km',
            'ms_BN',
            'ar_LB',
            'ta_LK',
            'kn',
            'ur_IN',
            'fr_CI',
            'ko',
            'ha_Latn_NG',
            'sg_CF',
            'om_ET',
            'zh_Hant_MO',
            'uk_UA',
            'fa',
            'mt_MT',
            'ki_KE',
            'luy_KE',
            'kw',
            'pa_Guru_IN',
            'en_IN',
            'kab',
            'ar_IQ',
            'ff',
            'en_TT',
            'bez_TZ',
            'es_NI',
            'uz_Arab',
            'ne_NP',
            'fi',
            'khq',
            'gsw',
            'zh_Hans_MO',
            'en_MH',
            'hu_HU',
            'en_GB',
            'fr_BE',
            'de_BE',
            'saq',
            'be_BY',
            'sl_SI',
            'sr_Latn_RS',
            'fo',
            'fr',
            'xog',
            'fr_BF',
            'tzm',
            'sk_SK',
            'fr_ML',
            'he_IL',
            'ha_Latn_NE',
            'ru_RU',
            'fr_CM',
            'teo_KE',
            'seh_MZ',
            'kl_GL',
            'fi_FI',
            'kam',
            'es_ES',
            'af',
            'asa_TZ',
            'cs_CZ',
            'tr_TR',
            'es_PY',
            'tzm_Latn_MA',
            'lg',
            'ebu',
            'en_HK',
            'nl_NL',
            'en_BE',
            'ms_MY',
            'es_UY',
            'ar_BH',
            'kw_GB',
            'ak',
            'chr',
            'dav',
            'lag_TZ',
            'am',
            'so_DJ',
            'shi_Tfng_MA',
            'sr_Latn_ME',
            'sn_ZW',
            'or_IN',
            'ar',
            'as',
            'fr_BI',
            'jmc_TZ',
            'chr_US',
            'eu_ES',
            'saq_KE',
            'vun',
            'lt',
            'naq_NA',
            'ga',
            'af_NA',
            'kea_CV',
            'es_DO',
            'lv',
            'kok_IN',
            'de_LI',
            'fr_BJ',
            'az',
            'guz_KE',
            'rw_RW',
            'mg_MG',
            'km_KH',
            'gl',
            'shi_Tfng',
            'ar_AE',
            'fr_MQ',
            'rm',
            'sv_SE',
            'az_Cyrl',
            'ro',
            'so_ET',
            'en_ZA',
            'ii_CN',
            'fr_BL',
            'hi_IN',
            'gu',
            'mer_KE',
            'nn_NO',
            'gv',
            'ru',
            'ar_DZ',
            'ar_SY',
            'en_MP',
            'nl_BE',
            'rw',
            'be',
            'en_VI',
            'es_CL',
            'bg',
            'mg',
            'hy_AM',
            'zu_ZA',
            'guz',
            'mk',
            'es_VE',
            'ml',
            'bm',
            'khq_ML',
            'bn',
            'ps_AF',
            'so_SO',
            'sr_Cyrl',
            'pl_PL',
            'fr_GN',
            'bo',
            'om_KE'
        ];

        foreach ($languages as $language) {
            $languageObject = new \App\Models\Language(['language' => $language]);
            $languageObject->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();

        // Accessories

        $accessories = \App\Models\Accessory::all();
        foreach ($accessories as $accessory) {
            $accessory->forceDelete();
        }

        // Authorizations

        $authorizations = \App\Models\Authorization::all();
        foreach ($authorizations as $authorization) {
            $authorization->forceDelete();
        }

        // Boat lists

        $boatTypes = \App\Models\BoatType::all();
        foreach ($boatTypes as $boatType) {
            $boatType->forceDelete();
        }

        $boatMaterials = \App\Models\BoatMaterial::all();
        foreach ($boatMaterials as $boatMaterial) {
            $boatMaterial->forceDelete();
        }

        $motorTypes = \App\Models\MotorType::all();
        foreach ($motorTypes as $motorType) {
            $motorType->forceDelete();
        }

        // Countries

        $countries = \App\Models\Country::all();
        foreach ($countries as $country) {
            $country->forceDelete();
        }

        // Currencies

        $currencies = \App\Models\Currency::all();
        foreach ($currencies as $currency) {
            $currency->forceDelete();
        }

        // CurrencyChange

        $currencyChanges = \App\Models\CurrencyChange::all();
        foreach ($currencyChanges as $currencyChange) {
            $currencyChange->forceDelete();
        }

        // Devices

        $devices = \App\Models\Device::all();
        foreach ($devices as $device) {
            $device->forceDelete();
        }

        // Experience Type Fixed Additional Services

        $experienceTypeFixedAdditionalServices = \App\Models\ExperienceTypesFixedAdditionalServices::all();
        foreach ($experienceTypeFixedAdditionalServices  as $experienceTypeFixedAdditionalService) {
            $experienceTypeFixedAdditionalService->forceDelete();
        }

        // Experience Types

        $experienceTypes = \App\Models\ExperienceType::all();
        foreach ($experienceTypes as $experienceType) {
            $experienceType->forceDelete();
        }

        // Fishing Partition

        $fishingPartitions = \App\Models\FishingPartition::all();
        foreach ($fishingPartitions as $fishingPartition) {
            $fishingPartition->forceDelete();
        }

        // Fishing Types

        $fishingTypes = \App\Models\FishingType::all();
        foreach ($fishingTypes as $fishingType) {
            $fishingType->forceDelete();
        }

        // Fixed additional Services

        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all();
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $fixedAdditionalService->forceDelete();
        }

        // Languages

        $languages = \App\TranslationModels\Language::all();
        foreach ($languages as $language) {
            $language->forceDelete();
        }

        // Rules

        $rules = \App\Models\Rule::all();
        foreach ($rules as $rule) {
            $rule->forceDelete();
        }

        // Spoken Language

        $languages = \App\Models\Language::all();
        foreach ($languages as $language) {
            $language->forceDelete();
        }

        Schema::enableForeignKeyConstraints();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }
}
