<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOusmaneDiengToAdministrator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production') {
            $ousmaneDieng = factory(\App\Models\Administrator::class)->states('super_admin')->create(
                [
                    'first_name' => 'Ousmane',
                    'last_name' => 'Dieng',
                    'email' => 'ousmane@nausdream.com',
                    'phone' => '+17876771455',
                    'currency' => 'EUR',
                    'language' => 'it',
                ]
            );

            // Fetch authorizations
            $authorizations = \App\Models\Authorization::all();
            $authIds = [];
            foreach ($authorizations as $authorization) {
                $authIds[] = $authorization->id;
            }
            $ousmaneDieng->authorizations()->attach($authIds);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') == 'production') {
            Schema::disableForeignKeyConstraints();
            $ousmaneDieng = \App\Models\Administrator::all()->where('email', 'ousmane@nausdream.com')->last();
            $ousmaneDieng->authorizations()->detach();
            $ousmaneDieng->forceDelete();
            Schema::enableForeignKeyConstraints();
        }
    }
}
