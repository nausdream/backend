<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingsFixedAdditionalServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings_fixed_additional_services', function (Blueprint $table) {
            $table->increments('id');

            $table->bigInteger('booking_id')->unsigned();
            $table->foreign('booking_id', 'book_fixed_add_serv_book')->references('id')->on('bookings');

            $table->bigInteger('fixed_additional_service_id')->unsigned();
            $table->foreign('fixed_additional_service_id', 'book_fixed_add_serv_serv')->references('id')->on('fixed_additional_services');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings_fixed_additional_services');
    }
}
