<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePeriodsAndPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periods', function (Blueprint $table) {
            $table->date('date_start')->nullable()->change();
            $table->integer('min_person')->nullable();
            $table->boolean('entire_boat')->default(false);
            $table->integer('price');
        });

        // Change foreign key from experience_versions_id to experience_id

        if (env('APP_ENV') == 'testing') {
            Schema::disableForeignKeyConstraints();
        }

        Schema::table('periods', function (Blueprint $table) {
            $table->bigInteger('experience_id')->unsigned();
            $table->foreign('experience_id', 'periods_exp_id_foreign')->references('id')->on('experiences');
        });

        $periods = \App\Models\Period::all();
        foreach ($periods as $period) {
            $experienceVersion = \App\Models\ExperienceVersion::find($period->experience_version_id);
            $entireBoatPrice = $period->prices()->where('entire_boat', true)->get()->first();
            $period->experience_id = $experienceVersion->experience()->first()->id;
            if (isset($entireBoatPrice)){
                $period->entire_boat = true;
                $period->price = $entireBoatPrice->price;
            }
            $period->save();
        }

        Schema::table('periods', function (Blueprint $table) {
            $table->dropForeign('exp_ver_id');
            $table->dropColumn('experience_version_id');
        });

        if (env('APP_ENV') == 'testing') {
            Schema::enableForeignKeyConstraints();
        }

        Schema::table('prices', function (Blueprint $table) {
            $table->dropColumn('entire_boat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('prices', function (Blueprint $table) {
            $table->boolean('entire_boat')->default(false);
        });

        // Change foreign key from experience_id to experience_versions_id
        Schema::table('periods', function (Blueprint $table) {
            $table->bigInteger('experience_version_id')->unsigned();
            $table->foreign('experience_version_id', 'exp_ver_id')->references('id')->on('experience_versions');
        });

        $periods = \App\Models\Period::all();
        foreach ($periods as $period) {
            $experience = \App\Models\Experience::find($period->experience_id);
            $lastFinishedExperienceVersion = $experience->experienceVersions()->where('is_finished', true)->get()->last();
            if (isset($lastFinishedExperienceVersion)){
                $period->experience_version_id = $lastFinishedExperienceVersion->id;
            } else {
                $period->experience_version_id = $experience->experienceVersions()->get()->last()->id;
            }

            if ($period->entire_boat){
                $newPrice = new \App\Models\Price(["price" => $period->price, "entire_boat" => true]);
                $period->prices()->save($newPrice);
            }
            $period->save();
        }

        Schema::table('periods', function (Blueprint $table) {
            $table->dropForeign('periods_exp_id_foreign');
            $table->dropColumn('experience_id');
        });

        Schema::table('periods', function (Blueprint $table) {
            $table->dropColumn('min_person');
            $table->dropColumn('entire_boat');
            $table->dropColumn('price');
            $table->date('date_start')->change();
        });

        Schema::enableForeignKeyConstraints();
    }
}
