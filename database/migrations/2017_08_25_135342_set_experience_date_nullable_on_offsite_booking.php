<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetExperienceDateNullableOnOffsiteBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->date('experience_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->date('experience_date')->change();
        });
    }
}
