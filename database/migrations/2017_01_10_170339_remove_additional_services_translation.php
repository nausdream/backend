<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAdditionalServicesTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_services', function (Blueprint $table) {
            $table->string('name', Constant::ADDITIONAL_SERVICE_LENGTH);
        });

        Schema::table('additional_service_translations', function (Blueprint $table) {
            $table->dropForeign('additional_service_translations_additional_service_id_foreign');
        });

        Schema::dropIfExists('additional_service_translations');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_services', function (Blueprint $table) {
            $table->dropColumn('name');
        });

        Schema::create('additional_service_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('additional_service_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('additional_service_translations', function (Blueprint $table) {
            $table->foreign('additional_service_id')->references('id')->on('additional_services');
        });
    }
}
