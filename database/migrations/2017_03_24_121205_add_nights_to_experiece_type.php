<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNightsToExperieceType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $experienceType = new \App\Models\ExperienceType(['name' => 'nights']);
        $experienceType->save();

        // Nights additional services

        $additionalServices = [
            'welcome-aperitif',
            'alcoholic-drinks',
            'soft-drinks',
            'bath-linen',
            'breakfast',
            'interpreter',
            'parking-lot',
            'overnight-stay',
            'bath-products',
            'final-cleaning-aboard',
            'daily-cleaning',
            'bed-linen',
            'dock-shower',
            'transfer-from-to',
        ];

        foreach ($additionalServices as $additionalServiceName) {
            $fixedAdditionalService = \App\Models\FixedAdditionalService::all()->where('name', $additionalServiceName)->last();
            $experienceType->fixedAdditionalServices()->attach($fixedAdditionalService->id);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $experienceType = \App\Models\ExperienceType::all()->where('name', 'nights')->first();
        $experienceTypeFixedAdditionalServices = \App\Models\ExperienceTypesFixedAdditionalServices::all()->where('experience_type_id', $experienceType->id);
        foreach ($experienceTypeFixedAdditionalServices  as $experienceTypeFixedAdditionalService) {

            $experienceTypeFixedAdditionalService->forceDelete();
        }
        $experienceType->forceDelete();
    }
}
