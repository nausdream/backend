<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCurrencyChangeTablePrecisionForDoubleColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currency_changes', function (Blueprint $table) {
            $table->dropColumn('change');
        });

        Schema::table('currency_changes', function (Blueprint $table) {
            $table->float('change', 15, 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currency_changes', function (Blueprint $table) {
            $table->dropColumn('change');
        });
        Schema::table('currency_changes', function (Blueprint $table) {
            $table->float('change');
        });
    }
}
