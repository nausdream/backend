<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditExperienceVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
            $table->renameColumn('lat', 'departure_lat');
            $table->renameColumn('lng', 'departure_lng');
            $table->boolean('is_finished')->default(false)->change();

            $table->integer('days')->nullable();
            $table->boolean('road_stead')->default(false);
            $table->boolean('fixed_menu')->default(false);
            $table->boolean('starter_dish')->default(false);
            $table->boolean('first_dish')->default(false);
            $table->boolean('second_dish')->default(false);
            $table->boolean('side_dish')->default(false);
            $table->boolean('dessert_dish')->default(false);
            $table->boolean('bread')->default(false);
            $table->boolean('wines')->default(false);
            $table->boolean('champagne')->default(false);
            $table->boolean('weather')->default(false);
            $table->tinyInteger('fishing_partition')->nullable();
            $table->tinyInteger('fishing_type')->nullable();
            $table->boolean('lures')->default(false);
            $table->boolean('fishing_pole')->default(false);
            $table->boolean('fresh_bag')->default(false);
            $table->boolean('ice')->default(false);
            $table->boolean('underwater_baptism')->default(false);
            $table->boolean('down_payment')->default(false);
            $table->integer('cancellation_max_days')->nullable();
            $table->integer('deposit')->nullable();
            $table->tinyInteger('deposit_method')->nullable();
            $table->string('currency', Constant::CURRENCY_LENGTH);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->renameColumn('departure_lat', 'lat');
            $table->renameColumn('departure_lng', 'lng');
            $table->boolean('is_finished')->default(true)->change();

            $table->dropColumn('days');
            $table->dropColumn('road_stead');
            $table->dropColumn('fixed_menu');
            $table->dropColumn('starter_dish');
            $table->dropColumn('first_dish');
            $table->dropColumn('second_dish');
            $table->dropColumn('side_dish');
            $table->dropColumn('dessert_dish');
            $table->dropColumn('bread');
            $table->dropColumn('wines');
            $table->dropColumn('champagne');
            $table->dropColumn('weather');
            $table->dropColumn('fishing_partition');
            $table->dropColumn('fishing_type');
            $table->dropColumn('lures');
            $table->dropColumn('fishing_pole');
            $table->dropColumn('fresh_bag');
            $table->dropColumn('ice');
            $table->dropColumn('underwater_baptism');
            $table->dropColumn('down_payment');
            $table->dropColumn('cancellation_max_days');
            $table->dropColumn('deposit');
            $table->dropColumn('deposit_method');
            $table->dropColumn('currency');
        });
    }
}
