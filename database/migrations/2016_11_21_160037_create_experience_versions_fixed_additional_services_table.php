<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceVersionsFixedAdditionalServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price')->unsigned();
            $table->boolean('per_person')->default(false);

            $table->bigInteger('fixed_additional_service_id')->unsigned();
            $table->foreign('fixed_additional_service_id', 'exp_ver_fixed_add_ser_fixed_add_ser_id')->references('id')->on('fixed_additional_services');

            $table->bigInteger('experience_version_id')->unsigned();
            $table->foreign('experience_version_id', 'exp_ver_fixed_add_ser_exp_ver_id')->references('id')->on('experience_versions');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->dropForeign('exp_ver_fixed_add_ser_fixed_add_ser_id');
            $table->dropForeign('exp_ver_fixed_add_ser_exp_ver_id');
        });
        Schema::dropIfExists('experience_versions_fixed_additional_services');
    }
}
