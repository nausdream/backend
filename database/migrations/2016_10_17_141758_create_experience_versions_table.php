<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('experience_versions');
        Schema::create('experience_versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type');
            $table->tinyInteger('duration');
            $table->string('departure_port', Constant::PORT_DEPARTURE_LENGTH)->nullable();
            $table->time('departure_time')->nullable();
            $table->string('arrival_port', Constant::PORT_RETURN_LENGTH)->nullable();
            $table->time('arrival_time')->nullable();

            /*
             * Availabilities
             */

            $table->boolean('monday')->default(false);
            $table->boolean('tuesday')->default(false);
            $table->boolean('wednesday')->default(false);
            $table->boolean('thursday')->default(false);
            $table->boolean('friday')->default(false);
            $table->boolean('saturday')->default(false);
            $table->boolean('sunday')->default(false);

            /*
             * Rules
             */

            $table->boolean('animal_small_allowed')->default(false);
            $table->boolean('animal_medium_allowed')->default(false);
            $table->boolean('animal_large_allowed')->default(false);
            $table->boolean('alcohol_allowed')->default(false);
            $table->boolean('babies_allowed')->default(false);
            $table->boolean('kids_allowed')->default(false);
            $table->boolean('cooking_allowed')->default(false);

            /*
             * Additional services
             */

            $table->boolean('aperitif')->default(false);
            $table->boolean('aperitif_per_person')->default(false);
            $table->integer('aperitif_price')->nullable();
            $table->boolean('fishing')->default(false);
            $table->boolean('fishing_per_person')->default(false);
            $table->integer('fishing_price')->nullable();
            $table->boolean('sport')->default(false);
            $table->boolean('sport_per_person')->default(false);
            $table->integer('sport_price')->nullable();
            $table->boolean('alcohol')->default(false);
            $table->boolean('alcohol_per_person')->default(false);
            $table->integer('alcohol_price')->nullable();
            $table->boolean('analcoholic')->default(false);
            $table->boolean('analcoholic_per_person')->default(false);
            $table->integer('analcoholic_price')->nullable();
            $table->boolean('fuel')->default(false);
            $table->boolean('fuel_per_person')->default(false);
            $table->integer('fuel_price')->nullable();
            $table->boolean('sun_cream')->default(false);
            $table->boolean('sun_cream_per_person')->default(false);
            $table->integer('sun_cream_price')->nullable();
            $table->boolean('chef')->default(false);
            $table->boolean('chef_per_person')->default(false);
            $table->integer('chef_price')->nullable();
            $table->boolean('fruits')->default(false);
            $table->boolean('fruits_per_person')->default(false);
            $table->integer('fruits_price')->nullable();
            $table->boolean('inflatable')->default(false);
            $table->boolean('inflatable_per_person')->default(false);
            $table->integer('inflatable_price')->nullable();
            $table->boolean('hostess')->default(false);
            $table->boolean('hostess_per_person')->default(false);
            $table->integer('hostess_price')->nullable();
            $table->boolean('sailor')->default(false);
            $table->boolean('sailor_per_person')->default(false);
            $table->integer('sailor_price')->nullable();
            $table->boolean('tender')->default(false);
            $table->boolean('tender_per_person')->default(false);
            $table->integer('tender_price')->nullable();
            $table->boolean('snack')->default(false);
            $table->boolean('snack_per_person')->default(false);
            $table->integer('snack_price')->nullable();
            $table->boolean('towel')->default(false);
            $table->boolean('towel_per_person')->default(false);
            $table->integer('towel_price')->nullable();

            /*
             * Price
             */

            $table->boolean('is_fixed_price')->default(false);
            $table->integer('seats')->nullable();

            $table->boolean('is_searchable')->default(true);
            $table->boolean('is_finished')->default(true);
            $table->tinyInteger('status')->nullable(); //0=pending, 1=approved, 2=approved with edits, 3=rejected

            $table->bigInteger('experience_id')->unsigned();
            $table->bigInteger('admin_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experience_versions');
    }
}
