<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaptainTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captain_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', Constant::CAPTAIN_TYPE_LENGTH);
            $table->timestamps();
            $table->softDeletes();
        });

        // Accessories

        $captain_types = [
            'private',
            'enterprise',
            'association'
        ];

        foreach ($captain_types as $captain_type) {
            $captainType = new \App\Models\CaptainType(['name' => $captain_type]);
            $captainType->save();
        }

        /*
        if (env('APP_ENV') != 'development') {
            $users = \App\Models\User::all();
            foreach ($users as $user) {
                if (isset($user->captain_type)) {
                    $user->captain_type = $user->captain_type + 1;
                    $user->save();
                }
            }
        }
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('captain_types');
    }
}
