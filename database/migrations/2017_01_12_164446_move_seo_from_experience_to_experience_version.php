<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveSeoFromExperienceToExperienceVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seo', function (Blueprint $table) {
            $table->dropForeign('seo_experience_id_foreign');
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->dropColumn('experience_id');
            $table->bigInteger('experience_version_id')->unsigned();
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('seo', function (Blueprint $table) {
            $table->dropForeign('seo_experience_version_id_foreign');
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->dropColumn('experience_version_id');
            $table->bigInteger('experience_id')->unsigned();
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::enableForeignKeyConstraints();
    }
}
