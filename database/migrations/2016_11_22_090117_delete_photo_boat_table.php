<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePhotoBoatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boat_photos', function (Blueprint $table) {
            $table->dropForeign('boat_photos_boat_version_id_foreign');
        });
        Schema::dropIfExists('boat_photos');
        Schema::table('boat_versions', function (Blueprint $table) {
            $table->string('version_external_photo')->nullable();
            $table->string('version_internal_photo')->nullable();
            $table->string('version_sea_photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('boat_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_cover');

            $table->bigInteger('boat_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('boat_photos', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });
        Schema::table('boat_versions', function (Blueprint $table) {
            $table->dropColumn('version_external_photo');
            $table->dropColumn('version_internal_photo');
            $table->dropColumn('version_sea_photo');
        });
    }
}
