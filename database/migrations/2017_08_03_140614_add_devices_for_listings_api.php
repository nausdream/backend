<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDevicesForListingsApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Devices

        $devices = [
            [
                'name' => 'mobile_search',
                'width' => 1000,
                'height' => 290,
            ],
            [
                'name' => 'tablet_search',
                'width' => 1000,
                'height' => 400,
            ],
            [
                'name' => 'desktop_search',
                'width' => 1000,
                'height' => 350,
            ],


        ];

        foreach ($devices as $device) {
            $deviceObject = new App\Models\Device($device);
            $deviceObject->save();
        }


        // Update old devices

        $mobile = \App\Models\Device::where('name', 'mobile')->first();

        $mobile->width = 1000;
        $mobile->height = 230;

        $tablet = \App\Models\Device::where('name', 'tablet')->first();

        $tablet->width = 1000;
        $tablet->height = 400;

        $desktop = \App\Models\Device::where('name', 'desktop')->first();

        $desktop->width = 1000;
        $desktop->height = 500;

        $mobile->save();
        $tablet->save();
        $desktop->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $devices = [];

        $devices[] = \App\Models\Device::where('name', 'mobile_search')->first();
        $devices[] = \App\Models\Device::where('name', 'tablet_search')->first();
        $devices[] = \App\Models\Device::where('name', 'desktop_search')->first();

        foreach ($devices as $device) {
            $device->forceDelete();
        }

        $mobile = \App\Models\Device::where('name', 'mobile')->first();

        $mobile->width = 360;
        $mobile->height = 640;

        $tablet = \App\Models\Device::where('name', 'tablet')->first();

        $tablet->width = 980;
        $tablet->height = 1280;

        $desktop = \App\Models\Device::where('name', 'desktop')->first();

        $desktop->width = 1366;
        $desktop->height = 768;

        $mobile->save();
        $tablet->save();
        $desktop->save();
    }
}
