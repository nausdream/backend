<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArrivingLatLngAndDepartureDateArrivalDateExperienceVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->double('destination_lat')->nullable();
            $table->double('destination_lng')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropColumn('destination_lat');
            $table->dropColumn('destination_lng');
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
        });
    }
}
