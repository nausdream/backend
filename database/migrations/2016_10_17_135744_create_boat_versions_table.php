<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('boat_versions');
        Schema::create('boat_versions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('production_site', Constant::PRODUCTION_SITE_LENGTH)->nullable();
            $table->string('model', Constant::BOAT_MODEL_LENGTH)->nullable();
            $table->string('name', Constant::BOAT_NAME_LENGTH)->nullable();
            $table->text('description')->nullable();
            $table->integer('construction_year')->nullable();
            $table->integer('restoration_year')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('material')->nullable();
            $table->smallInteger('length')->nullable();
            $table->boolean('has_insurance')->nullable();
            $table->tinyInteger('motors_quantity')->nullable();
            $table->tinyInteger('motor_type')->nullable();
            $table->string('docking_place', Constant::DOCKING_PLACE_LENGTH)->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->integer('fee')->nullable();
            $table->integer('seats')->nullable();
            $table->integer('berths')->nullable();
            $table->integer('cabins')->nullable();
            $table->integer('toilets')->nullable();
            $table->boolean('is_finished')->default(false);
            $table->tinyInteger('status')->nullable(); //0=pending, 1=accepted, 2=accepted_conditionally, 3=rejected

            $table->bigInteger('boat_id')->unsigned();
            $table->bigInteger('admin_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boat_versions');
    }
}
