<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceTranslationTitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_translation_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->string('text', Constant::EXPERIENCE_TITLE_LENGTH);
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('experience_version_id')->unsigned();
            $table->foreign('experience_version_id', 'exp_transl_title_vers')->references('id')->on('experience_versions');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('experience_translation_titles');
        Schema::enableForeignKeyConstraints();
    }
}
