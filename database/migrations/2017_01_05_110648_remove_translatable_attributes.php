<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTranslatableAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Boat

        Schema::table('boat_versions', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->text('rules')->nullable();
            $table->text('indications')->nullable();
        });

        Schema::table('boat_translation_descriptions', function (Blueprint $table) {
            $table->dropForeign('boat_translation_descriptions_boat_version_id_foreign');
        });

        Schema::dropIfExists('boat_translation_descriptions');

        Schema::table('boat_translation_rules', function (Blueprint $table) {
            $table->dropForeign('boat_transl_rule_vers');
        });

        Schema::dropIfExists('boat_translation_rules');

        Schema::table('boat_translation_indications', function (Blueprint $table) {
            $table->dropForeign('boat_translation_indications_boat_version_id_foreign');
        });

        Schema::dropIfExists('boat_translation_indications');

        // Experience

        Schema::table('experience_versions', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->text('rules')->nullable();
            $table->string('title', Constant::EXPERIENCE_TITLE_LENGTH)->nullable();
        });

        Schema::table('experience_translation_descriptions', function (Blueprint $table) {
            $table->dropForeign('exp_transl_desc_vers');
        });

        Schema::dropIfExists('experience_translation_descriptions');

        Schema::table('experience_translation_rules', function (Blueprint $table) {
            $table->dropForeign('exp_transl_rule_vers');
        });

        Schema::dropIfExists('experience_translation_rules');

        Schema::table('experience_translation_titles', function (Blueprint $table) {
            $table->dropForeign('exp_transl_title_vers');
        });

        Schema::dropIfExists('experience_translation_titles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Boat

        Schema::table('boat_versions', function ($table) {
            $table->dropColumn('description');
            $table->dropColumn('rules');
            $table->dropColumn('indications');
        });

        Schema::create('boat_translation_descriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('boat_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('boat_translation_descriptions', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });

        Schema::create('boat_translation_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('boat_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('boat_translation_rules', function (Blueprint $table) {
            $table->foreign('boat_version_id', 'boat_transl_rule_vers')->references('id')->on('boat_versions');
        });

        Schema::create('boat_translation_indications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('boat_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('boat_translation_indications', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });

        // Experience

        Schema::table('experience_versions', function ($table) {
            $table->dropColumn('description');
            $table->dropColumn('rules');
            $table->dropColumn('title');
        });

        Schema::create('experience_translation_descriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('experience_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('experience_translation_descriptions', function (Blueprint $table) {
            $table->foreign('experience_version_id', 'exp_transl_desc_vers')->references('id')->on('experience_versions');
        });

        Schema::create('experience_translation_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('experience_version_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('experience_translation_rules', function (Blueprint $table) {
            $table->foreign('experience_version_id', 'exp_transl_rule_vers')->references('id')->on('experience_versions');
        });

        Schema::create('experience_translation_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->string('text', Constant::EXPERIENCE_TITLE_LENGTH);
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('experience_version_id')->unsigned();
            $table->foreign('experience_version_id', 'exp_transl_title_vers')->references('id')->on('experience_versions');

            $table->timestamps();
            $table->softDeletes();
        });
    }
}
