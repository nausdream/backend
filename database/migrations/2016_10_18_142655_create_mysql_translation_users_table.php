<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMysqlTranslationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constant::FIRST_NAME_LENGTH);
            $table->string('last_name', Constant::LAST_NAME_LENGTH);
            $table->string('email', Constant::EMAIL_LENGTH)->unique();
            $table->string('phone', Constant::PHONE_LENGTH)->unique();
            $table->boolean('is_admin');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->dropIfExists('users');
    }
}
