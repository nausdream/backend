<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('feedback')->nullable();
            $table->date('experience_date');
            $table->date('request_date');
            $table->date('acceptance_date')->nullable();
            $table->tinyInteger('status'); //0=requested, 1=accepted, 2=rejected, 3=paid
            $table->integer('price');
            $table->integer('adults');
            $table->integer('kids');
            $table->integer('babies');
            $table->boolean('is_feedback_approved')->nullable();
            $table->integer('partner_fee')->nullable();

            $table->boolean('aperitif')->default(false);
            $table->boolean('fishing')->default(false);
            $table->boolean('sport')->default(false);
            $table->boolean('alcohol')->default(false);
            $table->boolean('analcoholic')->default(false);
            $table->boolean('fuel')->default(false);
            $table->boolean('sun_cream')->default(false);
            $table->boolean('chef')->default(false);
            $table->boolean('fruits')->default(false);
            $table->boolean('inflatable')->default(false);
            $table->boolean('hostess')->default(false);
            $table->boolean('sailor')->default(false);
            $table->boolean('tender')->default(false);
            $table->boolean('snack')->default(false);
            $table->boolean('towel')->default(false);

            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('experience_version_id')->unsigned();
            $table->bigInteger('conversation_id')->unsigned();
            $table->bigInteger('captain_feedback_id')->unsigned()->nullable();
            $table->bigInteger('user_feedback_id')->unsigned()->nullable();
            $table->bigInteger('boat_feedback_id')->unsigned()->nullable();
            $table->bigInteger('experience_feedback_id')->unsigned()->nullable();
            $table->bigInteger('contact_customer_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
