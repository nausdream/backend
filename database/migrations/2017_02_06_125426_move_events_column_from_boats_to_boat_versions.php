<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveEventsColumnFromBoatsToBoatVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boat_versions', function (Blueprint $table) {
            $table->boolean('events')->default(false);
        });
        Schema::table('boats', function ($table) {
            $table->dropColumn('events');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boat_versions', function ($table) {
            $table->dropColumn('events');
        });
        Schema::table('boats', function (Blueprint $table) {
            $table->boolean('events')->default(false);
        });
    }
}
