<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name', Constant::LAST_NAME_LENGTH)->nullable();
            $table->string('email', Constant::EMAIL_LENGTH)->nullable();
            $table->string('phone', Constant::PHONE_LENGTH)->nullable();
            $table->text('description');
            $table->string('place', Constant::EVENT_PLACE_LENGTH);

            $table->bigInteger('user_id')->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
