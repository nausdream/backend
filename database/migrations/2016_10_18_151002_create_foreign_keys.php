<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boats', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('boat_versions', function (Blueprint $table) {
            $table->foreign('boat_id')->references('id')->on('boats');
            $table->foreign('admin_id')->references('id')->on('administrators');
        });

        Schema::table('experiences', function (Blueprint $table) {
            $table->foreign('boat_id')->references('id')->on('boats');
        });

        Schema::table('experience_versions', function (Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
            $table->foreign('conversation_id')->references('id')->on('conversations');
            $table->foreign('captain_feedback_id')->references('id')->on('captain_feedbacks');
            $table->foreign('user_feedback_id')->references('id')->on('user_feedbacks');
            $table->foreign('boat_feedback_id')->references('id')->on('boat_feedbacks');
            $table->foreign('experience_feedback_id')->references('id')->on('experience_feedbacks');
            $table->foreign('contact_customer_id')->references('id')->on('contact_customers');
        });

        Schema::table('coupons', function (Blueprint $table) {
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('tokens', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('languages_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('language_id')->references('id')->on('languages');
        });

        Schema::table('captain_feedbacks', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('captain_id')->references('id')->on('users');
        });

        Schema::table('user_feedbacks', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('captain_id')->references('id')->on('users');
        });

        Schema::table('boat_feedbacks', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('boat_id')->references('id')->on('boats');
        });

        Schema::table('experience_feedbacks', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('conversations', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('captain_id')->references('id')->on('users');
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('messages', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('conversation_id')->references('id')->on('conversations');
        });

        Schema::table('boat_photos', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });

        Schema::table('experience_photos', function (Blueprint $table) {
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
        });

        Schema::table('boat_availabilities', function (Blueprint $table) {
            $table->foreign('boat_id')->references('id')->on('boats');
        });

        Schema::table('experience_availabilities', function (Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('email_verifications', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('additional_services', function (Blueprint $table) {
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
        });

        Schema::table('additional_service_translations', function (Blueprint $table) {
            $table->foreign('additional_service_id')->references('id')->on('additional_services');
        });

        Schema::table('boat_translation_descriptions', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });

        Schema::table('boat_translation_indications', function (Blueprint $table) {
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');
        });

        Schema::table('experience_translation_descriptions', function (Blueprint $table) {
            $table->foreign('experience_version_id', 'exp_transl_desc_vers')->references('id')->on('experience_versions');
        });

        Schema::table('experience_translation_rules', function (Blueprint $table) {
            $table->foreign('experience_version_id', 'exp_transl_rule_vers')->references('id')->on('experience_versions');
        });

        Schema::table('steps', function (Blueprint $table) {
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('experiences');
        });

        Schema::table('boat_translation_rules', function (Blueprint $table) {
            $table->foreign('boat_version_id', 'boat_transl_rule_vers')->references('id')->on('boat_versions');
        });

        Schema::table('bookings_additional_services', function (Blueprint $table) {
            $table->foreign('booking_id', 'book_add_serv_book')->references('id')->on('bookings');
            $table->foreign('additional_service_id', 'book_add_serv')->references('id')->on('additional_services');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('bookings');
        });

        Schema::table('administrators_authorizations', function (Blueprint $table) {
            $table->foreign('administrator_id')->references('id')->on('administrators');
            $table->foreign('authorization_id')->references('id')->on('authorizations');
        });

        Schema::table('admin_tokens', function (Blueprint $table) {
            $table->foreign('administrator_id')->references('id')->on('administrators');
        });

        Schema::table('calls', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('experience_version_id')->references('id')->on('experience_versions');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('periods', function (Blueprint $table) {
            $table->foreign('experience_version_id', 'exp_ver_id')->references('id')->on('experience_versions');
        });

        Schema::table('prices', function (Blueprint $table) {
            $table->foreign('period_id')->references('id')->on('periods');
        });

        /*
         * Setup foreign key for translation database
         */

        Schema::connection('mysql_translation')->table('tokens', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::connection('mysql_translation')->table('sentences', function (Blueprint $table) {
            $table->foreign('page_id')->references('id')->on('pages');
        });

        Schema::connection('mysql_translation')->table('sentence_translations', function (Blueprint $table) {
            $table->foreign('sentence_id')->references('id')->on('sentences');
            $table->foreign('language_id')->references('id')->on('languages');
        });

        Schema::connection('mysql_translation')->table('languages_users', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('boats', function (Blueprint $table) {
            $table->dropForeign('boats_user_id_foreign');
        });

        Schema::table('boat_versions', function (Blueprint $table) {
            $table->dropForeign('boat_versions_boat_id_foreign');
            $table->dropForeign('boat_versions_admin_id_foreign');
        });

        Schema::table('experiences', function (Blueprint $table) {
            $table->dropForeign('experiences_boat_id_foreign');
        });

        Schema::table('experience_versions', function (Blueprint $table) {
            $table->dropForeign('experience_versions_experience_id_foreign');
        });

        Schema::table('bookings', function (Blueprint $table) {
            $table->dropForeign('bookings_user_id_foreign');
            $table->dropForeign('bookings_experience_version_id_foreign');
            $table->dropForeign('bookings_conversation_id_foreign');
            $table->dropForeign('bookings_captain_feedback_id_foreign');
            $table->dropForeign('bookings_user_feedback_id_foreign');
            $table->dropForeign('bookings_boat_feedback_id_foreign');
            $table->dropForeign('bookings_experience_feedback_id_foreign');
            $table->dropForeign('bookings_contact_customer_id_foreign');
        });

        Schema::table('coupons', function (Blueprint $table) {
            $table->dropForeign('coupons_area_id_foreign');
            $table->dropForeign('coupons_user_id_foreign');
            $table->dropForeign('coupons_experience_id_foreign');
        });

        Schema::table('tokens', function (Blueprint $table) {
            $table->dropForeign('tokens_user_id_foreign');
        });

        Schema::table('languages_users', function (Blueprint $table) {
            $table->dropForeign('languages_users_user_id_foreign');
            $table->dropForeign('languages_users_language_id_foreign');
        });

        Schema::table('captain_feedbacks', function (Blueprint $table) {
            $table->dropForeign('captain_feedbacks_user_id_foreign');
            $table->dropForeign('captain_feedbacks_captain_id_foreign');
        });

        Schema::table('user_feedbacks', function (Blueprint $table) {
            $table->dropForeign('user_feedbacks_user_id_foreign');
            $table->dropForeign('user_feedbacks_captain_id_foreign');
        });

        Schema::table('boat_feedbacks', function (Blueprint $table) {
            $table->dropForeign('boat_feedbacks_user_id_foreign');
            $table->dropForeign('boat_feedbacks_boat_id_foreign');
        });

        Schema::table('experience_feedbacks', function (Blueprint $table) {
            $table->dropForeign('experience_feedbacks_user_id_foreign');
            $table->dropForeign('experience_feedbacks_experience_id_foreign');
        });

        Schema::table('conversations', function (Blueprint $table) {
            $table->dropForeign('conversations_user_id_foreign');
            $table->dropForeign('conversations_captain_id_foreign');
            $table->dropForeign('conversations_experience_id_foreign');
        });

        Schema::table('messages', function (Blueprint $table) {
            $table->dropForeign('messages_user_id_foreign');
            $table->dropForeign('messages_conversation_id_foreign');
        });

        Schema::table('boat_photos', function (Blueprint $table) {
            $table->dropForeign('boat_photos_boat_version_id_foreign');
        });

        Schema::table('experience_photos', function (Blueprint $table) {
            $table->dropForeign('experience_photos_experience_version_id_foreign');
        });

        Schema::table('boat_availabilities', function (Blueprint $table) {
            $table->dropForeign('boat_availabilities_boat_id_foreign');
        });

        Schema::table('experience_availabilities', function (Blueprint $table) {
            $table->dropForeign('experience_availabilities_experience_id_foreign');
        });

        Schema::table('email_verifications', function (Blueprint $table) {
            $table->dropForeign('email_verifications_user_id_foreign');
        });

        Schema::table('additional_services', function (Blueprint $table) {
            $table->dropForeign('additional_services_experience_version_id_foreign');
        });

        Schema::table('additional_service_translations', function (Blueprint $table) {
            $table->dropForeign('additional_service_translations_additional_service_id_foreign');
        });

        Schema::table('boat_translation_descriptions', function (Blueprint $table) {
            $table->dropForeign('boat_translation_descriptions_boat_version_id_foreign');
        });

        Schema::table('boat_translation_indications', function (Blueprint $table) {
            $table->dropForeign('boat_translation_indications_boat_version_id_foreign');
        });

        Schema::table('experience_translation_descriptions', function (Blueprint $table) {
            $table->dropForeign('exp_transl_desc_vers');
        });

        Schema::table('experience_translation_rules', function (Blueprint $table) {
            $table->dropForeign('exp_transl_rule_vers');
        });

        Schema::table('steps', function (Blueprint $table) {
            $table->dropForeign('steps_experience_version_id_foreign');
        });

        Schema::table('seo', function (Blueprint $table) {
            $table->dropForeign('seo_experience_id_foreign');
        });

        Schema::table('boat_translation_rules', function (Blueprint $table) {
            $table->dropForeign('boat_transl_rule_vers');
        });

        Schema::table('bookings_additional_services', function (Blueprint $table) {
            $table->dropForeign('book_add_serv_book');
            $table->dropForeign('book_add_serv');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->dropForeign('customers_booking_id_foreign');
        });

        Schema::table('administrators_authorizations', function (Blueprint $table) {
            $table->dropForeign('administrators_authorizations_administrator_id_foreign');
            $table->dropForeign('administrators_authorizations_authorization_id_foreign');
        });

        Schema::table('admin_tokens', function (Blueprint $table) {
            $table->dropForeign('admin_tokens_administrator_id_foreign');
        });

        Schema::table('calls', function (Blueprint $table) {
            $table->dropForeign('calls_user_id_foreign');
            $table->dropForeign('calls_experience_version_id_foreign');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign('events_user_id_foreign');
        });

        Schema::table('periods', function (Blueprint $table) {
            $table->dropForeign('exp_ver_id');
        });

        Schema::table('prices', function (Blueprint $table) {
            $table->dropForeign('prices_period_id_foreign');
        });

        Schema::connection('mysql_translation')->table('tokens', function (Blueprint $table) {
            $table->dropForeign('tokens_user_id_foreign');
        });

        Schema::connection('mysql_translation')->table('sentences', function (Blueprint $table) {
            $table->dropForeign('sentences_page_id_foreign');
        });

        Schema::connection('mysql_translation')->table('sentence_translations', function (Blueprint $table) {
            $table->dropForeign('sentence_translations_sentence_id_foreign');
            $table->dropForeign('sentence_translations_language_id_foreign');
        });

        Schema::connection('mysql_translation')->table('languages_users', function (Blueprint $table) {
            $table->dropForeign('languages_users_user_id_foreign');
            $table->dropForeign('languages_users_language_id_foreign');
        });

        Schema::enableForeignKeyConstraints();

    }
}
