<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeExperienceVersionFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Doctrine, which laravel uses to modify tables, has a bug with tinyInteger and mediumInteger
        // by default, boolean is a TINYINT(1), while tinyInteger is TINYINT(4). The argument is there
        // just for 'display' purposes, as explained here: http://dev.mysql.com/doc/refman/5.7/en/numeric-type-attributes.html
        Schema::table('experience_versions', function (Blueprint $table) {
            $table->boolean('type')->nullable(true)->change();
            $table->boolean('duration')->nullable(true)->change();
            $table->boolean('monday')->nullable()->change();
            $table->boolean('tuesday')->nullable()->change();
            $table->boolean('wednesday')->nullable()->change();
            $table->boolean('thursday')->nullable()->change();
            $table->boolean('friday')->nullable()->change();
            $table->boolean('saturday')->nullable()->change();
            $table->boolean('sunday')->nullable()->change();
            $table->boolean('is_fixed_price')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (env('APP_ENV') == 'development' || env('APP_ENV') == 'testing') {
            Schema::disableForeignKeyConstraints();
            DB::table('experience_versions')->truncate();
            Schema::enableForeignKeyConstraints();
        }

        Schema::table('experience_versions', function (Blueprint $table) {
            $table->boolean('type')->nullable(false)->change();
            $table->boolean('duration')->nullable(false)->change();
            $table->boolean('monday')->nullable(false)->change();
            $table->boolean('tuesday')->nullable(false)->change();
            $table->boolean('wednesday')->nullable(false)->change();
            $table->boolean('thursday')->nullable(false)->change();
            $table->boolean('friday')->nullable(false)->change();
            $table->boolean('saturday')->nullable(false)->change();
            $table->boolean('sunday')->nullable(false)->change();
            $table->boolean('is_fixed_price')->nullable(false)->change();
        });
    }
}
