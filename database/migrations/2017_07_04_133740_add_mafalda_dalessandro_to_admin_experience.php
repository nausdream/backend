<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMafaldaDalessandroToAdminExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production') {
            $mafaldaDalessandro = factory(\App\Models\Administrator::class)->states('super_admin')->create(
                [
                    'first_name' => 'Mafalda',
                    'last_name' => 'D\'Alessandro',
                    'email' => 'mafalda@nausdream.com',
                    'phone' => '+393334784689',
                    'currency' => 'EUR',
                    'language' => 'it',
                ]
            );

            // Fetch authorizations
            $authorizationExperiences = \App\Models\Authorization::all()->where('name', 'experiences')->first();
            $mafaldaDalessandro->authorizations()->attach($authorizationExperiences->id);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') == 'production') {
            Schema::disableForeignKeyConstraints();
            $mafaldaDalessandro = \App\Models\Administrator::all()->where('email', 'mafalda@nausdream.com')->last();
            $mafaldaDalessandro->authorizations()->detach();
            $mafaldaDalessandro->forceDelete();
            Schema::enableForeignKeyConstraints();
        }
    }
}
