<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTranslatorsToDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $translators = [];

        $giuseppeBasciu = factory(\App\TranslationModels\User::class)->states('admin')->create(
            [
                'first_name' => 'Giuseppe',
                'last_name' => 'Basciu',
                'email' => 'giuseppe@nausdream.com',
                'phone' => '+393895999330',
            ]
        );
        $ousmaneDieng = factory(\App\TranslationModels\User::class)->states('admin')->create(
            [
                'first_name' => 'Ousmane',
                'last_name' => 'Dieng',
                'email' => 'ousmane@nausdream.com',
                'phone' => '+393403224160',
            ]
        );
        $mafaldaDalessandro = factory(\App\TranslationModels\User::class)->create(
            [
                'first_name' => 'Mafalda',
                'last_name' => 'D\'Alessandro',
                'email' => 'mafalda@nausdream.com',
                'phone' => '+393334784689',
            ]
        );
        array_push($translators, $giuseppeBasciu, $ousmaneDieng, $mafaldaDalessandro);

        $language = \App\TranslationModels\Language::all()->where('language', 'it')->first();
        $mafaldaDalessandro->languages()->attach($language->id);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();
        $giuseppeBasciu = \App\TranslationModels\User::all()->where('email', 'giuseppe@nausdream.com')->last();
        $giuseppeBasciu->languages()->detach();
        $giuseppeBasciu->forceDelete();
        $ousmaneDieng = \App\TranslationModels\User::all()->where('email', 'ousmane@nausdream.com')->last();
        $ousmaneDieng->forceDelete();
        $mafaldaDalessandro = \App\TranslationModels\User::all()->where('email', 'mafalda@nausdream.com')->last();
        $mafaldaDalessandro->languages()->detach();
        $mafaldaDalessandro->forceDelete();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }
}
