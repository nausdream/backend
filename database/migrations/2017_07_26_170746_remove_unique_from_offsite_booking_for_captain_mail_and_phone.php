<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueFromOffsiteBookingForCaptainMailAndPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->dropUnique('offsite_bookings_email_captain_unique');
            $table->dropUnique('offsite_bookings_phone_captain_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offsite_bookings', function (Blueprint $table) {
            $table->string('email_captain', Constant::EMAIL_LENGTH)->unique()->change();
            $table->string('phone_captain', Constant::PHONE_LENGTH)->unique()->change();
        });
    }
}
