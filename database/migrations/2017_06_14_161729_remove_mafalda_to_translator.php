<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMafaldaToTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_translation')->disableForeignKeyConstraints();
        $mafalda = \App\TranslationModels\User::all()->where('email', 'mafalda@nausdream.com')->last();
        $mafalda->delete();
        Schema::connection('mysql_translation')->enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $mafalda = \App\TranslationModels\User::onlyTrashed()->where('email', 'mafalda@nausdream.com')->get()->last();
        $mafalda->restore();
    }
}
