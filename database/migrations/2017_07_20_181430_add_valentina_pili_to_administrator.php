<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValentinaPiliToAdministrator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_ENV') == 'production') {
            $admin = factory(\App\Models\Administrator::class)->states('super_admin')->create(
                [
                    'first_name' => 'Valentina',
                    'last_name' => 'Pili',
                    'email' => 'valentina@nausdream.com',
                    'phone' => '+393481211025',
                    'currency' => 'EUR',
                    'language' => 'it',
                ]
            );

            // Fetch authorizations
            $authorizations = \App\Models\Authorization::all();
            $authIds = [];
            foreach ($authorizations as $authorization) {
                $authIds[] = $authorization->id;
            }
            $admin->authorizations()->attach($authIds);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (env('APP_ENV') == 'production') {
            Schema::disableForeignKeyConstraints();
            $admin = \App\Models\Administrator::all()->where('email', 'valentina@nausdream.com')->last();
            $admin->authorizations()->detach();
            $admin->forceDelete();
            Schema::enableForeignKeyConstraints();
        }
    }
}
