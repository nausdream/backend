<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalServiceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_service_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('text');
            $table->bigInteger('translator_id')->nullable();

            $table->bigInteger('additional_service_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_service_translations');
    }
}
