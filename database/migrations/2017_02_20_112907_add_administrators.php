<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdministrators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $administrators = [];

        $mariaAntoniettaMelis = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Maria Antonietta',
                'last_name' => 'Melis',
                'email' => 'maria.antonietta@nausdream.com',
                'phone' => '+393270457440',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        $stefanoCorso = factory(\App\Models\Administrator::class)->states('super_admin')->create(
            [
                'first_name' => 'Stefano',
                'last_name' => 'Corsini',
                'email' => 'stefano@nausdream.com',
                'phone' => '+393409303152',
                'currency' => 'EUR',
                'language' => 'it',
            ]
        );
        array_push($administrators, $mariaAntoniettaMelis, $stefanoCorso);

        // Fetch authorizations
        $authorizations = \App\Models\Authorization::all();
        $authIds = [];
        foreach ($authorizations as $authorization) {
            $authIds[] = $authorization->id;
        }
        foreach ($administrators as $administrator) {
            $administrator->authorizations()->attach($authIds);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::disableForeignKeyConstraints();
        $mariaAntoniettaMelis = \App\Models\Administrator::all()->where('email', 'maria.antonietta@nausdream.com')->last();
        $mariaAntoniettaMelis->authorizations()->detach();
        $mariaAntoniettaMelis->forceDelete();
        $stefanoCorso = \App\Models\Administrator::all()->where('email', 'stefano@nausdream.com')->last();
        $stefanoCorso->authorizations()->detach();
        $stefanoCorso->forceDelete();
        Schema::enableForeignKeyConstraints();

    }
}
