<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRubberBoatToBoatTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $boatType = new \App\Models\BoatType(['name' => 'rubberboat']);
        $boatType->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $boatType = \App\Models\BoatType::all()->where('name', 'rubberboat')->first();
        $boatType->forceDelete();
    }
}
