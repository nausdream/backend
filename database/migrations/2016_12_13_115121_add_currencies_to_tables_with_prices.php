<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrenciesToTablesWithPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->string('currency', \App\Constant::CURRENCY_LENGTH);
        });

        Schema::table('additional_services', function (Blueprint $table) {
            $table->string('currency', \App\Constant::CURRENCY_LENGTH);
        });

        Schema::table('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->string('currency', \App\Constant::CURRENCY_LENGTH);
        });

        Schema::table('periods', function (Blueprint $table) {
            $table->string('currency', \App\Constant::CURRENCY_LENGTH);
        });

        Schema::table('coupons', function (Blueprint $table) {
            $table->string('currency', \App\Constant::CURRENCY_LENGTH)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prices', function (Blueprint $table) {
            $table->dropColumn('currency');
        });

        Schema::table('additional_services', function (Blueprint $table) {
            $table->dropColumn('currency');
        });

        Schema::table('experience_versions_fixed_additional_services', function (Blueprint $table) {
            $table->dropColumn('currency');
        });

        Schema::table('periods', function (Blueprint $table) {
            $table->dropColumn('currency');
        });

        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('currency');
        });
    }
}
