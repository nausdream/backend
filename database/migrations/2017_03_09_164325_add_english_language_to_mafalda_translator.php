<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnglishLanguageToMafaldaTranslator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ousmaneDieng = \App\TranslationModels\User::all()->where('email', 'ousmane@nausdream.com')->first();
        $ousmaneDieng->phone = '+17876771455';
        $ousmaneDieng->save();
        $mafaldaDalessandro = \App\TranslationModels\User::all()->where('email', 'mafalda@nausdream.com')->first();
        $language = \App\TranslationModels\Language::all()->where('language', 'en')->first();
        $mafaldaDalessandro->languages()->attach($language->id);
        $mafaldaDalessandro->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $mafaldaDalessandro = \App\TranslationModels\User::all()->where('email', 'mafalda@nausdream.com')->last();
        $language = \App\TranslationModels\Language::all()->where('language', 'en')->first();
        $mafaldaDalessandro->languages()->detach($language->id);
        $mafaldaDalessandro->save();
    }
}
