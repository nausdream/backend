<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixSpokenLanguageName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $languageObject = \App\Models\Language::all()->where('language', '    or')->first();
        $languageObject->language = 'or';
        $languageObject->save();

        $languageObject = \App\Models\Language::all()->where('language', 'sr_Latin_BA')->first();
        $languageObject->language = 'sr_Latn_BA';
        $languageObject->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $languageObject = \App\Models\Language::all()->where('language', 'or')->first();
        $languageObject->language = '    or';
        $languageObject->save();

        $languageObject = \App\Models\Language::all()->where('language', 'sr_Latn_BA')->first();
        $languageObject->language = 'sr_Latin_BA';
        $languageObject->save();
    }
}
