<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', Constant::COUNTRY_NAME_LENGTH);
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::table('users', function (Blueprint $table) {
            $table->foreign('enterprise_country')->references('id')->on('countries');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_enterprise_country_foreign');
        });
        Schema::dropIfExists('countries');
    }
}
