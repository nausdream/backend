<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveFeeColumnFromBoatVersionToBoat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boat_versions', function ($table) {
            $table->dropColumn('fee');
        });
        Schema::table('boats', function (Blueprint $table) {
            $table->integer('fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boat_versions', function (Blueprint $table) {
            $table->integer('fee')->nullable();
        });
        Schema::table('boats', function ($table) {
            $table->dropColumn('fee');
        });
    }
}
