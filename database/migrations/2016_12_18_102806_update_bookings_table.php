<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('aperitif');
            $table->dropColumn('fishing');
            $table->dropColumn('sport');
            $table->dropColumn('alcohol');
            $table->dropColumn('analcoholic');
            $table->dropColumn('fuel');
            $table->dropColumn('sun_cream');
            $table->dropColumn('chef');
            $table->dropColumn('fruits');
            $table->dropColumn('inflatable');
            $table->dropColumn('hostess');
            $table->dropColumn('sailor');
            $table->dropColumn('tender');
            $table->dropColumn('snack');
            $table->dropColumn('towel');

            $table->bigInteger('conversation_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('aperitif')->default(false);
            $table->boolean('fishing')->default(false);
            $table->boolean('sport')->default(false);
            $table->boolean('alcohol')->default(false);
            $table->boolean('analcoholic')->default(false);
            $table->boolean('fuel')->default(false);
            $table->boolean('sun_cream')->default(false);
            $table->boolean('chef')->default(false);
            $table->boolean('fruits')->default(false);
            $table->boolean('inflatable')->default(false);
            $table->boolean('hostess')->default(false);
            $table->boolean('sailor')->default(false);
            $table->boolean('tender')->default(false);
            $table->boolean('snack')->default(false);
            $table->boolean('towel')->default(false);

            $table->bigInteger('conversation_id')->unsigned()->change();
        });
    }
}
