<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Http\Controllers\v1\SeosController;
use App\Models\AdditionalService;
use App\Models\ExperienceAvailability;
use App\Models\Period;
use App\Models\Price;
use JD\Cloudder\Facades\Cloudder;
use App\Services\PhotoHelper;

class MigrateOldSiteAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        if (env('APP_ENV') == 'staging' || env('APP_ENV') == 'production') {
            DB::beginTransaction();

            try {
                $conn = 'mysql_old_site';

                // Get possible spoken languages ids
                $languages = [];
                $languageNames = ['it', 'en', 'fr', 'es', 'de', 'ru', 'zh', 'hi', 'ar', 'pt'];
                foreach ($languageNames as $languageName) {
                    $languages[$languageName] = \App\Models\Language::all()->where('language', $languageName)->first();
                }

                // Set conversion array for accessories
                $accessoriesName = [
                    'cucina' => 'kitchen',
                    'radio' => 'radio',
                    'ecoscandaglio' => 'echo-sounder',
                    'attrezzatura_sportiva' => 'sports-gear',
                    'tendalino' => 'awning',
                    'doccia_esterna' => 'outdoor-shower',
                    'microonde' => 'microwave',
                    'tender' => 'tender',
                    'tv' => 'tv',
                    'frigo' => 'fridge',
                    'passerella' => 'gangway',
                    'prese' => 'wall-socket',
                    'utensili_cucina' => 'kitchen-tools',
                    'estintore' => 'fire-extinguisher',
                    'epirb' => 'epirb',
                    'vhf' => 'vhf',
                    'riscaldamento' => 'heating',
                    'dotazioni_sicurezza' => 'security-equipment',
                    'freezer' => 'freezer',
                    'generator' => 'power-unit',
                    'fire_saving' => 'fire-prevention-system'
                ];

                // Set conversion array for experience type name
                $experienceTypesName = [
                    'escursioni' => 'trips-and-tours',
                    'aperitivi' => 'aperitif',
                    'cene' => 'dinners',
                    'pesca' => 'fishing',
                    'sport' => 'other',
                    'notti' => 'nights',
                ];

                // Set conversion array for experience type
                $experienceTypes = [
                    1 => 'trips-and-tours',
                    2 => 'aperitif',
                    3 => 'fishing',
                    4 => 'dinners',
                    5 => 'nights',
                    6 => 'other',
                    7 => 'romantic'
                ];

                // Helper model
                $breakfast = \App\Models\FixedAdditionalService::all()->where('name', 'breakfast')->first();

                // Fetch old users
                $users = DB::connection($conn)->select('select * from UTENTI where attivato = 1 AND bloccato = 0 AND eliminato = 0');
                foreach ($users as $user) {

                    // Check if captain
                    $boats = DB::connection($conn)->select('select * from BARCHE where eliminata = 0 AND bloccata = 0 AND id_utente = ?', [$user->id]);
                    if (!empty($boats)) {
                        $is_captain = true;
                        if (!isset($user->azienda)) {
                            $captain_type = 0;
                        } else {
                            $captain_type = 1;
                        }
                        if (isset($user->citta)) {
                            $docking_place = $user->citta;
                        } else {
                            $docking_place = null;
                        }
                    } else {
                        $is_captain = false;
                        $captain_type = null;
                        $docking_place = null;
                    }

                    // Set new user fields
                    $langOld = substr($user->lang, 0, 2);
                    $newUser = new \App\Models\User([
                        'first_name' => $user->nome,
                        'last_name' => $user->cognome,
                        'birth_date' => $user->data_nascita == '0000-00-00' ? null : $user->data_nascita,
                        'captain_type' => $captain_type,
                        'sex' => $user->sesso,
                        'currency' => isset($user->cur) ? $user->cur : 'EUR',
                        'language' => (isset($langOld) && $langOld != '') ? $langOld : \Lang::getFallback(),
                        'description' => $user->descrizione,
                        'enterprise_name' => $user->nome_societa,
                        'enterprise_address' => $user->sede_legale,
                        'vat_number' => $user->partita_iva,
                        'card_price' => $user->card_price,
                        'docking_place' => $docking_place,
                        'skippers_have_license' => isset($user->patente),
                        'newsletter' => false,
                        'has_card' => $user->association_card
                    ]);
                    $newUser->is_captain = $is_captain;
                    $newUser->is_phone_activated = false;
                    $newUser->email = $user->email;
                    $newUser->phone = $user->telefono;
                    $newUser->default_fee = env('DEFAULT_FEE');
                    $newUser->is_mail_activated = $user->mail_active;
                    $newUser->is_partner = false;
                    $newUser->is_suspended = false;
                    $newUser->is_set_password = false;
                    try {
                        $newUser->save();
                    } catch (\Exception $e) {
                        $newUser->birth_date = null;
                        $newUser->save();
                    }

                    // Spoken languages
                    if ($user->italiano) {
                        $newUser->languages()->attach($languages['it']->id);
                    }
                    if ($user->inglese) {
                        $newUser->languages()->attach($languages['en']->id);
                    }
                    if ($user->francese) {
                        $newUser->languages()->attach($languages['fr']->id);
                    }
                    if ($user->spagnolo) {
                        $newUser->languages()->attach($languages['es']->id);
                    }
                    if ($user->tedesco) {
                        $newUser->languages()->attach($languages['de']->id);
                    }
                    if ($user->russo) {
                        $newUser->languages()->attach($languages['ru']->id);
                    }
                    if ($user->cinese) {
                        $newUser->languages()->attach($languages['zh']->id);
                    }
                    if ($user->hindi) {
                        $newUser->languages()->attach($languages['hi']->id);
                    }
                    if ($user->arabo) {
                        $newUser->languages()->attach($languages['ar']->id);
                    }
                    if ($user->portoghese) {
                        $newUser->languages()->attach($languages['pt']->id);
                    }

                    // Set profile pitcure
                    if (isset($user->foto_version)) {
                        $link = 'https://res.cloudinary.com/naustrip/image/upload/v' . $user->foto_version . '/user_images/' . $user->id . '/foto_profilo.jpg';
                        $url = PhotoHelper::getProfilePhotoPublicId($newUser->id);

                        Cloudder::upload(
                            $link,
                            $url,
                            ["format" => "jpg"]
                        );
                        $arrayResult = Cloudder::getResult();

                        // Update database value

                        $newUser->photo_version = $arrayResult['version'];
                        $newUser->save();
                    }


                    // Boats

                    foreach ($boats as $boat) {
                        // Set new boat fields
                        $newBoat = new \App\Models\Boat([
                            'is_luxury' => false,
                            'fee' => env('DEFAULT_FEE'),
                            'user_id' => $newUser->id
                        ]);
                        $newBoat->save();

                        // Set new boatVersion fields
                        switch ($boat->tipo) {
                            case 1:
                                $boat_type = 'sailboat';
                                break;
                            case 2:
                                $boat_type = 'motorboat';
                                break;
                            case 3:
                                $boat_type = 'motorboat';
                                break;
                            case 4:
                                $boat_type = 'catamaran';
                                break;
                            case 5:
                                $boat_type = 'motorboat';
                                break;
                            default:
                                $boat_type = 'sailboat';
                        }
                        switch ($boat->tipo_motore) {
                            case 1:
                                $motor_type = 'inboard';
                                break;
                            case 2:
                                $motor_type = 'outboard';
                                break;
                            case 3:
                                $motor_type = 'sterndrive';
                                break;
                            default:
                                $motor_type = null;
                        }
                        $newBoatVersion = new \App\Models\BoatVersion([
                            'production_site' => $boat->marca,
                            'model' => $boat->modello,
                            'name' => $boat->nome,
                            'construction_year' => $boat->anno,
                            'restoration_year' => null,
                            'type' => $boat_type,
                            'material' => null,
                            'length' => $boat->lunghezza,
                            'has_insurance' => isset($boat->assicurazione) && $boat->assicurazione != '0000-00-00' ? true : false,
                            'motors_quantity' => $boat->numero_motori,
                            'motor_type' => $motor_type,
                            'docking_place' => $boat->citta,
                            'lat' => $boat->lat,
                            'lng' => $boat->lng,
                            'seats' => $boat->n_posti,
                            'berths' => $boat->posti_letto,
                            'cabins' => $boat->cabine_ospiti,
                            'toilets' => $boat->bagni,
                            'is_finished' => true,
                            'description' => $boat->descrizione,
                            'rules' => $boat->regole,
                            'indications' => $boat->indicazioni,
                            'events' => $boat->eventi == 1
                        ]);
                        $newBoatVersion->boat_id = $newBoat->id;
                        $newBoatVersion->status = $boat->attivata ? \App\Constant::STATUS_ACCEPTED : \App\Constant::STATUS_PENDING;
                        $newBoatVersion->save();

                        // Boat accessories
                        $accessory_ids = [];
                        foreach ($accessoriesName as $key => $accessoryName) {
                            if ($boat->{$key}) {
                                $accessory_ids[] = \App\Models\Accessory::all()->where('name', $accessoryName)->first()->id;
                            }
                        }
                        $newBoatVersion->accessories()->attach($accessory_ids);
                        $newBoatVersion->save();

                        // Boat experience types
                        $experienceType_ids = [];
                        foreach ($experienceTypesName as $key => $experienceTypeName) {
                            if ($boat->{$key}) {
                                $experienceType_ids[] = \App\Models\ExperienceType::all()->where('name', $experienceTypeName)->first()->id;
                            }
                        }
                        $newBoat->experienceTypes()->attach($experienceType_ids);
                        $newBoat->save();

                        // Boat photos

                        $boatPhotos = DB::connection($conn)->select('select * from FOTO_BARCHE where eliminata = 0 and id_barca = ? order by foto_copertina desc', [$boat->id]);
                        $i = 0;
                        if (!empty($boatPhotos) && isset($boatPhotos[$i])) {
                            do {
                                $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                $i++;
                            } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                            if (@is_array(getimagesize($link))) {
                                $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'external');
                                Cloudder::upload(
                                    $link,
                                    $url,
                                    ["format" => "jpg"]
                                );
                                $arrayResult = Cloudder::getResult();
                                $newBoatVersion->version_external_photo = $arrayResult['version'];
                                if (isset($boatPhotos[$i])) {
                                    do {
                                        $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                        $i++;
                                    } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                                    if (@is_array(getimagesize($link))) {
                                        $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'internal');
                                        Cloudder::upload(
                                            $link,
                                            $url,
                                            ["format" => "jpg"]
                                        );
                                        $arrayResult = Cloudder::getResult();
                                        $newBoatVersion->version_internal_photo = $arrayResult['version'];
                                        if (isset($boatPhotos[$i])) {
                                            do {
                                                $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                                $i++;
                                            } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                                            if (@is_array(getimagesize($link))) {
                                                $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'sea');
                                                Cloudder::upload(
                                                    $link,
                                                    $url,
                                                    ["format" => "jpg"]
                                                );
                                                $arrayResult = Cloudder::getResult();
                                                $newBoatVersion->version_sea_photo = $arrayResult['version'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $newBoatVersion->save();

                        $experiences = DB::connection($conn)->select('select * from TRIP where eliminato = 0 AND bloccato = 0 AND id_barca = ?', [$boat->id]);
                        foreach ($experiences as $experience) {

                            $experiencePhotos = DB::connection($conn)->select('select * from FOTO_TRIP where eliminata = 0 AND id_trip = ? ORDER BY foto_copertina DESC, id ASC', [$experience->id]);

                            if (!empty($experiencePhotos)) {

                                // Set new experience fields
                                $newExperience = new \App\Models\Experience();
                                $newExperience->boat_id = $newBoat->id;
                                $newExperience->save();

                                if (isset($experience->citta_rientro) && $experience->citta_rientro != '') {
                                    $citta_rientro = $experience->citta_rientro;
                                } else {
                                    $citta_rientro = $experience->citta;
                                }

                                if (isset($experience->lat_rientro) && $experience->lat_rientro != '') {
                                    $lat_rientro = $experience->lat_rientro;
                                    $lng_rientro = $experience->lng_rientro;
                                } else {
                                    $lat_rientro = $experience->lat;
                                    $lng_rientro = $experience->lng;
                                }

                                $experienceVersion = new \App\Models\ExperienceVersion([
                                    'type' => $experienceTypes[$experience->tipo_uscita],
                                    'departure_port' => $experience->citta,
                                    'departure_time' => $experience->ora_partenza,
                                    'arrival_port' => $citta_rientro,
                                    'arrival_time' => $experience->ora_fine,
                                    'monday' => $experience->lun,
                                    'tuesday' => $experience->mar,
                                    'wednesday' => $experience->mer,
                                    'thursday' => $experience->gio,
                                    'friday' => $experience->ven,
                                    'saturday' => $experience->sab,
                                    'sunday' => $experience->dom,
                                    'is_fixed_price' => true,
                                    'seats' => $experience->posti,
                                    'kids' => $experience->posti,
                                    'babies' => $experience->posti,
                                    'departure_lat' => $experience->lat,
                                    'departure_lng' => $experience->lng,
                                    'description' => mb_substr($experience->descrizione, 0, \App\Constant::EXPERIENCE_DESCRIPTION_LENGTH),
                                    'title' => mb_substr($experience->nome, 0, \App\Constant::EXPERIENCE_TITLE_LENGTH),
                                    'destination_lat' => $lat_rientro,
                                    'destination_lng' => $lng_rientro,
                                    'days' => 1,
                                    'road_stead' => false,
                                    'down_payment' => isset($user->caparra) && $user->caparra,
                                    'cancellation_max_days' => env('DEFAULT_CANCELLATION'),
                                    'deposit' => 0,
                                    'currency' => $experience->cur,
                                ]);
                                $experienceVersion->experience_id = $newExperience->id;
                                $experienceVersion->is_finished = true;
                                $experienceVersion->status = \App\Constant::STATUS_PENDING;
                                $experienceVersion->is_searchable = $experience->searchable;

                                $experienceVersion->save();

                                // Experience seo

                                $seoController = new SeosController();
                                $seoController->createOrUpdateSeo(
                                    $experienceVersion->title,
                                    $experienceVersion->description,
                                    $newUser->language,
                                    $experienceVersion->id
                                );

                                // Fixed additional services
                                if ($experience->colazione) {
                                    $experienceVersion->fixedAdditionalServices()->attach($breakfast->id, ["price" => 0, "per_person" => false, "currency" => $experienceVersion->currency]);
                                }

                                // Additional services

                                for ($j = 1; $j <= 5; $j++) {
                                    if (isset($experience->{'nome_servizio_' . $j}) && $experience->{'nome_servizio_' . $j} != '') {
                                        $price = isset($experience->{'prezzo_servizio_' . $j}) ? $experience->{'prezzo_servizio_' . $j} : 0;
                                        $additionalServiceData = ['name' => $experience->{'nome_servizio_' . $j}, 'price' => $price, 'per_person' => false, 'currency' => $experienceVersion->currency];
                                        $additionalService = new AdditionalService($additionalServiceData);
                                        $experienceVersion->additionalServices()->save($additionalService);
                                    }

                                }

                                // Period

                                $periodData = [
                                    'currency' => $experienceVersion->currency,
                                    'date_start' => null,
                                    'date_end' => null,
                                    'entire_boat' => false,
                                    'price' => 0,
                                    'min_person' => $experience->n_posti_min
                                ];

                                $period = new Period($periodData);
                                $newExperience->periods()->save($period);

                                // Prices

                                $prices = DB::connection($conn)->select('select * from PREZZI_TRIP where eliminato = 0 AND mezza = 0 AND id_trip = ?', [$experience->id]);
                                foreach ($prices as $price) {
                                    // Add price to period
                                    $newPrice = new Price(['person' => $price->n_persone, 'price' => $price->prezzo, 'currency' => $experienceVersion->currency]);
                                    $period->prices()->save($newPrice);
                                }

                                // Availability

                                $availability = ['date_start' => '2017-03-24', 'date_end' => '2017-12-31'];
                                $availability = new ExperienceAvailability($availability);
                                $newExperience->experienceAvailabilities()->save($availability);

                                // Photos

                                $experiencePhotos = DB::connection($conn)->select('select * from FOTO_TRIP where eliminata = 0 AND id_trip = ? ORDER BY foto_copertina DESC, id ASC', [$experience->id]);

                                $k = 0;
                                $photoCounter = 0;
                                $is_first = true;
                                while (isset($experiencePhotos[$k]) && $photoCounter <= \App\Constant::MAX_PHOTO_NUMBER) {
                                    do {
                                        $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/trip/' . $experience->id . '/' . $experiencePhotos[$k]->id . '.jpg';
                                        $k++;
                                    } while (!@is_array(getimagesize($link)) && isset($experiencePhotos[$k]));
                                    if (@is_array(getimagesize($link))) {
                                        $photo = $experienceVersion->experienceVersionPhotos()->create([
                                            'is_cover' => false
                                        ]);
                                        if ($is_first) {
                                            $photo->is_cover = true;
                                            $is_first = false;
                                        }

                                        // Create public id
                                        $url = PhotoHelper::getExperiencePhotoPublicId($newUser->id,
                                            $newBoat->id,
                                            $newExperience->id,
                                            $photo->id
                                        );

                                        Cloudder::upload(
                                            $link,
                                            $url,
                                            ["format" => "jpg"]
                                        );

                                        $photoCounter++;
                                        $photo->save();
                                    }
                                }
                            }



                        }

                    }
                }
            } catch (\Exception $e) {
                DB::rollBack();
                dd('Error: ' . $e->getMessage());
            }
            DB::commit();
        }
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
