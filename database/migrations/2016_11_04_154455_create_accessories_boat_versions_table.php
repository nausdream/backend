<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessoriesBoatVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessories_boat_versions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('boat_version_id')->unsigned();
            $table->foreign('boat_version_id')->references('id')->on('boat_versions');

            $table->bigInteger('accessory_id')->unsigned();
            $table->foreign('accessory_id')->references('id')->on('accessories');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('accessories_boat_versions');
        Schema::enableForeignKeyConstraints();
    }
}
