<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperienceVersionsRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experience_versions_rules', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('rule_id')->unsigned();
            $table->foreign('rule_id', 'exp_ver_rules_rule_id')->references('id')->on('rules');

            $table->bigInteger('experience_version_id')->unsigned();
            $table->foreign('experience_version_id', 'exp_ver_rules_exp_ver_id')->references('id')->on('experience_versions');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions_rules', function (Blueprint $table) {
            $table->dropForeign('exp_ver_rules_rule_id');
            $table->dropForeign('exp_ver_rules_exp_ver_id');
        });
        Schema::dropIfExists('experience_versions_rules');
    }
}
