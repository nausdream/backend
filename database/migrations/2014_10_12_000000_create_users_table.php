<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Constant;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name', Constant::LAST_NAME_LENGTH)->nullable();
            $table->string('docking_place', Constant::DOCKING_PLACE_LENGTH)->nullable();
            $table->string('first_name_skipper', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name_skipper', Constant::LAST_NAME_LENGTH)->nullable();
            $table->boolean('skippers_have_license')->nullable();
            $table->string('first_name_contact', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('last_name_contact', Constant::LAST_NAME_LENGTH)->nullable();
            $table->string('email', Constant::EMAIL_LENGTH)->unique()->nullable();
            $table->string('phone', Constant::PHONE_LENGTH)->unique()->nullable();
            $table->string('password')->nullable();
            $table->boolean('is_captain');
            $table->tinyInteger('captain_type')->nullable();
            $table->boolean('is_skipper')->nullable();
            $table->char('sex')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('nationality', Constant::NATIONALITY_LENGTH)->nullable();
            $table->string('currency', Constant::CURRENCY_LENGTH);
            $table->string('language', Constant::LANGUAGE_LENGTH);
            $table->text('description')->nullable();
            $table->string('enterprise_name', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('vat_number', Constant::LAST_NAME_LENGTH)->nullable();
            $table->string('enterprise_address', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->string('enterprise_city', Constant::LAST_NAME_LENGTH)->nullable();
            $table->string('enterprise_zip_code', Constant::FIRST_NAME_LENGTH)->nullable();
            $table->integer('enterprise_country')->unsigned()->nullable();
            $table->boolean('has_card')->nullable();
            $table->boolean('is_partner');
            $table->boolean('is_mail_activated');
            $table->boolean('is_suspended');
            $table->boolean('newsletter');
            $table->integer('default_fee')->nullable();
            $table->string('photo_version')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
