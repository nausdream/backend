<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFishingLinePullCoastToFishingTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fishingType = new \App\Models\FishingType(['name' => 'fishing-line-pull-coast']);
        $fishingType->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $fishingType = \App\Models\FishingType::where('name', 'fishing-line-pull-coast')->first();
        $fishingType->forceDelete();
    }
}
