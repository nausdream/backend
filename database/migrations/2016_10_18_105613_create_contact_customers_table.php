<?php

use App\Constant;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', Constant::FIRST_NAME_LENGTH);
            $table->string('last_name', Constant::LAST_NAME_LENGTH);
            $table->string('email', Constant::EMAIL_LENGTH);
            $table->string('phone', Constant::PHONE_LENGTH)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_customers');
    }
}
