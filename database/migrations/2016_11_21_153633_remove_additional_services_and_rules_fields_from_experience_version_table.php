<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAdditionalServicesAndRulesFieldsFromExperienceVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            /*
             * Rules
             */
            $table->dropColumn('animal_small_allowed');
            $table->dropColumn('animal_medium_allowed');
            $table->dropColumn('animal_large_allowed');
            $table->dropColumn('alcohol_allowed');
            $table->dropColumn('babies_allowed');
            $table->dropColumn('kids_allowed');
            $table->dropColumn('cooking_allowed');

            /*
             * Additional services
             */
            $table->dropColumn('aperitif');
            $table->dropColumn('aperitif_per_person');
            $table->dropColumn('aperitif_price');
            $table->dropColumn('fishing');
            $table->dropColumn('fishing_per_person');
            $table->dropColumn('fishing_price');
            $table->dropColumn('sport');
            $table->dropColumn('sport_per_person');
            $table->dropColumn('sport_price');
            $table->dropColumn('alcohol');
            $table->dropColumn('alcohol_per_person');
            $table->dropColumn('alcohol_price');
            $table->dropColumn('analcoholic');
            $table->dropColumn('analcoholic_per_person');
            $table->dropColumn('analcoholic_price');
            $table->dropColumn('fuel');
            $table->dropColumn('fuel_per_person');
            $table->dropColumn('fuel_price');
            $table->dropColumn('sun_cream');
            $table->dropColumn('sun_cream_per_person');
            $table->dropColumn('sun_cream_price');
            $table->dropColumn('chef');
            $table->dropColumn('chef_per_person');
            $table->dropColumn('chef_price');
            $table->dropColumn('fruits');
            $table->dropColumn('fruits_per_person');
            $table->dropColumn('fruits_price');
            $table->dropColumn('inflatable');
            $table->dropColumn('inflatable_per_person');
            $table->dropColumn('inflatable_price');
            $table->dropColumn('hostess');
            $table->dropColumn('hostess_per_person');
            $table->dropColumn('hostess_price');
            $table->dropColumn('sailor');
            $table->dropColumn('sailor_per_person');
            $table->dropColumn('sailor_price');
            $table->dropColumn('tender');
            $table->dropColumn('tender_per_person');
            $table->dropColumn('tender_price');
            $table->dropColumn('snack');
            $table->dropColumn('snack_per_person');
            $table->dropColumn('snack_price');
            $table->dropColumn('towel');
            $table->dropColumn('towel_per_person');
            $table->dropColumn('towel_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('experience_versions', function (Blueprint $table) {
            /*
             * Rules
             */
            $table->boolean('animal_small_allowed')->default(false);
            $table->boolean('animal_medium_allowed')->default(false);
            $table->boolean('animal_large_allowed')->default(false);
            $table->boolean('alcohol_allowed')->default(false);
            $table->boolean('babies_allowed')->default(false);
            $table->boolean('kids_allowed')->default(false);
            $table->boolean('cooking_allowed')->default(false);

            /*
             * Additional services
             */
            $table->boolean('aperitif')->default(false);
            $table->boolean('aperitif_per_person')->default(false);
            $table->integer('aperitif_price')->nullable();
            $table->boolean('fishing')->default(false);
            $table->boolean('fishing_per_person')->default(false);
            $table->integer('fishing_price')->nullable();
            $table->boolean('sport')->default(false);
            $table->boolean('sport_per_person')->default(false);
            $table->integer('sport_price')->nullable();
            $table->boolean('alcohol')->default(false);
            $table->boolean('alcohol_per_person')->default(false);
            $table->integer('alcohol_price')->nullable();
            $table->boolean('analcoholic')->default(false);
            $table->boolean('analcoholic_per_person')->default(false);
            $table->integer('analcoholic_price')->nullable();
            $table->boolean('fuel')->default(false);
            $table->boolean('fuel_per_person')->default(false);
            $table->integer('fuel_price')->nullable();
            $table->boolean('sun_cream')->default(false);
            $table->boolean('sun_cream_per_person')->default(false);
            $table->integer('sun_cream_price')->nullable();
            $table->boolean('chef')->default(false);
            $table->boolean('chef_per_person')->default(false);
            $table->integer('chef_price')->nullable();
            $table->boolean('fruits')->default(false);
            $table->boolean('fruits_per_person')->default(false);
            $table->integer('fruits_price')->nullable();
            $table->boolean('inflatable')->default(false);
            $table->boolean('inflatable_per_person')->default(false);
            $table->integer('inflatable_price')->nullable();
            $table->boolean('hostess')->default(false);
            $table->boolean('hostess_per_person')->default(false);
            $table->integer('hostess_price')->nullable();
            $table->boolean('sailor')->default(false);
            $table->boolean('sailor_per_person')->default(false);
            $table->integer('sailor_price')->nullable();
            $table->boolean('tender')->default(false);
            $table->boolean('tender_per_person')->default(false);
            $table->integer('tender_price')->nullable();
            $table->boolean('snack')->default(false);
            $table->boolean('snack_per_person')->default(false);
            $table->integer('snack_price')->nullable();
            $table->boolean('towel')->default(false);
            $table->boolean('towel_per_person')->default(false);
            $table->integer('towel_price')->nullable();
        });
    }
}
