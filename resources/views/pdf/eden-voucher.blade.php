<!DOCTYPE html>
<html>
<head>
    <title>{{ $guest_name }}</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <style type="text/css">
        @page {
            size: A4;
            margin: 0;
        }
    </style>

</head>
<body style="color: #5B5C59; font-family: 'Montserrat', sans-serif; font-size: 12px; margin: 0;">
<div id="container">
    <div id="header" style="background-color: #37A29E; padding: 30px 0; text-align: center;">
        <img id="logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUoAAAB1CAYAAAAocqjJAAAAAXNSR0IArs4c6QAAIZlJREFUeAHtnQe8FNXZxkVFLNgxglEsqFixK1Hsxq5Ro8ZCsEZjPk0U22dFMYqfWGONJtZoCoktxpIY7AVEjQqKHeyiWIMi7X7/5zoH3zt3d3Zmd/bu3t33/f2ee86c8r7nPHPmnXPOzOztMoeLMxBjoKWlZV2S7gCvgRfBuCgc26VLl4+JuzgDTcVAl6bqrXc2FQM4yrkpuCf4IdgaLAuCvEVkdISnCMfgPP8bMj10BhqRAXeUjXhWc+4TjnNFVG4DtgdynN1BkFlEngH3R3gMxzk1ZHroDDQCA+4oG+EsdmAfcJrzYG4A2BHsAFYDVuQkHwXBcT6L45QzdXEGnAFnoDkZwHEuD44BD4IZIC6TSRgBDgd2Cd+chHmvnQFnoLkZwBH2AAeC28FXoJCMJvEE0Ke52fLeOwPOQNMzgCPsDvYDd4CpoJA8Q+IpoG/TE+YEOAPOQHMzgCNcGBwA7gbTQCF5gcQhYPXmZst77ww4A03PAI5wcXAUGAOKyXNkHAt6NT1hToAz4Aw0NwM4wtXBcPAeKCR6OHQP2Bd0a262vPfOgDPQ1AzgBOcCOwI9HZ8OComenl8C+jU1Wd55Z8AZcAZwhL3AaeBtUExGkXEQmM8ZcwacAWegaRnACWqW+SNwL5gFCsknJF4E/Kl5044U77gz4Ay0MoAj7AMuAJ+DQiJHKoeq5bt/bebjxhlwBpqXAZygXjM6HrwDiskrZBwJFmheprznzoAz0PQM4AS7gkHgeVBMtCwfBvwVo6YfMZUT4MuUyjl0DTVkAEe4HeZPAFsVacY00m8Cw/lxjpeLlKlJMm3XrHehCAsXiCtND6z0QyRCVxOPH89JnqQlQojHQ/HxDdCPlxQLlfcl+DzCZyb+OTxKR1OJO8qmOt2N21mczqb0bijYokgv9QtGt4GzudCfLVImczJ2F6VST6CZq8LFQHB6IZQzDPEQKm0u0BlFjlROdDRc7toZO5C1ze4oszLm5euaARzXljRQDlM/BVdM/kHGmVzk+uHhdoIOzdzk9KwDtPHgFJXWzC/CXwuHh7QjsAET3FE24En1LrH+bGnRr7PLYfZP4OMu8l4C1vEprlmhXxuQkCBajvfFUU5KKNMwWT4YGuZUekfEAA5Sy1n97qV+lV04HPQDLvkycARO8qp8VdavNneU9XtuvGVFGIiWxsuTHZyhDZcjXUvnWoj2Qb8CUwz0/4TscTwez59KeekR9GAmHi+UZssqX9e1HgIJ84L5zXFILxX2oY44LiRPkrgxjlK2mkLcUTbFae58ncQZ6gJfAVgnGOK9Sa/2g5Dp2JgcQf95UnGFNh7SQqgnwg3hPOB/Y/r6GIiLeFmffj4fz2jk47kbuXPet/pngAtSe4L6vzsB+hxRDnFpUI0b+afofRe8E4WKB2i/rdXp4Qi+IN7MckCRzg9rNicpHqoxEIvw68nNzAAOUY4vOEOF+oFehYuAPGQGSt4HwekVcoTvcpF/nYexRtbBudLT/DeBZvVWxnKwHhw23XuUPqO0w8DjFTPARab3A9cAeoAirBmFSq9UtOx9IwZd0Ep7mwt4JqFL5QwMRkXcSYrbg5rRSYpOd5RiwSUzAzjEOam0EggOMYTLklbuSkUzlQkg7gxbj7lI9bWISxUZ4Lz2QP0RBUycA/9jCqQ3RZI7yqY4zZV1kotHn8tpmbwuWCfCWoTdQTkyiUrjC2AiF+OschR6ndwY0Oeg8fOqF/P1TmrTSrl3/qYlrNE7jlPUkmttsD4ITlH7iXKWWURLNc0ErUPUy93jcYZ6oOJSZwxw7rU3+TqY3zRNrzutwzl7xaQ1XdRnlE13yr/rMBdGV460hyinuEEUrkGYZVzIIb4KXgDa7BfkHF/j4mq6TX/63ZnldBpvnaT6cmyzO0mR4DNKsdAEglPUudarNxuBDYGco5bP3UBa0VPl5yLIIco5aob4DaFLJ2aA8aGxoXNqb5K3c25378Tdyq3plpTclLqi2jPAwF+cVsgpCv2BnGPaV3H0qs2LIDjF1pCL5iPSXBqTgfPolvUHEzg+qDG7mr1XPqPMzlnd1cAp6isVLaE3AXKKwoogjUylkL6yeBY8E+EFnyXCRJMI42dLujrSdFdf3wxgDIw2aU0dtXeQpiaiM3Wegb0w7ZUz3BjIOWrWGH9SSVI70RJZTnGMwYtcEJpBujQhA4wl+YBLY10/0Z1kjBE/rH8GGMy9wf7gKqB/fzATlBKVGQt+Dw4H6wI9vHFxBmYzwJgYDKzcPjvTI7MZ8KX3bCrqI8KI1TlZFWxq0DtF6z6gzBNgFNCSaQyzgi8JXZyBggww1nqS8TIIX02NJ74R4+aLghWaONGX3jU++QxWfeGip89bgM3BAKAHMUmiPSTtKT4J5ByfYHBPJHRxBrIwcDGFg5OUc9zNnWQW+rxs1RiQYwTrAS157gSfglIymQJ3gZPAZkC/JejiDJTNAGNoVxBE/xd957KVeUVnoFIGGIBdwFrgGPB38BkoJRMp8AegvcXVgG+RVHoivP5sBhhP+v/o74Igp83O9Igz0FEMMPr6giPACPAxKCUvU+BqoAc2afYjO6orbqcBGWCMXQOC/JWI34gb8DzXXZcYaL3AQHADsHdqDgvKeFKvBD8B2lB3cQY6hAHG23YgyBNEfBunQ5hvQiMMru5gJ3AR0Cs4peQNCuguvi/QL3q7OAMdzgBjrwd4D0heA0t0eCPcYOMywIDSPqPeQ9TDlAfANJAkk8j8IzgELNe4zHjPOhMDjMXbgUTbQSt3prZ7W+uUAQbSkmAQuAXI8SXJ12T+CxwH1ga+51On57VZm8WYPAxIpgJ9zeXiDGRngMEzNxgAzgZPA70ykSQvkamlt/Z8fJ8nO+Veo4MYYHyuAqYAfa21dweZdTONwgCD5ntAs8Y/g09BknxFpt5l1NPs5RqFA+9HYzPAWNV++jgg+Vlj99Z7lxsDDBbtNZ4ORoFis0a96/gkuA4cD3zWmNsZcEUdyQBj909AcnRH2nVbnZABBkkf8FsQf3XnfdJGgsvBkWBrsFQn7KI32RloxwBj+WggObVdpic4A3EGGChbAX0uOBwcDH4A0v6IbVydHzsDdc8A43tTMB2cW/eN9QY6A86AM1ALBnCQWinFf2eyFk1xm86AM+AM1CcDOMms/zWzPjvirXIGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8AZcAacgaZgoEtLS8vB9PRH4GtwVpcuXcbl3XNszIfO4aAn+A02Hs7bhvRhZyDBnuBNcAJ2phPmJug/EGW7g8fR/X+5KY4pwo7Ox4HgVXAStmYSliXo6kHFDcDaYGmwEJgffAk+A2+Bp8Gz2PmCsGzB1jlUXr0MBdOoMwl8GOE/hE/RnlmEuQptXASFp4IlMijWOBJXwgfgKTC2kvNC/TZCu4aSsHmbxHwOPkbNUbT1vXzUtV5nK6Dr12Buheh+Pi/dVg+c7M3xflHandi51uZXK47dhdF9Pvge+BScIucyFQR5hUh3MnIVdO4SDBCOzFV5pAy9S4CvjZ1D87aD7ilG/5J56w/6sPGqsbNrSM8SUv/H4F4ww+hKik4n826wH5gniy2VpU4/kKdMQtkNYFfQJWt7ipVH189BHqKxcBvYFlTUPuovDqopvyrGRznpNHSEaezj5ehIUwcb7xo7Gp8rpqlXaRnsDDd2FT1pTpR2M4pXIn6ZOc4rOq9RZOMmueLoUWiwuo+jg+pfnmL1W97ytCFd1o5mf6mFPq8CHqHCX8F2YK6UlTU72AHcDMajYx+QxQHkzYdmfIPAHeAp2qK+5CGZ+EwwKD27gfvAWNq3UULZUlldSxWoMP+jCuvPrk4/NZvUqirID0gbEA5yDu11oPF5es7626mjL5pF/iKW0U3G43IAhe9jmvvHeEa9HtNeDdp45/qSpiXsbfXa7rzbBQ9auqm/i8Z0a8n4OngnwmTCBcFiQEvlNYF1dMtzrPO/CLgKZBUtVQdnqCRHIceoWfoyQBeetmuCrEdEs+N/Ew5kbH4QMioMn6V+muWc2rcwEB+rArVncRBkNSKP0b6htG1oSKwgPIC6syqob6vqXMuZ5yXHoih+8z2BtEfzMpCgR6uds+H45YQylWadiAL5k7aC4ULyOYm6WHIRdO1ljOQ+VUf3kUa/jT6RSwciJSieaZT3zlO31YWNt42dfWxesTjllwdfmHqKPg5KLqUp0xXsBG4BWuIE2auYvXg6FTYIlQinxPOzHFN/XrAD0NI7vnWg5Vj/LPpsWeoOBkH+bPOyxFGgmdR1wPLFYUvmWQ91eqqikbxXQlm6VrQs7dMWgd1+Ck2eRUQ3kFwFnZODARPqBl4VwYbOw1fGVoieob0lK3KQQZ4kUmjGmbmR6Kmao0T3XOB1EOS3RKaFA8LNMje4SAV01bOj/Jfps87jjkW6kZhMvRXB5eCsxIKxTMrn5iitavSuBm4HVr7hYJAtlzZOvVwcZbCHvvXAOGBFDyFSCxU7i6McYjr5EPEXzPG1qTucsiC6CzlKXYPlPDAsaRW9F5v+2Gg7RzmQXOsMzimpPUUBdFbTUe5teqS7QQ9wvUm7K0UTUxVBp+Wmd6pKZRTCTqYZJeX7mP4qukMZZiuqgs2qOMrQKPQfAuwNUDPNbUN+2pA6uTpK2UXnYsA6jbc4ni9Dm+reUao/QA/YgugB7aBwQKib11Jp+5ymHPqso7TnfkSa+lnKYKsXsA+Drb0z4lP8B1B+rjFwIpW3NMf1GD3ONOo69i/0OoQe7bdE6TvSh6rcgYzdWketw3gEDu6pdYPytk+ffo/ObYD23CTaJ/sL51b7gzUV2vYJDdgZTI0aon3Ww6N4owTaN9U+skR7hJqAaBmsfW/JPODo1lh1/lxv1P6Y897PHOcRPQkl4eGR9sDvtUrjjlJ5Q8CoqJDy/0Cj7MZ1lFX7gHZtQSs2iFoyi/BixRm4YwnuVhzRk9vjW2ON+0fvRwZ5LEQaLeS8PkyftgdfR33TA5Y7GQepZ29RvdwD2jYRpZcYxXuYeKeOwq/8gH04dyH9bQF6aHeR6dzhlNV7utWQa1AanLKu6aF5GaHN30fXz4y+YcS/MsdziIA2QudnkKCXPL+IMjSdzn3/IdJdaWAd4B20/VWjcLiJ7wcZ1pmYrIaIauAECTPpcNxQIed4DB0aCEI/+xA/pk46ebNpx8aMufjbBya7U0V3o7UrRS3+iPBG03o5sM+iYznJas2kv0H3ryM7Cn4Ev+ua40qidjb5HoquBmF8tept5yiVymB8g+AXrSW+/aOXfo80xzWP0h4tuexe3AW2UfThIY5HR2ldCevlYrLNzCs+wShawcQbMsq5vZWOXWg6py2isCw0yR0bpV0vYPHDyKq2Blbu2BZUzZqdkFxBP8MWg3zFl1i90lg+mnOhZXg1RBO2CUbxUBMvK0pbNYE61FTW60ez+xfSCzpKZVJYd8cbQ0FCva2u9+3qRbQ3GWZSo2jvYwUadp5JO4z2L2KOGylqZ9Lak9WStNFFT+U/jjqpmcz/1kmHw/JQzVmyTtpUdjMYSwOo3D9SIAdyeQFlvyFNMz6JVqD7t8Zy/sM1Ph2VOu9B9ErbhuGgzPBk6nWL6r5NqL1wSekZ5bflWv9qFvl6dKyNzj/RsJrvB9GG+Mmwy+youa3Bbfx9LUroTmhnyVFyQwQP04sJUU8WJDwzijdswEXzOZ2zF02mV3KqSMwnRndd7u2b9qWJ2tnkjfCupXcbIU0PP+ykSl/FhUlMm7I5HMhO8ElSZ8dAJvW0cRkqHGIqaTYZHL5JnqP9HqXNpZKm1fsCeXKJlrt287Y1sQZ/fonNML3XNsHthdpA+2eRfr7J+yXkyOE3lNDPmXToMtOpX9HPU6s4WI2pmkZ10cyIWrA0/c1rz6qSTml2G2RKiHTGED770u5dorZrhpV07es60/UmkZ/YuTWW8x/Gus73UKNW39pvYo6zRE+hcPAjE4lraR8k04xSS/CnqHlaqE2oJ1t7mOMOjWJbM6afG6MXRY7CJLWJ3sDRpChFS6FBbXIb50DLn0dMd3Sn1UvB5Q4io6o+o5x3PUSwfd61Dlpql9uT66A9lTTBbm/dBd/jiykj7xXy7jD5J5h43lFtC75slFrHaZKLR7kuliX3YFNCv5wWJoQm+dvonO1SCidoaftvk/U7DGnaWgv5GUbDHtynxK9LagSd176KnEiQavxYRtBdszA6yT+mAa+aRmxK/FHOlX5UQp959jR5jRK923RkLRPv8Cj8fh+jyxnD40y8U0Xpixz+T02jLzDxYtHzTMYAdPQ3x7lFGeszUXamUbgVtjY3x2mip1Coa1TwDUJNqKy0mVHObXOKxWmYvuXUTOw50AMsCvR+5VZRozmsvmBP7T3aWLoS+2mWN1dQ5ySwAFgJ7A7+BhpK4OIjONLy82Jg917W51i4hHzNwNT3Wyn/LmFnlzdNB2p9I7Az2jfgV3t35ch5nKc2F2oZSl7Cvl1KZlWh7a3wkONpdD1USgFl9Nmzxpdu0BLNKqu1+vwzuk8FWuZLNKtM5Sxp43KUPRAE0WwybOGEtLahTogR3RGLCuX02ZKV04sWNhlUyOUTRvQMNMb1yVQvYyYxSln7HeeoxMJFMtFRl58wFmoubd0I6Btp3eQKidL1oxnat634oQM6qvoJY6E+Kg27mrkEsU6zYBUK5v4JY9QO/ZCHPl0Mcm7BBhRIpEL8E8ago9Jw1QLmSiZhdAHwiTGu5xSphDo7m3q6Xsp+RYq69hPGfvEGkL+nsaXoNvEyhY4ppxVxkFeJtJswknZTKEB4RvxHMRIdpYxS6VKjYAbxTQo1xqZRJi9H+R9jO9Pdknq9gf2lly1sG9PEqd9pHGXoD21eDpwGxoNiopvOzWCNUC9rSN1aOcq+plN6+JgolK2Wo7zCtEPXRe/EhphMylbLUa5uzKSO0h49DAwykUg7R1JMGWW7gHGhMuHVxcqWSqduKUcpW88ZW4+n0LkC5a0fsNsLs6tTpo2jTE3AbA3ffg6oKe6aYC6gC2xtpq7aWK+aYGNblNs9qDR7JrPbQ/t0t9d0ff8oUcuCB6N4wwb0ewKdO0ug/7pw9gTay9T5CzIPkf3AvpS5ifBI6pV0OqFyjcOFjP2qjkFjZ3YUvnQNDQNHzE6cY47zNN7Mcdbo5VSYlbVSrLyW3pn3SOmPrmm7vXUJepKXpcYwZVHRomca10XJgzg+nfRytyGM9rbRyNYQUm+Lcn6Are1Jv7dtyTZHWq4Hv6cHQre0yf3uoOW7KDH1ykjJGaUqU351YH+37S9tlMYOKFvxjBId9mfE7AZ+zFrxQ3SsBaxYZ1G8YpRDxU43oyzWKfqyMjgdFJppvkZ6ptkI5Ws1o7TbQaOL9Tek087cZpTo2gI8DaxoOyPs7QWziSHl4zPKtA9ZE/WWk0lbdLMMop/rszeiVCqpMw94JyghPCdVxVgh6iXOKENxytlzUHQMUK4PsLPJolsKlLsRBGn360HBdmKIx9adarApJEd4qDnONYrutVG4jVGaaTYZ6tFuPYy6LxwTalaZRdreZbLULL9sVWzCxStAv8i9Ck3bETxumtiH+D/g/XsmrV6jS5uG5T5rkW54mAssDlYEu4Fh4EWyHgB6eBbkLiLbwOk3IaEThseZNl9NX74wx6mi1JlGwYtN4SPgq7s5zjtqn5Xohr1LEQMqF2aTLxHXCrOYtL3uUGol1YwyaKaiHhYEmUJEF107IV2ONIi9INuVLZRARS3vgzxbqEzaNJRsFRQR6u6SZS/pM1N3tbQ2s5bDxqfGzk5Z65dTHntaM/0C2Dvu/Wl1UU8DNEiaNxHSqk4sh8G/BqOE5yYWJpMydkapqnqoVQoqlyRaaVwGtGzNLNSrixkl7YhfG8tk7kxUAV0LAXu92IlVKrXUTzWjlDLKjgJBniHSxRrheCWgveMge9v8eJxCN4SChGcE7xovl/b4EArqtRM52PnBH1HaP887KvrkyGyn9AVA2ULbRqLzaRSsB9T/YyIQlJTJlFg4KrVYydJlFKBtWnYFG9Igm1UXeNEdVA8kNMAuiwxuzfE65FV0c6pW42mblrjbGf13mnjaaJsLKm2lqNwswhHgTDjSDKWzy/GmA1OJa/lpkjJH9b5jkKPRdSk8TQ8JOYenoS+sFtchvhu4zdjQbDLcyMYS13lLkrYdp/FWMs0oZYXKWwK7d2en3K0NIb/sGSV1LwJB3iYSXhJN6mRiHjr2DgoJ/wsWTawQZVJOL24H2SNNnaxlUL5kMBCFK2fVUWl57Op9uCBXptFH4Q6fUWJTP+Aa5EMiJff2KBOfUYb6aUK7L6/yV6ThplQZ9NR8Rkkb1lSHqiwFnzAX44e2pJ5RSgfl9XFFkOeJtN4ECfVmhJ1N6mFmolD+ehCkvD1Ka4E7xAMc2yWPXi3QnlfFgp5FUHKoUaQncHnckf6GzjcivQsQ/o+xkRR902RuZOJ5Rq1e9fWdPJWn1GUdwAYp63RoMcaGVgNnG6N6gV4zvCyiGYfGWCksSJmu6Neq6XYQ5DDasWk46OSh3ZusVlfsjLUaNjRrDLImkb2iAzub1HOKW0OhhLDNjFKDLQ8ZgpKtQbjI5Y37MbAq3VzXKxdhE1ivq1wDKhbapRnwhSgKS8yjOD6fdC03kuRhMgP5A5IKVpBn9eqLiK8q0FVu1RdMxd4mXk/Rw2lM36hBOm/lPFmdDr+fZ+xU2G7SQyQt5bR/vhZ6Ps2op26K0371xT4BHsbx+Jwa2A09uvHK12jWugNc3ZOT7jZq0KtttYdI3DzKGMLxOOL7mIJDKNfGCZq84lEUWcm89A6aUbIC0OsEQf5JJEx9My+9qatXDN4LyggvCLbyCNGnf5akT/6C/LyUXgra5YmcrT6HzE3QNzd4CwSxM/Xc7JRShPGlQgMI9aAjzZK2w5betEeveX0JgujCTiVUsEvvpKeeRfWhYzNgl3JaoZQt6Krp0hv7w0GQ14mUPN9ZOou+m4JywgfS1qVspqW39FJnU2NL0Qnm+OkMtq8z9c6QYitlO8qokftbZcRPiNLLcZQHG12Znk5nIGOIsaF3BxMHCPl6MvyyqXNlWltpyqF3oNGt6IZp6uVdBrvrmHZ8lEY/5TvEUWKnF7A3E91MU7/rR9mKHaX4QM8ZwMrhaXgqVAYlNXOU2NbTaTvBSbsNVagrBdPQ3w9Y2aBgwVgiFTI7Sqmg3r+sMRPfOWai6CF1rjX18nWUUSPtY/VpGNMFlMlRUl4O6UUQ5JaiPaogA+V6N25KMEIYltVFtVLmUFNeDnzjooUzZKBnCfCO0Z3qzkv5rYCeVi+fwVxiUXSdAoI8kVg4yqSwznOQKWnqZC2D8mXAc8EI4dcgbPekUkf5vByl3q18BATRg56yXhmjXi0d5fGhA4RyTNqHzV3Qe4+xMyKNgag9oVq/NHVUhgr6Qicuo9PWj3T83iioiqNcEAOanQV5lYidHZZ8j5Ly9sN66Vk3SyezlEX3pTIQyVOl6lKuG9DT9yCKVzoTl877g8Io/GGKtqxK2W+i8tpGqPjBAjq07P440qngmFLtUD7lquoo0a8llZ5sB9GWwE/StM2WoU4ujjLqsxz3J6FBhM+Dea29NHHq1MRRYrcrsDfns9K0t5wy2NHbMUFmEulTSg9lyppRSi917w7GonD7UvZsPnWq6yijRuqi0WwyiD0ZaRzlQ6Ei4Ujbgbzj6F8ezDD2tiplg7Lao9JsMoiWf5lmNsEG9eSYRgVFUXh5yE8KKbsusG2X0zwNhAdgSdXb5VFPF74u9iAaqD3aFSyQQLmqOEr0LgLOAXY8yUkOLtCMkkmqB4KUtUdpjaBoj6AsCsMDQlssMU69WjnKA0zbpxJfMrGhFWai375ed2UpdZSvxFHa8ZhqVWTbg+3fgSD5zyiDMSycGKzEwkRHSdkNY+V3CjqrFWJPL8oHuTeNHQrrB4Ct6ELWXWjFlPW17B8K7Fc4HLY6zXnS6FAZym8H7BcQ0jEJyCH0TKOHcouC/wVxPSW3IoJ+6tqBWfHSG326gZ0M7IyNw9b9tF2C3awh9XN1lLKPzqvUMCO7ZmkX9WrlKF8wbS77V37S9hVbexl72qpYIqku+WU7Suml/m+BVkebJNkplEedNo4yr9eDCtk6j0QtH7culJmQdrzJ+5C4OlrthxoPYie8QiDHo9c99L5VUSH/fMpNp4Cexus1ka7gYIH0cYR6TeEl8An4DGiWtxjQXuJmYH0Q519Oeh9061vZVELZ+7DXn8J3gvAUXgNQ7VIbtZ3wTzARvA++AEuBZSKsQrglUPuD6PWJwehOtZcUKplQ+3fbmuNSUfGgmatmNGqXxo3aFZfxJOxBu8RrPYm2J/Ra1+pRo/QgQK/HvVdmI7el/qwy6xaqpl8SettmoH8HjteI0nS+L7T5VYrfit7XgZbd84GjwOmgKkKf9YBNKEfEyXcCYVYq2mv7Tuu3MRT3AvYVHNkqOqMkrw+YqUI1lpvjfSl2TDu3ARMqbK+WzFpeJj51L9YGpVNXPxyr2VKcb5IyyfuULvnlQrwt1LEzykwGUxTW1sYRwDrzeBNSHaMj9xmlDKN3DaCHS0FGEkl1PikXn1EGHXmFmr0tbAniWO0LoptshwgGdR6DaMa4QDHD5FU0oyymN006tq8JjSRs/TLH3rlmplGStgweXTMYzbKsJNk4iIKpBpdVWIX43pCzaBq99PF+yq0EDgO6W2aRqRTWy7groudkYM9FFj36J3BTgWYFK4CTQeKMuIDyd0n7NeiLnr8VyC+VVHbbiyiWvifBsUD86N9+aAZfqdiZQm5tpm1jadhg0zjN0vcyx0nRGUmZOeRp9qavj1qFsa0xovYFOT9EOiC8HhsfRXYWI9wzihcKrK+w8UJl806z9mZpyTMMaAbxKCf7g7ytofPvnBhduD8FMn5Zgo0HydsFdEso0xFZL2JES9RUEl3A11BYd6G+hNpu2Bj0BFpSakB8BT4Gk4G+fBkJHqeunGVugr4vUaZzOoy2LEu4K1gXaLndC+gG8F/wOZgIngVPgEeoW4njkGPW0iosP4mmlmmUnAS01aIx+B9wD+0RX3nL3SjUzbs7SL1ySNMI2nslnK9J2d2Bxo+2CkqK+kk9bVVtXrJweQVGY0PnOoh41g9ILAceJO9hwg4RbGnW/SuMnQI09sckGB5Ons6VxlZHb7fciM31gW7OI4CLM+AMOAPOQBID/w/wsru/iIRiEwAAAABJRU5ErkJggg==" style="width: 200px;">
    </div>
    <div id="content" style="padding: 30px 50px 30px;">
        <table class="table" style="border-collapse: collapse; width: 100%;">
            <tr>
                <td colspan="2" class="h5" style="font-size: 14px !important; padding: 4px 0;">{{ $village_name }}</td>
                <td colspan="2" rowspan="3" class="text-right h5" style="font-size: 14px !important; padding: 4px 0; text-align: right !important;">{!! trans($translationAddress . 'voucher', ['number' => $voucher_number], null, $language) !!}</td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 4px 0;">{!! trans($translationAddress . 'assistant', ['name' => $assistant_name], null, $language) !!}</td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 4px 0;">{!! trans($translationAddress . 'phone', ['phone' => $village_phone], null, $language) !!}</td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 4px 0;">{!! trans($translationAddress . 'email', ['email' => $village_mail], null, $language) !!}</td>
                <td colspan="2" class="text-right h5" style="font-size: 14px !important; padding: 4px 0; text-align: right !important;">{{  trans($translationAddress . 'today', ['city' => $company_city, 'date' => $today], null, $language) }}</td>
            </tr>
            <tr>
                <td colspan="4" style="padding: 4px 0;">
                    <hr class="blue margin-top-lg margin-bottom-lg small" style="border: 2px solid; border-bottom: none; border-color: #37A29E; border-width: 1px; color: #BEBEBE; margin-bottom: 24px !important; margin-top: 24px !important;">
                </td>
            </tr>

            <tr>
                <td colspan="4" style="padding: 4px 0;">
                    <div class="h3 bold text-uppercase margin-bottom-xxl" style="font-size: 20px !important; font-weight: bold !important; margin-bottom: 66px !important; text-transform: uppercase !important;">{{  trans($translationAddress . 'summary', [], null, $language) }}</div>
                </td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-md bold" style="font-weight: bold !important; padding: 4px 0; padding-right: 25px !important;">{{  trans($translationAddress . 'contact_name', [], null, $language) }}</td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important; vertical-align: top;">{{ $contact_name }}</td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important; width: 50%;">
                    <hr class="margin-bottom-lg" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 24px !important;">
                </td>
            </tr>

            <!--SECOND ROW-->
            <tr>
                <td colspan="2" class="padding-right-sm" style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'customer_name', [], null, $language) }}</span></td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'room_number', [], null, $language) }}</span></td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'guests', [], null, $language) }}</span></td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-sm" style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;">{{ $guest_name }}</td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; vertical-align: top;">{{ $room_number }}</td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; vertical-align: top;">{{ $guests }}</td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; width: 25%;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; width: 25%;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
            </tr>

            <!--THIRD ROW-->
            <tr>
                <td colspan="2" class="padding-right-sm" style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'experience', [], null, $language) }}</span></td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'tour', [], null, $language) }}</span></td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; vertical-align: top;"><span class="bold" style="font-weight: bold !important;">{{  trans($translationAddress . 'date', [], null, $language) }}</span></td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-sm" style="padding: 4px 0; padding-right: 12px !important; vertical-align: top;">{{ $experience_title }}</td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; vertical-align: top;">{{ $tour_number }}</td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; vertical-align: top;">{{ $experience_date }}</td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
                <td class="padding-left-md padding-right-md" style="padding: 4px 0; padding-left: 25px !important; padding-right: 25px !important; width: 23%;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
                <td class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important; width: 27%;">
                    <hr class="margin-bottom-xl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
            </tr>

            <tr>
                <td colspan="2" class="padding-right-md bold" style="font-weight: bold !important; padding: 4px 0; padding-right: 25px !important;">{{  trans($translationAddress . 'departure', [], null, $language) }}</td>
                <td colspan="2" class="padding-left-md bold" style="font-weight: bold !important; padding: 4px 0; padding-left: 25px !important;">{{  trans($translationAddress . 'schedule', [], null, $language) }}</td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important;">{{ $departure_port }}</td>
                <td colspan="2" class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important;">{{ $time }}</td>
            </tr>
            <tr>
                <td colspan="2" class="padding-right-md" style="padding: 4px 0; padding-right: 25px !important; width: 50%;">
                    <hr class="margin-bottom-xxxl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
                <td colspan="2" class="padding-left-md" style="padding: 4px 0; padding-left: 25px !important;">
                    <hr class="margin-bottom-xxxl" style="border: 2px solid; border-bottom: none; color: #BEBEBE; margin-bottom: 50px !important;">
                </td>
            </tr>

            <!--FOOTER-->
            <tr>
                <td colspan="4" style="padding: 4px 0;">
                    <hr style="border: 2px solid; border-bottom: none; border-color: #D8D8D8; color: #BEBEBE; margin-bottom: 16px !important;" class="margin-bottom-md">
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center h7" style="font-size: 9px; padding: 4px 0; text-align: center !important;">
                    <span class="bold" style="font-weight: bold !important;">{{ $company_name }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center h7" style="font-size: 9px; padding: 4px 0; text-align: center !important;">
                    {!! trans($translationAddress . 'residence', ['legal_residence' => $company_legal_address , 'executive_residence' => $company_operative_address], null, $language) !!}
                </td>
            </tr>
            <tr>
                <td colspan="4" class="text-center h7" style="font-size: 9px; padding: 4px 0; text-align: center !important;">
                    {!! trans($translationAddress . 'contacts', ['phone' => $company_phone, 'mail' => $company_mail, 'web_site' => $company_site], null, $language) !!}
                </td>
            </tr>

        </table>
    </div>
</div>
</body>
</html>