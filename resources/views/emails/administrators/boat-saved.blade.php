<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>New boat saved</title>
    <link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf">


    <style type="text/css">
        /* GENERICO */

        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Montserrat", "Helvetica", Helvetica, Arial, sans-serif; }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }

        html{ background-color: #EEEEEE;}



        /* ELEMENTI VARI*/

        a {color: #31BEF8; text-decoration:none;}

        .btn {
            text-decoration:none;
            color: #31bef8;
            background-color: #fff;
            padding:10px 40px;
            font-weight:normal;
            margin-right:10px;
            font-size: 18px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
            border: solid 2px; color: #31bef8;
            border-radius: 100px;
        }

        .btn:hover {
            background-color: #31bef8;
            text-decoration:none;
            color: #fff;
        }

        .calltoaction {
            padding:20px;
            background-color:#fff;
            margin-bottom: 15px;

        }

        /* PREHEADER */

        table.head-wrap { width: 100%;}

        .header.container table { padding-top:0px; }



        /* BODY */

        table.body-wrap { width: 100%;}


        /* FOOTER */

        table.footer-wrap { width: 100%; background-color: #eee; clear:both!important; padding-top: 0px; }

        .content-social { background-color: #101C33; padding-top: 15px; padding-bottom: 15px;border-bottom: solid 1px #101c33; border-top: solid 1px #101c33;}

        /* CARATTERI */

        h1,h2,h3,h4,h5,h6 {
            font-family: "Montserrat-Light", "Helvetica", "Arial", "sans-serif"; line-height: 1.1; margin-bottom:15px; color:#5B5C59;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #5B5C59; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 44px;}
        h2 { font-font-family: "Montserrat-Regular"; font-weight:100; font-size: 26px;color:#31BEF8;}
        h3 { font-weight:100; font-size: 16px; font-style: italic; color:#31BEF8;}
        h4 { font-weight:500; font-size: 12px; color:#5B5C59;}
        h5 { font-weight:600; font-size: 18px;text-align: center;}
        h6 { font-weight:200; font-size: 11px; color:#31BEF8;}

        .bluetext { margin:0!important;}

        p {
            font-family: "Montserrat-Light", "Helvetica", "Arial", "sans-serif";;
            color: #5B5C59;
            margin-bottom: 10px;
            font-weight: normal;
            font-size:16px;
            line-height: 1.6;
        }
        p.lead { font-size:16px; }

        p.headertext { font-size:14px; color: #fff; }

        p.footertext { font-size:14px; color: #fff; }

        .prefooter {
            padding-top: 30px;
            border-top: solid 1px #D8D8D8;}


        /* CONTAINER */


        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important; /* per centrare */
            clear:both!important;

        }


        .content {
            padding:20px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }


        .contentheader {
            background-color: #fff;
            padding:20px;
            max-width:600px;
            margin:0 auto;
            display:block;
            border-bottom: solid 1px #101c33;
        }

        .content table { width: 100%; }



        .clear { display: block; clear: both; }


        /* Specifiche Font */


        @font-face {
            font-family: 'Montserrat-Light';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142575/production/assets/fonts/Montserrat-Light.ttf');

        }

        @font-face {
            font-family: 'Montserrat-Regular';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf');

        }
    </style>
</head>

<body bgcolor="#FFFFFF">

<!-- PRE-HEADER -->
<table class="head-wrap" bgcolor="#EEEEEE">
    <tr>
        <td></td>
        <td class="header container" >

            <div class="content">
                <table bgcolor="#EEEEEE">
                    <tr>
                        <td align="left"><h4 class="bluetext">There are news</h4></td>
                        <!-- <td align="right"><h6 class="bluetext"><a href="#">Visualizza e-mail sul browser</a></h6></td> -->
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table>
<!-- /PRE-HEADER -->


<!-- BODY -->
<table class="body-wrap"  bgcolor="#EEEEEE">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- HEADER -->

            <div class="contentheader">
                <table  width="100%" bgcolor="#fff">
                    <tr>
                        <td align="left"><a href="#" target="_blank"><img src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/logo_blu.png" alt="nausdream_logo"/></a></td>

                    </tr>
                </table>
            </div>
            <!-- HEADER -->

            <div class="content">
                <table >
                    <tr>
                        <td>
                            <h2>Hi Admin,</h2>
                            <p class="lead">there are news. It was added a new boat.</p>
                            <p>Access to control panel and check that it has requisites to go on platform.</p>

                            <!-- CALL TO ACTION -->

                            <table class="calltoaction" width="100%">
                                <tr>
                                    <td align="center">											<a href="{{ env('FRONTEND_LINK') }}/admin">{{ $name }}</a>
                                        <p>{{ $first_name }} {{ $last_name }}, {{ $docking_place }}</p>

                                        <div>
                                            <table  width="100%" bgcolor="#fff">
                                                <tr>
                                                    <td align="center"><img src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/nuova_barca.png" alt="barca"/></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div> <br> </div>
                                        <div>
                                            <table  width="100%" bgcolor="#fff">
                                                <tr>
                                                    <td align="center"><a href="{{ env('FRONTEND_LINK') }}/admin" class="btn" >ACCESS TO CONTROL PANEL</a></td>
                                                </tr>
                                            </table>
                                        </div>

                                    </td>
                                </tr>
                            </table>

                            <!-- /CALL TO ACTION -->



                        </td>
                    </tr>
                </table>


                <!-- PRE-FOOTER -->

                <table class="prefooter" width="100%">
                    <tr >
                        <td class="container" align="center">
                            <h4>Se non riesci ad accedere al tuo pannello di controllo chiedi pure a Mister Basciu o Sir Puddu, sono loro che fanno impicci. Se invece non ti piace l'interfaccia chiedi pure a Davide o Paola.</h4>

                        </td>
                    </tr>
                </table>

                <!-- /PRE-FOOTER -->
            </div>
            <!-- /content -->

        </td>
        <td></td>
    </tr>
</table>

<!-- /BODY -->



<!-- FOOTER -->

<table class="footer-wrap" width="100%">
    <tr>
        <td></td>
        <td class="container">

            <!-- Social -->
            <div class="content-social" align="center">
                <table>
                    <tr>
                        <td align="center">

                            <p class="footertext">Nausdream S.r.l. - P.IVA: 03635840923 <br> &copy; 2017. Tutti i diritti sono riservati</p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /social -->

        </td>
        <td></td>
    </tr>
</table>

<!-- /FOOTER -->

</body>
</html>