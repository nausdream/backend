<table class="prefooter" width="100%"
       style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;padding-top: 35px;border-top: solid 1px #D8D8D8;width: 100%;">
    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        <td class="container" align="center"
            style="margin: 0 auto!important;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
            <h4 style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, &quot;sans-serif&quot;;line-height: 1.1;margin-bottom: 15px;color: #5B5C59;font-weight: 500;font-size: 12px;">
                {{ trans('emails/footer.donot', [], null, $language) }}</h4>
        </td>
    </tr>
</table>