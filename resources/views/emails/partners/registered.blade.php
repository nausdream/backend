<!DOCTYPE html>

<html style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: sans-serif;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;font-size: 10px;-webkit-tap-highlight-color: rgba(0,0,0,0);">
<head style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
    <title style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
        Email_link_form_ins_barca</title>
    <meta charset="UTF-8" style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"
          style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat"
          style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
    <style style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
        body {
            background-color: #F5F5F5 !important;
            font-family: 'Montserrat', sans-serif !important;
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            line-height: 25px;
            letter-spacing: 1px;
            color: #5b5c59 !important;
        }

        .container {
            padding-bottom: 52px;
        }

        #head-txt {
            font-size: 12px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            color: #5b5c59;
            padding-top: 20px;
            line-height: 12px;
            white-space: nowrap;
        }

        #head-img {
            display: block;
            position: relative;
        }

        .naus-logo {
            position: absolute;
            left: 45%;
            top: 5%;
        }

        #email-body-txt {
            padding: 22px 20px;
            background: #fff;
        }

        #email-body-txt h3 {
            color: #31bef8;
            font-size: 20px;
        }

        #email-body-txt p {
            font-size: 16px;
        }

        .butn {
            font-size: 20px;
            font-weight: 600;
            font-style: normal;
            font-stretch: normal;
            color: #31bef8;
            padding-top: 46px;
            white-space: nowrap;
            margin-bottom: 80px;
        }

        *|*:-moz-any-link {
            text-decoration: none;
        }

        #head-txt-div {
            padding-right: 0px;
        }

        .sep_0 {
            padding-left: 0px;
            padding-right: 0px;
        }

        .sep_1 {
            background-color: #fff;
            border: 1px solid #b2b2b2;
            border-radius: 100px;

        }

        #email-body h4 {
            padding-top: 20px;
            font-size: 20px;

        }

        #helper-1, #helper-2 {
            text-align: center;
            display: block;
            padding-top: 14px;
            padding-bottom: 20px;
        }

        .bl_rect-c {
            background-color: #fff;
            padding-top: 0px;
            /*padding-left: 25px !important;*/
            padding-left: 10px;
        }

        .blue_rec-c {
            width: 38px;
            height: 3px;
            background-color: #fff;
            margin-top: 9px;
            border: 3px solid #31bef8;
            border-radius: 100px;
        }

        #helper-1 #help-email,
        #helper-2 #help-phone {
            font-size: 18px;
            padding-top: 30px;
        }

        #email-body-txt p.text-center {
            padding-top: 16px;
        }

        #fooot {
            background: #101c33;
            padding-left: 0px;
            margin: 0px;

        }

        #copyright {
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            color: #ffffff;
            letter-spacing: .25px;
            line-height: 18px;
            padding-left: 20px;
            padding-top: 35px;
            padding-bottom: 35px;
        }

        #social {
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            font-stretch: normal;
            color: #ffffff;
            letter-spacing: .25px;
            line-height: 18px;
            padding-top: 35px;
            text-align: center;
        }

        #social-icons {
            padding-top: 30px;
        }

        #social-icons img {
            margin: 0 10px 0 10px;
        }

    </style>
</head>
<body style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin: 0;font-family: 'Montserrat', sans-serif !important;font-size: 14px;line-height: 25px;color: #5b5c59 !important;background-color: #F5F5F5 !important;font-weight: 300;font-style: normal;font-stretch: normal;letter-spacing: 1px;">
<div class="container"
     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;padding-bottom: 52px;">
    <div id="first-row" class="row"
         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
        <div id="head-txt-div" class="col-md-3 col-md-offset-2"
             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 15px;width: 25%;margin-left: 16.66666667%;">
            <p id="head-txt"
               style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 12px;font-weight: normal;font-style: normal;font-stretch: normal;color: #5b5c59;padding-top: 20px;line-height: 12px;white-space: nowrap;">
                Diventa un Capitano Nausdream</p>
        </div>
    </div><!-- end of first row  -->

    <div id="header-img" class="row"
         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
        <div id="head-img" class="col-md-8 col-md-offset-2"
             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 66.66666667%;margin-left: 16.66666667%;display: block;">
            <img class="naus-logo"
                 src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/logo-bianco.svg" alt=""
                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;position: absolute;left: 45%;top: 5%;max-width: 100%!important;">
            <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084913/assets/emails/img-header_2x.jpg"
                 alt="" class="img-responsive"
                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;display: block;max-width: 100%!important;height: auto;">
        </div>
    </div>
    <div class="row" id="email-body"
         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
        <div class="col-md-8 col-md-offset-2"
             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 66.66666667%;margin-left: 16.66666667%;">
            <div id="email-body-txt"
                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding: 22px 20px;background: #fff;">
                <h3 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;page-break-after: avoid;font-family: inherit;font-weight: 500;line-height: 1.1;color: #31bef8;margin-top: 20px;margin-bottom: 10px;font-size: 20px;">
                    Ciao<i style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">{{ $name }}
                        /i></h3>
                <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 16px;">
                    grazie per aver compilato il form su nausdream.com</p>
                <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 16px;">
                    Vivamus vitae feugiat metus, euismod sollicitudin libero. Cras cursus tellus massa, eu tempus risus
                    convallis quis. In hac habitasse platea dictumst</p>
                <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 16px;">
                    Mauris eu pulvinar justo, vitae iaculis nisl. Vivamus maximus bibendum eleifend. Ut dignissim
                    lobortis arcu quis suscipit. Praesent eget porta sem. Suspendisse potenti.</p>
                <div class="row"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
                    <div id="btn-cap" class="text-center"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;text-align: center;">
                        <a href="{{ env("APP_URL") }}/insert-captain/{{ $token }}"
                           style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: transparent;color: #337ab7;text-decoration: underline;">
                            <div class="butn col-md-4 col-md-offset-4"
                                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 33.33333333%;margin-left: 33.33333333%;font-size: 20px;font-weight: 600;font-style: normal;font-stretch: normal;color: #31bef8;padding-top: 46px;white-space: nowrap;margin-bottom: 80px;">
                                Diventa Capitano
                                <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/avanti-arrow.svg"
                                     alt=""
                                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;">
                            </div>
                        </a>
                    </div>
                </div>
                <!-- grey separator -->
                <div class="col-md-12 sep_0"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
                    <div class="sep_1"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #b2b2b2;border-radius: 100px;">
                    </div>
                </div>

                <h4 class="text-center"
                    style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: inherit;font-weight: 500;line-height: 1.1;color: inherit;margin-top: 10px;margin-bottom: 10px;font-size: 20px;text-align: center;padding-top: 20px;">
                    Per assistenza</h4>

                <div class="row"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
                    <div id="helper-1" class="col-md-6"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 50%;text-align: center;display: block;padding-top: 14px;padding-bottom: 20px;">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/mail-icon.svg"
                             alt=""
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;">
                        <h4 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: inherit;font-weight: 500;line-height: 1.1;color: inherit;margin-top: 10px;margin-bottom: 10px;font-size: 20px;padding-top: 20px;">
                            Scrivici</h4>

                        <div class="col-md-1 col-md-offset-5 bl_rect-c"
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 10px;width: 8.33333333%;margin-left: 41.66666667%;background-color: #fff;padding-top: 0px;">
                            <div class="blue_rec-c"
                                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;width: 38px;height: 3px;background-color: #fff;margin-top: 9px;border: 3px solid #31bef8;border-radius: 100px;">
                            </div>
                        </div>
                        <p id="help-email"
                           style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 18px;padding-top: 30px;">
                            assistenza@nausdream.com</p>
                    </div>

                    <div id="helper-2" class="col-md-6"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 50%;text-align: center;display: block;padding-top: 14px;padding-bottom: 20px;">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/phone-icon.svg"
                             alt=""
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;max-width: 100%!important;">
                        <h4 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;font-family: inherit;font-weight: 500;line-height: 1.1;color: inherit;margin-top: 10px;margin-bottom: 10px;font-size: 20px;padding-top: 20px;">
                            Chiamaci</h4>

                        <div class="col-md-1 col-md-offset-5 bl_rect-c"
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 10px;width: 8.33333333%;margin-left: 41.66666667%;background-color: #fff;padding-top: 0px;">
                            <div class="blue_rec-c"
                                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;width: 38px;height: 3px;background-color: #fff;margin-top: 9px;border: 3px solid #31bef8;border-radius: 100px;">
                            </div>
                        </div>
                        <p id="help-phone"
                           style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;font-size: 18px;padding-top: 30px;">
                            +39 06 404 028 60</p>
                    </div>
                </div>
                <!-- grey separator -->
                <div class="col-md-12 sep_0"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 0px;padding-left: 0px;width: 100%;">
                    <div class="sep_1"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;background-color: #fff;border: 1px solid #b2b2b2;border-radius: 100px;">
                    </div>
                </div>
                <p class="text-center"
                   style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;text-align: center;font-size: 16px;padding-top: 16px;">
                    Non rispondere a questa e-mail</p>
            </div>
        </div>
    </div>
    <div class="row"
         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;">
        <div class="col-md-8 col-md-offset-2"
             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 66.66666667%;margin-left: 16.66666667%;">
            <div id="fooot" class="row"
                 style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;margin-right: -15px;margin-left: -15px;background: #101c33;padding-left: 0px;margin: 0px;">
                <div id="copyright" class="col-md-6"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 20px;width: 50%;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;color: #ffffff;letter-spacing: .25px;line-height: 18px;padding-top: 35px;padding-bottom: 35px;">
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        Copyright © 2016 Nausdream. Tutti i diritti sono riservati.</p>
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        P.IVA: 03635840923</p>
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        Nausdream Srl</p>
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        LUISS EnLabs - Via Marsala 29H/I, 00185 Roma</p>
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        Open Campus di Tiscali - Loc. Sa Illetta S.S. 195, 09123 Cagliari</p>
                </div>

                <div id="social" class="col-md-6"
                     style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;position: relative;min-height: 1px;padding-right: 15px;padding-left: 15px;width: 50%;font-size: 14px;font-weight: normal;font-style: normal;font-stretch: normal;color: #ffffff;letter-spacing: .25px;line-height: 18px;padding-top: 35px;text-align: center;">
                    <p style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;orphans: 3;widows: 3;margin: 0 0 10px;">
                        Seguici</p>
                    <div id="social-icons"
                         style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding-top: 30px;">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/facebook-icon.svg"
                             alt=""
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;margin: 0 10px 0 10px;max-width: 100%!important;">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/istagram-icon.svg"
                             alt=""
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;margin: 0 10px 0 10px;max-width: 100%!important;">
                        <img src="https://res.cloudinary.com/naustrip/image/upload/v1478084912/assets/emails/twitter-icon.svg"
                             alt=""
                             style="-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border: 0;vertical-align: middle;page-break-inside: avoid;margin: 0 10px 0 10px;max-width: 100%!important;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>