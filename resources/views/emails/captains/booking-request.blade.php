<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{!! trans($translationAddress . 'subject', [], null, $language) !!} {{ $title }}</title>
    <link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf">


    <style type="text/css">
        /* GENERICO */

        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Montserrat", "Helvetica", Helvetica, Arial, sans-serif; }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }


        /* ELEMENTI VARI*/

        a { color: #31BEF8; text-decoration:none;}

        .btn {
            text-decoration:none;
            color: #31bef8;
            background-color: #fff;
            padding:10px 40px;
            margin-right:10px;
            font-size: 18px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
            border: solid 2px; color: #31bef8;
            border-radius: 100px;
        }

        .btn:hover {
            background-color: #31bef8;
            text-decoration:none;
            color: #fff;
        }


        .standard-btn {
            text-decoration:none;
            color: #fff;
            background-color: #31bef8;
            border-radius: 100px;
            padding:10px 40px;
            margin-right:10px;
            font-size: 18px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
            border: solid 2px;
        }

        .standard-btn:hover {

            background: rgba(48,189,248,0.60);
            border: 2px solid #31BEF8;
            text-decoration:none;
            color: #fff;
        }

        .calltoaction {
            padding:20px;
            background-color:#fff;
            margin-bottom: 15px;

        }

        #prenotazione {
            padding-top: 20px;
            background-color: #fff;
            border-top: solid 1px #D8D8D8;


        }

        #prenotazione-elenco {
            background-color: #fff;

        }

        .rettangolo {
            width:32px;
            height:4px;
            background:#31BEF8;
            border-radius: 100px;
            margin-bottom: 15px;
        }

        /* PREHEADER */

        table.head-wrap { width: 100%;}

        .header.container table { padding-top:10px; }



        /* BODY */

        table.body-wrap { width: 100%;}


        /* FOOTER */

        table.footer-wrap { width: 100%; background-color: #101C33; clear:both!important; padding-top: 0px;}

        .content-social { padding-top: 15px; padding-bottom: 15px;}


        /* CARATTERI */

        h1,h2,h3,h4,h5,h6 {
            font-family: "Montserrat-Light", "Helvetica", "Arial", "sans-serif"; line-height: 1.1; margin-bottom:15px; color:#5B5C59;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #5B5C59; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 44px;}
        h2 { font-family: "Montserrat-Regular"; font-weight:100; font-size: 26px;color:#31BEF8;}
        h3 { font-weight:100; font-size: 16px; font-style: italic; color:#31BEF8;}
        h4 { font-weight:500; font-size: 12px; color:#5B5C59;}
        h5 { font-weight:600; font-size: 18px;text-align: left;}
        h6 { font-weight:200; font-size: 11px; color:#31BEF8;}

        .bluetext { margin:0!important;}

        p {
            font-family: "Montserrat-Light";
            color: #5B5C59;
            margin-bottom: 10px;
            font-size:16px;
            line-height:1.6;
        }


        ul, li {
            font-family: "Montserrat-Light";
            color: #5B5C59;
            margin-bottom: 10px;
            margin-left: 10px;
            font-size:16px;
            line-height:1.6;
        }


        p.lead { font-size:16px; }

        p.headertext { font-size:14px; color: #fff; text-align: right}

        p.footertext { font-size:14px; color: #fff; }

        .prefooter { padding-top: 35px;
            border-top: solid 1px #D8D8D8;}


        /* CONTAINER */


        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important; /* per centrare */
            clear:both!important;

        }


        .content {
            padding:20px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }


        .contentheader {
            background-color: #101c33;
            padding:25px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }

        .content table { width: 100%; }

        .column  {
            width: 100%;
        }

        .column tr td { padding: 14px;}


        #prenotazione .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }


        .clear { display: block; clear: both; }

        /* Specifiche Font */


        @font-face {
            font-family: 'Montserrat-Light';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142575/production/assets/fonts/Montserrat-Light.ttf');

        }

        @font-face {
            font-family: 'Montserrat-Regular';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf');

        }



        /* TELEFONO */
        @media only screen and (max-width: 600px) {

            a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

            div[class="column"] { width: auto!important; float:none!important;}

            table #prenotazione div[class="column"] {
                width:auto!important;
            }


        }
    </style>
</head>

<body bgcolor="#FFFFFF">

<!-- PRE-HEADER -->
<table class="head-wrap" bgcolor="#EEEEEE">
    <tr>
        <td></td>
        <td class="header container" >

            <div class="content">
                <table bgcolor="#EEEEEE">
                    <tr>
                        <td align="left"><h4 class="bluetext">{!! trans($translationAddress . 'subject', [], null, $language) !!}</h4></td>
                        <!-- <td align="right"><h6 class="bluetext"><a href="#">View e-mail online</a></h6></td> -->
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table>
<!-- /PRE-HEADER -->


<!-- BODY -->
<table class="body-wrap"  bgcolor="#EEEEEE">
    <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">

            <!-- HEADER -->

            <div class="contentheader">
                <table bgcolor="#101c33" style="width:100%";>
                    <tr>
                        <td><img src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/logo_bianco.png" alt="nausdream_logo"/></td>
                        <td align="right"><p class="headertext">{!! trans($translationAddress . 'subject', [], null, $language) !!}<br>{!! trans($translationAddress . 'on', [ 'experience_date' => $experience_date ], null, $language) !!}</p></td>
                    </tr>
                </table>
            </div>
            <!-- HEADER -->

            <div class="content">
                <table >
                    <tr>
                        <td>
                            <h2>{!! trans($translationAddress . 'hi', [ 'name' => $captain_name ], null, $language) !!}</h2>
                            <p class="lead">{!! trans($translationAddress . 'interested', [ 'name' => $guest_name ], null, $language) !!} <a href="{!! env('FRONTEND_LINK') . '/' . $language . '/experiences/' . $slug_url !!}"><strong>{!! $title !!}</strong></a></p>
                            <p>
                                {!! trans($translationAddress . 'booking', [], null, $language) !!}
                                <br>
                                <a href="{!! env('FRONTEND_LINK') . '/' . $language . '/experiences/' . $slug_url !!}"><strong>{!! $title !!}</strong></a> <br> {!! $departure_port !!}</p>



                            <!-- DETTAGLIO PRENOTAZIONE -->

                            <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <h5>{!! trans($translationAddress . 'detail', [], null, $language) !!}</h5>
                                        <div class="rettangolo"></div>
                                        <!-- 1colonna sn -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                            <tr>
                                                <td style="padding: 0 0 10px 0;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="left"><p>{!! trans($translationAddress . 'request_date', [ 'date' => $request_date], null, $language) !!}</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /1colonna sn -->

                                        <!-- 2colonna dx -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                            <tr>
                                                <td style="padding: 0 0 10px 0;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="right"><p>{!! trans($translationAddress . 'adults', [ 'adults' => $adults], null, $language) !!}</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /2colonna dx -->

                                        <span class="clear"></span>

                                    </td>
                                </tr>
                            </table>



                            <!-- PRENOTAZIONE ELENCO -->

                            <table id="prenotazione-elenco" cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <td>

                                        <!-- 1colonna sn -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                            <tr>
                                                <td style="padding: 0 0 10px 0;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="left"><p>{!! trans($translationAddress . 'babies', [ 'babies' => $babies], null, $language) !!}</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /1colonna sn -->

                                        <!-- 2colonna dx -->
                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                            <tr>
                                                <td style="padding: 0 0 10px 0;">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="right"><p>{!! trans($translationAddress . 'kids', [ 'kids' => $kids], null, $language) !!}</p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- /2colonna dx -->


                                        <!-- PRENOTAZIONE ELENCO -->
                                        <table id="prenotazione-elenco" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td>

                                                    <!-- 1colonna sn -->
                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                        <tr>
                                                            <td style="padding: 0 0 10px 0;">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td align="left"><p>{!! trans($translationAddress . 'departure', [ 'time' => $departure_time], null, $language) !!}</p></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- /1colonna sn -->

                                                    <!-- 2colonna dx -->
                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                        <tr>
                                                            <td style="padding: 0 0 10px 0;">
                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td align="right"><p>{!! trans($translationAddress . 'arrival', [ 'time' => $arrival_time], null, $language) !!}</p></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- /2colonna dx -->


                                                    <!-- PRENOTAZIONE ELENCO -->
                                                    <table id="prenotazione-elenco" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td>

                                                                <!-- 1colonna sn -->
                                                                <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                    <tr>
                                                                        <td style="padding: 0 0 10px 0;">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td align="left"><p>{!! trans($translationAddress . 'from', [ 'port' => $departure_port], null, $language) !!}</p></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <!-- /1colonna sn -->

                                                                <!-- 2colonna dx -->
                                                                <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                    <tr>
                                                                        <td style="padding: 0 0 10px 0;">
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td align="right"><p>
                                                                                            @if (isset($arrival_port))
                                                                                                {!! trans($translationAddress . 'to', [ 'port' => $arrival_port], null, $language) !!}
                                                                                            @else
                                                                                                {!! trans($translationAddress . 'to', [ 'port' => $departure_port], null, $language) !!}
                                                                                            @endif
                                                                                        </p></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <!-- /2colonna dx -->


                                                                <!-- SERVIZI OPZIONALI -->

                                                                <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <!-- 1colonna sn -->
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                <tr>
                                                                                    <td style="padding: 0 0 10px 0;">
                                                                                        <h5>{!! trans($translationAddress . 'request_services', [], null, $language) !!}</h5>
                                                                                        <div class="rettangolo"></div>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td align="left"><ul>
                                                                                                        @foreach ($priced_services as $priced_service)
                                                                                                            @if ($priced_service['fixed'])
                                                                                                                <li>{!! trans($translationAddress . 'service', [ 'name' => trans('fixed-services.' . $priced_service['name'], [], null, $language), 'price' => $priced_service['price'], 'currency_symbol' => $currency_symbol ], null, $language) !!}</li>
                                                                                                            @else
                                                                                                                <li>{!! trans($translationAddress . 'service', [ 'name' => $priced_service['name'], 'price' => $priced_service['price'], 'currency_symbol' => $currency_symbol ], null, $language) !!}</li>
                                                                                                            @endif
                                                                                                        @endforeach
                                                                                                    </ul></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!-- /1colonna sn -->

                                                                            <!-- 2colonna dx -->
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                <tr>
                                                                                    <td style="padding: 0 0 10px 0;">
                                                                                        <h5>{!! trans($translationAddress . 'free_services', [], null, $language) !!}</h5>
                                                                                        <div class="rettangolo"></div>
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td align="left"><ul>
                                                                                                        @foreach ($free_services as $free_service)
                                                                                                            @if ($free_service['fixed'])
                                                                                                                <li>{{ trans('fixed-services.' . $free_service['name'], [], null, $language) }}</li>
                                                                                                            @else
                                                                                                                <li>{!! $free_service['name'] !!}</li>
                                                                                                            @endif
                                                                                                        @endforeach
                                                                                                    </ul></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!-- /2colonna dx -->

                                                                            <span class="clear"></span>

                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <!-- RIEPILOGO -->

                                                                <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <h5>{!! trans($translationAddress . 'summary', [], null, $language) !!}</h5>
                                                                            <div class="rettangolo"></div>
                                                                            <!-- 1colonna sn -->
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                <tr>
                                                                                    <td style="padding: 0 0 10px 0;">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td align="left"><p>{!! trans($translationAddress . 'payment', [ 'price' => $price_per_person, 'currency_symbol' => $currency_symbol, 'seats' => $total_seats ], null, $language) !!}</p></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!-- /1colonna sn -->

                                                                            <!-- 2colonna dx -->
                                                                            <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                <tr>
                                                                                    <td style="padding: 0 0 10px 0;">
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td align="right"><p>{!! trans($translationAddress . 'total', [ 'price' => $total_price_without_services, 'currency_symbol' => $currency_symbol ], null, $language) !!}</p></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!-- /2colonna dx -->


                                                                            <!-- PRENOTAZIONE ELENCO -->
                                                                            <table id="prenotazione-elenco" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                <tr>
                                                                                    <td>

                                                                                        <!-- 1colonna sn -->
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                            <tr>
                                                                                                <td style="padding: 0 0 10px 0;">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="left"><p>{!! trans($translationAddress . 'optional', [ 'total_seats' => $total_seats ], null, $language) !!}</p></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- /1colonna sn -->

                                                                                        <!-- 2colonna dx -->
                                                                                        <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                            <tr>
                                                                                                <td style="padding: 0 0 10px 0;">
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td align="right"><p>{!! trans($translationAddress . 'total', [ 'price' => $total_price_services, 'currency_symbol' => $currency_symbol ], null, $language) !!}</p></td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <!-- /2colonna dx -->


                                                                                        <!-- TOTALE -->

                                                                                        <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td>

                                                                                                    <!-- 1colonna sn -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="left"><h5>{!! trans($translationAddress . 'total_2', [], null, $language) !!}</h5></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /1colonna sn -->

                                                                                                    <!-- 2colonna dx -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="right"><p>{!! trans($translationAddress . 'total', [ 'price' => $total_price, 'currency_symbol' => $currency_symbol ], null, $language) !!}</p></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /2colonna dx -->

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>


                                                                                        <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <!-- 1colonna sn -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="left"><h5>{!! trans($translationAddress . 'nausdream_fee', [ 'fee' => $fee ], null, $language) !!}</h5></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /1colonna sn -->

                                                                                                    <!-- 2colonna dx -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="right"><p>{!! trans($translationAddress . 'total_fee', [ 'price' => $nausdream_fee, 'currency_symbol' => $currency_symbol ], null, $language) !!}</p></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /2colonna dx -->

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td>

                                                                                                    <!-- 1colonna sn -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="left"><h5>{!! trans($translationAddress . 'total_without_fee', [], null, $language) !!}</h5><div class="rettangolo"></div></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /1colonna sn -->

                                                                                                    <!-- 2colonna dx -->
                                                                                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                                        <tr>
                                                                                                            <td style="padding: 0 0 10px 0;">
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="right"><p>{!! trans($translationAddress . 'total', [ 'price' => $total_without_fee, 'currency_symbol' => $currency_symbol ], null, $language) !!}</p></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                    <!-- /2colonna dx -->

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>

                                                                                        <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                            <tr>
                                                                                                <td>

                                                                                                    <!-- CTA -->

                                                                                                    <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td>

                                                                                                                <!-- 1colonna sn -->
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
                                                                                                                    <tr>
                                                                                                                        <td style="padding: 0 0 10px 0;">
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td align="left"> <a href="{{ env('FRONTEND_LINK') . '/' . $language . '/bookings/' . $id . '/reject?token=' . $token }}"><span class="btn" >{!! trans($translationAddress . 'reject', [], null, $language) !!}</span></a></td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <!-- /1colonna sn -->

                                                                                                                <!-- 2colonna dx -->
                                                                                                                <table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
                                                                                                                    <tr>
                                                                                                                        <td style="padding: 0 0 10px 0;">
                                                                                                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td align="right"> <a href="{{ env('FRONTEND_LINK') . '/' . $language . '/bookings/' . $id . '/accept?token=' . $token }}"><span class="standard-btn">{!! trans($translationAddress . 'accept', [], null, $language) !!}</span></a></td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <!-- /2colonna dx -->

                                                                                                                <table id="prenotazione" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td align="centre">
                                                                                                                            <p>{!! trans($translationAddress . 'want', [], null, $language) !!}</p>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                                                                        <tr>
                                                                                                                            <td align="centre">
                                                                                                                                <a href="{{ env('FRONTEND_LINK') . '/' . $language . '/bookings/' . $id . '/change?token=' . $token }}">{!! trans($translationAddress . 'change', [], null, $language); !!}</a> <br> <br>
                                                                                                                            </td>
                                                                                                                        </tr>


                                                                                                                    </table>

                                                                                                                </table>

                                                                                                                <span class="clear"></span>

                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>

                                                                                                    <span class="clear"></span>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>



                                                                                        <!-- /PRENOTAZIONE -->

                                                                                        <!-- ASSISTENZA -->

                                                                                    @include('emails.support', ['language' => $language])

                                                                                    <!-- /ASSISTENZA -->

                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        <!-- PRE-FOOTER -->

                                                                        @include('emails.prefooter', ['language' => $language])

                                                                        <!-- /PRE-FOOTER -->
            </div>
            <!-- /content -->

        </td>
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    </tr>
</table>

<!-- /BODY -->



<!-- FOOTER -->

@include('emails.footer', ['language' => $language])

<!-- /FOOTER -->

</body>
</html>