<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">

<head style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">

    <meta name="viewport" content="width=device-width" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
    <title style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">{{ trans($translationAddress . 'subject', [], null, $language) }}</title>
    <link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">


    <style type="text/css" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        /* GENERICO */

        * {
            margin:0;
            padding:0;
        }
        * { font-family: "Montserrat", "Helvetica", Helvetica, Arial, sans-serif; }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width: 100%!important;
            height: 100%;
        }


        /* ELEMENTI VARI*/

        a { color: #31BEF8; text-decoration:none;}

        .btn {
            text-decoration:none;
            color: #31bef8;
            background-color: #fff;
            padding:10px 40px;
            font-weight:normal;
            margin-right:10px;
            font-size: 18px;
            text-align:center;
            cursor:pointer;
            display: inline-block;
            border: solid 2px; color: #31bef8;
            border-radius: 100px;
        }

        .btn:hover {
            background-color: #31bef8;
            text-decoration:none;
            color: #fff;
        }

        .calltoaction {
            padding:20px;
            background-color:#fff;
            margin-bottom: 15px;

        }


        #assistenza {
            padding-top: 20px;
            background-color: #fff;
            border-top: solid 1px #D8D8D8;
            border-bottom: solid 1px #D8D8D8;

        }




        .rettangolo {
            width:32px;
            height:4px;
            background:#31BEF8;
            border-radius: 100px;
            margin-bottom: 15px;
        }

        /* PREHEADER */

        table.head-wrap { width: 100%;}

        .header.container table { padding-top:10px; }



        .imgheader {
            margin-top: 0px;
        }

        /* BODY */

        table.body-wrap { width: 100%;}


        /* FOOTER */

        table.footer-wrap { width: 100%; background-color: #eee; clear:both!important; padding-top: 0px;}

        .content-social { background-color: #101C33; padding-top: 15px; padding-bottom: 15px;}

        /* CARATTERI */

        h1,h2,h3,h4,h5,h6 {
            font-family: "Montserrat-Light", "Helvetica", "Arial", "sans-serif"; line-height: 1.1; margin-bottom:15px; color:#5B5C59;
        }
        h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #5B5C59; line-height: 0; text-transform: none; }

        h1 { font-weight:200; font-size: 44px;}
        h2 { font-family: "Montserrat-Regular"; font-weight:100; font-size: 26px;color:#31BEF8;}
        h3 { font-weight:100; font-size: 16px; font-style: italic; color:#31BEF8;}
        h4 { font-weight:500; font-size: 12px; color:#5B5C59;}
        h5 { font-weight:600; font-size: 18px;text-align: center;}
        h6 { font-weight:200; font-size: 11px; color:#31BEF8;}

        .bluetext { margin:0!important;}

        p {
            font-family: "Montserrat-Light";
            color: #5B5C59;
            margin-bottom: 10px;
            font-size:16px;
            line-height:1.6;
        }
        p.lead { font-size:16px; }


        p.footertext { font-size:14px; color: #fff; }

        .prefooter { padding-top: 15px;}


        /* CONTAINER */


        .container {
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important; /* per centrare */
            clear:both!important;

        }


        .content {
            padding:10px;
            max-width:600px;
            margin:0 auto;
            display:block;
        }


        .content table { width: 100%; }

        .column  {
            width: 100%;
        }

        .column tr td { padding: 14px;}

        #assistenza .column {
            width: 280px;
            min-width: 279px;
            float:left;
        }


        .clear { display: block; clear: both; }


        /* Specifiche Font */


        @font-face {
            font-family: 'Montserrat-Light';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142575/production/assets/fonts/Montserrat-Light.ttf');

        }

        @font-face {
            font-family: 'Montserrat-Regular';
            src: url('https://res.cloudinary.com/naustrip/raw/upload/v1489142592/production/assets/fonts/Montserrat-Regular.ttf');

        }



        /* TELEFONO */
        @media only screen and (max-width: 600px) {

            a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

            div[class="column"] { width: auto!important; float:none!important;}

            table #assistenza div[class="column"] {
                width:auto!important;
            }

        }
    </style>
</head>

<body bgcolor="#FFFFFF" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;width: 100%!important;">

<!-- PRE-HEADER -->
<table class="head-wrap" bgcolor="#EEEEEE" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;">
    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
        <td class="header container" style="margin: 0 auto!important;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">

            <div class="content" style="margin: 0 auto;padding: 10px;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
                <table bgcolor="#EEEEEE" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;padding-top: 10px;">
                    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                        <td align="left" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"><h4 class="bluetext" style="margin: 0!important;padding: 0;font-family: &quot;Montserrat-Light&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, &quot;sans-serif&quot;;line-height: 1.1;margin-bottom: 15px;color: #5B5C59;font-weight: 500;font-size: 12px;">{{ trans($translationAddress . 'become_captain', [], null, $language) }}</h4></td>
                        <!-- <td align="right"><h6 class="bluetext"><a href="#">View e-mail online</a></h6></td> -->
                    </tr>
                </table>
            </div>

        </td>
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    </tr>
</table>
<!-- /PRE-HEADER -->


<!-- BODY -->
<table class="body-wrap" bgcolor="#EEEEEE" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;">
    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
        <td class="container" bgcolor="#FFFFFF" style="margin: 0 auto!important;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">

            <!-- HEADER -->

            <!-- HEADER -->

            <div class="content" style="margin: 0 auto;padding: 10px;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
                <table style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;">
                    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                            <h2 style="margin: 0;padding: 0;font-family: &quot;Montserrat-Regular&quot;;line-height: 1.1;margin-bottom: 15px;color: #31BEF8;font-weight: 100;font-size: 26px;">{{ trans($translationAddress . 'hi', ['name' => $name], null, $language) }}</h2>
                            <p class="lead" style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">{{ trans($translationAddress . 'thank', [], null, $language) }}</p>
                            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">{{ trans($translationAddress . 'please', [], null, $language) }}</p>


                            <!-- CALL TO ACTION -->

                            <table class="calltoaction" width="100%" style="margin: 0;padding: 20px;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;background-color: #fff;margin-bottom: 15px;width: 100%;">
                                <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                                    <td align="center" style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                                        <a href="{{ env('FRONTEND_LINK') . '/' . $language . '/new-captain?token=' . $token }}"><span class="btn" style="margin: 0;padding: 10px 40px;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;text-decoration: none;color: #31bef8;background-color: #fff;font-weight: normal;margin-right: 10px;font-size: 18px;text-align: center;cursor: pointer;display: inline-block;border: solid 2px;border-radius: 100px;">{{ trans($translationAddress . 'become_upper', [], null, $language) }}</span></a>
                                    </td>
                                </tr>
                            </table>

                            <!-- /CALL TO ACTION -->

                            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">{{ trans($translationAddress . 'will', [], null, $language) }}</p>

                            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">{{ trans($translationAddress . 'happy', [], null, $language) }}</p>
                            <h3 style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, &quot;sans-serif&quot;;line-height: 1.1;margin-bottom: 15px;color: #31BEF8;font-weight: 100;font-size: 16px;font-style: italic;"> <strong style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">{{ trans($translationAddress . 'team', [], null, $language) }}</strong></h3>

                            <!-- ASSISTENZA -->

                        @include('emails.support', ['language' => $language])

                        <!-- /ASSISTENZA -->

                        </td>
                    </tr>
                </table>

                <!-- PRE-FOOTER -->

            @include('emails.prefooter', ['language' => $language])

            <!-- /PRE-FOOTER -->
            </div>
            <!-- /content -->

        </td>
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    </tr>
</table>

<!-- /BODY -->



<!-- FOOTER -->

@include('emails.footer', ['language' => $language])

<!-- /FOOTER -->

</body>
</html>