<table class="footer-wrap"
       style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;background-color: #eee;padding-top: 0px;clear: both!important;">
    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
        <td class="container"
            style="margin: 0 auto!important;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">

            <!-- Social -->
            <div class="content-social" align="center"
                 style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;background-color: #101C33;padding-top: 15px;padding-bottom: 15px;">
                <table style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                        <td align="center"
                            style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                            <p class="footertext"
                               style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #fff;margin-bottom: 10px;font-size: 14px;line-height: 1.6;">
                                {{ trans('emails/footer.follow', [], null, $language) }}</p>
                            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">
                                <a href="{{ trans('emails/footer.facebook', [], null, $language) }}"
                                   style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color: #31BEF8;text-decoration: none;"><img
                                            src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/facebook_icon.png"
                                            alt="facebook"
                                            style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 100%;"></a>
                                <a href="{{ env('TWITTER_LINK') }}"
                                   style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color: #31BEF8;text-decoration: none;"><img
                                            src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/twitter__icon.png"
                                            style="padding-left: 20px;padding-right: 20px;margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 100%;"
                                            alt="twitter"></a>
                                <a href="{{ env('INSTAGRAM_LINK') }}"
                                   style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;color: #31BEF8;text-decoration: none;"><img
                                            src="https://res.cloudinary.com/naustrip/raw/upload/production/assets/img/istagram_icon.png"
                                            alt="instagram"
                                            style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 100%;"></a>
                            </p>
                            <p class="footertext"
                               style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #fff;margin-bottom: 10px;font-size: 14px;line-height: 1.6;">
                                {{ trans('emails/footer.srl', [], null, $language) }} <br
                                        style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"> &copy;
                                {{ trans('emails/footer.rights', [], null, $language) }}</p>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /social -->

        </td>
        <td style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
    </tr>
</table>