<table class="prefooter" width="100%"
       style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;padding-top: 35px;border-top: solid 1px #D8D8D8;width: 100%;">
    <tr style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
        <td class="container" align="center"
            style="margin: 0 auto!important;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;display: block!important;max-width: 600px!important;clear: both!important;">
            <h5 style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, &quot;sans-serif&quot;;line-height: 1.1;margin-bottom: 15px;color: #5B5C59;font-weight: 600;font-size: 18px;text-align: center;">
                {{ trans('emails/footer.assistance', [], null, $language) }}</h5>
            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">
                <strong style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">{{ trans('emails/footer.write', [], null, $language) }}</strong> {{ env('SUPPORT_EMAIL') }}</p>
            <p style="margin: 0;padding: 0;font-family: &quot;Montserrat-Light&quot;;color: #5B5C59;margin-bottom: 10px;font-size: 16px;line-height: 1.6;">
                <strong style="margin: 0;padding: 0;font-family: &quot;Montserrat&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">{{ trans('emails/footer.call', [], null, $language) }}</strong> {{ env('NAUSDREAM_TELEPHONE') }}</p>

        </td>
    </tr>
</table>