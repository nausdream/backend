<?php
/**
 * User: Giuseppe
 * Date: 20/07/2017
 * Time: 14:46
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('pdf_eden_voucher', 'it');