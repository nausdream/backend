<?php
/**
 * User: Giuseppe
 * Date: 14/02/2017
 * Time: 16:13
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_password_restore', 'it');
