<?php
/**
 * User: Giuseppe
 * Date: 31/01/2017
 * Time: 12:26
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_new_message_guest', 'it');