<?php
/**
 * User: Giuseppe
 * Date: 09/03/2017
 * Time: 20:37
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_footer', 'it');