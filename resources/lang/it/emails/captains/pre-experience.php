<?php
/**
 * User: Giuseppe
 * Date: 27/04/2017
 * Time: 19:39
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_pre_experience_captain', 'it');