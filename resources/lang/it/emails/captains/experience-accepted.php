<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 31/10/2016
 * Time: 16:51
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_experience_accepted', 'it');
