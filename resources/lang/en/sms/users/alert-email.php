<?php
/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 13:26
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('sms_alert_email', 'en');