<?php
/**
 * User: Giuseppe
 * Date: 24/04/2017
 * Time: 18:29
 */

use App\Http\Controllers\v1\TranslationsController;

return TranslationsController::getTranslations('emails_booking_rejected', 'en');