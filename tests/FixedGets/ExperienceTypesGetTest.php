<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperienceTypesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/experience-types';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_experience_types()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $experienceTypes = \App\Models\ExperienceType::all();
        foreach ($experienceTypes as $experienceType) {
            $this->seeJson(['id' => (string) $experienceType->id]);
            $this->seeJson(['name' => $experienceType->name]);
        }
    }
}