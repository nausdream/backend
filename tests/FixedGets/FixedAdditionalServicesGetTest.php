<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class FixedAdditionalServicesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/fixed-additional-services';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_fixed_additional_services()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all();
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $this->seeJson(['id' => (string) $fixedAdditionalService->id]);
            $this->seeJson(['name' => $fixedAdditionalService->name]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_specific_experience_type_fixed_additional_services()
    {
        $experienceTypes = \App\Models\ExperienceType::all();

        foreach ($experienceTypes as $experienceType)
        {
            //act
            $this->getJson($this->getRouteExperienceType($experienceType->name));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $responseContent = json_decode($this->response->getContent());
            $this->assertNotEquals($responseContent->data, []);
            $fixedAdditionalServices = $experienceType->fixedAdditionalServices()->get();

            foreach ($fixedAdditionalServices as $fixedAdditionalService) {
                $this->seeJson(['id' => (string) $fixedAdditionalService->id]);
                $this->seeJson(['name' => $fixedAdditionalService->name]);
            }
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_not_specific_experience_type_fixed_additional_services()
    {
        $experienceTypes = \App\Models\ExperienceType::all();

        foreach ($experienceTypes as $experienceType)
        {
            //act
            $this->getJson($this->getRouteNotExperienceType($experienceType->name));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $responseContent = json_decode($this->response->getContent());
            $this->assertNotEquals($responseContent->data, []);
            $fixedAdditionalServices = $experienceType->fixedAdditionalServices()->get();

            foreach ($fixedAdditionalServices as $fixedAdditionalService) {
                $this->dontSeeJson(['id' => (string) $fixedAdditionalService->id]);
                $this->dontSeeJson(['name' => $fixedAdditionalService->name]);
            }
        }
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_type_specified_does_not_exist()
    {
        //act
        $this->getJson($this->getRouteExperienceType('pippo'));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_404_if_not_experience_type_specified_does_not_exist()
    {
        //act
        $this->getJson($this->getRouteNotExperienceType('pippo'));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    protected function getRouteExperienceType($experienceType)
    {
        return $this->route . '?type=' . $experienceType;
    }

    protected function getRouteNotExperienceType($experienceType)
    {
        return $this->route . '?not_type=' . $experienceType;
    }
}