<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CaptainTypesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/captain-types';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_captain_types()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $captainTypes = \App\Models\CaptainType::all();
        foreach ($captainTypes as $captainType) {
            $this->seeJson(['id' => (string) $captainType->id]);
            $this->seeJson(['name' => $captainType->name]);
        }
    }
}