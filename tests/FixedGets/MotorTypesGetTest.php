<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class MotorTypesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/motor-types';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_motor_types()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $motorTypes = \App\Models\MotorType::all();
        foreach ($motorTypes as $motorType) {
            $this->seeJson(['id' => (string) $motorType->id]);
            $this->seeJson(['name' => $motorType->name]);
        }
    }
}