<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CountriesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/countries';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_countries()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $countries = \App\Models\Country::all();
        foreach ($countries as $country) {
            $this->seeJson(['id' => (string) $country->id]);
            $this->seeJson(['name' => $country->name]);
        }
    }
}