<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class AccessoriesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/accessories';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_accessories()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $accessories = \App\Models\Accessory::all();
        foreach ($accessories as $accessory) {
            $this->seeJson(['id' => (string) $accessory->id]);
            $this->seeJson(['name' => $accessory->name]);
        }
    }
}