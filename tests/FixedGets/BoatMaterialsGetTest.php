<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatMaterialsGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/boat-materials';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_boat_materials()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $boatMaterials = \App\Models\BoatMaterial::all();
        foreach ($boatMaterials as $boatMaterial) {
            $this->seeJson(['id' => (string) $boatMaterial->id]);
            $this->seeJson(['name' => $boatMaterial->name]);
        }
    }
}