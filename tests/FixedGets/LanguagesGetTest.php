<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class LanguagesGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/languages';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_languages()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $languages = \App\TranslationModels\Language::all();
        foreach ($languages as $language) {
            $this->seeJson(['id' => (string) $language->id]);
            $this->seeJson(['language' => $language->language]);
        }
    }
}