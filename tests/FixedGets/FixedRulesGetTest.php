<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class FixedRulesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/fixed-rules';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_fixed_rules()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $rules = \App\Models\Rule::all();
        foreach ($rules as $rule) {
            $this->seeJson(['id' => (string) $rule->id]);
            $this->seeJson(['name' => $rule->name]);
        }
    }
}