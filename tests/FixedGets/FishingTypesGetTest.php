<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class FishingTypesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/fishing-types';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_fishing_types()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $fishingTypes = \App\Models\FishingType::all();
        foreach ($fishingTypes as $fishingType) {
            $this->seeJson(['id' => (string) $fishingType->id]);
            $this->seeJson(['name' => $fishingType->name]);
        }
    }
}