<?php

/**
 * User: Giuseppe
 * Date: 08/02/2017
 * Time: 15:34
 */

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CurrenciesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route = 'v1/currencies';

    /**
     * @test
     */
    public function it_returns_200_and_list_of_the_all_currencies()
    {
        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $currencies = \App\Models\Currency::all();
        foreach ($currencies as $currency) {
            $this->seeJson(['id' => (string) $currency->id]);
            $this->seeJson(['name' => $currency->name]);
        }
    }
}