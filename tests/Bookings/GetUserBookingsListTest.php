<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class GetUserBookingsListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $type;
    private $user;
    private $admin;
    private $bookings;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'bookings';
    }

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->user->id), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->user); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($this->user->id), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->user->id), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_user_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeId = App\Models\User::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($fakeId), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'user_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_the_same()
    {
        //arrange
        $this->setThingsUp();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($otherUser));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_bookings_to_guest()
    {
        //arrange
        $this->setThingsUp();
        $bookings = $this->user->bookings()
            ->orderBy('id', 'desc')
            ->orderBy('updated_at', 'desc')->get()->slice(0, Constant::BOOKINGS_PER_PAGE)
        ;

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        foreach ($bookings as $booking) {
            $experienceVersion = $booking->experienceVersion()->first();
            $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
            $guest = $booking->user()->first();
            $this->seeJson(['id' => (string) $booking->id]);
            $this->seeJson(['experience_date' => $booking->experience_date]);
            $this->seeJson(['experience_title' => $experienceVersion->title]);
            $this->seeJson(['first_name_captain' => $captain->first_name]);
            $this->seeJson(['last_name_captain' => $captain->last_name]);
            $this->seeJson(['first_name_guest' => $guest->first_name]);
            $this->seeJson(['last_name_guest' => $guest->last_name]);
            $this->seeJson(['status' => Constant::BOOKING_STATUS_NAMES[$booking->status]]);
        }
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_bookings_to_admin()
    {
        //arrange
        $this->setThingsUp();
        $bookings = $this->user->bookings()
            ->orderBy('id', 'desc')
            ->orderBy('updated_at', 'desc')->get()->slice(0, Constant::BOOKINGS_PER_PAGE)
        ;

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        foreach ($bookings as $booking) {
            $experienceVersion = $booking->experienceVersion()->first();
            $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
            $guest = $booking->user()->first();
            $this->seeJson(['id' => (string) $booking->id]);
            $this->seeJson(['experience_date' => $booking->experience_date]);
            $this->seeJson(['experience_title' => $experienceVersion->title]);
            $this->seeJson(['first_name_captain' => $captain->first_name]);
            $this->seeJson(['last_name_captain' => $captain->last_name]);
            $this->seeJson(['first_name_guest' => $guest->first_name]);
            $this->seeJson(['last_name_guest' => $guest->last_name]);
            $this->seeJson(['status' => Constant::BOOKING_STATUS_NAMES[$booking->status]]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_bookings()
    {
        //arrange
        $this->setThingsUp();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($otherUser->id), $this->getHeaders($otherUser));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertEquals($responseContent->data, []);
    }


    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $user->save();
        $this->user = $user;
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });
        $this->bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $user) {
                $e->user()->associate($user);
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->save();
            });
    }

    public function getRoute($id)
    {
        return 'v1/users/' . $id . '/bookings';
    }


}