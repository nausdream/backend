<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BookingPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $type;
    private $user;
    private $captain;
    private $boat;
    private $experience;
    private $conversation;
    private $fixedAdditionalServicePrices;
    private $additionalServices;
    private $period;
    private $experienceVersion;

    /**
     * ConversationsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/bookings';
        $this->type = 'bookings';
    }


    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getRequestBody(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->user); // Retrieves the generated token

        //act
        $this->postJson($this->route, $this->getRequestBody(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->getRequestBody(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_fields_do_not_pass_validation()
    {
        //experience_date
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['experience_date'] = null;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'experience_date']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['experience_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'experience_date']);
        $this->seeJson(['code' => 'dateformat']);

        //adults
        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['adults'] = null;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'adults']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['adults'] = -1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'adults']);
        $this->seeJson(['code' => 'min']);

        //kids
        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['kids'] = null;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'kids']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['kids'] = -1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'kids']);
        $this->seeJson(['code' => 'min']);

        //babies
        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['babies'] = null;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'babies']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['babies'] = -1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'babies']);
        $this->seeJson(['code' => 'min']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_both_experience_and_conversation_id_are_not_set()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_or_conversation_required']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_experience_or_conversation_relationships_has_the_wrong_type()
    {
        //experience
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['experience']['data']['type'] = 'not_experiences';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'in']);
        $this->seeJson(['title' => 'type']);

        //conversation
        //arrange
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $requestBody['data']['relationships']['conversation']['data']['id'] = $conversation->id;
        $requestBody['data']['relationships']['conversation']['data']['type'] = 'not_conversations';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'in']);
        $this->seeJson(['title' => 'type']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['experience']['data']['id'] = \App\Models\Experience::all()->last()->id + 1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_have_a_finished_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $requestBody['data']['relationships']['experience']['data']['id'] = $experience->id;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_if_last_experience_version_is_rejected_or_freezed()
    {
        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_REJECTED;
        $this->experience->experienceVersions()->save($experienceVersion);

        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);

        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_FREEZED;
        $this->experience->experienceVersions()->save($experienceVersion);

        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_have_a_last_finished_experience_accepted()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_PENDING;
        $experience->experienceVersions()->save($experienceVersion);
        $requestBody['data']['relationships']['experience']['data']['id'] = $experience->id;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_conversation_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $lastConversation = \App\Models\Conversation::all()->last();
        $id = isset($lastConversation) ? $lastConversation->id + 1 : 1;
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $id,
                    'type' => 'conversations'
                ]
            ]
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'conversation_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_of_conversation_does_not_have_a_finished_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $conversation->experience_id = $experience->id;
        $conversation->save();
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $conversation->id,
                    'type' => 'conversations'
                ]
            ]
        ];


        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_of_conversation_if_last_experience_version_is_rejected_or_freezed()
    {
        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_REJECTED;
        $this->experience->experienceVersions()->save($experienceVersion);
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $conversation->experience_id = $this->experience->id;
        $conversation->save();
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $conversation->id,
                    'type' => 'conversations'
                ]
            ]
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);

        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_FREEZED;
        $this->experience->experienceVersions()->save($experienceVersion);
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $conversation->experience_id = $this->experience->id;
        $conversation->save();
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $conversation->id,
                    'type' => 'conversations'
                ]
            ]
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_of_conversation_does_not_have_a_last_finished_experience_accepted()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_PENDING;
        $experience->experienceVersions()->save($experienceVersion);
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $conversation->experience_id = $experience->id;
        $conversation->save();
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $conversation->id,
                    'type' => 'conversations'
                ]
            ]
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_the_fixed_additional_services_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['fixed_additional_service_prices']['data'][] = [
            'id' => \App\Models\ExperienceVersionsFixedAdditionalServices::all()->last()->id + 1,
            'type' => 'fixed-additional-service-prices'
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'fixed_service_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_fixed_additional_services_has_the_wrong_type()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['fixed_additional_service_prices']['data'][0]['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'in']);
        $this->seeJson(['title' => 'type']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_the_additional_service_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['additional_services']['data'][] = [
            'id' => \App\Models\AdditionalService::all()->last()->id + 1,
            'type' => 'additional-services'
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'additional_service_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_additional_services_has_the_wrong_type()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['relationships']['additional_services']['data'][0]['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'in']);
        $this->seeJson(['title' => 'type']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_experience_date_is_not_available()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['experience_date'] = \Carbon\Carbon::today()->addMonth(2)->format('Y-m-d');

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_day_of_the_week_is_not_available()
    {
        //arrange
        $this->setThingsUp();
        $experienceVersion = $this->experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = false;
        $experienceVersion->save();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['experience_date'] = \Carbon\Carbon::parse('next monday')->format('Y-m-d');

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_there_is_not_a_period_with_that_date_available()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['experience_date'] = \Carbon\Carbon::parse('this monday')->addWeek(5)->format('Y-m-d');

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_seats_kids_or_babies_are_too_much_or_too_few()
    {
        //seats
        //arrange
        $this->setThingsUp();
        $this->period->min_person = 2;
        $this->period->save();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['adults'] = $this->period->min_person - 1;
        $requestBody['data']['attributes']['kids'] = 0;
        $requestBody['data']['attributes']['babies'] = 0;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'seats']);

        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['adults'] = $this->experienceVersion->seats + 1;
        $requestBody['data']['attributes']['kids'] = 0;
        $requestBody['data']['attributes']['babies'] = 0;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'seats']);

        //kids
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['kids'] = $this->experienceVersion->kids + 1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'kids']);

        //babies
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['babies'] = $this->experienceVersion->babies + 1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'babies']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_there_is_already_a_booking_request_not_cancelled_or_rejected_for_this_experience_version_this_date_and_this_user()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $booking = new \App\Models\Booking([
            'experience_date' => \Carbon\Carbon::parse('next monday')->format('Y-m-d'),
            'request_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'status' => Constant::BOOKING_STATUS_PENDING,
            'price' => 100,
            'adults' => 10,
            'kids' => 0,
            'babies' => 0,
            'coupon' => null,
            'currency' => 'EUR',
            'entire_boat' => true
        ]);

        $booking->user_id = $this->user->id;
        $booking->experience_version_id = $this->experienceVersion->id;
        $booking->save();

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'booking_already_exists']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_the_guest_is_the_captain_of_the_experience()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['coupon'] = 'not_a_coupon';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_consumed()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = true;
        $coupon->save();
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_expired()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->expiration_date = \Carbon\Carbon::today()->subDay(1)->format('Y-m-d');
        $coupon->is_consumed = false;
        $coupon->save();
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_users_but_not_this_one()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = false;
        $coupon->save();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();
        $coupon->users()->attach($otherUser->id, ['is_coupon_consumed' => false]);
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_assigned_to_user_but_it_is_already_consumed()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $coupon->users()->attach($this->user->id, ['is_coupon_consumed' => true]);
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_another_experience()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $coupon->experience_id = $experience->id;
        $coupon->save();
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon_experience']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_an_area_and_experience_version_is_outside()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $area = factory(\App\Models\Area::class)->create();
        $coupon->area_id = $area->id;
        $coupon->save();
        $this->experienceVersion->departure_lat = $area->point_b_lat + 5;
        $this->experienceVersion->departure_lng = $area->point_b_lng + 5;
        $this->experienceVersion->save();
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon_area']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_201_and_create_the_booking_request_if_all_the_information_provided_are_ok()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertFalse($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PENDING, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_201_and_create_the_booking_request_if_all_the_information_provided_are_ok_for_conversation()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        unset($requestBody['data']['relationships']['experience']);
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create();
        $conversation->experience_id = $this->experience->id;
        $conversation->save();
        $requestBody['data']['relationships'] = [
            'conversation' => [
                'data' => [
                    'id' => $conversation->id,
                    'type' => 'conversations'
                ]
            ]
        ];

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertFalse($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PENDING, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_201_and_create_the_booking_request_if_all_the_information_provided_are_ok_with_coupon()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = false;
        $coupon->save();
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $couponConsumed = \App\Models\Coupon::find($coupon->id);
        $this->assertTrue($couponConsumed->is_consumed);
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertFalse($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PENDING, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_201_and_create_the_booking_request_if_all_the_information_provided_are_ok_with_coupon_for_user()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = false;
        $coupon->save();
        $coupon->users()->attach($this->user->id, ['is_coupon_consumed' => false]);
        $requestBody['data']['attributes']['coupon'] = $coupon->name;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $couponUser = $coupon->couponsUsers()->where('user_id', $this->user->id)->first();
        $this->assertTrue($couponUser->is_coupon_consumed);
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertFalse($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PENDING, $booking->status);
    }


    // HELPERS

    protected function setThingsUp()
    {
        // Create data

        // user
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->user = $user;

        // captain
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $this->captain = $captain;

        // boat
        $boat = factory(\App\Models\Boat::class)
            ->make();
        $boat->user()->associate($captain);
        $boat->save();
        $this->boat = $boat;
        $boat->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());

        // experience
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($boat);
        $experience->save();
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $period = factory(\App\Models\Period::class)->states('dummy')->make();
        $period->date_start = \Carbon\Carbon::parse('this monday')->format('Y-m-d');
        $period->date_end = \Carbon\Carbon::parse('this monday')->addWeek(4)->format('Y-m-d');
        $period->is_default = false;
        $experience->periods()->save($period);
        $this->period = $period;
        if (!$period->entire_boat) {
            $price = factory(\App\Models\Price::class)->states('dummy')->make();
            $price->person = 1;
            $period->prices()->save($price);
        }

        $experienceVersion = $experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = true;
        $experienceVersion->save();
        $this->experienceVersion = $experienceVersion;

        $this->fixedAdditionalServicePrices = [];
        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all()->take(10);
        $i = 0;
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $this->fixedAdditionalServicePrices[$i] = new \App\Models\ExperienceVersionsFixedAdditionalServices([
                "price" => $this->fake->numberBetween(0, 200),
                "per_person" => $this->fake->boolean,
                "currency" => $this->captain->currency
            ]);
            $this->fixedAdditionalServicePrices[$i]->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $this->fixedAdditionalServicePrices[$i]->fixed_additional_service_id = $fixedAdditionalService->id;
            $this->fixedAdditionalServicePrices[$i]->save();
            $i++;
        }

        $this->additionalServices = [];
        $additionalServices = factory(\App\Models\AdditionalService::class, 10)->make();
        $i = 0;
        foreach ($additionalServices as $additionalService) {
            $additionalService->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $additionalService->currency = $captain->currency;
            $this->additionalServices[$i] = $additionalService;
            $this->additionalServices[$i]->save();
            $i++;
        }

        $this->experience = $experience;

        //seo

        $seoController = new \App\Http\Controllers\v1\SeosController();

        $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);
    }


    protected function getRequestBody()
    {
        $requestBody = [
            'data' => [
                'type' => 'bookings',
                'attributes' => [
                    'experience_date' => \Carbon\Carbon::parse('next monday')->format('Y-m-d'),
                    'adults' => $this->fake->numberBetween($this->period->min_person, $this->experienceVersion->seats),
                    'kids' => 0,
                    'babies' => 0,
                    'coupon' => null,
                ],
                'relationships' => [
                    'experience' => [
                        'data' => [
                            'id' => $this->experience->id,
                            'type' => 'experiences'
                        ]
                    ],
                ]
            ]
        ];

        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $requestBody['data']['relationships']['fixed_additional_service_prices']['data'][] = [
                'id' => $fixedAdditionalServicePrice->id,
                'type' => 'fixed-additional-service-prices'
            ];
        }

        foreach ($this->additionalServices as $additionalService) {
            $requestBody['data']['relationships']['additional_services']['data'][] = [
                'id' => $additionalService->id,
                'type' => 'additional-services'
            ];
        }

        return $requestBody;
    }
}