<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class GetSingleBookingTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $type;
    private $user;
    private $captain;
    private $admin;
    private $bookings;
    private $route;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'bookings';
        $this->route = 'v1/bookings/';
    }

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route . $this->bookings->random()->id, []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->user); // Retrieves the generated token

        //act
        $this->getJson($this->route . $this->bookings->random()->id, ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->route . $this->bookings->random()->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_booking_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeId = App\Models\Booking::all()->last()->id + 1;

        //act
        $this->getJson($this->route . $fakeId, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'booking_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->route . $this->bookings->random()->id, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_the_same_of_the_booking()
    {
        //arrange
        $this->setThingsUp();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->route . $this->bookings->random()->id, $this->getHeaders($otherUser));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_200_and_the_right_booking_if_guest_request_it()
    {
        //arrange
        $this->setThingsUp();
        $booking = $this->bookings->random();

        //act
        $this->getJson($this->route . $booking->id, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['id' => (string) $booking->id]);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['experience_date' => $booking->experience_date]);
        $this->seeJson(['adults' => $booking->adults]);
        $this->seeJson(['kids' => $booking->kids]);
        $this->seeJson(['babies' => $booking->babies]);
        $this->seeJson(['coupon' => $booking->coupon]);
        $this->seeJson(['entire_boat' => $booking->entire_boat]);
        $this->seeJson(['price' => $booking->price]);
        $this->seeJson(['acceptance_date' => $booking->acceptance_date]);
        $this->seeJson(['status' => Constant::BOOKING_STATUS_NAMES[$booking->status]]);
        $this->seeJson(['request_date' => $booking->request_date]);
        $this->seeJson(['discount' => $booking->discount]);
        $this->seeJson(['price_on_board' => $booking->price_on_board]);
        $this->seeJson(['price_to_pay' => $booking->price_to_pay]);
        $this->seeJson(['fee' => $booking->fee]);
        $this->seeJson(['currency' => $booking->currency]);

        $experienceVersion = $booking->experienceVersion()->first();
        $currencyOut = isset($booking->currency) ? $booking->currency : $experienceVersion->experience()->first()->boat()->user()->first()->currency;

        // Fixed additional services

        $fixedAdditionalServices = $booking->fixedAdditionalServices()->get();
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = App\Models\ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            $this->seeJson(['type' => 'fixed-additional-service-prices']);
            $this->seeJson(['id' => (string) $fixedServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalService->name]);
            $this->seeJson(['per_person' => $fixedServicePrice->per_person]);
        }

        // Additional services

        $additionalServices = $booking->additionalServices()->get();
        foreach ($additionalServices as $additionalService) {
            $this->seeJson(['type' => 'additional-services']);
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_the_right_booking_if_captain_request_it()
    {
        //arrange
        $this->setThingsUp();
        $booking = $this->bookings->random();

        //act
        $this->getJson($this->route . $booking->id, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['id' => (string) $booking->id]);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['experience_date' => $booking->experience_date]);
        $this->seeJson(['adults' => $booking->adults]);
        $this->seeJson(['kids' => $booking->kids]);
        $this->seeJson(['babies' => $booking->babies]);
        $this->seeJson(['coupon' => $booking->coupon]);
        $this->seeJson(['entire_boat' => $booking->entire_boat]);
        $this->seeJson(['price' => $booking->price]);
        $this->seeJson(['acceptance_date' => $booking->acceptance_date]);
        $this->seeJson(['status' => Constant::BOOKING_STATUS_NAMES[$booking->status]]);
        $this->seeJson(['request_date' => $booking->request_date]);
        $this->seeJson(['discount' => $booking->discount]);
        $this->seeJson(['price_on_board' => $booking->price_on_board]);
        $this->seeJson(['price_to_pay' => $booking->price_to_pay]);
        $this->seeJson(['fee' => $booking->fee]);
        $this->seeJson(['currency' => $booking->currency]);

        $experienceVersion = $booking->experienceVersion()->first();
        $currencyOut = isset($booking->currency) ? $booking->currency : $experienceVersion->experience()->first()->boat()->user()->first()->currency;

        // Fixed additional services

        $fixedAdditionalServices = $booking->fixedAdditionalServices()->get();
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = App\Models\ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            $this->seeJson(['type' => 'fixed-additional-service-prices']);
            $this->seeJson(['id' => (string) $fixedServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalService->name]);
            $this->seeJson(['per_person' => $fixedServicePrice->per_person]);
        }

        // Additional services

        $additionalServices = $booking->additionalServices()->get();
        foreach ($additionalServices as $additionalService) {
            $this->seeJson(['type' => 'additional-services']);
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_the_right_booking_if_admin_request_it()
    {
        //arrange
        $this->setThingsUp();
        $booking = $this->bookings->random();

        //act
        $this->getJson($this->route . $booking->id, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['id' => (string) $booking->id]);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['experience_date' => $booking->experience_date]);
        $this->seeJson(['adults' => $booking->adults]);
        $this->seeJson(['kids' => $booking->kids]);
        $this->seeJson(['babies' => $booking->babies]);
        $this->seeJson(['coupon' => $booking->coupon]);
        $this->seeJson(['entire_boat' => $booking->entire_boat]);
        $this->seeJson(['price' => $booking->price]);
        $this->seeJson(['acceptance_date' => $booking->acceptance_date]);
        $this->seeJson(['status' => Constant::BOOKING_STATUS_NAMES[$booking->status]]);
        $this->seeJson(['request_date' => $booking->request_date]);
        $this->seeJson(['discount' => $booking->discount]);
        $this->seeJson(['price_on_board' => $booking->price_on_board]);
        $this->seeJson(['price_to_pay' => $booking->price_to_pay]);
        $this->seeJson(['fee' => $booking->fee]);
        $this->seeJson(['currency' => $booking->currency]);

        $experienceVersion = $booking->experienceVersion()->first();
        $currencyOut = isset($booking->currency) ? $booking->currency : $experienceVersion->experience()->first()->boat()->user()->first()->currency;

        // Fixed additional services

        $fixedAdditionalServices = $booking->fixedAdditionalServices()->get();
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = App\Models\ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            $this->seeJson(['type' => 'fixed-additional-service-prices']);
            $this->seeJson(['id' => (string) $fixedServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalService->name]);
            $this->seeJson(['per_person' => $fixedServicePrice->per_person]);
        }

        // Additional services

        $additionalServices = $booking->additionalServices()->get();
        foreach ($additionalServices as $additionalService) {
            $this->seeJson(['type' => 'additional-services']);
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }
    }

    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $user->save();
        $this->user = $user;
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $this->captain = $captain;
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captain) {
                $b->user()->associate($captain);
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        // Additional Services
        $additional_service = new \App\Models\AdditionalService(
            [
                'name' => str_random(Constant::ADDITIONAL_SERVICE_LENGTH),
                'price' => $this->fake->numberBetween(0, 1000),
                'per_person' => $this->fake->boolean,
                'currency' => \App\Models\Currency::all()->random()->name
            ]
        );
        // Fixed Additional Services
        $fixed_additional_service = \App\Models\FixedAdditionalService::all()->random();
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });
        $this->bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $user, $fixed_additional_service, $additional_service) {
                $e->user()->associate($user);
                $experienceVersion = $experiences->random()->experienceVersions()->get()->where('is_finished', true)->last();
                $experienceVersion->additionalServices()->save($additional_service);
                $experienceVersion->fixedAdditionalServices()->attach(
                    $fixed_additional_service->id,
                    [
                        'price' => $this->fake->numberBetween(0, 100),
                        'per_person' => $this->fake->boolean,
                        'currency' => \App\Models\Currency::all()->random()->name
                    ]
                );
                $e->experienceVersion()->associate($experienceVersion);
                $e->fixedAdditionalServices()->attach([$fixed_additional_service->id]);
                $e->additionalServices()->attach([$additional_service->id]);
                $e->save();
            });
    }

}