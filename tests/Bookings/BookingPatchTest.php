<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use App\Services\PriceHelper;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BookingPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $type;
    private $user;
    private $captain;
    private $boat;
    private $experience;
    private $conversation;
    private $fixedAdditionalServicePrices;
    private $additionalServices;
    private $period;
    private $experienceVersion;
    private $booking;

    /**
     * ConversationsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/bookings/';
        $this->type = 'bookings';
    }


    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(), $this->getRequestBody(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->captain); // Retrieves the generated token

        //act
        $this->patchJson($this->getRoute(), $this->getRequestBody(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->patchJson($this->getRoute(), $this->getRequestBody(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_fields_do_not_pass_validation()
    {
        //experience_date
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['status'] = null;

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'status']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['data']['attributes']['status'] = 'pending';

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'status']);
        $this->seeJson(['code' => 'in']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_the_booking_experience_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->patchJson($this->getRoute(\App\Models\Booking::all()->last()->id + 1), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'booking_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_booking_is_not_in_pending_status()
    {
        //experience
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $this->booking->status = Constant::STATUS_REJECTED;
        $this->booking->save();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'booking_not_pending']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_the_user_that_it_is_answering_is_not_the_captain()
    {
        //experience
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_booking_is_acceppted_but_price_does_not_pass_the_validation()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody('accepted', 9);

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody('accepted', null);

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $this->setThingsUp();

        // Get included resources
        // Set seats to calculate price of services and initialize services price
        $seats = $this->booking->adults + $this->booking->babies + $this->booking->kids;
        $services_price = 0;

        // Get fixed additional service prices array

        $fixedServices = $this->booking->fixedAdditionalServices()->get();
        $experienceVersion = $this->booking->experienceVersion()->first();
        $fixed_additional_service_prices = [];
        foreach ($fixedServices as $fixedService) {
            $fixedServicePriceObject = App\Models\ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            if ($fixedService->per_person) {
                $fixedServicePrice = $fixedServicePriceObject->price * $seats;
            } else {
                $fixedServicePrice = $fixedServicePriceObject->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($fixedServicePriceObject->currency, $this->booking->currency, $fixedServicePrice));
            $fixed_additional_service_prices[] = [
                'id' => $fixedServicePriceObject->id,
                'name' => $fixedService->name,
                'price' => $tempPrice,
                'per_person' => $fixedServicePriceObject->per_person,
            ];
            $services_price += $tempPrice;
        }

        // Add price for each requested additional service

        $additionalServices = $this->booking->additionalServices()->get();
        $additional_services = [];
        foreach ($additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $additionalServicePrice = $additionalService->price * $seats;
            } else {
                $additionalServicePrice = $additionalService->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $this->booking->currency, $additionalServicePrice));
            $additional_services[] = [
                'id' => $additionalService->id,
                'name' => $additionalService->name,
                'price' => $tempPrice,
                'per_person' => $additionalService->per_person,
            ];
            $services_price += $tempPrice;
        }
        $requestBody = $this->getRequestBody('accepted', $services_price);

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'booking_price_lte_services_price']);
        $this->seeJson(['title' => 'price']);
    }

    /**
     * @test
     */
    public function it_returns_200_if_booking_is_accepted_and_everything_is_ok()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertEquals(Constant::BOOKING_STATUS_ACCEPTED, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_200_if_booking_is_accepted_and_everything_is_ok_with_coupon()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = true;
        $coupon->save();
        $this->booking->coupon = $coupon->name;
        $this->booking->save();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertEquals(Constant::BOOKING_STATUS_ACCEPTED, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_200_if_booking_is_rejected_and_everything_is_ok()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody('rejected');

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertEquals(Constant::BOOKING_STATUS_REJECTED, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_200_if_booking_is_rejected_and_everything_is_ok_with_coupon()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody('rejected');
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = true;
        $coupon->save();
        $this->booking->coupon = $coupon->name;
        $this->booking->save();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $couponNotConsumed = \App\Models\Coupon::find($coupon->id);
        $this->assertFalse($couponNotConsumed->is_consumed);
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertEquals(Constant::BOOKING_STATUS_REJECTED, $booking->status);
    }

    /**
     * @test
     */
    public function it_returns_200_if_booking_is_rejected_and_everything_is_ok_with_coupon_to_user()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody('rejected');
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumable = true;
        $coupon->is_consumed = false;
        $coupon->save();
        $coupon->users()->attach($this->user->id, ['is_coupon_consumed' => true]);
        $this->booking->coupon = $coupon->name;
        $this->booking->save();

        //act
        $this->patchJson($this->getRoute(), $requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
        $couponUser = $coupon->couponsUsers()->where('user_id', $this->user->id)->first();
        $this->assertFalse($couponUser->is_coupon_consumed);
        $responseContent = json_decode($this->response->getContent());
        $booking = \App\Models\Booking::find($responseContent->data->id);
        $this->assertEquals(Constant::BOOKING_STATUS_REJECTED, $booking->status);
    }


    // HELPERS

    protected function setThingsUp()
    {
        // Create data

        // user
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->user = $user;

        // captain
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $this->captain = $captain;

        // boat
        $boat = factory(\App\Models\Boat::class)
            ->make();
        $boat->user()->associate($captain);
        $boat->save();
        $this->boat = $boat;
        $boat->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());

        // experience
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($boat);
        $experience->save();
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $period = factory(\App\Models\Period::class)->states('dummy')->make();
        $period->date_start = \Carbon\Carbon::parse('this monday')->format('Y-m-d');
        $period->date_end = \Carbon\Carbon::parse('this monday')->addWeek(4)->format('Y-m-d');
        $period->is_default = false;
        $experience->periods()->save($period);
        $this->period = $period;
        if (!$period->entire_boat) {
            $price = factory(\App\Models\Price::class)->states('dummy')->make();
            $price->person = 1;
            $period->prices()->save($price);
        }

        $experienceVersion = $experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = true;
        $experienceVersion->save();
        $this->experienceVersion = $experienceVersion;

        $this->fixedAdditionalServicePrices = [];
        $fixedAdditionalServiceIds = [];
        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all()->take(10);
        $i = 0;
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $this->fixedAdditionalServicePrices[$i] = new \App\Models\ExperienceVersionsFixedAdditionalServices([
                "price" => $this->fake->numberBetween(0, 10),
                "per_person" => $this->fake->boolean,
                "currency" => $this->captain->currency
            ]);
            $this->fixedAdditionalServicePrices[$i]->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $this->fixedAdditionalServicePrices[$i]->fixed_additional_service_id = $fixedAdditionalService->id;
            $this->fixedAdditionalServicePrices[$i]->save();
            $fixedAdditionalServiceIds[] = $fixedAdditionalService->id;
            $i++;
        }

        $this->additionalServices = [];
        $additionalServiceIds = [];
        $additionalServices = factory(\App\Models\AdditionalService::class, 10)->make();
        $i = 0;
        foreach ($additionalServices as $additionalService) {
            $additionalService->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $additionalService->currency = $captain->currency;
            $this->additionalServices[$i] = $additionalService;
            $this->additionalServices[$i]->save();
            $additionalServiceIds[] = $this->additionalServices[$i]->id;
            $i++;
        }

        $this->experience = $experience;

        //seo

        $seoController = new \App\Http\Controllers\v1\SeosController();

        $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);

        //booking

        $booking = new \App\Models\Booking([
            'experience_date' => \Carbon\Carbon::parse('next monday')->format('Y-m-d'),
            'request_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'status' => Constant::BOOKING_STATUS_PENDING,
            'price' => 1000,
            'adults' => 3,
            'kids' => 0,
            'babies' => 0,
            'coupon' => null,
            'currency' => $this->captain->currency,
            'entire_boat' => true,
            'fee' => 20,
            'discount' => 0,
            'price_on_board' => 800,
            'price_to_pay' => 200,
        ]);

        $booking->user_id = $this->user->id;
        $booking->experience_version_id = $this->experienceVersion->id;
        $booking->save();

        // Associate fixed additional services

        $booking->fixedAdditionalServices()->attach($fixedAdditionalServiceIds);

        // Associate additional services

        $booking->additionalServices()->attach($additionalServiceIds);

        $this->booking = $booking;
    }


    protected function getRequestBody($status = 'accepted', $price = 1200)
    {
        if ($status == Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_ACCEPTED]) {
            $attributes = [
                'price' => $price,
                'status' => $status
            ];
        }
        if ($status == Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_REJECTED]) {
            $attributes = [
                'status' => $status
            ];
        }
        $requestBody = [
            'data' => [
                'id' => $this->booking->id,
                'type' => 'bookings',
                'attributes' => $attributes
            ]
        ];

        return $requestBody;
    }

    protected function getRoute($id = null)
    {
        if (!isset($id)) {
            return $this->route . $this->booking->id;
        }
        return $this->route . $id;
    }
}