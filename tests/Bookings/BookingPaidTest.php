<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use App\Services\PriceHelper;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BookingPaidTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $type;
    private $user;
    private $captain;
    private $boat;
    private $experience;
    private $conversation;
    private $fixedAdditionalServicePrices;
    private $additionalServices;
    private $period;
    private $experienceVersion;
    private $booking;

    /**
     * ConversationsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/bookings/';
        $this->type = 'bookings';
    }

    /** @test */
    public function it_does_not_send_emails_if_booking_is_not_in_status_accepted()
    {
        //arrange
        $this->setThingsUp();
        $this->booking->status = Constant::BOOKING_STATUS_PENDING;
        $this->booking->save();

        //act
        $bookingController = new \App\Http\Controllers\v1\BookingsController();
        $bookingController->payBookingRequest($this->booking->id);

        //assert
        $this->seeEmailsSent(0);
    }

    /** @test */
    public function it_pays_booking_and_does_not_send_a_mail_to_captain_and_users_with_contacts_if_experience_date_is_more_than_3_days_after_today()
    {
        //arrange
        $this->setThingsUp();

        //act
        $bookingController = new \App\Http\Controllers\v1\BookingsController();
        $bookingController->payBookingRequest($this->booking->id);

        //assert
        $this->seeEmailsSent(3);
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailTo($this->user->email);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $booking = \App\Models\Booking::find($this->booking->id);
        $this->assertFalse($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PAID, $booking->status);
    }



    /** @test */
    public function it_pays_booking_and_sends_a_mail_to_captain_and_users_with_contacts_if_experience_date_is_less_than_3_days_after_today()
    {
        //arrange
        $this->setThingsUp();
        $this->booking->experience_date = \Carbon\Carbon::tomorrow()->format('Y-m-d');
        $this->booking->save();

        //act
        $bookingController = new \App\Http\Controllers\v1\BookingsController();
        $bookingController->payBookingRequest($this->booking->id);

        //assert
        $this->seeEmailsSent(5);
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailTo($this->user->email);
        $this->seeEmailTo($this->captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $booking = \App\Models\Booking::find($this->booking->id);
        $this->assertTrue($booking->is_pre_experience_mail_sent);
        $this->assertEquals(Constant::BOOKING_STATUS_PAID, $booking->status);
    }

    // HELPERS

    protected function setThingsUp()
    {
        // Create data

        // user
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->user = $user;

        // captain
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $this->captain = $captain;

        // boat
        $boat = factory(\App\Models\Boat::class)
            ->make();
        $boat->user()->associate($captain);
        $boat->save();
        $this->boat = $boat;
        $boat->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());

        // experience
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($boat);
        $experience->save();
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $period = factory(\App\Models\Period::class)->states('dummy')->make();
        $period->date_start = \Carbon\Carbon::parse('this monday')->format('Y-m-d');
        $period->date_end = \Carbon\Carbon::parse('this monday')->addWeek(4)->format('Y-m-d');
        $period->is_default = false;
        $experience->periods()->save($period);
        $this->period = $period;
        if (!$period->entire_boat) {
            $price = factory(\App\Models\Price::class)->states('dummy')->make();
            $price->person = 1;
            $period->prices()->save($price);
        }

        $experienceVersion = $experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = true;
        $experienceVersion->save();
        $this->experienceVersion = $experienceVersion;

        $this->fixedAdditionalServicePrices = [];
        $fixedAdditionalServiceIds = [];
        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all()->take(10);
        $i = 0;
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $this->fixedAdditionalServicePrices[$i] = new \App\Models\ExperienceVersionsFixedAdditionalServices([
                "price" => $this->fake->numberBetween(0, 10),
                "per_person" => $this->fake->boolean,
                "currency" => $this->captain->currency
            ]);
            $this->fixedAdditionalServicePrices[$i]->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $this->fixedAdditionalServicePrices[$i]->fixed_additional_service_id = $fixedAdditionalService->id;
            $this->fixedAdditionalServicePrices[$i]->save();
            $fixedAdditionalServiceIds[] = $fixedAdditionalService->id;
            $i++;
        }

        $this->additionalServices = [];
        $additionalServiceIds = [];
        $additionalServices = factory(\App\Models\AdditionalService::class, 10)->make();
        $i = 0;
        foreach ($additionalServices as $additionalService) {
            $additionalService->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $additionalService->currency = $captain->currency;
            $this->additionalServices[$i] = $additionalService;
            $this->additionalServices[$i]->save();
            $additionalServiceIds[] = $this->additionalServices[$i]->id;
            $i++;
        }

        $this->experience = $experience;

        //seo

        $seoController = new \App\Http\Controllers\v1\SeosController();

        $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);

        //booking

        $booking = new \App\Models\Booking([
            'experience_date' => \Carbon\Carbon::tomorrow()->addWeek()->format('Y-m-d'),
            'request_date' => \Carbon\Carbon::now()->format('Y-m-d'),
            'status' => Constant::BOOKING_STATUS_ACCEPTED,
            'price' => 1000,
            'adults' => 3,
            'kids' => 0,
            'babies' => 0,
            'coupon' => null,
            'currency' => $this->captain->currency,
            'entire_boat' => true,
            'fee' => 20,
            'discount' => 0,
            'price_on_board' => 800,
            'price_to_pay' => 200,
        ]);

        $booking->user_id = $this->user->id;
        $booking->experience_version_id = $this->experienceVersion->id;
        $booking->save();

        // Associate fixed additional services

        $booking->fixedAdditionalServices()->attach($fixedAdditionalServiceIds);

        // Associate additional services

        $booking->additionalServices()->attach($additionalServiceIds);

        $this->booking = $booking;
    }
}