<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class EstimationGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $request_date;
    private $user;
    private $captain;
    private $boat;
    private $experience;
    private $fixedAdditionalServicePrices;
    private $additionalServices;
    private $period;
    private $experienceVersion;
    private $fixedAdditionalServiceIds;
    private $additionalServiceIds;

    /**
     * ConversationsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/estimations';
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_fields_do_not_pass_validation()
    {
        //$route = $this->getRoute($this->experience_id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');
        //experience_id
        //arrange
        $this->setThingsUp();
        $route = $this->getRoute('', null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'experience_id']);
        $this->seeJson(['code' => 'required']);

        //hosts
        //arrange
        $route = $this->getRoute($this->experience->id, 0, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'hosts']);
        $this->seeJson(['code' => 'min']);

        //request_date
        //arrange
        $route = $this->getRoute($this->experience->id, null, null, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'request_date']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $route = $this->getRoute($this->experience->id, null, 'not_a_date', $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'request_date']);
        $this->seeJson(['code' => 'dateformat']);

        //currency
        //arrange
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, null);

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'currency']);
        $this->seeJson(['code' => 'required']);

        //arrange
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'not_a_currency');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['title' => 'currency']);
        $this->seeJson(['code' => 'exists']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $route = $this->getRoute(\App\Models\Experience::all()->last()->id + 1, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_have_a_finished_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $route = $this->getRoute($experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_if_last_experience_version_is_rejected_or_freezed()
    {
        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_REJECTED;
        $this->experience->experienceVersions()->save($experienceVersion);
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);

        //arrange
        $this->setThingsUp();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_FREEZED;
        $this->experience->experienceVersions()->save($experienceVersion);
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_does_not_have_a_last_finished_experience_accepted()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished')->make();
        $experienceVersion->status = Constant::STATUS_PENDING;
        $experience->experienceVersions()->save($experienceVersion);
        $route = $this->getRoute($experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_the_fixed_additional_services_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->fixedAdditionalServiceIds[] = \App\Models\ExperienceVersionsFixedAdditionalServices::all()->last()->id + 1;
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'fixed_service_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_the_additional_service_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->additionalServiceIds[] = \App\Models\AdditionalService::all()->last()->id + 1;
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'additional_service_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_experience_date_is_not_available()
    {
        //arrange
        $this->setThingsUp();
        $this->request_date = \Carbon\Carbon::today()->addMonth(2)->format('Y-m-d');
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_the_day_of_the_week_is_not_available()
    {
        //arrange
        $this->setThingsUp();
        $experienceVersion = $this->experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = false;
        $experienceVersion->save();
        $this->request_date = \Carbon\Carbon::parse('next monday')->format('Y-m-d');
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_there_is_not_a_period_with_that_date_available()
    {
        //arrange
        $this->setThingsUp();
        $this->request_date = \Carbon\Carbon::parse('this monday')->addWeek(5)->format('Y-m-d');
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'experience_date_not_available']);
        $this->seeJson(['title' => 'experience_date']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_hosts_are_too_much_or_too_few()
    {
        //seats
        //arrange
        $this->setThingsUp();
        $this->period->min_person = 2;
        $this->period->save();
        $route = $this->getRoute($this->experience->id, $this->period->min_person - 1, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'hosts']);

        //arrange
        $this->setThingsUp();
        $route = $this->getRoute($this->experience->id, $this->experienceVersion->seats + 1, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'hosts']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, 'not_a_coupon', 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_consumed()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = true;
        $coupon->save();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_expired()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->expiration_date = \Carbon\Carbon::today()->subDay(1)->format('Y-m-d');
        $coupon->is_consumed = false;
        $coupon->save();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_users_but_not_this_one()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();
        $coupon->users()->attach($otherUser->id, ['is_coupon_consumed' => false]);
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_users_but_the_request_is_maden_by_a_not_logged_user()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();
        $coupon->users()->attach($otherUser->id, ['is_coupon_consumed' => false]);
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon_login']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_is_assigned_to_user_but_it_is_already_consumed()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $coupon->users()->attach($this->user->id, ['is_coupon_consumed' => true]);
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'coupon_not_found']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_another_experience()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($this->boat);
        $experience->save();
        $coupon->experience_id = $experience->id;
        $coupon->save();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon_experience']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_coupon_is_assigned_to_an_area_and_experience_version_is_outside()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $area = factory(\App\Models\Area::class)->create();
        $coupon->area_id = $area->id;
        $coupon->save();
        $this->experienceVersion->departure_lat = $area->point_b_lat + 5;
        $this->experienceVersion->departure_lng = $area->point_b_lng + 5;
        $this->experienceVersion->save();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized_coupon_area']);
        $this->seeJson(['title' => 'coupon']);
    }

    /**
     * @test
     */
    public function it_returns_200_and_create_the_booking_request_if_all_the_information_provided_are_ok()
    {
        //arrange
        $this->setThingsUp();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $this->seeJson(['id' => (string) $fixedAdditionalServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->name]);
            $this->seeJson(['per_person' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->per_person]);
        }
        foreach ($this->additionalServices as $additionalService) {
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }

        //arrange
        $this->setThingsUp();
        $route = $this->getRoute($this->experience->id, 3, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, null, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $this->seeJson(['id' => (string) $fixedAdditionalServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->name]);
            $this->seeJson(['per_person' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->per_person]);
        }
        foreach ($this->additionalServices as $additionalService) {
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_create_the_booking_request_if_all_the_information_provided_are_ok_with_coupon()
    {
        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $route = $this->getRoute($this->experience->id, null, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $this->seeJson(['id' => (string) $fixedAdditionalServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->name]);
            $this->seeJson(['per_person' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->per_person]);
        }
        foreach ($this->additionalServices as $additionalService) {
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }

        //arrange
        $this->setThingsUp();
        $coupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $coupon->is_consumed = false;
        $coupon->save();
        $route = $this->getRoute($this->experience->id, 3, $this->request_date, $this->fixedAdditionalServiceIds, $this->additionalServiceIds, $coupon->name, 'EUR');

        //act
        $this->get($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $this->seeJson(['id' => (string) $fixedAdditionalServicePrice->id]);
            $this->seeJson(['name' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->name]);
            $this->seeJson(['per_person' => $fixedAdditionalServicePrice->fixedAdditionalService()->first()->per_person]);
        }
        foreach ($this->additionalServices as $additionalService) {
            $this->seeJson(['id' => (string) $additionalService->id]);
            $this->seeJson(['name' => $additionalService->name]);
            $this->seeJson(['per_person' => $additionalService->per_person]);
        }
    }


    // HELPERS

    protected function setThingsUp()
    {
        // Create data

        // user
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->user = $user;

        // captain
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $this->captain = $captain;

        // boat
        $boat = factory(\App\Models\Boat::class)
            ->make();
        $boat->user()->associate($captain);
        $boat->save();
        $this->boat = $boat;
        $boat->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());

        // experience
        $experience = factory(\App\Models\Experience::class)
            ->make();
        $experience->boat()->associate($boat);
        $experience->save();
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $period = factory(\App\Models\Period::class)->states('dummy')->make();
        $period->date_start = \Carbon\Carbon::parse('this monday')->format('Y-m-d');
        $period->date_end = \Carbon\Carbon::parse('this monday')->addWeek(4)->format('Y-m-d');
        $period->is_default = false;
        $experience->periods()->save($period);
        $this->period = $period;
        if (!$period->entire_boat) {
            $price = factory(\App\Models\Price::class)->states('dummy')->make();
            $price->person = 1;
            $period->prices()->save($price);
        }

        $experienceVersion = $experience->lastFinishedExperienceVersion();
        $experienceVersion->monday = true;
        $experienceVersion->save();
        $this->experienceVersion = $experienceVersion;

        $this->fixedAdditionalServicePrices = [];
        $fixedAdditionalServices = \App\Models\FixedAdditionalService::all()->take(10);
        $i = 0;
        foreach ($fixedAdditionalServices as $fixedAdditionalService) {
            $this->fixedAdditionalServicePrices[$i] = new \App\Models\ExperienceVersionsFixedAdditionalServices([
                "price" => $this->fake->numberBetween(0, 200),
                "per_person" => $this->fake->boolean,
                "currency" => $this->captain->currency
            ]);
            $this->fixedAdditionalServicePrices[$i]->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $this->fixedAdditionalServicePrices[$i]->fixed_additional_service_id = $fixedAdditionalService->id;
            $this->fixedAdditionalServicePrices[$i]->save();
            $i++;
        }

        $this->additionalServices = [];
        $additionalServices = factory(\App\Models\AdditionalService::class, 10)->make();
        $i = 0;
        foreach ($additionalServices as $additionalService) {
            $additionalService->experience_version_id = $experience->lastFinishedAndAcceptedExperienceVersion()->id;
            $additionalService->currency = $captain->currency;
            $this->additionalServices[$i] = $additionalService;
            $this->additionalServices[$i]->save();
            $i++;
        }

        $this->experience = $experience;

        //seo

        $seoController = new \App\Http\Controllers\v1\SeosController();

        $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);

        //request_date
        $this->request_date = \Carbon\Carbon::parse('next monday')->format('Y-m-d');

        // fixed and additional service ids
        $this->fixedAdditionalServiceIds = [];
        foreach ($this->fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $this->fixedAdditionalServiceIds[] = $fixedAdditionalServicePrice->id;
        }

        $this->additionalServiceIds = [];
        foreach ($this->additionalServices as $additionalService) {
            $this->additionalServiceIds[] = $additionalService->id;
        }
    }


    /**
     * Get route with parameters
     *
     * @param $experience_id
     * @param null $hosts
     * @param $request_date
     * @param null $fixed_additional_service_prices
     * @param null $additional_services
     * @param null $coupon
     * @param $currency
     * @return string
     */
    protected function getRoute($experience_id, $hosts = null, $request_date, $fixed_additional_service_prices = null, $additional_services = null, $coupon = null, $currency)
    {
        $route = $this->route . '?experience_id=' . $experience_id . '&request_date=' . $request_date . '&currency=' . $currency;

        if (!empty($fixed_additional_service_prices)) {
            $fixedServicesString = implode(',', $fixed_additional_service_prices);
            $route = $route . '&fixed_additional_service_prices=' . $fixedServicesString;
        }

        if (!empty($additional_services)) {
            $additionalServicesString = implode(',', $additional_services);
            $route = $route . '&additional_services=' . $additionalServicesString;
        }

        if (isset($hosts)) {
            $route = $route . '&hosts=' . $hosts;
        }

        if (isset($coupon)) {
            $route = $route . '&coupon=' . $coupon;
        }

        return $route;
    }
}