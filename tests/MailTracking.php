<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 28/10/2016
 * Time: 15:01
 */
trait MailTracking
{
    protected $emails = [];

    /** @before */
    public function setUpMailTracking()
    {
        Mail::getSwiftMailer()
            ->registerPlugin(new TestingMailEventListener($this));
    }

    /**
     * Check if at least one mail was sent
     *
     * @return $this
     */
    protected function seeEmailWasSent()
    {
        $this->assertNotEmpty(
            $this->emails, 'No emails have been sent.'
        );

        return $this;
    }

    /**
     * Check that no mail was sent
     *
     * @return $this
     */
    protected function seeEmailWasNotSent()
    {
        $this->assertEmpty(
            $this->emails, 'Did not expect any emails to have been sent.'
        );

        return $this;
    }

    /**
     * Check that email body is the same of the provided one
     *
     * @param $body
     * @param Swift_Message|null $message
     * @return $this
     */
    protected function seeEmailEquals($body, Swift_Message $message = null)
    {
        $this->assertEquals(
            $body, $this->getEmail($message)->getBody(),
            "No email with the provided body was sent");

        return $this;
    }

    /**
     * Check if a email contains a given string
     *
     * @param $excerpt
     * @param Swift_Message|null $message
     * @return $this
     */
    protected function seeEmailContains($excerpt, Swift_Message $message = null)
    {
        $this->assertContains(
            $excerpt, $this->getEmail($message)->getBody(),
            "No email containing the provided body was found");

        return $this;
    }


    /**
     * Check if the given number of mail was sent
     *
     * @param $count
     * @return $this
     */
    protected function seeEmailsSent($count)
    {
        $emailsSent = count($this->emails);

        $this->assertCount(
            $count, $this->emails,
            "Expected $count emails to have been sent, but $emailsSent were"
        );

        return $this;
    }

    /**
     * Check if the email to the specified recipient was sent
     *
     * @param $recipient
     * @param Swift_Message|null $message
     * @return $this
     */
    protected function seeEmailTo($recipient, Swift_Message $message = null)
    {
        $this->assertArrayHasKey($recipient, $this->getEmailsTo($message), "No email was sent to $recipient.");

        return $this;
    }

    /**
     * Check if the email from the specified sender was sent
     *
     * @param $sender
     * @param Swift_Message|null $message
     * @return $this
     */
    protected function seeEmailFrom($sender, Swift_Message $message = null)
    {
        $this->assertArrayHasKey($sender, $this->getEmailsFrom($message), "No email was sent from $sender.");
        return $this;
    }

    /**
     * Add email
     *
     * @param Swift_Message $email
     */
    public function addEmail(Swift_Message $email)
    {
        $this->emails[] = $email;
    }

    /**
     * Get email from message or, if it is null, from last tracked email
     *
     * @param Swift_Message|null $message
     * @return mixed
     */
    protected function getEmail(Swift_Message $message = null)
    {
        return $message ?: $this->lastEmail();
    }

    /**
     * Get an array of all recipients of last emails sent
     *
     * @param Swift_Message
     * @return mixed
     */
    protected function getEmailsTo(Swift_Message $message = null)
    {
        if (isset($message)) {
            return $message->getTo();
        }
        $tos = [];
        foreach ($this->emails as $message) {
            $tos[array_keys($message->getTo())[0]] = null;
        }
        return $tos;
    }

    /**
     * Get an array of all senders of last emails sent
     *
     * @param Swift_Message
     * @return mixed
     */
    protected function getEmailsFrom(Swift_Message $message = null)
    {
        if (isset($message)) {
            return $message->getFrom();
        }
        $froms = [];
        foreach ($this->emails as $message) {
            $froms[array_keys($message->getFrom())[0]] = null;
        }
        return $froms;
    }

    /**
     * Get the last sent mail
     *
     * @return mixed
     */
    protected function lastEmail()
    {
        return end($this->emails);
    }

}