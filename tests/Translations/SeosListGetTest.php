<?php

/**
 * User: Giuseppe
 * Date: 03/12/2016
 * Time: 16:29
 */

use App\Constant;
use App\Models\User;
use App\Services\JwtService;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class SeosListGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $translator;
    protected $seos;
    protected $route = 'v1/seos';

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(\Lang::getFallback()), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->translator); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute(\Lang::getFallback()), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute(\Lang::getFallback()), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /** @test */
    public function it_returns_422_if_one_of_the_parameter_is_not_valid()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route, $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language']);

        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute('not_a_language'), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'language']);

        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route . '?language=' . \Lang::getFallback() . '&translated=not_a_boolean', $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'boolean']);
        $this->seeJson(['title' => 'translated']);
    }

    /** @test */
    public function it_returns_403_if_it_is_not_a_translator()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->create();

        //act
        $this->getJson($this->getRoute(\Lang::getFallback()), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);

    }

    /** @test */
    public function it_returns_403_if_the_translator_has_not_the_language_that_it_is_asking_for_and_it_is_not_the_fallback_language()
    {
        //arrange
        $this->setThingsUp();
        $language = Language::all()->where('language', 'it')->first();
        $this->translator->languages()->detach($language->id);

        //act
        $this->getJson($this->getRoute('it'), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);

    }

    /** @test */
    public function it_returns_200_and_list_of_all_seo_if_request_and_translator_are_ok()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(\Lang::getFallback()), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
    }

    /** @test */
    public function it_returns_200_and_list_of_all_seo_if_request_and_translator_are_ok_for_translated_seo()
    {
        //arrange
        $this->setThingsUp();
        foreach ($this->seos as $seo) {
            $seoTranslated = factory(\App\Models\Seo::class)->make();
            if ($seoTranslated->language != $seo->language) {
                $experienceVersion = $seo->experienceVersion()->first();
                $experienceVersion->seos()->save($seoTranslated);
            }
        }

        //act
        $this->getJson($this->getRoute(\Lang::getFallback(), true), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $this->assertEquals(true, $element->attributes->translated);
        }

        //arrange
        $this->setThingsUp();
        foreach ($this->seos as $seo) {
            $seoTranslated = factory(\App\Models\Seo::class)->make();
            if ($seoTranslated->language != $seo->language) {
                $experienceVersion = $seo->experienceVersion()->first();
                $experienceVersion->seos()->save($seoTranslated);
            }
        }

        //act
        $this->getJson($this->getRoute('it', true), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $this->assertEquals(true, $element->attributes->translated);
        }
    }

    /** @test */
    public function it_returns_200_and_list_of_all_seo_if_request_and_translator_are_ok_for_not_translated_seo()
    {
        //arrange
        $this->setThingsUp();
        foreach ($this->seos as $seo) {
            $seoTranslated = factory(\App\Models\Seo::class)->make();
            if ($seoTranslated->language != $seo->language) {
                $experienceVersion = $seo->experienceVersion()->first();
                $experienceVersion->seos()->save($seoTranslated);
            }
        }

        //act
        $this->getJson($this->getRoute(\Lang::getFallback(), false), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $this->assertEquals(false, $element->attributes->translated);
        }

        //arrange
        $this->setThingsUp();
        foreach ($this->seos as $seo) {
            $seoTranslated = factory(\App\Models\Seo::class)->make();
            if ($seoTranslated->language != $seo->language) {
                $experienceVersion = $seo->experienceVersion()->first();
                $experienceVersion->seos()->save($seoTranslated);
            }
        }

        //act
        $this->getJson($this->getRoute('it', false), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $this->assertEquals(false, $element->attributes->translated);
        }
    }

    protected function setThingsUp()
    {
        $this->translator = factory(\App\TranslationModels\User::class)->create();
        for ($i = 0; $i < 30; $i++) {
            $user = factory(\App\Models\User::class)->states('captain')->create();
            $boat = factory(\App\Models\Boat::class)->make();
            $boat->user_id = $user->id;
            $boat->save();
            $experience = factory(\App\Models\Experience::class)->make();
            $experience->boat_id = $boat->id;
            $experience->save();
            $experienceVersion = factory(\App\Models\ExperienceVersion::class)->states('dummy', 'finished_and_accepted')->make();
            $experienceVersion->experience_id = $experience->id;
            $experienceVersion->save();
            $seo = factory(\App\Models\Seo::class)->make();
            $this->seos[] = $seo;
            $experienceVersion->seos()->save($seo);

            $language = App\TranslationModels\Language::all()->where('language', $seo->language)->first();
            $this->translator->languages()->attach($language->id);
        }
    }

    /**
     * Get route with parameters
     *
     * @param $language
     * @param $translated
     * @return string
     */
    protected function getRoute($language, $translated = null)
    {
        $route = $this->route . '?language=' . $language;

        if (isset($translated)) {
            $route = $route . '&translated=' . (int) $translated;
        }

        return $route;
    }
}