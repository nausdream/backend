<?php

/**
 * User: Giuseppe
 * Date: 01/12/2016
 * Time: 19:36
 */

use App\Models\User;
use App\TranslationModels\Page;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class PageTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // GET tests
    /** @test */
    public function it_gets_200_and_list_of_pages()
    {
        //arrange
        $translator = factory(Translator::class)->create();

        $page1 = $this->createPage();
        $page2 = $this->createPage();

        //act
        $this->getJson('v1/pages/', $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'pages']);
        $this->seeJson(['id' => (string)$page1->id]);
        $this->seeJson(['name' => $page1->name]);
        $this->seeJson(['created_at' => $page1->created_at->toDateString()]);
        $this->seeJson(['id' => (string)$page2->id]);
        $this->seeJson(['name' => $page2->name]);
        $this->seeJson(['created_at' => $page2->created_at->toDateString()]);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_translator()
    {
        //arrange
        $user = factory(User::class)->create();

        $this->createPage();
        $this->createPage();

        //act
        $this->getJson('v1/pages/', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $translator = factory(User::class)->create();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson('v1/pages/', ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    protected function createPage()
    {
        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        return $page;
    }
}