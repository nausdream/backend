<?php

/**
 * User: Giuseppe
 * Date: 03/12/2016
 * Time: 16:29
 */

use App\Constant;
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class SentencesPostTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /** @test */
    public function it_gets_201_if_it_is_a_translator_and_parameters_are_valid()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'sentences']);
        $this->seeJson(['id' => (string)$sentence->id]);
        $this->seeJson(['text' => $text]);
        $this->seeJson(['language' => $language->language]);
        $sentenceTranslation = SentenceTranslation::all()
            ->where('language_id', $language->id)
            ->where('sentence_id', $sentence->id)
            ->first();;
        $this->assertNotNull($sentenceTranslation);
    }

    /** @test */
    public function it_gets_201_if_it_is_an_admin_and_parameters_are_valid_for_insert_a_translation()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);

        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'sentences']);
        $this->seeJson(['id' => (string)$sentence->id]);
        $this->seeJson(['text' => $text]);
        $this->seeJson(['language' => $language->language]);
        $sentenceTranslation = SentenceTranslation::all()
            ->where('language_id', $language->id)
            ->where('sentence_id', $sentence->id)
            ->first();;
        $this->assertNotNull($sentenceTranslation);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_translator()
    {
        //arrange
        $user = factory(User::class)->create();
        $language = Language::inRandomOrder()->first();

        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $sentence = new Sentence(['name' => $this->fake->word]);

        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson('v1/sentences', ['data' => $data], ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_403_if_it_is_a_translator_but_has_not_permission_for_that_language()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();

        $page = $this->createPage();

        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_gets_404_if_sentence_does_not_exists_and_request_is_for_translation()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', ($sentence->id + 1), ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_gets_422_or_409_if_a_parameters_is_not_valid_for_insert_a_new_translation()
    {
        /** language */
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => 'pippo']);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** type */
        //arrange

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('pippo', $sentence->id, ['text' => $text, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

        /** text */
        //arrange

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => null, 'language' => $language->language]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }


    /** @test */
    public function it_gets_201_if_it_is_an_admin_and_parameters_are_valid_for_insert_a_sentence()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $name = $this->fake->word;
        $text = $this->fake->text;

        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $name], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'sentences']);
        $sentence = Sentence::all()->last();
        $sentenceTranslation = SentenceTranslation::all()->last();
        $this->seeJson(['id' => (string)$sentence->id]);
        $this->seeJson(['text' => $text]);
        $this->assertEquals($text, $sentenceTranslation->text);
        $this->seeJson(['language' => \Lang::getFallback()]);
    }

    /** @test */
    public function it_gets_422_or_409_or_404_if_a_parameters_is_not_valid_for_insert_a_new_sentence()
    {
        /** type */
        //arrange

        $translator = factory(Translator::class)->states('admin')->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $name = $this->fake->word;
        $text = $this->fake->text;

        $data = $this->createData('pippo', null, ['text' => $text, 'name' => $name], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

        /** text */
        //arrange
        $data = $this->createData('sentences', null, ['text' => null, 'name' => $name], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** name */

        $data = $this->createData('sentences', null, ['text' => $text, 'name' => ''], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $data = $this->createData('sentences', null, ['text' => $text, 'name' => str_random(Constant::SENTENCE_NAME_LENGTH + 1)], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $data = $this->createData('sentences', null, ['text' => $text, 'name' => str_random(Constant::SENTENCE_NAME_LENGTH - 1)  . ' '], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $nameNew = $this->fake->word;
        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $nameNew], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));
        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $nameNew], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** page */

        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $name], ['page' => ['data' => ['type' => 'pages', 'id' => ($page->id + 1)]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);

        /** relationship type */

        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $name], ['page' => ['data' => ['type' => 'pippo', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }


    /** @test */
    public function it_gets_403_if_it_is_a_translator_but_is_not_admin_to_insert_new_sentence()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $name = $this->fake->word;
        $text = $this->fake->text;

        $data = $this->createData('sentences', null, ['text' => $text, 'name' => $name], ['page' => ['data' => ['type' => 'pages', 'id' => $page->id]]]);

        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }


    /** @test */
    public function it_returns_201_and_update_sentence_translation_if_sentence_already_exists()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence = new Sentence(['name' => $this->fake->word]);


        $page->sentences()->save($sentence);

        $text = $this->fake->text;
        $text2 = $this->fake->text;
        $data = $this->createData('sentences', $sentence->id, ['text' => $text, 'language' => $language->language]);
        $data2 = $this->createData('sentences', $sentence->id, ['text' => $text2, 'language' => $language->language]);


        //act
        $this->postJson('v1/sentences', ['data' => $data], $this->getHeaders($translator));
        $sentenceTranslation1 = SentenceTranslation::all()
            ->where('language_id', $language->id)
            ->where('sentence_id', $sentence->id)
            ->first();
        $this->postJson('v1/sentences', ['data' => $data2], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'sentences']);
        $this->seeJson(['id' => (string)$sentence->id]);
        $this->seeJson(['text' => $text2]);
        $this->seeJson(['language' => $language->language]);
        $sentenceTranslation2 = SentenceTranslation::all()
            ->where('language_id', $language->id)
            ->where('sentence_id', $sentence->id)
            ->last();
        $this->assertNotNull($sentenceTranslation2);
        $this->assertEquals($sentenceTranslation1->id, $sentenceTranslation2->id);
        $this->assertNotEquals($text, $sentenceTranslation2->text);

    }

    protected function createPage()
    {
        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        return $page;
    }
}