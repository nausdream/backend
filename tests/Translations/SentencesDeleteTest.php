<?php

/**
 * User: Giuseppe
 * Date: 28/12/2016
 * Time: 10:00
 */

use App\Constant;
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class SentencesDeleteTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;


    /** @test */
    public function it_gets_200_if_it_is_an_admin_translator_and_parameters_are_valid_for_delete_a_sentence()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $sentence = $this->createSentence();
        $sentenceTranslation = $sentence->sentenceTranslations()->get()->last();

        //act
        $this->deleteJson('v1/sentences/' . $sentence->id, [], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $newSentence = Sentence::find($sentence->id);
        $this->assertNull($newSentence);
        $newSentenceTranslation = SentenceTranslation::find($sentenceTranslation->id);
        $this->assertNull($newSentenceTranslation);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_admin_translator()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $sentence = $this->createSentence();

        //act
        $this->deleteJson('v1/sentences/' . $sentence->id, [], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot delete this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $sentence = $this->createSentence();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->deleteJson('v1/sentences/' . $sentence->id, [], ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_404_if_sentence_does_not_exist()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $sentence = $this->createSentence();

        //act
        $this->deleteJson('v1/sentences/' . ($sentence->id + 1), [], $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    protected function createSentence()
    {
        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $name = $this->fake->word;
        $text = $this->fake->text;

        $sentence = new Sentence(['name' => $name]);
        $page->sentences()->save($sentence);

        // Create sentence translation

        $sentenceTranslation = new SentenceTranslation(['text' => $text]);
        $language = Language::all()->where('language', \Lang::getFallback())->first();
        $sentenceTranslation->language()->associate($language);
        $sentence->sentenceTranslations()->save($sentenceTranslation);

        return $sentence;
    }
}