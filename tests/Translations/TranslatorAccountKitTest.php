<?php
/**
 * User: Luca Puddu
 * Date: 29/10/2016
 * Time: 15:20
 * Notes: This class can't be tested with a valid access code because, unlike accessTokens, access codes are single-use
 *        codes.
 */

use App\Http\Controllers\v1\Auth\TranslatorAccountKitController;
use App\Models\Currency;
use App\TranslationModels\User as Translator;
use App\Services\JsonHelper;
use App\TranslationModels\Language;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class TranslatorAccountKitTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /** @test */
    public function ak_it_returns_422_if_code_not_found_or_not_valid()
    {
        //arrange
        $data = JsonHelper::createMetaMessage(['code' => 'qwertyuiop']);

        //act
        $this->postJson('v1/auth/translator/', $data);

        //assert
        $this->seeJson(['code' => 'authentication_failed']);
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_returns_200_with_translator_and_jwt()
    {
        //arrange
        $translator = factory(Translator::class)->make();
        $translator->phone = '070666666';
        $translator->save();

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $translator->phone
            ]
        );

        //assert
        $this->assertContains(json_encode(["type" => "translators", "id" => (string) $translator->id]), $response->getContent());
        $this->assertEquals(SymfonyResponse::HTTP_OK, $response->getStatusCode());
    }

    /** @test */
    public function it_returns_403_if_translator_doesnt_exists()
    {   //act
        $response = $this->getAccountKitLoginProtected(
            [
                '070666666'
            ]
        );

        //assert
        $this->assertEquals(SymfonyResponse::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /**
     * Get mock data
     *
     * @return array
     */
    protected function getStub()
    {
        return [
            'phone' => $this->fake->phoneNumber,
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'language' => Language::inRandomOrder()->first()->language,
            'currency' => Currency::inRandomOrder()->first()->name
        ];
    }

    /**
     * Allow calling of protected function on AccountKitController
     *
     * @param array|null $args
     * @return mixed
     */
    protected function getAccountKitLoginProtected(array $args = null)
    {
        $class = new ReflectionClass('App\Http\Controllers\v1\Auth\TranslatorAccountKitController');
        $method = $class->getMethod('accountKitLogin');
        $method->setAccessible(true);
        $object = new TranslatorAccountKitController();
        return $method->invokeArgs($object, $args);
    }
}