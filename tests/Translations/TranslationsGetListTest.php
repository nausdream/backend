<?php

/**
 * User: Giuseppe
 * Date: 02/12/2016
 * Time: 17:18
 */
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class TranslationsGetListTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // GET tests
    /** @test */
    public function it_gets_200_and_list_of_sentences_translation()
    {
        //arrange
        $language = Language::inRandomOrder()->first();

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/translations?page=' . $page->name . '&language=' . $language->language);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson([$sentence1->name => $sentenceTranslation1->text]);
        $this->seeJson([$sentence2->name => $sentenceTranslation2->text]);
    }

    /** @test */
    public function it_gets_200_and_list_of_sentences_translation_with_also_fallback_translation_if_request_language_translation_is_not_present()
    {
        //arrange
        $language = Language::all()->where('language', 'it')->last();
        $fallabackLanguage = Language::all()->where('language', \Lang::getFallback())->last();

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($fallabackLanguage);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/translations?page=' . $page->name . '&language=' . $language->language);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson([$sentence1->name => $sentenceTranslation1->text]);
        $this->seeJson([$sentence2->name => $sentenceTranslation2->text]);
    }

    /** @test */
    public function it_gets_200_and_empty_list_of_sentence_translation_if_there_are_not_translations()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();

        //act
        $this->getJson('v1/translations?page=' . $page->name . '&language=' . $language->language);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson([$page->name => []]);
    }

    /** @test */
    public function it_gets_200_and_list_of_sentences_translation_for_multiple_page_if_requested()
    {
        //arrange
        $language = Language::inRandomOrder()->first();

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        $page2 = $this->createPage();
        $sentence3 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation3 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence4 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation4 = new SentenceTranslation(['text' => $this->fake->text]);

        $page2->sentences()->save($sentence3);
        $page2->sentences()->save($sentence4);

        $sentenceTranslation3->language()->associate($language);
        $sentence3->sentenceTranslations()->save($sentenceTranslation3);
        $sentenceTranslation4->language()->associate($language);
        $sentence4->sentenceTranslations()->save($sentenceTranslation4);

        //act
        $this->getJson('v1/translations?page=' . $page->name . ',' . $page2->name . '&language=' . $language->language);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson([$sentence1->name => $sentenceTranslation1->text]);
        $this->seeJson([$sentence2->name => $sentenceTranslation2->text]);
        $this->seeJson([$sentence3->name => $sentenceTranslation3->text]);
        $this->seeJson([$sentence4->name => $sentenceTranslation4->text]);
    }


    /** @test */
    public function it_gets_404_if_page_does_not_exists()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/translations?page=' . $page->name . 'pippo' . '&language=' . $language->language);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'page_not_found']);
    }

    /** @test */
    public function it_gets_404_if_language_does_not_exists()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/translations?page=' . $page->name . '&language=' . 'pippo');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'language_not_found']);
    }

    protected function createPage()
    {
        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        return $page;
    }
}