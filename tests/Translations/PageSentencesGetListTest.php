<?php

/**
 * User: Giuseppe
 * Date: 02/12/2016
 * Time: 17:18
 */
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class PageSentencesGetListTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // GET tests
    /** @test */
    public function it_gets_200_and_list_of_sentences_translation()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'sentences']);
        $this->seeJson(['id' => (string)$sentence1->id]);
        $this->seeJson(['id' => (string)$sentence2->id]);
        $this->seeJson(['text' => $sentenceTranslation1->text]);
        $this->seeJson(['text' => $sentenceTranslation2->text]);
        $this->seeJson(['language' => $language->language]);
    }

    /** @test */
    public function it_gets_200_and_empty_list_of_sentence_translation_if_there_are_not_translations()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_translator()
    {
        //arrange
        $user = factory(User::class)->create();
        $language = Language::inRandomOrder()->first();

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_403_if_it_is_a_translator_but_has_not_permission_for_that_language()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::all()->where('language', 'it')->first();

        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_gets_200_and_list_of_sentences_translation_if_translator_is_admin()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $language = Language::inRandomOrder()->first();

        $page = $this->createPage();
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . $page->id . '/sentences?language=' . $language->language, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'sentences']);
        $this->seeJson(['id' => (string)$sentence1->id]);
        $this->seeJson(['id' => (string)$sentence2->id]);
        $this->seeJson(['text' => $sentenceTranslation1->text]);
        $this->seeJson(['text' => $sentenceTranslation2->text]);
        $this->seeJson(['language' => $language->language]);
    }

    /** @test */
    public function it_gets_404_if_page_does_not_exists()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . ($page->id + 1) . '/sentences?language=' . $language->language, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_gets_422_if_language_does_not_exists()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $language = Language::inRandomOrder()->first();
        $translator->languages()->attach($language->id);

        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        $sentence1 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation1 = new SentenceTranslation(['text' => $this->fake->text]);
        $sentence2 = new Sentence(['name' => $this->fake->word]);
        $sentenceTranslation2 = new SentenceTranslation(['text' => $this->fake->text]);

        $page->sentences()->save($sentence1);
        $page->sentences()->save($sentence2);

        $sentenceTranslation1->language()->associate($language);
        $sentence1->sentenceTranslations()->save($sentenceTranslation1);
        $sentenceTranslation2->language()->associate($language);
        $sentence2->sentenceTranslations()->save($sentenceTranslation2);

        //act
        $this->getJson('v1/pages/' . ($page->id) . '/sentences?language=' . 'pippo', $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    protected function createPage()
    {
        $it_failed = 0;
        do {
            try {
                $page = factory(\App\TranslationModels\Page::class)->create();
                $it_failed = 0;
            } catch (\Exception $e) {
                $it_failed = 1;
            }
        } while ($it_failed);
        return $page;
    }
}