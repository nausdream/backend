<?php

/**
 * User: Giuseppe
 * Date: 03/12/2016
 * Time: 16:29
 */

use App\Constant;
use App\Models\User;
use App\Services\JwtService;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class SeosSingleGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $translator;
    protected $seo;
    protected $route = 'v1/seos/';

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->translator); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /** @test */
    public function it_returns_404_if_seo_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $id = \App\Models\Seo::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($id), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'seo_not_found']);

    }

    /** @test */
    public function it_returns_403_if_it_is_not_a_translator()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->create();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);

    }

    /** @test */
    public function it_returns_200_if_seo_exist_and_it_is_a_translator_to_make_the_request()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['id' => (string) $this->seo->id]);
        $this->seeJson(['type' => 'seos']);
        $this->seeJson(['title' => $this->seo->title]);
        $this->seeJson(['description' => $this->seo->description]);
        $this->seeJson(['slug_url' => $this->seo->slug_url]);
        $this->seeJson(['language' => $this->seo->language]);
    }

    protected function setThingsUp()
    {
        $this->translator = factory(\App\TranslationModels\User::class)->create();
        $user = factory(\App\Models\User::class)->states('captain')->create();
        $boat = factory(\App\Models\Boat::class)->make();
        $boat->user_id = $user->id;
        $boat->save();
        $experience = factory(\App\Models\Experience::class)->make();
        $experience->boat_id = $boat->id;
        $experience->save();
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->states('dummy')->make();
        $experienceVersion->experience_id = $experience->id;
        $experienceVersion->save();
        $this->seo = factory(\App\Models\Seo::class)->make();
        $experienceVersion->seos()->save($this->seo);

        $language = App\TranslationModels\Language::all()->where('language', $this->seo->language)->first();
        $this->translator->languages()->attach($language->id);
    }

    protected function getRoute($id = null)
    {
        if (!isset($id)) {
            return $this->route . $this->seo->id;
        }
        return $this->route . $id;
    }
}