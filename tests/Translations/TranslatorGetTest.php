<?php

/**
 * User: Giuseppe
 * Date: 02/12/2016
 * Time: 17:18
 */
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class TranslatorGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];

    protected $adminTranslator;
    protected $translator;

    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // GET tests
    /** @test */
    public function it_gets_200_and_all_admin_translator_data()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->adminTranslator->id), $this->getHeaders($this->adminTranslator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'translators']);
        $this->seeJson(['id' => (string)$this->adminTranslator->id]);
        $this->seeJson(['is_admin' => true]);
        $this->seeJson(['first_name' => $this->adminTranslator->first_name]);
        $this->seeJson(['last_name' => $this->adminTranslator->last_name]);
    }

    /** @test */
    public function it_gets_200_and_all_translator_data_and_languages()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->translator->id), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'translators']);
        $this->seeJson(['id' => (string)$this->translator->id]);
        $this->seeJson(['is_admin' => false]);
        $this->seeJson(['first_name' => $this->translator->first_name]);
        $this->seeJson(['last_name' => $this->translator->last_name]);
        $languages = Language::all();
        $this->seeJson(['type' => 'languages']);
        foreach ($languages as $language) {
            $this->seeJson(['id' => (string)$language->id]);
            $this->seeJson(['language' => $language->language]);
            $this->seeJson(['rtl' => $language->rtl]);
        }
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_translator()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->create();

        //act
        $this->getJson($this->getRoute($this->translator->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();
        $signer = new Sha256();
        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->translator->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->translator->id), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_404_if_translator_does_not_exists()
    {
        //arrange
        $this->setThingsUp();
        $translatorId = Translator::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($translatorId), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'translator_not_found']);
    }

    protected function setThingsUp()
    {
        $this->translator = factory(Translator::class)->create();
        $languages = Language::all();
        foreach ($languages as $language) {
            $this->translator->languages()->attach($language->id);
        }
        $this->adminTranslator = factory(Translator::class)->states('admin')->create();
    }

    protected function getRoute($id)
    {
        return 'v1/translators/' . $id;
    }
}