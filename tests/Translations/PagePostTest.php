<?php

/**
 * User: Giuseppe
 * Date: 30/12/2016
 * Time: 13:53
 */

use App\Constant;
use App\Models\User;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class PagePostTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql_translation', 'mysql'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;


    /** @test */
    public function it_gets_201_if_it_is_an_admin_translator_and_parameters_are_valid_for_post_a_page()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $stub = $this->getStub();

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'pages']);
        $this->seeJson(['name' => $stub['data']['attributes']['name']]);
        $page = Page::all()->last();
        $this->seeJson(['id' => (string)$page->id]);
    }

    /** @test */
    public function it_gets_409_if_type_is_not_pages()
    {
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $stub = $this->getStub();
        $stub['data']['type'] = 'page';

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter()
    {
        /** name */
        //arrange
        $translator = factory(Translator::class)->states('admin')->create();
        $stub = $this->getStub();
        $stub['data']['attributes']['name'] = str_random(Constant::PAGE_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['name'] = '';

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['name'] = str_random(Constant::PAGE_NAME_LENGTH - 1) . ' ';

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['name'] = str_random(Constant::PAGE_NAME_LENGTH);
        $page = new Page(['name' => $stub['data']['attributes']['name']]);
        $page->save();

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_a_admin_translator()
    {
        //arrange
        $translator = factory(Translator::class)->create();
        $stub = $this->getStub();

        //act
        $this->postJson('v1/pages', $stub, $this->getHeaders($translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot create this resource']);

    }

    protected function getStub()
    {
        return ['data' => [
            'type' => 'pages',
            'attributes' => [
                'name' => str_random(Constant::PAGE_NAME_LENGTH)
            ]
        ]
        ];
    }
}