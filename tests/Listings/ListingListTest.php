<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ListingListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/listings';
        $this->type = 'listings';
    }

    /**
     * @test
     */
    public function it_returns_422_if_parameters_are_invalid()
    {
        // LATITUDE AND LONGITUDE
        // LAT
        //arrange
        $route = $this->getRoute(null, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'lat']);

        //arrange
        $route = $this->getRoute(Constant::MAX_LATITUDE + 1, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'between']);
        $this->seeJson(['title' => 'lat']);

        //arrange
        $route = $this->getRoute(-Constant::MAX_LATITUDE - 1, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'between']);
        $this->seeJson(['title' => 'lat']);

        //LNG
        //arrange
        $route = $this->getRoute(39.9787, null, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'lng']);

        //arrange
        $route = $this->getRoute(39.9787, Constant::MAX_LONGITUDE + 1, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'between']);
        $this->seeJson(['title' => 'lng']);

        //arrange
        $route = $this->getRoute(39.9787, - Constant::MAX_LONGITUDE - 1, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'between']);
        $this->seeJson(['title' => 'lng']);

        //EXPERIENCE_TYPES
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, ['not_an_experience_type'], true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'experience_types']);

        //ENTIRE_BOAT
        //arrange
        $route = 'v1/listings?lat=39.9787&lng=8.269958&distance=5000&babies=0&kids=0&min_price=1&min_length=1&currency=EUR&language=en&device=desktop&entire_boat=not_a_boolean';

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'boolean']);
        $this->seeJson(['title' => 'entire_boat']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, 2, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'boolean']);
        $this->seeJson(['title' => 'entire_boat']);

        //arrange
        $route = 'v1/listings?lat=39.9787&lng=8.269958&distance=5000&babies=0&kids=0&min_price=1&min_length=1&currency=EUR&language=en&device=desktop';

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'entire_boat']);

        //BOAT_TYPES
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, ['not_a_boat_type'], null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'boat_types']);

        //ADULTS
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, -1, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, 'not_a_number', 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'adults']);

        // BABIES AND KIDS
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, -1, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, null, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 'not_a_number', 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, -1, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, null, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 'not_a_number', 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'kids']);

        // MIN_PRICE
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 0, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'min_price']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, null, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'min_price']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 'not_a_number', null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'min_price']);

        // MAX_PRICE
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, 1, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'max_price']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, 'not_a_number', null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'max_price']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 90, 80, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max_price_greater']);
        $this->seeJson(['title' => 'max_price']);

        //ROAD_STEAD
        //arrange
        $route = 'v1/listings?lat=39.9787&lng=8.269958&distance=5000&babies=0&kids=0&min_price=1&min_length=1&currency=EUR&language=en&device=desktop&road_stead=not_a_boolean&entire_boat=1';


        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'boolean']);
        $this->seeJson(['title' => 'road_stead']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, 2, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'boolean']);
        $this->seeJson(['title' => 'road_stead']);

        // MIN_LENGTH
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 0, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'min_length']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, null, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'min_length']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 'not_a_number', null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'min_length']);

        // MAX_LENGTH
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, 1, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'max_length']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, 'not_a_number', null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'max_length']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 90, 80, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max_length_greater']);
        $this->seeJson(['title' => 'max_length']);

        //RULES
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, ['not_a_rule'], 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'rules']);

        //CURRENCY
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, null, 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'not_a_currency', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency']);

        //LANGUAGE
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', null, 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'not_a_language', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'language']);

        //DEVICE
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', null);

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'device']);

        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'not_a_device');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'device']);
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings()
    {
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_without_last_experience_versions_freezed()
    {
        //arrange
        $route = $this->getRoute(10, 10, 5000, null, false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');
        $this->getJson($route);
        $responseContent = json_decode($this->response->getContent());
        $experienceId = $responseContent->data[0]->id;
        $experience = \App\Models\Experience::find($experienceId);
        $oldExperienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
        $oldExperienceVersion->departure_lat = 10;
        $oldExperienceVersion->departure_lng = 10;
        $oldExperienceVersion->save();
        $route = $this->getRoute(10, 10, 40, null, false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $this->seeJson(['id' => (string)$experience->id]);

        //arrange
        $oldExperienceVersion->save();
        $experience = $oldExperienceVersion->experience()->first();
        $experienceVersion = factory(App\Models\ExperienceVersion::class)->make();
        $experience->experienceVersions()->save($experienceVersion);

        // Link relationship of old experience version to new experience version

        // Fixed rules
        $experienceRules = $oldExperienceVersion->rules()->get();
        $ids = [];
        foreach ($experienceRules as $experienceRule) {
            $ids[] = $experienceRule->id;
        }
        $experienceVersion->rules()->attach($ids);

        // Additional services
        $experienceAdditionalServices = $oldExperienceVersion->additionalServices()->get();
        foreach ($experienceAdditionalServices as $experienceAdditionalService) {
            $additionalServiceData = [
                'name' => $experienceAdditionalService->name,
                'price' => $experienceAdditionalService->price,
                'per_person' => $experienceAdditionalService->per_person,
                'currency' => $experienceAdditionalService->currency
            ];
            $additionalService = new App\Models\AdditionalService($additionalServiceData);
            $experienceVersion->additionalServices()->save($additionalService);
        }

        // Fixed additional service prices
        $experienceFixedAdditionalServicePrices = $oldExperienceVersion->experienceVersionsFixedAdditionalServices()->get();
        foreach ($experienceFixedAdditionalServicePrices as $experienceFixedAdditionalServicePrice) {
            $experienceVersion->fixedAdditionalServices()->attach($experienceFixedAdditionalServicePrice->fixed_additional_service_id, ["price" => $experienceFixedAdditionalServicePrice->price, "per_person" => $experienceFixedAdditionalServicePrice->per_person, "currency" => $experienceFixedAdditionalServicePrice->currency]);
        }

        $experienceVersion->fill($oldExperienceVersion->toArray());

        $experienceVersion->departure_lat = 10;
        $experienceVersion->departure_lng = 10;
        $experienceVersion->is_finished = true;
        $experienceVersion->status = Constant::STATUS_FREEZED;
        $experienceVersion->save();

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->dontSeeJson(['id' => (string)$experience->id]);

    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_without_last_experience_versions_not_searchable()
    {
        //arrange
        $route = $this->getRoute(10, 10, 5000, null, false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');
        $this->getJson($route);
        $responseContent = json_decode($this->response->getContent());
        $experienceId = $responseContent->data[0]->id;
        $experience = \App\Models\Experience::find($experienceId);
        $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
        $experienceVersion->departure_lat = 10;
        $experienceVersion->departure_lng = 10;
        $experienceVersion->save();
        $route = $this->getRoute(10, 10, 40, null, false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $this->seeJson(['id' => (string)$experience->id]);

        //arrange
        $experienceVersion->is_searchable = false;
        $experienceVersion->save();

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->dontSeeJson(['id' => (string)$experience->id]);

    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_of_some_specific_types()
    {
        //arrange
        $type_1 = \App\Models\ExperienceType::all()->first()->name;
        $type_2 = \App\Models\ExperienceType::all()->last()->name;
        $route = $this->getRoute(39.9787, 8.269958, 18000, [$type_1, $type_2], false, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $this->assertTrue($element->attributes->type == $type_1 || $element->attributes->type == $type_2);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_entire_boat_option()
    {
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, true, null, null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $period = $experience->periods()->where('date_start', null)->first();
            if (!isset($period)) {
                $period = $experience->periods()->where('date_start', '>=', $tomorrow)->where('date_end', '<=', $tomorrow)->first();
            }
            if (!isset($period)) {
                $period = $experience->periods()->where('is_default', true)->first();
            }
            $this->assertTrue($period->entire_boat);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_boat_of_some_specific_types()
    {
        //arrange
        $type_1 = \App\Models\BoatType::all()->first()->name;
        $type_2 = \App\Models\BoatType::all()->last()->name;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, [$type_1, $type_2], null, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $boatType = \App\Models\BoatType::find($experience->boat()->first()->lastFinishedAndAcceptedBoatVersion()->type)->name;
            $this->assertTrue($boatType == $type_1 || $boatType == $type_2);
        }
    }


    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_min_seats()
    {
        //arrange
        $seats = 2;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, $seats, 0, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $period = $experience->periods()->where('min_person', '<=', $seats)->where('date_start', null)->first();
            if (!isset($period)) {
                $period = $experience->periods()->where('min_person', '<=', $seats)->where('date_start', '>=', $tomorrow)->where('date_end', '<=', $tomorrow)->first();
            }
            if (!isset($period)) {
                $period = $experience->periods()->where('min_person', '<=', $seats)->where('is_default', true)->first();
            }
            $this->assertTrue($experience->lastFinishedAndAcceptedExperienceVersion()->seats >= $seats);
            $this->assertTrue(isset($period));
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_min_babies()
    {
        //arrange
        $babies = 3;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, null, $babies, 0, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertTrue($experience->lastFinishedAndAcceptedExperienceVersion()->babies >= $babies);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_min_kids()
    {
        //arrange
        $kids = 3;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, null, 0, $kids, 1, null, null, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertTrue($experience->lastFinishedAndAcceptedExperienceVersion()->kids >= $kids);
        }
    }

    /**
    * @test
    */
    public function it_returns_paginated_list_of_all_listings_with_min_price()
    {
        //arrange
        $minPrice = 10;
        $currency = 'EUR';
        $entireBoat = false;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, $entireBoat, null, null, 0, 0, $minPrice, null, null, 1, null, null, $currency, 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);

            $period = $experience->periods()->where('date_start', null)->first();
            if (!isset($period)) {
                $period = $experience->periods()->where('date_start', '>=', $tomorrow)->where('date_end', '<=', $tomorrow)->first();
            }
            if (!isset($period)) {
                $period = $experience->periods()->where('is_default', true)->first();
            }
            $this->assertTrue(isset($period));
            if (isset($period)) {
                if ($entireBoat == true) {
                    if ($period->entire_boat == true) {
                        $finalSeats = $experience->lastFinishedAndAcceptedExperienceVersion()->seats;
                        $price = $period->price;
                        $currencyIn = $period->currency;
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    }
                } else {
                    if ($period->entire_boat == true) {
                        if (isset($seats)) {
                            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
                            if (isset($priceObject)) {
                                $price = $priceObject->price;
                                $currencyIn = $priceObject->currency;
                            } else {
                                $price = $period->price;
                                $currencyIn = $period->currency;
                            }
                            $finalSeats = $seats;
                        } else {
                            $finalSeats = $experience->lastFinishedAndAcceptedExperienceVersion()->seats;
                            $price = $period->price;
                            $currencyIn = $period->currency;
                        }
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    } else {
                        $priceObject = $period->prices()->where('person', 1)->first();
                        if (isset($priceObject)) {
                            $price = ceil(PriceHelper::convertPrice($priceObject->currency, $currency, $priceObject->price));
                        }
                    }
                }
                $this->assertTrue(isset($price) && $price >= $minPrice);
                unset($price);
            }
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_max_price()
    {
        //arrange
        $minPrice = 1;
        $maxPrice = 50;
        $currency = 'EUR';
        $entireBoat = false;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, $entireBoat, null, null, 0, 0, $minPrice, $maxPrice, null, 1, null, null, $currency, 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        $tomorrow = Carbon::tomorrow()->toDateString();
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $period = $experience->periods()->where('date_start', null)->first();
            if (!isset($period)) {
                $period = $experience->periods()->where('date_start', '>=', $tomorrow)->where('date_end', '<=', $tomorrow)->first();
            }
            if (!isset($period)) {
                $period = $experience->periods()->where('is_default', true)->first();
            }
            $this->assertTrue(isset($period));
            if (isset($period)) {
                if ($entireBoat == true) {
                    if ($period->entire_boat == true) {
                        if (isset($seats)) {
                            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
                            if (isset($priceObject)) {
                                $price = $priceObject->price;
                                $currencyIn = $priceObject->currency;
                            } else {
                                $price = $period->price;
                                $currencyIn = $period->currency;
                            }
                            $finalSeats = $seats;
                        } else {
                            $finalSeats = $experience->lastFinishedAndAcceptedExperienceVersion()->seats;
                            $price = $period->price;
                            $currencyIn = $period->currency;
                        }
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    }
                } else {
                    if ($period->entire_boat == true) {
                        if (isset($seats)) {
                            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
                            if (isset($priceObject)) {
                                $price = $priceObject->price;
                                $currencyIn = $priceObject->currency;
                            } else {
                                $price = $period->price;
                                $currencyIn = $period->currency;
                            }
                            $finalSeats = $seats;
                        } else {
                            $finalSeats = $experience->lastFinishedAndAcceptedExperienceVersion()->seats;
                            $price = $period->price;
                            $currencyIn = $period->currency;
                        }
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    } else {
                        $priceObject = $period->prices()->where('person', 1)->first();
                        if (isset($priceObject)) {
                            $price = ceil(PriceHelper::convertPrice($priceObject->currency, $currency, $priceObject->price));
                        }
                    }
                }
                $this->assertTrue(isset($price) && $price >= $minPrice && $price <= $maxPrice);
                unset($price);
            }
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_road_stead_true()
    {
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 18000, ['nights', 'dinners'], false, null, null, 0, 0, 1, null, true, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertTrue($experience->lastFinishedAndAcceptedExperienceVersion()->road_stead);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_road_stead_false()
    {
        //arrange
        $route = $this->getRoute(39.9787, 8.269958, 5000, ['nights', 'dinners'], false, null, 5, 0, 0, 1, null, false, 1, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert;
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertFalse($experience->lastFinishedAndAcceptedExperienceVersion()->road_stead);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_boat_that_has_min_length_20()
    {
        //arrange
        //dd(\App\Models\Experience::find(150)->boat()->first()->lastFinishedAndAcceptedBoatVersion());
        $minLength = 20;
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, null, 0, 0, 1, null, null, $minLength, null, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert;
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertTrue($experience->boat()->first()->lastFinishedAndAcceptedBoatVersion()->length >= $minLength);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_boat_that_has_max_length_30()
    {
        //arrange
        $minLength = 1;
        $maxLength = 30;
        $route = $this->getRoute(39.9787, 8.269958, 18000, null, false, null, null, 0, 0, 1, null, null, $minLength, $maxLength, null, 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert;
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $this->assertTrue($experience->boat()->first()->lastFinishedAndAcceptedBoatVersion()->length >= $minLength);
            $this->assertTrue($experience->boat()->first()->lastFinishedAndAcceptedBoatVersion()->length <= $maxLength);
        }
    }

    /**
     * @test
     */
    public function it_returns_paginated_list_of_all_listings_with_some_specific_rules()
    {
        //arrange
        $rule_1 = \App\Models\Rule::all()->first();
        $rule_2 = \App\Models\Rule::all()->last();
        $route = $this->getRoute(39.9787, 8.269958, 5000, null, false, null, null, 0, 0, 1, null, null, 1, null, [$rule_1->name, $rule_2->name], 'EUR', 'en', 'desktop');

        //act
        $this->getJson($route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $elements = $responseContent->data;
        foreach ($elements as $element) {
            $experience = \App\Models\Experience::find($element->id);
            $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
            $rule_experience_1 = $experienceVersion->rules()->where('rules.id', $rule_1->id)->first();
            $rule_experience_2 = $experienceVersion->rules()->where('rules.id', $rule_2->id)->first();
            $this->assertTrue(isset($rule_experience_1) && isset($rule_experience_2));
        }
    }

    /**
     * Get route with parameters
     *
     * @param $lat
     * @param $lng
     * @param $distance
     * @param null $experience_types
     * @param $entire_boat
     * @param null $boat_types
     * @param null $adults
     * @param $babies
     * @param $kids
     * @param $min_price
     * @param null $max_price
     * @param null $road_stead
     * @param $min_length
     * @param null $max_length
     * @param null $rules
     * @param $currency
     * @param $language
     * @param $device
     * @return string
     */
    protected function getRoute($lat, $lng, $distance, $experience_types = null, $entire_boat = false, $boat_types = null, $adults = null, $babies, $kids, $min_price, $max_price = null, $road_stead = null, $min_length, $max_length = null, $rules = null, $currency, $language, $device)
    {
        $route = $this->route . '?lat=' . $lat . '&lng=' . $lng . '&distance=' . $distance . '&babies=' . $babies . '&kids=' . $kids . '&min_price=' . $min_price . '&min_length=' . $min_length . '&currency=' . $currency . '&language=' . $language . '&device=' . $device . '&entire_boat=' . (int) $entire_boat;

        if (!empty($experience_types)) {
            $experienceTypeString = implode(',', $experience_types);
            $route = $route . '&experience_types=' . $experienceTypeString;
        }

        if (!empty($boat_types)) {
            $boatTypeString = implode(',', $boat_types);
            $route = $route . '&boat_types=' . $boatTypeString;
        }

        if (isset($adults)) {
            $route = $route . '&adults=' . $adults;
        }

        if (isset($max_price)) {
            $route = $route . '&max_price=' . $max_price;
        }

        if (isset($road_stead)) {
            $route = $route . '&road_stead=' . (int) $road_stead;
        }

        if (isset($max_length)) {
            $route = $route . '&max_length=' . $max_length;
        }

        if (!empty($rules)) {
            $ruleString = implode(',', $rules);
            $route = $route . '&rules=' . $ruleString;
        }

        return $route;
    }

}