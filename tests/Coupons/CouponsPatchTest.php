<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $type;
    private $admin;
    private $coupon;
    private $requestBody;

    /**
     * CouponsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'coupons';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = \App\Models\User::all()->random()->first();

        //act
        $this->patchJson($this->getRoute(null), $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_edits_coupon()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertEdited();
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_coupons()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'not coupons';

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_not_found()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(999999999), $this->getRequestBody(999999999), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_409_if_coupon_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['id'] = $this->coupon->id + 1;

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_422_if_coupon_with_same_name_already_exists()
    {
        //arrange
        $this->setThingsUp();
        factory(\App\Models\Coupon::class)->states('dummy')->create(['name' => 'nausdream']);
        $this->requestBody['data']['attributes']['name'] = 'nausdream';

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {
        //NAME
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = -15;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = null;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = '';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();


        //IS_PERCENTUAL/IS_CONSUMABLE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = null;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = 2;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = 'not a boolean';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();


        //AMOUNT
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = null;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = -2;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = 10.5;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = true;
        $this->requestBody['data']['attributes']['amount'] = 101;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = 'not a number';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();


        //EXPIRATION_DATE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = 'not a date';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = '2099-30-03';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = 10.5;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = '1998-10-03';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();


        //CURRENCY
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = 'EURO';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = null;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = true;
        $this->requestBody['data']['attributes']['currency'] = null;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = 10.5;
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();
    }


    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        // Create data
        factory(\App\Models\User::class, 5)->states('dummy')->create();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) {
                $b->user()->associate(\App\Models\User::all()->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) {
                $e->boat()->associate(\App\Models\Boat::all()->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        // Create 10 different coupons
        $i = 0;
        while ($i < 10) {
            try {
                $coupon = factory(\App\Models\Coupon::class)
                    ->states('dummy')
                    ->create();
            } catch (Exception $e) {
                $i++;
                continue;
            }
            $coupon->area()->associate(\App\Models\Area::all()->random());
            $coupon->experience()->associate(\App\Models\Experience::all()->random());
            $ii = 0;
            while ($ii < 5) {
                $user = \App\Models\User::all()->random();
                if ($coupon->couponsUsers()->where('user_id', $user->id)->get()->count() > 0) {
                    $ii++;
                    continue;
                }
                $coupon->users()->attach($user, ['is_coupon_consumed' => $this->fake->boolean]);
                $ii++;
            }
            $coupon->save();
            $i++;
        }

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        $this->coupon = \App\Models\Coupon::all()->random();

        $this->requestBody = $this->getRequestBody();
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }

    protected function getRequestBody(int $id = null)
    {
        $requestBody = [
            "data" => [
                "type" => "coupons",
                "id" => $id ?? $this->coupon->id,
                "attributes" => [
                    "name" => $this->fake->password(2, \App\Constant::COUPON_NAME_LENGTH),
                    "is_percentual" => $this->fake->boolean,
                    "amount" => $this->fake->numberBetween(1, 100),
                    "expiration_date" => date('Y-m-d', strtotime('+' . $this->fake->numberBetween(2, 5) . ' months')),
                    "is_consumable" => $this->fake->boolean,
                    "currency" => \App\Models\Currency::all()->random()->name
                ]
            ]
        ];

        return $requestBody;
    }

    private function getRoute(int $id = null)
    {
        return 'v1/' . $this->type . '/' . ($id ?? $this->coupon->id);
    }

    private function assertNotEdited()
    {
        $this->dontSeeJson(['id' => (string)$this->coupon->id]);
        $this->dontSeeJson(['name' => $this->requestBody['data']['attributes']['name']]);
    }

    private function assertEdited()
    {
        $this->seeJson(['id' => (string)$this->coupon->id]);
        $this->seeJson(['name' => $this->requestBody['data']['attributes']['name']]);
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }
}