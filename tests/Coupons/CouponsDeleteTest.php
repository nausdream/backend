<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsDeleteTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $coupon;
    private $admin;
    private $couponsCount;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'coupons';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = \App\Models\User::all()->random()->first();

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotDeleted();
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotDeleted();
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_does_not_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->deleteJson($this->getRoute($this->coupon->id + 999999), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotDeleted();
    }

    /**
     * @test
     */
    public function it_deletes_a_coupon()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($this->admin));

        //assert
        $this->assertDeleted();
    }

    /**
     * @test
     */
    public function it_deletes_pivot_record()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($this->admin));

        //assert
        $this->assertDeleted();
        $this->assertCount(0, \App\Models\CouponsUser::all()->where('coupon_id', $this->coupon->id));
    }

    /**
     * @test
     */
    public function it_changes_name()
    {
        //arrange
        $this->setThingsUp();
        $name = $this->coupon->name;

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($this->admin));
        $newName = \App\Models\Coupon::withTrashed()->where('id', $this->coupon->id)->get()->first()->name;

        //assert
        $this->assertDeleted();
        $this->assertNotEquals($name, $newName);
    }

    /**
     * @test
     */
    public function it_avoids_duplicate_names()
    {
        //arrange
        $this->setThingsUp();
        $coupon_1 = factory(\App\Models\Coupon::class)->states('dummy')->create(['name' => 'nausdream']);
        $this->deleteJson($this->getRoute($coupon_1->id), [], $this->getHeaders($this->admin));
        $coupon_2 = factory(\App\Models\Coupon::class)->states('dummy')->create(['name' => 'nausdream']);
        $this->deleteJson($this->getRoute($coupon_2->id), [], $this->getHeaders($this->admin));

        //act
        $this->deleteJson($this->route, [], $this->getHeaders($this->admin));
        $coupon_1 = \App\Models\Coupon::withTrashed()->where('id', $coupon_1->id)->get()->first();
        $coupon_2 = \App\Models\Coupon::withTrashed()->where('id', $coupon_2->id)->get()->first();

        //assert
        $this->assertDeleted();
        $this->assertNotEquals($coupon_1->name, $coupon_2->name);
    }


    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        // Create data
        factory(\App\Models\User::class, 5)->states('dummy')->create();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) {
                $b->user()->associate(\App\Models\User::all()->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) {
                $e->boat()->associate(\App\Models\Boat::all()->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        // Create 10 different coupons
        $i = 0;
        while ($i < 10) {
            try {
                $coupon = factory(\App\Models\Coupon::class)
                    ->states('dummy')
                    ->create();
            } catch (Exception $e) {
                $i++;
                continue;
            }
            $coupon->area()->associate(\App\Models\Area::all()->random());
            $coupon->experience()->associate(\App\Models\Experience::all()->random());
            $ii = 0;
            while ($ii < 5) {
                $user = \App\Models\User::all()->random();
                if ($coupon->couponsUsers()->where('user_id', $user->id)->get()->count() > 0) {
                    $ii++;
                    continue;
                }
                $coupon->users()->attach($user, ['is_coupon_consumed' => $this->fake->boolean]);
                $ii++;
            }
            $coupon->save();
            $i++;
        }

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        $this->coupon = \App\Models\Coupon::all()->random();
        $this->route = $this->getRoute($this->coupon->id);
        $this->couponsCount = \App\Models\Coupon::all()->count();
    }

    private function getRoute(int $id = null)
    {
        return 'v1/coupons/' . ($id ?? $this->coupon->id);
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }

    private function assertDeleted()
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->assertCount($this->couponsCount-1, \App\Models\Coupon::all());
    }

    private function assertNotDeleted()
    {
        $this->assertCount($this->couponsCount, \App\Models\Coupon::all());
    }
}