<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $admin;
    private $experience;
    private $area;
    private $users;
    private $coupons;
    private $requestBody;

    /**
     * CouponsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/coupons';
        $this->type = 'coupons';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = \App\Models\User::all()->random()->first();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_saves_a_new_coupon()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
    }

    /**
     * @test
     */
    public function it_saves_relationships_correctly()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        $createdCoupon = \App\Models\Coupon::all()->last();
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals($this->area->id, $createdCoupon->area()->first()->id);
        $this->assertEquals($this->experience->id, $createdCoupon->experience()->first()->id);
        $this->assertEquals($this->users->first()->id, $createdCoupon->users()->get()->first()->id);
        $this->assertEquals($this->users->last()->id, $createdCoupon->users()->get()->last()->id);
        $this->assertCount($this->users->count(), $createdCoupon->users()->get());
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_coupons()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'not coupons';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_422_if_coupon_with_same_name_already_exists()
    {
        //arrange
        $this->setThingsUp();
        factory(\App\Models\Coupon::class)->states('dummy')->create(['name' => 'nausdream']);
        $this->requestBody['data']['attributes']['name'] = 'nausdream';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {
        //NAME
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = -15;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['name'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //IS_PERCENTUAL/IS_CONSUMABLE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = 2;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = 'not a boolean';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //AMOUNT
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = -2;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = 10.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = true;
        $this->requestBody['data']['attributes']['amount'] = 101;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['amount'] = 'not a number';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //EXPIRATION_DATE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = 'not a date';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = '2099-30-03';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = 10.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['expiration_date'] = '1998-10-03';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //CURRENCY
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = 'EURO';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = true;
        $this->requestBody['data']['attributes']['currency'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['is_percentual'] = false;
        $this->requestBody['data']['attributes']['currency'] = 10.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //EXPERIENCES
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['type'] = 'wrong type';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['type'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['type'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //AREAS
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['type'] = 'wrong type';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['type'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['type'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();



        //USERS
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['type'] = 'wrong type';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['type'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['type'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_404_if_relationships_are_not_in_db()
    {
        //EXPERIENCES
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['id'] = 999999999999;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['id'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['id'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();



        //AREAS
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['id'] = 999999999999;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['id'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['area']['data']['id'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();



        //USERS
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['id'] = 999999999999;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['id'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['users']['data'][0]['id'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }




    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        // Create data
        factory(\App\Models\User::class, 5)->states('dummy')->create();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) {
                $b->user()->associate(\App\Models\User::all()->random()->first());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) {
                $e->boat()->associate(\App\Models\Boat::all()->random()->first());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        $this->area = \App\Models\Area::all()->random();
        $this->experience = \App\Models\Experience::all()->random();
        $this->users = \Illuminate\Foundation\Auth\User::all()->random(3);
        $this->coupons = \App\Models\Coupon::all();

        $this->requestBody = $this->getRequestBody($this->experience, $this->area, $this->users);
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }

    protected function getRequestBody(\App\Models\Experience $experience = null, \App\Models\Area $area = null, \Illuminate\Database\Eloquent\Collection $users = null)
    {
        $requestBody = [
            "data" => [
                "type" => "coupons",
                "attributes" => [
                    "name" => $this->fake->password(2, \App\Constant::COUPON_NAME_LENGTH),
                    "is_percentual" => $this->fake->boolean,
                    "amount" => $this->fake->numberBetween(1, 100),
                    "expiration_date" => date('Y-m-d', strtotime('+'. $this->fake->numberBetween(2, 5).' months')),
                    "is_consumable" => $this->fake->boolean,
                    "currency" => \App\Models\Currency::all()->random()->name
                ]
            ]
        ];
        if (isset($experience)) {
            $requestBody['data']['relationships']['experience'] = [
                "data" => [
                    "type" => "experiences",
                    "id" => $experience->id
                ]
            ];
        }
        if (isset($area)) {
            $requestBody['data']['relationships']['area'] = [
                "data" => [
                    "type" => "areas",
                    "id" => $area->id
                ]
            ];
        }
        if (isset($users)) {
            foreach ($users as $user) {
                $requestBody['data']['relationships']['users']['data'][] = [
                    "type" => "users",
                    "id" => $user->id
                ];
            }
        }

        return $requestBody;
    }

    private function assertNotCreated()
    {
        $this->assertCount($this->coupons->count(), \App\Models\Coupon::all());
    }

    private function assertCreated()
    {
        $this->assertCount($this->coupons->count()+1, \App\Models\Coupon::all());
        $newCoupon = factory(\App\Models\Coupon::class)->states('dummy')->create();
        $this->seeJson(['id' => (string)($newCoupon->id-1)]);
    }
}