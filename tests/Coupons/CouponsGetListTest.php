<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $admin;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/coupons';
        $this->type = 'coupons';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = \App\Models\User::all()->random()->first();

        //act
        $this->getJson($this->route, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_coupons()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empy_list_if_there_are_no_coupons()
    {
        if (\App\Models\Coupon::all()->count() == 0){
            //arrange
            $this->setThingsUp();
            DB::table('coupons_users')->delete();
            DB::table('coupons')->delete();

            //act
            $this->getJson($this->route, $this->getHeaders($this->admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }
    }


    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $boats = factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($users) {
                $b->user()->associate($users->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->save();
            });
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats){
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
                $e->save();
            });


        // Create 10 different coupons
        $coupons = new \Illuminate\Database\Eloquent\Collection();
        $i = 0;
        while ($i < 10) {
            try {
                $coupon = factory(\App\Models\Coupon::class)
                    ->states('dummy')
                    ->create();
                $coupons->add($coupon);
            } catch (Exception $e) {
                $i++;
                continue;
            }

            $fakeNumber = $this->fake->numberBetween(0, 2);
            if ($fakeNumber == 0) {
                $coupon->area()->associate($areas->random());
            }
            if ($fakeNumber == 1) {
                $coupon->experience()->associate($experiences->random());
            }
            if ($fakeNumber == 2) {
                $coupon->is_consumed = null;
                $ii = 0;
                while ($ii < 5) {
                    $user = $users->random();
                    if ($coupon->couponsUsers()->where('user_id', $user->id)->get()->count() > 0) {
                        $ii++;
                        continue;
                    }
                    $coupon->users()->attach($user, ['is_coupon_consumed' => $this->fake->boolean]);
                    $ii++;
                }
            }
            $coupon->save();
            $i++;
        }

        $this->coupon = $coupons->random();
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}