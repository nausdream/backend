<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsGetSingleTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $coupon;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = User::all()->random();

        //act
        $this->getJson($this->getRoute($this->coupon->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->coupon->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_200_and_a_single_coupon()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->coupon->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['name' => $this->coupon->name]);
    }

    /**
     * @test
     */
    public function it_returns_404_if_coupon_id_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeCouponId = \App\Models\Coupon::all()->count() != 0 ? \App\Models\Coupon::all()->last()->id + 1 : 1;

        //act
        $this->getJson($this->getRoute($fakeCouponId), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }


    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        // Create data
        factory(\App\Models\User::class, 5)->states('dummy')->create();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) {
                $b->user()->associate(\App\Models\User::all()->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) {
                $e->boat()->associate(\App\Models\Boat::all()->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });


        // Create 10 different coupons
        $i = 0;
        while ($i < 10) {
            try {
                $coupon = factory(\App\Models\Coupon::class)
                    ->states('dummy')
                    ->create();
            } catch (Exception $e) {
                $i++;
                continue;
            }

            $fakeNumber = $this->fake->numberBetween(0, 2);
            if ($fakeNumber == 0) {
                $coupon->area()->associate(\App\Models\Area::all()->random());
            }
            if ($fakeNumber == 1) {
                $coupon->experience()->associate(\App\Models\Experience::all()->random());
            }
            if ($fakeNumber == 2) {
                $coupon->is_consumed = null;
                $ii = 0;
                while ($ii < 5) {
                    $user = \App\Models\User::all()->random();
                    if ($coupon->couponsUsers()->where('user_id', $user->id)->get()->count() > 0) {
                        $ii++;
                        continue;
                    }
                    $coupon->users()->attach($user, ['is_coupon_consumed' => $this->fake->boolean]);
                    $ii++;
                }
            }
            $coupon->save();
            $i++;
        }

        $this->coupon = \App\Models\Coupon::all()->random();
    }

    public function getRoute(int $couponId)
    {
        return 'v1/coupons/' . $couponId;
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}