<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Coupon;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CouponsGetUserCouponsTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $user;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = User::all()->random();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_coupons_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_experiences_coupons()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * @test
     */
    public function it_displays_deleted_coupons()
    {
        //arrange
        $this->setThingsUp();
        $deletedCouponExpDate = $this->user->coupons()->get()->last()->expiration_date;
        $this->user->coupons()->get()->last()->name .= '_deleted_1';
        $this->user->coupons()->get()->last()->save();
        Coupon::whereHas('users', function ($q) {
            $q->where('user_id', $this->user->id);
        })->delete();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['expiration_date' => $deletedCouponExpDate]);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * @test
     */
    public function it_strips_deleted_number_out_of_coupon_name()
    {
        //arrange
        $this->setThingsUp();
        $couponNameBeforeDeleting = $this->user->coupons()->get()->last()->name;
        $lastCoupon = $this->user->coupons()->get()->last();
        $lastCoupon->name = $couponNameBeforeDeleting . '_deleted_1';
        $lastCoupon->save();
        Coupon::whereHas('users', function ($q) {
            $q->where('user_id', $this->user->id);
        })->delete();


        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
        $this->seeJson(['name' => $couponNameBeforeDeleting]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empy_list_if_there_are_no_coupons()
    {
        //arrange
        $this->setThingsUp();
        do {
            $user = \App\Models\User::all()->random();
        } while ($user->id == $this->user->id);


        //act
        $this->getJson($this->getRoute($user->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_id_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(999999999999), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }


    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'coupons')->first()->id);

        // Create data
        factory(\App\Models\User::class, 5)->states('dummy')->create();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) {
                $b->user()->associate(\App\Models\User::all()->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });
        factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) {
                $e->boat()->associate(\App\Models\Boat::all()->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
            });


        $this->user = \App\Models\User::all()->random();

        // Create 10 different coupons
        $i = 0;
        while ($i < 10) {
            try {
                $coupon = factory(\App\Models\Coupon::class)
                    ->states('dummy')
                    ->create();
            } catch (Exception $e) {
                $i++;
                continue;
            }

            $fakeNumber = $this->fake->numberBetween(0, 1);
            if ($fakeNumber == 0) {
                $coupon->area()->associate(\App\Models\Area::all()->random());
            }
            if ($fakeNumber == 1) {
                $coupon->experience()->associate(\App\Models\Experience::all()->random());
            }
            $coupon->is_consumed = null;
            $coupon->users()->attach($this->user, ['is_coupon_consumed' => $this->fake->boolean]);
            $coupon->save();
            $i++;
        }
    }

    public function getRoute(int $id)
    {
        return 'v1/users/' . $id . '/coupons';
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}