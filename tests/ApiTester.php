<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 21/10/2016
 * Time: 18:12
 */

use App\Services\JwtService;
use Faker\Factory as Faker;

abstract class ApiTester extends TestCase
{
    protected $fake;

    protected $times = 1;

    /**
     * ApiTester constructor.
     */
    function __construct()
    {
        $this->fake = Faker::create();
        ini_set('memory_limit','1024M');
    }

    /**
     * Set the number of times to do something
     *
     * @param $count
     * @return $this
     */
    protected function times($count)
    {
        $this->times = $count;

        return $this;
    }

    /**
     * Make a record for the specified object type
     *
     * @param $type
     * @param array $fields
     */
    protected function make($type, array $fields = [])
    {
        while ($this->times--) {
            $stub = array_merge($this->getStub(), $fields);
            $type::create($stub);
        }
    }

    /**
     * Test if the object has all the specified attributes
     */
    protected function assertObjectHasAttributes()
    {
        $args = func_get_args();
        $object = array_shift($args);

        foreach ($args as $attribute) {
            $this->assertObjectHasAttribute($attribute, $object);
        }
    }

    /**
     * Stub method to create a factory
     */
    protected function getStub()
    {
        throw new BadMethodCallException('Create your own getStub method');
    }

    /**
     * Create the string with parameters for get by an array
     *
     * @param array $parameters
     * @return string
     */
    protected function getParametersStringForGet(array $parameters)
    {
        $string = '';
        $is_first = true;
        foreach ($parameters as $key => $parameter) {
            if ($is_first == true) {
                $is_first = false;
                $amper = '';
            } else {
                $amper = '&';
            }
            $string = $string . $amper . $key . '=' . $parameter;
        }
        return $string;
    }

    /**
     * Check differences on two multidimensional arrays
     *
     * @param $aArray1
     * @param $aArray2
     * @return array
     */
    public function arrayRecursiveDiff($aArray1, $aArray2)
    {
        $aReturn = array();

        foreach ($aArray1 as $mKey => $mValue) {
            if (array_key_exists($mKey, $aArray2)) {
                if (is_array($mValue)) {
                    $aRecursiveDiff = $this->arrayRecursiveDiff($mValue, $aArray2[$mKey]);
                    if (count($aRecursiveDiff)) {
                        $aReturn[$mKey] = $aRecursiveDiff;
                    }
                } else {
                    if ($mValue != $aArray2[$mKey]) {
                        $aReturn[$mKey] = $mValue;
                    }
                }
            } else {
                $aReturn[$mKey] = $mValue;
            }
        }

        return $aReturn;
    }

    /**
     * Create a data response
     *
     * @param string $type
     * @param $id
     * @param $attributes
     * @param $relationships
     * @return array
     */
    public static function createData(string $type, $id = null, $attributes = [], $relationships = [])
    {
        $data = [
            'type' => $type,
        ];
        if (isset($id)) {
            $data['id'] = (string)$id;
        }
        if (!empty ($attributes)) {
            $data['attributes'] = $attributes;
        }
        if (!empty ($relationships)) {
            $data['relationships'] = $relationships;
        }

        return $data;
    }

    /**
     * Get right header value by account
     *
     * @param $account
     * @return array
     */
    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }
}