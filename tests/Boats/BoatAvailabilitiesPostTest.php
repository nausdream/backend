<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatAvailabilitiesPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $boat_1;
    public $id;
    public $boatVersions;
    public $requestBody;
    public $stub;

    // ANYONE
    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['type']);
        $this->requestBody['data']['type'] = 'wrong type';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_attributes_not_present()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['attributes']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_date_start_not_present()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['attributes']['date_start']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_date_end_not_present()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['attributes']['date_end']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_date_start_is_more_recent_than_date_end()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = date('Y-m-d', strtotime('+2 months'));
        $this->requestBody['data']['attributes']['date_end'] = date('Y-m-d');

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_date_start_or_date_end_overlaps_with_one_already_present_in_db()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boat_1->boatAvailabilities()->save(new \App\Models\BoatAvailability(['date_start' => '2016-05-20', 'date_end' => '2016-10-20']));
        $this->requestBody['data']['attributes']['date_start'] = '2016-07-20';
        $this->requestBody['data']['attributes']['date_end'] = '2017-02-01';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount(1, $this->boat_1->boatAvailabilities()->get());
    }

    /** @test */
    public function it_returns_422_if_date_start_and_date_end_are_equal()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->requestBody['data']['attributes']['date_start'] = date('Y-m-d');
        $this->requestBody['data']['attributes']['date_end'] = date('Y-m-d');

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_boat_type_or_id_not_present()
    {
        // TYPE
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['relationships']['boat']['data']['type']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //ID
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['relationships']['boat']['data']['id']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $boat = $this->getBoat($this->boat);
        $this->requestBody['data']['relationships']['boat']['data']['id'] = $boat->id;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_boat()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $boats = \App\Models\Boat::all();
        $boatId = $boats->count() > 0 ? $boats->last()->id + 1 : 1;
        $this->requestBody['data']['relationships']['boat']['data']['id'] = $boatId;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_validates_fields()
    {
        // DATE START/DATE END
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = 'not a date';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = 5;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = '5/4/4';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = '2500/4/4';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = '1998/4/4';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['date_start'] = \Carbon\Carbon::today()->addMonth()->toDateString();
        $this->requestBody['data']['attributes']['date_end'] = date('Y-m-d');

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);


        // RELATIONSHIP TYPE
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['relationships']['boat']['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);

        // RELATIONSHIP ID
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['relationships']['boat']['data']['id'] = 'not a number';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }



    // ADMIN
    /** @test */
    public function it_returns_201_and_edit_boat_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->boat_1);

    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[2]->lat = -5001;
        $this->boatVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_boats_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_finished_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[0]->is_finished = false;
        $this->boatVersions[0]->save();
        $this->boatVersions[1]->is_finished = false;
        $this->boatVersions[1]->save();
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }




    // USER
    /** @test */
    public function it_returns_201_and_edit_boat()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_boat_version_is_not_accepted_conditionally()
    {

        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);

        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }


    //helpers

    private function getBoatVersionStub(bool $isFinished = true)
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'is_finished' => $isFinished
        ];
    }

    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true)
    {
        $boatVersion = factory(\App\Models\BoatVersion::class)->create($this->getBoatVersionStub($isFinished));
        if (isset($boat)) {
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['boat_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'boat_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'boats')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    public function getRequestBody(int $boatId)
    {
        $body = [];
        $body['data'] = [
            "type" => "boat-availabilities",
            "attributes" => [
                "date_start" => date('Y-m-d'),
                "date_end" => date('Y-m-d', strtotime('+' . $this->fake->randomDigitNotNull . ' months'))
            ],
            "relationships" => [
                "boat" => [
                    "data" => [
                        "type" => "boats",
                        "id" => $boatId
                    ]
                ]
            ]
        ];
        return $body;
    }

    public function getRoute()
    {
        return 'v1/boat-availabilities/';
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat_1 = $this->getBoat($this->user_1);
        $this->id = $this->boat_1->id;
        $this->boatVersions = [];
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->requestBody = $this->getRequestBody($this->boat_1->id);
    }

    protected function setStatus(int ...$status)
    {
        for ($i = 0; $i < count($this->boatVersions); $i++) {
            $this->boatVersions[$i]->status = $status[$i];
            $this->boatVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\Boat $boat)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCount(1, $boat->boatAvailabilities()->get());

        $availability = \App\Models\BoatAvailability::where('boat_id', $this->boat_1->id)
            ->where('date_start', $this->requestBody['data']['attributes']['date_start'])
            ->get()->first();
        $this->seeJson(["id" => (string) $availability->id]);
    }

    protected function assertNotEdited(\App\Models\Boat $boat)
    {
        $this->assertCount(0, $boat->boatAvailabilities()->get());
    }
}