<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Experience;
use App\Services\JwtService;
use JD\Cloudder\Facades\Cloudder;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    /**
     * ExperiencePostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /** @test */
    public function it_returns_200_and_edit_last_unfinished_boat_version()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_200_and_edit_last_accepted_conditionally_boat_version()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat);
        $boatVersion_3->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $boatVersion_3->save();

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_200_and_create_new_boat_version_if_last_is_accepted()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat);
        $boatVersion_3->status = Constant::STATUS_ACCEPTED;
        $accessory_1_id = \App\Models\Accessory::all()->first()->id;
        $accessory_2_id = \App\Models\Accessory::all()->last()->id;
        $boatVersion_3->accessories()->attach([$accessory_1_id, $accessory_2_id]);

        $link = 'http://www.w3schools.com/css/img_fjords.jpg';
        $url = \App\Services\PhotoHelper::getBoatPhotoPublicId($user->id, $boat->id, 'external');
        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );
        $arrayResult = Cloudder::getResult();
        $boatVersion_3->version_external_photo = $arrayResult['version'];

        $url = \App\Services\PhotoHelper::getBoatPhotoPublicId($user->id, $boat->id, 'internal');
        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );
        $arrayResult = Cloudder::getResult();
        $boatVersion_3->version_internal_photo = $arrayResult['version'];

        $url = \App\Services\PhotoHelper::getBoatPhotoPublicId($user->id, $boat->id, 'sea');
        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );
        $arrayResult = Cloudder::getResult();
        $boatVersion_3->version_sea_photo = $arrayResult['version'];

        $boatVersion_3->save();

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber + 1, $boat->boatVersions()->get());

        $boatVersionNew = $boat->boatVersions()->get()->last();
        $this->assertNotEquals($boatVersion_3->id, $boatVersionNew->id);

        $this->assertNotNull($boatVersionNew->accessories()->where('accessory_id', $accessory_1_id)->first());
        $this->assertNotNull($boatVersionNew->accessories()->where('accessory_id', $accessory_2_id)->first());

        $this->assertNotNull($boatVersionNew->version_external_photo);
        $this->assertNotNull($boatVersionNew->version_internal_photo);
        $this->assertNotNull($boatVersionNew->version_sea_photo);

        $this->assertTrue($this->url_exists(\App\Services\PhotoHelper::getBoatPhotoLink($boatVersionNew, 'external', null)));
        $this->assertTrue($this->url_exists(\App\Services\PhotoHelper::getBoatPhotoLink($boatVersionNew, 'internal', null)));
        $this->assertTrue($this->url_exists(\App\Services\PhotoHelper::getBoatPhotoLink($boatVersionNew, 'sea', null)));
    }

    /** @test */
    public function it_returns_403_if_last_boat_version_is_pending()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat);
        $boatVersion_3->status = Constant::STATUS_PENDING;
        $boatVersion_3->save();

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_last_boat_version_is_rejected()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat);
        $boatVersion_3->status = Constant::STATUS_REJECTED;
        $boatVersion_3->save();

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $user = $this->getUser();
        $user_2 = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_user_is_admin()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_user_is_a_visitor()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_404_if_last_boat_version_doesnt_exists()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_404_if_boat_doesnt_exists()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $boatId = 1;
        if (Experience::all()->count() != 0){
            $boatId = Experience::all()->last()->id + 1;
        }

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boatId, $stub);

        //act
        $this->patchJson($this->getRoute($boatId), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->dontSeeJson(['model' => $stub['model']]);
    }

    /** @test */
    public function it_returns_409_if_object_type_is_not_boats()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        $requestBody['data']['type'] = 'wrong_type';
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);
        $boat_2 = $this->getBoat($user);

        $stub = $this->getBoatVersionStub(false);
        $requestBody = $this->getRequestBody($boat_2->id, $stub);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());
    }

    /** @test */
    public function it_validates_fields()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $boatVersion_1 = $this->getBoatVersion($boat);
        $boatVersion_2 = $this->getBoatVersion($boat);
        $boatVersion_3 = $this->getBoatVersion($boat, false);
        $boatVersionsNumber = $boat->boatVersions()->get()->count();


        //=====================PRODUCTION SITE/DOCKING PLACE
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['production_site'] = str_random(Constant::PRODUCTION_SITE_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['docking_place'] = str_random(Constant::DOCKING_PLACE_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);

        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //=====================MODEL/NAME
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['model'] = str_random(Constant::BOAT_MODEL_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['name'] = str_random(Constant::BOAT_NAME_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());


        //=====================CONSTRUCTION YEAR/RESTORATION YEAR
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['construction_year'] = 'not a date';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['construction_year'] = '5/4/4';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['construction_year'] = date('Y') + strtotime('+ 1 year');
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());



        //=====================TYPE/MATERIAL/MOTORS QUANTITY/MOTOR TYPE
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['type'] = 'not_a_valid_type';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['type'] = -10;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['type'] = 1.5;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['type'] = 200;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());



        //=====================LENGTH
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['length'] = 'not_an_integer';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['length'] = -10;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['length'] = 1.5;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['length'] = 65536;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());




        //=====================HAS INSURANCE
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['has_insurance'] = 'not_a_boolean';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['has_insurance'] = -3;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['has_insurance'] = 5;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //=====================EVENTS
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['events'] = 'not_a_boolean';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['events' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['events'] = -3;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['events'] = 5;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());



        //=====================LAT/LNG
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['lat'] = 'not a number';
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['lat'] = -50000;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['lat'] = 50000;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['lat'] = true;
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //=====================SEATS/BERTHS/TOILETS/CABINS
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['seats'] = 'dsfsd';
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['seats'] = 0.5;
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['seats'] = -5;
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //=====================DESCRIPTION/RULES/INDICATIONS
        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['description'] = str_random(Constant::BOAT_DESCRIPTION_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['description' => $stub['description']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['rules'] = str_random(Constant::BOAT_RULES_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['rules' => $stub['rules']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

        //arrange
        $stub = $this->getBoatVersionStub(false);
        $stub['indications'] = str_random(Constant::BOAT_INDICATIONS_LENGTH + 1);
        $requestBody = $this->getRequestBody($boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($boat->id), $requestBody, $this->getHeaders($user));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['indications' => $stub['indications']]);
        $this->assertCount($boatVersionsNumber, $boat->boatVersions()->get());

    }

    // HELPERS
    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true)
    {
        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy')->create(['is_finished' => $isFinished]);
        if (isset($boat)) {
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionStub(bool $isFinished)
    {
        $requestBody = [
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->name,
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->name,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'fee' => $this->fake->numberBetween(10, 50),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'events' => $this->fake->boolean()
        ];

        if ($isFinished){
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $boatId, array $stub)
    {
        return [
            'data' => [
                'type' => 'boats',
                'id' => (string)$boatId,
                'attributes' => $stub
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/boats/'.$id;
    }

    protected function url_exists($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        }
        return true;
    }
}