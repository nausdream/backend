<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceTranslationTitle;
use App\Models\User;
use App\Services\JwtService;
use App\Services\PhotoHelper;
use JD\Cloudder\Facades\Cloudder;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class BoatGetSingleTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $boat;
    private $captain;
    private $admin;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'boats';
    }

    /**
     * ?editing=true
     * @test
     */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->get()->last();
        $token = JwtService::getTokenStringFromAccount($this->captain); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($this->boat->id, true), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /**
     * ?editing=true
     * @test
     */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->captain->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_captain_it_returns_200_and_last_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->get()->last();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['is_finished' => false]);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_200_and_last_finished_boat_version_in_admin_area()
    {
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('is_finished', true)->get()->last();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['is_finished' => true]);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_403_if_admin_has_no_boats_permission_or_last_finished_is_not_in_area()
    {
        // ADMIN NO PERMISSION
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        // ADMIN NOT IN AREA
        //arrange
        $this->setThingsUp();
        $smallArea = factory(Area::class)->create(['point_a_lat' => 0, 'point_b_lat' => 0]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_user_it_returns_403()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders(new \App\Models\User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=false
     * @test
     */
    public function it_returns_200_and_last_ACCEPTED_boat_version_for_any_user()
    {
        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);



        //ADMIN NO PERMISSIONS
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);



        //ADMIN NO AREA
        //arrange
        $this->setThingsUp();
        $this->admin->area()->first()->point_a_lat = 0;
        $this->admin->area()->first()->point_b_lat = 0;
        $this->admin->area()->first()->save();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);

        //ADMIN WITH AREA AND PERMISSIONS
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);

        //USER
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route, $this->getHeaders(new \App\Models\User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);

        //VISITOR
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);
    }

    /**
     * ?editing=false
     * @test
     */
    public function it_returns_403_if_last_finished_boat_version_is_rejected()
    {
        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);



        //ADMIN NO PERMISSIONS
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);



        //ADMIN NO AREA
        //arrange
        $this->setThingsUp();
        $this->admin->area()->first()->point_a_lat = 0;
        $this->admin->area()->first()->point_b_lat = 0;
        $this->admin->area()->first()->save();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        //ADMIN WITH AREA AND PERMISSIONS
        //arrange
        $this->setThingsUp();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);



        //USER
        //arrange
        $this->setThingsUp();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route, $this->getHeaders(new \App\Models\User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);



        //VISITOR
        //arrange
        $this->setThingsUp();
        $lastFinished = $this->boat->boatVersions()->where('is_finished', true)->get()->last();
        $lastFinished->status = Constant::STATUS_REJECTED;
        $lastFinished->save();

        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /*
    public function it_fetches_correct_language_data_for_visitors()
    {
        //VISITOR
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();
        $description_1 = new BoatTranslationDescription(
            [
                'language' => 'it',
                'text' => $this->fake->text
            ]
        );
        $description_2 = new BoatTranslationDescription(
            [
                'language' => 'en',
                'text' => $this->fake->text
            ]
        );
        $returnedVersion->boatTranslationDescriptions()->saveMany([$description_1, $description_2]);

        //act
        $this->getJson($this->getRoute($this->boat->id, false, 'en'));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);
        $this->seeJson(['description' => $description_2['text']]);

        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();
        $description_1 = new BoatTranslationDescription(
            [
                'language' => 'it',
                'text' => $this->fake->text
            ]
        );
        $description_2 = new BoatTranslationDescription(
            [
                'language' => 'en',
                'text' => $this->fake->text
            ]
        );
        $returnedVersion->boatTranslationDescriptions()->saveMany([$description_1, $description_2]);

        //act
        $this->getJson($this->getRoute($this->boat->id, false, 'it'));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);
        $this->seeJson(['description' => $description_1['text']]);
    }
    */

    /**
     * @test
     */
    public function it_returns_404_if_boat_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute(9999999999999), $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_404_if_correct_boat_version_doesnt_exist()
    {
        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $boat = factory(Boat::class)->create();

        //act
        $this->getJson($this->getRoute($boat->id), $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);


        // ADMIN
        //arrange
        $this->setThingsUp();
        $boat = factory(Boat::class)->create();
        $bv_1 = factory(BoatVersion::class)->states('dummy')->create(['is_finished' => false]);
        $bv_2 = factory(BoatVersion::class)->states('dummy')->create(['is_finished' => false]);
        $boat->boatVersions()->saveMany([$bv_1, $bv_2]);

        //act
        $this->getJson($this->getRoute($boat->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);


        // EVERYONE WITH EDITING=FALSE
        //arrange
        $this->setThingsUp();
        $boat = factory(Boat::class)->create();
        $bv_1 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $bv_2 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat->boatVersions()->saveMany([$bv_1, $bv_2]);

        //act
        $this->getJson($this->getRoute($boat->id));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }


    /** @test */
    public function it_returns_also_url_to_boat_pictures_in_attributes()
    {
        //arrange
        $this->setThingsUp();
        $boatVersion = $this->boat->boatVersions()->get()->last();
        $photo_version = $boatVersion->external_photo_version;
        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $this->boat->id)]];
        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            $this->getHeaders($this->captain)
        );
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->captain));

        //assert
        $device = PhotoHelper::getDeviceByName(null);

        $newBoatVersion = BoatVersion::find($boatVersion->id);
        $this->assertNotEquals($photo_version, $newBoatVersion->version_external_photo);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER')
            . 'users/'
            . $this->boat->user->id . "/"
            . $this->boat->id . "/"
            . 'external');
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_returns_also_url_with_null_value_in_attributes_if_boat_has_not_photo()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->boat->id, true), $this->getHeaders($this->captain));

        //assert
        $this->seeJson(['external_photo' => null]);
        $this->seeJson(['internal_photo' => null]);
        $this->seeJson(['sea_photo' => null]);
    }

    /**
     * @test
     */
    public function it_includes_default_relationships_and_included()
    {
        //arrange
        $this->setThingsUp();
        $returnedVersion = $this->boat->boatVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();

        // Accessories
        $accessory = new \App\Models\Accessory(['name' => $this->fake->text(Constant::ACCESSORY_LENGTH)]);
        $accessory->save();
        $returnedVersion->accessories()->attach($accessory->id);

        // Experience_Types
        $experience_type = new \App\Models\ExperienceType(['name' => $this->fake->text(Constant::ACCESSORY_LENGTH)]);
        $experience_type->save();
        $this->boat->experienceTypes()->attach($experience_type->id);

        // Availabilities
        $availability_1 = new \App\Models\BoatAvailability(
            [
                'date_start' => $this->fake->date(),
                'date_end' => $this->fake->date()
            ]);
        $availability_1->boat()->associate($this->boat);
        $availability_1->save();

        // Feedbacks
        $feedback_1 = new \App\Models\BoatFeedback(['score' => $this->fake->numberBetween(0, 5)]);
        $feedback_1->boat()->associate($this->boat);
        $feedback_1->user()->associate(\App\Models\User::all()->random());
        $feedback_1->save();

        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['model' => $returnedVersion->model]);
        $this->seeJson(['status' => 'accepted']);
        $this->seeJson(
            ["accessories" => ["data" => [["type" => "accessories", "id" => (string) $accessory->id]]]]
        );
        $this->seeJson(
            ["experience_types" => ["data" => [["type" => "experience-types", "id" => (string) $experience_type->id]]]]
        );
        $this->seeJson(
            ["availabilities" => ["data" => [["type" => "availabilities", "id" => (string) $availability_1->id]]]]
        );
        $this->dontSeeJson(
            ["feedbacks" => ["data" => [["type" => "feedbacks", "id" => (string) $feedback_1->id]]]]
        );
        $this->seeJson(
            [
                'date_start' => $availability_1->date_start
            ]
        );
    }

    /**
     * @test
     */
    public function it_includes_requested_relationships_and_included()
    {
        //arrange
        $this->setThingsUp();

        // Feedbacks
        $feedback_1 = new \App\Models\BoatFeedback(['score' => $this->fake->numberBetween(0, 5)]);
        $feedback_1->boat()->associate($this->boat);
        $feedback_1->user()->associate(\App\Models\User::all()->random());
        $feedback_1->save();

        //act
        $this->getJson($this->route .'&include=feedbacks');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(
            [
                'score' => $feedback_1->score
            ]
        );
    }



    // HELPERS
    protected function getHeaders($account){
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
        $this->admin->save();

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $boats = factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($users) {
                $b->user()->associate($users->random());
                $b->save();
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make(['is_finished' => false]));
            });

        $this->boat = $boats->random();
        $this->captain = $this->boat->user()->first();
        $this->route = $this->getRoute($this->boat->id, false);
    }

    protected function getRoute(int $id, bool $editing = false, string $language = null)
    {
        return 'v1/'.$this->type.'/'.$id . '?' . ($editing ? 'editing=true' : 'editing=false') . (isset($language) ? '&language='.$language : '');
    }

    protected function url_exists($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        }
        return true;
    }
}