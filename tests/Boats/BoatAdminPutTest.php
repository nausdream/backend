<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatAdminPutTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $id;
    public $boatVersions;
    public $requestBody;
    public $stub;

    /** @test */
    public function it_returns_200_and_edit_last_finished_boat_version_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, -1);
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[1]->id));
    }

    /** @test */
    public function it_returns_422_if_there_are_not_3_photos_set()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[2]->version_external_photo = null;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_doesnt_change_status()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $oldStatus = $this->boatVersions[2]->status;

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        $newStatus = $this->boat->boatVersions()->get()->last()->status;

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[2]->id));
        $this->assertEquals($oldStatus, $newStatus);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[2]->lat = -5001;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_finished_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(-1, -1, -1);
        $this->boatVersions[0]->is_finished = false;
        $this->boatVersions[0]->save();
        $this->boatVersions[1]->is_finished = false;
        $this->boatVersions[1]->save();
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_experiences()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['type'] = 'wrong_type';

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['id'] = (string) ($this->boat->id-1);

        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $empty_boat = $this->getBoat($this->user_1);

        //act
        $this->patchJson($this->getRoute($empty_boat->id), $this->getRequestBody($empty_boat->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_validates_fields()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);


        //=====================PRODUCTION SITE/DOCKING PLACE
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['production_site'] = str_random(Constant::PRODUCTION_SITE_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['docking_place'] = str_random(Constant::DOCKING_PLACE_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //=====================MODEL/NAME
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['model'] = str_random(Constant::BOAT_MODEL_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['name'] = str_random(Constant::BOAT_NAME_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();


        //=====================CONSTRUCTION YEAR/RESTORATION YEAR
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['construction_year'] = 'not a date';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['construction_year'] = '5/4/4';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['construction_year'] = date('Y') + strtotime('+ 1 year');
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();



        //=====================TYPE/MATERIAL/MOTORS QUANTITY/MOTOR TYPE
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['type'] = 'not_a_valid_type';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['type'] = -10;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['type'] = 1.5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['type'] = 200;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();



        //=====================LENGTH
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['length'] = 'not_an_integer';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['length'] = -10;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['length'] = 1.5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['length'] = 65536;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['has_insurance'] = -3;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['has_insurance'] = 5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();



        //=====================LAT/LNG
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['lat'] = 'not a number';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['lat'] = -50000;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['lat'] = 50000;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['lat'] = true;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();


        //=====================IS_LUXURY
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['is_luxury'] = 'dsfsd';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //=====================EVENTS
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['events'] = 'dsfsd';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getBoatVersionStub(true);
        unset($stub['events']);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //=====================FEE
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['fee'] = 'dsfsd';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);


        //fee
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['fee'] = 0.5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);


        //fee
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['fee'] = -5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);


        //fee
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['fee'] = 101;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['model' => $stub['model']]);


        //=====================SEATS/BERTHS/TOILETS/CABINS
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['seats'] = 'dsfsd';
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['seats'] = 0.5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['seats'] = -5;
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();


        //=====================DESCRIPTION/INDICATIONS/RULES
        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['description'] = str_random(Constant::BOAT_DESCRIPTION_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['rules'] = str_random(Constant::BOAT_RULES_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

        //arrange
        $stub = $this->getBoatVersionStub(true);
        $stub['indications'] = str_random(Constant::BOAT_INDICATIONS_LENGTH + 1);
        $this->requestBody = $this->getRequestBody($this->boat->id, $stub);
        //act
        $this->patchJson($this->getRoute($this->id), $this->requestBody, $this->getHeaders($this->admin));
        //assert
        $this->assertValidationError();

    }


    // HELPERS
    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true)
    {
        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy')->create(['is_finished' => $isFinished]);
        if (isset($boat)) {
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionStub(bool $isFinished)
    {
        $requestBody = [
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->name,
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->name,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'fee' => $this->fake->numberBetween(0, 99),
            'is_luxury' => $this->fake->boolean(),
            'events' => $this->fake->boolean()
        ];

        if ($isFinished){
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getBoatVersionRelatedFields()
    {
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, 7),
                random_int(1, 7),
                random_int(1, 7),
                random_int(1, 7),
                random_int(1, 7),
                random_int(1, 7),
                random_int(1, 7),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'boats')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $boatId, array $stub)
    {
        return [
            'data' => [
                'type' => 'boats',
                'id' => (string)$boatId,
                'attributes' => $stub
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/boats/' . $id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->id = $this->boat->id;
        $this->boatVersions = [];
        $this->boatVersions[] = $this->getBoatVersion($this->boat);
        $this->boatVersions[] = $this->getBoatVersion($this->boat);
        $this->boatVersions[] = $this->getBoatVersion($this->boat);

        $this->boatVersions[0]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->save();

        $this->boatVersions[1]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->save();

        $this->boatVersions[2]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->save();

        $this->stub = $this->getBoatVersionStub(true);
        $this->requestBody = $this->getRequestBody($this->boat->id, $this->stub);
    }

    protected function setStatus(int ...$status){
        for ($i = 0; $i < count($this->boatVersions); $i++){
            $this->boatVersions[$i]->status = $status[$i];
            $this->boatVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\BoatVersion $boatVersion){
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['model' => $boatVersion->model]);
    }

    protected function assertNotEdited(\App\Models\BoatVersion $boatVersion){
        $this->dontSeeJson(['model' => $boatVersion->model]);
    }

    protected function getBoatVersionById(int $id){
        return \App\Models\BoatVersion::find($id);
    }

    protected function assertValidationError(){
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }
}