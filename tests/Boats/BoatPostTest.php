<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $admin;
    private $captain;
    private $oldBoats;
    private $requestBody;

    /**
     * ExperiencePostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/boats';
    }

    /** @test */
    public function it_returns_201_and_creates_a_new_boat_and_boatversion()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertCreated();
    }

    /** @test */
    public function it_returns_403_if_user_is_not_a_captain()
    {
        //arrange
        $this->setThingsUp();
        $this->captain->is_captain = false;
        $this->captain->save();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_404_if_user_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getRequestBody(User::all()->last()->id + 1), $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_201_and_creates_a_new_boat_and_boatversion_if_user_is_admin()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertCreated();
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_boats_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_403_if_user_not_boat_owner()
    {
        // OTHER USER
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->states('dummy')->create();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();


        // VISITOR
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_422_if_relationship_object_name_is_not_user()
    {
        //arrange
        $this->setThingsUp();
        unset($this->requestBody['data']['relationships']['user']);

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_422_if_relationship_object_type_is_not_users()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['user']['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_422_if_relationship_object_id_is_not_int()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['user']['data']['id'] = 'not an id';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_404_if_relationship_object_id_doesnt_exists()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['user']['data']['id'] = User::all()->last()->id + 1;

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }

    /** @test */
    public function it_returns_409_if_object_type_is_not_boats()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotCreated();
    }

    // HELPERS
    protected function getHeaders($account){
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
        $this->admin->save();

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $user = $users->random();
        $user->is_captain = true;
        $user->save();
        factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($user) {
                $b->user()->associate($user);
                $b->save();
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make(['is_finished' => false]));
            });

        $this->captain = $user;
        $this->oldBoats = Boat::all()->count();

        $this->requestBody = $this->getRequestBody($this->captain->id);
    }

    protected function getRequestBody(int $userId = null){
        $request = ['data' => ['type' => 'boats']];
        if (isset($userId)){
            $request['data']['relationships']['user']['data'] = ['type' => 'users', 'id' => (string) $userId];
        }

        return $request;
    }

    protected function assertCreated()
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);

        $boats = Boat::all();
        $boatVersions = BoatVersion::all();
        $new_boat = $boats->last();
        $new_boatVersion = $boatVersions->last();

        $this->assertCount($this->oldBoats + 1, Boat::all());
        $this->seeJson(["id" => (string) $new_boat->id]);
        $this->seeJson(["type" => 'boats']);
        $this->assertEquals($new_boat->id, $new_boatVersion->boat_id);
        $this->assertEquals(false, $new_boatVersion->is_finished);
        $this->assertEquals(null, $new_boatVersion->status);
    }

    protected function assertNotCreated()
    {
        $this->assertCount($this->oldBoats, Boat::all());
        $this->dontSeeJson(["type" => 'boats']);
    }
}