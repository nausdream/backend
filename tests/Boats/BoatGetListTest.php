<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceTranslationTitle;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $admin;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/boats';
        $this->type = 'boats';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        // AUTHENTICATED USER
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route, $this->getHeaders(User::all()->random()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        // VISITOR
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_boats_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_last_finished_boat_versions_if_within_admin_area()
    {
        //arrange
        $this->setThingsUp();
        $smallArea = factory(Area::class)->create(['point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 5, 'point_b_lng' => 5]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        //act
        $this->getJson($this->route . '?per_page=50', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empy_list_if_there_are_no_boats()
    {
        //arrange
        if (Boat::all()->count() == 0){
            factory(\App\Models\Area::class)->states('world')->create();

            $this->admin = factory(Administrator::class)->create();
            $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
            $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
            $this->admin->save();

            //act
            $this->getJson($this->route, $this->getHeaders($this->admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }

    }

    /**
     * @test
     */
    public function it_returns_200_and_empy_list_if_there_are_no_finished_boat_versions()
    {
        if (\App\Models\BoatVersion::all()->count() == 0) {
            //arrange
            factory(\App\Models\User::class, 5)->states('dummy')->create();
            factory(\App\Models\Area::class)->states('world')->create();

            $this->admin = factory(Administrator::class)->create();
            $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
            $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
            $this->admin->save();

            //create boats
            $boat_1 = factory(\App\Models\Boat::class)
                ->create();
            $boat_1->user()->associate(User::all()->random());
            $boat_1->save();
            $boatVersion_1 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
            $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
            $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

            $boat_2 = factory(\App\Models\Boat::class)
                ->create();
            $boat_2->user()->associate(User::all()->random());
            $boat_2->save();
            $boatVersion_3 = factory(BoatVersion::class)->create(['lat' => -1, 'lng' => 1]);
            $boatVersion_4 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
            $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

            $boat_3 = factory(\App\Models\Boat::class)
                ->create();
            $boat_3->user()->associate(User::all()->random());
            $boat_3->save();
            $boatVersion_5 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
            $boatVersion_6 = factory(BoatVersion::class)->create(['lat' => -1, 'lng' => 1]);
            $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

            //act
            $this->getJson($this->route . '?per_page=50', $this->getHeaders($this->admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_empy_list_if_there_are_no_finished_experience_versions_in_admin_area()
    {
        if (\App\Models\BoatVersion::all()->count() == 0) {
            //arrange
            factory(\App\Models\User::class, 5)->states('dummy')->create();
            $area = factory(\App\Models\Area::class)->create(['point_a_lat' => 0, 'point_b_lat' => 0]);

            $this->admin = factory(Administrator::class)->create();
            $this->admin->area()->associate($area->id);
            $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
            $this->admin->save();

            //create boats
            $boat_1 = factory(\App\Models\Boat::class)
                ->create();
            $boat_1->user()->associate(User::all()->random());
            $boat_1->save();
            $boatVersion_1 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
            $boatVersion_2 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
            $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

            $boat_2 = factory(\App\Models\Boat::class)
                ->create();
            $boat_2->user()->associate(User::all()->random());
            $boat_2->save();
            $boatVersion_3 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
            $boatVersion_4 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
            $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

            $boat_3 = factory(\App\Models\Boat::class)
                ->create();
            $boat_3->user()->associate(User::all()->random());
            $boat_3->save();
            $boatVersion_5 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
            $boatVersion_6 = factory(BoatVersion::class)->states('finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
            $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

            //act
            $this->getJson($this->route . '?per_page=50', $this->getHeaders($this->admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }
    }

    // HELPERS
    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
        $this->admin->save();

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $boats = factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($users) {
                $b->user()->associate($users->random());
                $b->save();
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make(['is_finished' => false]));
            });

        $this->boat = $boats->random();
        $this->captain = $this->boat->user()->first();
    }
}