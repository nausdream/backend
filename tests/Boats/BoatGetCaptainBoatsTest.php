<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceTranslationTitle;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class BoatGetCaptainBoatsTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $type;
    private $admin;
    private $captain;

    /**
     * ExperienceGetCaptainExperiencesTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'boats';
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_last_finished_in_admin_area_else_nothing()
    {
        //arrange
        $this->setThingsUp();
        $smallArea = factory(Area::class)->create(['point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 5, 'point_b_lng' => 5]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true&per_page=50&language=en', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function if_user_OR_admin_without_boats_permission_it_returns_403()
    {
        // RANDOM USER
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);


        // ADMIN WITH NO PERMISSION
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        $token = JwtService::getTokenStringFromAccount($this->captain); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true&per_page=50&language=en', ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->captain->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true&per_page=50&language=en', ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }


    /**
     * ?editing=true
     *
     * @test
     */
    public function if_captain_it_returns_a_list_of_last_boat_version()
    {
        //arrange
        $this->setThingsUp();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=true&per_page=50&language=en', $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_last_finished_accepted_boat_version()
    {
        //ADMIN WITH PERMISSIONS
        //arrange
        $this->setThingsUp();
        $smallArea = factory(Area::class)->create(['point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 5, 'point_b_lng' => 5]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=false&per_page=50&language=en', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);


        //ADMIN WITHOUT PERMISSIONS
        //arrange
        $this->setThingsUp();
        $smallArea = factory(Area::class)->create(['point_a_lat' => 0, 'point_a_lng' => 0, 'point_b_lat' => 5, 'point_b_lng' => 5]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();
        $this->admin->authorizations()->detach();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=false&per_page=50&language=en', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);

        //RANDOM USER
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->states('dummy')->create();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=false&per_page=50&language=en', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);



        //CAPTAIN
        //arrange
        $this->setThingsUp();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=false&per_page=50&language=en', $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);


        //VISITOR
        //arrange
        $this->setThingsUp();

        //create boats
        $boat_1 = factory(\App\Models\Boat::class)
            ->create();
        $boat_1->user()->associate(User::all()->random());
        $boat_1->save();
        $boatVersion_1 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_2 = factory(BoatVersion::class)->create(['lat' => 1, 'lng' => 1]);
        $boat_1->boatVersions()->saveMany([$boatVersion_1, $boatVersion_2]);

        $boat_2 = factory(\App\Models\Boat::class)
            ->create();
        $boat_2->user()->associate(User::all()->random());
        $boat_2->save();
        $boatVersion_3 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_4 = factory(BoatVersion::class)->states('dummy', 'finished')->create(['lat' => 1, 'lng' => 1, 'status' => Constant::STATUS_ACCEPTED_CONDITIONALLY]);
        $boat_2->boatVersions()->saveMany([$boatVersion_3, $boatVersion_4]);

        $boat_3 = factory(\App\Models\Boat::class)
            ->create();
        $boat_3->user()->associate(User::all()->random());
        $boat_3->save();
        $boatVersion_5 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => 1, 'lng' => 1]);
        $boatVersion_6 = factory(BoatVersion::class)->states('dummy', 'finished', 'accepted')->create(['lat' => -1, 'lng' => 1]);
        $boat_3->boatVersions()->saveMany([$boatVersion_5, $boatVersion_6]);

        $this->captain->boats()->saveMany([$boat_1, $boat_2, $boat_3]);

        //act
        $this->getJson($this->getRoute($this->captain) . '?editing=false&per_page=50&language=en');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["docking_place" => $boatVersion_1->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_3->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_2->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_4->docking_place]);
        $this->dontSeeJson(["docking_place" => $boatVersion_5->docking_place]);
        $this->seeJson(["docking_place" => $boatVersion_6->docking_place]);
        $this->seeJson(['current_page' => 1]);
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_200_and_empty_list_if_boat_not_found()
    {
        //arrange
        $user = factory(User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($user) . '?per_page=50');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_200_and_empty_list_if_boat_version_not_found()
    {
        //arrange
        $user = factory(User::class)->states('dummy')->create();
        $boat = factory(\App\Models\Boat::class)
            ->create();
        $user->boats()->save($boat);

        //act
        $this->getJson($this->getRoute($user) . '?per_page=50');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_404_if_captain_not_found()
    {
        //arrange
        $lastUser = User::all()->last();
        $userId = isset($lastUser) ? $lastUser->id+1 : 1;

        //act
        $this->getJson('/v1/captains/' . $userId . '/boats' . '?editing=true&per_page=100', $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }


    // HELPERS
    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'boats')->first()->id);
        $this->admin->save();

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $boats = factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($users) {
                $b->user()->associate($users->random());
                $b->save();
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make(['is_finished' => false]));
            });

        $this->boat = $boats->random();
        $this->captain = $this->boat->user()->first();
    }

    protected function getRoute(User $captain)
    {
        return 'v1/captains/' . $captain->id . '/boats';
    }
}