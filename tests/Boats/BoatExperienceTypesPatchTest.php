<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatExperienceTypesPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat_1;
    public $id;
    public $boatVersions;
    public $requestBody;
    public $stub;

    // ANYONE
    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $requestBody = $this->requestBody;
        unset($requestBody['data'][0]['type']);
        $requestBody['data'][0]['type'] = 'wrong type';

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_422_if_object_id_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $requestBody = $this->requestBody;
        $requestBody['data'][0]['id'] = \App\Models\ExperienceType::all()->last()->id + 1;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $empty_boat = $this->getBoat($this->user_1);
        $requestBody = $this->requestBody;

        //act
        $this->patchJson($this->getRoute($empty_boat->id), $requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }




    // ADMIN
    /** @test */
    public function it_returns_204_and_edit_last_finished_boat_version_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NO_CONTENT);
        $this->assertEdited($this->boat_1);

    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[2]->lat = -5001;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_boats_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_finished_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->boatVersions[0]->is_finished = false;
        $this->boatVersions[0]->save();
        $this->boatVersions[1]->is_finished = false;
        $this->boatVersions[1]->save();
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }




    // USER
    /** @test */
    public function it_returns_204_and_edit_last_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_boat_version_is_not_accepted_conditionally()
    {

        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->requestBody, $this->getHeaders($this->user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $boat = $this->getBoat($this->user_1);

        //act
        $this->patchJson($this->getRoute($boat->id), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_boat()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $boats = \App\Models\Boat::all();
        $boatId = $boats->count()>0 ? $boats->last()->id + 1 : 1;

        //act
        $this->patchJson($this->getRoute($boatId), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->boat_1);
    }




    //helpers
    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true){
        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy', 'accepted')->create(['is_finished' => $isFinished]);
        if (isset($boat)){
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
//        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['boat_types']);
//        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'fee' => $this->fake->numberBetween(10, 50)
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'boat_types' => [
                random_int(1, 8),
                random_int(1, 8),
                random_int(1, 8),
                random_int(1, 8),
                random_int(1, 8),
                random_int(1, 8),
                random_int(1, 8),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null){
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'boats')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account){
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin){
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRandomAccessoryId(){
        return \App\Models\Accessory::inRandomOrder()->get()->first()->id;
    }

    public function getRequestBody(...$ids){
        if (! isset($ids)){
            for ($i = 0; $i<4; $i++){
                $ids[] = $this->getRandomAccessoryId();
            }
            array_unique($ids);
        }

        $requestBody = [];

        foreach ($ids as $id) {
            $requestBody['data'][] = ['type' => 'experience-types', 'id' => (string) $id];
        }

        return $requestBody;
    }

    public function getRoute(int $boatId){
        return 'v1/boats/'.$boatId.'/relationships/experience-types';
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat_1 = $this->getBoat($this->user_1);
        $this->id = $this->boat_1->id;
        $this->boatVersions = [];
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->requestBody = $this->getRequestBody(1, 2);
    }

    protected function setStatus(int ...$status){
        for ($i = 0; $i < count($this->boatVersions); $i++){
            $this->boatVersions[$i]->status = $status[$i];
            $this->boatVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\Boat $boat){
        $this->assertResponseStatus(SymfonyResponse::HTTP_NO_CONTENT);
        $this->assertCount(1, $boat->experienceTypes()->where('experience_type_id', (int) $this->requestBody['data'][0]['id'])->get());
    }

    protected function assertNotEdited(\App\Models\Boat $boat){
        $this->assertCount(0, $boat->experienceTypes()->where('experience_type_id', (int) $this->requestBody['data'][0]['id'])->get());
    }
}