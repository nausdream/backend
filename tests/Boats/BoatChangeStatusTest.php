<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatChangeStatusTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat_1;
    public $id;
    public $boatVersions;
    public $requestBody;

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_accepted_and_sends_email()
    {
        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED;
        $requestBody = $this->getRequestBody($this->id, $status);
        $requestBody['data']['attributes']['admin_text'] = null;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id,
            \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_rejected_and_sends_email()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_REJECTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_accepted_conditionally_and_sends_email()
    {
        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED_CONDITIONALLY;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_422_if_status_is_not_in_status_array()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);


        //act NEGATIVE NUMBER
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, 'not a status'), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

        //act OTHER OBJECT
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, new \App\Models\User()), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

    }

    /** @test */
    public function it_returns_422_if_admin_text_not_set_when_changing_to_accepted_conditionally_or_rejected()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $requestBody = $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        unset($requestBody['data']['attributes']['admin_text']);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

        //arrange
        $requestBody = $this->getRequestBody($this->id, Constant::STATUS_REJECTED);
        unset($requestBody['data']['attributes']['admin_text']);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

    }

    /** @test */
    public function it_returns_403_if_changing_from_any_status_to_the_same_status()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_PENDING;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $status = Constant::STATUS_ACCEPTED_CONDITIONALLY;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);
        $status = Constant::STATUS_REJECTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_422_if_changing_from_any_to_null()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, null), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, null), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, null), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, null), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_null_to_any()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, null);
        $status = Constant::STATUS_PENDING;

        //act PENDING
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $status = Constant::STATUS_ACCEPTED_CONDITIONALLY;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);
        $status = Constant::STATUS_REJECTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_any_to_pending()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_PENDING;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_accepted_to_any()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        $status = Constant::STATUS_PENDING;

        //act PENDING
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
        $status = Constant::STATUS_ACCEPTED_CONDITIONALLY;

        //act ACCEPTED_CONDITIONALLY
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
        $status = Constant::STATUS_REJECTED;

        //act REJECTED
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->boatVersions[2]->lat = -5001;
        $this->boatVersions[2]->save();
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_boats_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->admin->authorizations()->detach();
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_finished_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->boatVersions[0]->is_finished = false;
        $this->boatVersions[0]->save();
        $this->boatVersions[1]->is_finished = false;
        $this->boatVersions[1]->save();
        $this->boatVersions[2]->is_finished = false;
        $this->boatVersions[2]->save();
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id),
            $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_boats()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        $requestBody = $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey(Constant::STATUS_ACCEPTED, Constant::STATUS_NAMES));
        unset($requestBody['data']['type']);
        $requestBody['data']['type'] = 'wrong_type';

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $requestBody = $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey(Constant::STATUS_ACCEPTED, Constant::STATUS_NAMES));
        unset($requestBody['data']['id']);
        $requestBody['data']['id'] = (string)($this->boat_1->id - 1);

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_boat_version()
    {
        //arrange
        $this->setThingsUp();
        $empty_boat = $this->getBoat($this->user_1);

        //act
        $this->patchJson($this->getRoute($empty_boat->id),
            $this->getRequestBody($empty_boat->id, \App\Services\ValueKeyHelper::getValueByKey(Constant::STATUS_ACCEPTED, Constant::STATUS_NAMES)),
            $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

    /** @test */
    public function it_validates_status()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->boat_1->id), $this->getRequestBody($this->id, \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getBoatVersionById($this->boatVersions[1]->id), $status);
    }

    // HELPERS
    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true)
    {
        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy', 'accepted')->create(['is_finished' => $isFinished]);
        if (isset($boat)) {
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['boat_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'boat_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'boats')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $boatId, $status)
    {
        return [
            'data' => [
                'type' => 'boats',
                'id' => (string)$boatId,
                'attributes' => [
                    'status' => $status,
                    'admin_text' => $this->fake->text
                ]
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/boats/' . $id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat_1 = $this->getBoat($this->user_1);
        $this->id = $this->boat_1->id;
        $this->boatVersions = [];
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);
        $this->boatVersions[] = $this->getBoatVersion($this->boat_1);

        $this->requestBody = $this->getRequestBody($this->boat_1->id, Constant::STATUS_ACCEPTED);
    }

    protected function setStatus(...$status)
    {
        for ($i = 0; $i < count($this->boatVersions); $i++) {
            $this->boatVersions[$i]->status = $status[$i];
            $this->boatVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\BoatVersion $boatVersion, int $status)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['status' => \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)]);
        $this->assertEquals($status, $boatVersion->status);
    }

    protected function assertNotEdited(\App\Models\BoatVersion $boatVersion)
    {
        $this->dontSeeJson(['departure_port' => $boatVersion->departure_port]);
    }

    protected function getBoatVersionById(int $id)
    {
        return \App\Models\BoatVersion::find($id);
    }
}