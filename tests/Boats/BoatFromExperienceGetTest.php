<?php

/**
 * User: Giuseppe
 * Date: 06/02/2017
 * Time: 18:17
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Authorization;
use App\Models\Boat;
use App\Models\Country;
use App\Models\Language;
use App\Services\PhotoHelper;
use App\TranslationModels\Language as TranslationLanguage;
use App\Models\Currency;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use JD\Cloudder\Facades\Cloudder;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;

class BoatFromExperienceGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route;
    protected $admin;
    protected $captain;
    protected $boat;
    protected $experience;

    /**
     * Token validity is evaluated first: the test will fail if validation fails.
     *
     * @test
     */
    public function it_returns_404_if_experience_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $experienceId = \App\Models\Experience::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($experienceId), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'experience_not_found']);
    }

    /*
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($this->experience->id), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }*/

    /*

    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();


        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->experience->id), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }
    */

    /** @test */
    public function it_returns_200_if_experience_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->experience->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'boats']);
        $this->seeJson(['id' => (string)$this->boat->id]);
    }

    /*
    public function it_returns_403_if_experience_exist_but_admin_has_not_permission_to_show_boat_captain()
    {
        //arrange
        $this->setThingsUp();

        $this->admin->authorizations()->detach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);

        //act
        $this->getJson($this->getRoute($this->experience->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this boat data']);
    } */

    protected function getJwtString($id)
    {
        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time())// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() + 3600000)// Configures the expiration time of the token (exp claim)
            ->setSubject($id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        return $token;
    }

    public function createUser()
    {
        $user = factory(User::class)->make();

        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->docking_place = $this->fake->city;
        $user->first_name_skipper = $this->fake->firstName;
        $user->last_name_skipper = $this->fake->lastName;
        $user->skippers_have_license = $this->fake->boolean;
        $user->first_name_contact = $this->fake->firstName;
        $user->last_name_contact = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->is_captain = $this->fake->boolean;
        $user->is_skipper = $this->fake->boolean;
        $user->sex = $this->fake->randomElement(array('m', 'f', null));
        $user->birth_date = $this->fake->date();
        $user->nationality = Country::inRandomOrder()->first()->name;
        $user->currency = Currency::inRandomOrder()->first()->name;
        $user->language = TranslationLanguage::inRandomOrder()->first()->language;
        $user->description = $this->fake->text();
        $user->enterprise_name = $this->fake->name;
        $user->vat_number = str_random(Constant::ENTERPRISE_VAT_LENGTH);
        $user->enterprise_address = $this->fake->address;
        $user->enterprise_city = $this->fake->city;
        $user->enterprise_zip_code = $this->fake->postcode;
        $user->enterprise_country = Country::inRandomOrder()->first()->name;
        $user->has_card = $this->fake->boolean;
        $user->default_fee = $this->fake->numberBetween(1, 30);
        $user->photo_version = null;
        $user->is_partner = false;
        $user->is_mail_activated = true;
        $user->is_suspended = false;
        $user->newsletter = $this->fake->boolean;
        $user->fuid = $this->fake->numberBetween(1);
        $user->is_phone_activated = true;
        $user->card_price = $this->fake->numberBetween(0);

        return $user;
    }

    protected function setThingsUp()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        $this->admin = $admin;

        $captain = factory(\App\Models\User::class)->make();
        $captain->first_name = $this->fake->firstName;
        $captain->last_name = $this->fake->lastName;
        $captain->email = $this->fake->email;
        $captain->phone = $this->fake->phoneNumber;
        $captain->save();

        $this->captain = $captain;

        $boat = factory(Boat::class)->create();
        $boat->user()->associate($this->captain);
        $boat->save();

        $this->boat = $boat;

        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy')->create(['is_finished' => true]);
        if (isset($boat)) {
            $this->boat->boatVersions()->save($boatVersion);
        }

        $experience = factory(\App\Models\Experience::class)->create();
        $experience->boat()->associate($this->boat);
        $experience->save();

        $this->experience = $experience;

        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->states('dummy')->create(['is_finished' => true]);
        if (isset($experience)) {
            $this->experience->experienceVersions()->save($experienceVersion);
        }
    }

    protected function getRoute($experienceId)
    {
        return 'v1/experiences/' . $experienceId . '/boat';
    }
}