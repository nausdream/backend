<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ConversationsGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $admin;
    private $conversations;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/conversations';
        $this->type = 'conversations';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = \App\Models\User::all()->random();

        //act
        $this->getJson($this->route, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_conversations_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_conversations()
    {
        //arrange
        $this->setThingsUp();
        $conversation = $this->conversations->get(2);
        $conversation->updated_at = date('Y-m-d', strtotime('+3 months'));
        $conversation->save();
        $captain = factory(\App\Models\User::class)->states('dummy', 'captain')->create();
        $captain->first_name = 'fake_name';
        $captain->save();
        $randomConversation = $this->conversations->get(5);
        $randomConversation->updated_at = '2015-12-29';
        $randomConversation->captain()->associate($captain);
        $randomConversation->save();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
        $this->seeJson(['first_name_captain' => $conversation->captain()->first()->first_name]);
        $this->dontSeeJson(['first_name_captain' => $randomConversation->captain()->first()->first_name]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_conversations()
    {
        if (\App\Models\Conversation::all()->count() == 0) {
            $admin = factory(Administrator::class)->create();
            $admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);
            //act
            $this->getJson($this->route, $this->getHeaders($admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }
    }


    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });
        $this->conversations = new \Illuminate\Database\Eloquent\Collection();
        $bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $users) {
                $e->user()->associate($users->random());
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->conversation()->associate(factory(\App\Models\Conversation::class)->states('dummy')->create());
                $e->save();
                $this->conversations->add($e->conversation()->first());
            });
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}