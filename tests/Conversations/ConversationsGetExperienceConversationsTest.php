<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ConversationsGetExperienceConversationsTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $experience;
    private $captain;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_conversations_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_request_user_is_not_conversation_user_or_captain()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_experience_conversations()
    {
        //arrange
        $this->setThingsUp();
        $recentConversation = $this->experience->conversations()->get()->get(0);
        $recentConversation->updated_at = '2018-12-29';
        $recentConversation->captain()->associate(factory(\App\Models\User::class)->states('dummy', 'captain')->create(['first_name' => 'JohnSmith']));
        $recentConversation->save();
        $oldConversation = $this->experience->conversations()->get()->get(1);
        $oldConversation->updated_at = '2012-12-29';
        $oldConversation->captain()->associate(factory(\App\Models\User::class)->states('dummy', 'captain')->create(['first_name' => 'SamWhite']));
        $oldConversation->save();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
        $this->seeJson(['first_name_captain' => $recentConversation->captain()->first()->first_name]);
        $this->dontSeeJson(['first_name_captain' => $oldConversation->captain()->first()->first_name]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_conversations()
    {
        //arrange
        $this->setThingsUp();
        $this->experience->conversations()->delete();

        //act
        $this->getJson($this->getRoute(), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
    }


    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy')->make());
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });
        $this->experience = $experiences->random();
        $this->captain = $captains->random();
        $conversations = new \Illuminate\Database\Eloquent\Collection();
        $bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $conversations, $users) {
                $user = $users->random();
                $e->user()->associate($user);
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create(['user_id' => $user->id, 'captain_id' => $this->captain->id]);
                $this->experience->conversations()->save($conversation);
                $e->conversation()->associate($conversation);
                $e->save();
                $conversations->add($conversation);
            });
    }

    public function getRoute(int $experienceId = null)
    {
        return 'v1/experiences/' . ($experienceId ?? $this->experience->id) . '/conversations';
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}