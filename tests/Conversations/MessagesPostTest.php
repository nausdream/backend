<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class MessagesPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;
    private $type;
    private $admin;
    private $user;
    private $conversation;
    private $requestBody;
    private $messages;

    /**
     * MessagesPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/messages';
        $this->type = 'messages';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_admin()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_part_of_the_conversation()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_201_and_saves_a_new_message_if_user()
    {
        //USER
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $captain = $this->conversation->captain()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }

    /**
     * @test
     */
    public function it_returns_201_and_saves_a_new_message_if_captain()
    {
        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $captain = $this->conversation->captain()->first();
        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders($captain));
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $guest = $this->conversation->user()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($guest->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }


    /**
     * @test
     */
    public function it_returns_201_and_saves_a_new_message_if_there_is_still_not_a_booking_for_conversation()
    {
        //USER
        //arrange
        $this->setThingsUp();
        $bookings = $this->conversation->bookings()->get();
        foreach ($bookings as $booking) {
            $booking->conversation_id = null;
            $booking->save();
        }

        //act
        $this->postJson($this->route, $this->getRequestBody(), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $captain = $this->conversation->captain()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }



    /**
     * @test
     */
    public function it_saves_relationships_correctly()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        $createdMessage = \App\Models\Message::all()->last();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals($this->conversation->id, $createdMessage->conversation()->first()->id);
        $this->seeJson(['type' => 'conversations']);
        $captain = $this->conversation->captain()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_messages()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'not messages';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_404_if_conversation_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['conversation']['data']['id'] = \App\Models\Conversation::all()->count() > 0 ? \App\Models\Conversation::all()->last()->id+1 : 1;

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = '';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = '      ';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function it_replaces_emails_with_asterisks()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum dolor@sit.amet';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);
        $captain = $this->conversation->captain()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }

    /**
     * @test
     */
    public function it_replaces_websites_with_asterisks()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum www.pippo.net';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum www.pippo.net';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum http://www.pippo.net';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum http://pippo.net';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum http://pip-po.net';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);
    }

    /**
     * @test
     */
    public function it_replaces_telephone_numbers_with_asterisks()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum 070/5421212';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum 070/***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum 0705421212';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum 070 5421212';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum 070 ***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum 02-05421212';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum 02-***', $this->conversation->messages()->get()->last()->message);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum +66 02-0542-1212';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals('Lorem ipsum +66 02-0542-1212', $this->conversation->messages()->get()->last()->message);
    }

    /**
     * @test
     */
    public function it_sets_alert_field_on_the_conversation_if_necessary()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = 'Lorem ipsum dolor@sit.amet';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals(true, \App\Models\Message::all()->last()->conversation()->first()->alert);
        $captain = $this->conversation->captain()->first();
        $this->seeEmailsSent(2);
        $this->seeEmailTo($captain->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
    }


    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
                    "date_start" => date('Y-m-d', strtotime('-1 month')),
                    "date_end" => date('Y-m-d', strtotime('+1 month'))
                ]));
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });

        $conversations = new \Illuminate\Database\Eloquent\Collection();
        $bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($users, $experiences, $conversations) {
                $e->user()->associate($users->random());
                $experienceVersion = $experiences->random()->experienceVersions()->get()->where('is_finished', true)->last();
                $e->experienceVersion()->associate($experienceVersion);

                $seoController = new \App\Http\Controllers\v1\SeosController();
                $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
                $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);

                $conversation = factory(\App\Models\Conversation::class)->states('dummy')->create(['alert' => false]);
                $e->conversation()->associate($conversation);
                $conversations->add($conversation);
                $e->save();
            });
        $this->conversation = $conversations->random();
        $experience = $experiences->random();
        $this->conversation->experience()->associate($experience);
        $this->conversation->save();

        $seoController = new \App\Http\Controllers\v1\SeosController();
        $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
        $captain = $experience->boat()->first()->user()->first();
        $seoController->createOrUpdateSeo($experienceVersion->title, $experienceVersion->description, $captain->language, $experienceVersion->id);


        $i=1;
        factory(\App\Models\Message::class, 10)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) use ($i) {
                if ($i % 2 == 0){
                    $m->user()->associate($this->conversation->user()->first());
                }
                else {
                    $m->user()->associate($this->conversation->captain()->first());
                }
                $m->conversation()->associate($this->conversation);
                $m->save();
                $i++;
            });
        $this->messages = \App\Models\Message::all();

        $this->user = $this->conversation->user()->first();
        $this->requestBody = $this->getRequestBody();
    }

    protected function getExperience(){
        $experience = factory(\App\Models\Experience::class)
            ->create();
        $experience->boat()->associate(\App\Models\Boat::all()->random());
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $experience->save();

        return $experience;
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->user)];
    }

    protected function getRequestBody(\App\Models\Conversation $conversation = null)
    {
        if (!isset($conversation)){
            $conversation = $this->conversation;
        }
        $requestBody = [
            "data" => [
                "type" => "messages",
                "attributes" => [
                    "message" => $this->fake->text
                ],
                "relationships" => [
                    "conversation" => [
                        "data" => [
                            "type" => "conversations",
                            "id" => $conversation->id
                        ]
                    ]
                ]
            ]
        ];

        return $requestBody;
    }

    private function assertNotCreated()
    {
        $this->assertCount($this->messages->count(), \App\Models\Message::all());
    }

    private function assertCreated()
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCount($this->messages->count()+1, \App\Models\Message::all());
        $this->seeJson(['id' => (string)(\App\Models\Message::all()->last()->id)]);
    }
}