<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\StatusHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class MessagesPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $message;
    private $requestBody;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->states('dummy')->create();

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_conversations_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_200_and_edit_message()
    {
        //ADMIN
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertEdited();
    }

    /**
     * @test
     */
    public function it_returns_404_if_message_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeMessageId = \App\Models\Message::all()->count() != 0 ? \App\Models\Message::all()->last()->id + 1 : 1;

        //act
        $this->patchJson($this->getRoute($fakeMessageId), $this->getRequestBody($fakeMessageId), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_messages()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'not messages';

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_returns_409_if_message_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['id'] = $this->message->id + 1;

        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited();
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = '';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = '        ';
        //act
        $this->patchJson($this->getRoute(), $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited();
    }

    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions){
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                foreach ($e->experienceVersions()->get() as $experienceVersion) {
                    $experienceVersions->add($experienceVersion);
                }
            });
        $conversations = new \Illuminate\Database\Eloquent\Collection();
        $bookings = factory(\App\Models\Booking::class, 7)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $users, $conversations) {
                $e->user()->associate($users->random());
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->conversation()->associate(factory(\App\Models\Conversation::class)->states('dummy')->create());
                $e->save();
                $conversations->add($e->conversation()->first());
            });

        $conversation = $conversations->random();
        $conversation->experience()->associate($experiences->random());
        $conversation->save();

        $i=1;
        $messages = factory(\App\Models\Message::class, 10)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) use ($conversation, $i) {
                if ($i % 2 == 0){
                    $m->user()->associate($conversation->user()->first());
                }
                else {
                    $m->user()->associate($conversation->captain()->first());
                }
                $m->conversation()->associate($conversation);
                $m->save();
                $i++;
            });
        $this->message = $messages->random();
        $this->requestBody = $this->getRequestBody();
    }

    public function getRoute(int $messageId = null)
    {
        return 'v1/messages/' . ($messageId ?? $this->message->id);
    }

    protected function getRequestBody(int $id = null)
    {
        $requestBody = [
            "data" => [
                "type" => "messages",
                "id" => $id ?? $this->message->id,
                "attributes" => [
                    "message" => $this->fake->text
                ]
            ]
        ];

        return $requestBody;
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }

    private function assertNotEdited()
    {
        $this->dontSeeJson(['id' => (string)$this->message->id]);
        $this->dontSeeJson(['message' => $this->requestBody['data']['attributes']['message']]);
    }

    private function assertEdited()
    {
        $this->seeJson(['id' => (string)$this->message->id]);
        $this->seeJson(['message' => $this->requestBody['data']['attributes']['message']]);
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }
}