<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\StatusHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ConversationsGetSingleTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $conversation;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_in_the_conversation()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_conversations_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_200_and_a_single_conversation()
    {
        //ADMIN
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['first_name_guest' => $this->conversation->user()->first()->first_name]);
        $this->seeJson(['last_name_guest' => $this->conversation->user()->first()->last_name]);
        $this->assertCount(0, $this->conversation->messages()->where('read', true)->get());

        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $captain = $this->conversation->captain()->first();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);


        //USER
        //arrange
        $this->setThingsUp();
        $user = $this->conversation->user()->first();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /**
     * @test
     */
    public function it_sets_read_to_true_on_messages_if_user_or_captain()
    {
        //CAPTAIN
        //arrange
        $this->setThingsUp();
        $captain = $this->conversation->captain()->first();
        $user = $this->conversation->user()->first();

        $this->assertNotEquals(0, $this->conversation->messages()->where('user_id', $user->id)->where('read', false)->get()->count());
        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->assertCount(0, $this->conversation->messages()->where('user_id', $user->id)->where('read', false)->get());


        //USER
        //arrange
        $this->setThingsUp();
        $captain = $this->conversation->captain()->first();
        $user = $this->conversation->user()->first();

        $this->assertNotEquals(0, $this->conversation->messages()->where('user_id', $captain->id)->where('read', false)->get()->count());
        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->assertCount(0, $this->conversation->messages()->where('user_id', $captain->id)->where('read', false)->get());
    }

    /**
     * @test
     */
    public function it_returns_404_if_conversation_id_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeConversationId = \App\Models\Conversation::all()->count() != 0 ? \App\Models\Conversation::all()->last()->id + 1 : 1;

        //act
        $this->getJson($this->getRoute($fakeConversationId), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_includes_last_finished_experience_version_in_the_correct_language_if_booking_is_rejected_or_cancelled()
    {
        //THERE'S A BOOKING, AND AN EXPERIENCEVERSION WHERE TITLE HAS ADMIN LANGUAGE
        //arrange
        $this->setThingsUp();

        $booking = $this->conversation->bookings()->get()->first();
        $booking->conversation()->dissociate();
        $booking->save();

        $this->conversation->bookings()->saveMany([
            \App\Models\Booking::all()->get(0),
            \App\Models\Booking::all()->get(1),
            \App\Models\Booking::all()->get(2)
        ]);
        $booking = $this->conversation->bookings()->get()->last();
        $booking->status = Constant::BOOKING_STATUS_REJECTED;
        $booking->save();
        $experienceVersion = $this->conversation->experience()->first()->experienceVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();
        $this->admin->language = 'it';
        $this->admin->save();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['first_name_guest' => $this->conversation->user()->first()->first_name]);
        $this->seeJson(['last_name_guest' => $this->conversation->user()->first()->last_name]);
        $this->assertCount(0, $this->conversation->messages()->where('read', true)->get());



        //THERE'S A BOOKING, AND AN EXPERIENCEVERSION WHERE TITLE HAS NOT ADMIN LANGUAGE
        //arrange
        $this->setThingsUp();

        $booking = $this->conversation->bookings()->get()->first();
        $booking->conversation()->dissociate();
        $booking->save();

        $this->conversation->bookings()->saveMany([
            \App\Models\Booking::all()->get(0),
            \App\Models\Booking::all()->get(1),
            \App\Models\Booking::all()->get(2)
        ]);
        $booking = $this->conversation->bookings()->get()->last();
        $booking->status = Constant::BOOKING_STATUS_CANCELLED;
        $booking->save();
        $experienceVersion = $this->conversation->experience()->first()->experienceVersions()->where('status', Constant::STATUS_ACCEPTED)->get()->last();
        $this->admin->language = 'it';
        $this->admin->save();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['first_name_guest' => $this->conversation->user()->first()->first_name]);
        $this->seeJson(['last_name_guest' => $this->conversation->user()->first()->last_name]);
        $this->assertCount(0, $this->conversation->messages()->where('read', true)->get());
    }

    /**
     * @test
     */
    public function it_includes_booking_experience_version_if_booking_is_not_rejected_or_cancelled()
    {
        //arrange
        $this->setThingsUp();

        $booking = $this->conversation->bookings()->get()->first();
        $booking->conversation()->dissociate();
        $booking->save();

        $experience = factory(\App\Models\Experience::class)
            ->create();
        $experience->boat()->associate(\App\Models\Boat::all()->random());
        $experienceVersion_1 = factory(\App\Models\ExperienceVersion::class)->states('dummy', 'finished', 'accepted')->create();
        $experienceVersion_2 = factory(\App\Models\ExperienceVersion::class)->states('dummy', 'finished', 'accepted')->create();
        $experience->experienceVersions()->saveMany([$experienceVersion_1, $experienceVersion_2]);
        $experience->save();

        $this->conversation->bookings()->saveMany([
            \App\Models\Booking::all()->get(0),
            \App\Models\Booking::all()->get(1),
            \App\Models\Booking::all()->get(2)
        ]);
        $this->conversation->experience()->associate($experience);
        $this->conversation->save();

        $booking = $this->conversation->bookings()->get()->last();
        $booking->experienceVersion()->associate($experienceVersion_2);
        $booking->status = Constant::BOOKING_STATUS_ACCEPTED;
        $booking->save();
        $experienceVersion_2 = $booking->experienceVersion()->first();
        $this->admin->language = 'it';
        $this->admin->save();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['first_name_guest' => $this->conversation->user()->first()->first_name]);
        $this->seeJson(['last_name_guest' => $this->conversation->user()->first()->last_name]);
        $this->assertCount(0, $this->conversation->messages()->where('read', true)->get());
    }

    /**
     * @test
     */
    public function it_includes_other_relationships()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->conversation->id), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'messages']);
        $this->seeJson(['type' => 'bookings']);
        $this->seeJson(['price' => $this->conversation->bookings()->get()->last()->price]);
        $this->seeJson(['message' => $this->conversation->messages()->get()->last()->message]);
    }

    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions){
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                foreach ($e->experienceVersions()->get() as $experienceVersion) {
                    $experienceVersions->add($experienceVersion);
                }
            });
        $conversations = new \Illuminate\Database\Eloquent\Collection();
        $bookings = factory(\App\Models\Booking::class, 7)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $users, $conversations) {
                $e->user()->associate($users->random());
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->conversation()->associate(factory(\App\Models\Conversation::class)->states('dummy')->create());
                $e->save();
                $conversations->add($e->conversation()->first());
            });

        $this->conversation = $conversations->random();
        $this->conversation->experience()->associate($experiences->random());
        $this->conversation->save();

        factory(\App\Models\Message::class, 5)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) use ($users) {
                $m->user()->associate($this->conversation->user()->first());
                $m->conversation()->associate($this->conversation);
                $m->save();
            });
        factory(\App\Models\Message::class, 5)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) use ($captains) {
                $m->user()->associate($this->conversation->captain()->first());
                $m->conversation()->associate($this->conversation);
                $m->save();
            });
    }

    public function getRoute(int $conversationId = null)
    {
        return 'v1/conversations/' . ($conversationId ?? $this->conversation->id);
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }
}