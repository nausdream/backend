<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ConversationsPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $admin;
    private $user;
    private $conversations;
    private $requestBody;
    private $experiences;

    /**
     * ConversationsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/conversations';
        $this->type = 'conversations';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_admin()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_201_and_saves_a_new_conversation_if_there_isnt_already_one_for_the_same_user_experience_and_date()
    {
        //arrange
        $this->setThingsUp();
        $experience = $this->getExperience();
        $experience = \App\Models\Experience::find($this->requestBody['data']['relationships']['experience']['data']['id']);

        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('+11 months')),
            "date_end" => date('Y-m-d', strtotime('+12 month'))
        ]));

        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
    }

    /**
     * @test
     */
    public function it_returns_422_with_conversation_id_in_meta_object_if_conversation_for_same_date_and_experience_already_exists()
    {
        //arrange
        $this->setThingsUp();
        $experience = $this->getExperience();
        $conversation = factory(\App\Models\Conversation::class)->states('dummy')->make(
            [
                'request_date' => $this->requestBody['data']['attributes']['request_date'],
                'user_id' => $this->user->id
            ]
        );
        $experience->conversations()->save($conversation);
        $this->conversations->add($conversation);

        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['conversation_id' => $experience->conversations()->get()->last()->id]);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_422_if_date_is_not_present_in_some_experience_availability()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['request_date'] = date('Y-m-d', strtotime('+10 months'));
        $experience = \App\Models\Experience::find($this->requestBody['data']['relationships']['experience']['data']['id']);

        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('+11 months')),
            "date_end" => date('Y-m-d', strtotime('+12 month'))
        ]));

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_saves_relationships_correctly()
    {
        //arrange
        $this->setThingsUp();
        $experience = $this->getExperience();
        $this->requestBody['data']['relationships']['experience']['data']['id'] = $experience->id;

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        $createdConversation = \App\Models\Conversation::all()->last();
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCreated();
        $this->assertEquals($experience->id, $createdConversation->experience()->first()->id);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_conversations()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'not conversations';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['id'] = \App\Models\Experience::all()->count() > 0 ? \App\Models\Experience::all()->last()->id+1 : 1;

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_404_if_there_are_no_finished_experience_versions()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)->create();
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->make());

        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_403_if_last_finished_experience_version_is_rejected()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)->create();
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('finished')->make(
            ['status' => \App\Constant::STATUS_REJECTED]
        ));
        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_403_if_last_finished_experience_version_is_freezed()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)->create();
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('finished')->make(
            ['status' => \App\Constant::STATUS_FREEZED]
        ));
        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_returns_403_if_last_experience_version_is_freezed()
    {
        //arrange
        $this->setThingsUp();
        $experience = factory(\App\Models\Experience::class)->create();
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('finished')->make(
            ['status' => \App\Constant::STATUS_FREEZED]
        ));
        //act
        $this->postJson($this->route, $this->getRequestBody($experience), $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotCreated();
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['adults'] = -2;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['adults'] = 10.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['adults'] = 'not a number';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);



        //REQUEST_DATE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['request_date'] = 'not a date';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['request_date'] = 10.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['request_date'] = date('Y-m-d', strtotime('-5 days'));
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }




    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'conversations')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $this->experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy')->make());
                $e->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
                    "date_start" => date('Y-m-d', strtotime('-1 month')),
                    "date_end" => date('Y-m-d', strtotime('+1 month'))
                ]));
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });

        $bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($users) {
                $e->user()->associate($users->random());
                $e->experienceVersion()->associate($this->experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->conversation()->associate(factory(\App\Models\Conversation::class)->states('dummy')->create());
                $e->save();
            });
        $this->conversations = \App\Models\Conversation::all();
        $this->user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->requestBody = $this->getRequestBody();
    }

    protected function getExperience(){
        $experience = factory(\App\Models\Experience::class)
            ->create();
        $experience->boat()->associate(\App\Models\Boat::all()->random());
        $experience->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
        $experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
        $experience->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability([
            "date_start" => date('Y-m-d', strtotime('-1 month')),
            "date_end" => date('Y-m-d', strtotime('+1 month'))
        ]));
        $experience->save();

        return $experience;
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->user)];
    }

    protected function getRequestBody(\App\Models\Experience $experience = null)
    {
        if (!isset($experience)){
            $experience = $this->experiences->random();
        }
        $requestBody = [
            "data" => [
                "type" => "conversations",
                "attributes" => [
                    "request_date" => date('Y-m-d'),
                    "adults" => $this->fake->numberBetween(0, 7),
                    "kids" => $this->fake->numberBetween(0, 7),
                    "babies" => $this->fake->numberBetween(0, 7)
                ],
                "relationships" => [
                    "experience" => [
                        "data" => [
                            "type" => "experiences",
                            "id" => $experience->id
                        ]
                    ]
                ]
            ]
        ];

        return $requestBody;
    }

    private function assertNotCreated()
    {
        $this->assertCount($this->conversations->count(), \App\Models\Conversation::all());
    }

    private function assertCreated()
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCount($this->conversations->count()+1, \App\Models\Conversation::all());
        $this->seeJson(['id' => (string)(\App\Models\Conversation::all()->last()->id)]);
    }
}