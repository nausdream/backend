<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 03/11/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ResetPasswordTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    protected $user;
    protected $restoreRoute;
    protected $resetRoute;
    protected $restoreStub;
    protected $resetStub;
    protected $unhashedPassword;

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->restoreRoute = 'v1/auth/restore';
        $this->resetRoute = 'v1/auth/reset';
    }

    /** @test */
    public function it_gets_422_if_email_is_not_valid_or_present()
    {
        //arrange
        $this->setThingsUp();
        $this->restoreStub['meta']['email'] = $this->fake->firstName;

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        unset($this->restoreStub['meta']['email']);

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_404_if_email_does_not_exist_on_database()
    {
        //arrange
        $this->setThingsUp();
        $is_edited = false;
        do {
            $randomEmail = $this->fake->email;
            if ($randomEmail != $this->restoreStub['meta']['email']) {
                $this->restoreStub['meta']['email'] = $randomEmail;
                $is_edited = true;
            }
        } while ($is_edited == false);

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'email_not_found']);
    }

    /** @test */
    public function it_gets_422_if_email_exist_but_not_activated()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_mail_activated = 0;
        $this->user->save();

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email_not_activated']);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_captain_or_partner()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_captain = 0;
        $this->user->is_partner = 0;
        $this->user->save();

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner']);
    }

    /** @test */
    public function it_gets_200_if_parameters_are_ok()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->restoreRoute, $this->restoreStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->restoreStub['meta']['email']);
        $this->seeEmailFrom("noreply@nausdream.com");
    }

    /** @test */
    public function it_gets_422_if_parameters_are_not_valid_to_reset()
    {
        //arrange
        $this->setThingsUp();
        $this->resetStub['meta']['password'] = str_random(Constant::MIN_PASSWORD_LENGTH - 1);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->resetStub['meta']['password'] = str_random(Constant::MAX_PASSWORD_LENGTH + 1);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        unset($this->resetStub['meta']['password']);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);


        //arrange
        $this->setThingsUp();
        $this->resetStub['meta']['token'] = str_random(Constant::EMAIL_TOKEN_LENGTH + 1);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->resetStub['meta']['token'] = str_random(Constant::EMAIL_TOKEN_LENGTH - 1);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        unset($this->resetStub['meta']['token']);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }


    /** @test */
    public function it_gets_404_if_token_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->resetStub['meta']['token'] = str_random(Constant::EMAIL_TOKEN_LENGTH);

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'token_not_found']);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_captain_or_partner_to_reset()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_captain = 0;
        $this->user->is_partner = 0;
        $this->user->save();

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner']);
    }

    /** @test */
    public function it_gets_200_if_parameters_are_valid_to_reset()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->resetRoute, $this->resetStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    protected function setThingsUp()
    {
        $this->user = factory(\App\Models\User::class)->states(['dummy'])->make();
        $this->unhashedPassword = $this->fake->password(Constant::MIN_PASSWORD_LENGTH, Constant::MAX_PASSWORD_LENGTH);
        $this->user->password = Hash::make($this->unhashedPassword);
        $this->user->is_mail_activated = 1;
        $this->user->is_captain = 1;
        $this->user->is_partner = 1;
        $this->user->save();
        $this->restoreStub = JsonHelper::createMetaMessage([
            'email' => $this->user->email
        ]);
        $this->resetStub = JsonHelper::createMetaMessage([
            'password' => $this->fake->password(Constant::MIN_PASSWORD_LENGTH, Constant::MAX_PASSWORD_LENGTH),
            'token' => \App\Services\TokenService::generateTokenByUser($this->user)
        ]);
    }
}