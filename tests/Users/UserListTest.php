<?php

/**
 * User: Giuseppe
 * Date: 23/12/2016
 * Time: 12:44
 */
use App\Constant;
use App\Models\Administrator;
use App\Models\Authorization;
use App\Models\Country;
use App\Models\Language;
use App\Services\PhotoHelper;
use App\TranslationModels\Language as TranslationLanguage;
use App\Models\Currency;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use JD\Cloudder\Facades\Cloudder;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;

class UserListTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // GET tests
    /** @test */
    public function it_gets_200_and_list_of_users()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->getJson('v1/users', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
        $this->seeJson(['id' => (string)$user_2->id]);
    }

    /** @test */
    public function it_gets_200_and_list_of_captains()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_3 = $this->createUser();
        $user_1->is_captain = true;
        $user_2->is_captain = true;
        $user_3->is_captain = false;
        $user_1->save();
        $user_2->save();
        $user_3->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'captains')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->getJson('v1/users?type=captains', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
        $this->seeJson(['id' => (string)$user_2->id]);
        $this->dontSeeJson(['id' => (string)$user_3->id]);
    }

    /** @test */
    public function it_gets_200_and_list_of_partners()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_3 = $this->createUser();
        $user_1->is_partner = true;
        $user_2->is_partner = true;
        $user_3->is_partner = false;
        $user_1->save();
        $user_2->save();
        $user_3->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->getJson('v1/users?type=partners', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
        $this->seeJson(['id' => (string)$user_2->id]);
        $this->dontSeeJson(['id' => (string)$user_3->id]);
    }

    /** @test */
    public function it_gets_200_and_list_of_partners_and_captains()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_3 = $this->createUser();
        $user_4 = $this->createUser();
        $user_1->is_partner = true;
        $user_1->is_captain = false;
        $user_2->is_partner = true;
        $user_2->is_captain = false;
        $user_3->is_partner = false;
        $user_3->is_captain = true;
        $user_4->is_partner = false;
        $user_4->is_captain = false;
        $user_1->save();
        $user_2->save();
        $user_3->save();
        $user_4->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $authorization = Authorization::where('name', 'captains')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->getJson('v1/users?type=partners,captains', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
        $this->seeJson(['id' => (string)$user_2->id]);
        $this->seeJson(['id' => (string)$user_3->id]);
        $this->dontSeeJson(['id' => (string)$user_4->id]);
    }

    public function it_gets_200_and_list_of_users_with_first_name_or_last_name_pippo()
    {
        //arrange
        $user_1 = User::all()->first();
        $user_2 = User::all()->last();
        $user_1->first_name = "pippo";
        $user_2->last_name = "pippo";
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->getJson('v1/users?filter=pippo', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
        $this->seeJson(['id' => (string)$user_2->id]);
    }

    /** @test */
    public function it_gets_200_and_empty_list_of_users_if_there_are_not_users()
    {
        if (User::all()->count() == 0){
            $admin = factory(Administrator::class)->create();
            $authorization = Authorization::where('name', 'users')->first();
            $admin->authorizations()->attach($authorization->id);

            //act
            $this->getJson('v1/users', $this->getHeaders($admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
        }
    }

    /** @test */
    public function it_gets_403_if_user_is_not_an_administrator()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        //act
        $this->getJson('v1/users', $this->getHeaders($user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson('v1/users', ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_403_if_it_is_an_administrator_but_has_not_permission_for_that_type_of_users()
    {
        /** users */
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();

        //act
        $this->getJson('v1/users?type=users', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);

        /** captains */
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();

        //act
        $this->getJson('v1/users?type=captains', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);

        /** partners */
        //arrange
        $user_1 = $this->createUser();
        $user_2 = $this->createUser();
        $user_1->save();
        $user_2->save();

        $admin = factory(Administrator::class)->create();

        //act
        $this->getJson('v1/users?type=partners', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this data']);
    }

    protected function getJwtString($id)
    {
        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time())// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() + 3600000)// Configures the expiration time of the token (exp claim)
            ->setSubject($id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        return $token;
    }

    public function createUser()
    {
        $user = factory(User::class)->make();

        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->docking_place = $this->fake->city;
        $user->first_name_skipper = $this->fake->firstName;
        $user->last_name_skipper = $this->fake->lastName;
        $user->skippers_have_license = $this->fake->boolean;
        $user->first_name_contact = $this->fake->firstName;
        $user->last_name_contact = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->is_captain = $this->fake->boolean;
        $user->is_skipper = $this->fake->boolean;
        $user->sex = $this->fake->randomElement(array('m', 'f', null));
        $user->birth_date = $this->fake->date();
        $user->nationality = Country::inRandomOrder()->first()->name;
        $user->currency = Currency::inRandomOrder()->first()->name;
        $user->language = TranslationLanguage::inRandomOrder()->first()->language;
        $user->description = $this->fake->text();
        $user->enterprise_name = $this->fake->name;
        $user->vat_number = str_random(Constant::ENTERPRISE_VAT_LENGTH);
        $user->enterprise_address = $this->fake->address;
        $user->enterprise_city = $this->fake->city;
        $user->enterprise_zip_code = $this->fake->postcode;
        $user->enterprise_country = Country::inRandomOrder()->first()->name;
        $user->has_card = $this->fake->boolean;
        $user->default_fee = $this->fake->numberBetween(1, 30);
        $user->photo_version = null;
        $user->is_partner = $this->fake->boolean;
        $user->is_mail_activated = true;
        $user->is_suspended = false;
        $user->newsletter = $this->fake->boolean;
        $user->fuid = $this->fake->numberBetween(1);
        $user->is_phone_activated = true;
        $user->card_price = $this->fake->numberBetween(0);

        return $user;
    }
}