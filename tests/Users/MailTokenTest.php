<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 03/11/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class MailTokenTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /** @test */
    public function it_logs_in_user_given_a_right_token()
    {
        //arrange
        $captainTester = new CaptainTest();
        $this->postJson('v1/captains', $captainTester->getStub());
        $token =  Token::all()->last();

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token->token]));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_gets_422_if_unvalid_token_is_given()
    {
        //arrange
        $token = str_random(Constant::EMAIL_TOKEN_LENGTH-1);

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token]));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $token = str_random(Constant::EMAIL_TOKEN_LENGTH+1);

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token]));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_403_if_a_token_that_passes_validation_is_given_but_it_is_not_present_on_token_table()
    {
        //arrange
        do {
            $token = str_random(Constant::EMAIL_TOKEN_LENGTH);
            $tokenObject = Token::where('token', $token)
                        ->first();
        } while (isset($tokenObject));

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token]));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }


    /** @test */
    public function it_deletes_token_from_token_table_if_it_is_valid()
    {
        //arrange
        $captainTester = new CaptainTest();
        $this->postJson('v1/captains', $captainTester->getStub());
        $token =  Token::all()->last();
        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token->token]));

        $tokenObject = Token::where('token', $token->token)
            ->first();

        if (isset($tokenObject)) {
            $result = false;
        } else {
            $result = true;
        }

        $this->assertEquals(true, $result);
    }

    /** @test */
    public function it_sets_mail_as_activated_on_user_if_it_is_mail_token()
    {
        //arrange
        $captainTester = new CaptainTest();
        $this->postJson('v1/captains', $captainTester->getStub());
        $token =  Token::all()->last();
        $userId = $token->user->id;

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token->token]));

        //assert
        $user = User::find($userId);
        $this->assertEquals(true, $user->is_mail_activated);
    }

    /** @test */
    public function it_does_not_set_mail_as_activated_on_user_id_is_not_mail_token()
    {
        //arrange
        $user = factory(User::class)->create();
        $user->is_mail_activated = false;
        $user->save();
        $token = \App\Services\TokenService::generateTokenByUser($user, false);

        //act
        $this->postJson('v1/auth/token', JsonHelper::createMetaMessage(['token' => $token]));

        //assert
        $userAfterAccess = User::find($user->id);
        $this->assertEquals(false, $userAfterAccess->is_mail_activated);
    }


}