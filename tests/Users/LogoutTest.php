<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 03/11/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class LogoutTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $user;
    protected $route;
    protected $stub;

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/auth/logout';
    }

    /** @test */
    public function it_logs_out_user_given_a_right_email_and_password()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired_on_update()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->stub, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    protected function setThingsUp()
    {
        $this->user = factory(\App\Models\User::class)->states(['dummy'])->make();
        $this->user->is_mail_activated = 1;
        $this->user->save();
        $this->stub = [];
    }


}