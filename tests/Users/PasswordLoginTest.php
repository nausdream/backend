<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 03/11/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class PasswordLoginTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $user;
    protected $route;
    protected $stub;
    protected $unhashedPassword;

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/auth/password';
    }

    /** @test */
    public function it_logs_in_user_given_a_right_email_and_password()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_gets_422_if_there_is_not_valid_email_or_password_in_request()
    {
        //arrange
        $this->setThingsUp();
        unset($this->stub['meta']['email']);

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->stub['meta']['email'] = $this->fake->firstName;

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        unset($this->stub['meta']['password']);

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_404_if_email_does_not_exist_on_database()
    {
        //arrange
        $this->setThingsUp();
        $is_edited = false;
        do {
            $randomEmail = $this->fake->email;
            if ($randomEmail != $this->stub['meta']['email']) {
                $this->stub['meta']['email'] = $randomEmail;
                $is_edited = true;
            }
        } while ($is_edited == false);

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'email_not_found']);
    }

    /** @test */
    public function it_gets_422_if_email_exist_but_not_activated()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_mail_activated = 0;
        $this->user->save();

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email_not_activated']);
    }

    /** @test */
    public function it_gets_403_if_password_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $is_edited = false;
        do {
            $randomPassword = $this->fake->password();
            if ($randomPassword != $this->stub['meta']['password']) {
                $this->stub['meta']['password'] = $randomPassword;
                $is_edited = true;
            }
        } while ($is_edited == false);

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'incorrect_password']);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_captain_or_partner()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_captain = 0;
        $this->user->is_partner = 0;
        $this->user->save();

        //act
        $this->postJson($this->route, $this->stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner']);
    }

    protected function setThingsUp()
    {
        $this->user = factory(\App\Models\User::class)->states(['dummy'])->make();
        $this->unhashedPassword = $this->fake->password(Constant::MIN_PASSWORD_LENGTH, Constant::MAX_PASSWORD_LENGTH);
        $this->user->password = Hash::make($this->unhashedPassword);
        $this->user->is_mail_activated = 1;
        $this->user->is_captain = 1;
        $this->user->is_partner = 1;
        $this->user->save();
        $this->stub = JsonHelper::createMetaMessage([
            'email' => $this->user->email,
            'password' => $this->unhashedPassword]
        );
    }


}