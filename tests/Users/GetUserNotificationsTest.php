<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Services\JwtService;
use Carbon\Carbon;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class GetUserNotificationsTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $user;
    private $captain;
    private $admin;
    private $bookings;
    private $conversation;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->user->id), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->user); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($this->user->id), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($this->user->id), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_user_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $fakeId = App\Models\User::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($fakeId), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'user_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_the_same()
    {
        //arrange
        $this->setThingsUp();
        $otherUser = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($otherUser));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_200_and_notifications_for_user()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->user->id), $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['is_mail_activated' => false]);
        $this->seeJson(['is_phone_activated' => false]);
        $this->seeJson(['messages' => 5]);
        $this->seeJson(['bookings' => 12]);
    }

    /**
     * @test
     */
    public function it_returns_200_and_notifications_for_captain()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->getRoute($this->captain->id), $this->getHeaders($this->captain));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['is_mail_activated' => true]);
        $this->seeJson(['is_phone_activated' => true]);
        $this->seeJson(['messages' => 5]);
        $this->seeJson(['bookings' => 12]);
    }

    // HELPERS

    protected function setThingsUp()
    {
        $world = factory(\App\Models\Area::class)->states('world')->create();
        $areas = factory(\App\Models\Area::class, 4)->create();
        $areas->add($world);

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $user->is_mail_activated = 0;
        $user->is_phone_activated = 0;
        $user->save();
        $this->user = $user;
        $captains = factory(\App\Models\User::class, 5)->states('dummy', 'captain')->create();
        $captain = $captains->random();
        $captain->is_mail_activated = 1;
        $captain->is_phone_activated = 1;
        $captain->save();
        $this->captain = $captain;
        $boats = factory(\App\Models\Boat::class, 5)
            ->create()
            ->each(function ($b) use ($captains) {
                $b->user()->associate($captains->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $experienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats, $experienceVersions) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                $experienceVersions->add($e->experienceVersions()->get());
            });
        $this->bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($experiences, $user) {
                $e->status = $this->fake->randomElement([
                    Constant::BOOKING_STATUS_ACCEPTED,
                    Constant::BOOKING_STATUS_REJECTED
                ]);
                $e->user()->associate($user);
                $e->experienceVersion()->associate($experiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->save();
            });

        $captainBoats = factory(\App\Models\Boat::class, 3)
            ->create()
            ->each(function ($b) use ($captain) {
                $b->user()->associate($captain);
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy')->make());
                $b->save();
            });
        $captainExperienceVersions = new \Illuminate\Database\Eloquent\Collection();
        $captainExperiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($captainBoats, $captainExperienceVersions) {
                $e->boat()->associate($captainBoats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy', 'finished', 'accepted')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy')->make());
                $e->save();
                $captainExperienceVersions->add($e->experienceVersions()->get());
            });
        $this->bookings = factory(\App\Models\Booking::class, 12)->states('dummy')
            ->create()
            ->each(function ($e) use ($captainExperiences) {
                $e->status = $this->fake->randomElement([
                    Constant::BOOKING_STATUS_PENDING,
                    Constant::BOOKING_STATUS_PAID
                ]);
                $e->user()->associate($this->user);
                $e->experienceVersion()->associate($captainExperiences->random()->experienceVersions()->get()->where('is_finished', true)->last());
                $e->save();
            });

        // Create new conversation
        $experienceConversation = $experiences->random();
        $conversationData = [
            'request_date' => $this->fake->date(),
            'adults' => $this->fake->numberBetween(1, 5),
            'kids' => $this->fake->numberBetween(0, 3),
            'babies' => $this->fake->numberBetween(0, 3),
        ];
        $conversation = factory(App\Models\Conversation::class)->make($conversationData);
        $conversation->experience()->associate($experienceConversation);
        $conversation->user()->associate($this->user);
        $conversation->captain()->associate($experienceConversation->boat()->first()->user()->first());
        $conversation->save();

        $this->conversation = $conversation;

        factory(\App\Models\Message::class, 5)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) {
                $m->user()->associate($this->conversation->captain()->first());
                $m->conversation()->associate($this->conversation);
                $m->save();
            });


        // Create new conversation
        $experienceConversation = $experiences->random();
        $conversationData = [
            'request_date' => $this->fake->date(),
            'adults' => $this->fake->numberBetween(1, 5),
            'kids' => $this->fake->numberBetween(0, 3),
            'babies' => $this->fake->numberBetween(0, 3),
        ];
        $conversationCaptain = factory(App\Models\Conversation::class)->make($conversationData);
        $conversationCaptain->experience()->associate($experienceConversation);
        $conversationCaptain->user()->associate($this->user);
        $conversationCaptain->captain()->associate($this->captain);
        $conversationCaptain->save();

        factory(\App\Models\Message::class, 5)->states('dummy', 'unread')
            ->create()
            ->each(function ($m) use($conversationCaptain) {
                $m->user()->associate($this->user);
                $m->conversation()->associate($conversationCaptain);
                $m->save();
            });
    }

    public function getRoute($id)
    {
        return 'v1/users/' . $id . '/notifications';
    }


}