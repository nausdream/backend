<?php

/**
 * User: Giuseppe
 * Date: 21/10/2016
 * Time: 17:22
 */

use App\TranslationModels\Language;
use App\Models\Currency;
use App\Models\User;
use App\Constant;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CaptainTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    /** @test */
    public function it_creates_a_captain_given_valid_parameters()
    {
        //arrange

        //act
        $this->postJson('v1/captains', $this->getStub());

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_first_name()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["first_name"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["first_name"] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_phone()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["phone"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["phone"] = str_random(Constant::PHONE_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_docking_place()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["docking_place"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["docking_place"] = str_random(Constant::DOCKING_PLACE_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_email()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["email"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["email"] = str_random(Constant::EMAIL_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_language()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["language"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["language"] = str_random(Constant::LANGUAGE_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_currency()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["currency"] = null;

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["currency"] = str_random(Constant::CURRENCY_LENGTH + 1);

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_409_if_there_is_invalid_type()
    {
        //arrange
        $stub = $this->getStub();
        $stub['data']['type'] = 'user';

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

    }


    /** @test */
    public function it_gets_406_if_there_is_already_a_captain_with_same_email_and_phone_active()
    {
        //arrange
        $stub = $this->getStub();

        //act
        $this->postJson('v1/captains', $stub);
        $captain = User::where('email', $stub['data']['attributes']['email'])
            ->first();
        $captain->is_mail_activated = true;
        $captain->is_phone_activated = true;
        $captain->save();
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function it_gets_406_if_there_is_already_a_captain_with_same_email_active()
    {
        //arrange
        $stub = $this->getStub();

        //act
        $this->postJson('v1/captains', $stub);
        $captain = User::where('email', $stub['data']['attributes']['email'])
            ->first();
        $captain->is_mail_activated = true;
        $captain->save();
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function it_gets_406_if_there_is_already_a_captain_with_same_phone_active()
    {
        //arrange
        $stub = $this->getStub();

        //act
        $this->postJson('v1/captains', $stub);
        $captain = User::where('phone', $stub['data']['attributes']['phone'])
            ->first();
        $captain->is_phone_activated = true;
        $captain->save();
        $this->postJson('v1/captains', $stub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function email_is_sent_to_captain_and_administrator_on_captains_registration()
    {
        //arrange
        $stub = $this->getStub();

        //act
        $this->postJson('v1/captains', $stub);

        //assert
        $this->seeEmailsSent(2);
        $this->seeEmailTo($stub['data']['attributes']['email']);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo("support@nausdream.com");
        $this->seeEmailFrom("noreply@nausdream.com");
    }



    protected function getStub()
    {
        return [
            'data' => [
                'type' => 'users',
                'attributes' => [
                    'first_name' => $this->fake->firstName,
                    'phone' => $this->fake->phoneNumber,
                    'docking_place' => $this->fake->city,
                    'email' => $this->fake->email,
                    'language' => Language::inRandomOrder()->first()->language,
                    'currency' => Currency::inRandomOrder()->first()->name,
                ]
            ]
        ];

    }
}