<?php
/**
 * User: Luca Puddu
 * Date: 29/10/2016
 * Time: 15:20
 * Notes: This class can't be tested with a valid access code because, unlike accessTokens, access codes are single-use
 *        codes.
 */

use App\Constant;
use App\Http\Controllers\v1\Auth\AccountKitController;
use App\Http\Controllers\v1\Auth\UserAccountKitController;
use App\Models\Currency;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\TranslationModels\Language;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class UserAccountKitTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /** @test */
    public function ak_it_returns_422_if_code_not_found_or_not_valid()
    {
        //arrange
        $data = JsonHelper::createMetaMessage(['code' => 'random_code']);

        //act
        $this->postJson('v1/auth/accountkit/', $data);

        //assert
        $this->seeJson(['code' => 'authentication_failed']);
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_returns_200_and_user_token_if_user_with_phone_already_exists_and_all_params_are_already_set()
    {
        //arrange
        $stub = $this->getStub();

        $user = factory(User::class)->make();
        $user->phone = $stub['phone'];
        $user->email = $stub['email'];
        $user->first_name = $stub['name'];
        $user->language = $stub['language'];
        $user->currency = $stub['currency'];
        $user->save();
        $token = JwtService::getTokenStringFromAccount($user);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        //assert
        $this->assertEquals(200, $response->getStatusCode());
    }

    /** @test */
    public function it_returns_200_with_jwt_and_set_params_where_null_if_user_with_phone_already_exists_and_some_params_dont()
    {
        //arrange
        $stub = $this->getStub();

        //only phone and name are set (language and currency are explicitly set)
        $user = factory(User::class)->make();
        $user->phone = $stub['phone'];
        $user->email = null;
        $user->first_name = $stub['name'];
        $user->language = 'abcde';
        $user->currency = 'fgh';
        $user->save();
        $token = JwtService::getTokenStringFromAccount($user);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );;

        $editedUser = User::find($user->id); //fetch user after accountKitLogin edited it

        //assert
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($user->id, $editedUser->id);
        //assert that, after the request, user params that where null previously have now been set correctly
        $this->assertEquals($stub['email'], $editedUser->email);
        $this->assertEquals($stub['currency'], $editedUser->currency);
        $this->assertEquals($stub['language'], $editedUser->language);
    }

    /** @test */
    public function it_returns_201_and_registers_a_new_user_with_all_params_if_user_with_phone_or_email_doesnt_exists()
    {
        //arrange
        $stub = $this->getStub();

        //assert that there is no user with this phone number yet
        $user = User::where('phone', $stub['phone'])->get();
        $this->assertCount(0, $user);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        $new_user = User::where('phone', $stub['phone'])->get();

        //assert
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertCount(1, $new_user);
    }

    /** @test */
    public function it_returns_201_and_registers_a_new_user_with_name_and_phone_only_if_user_with_email_exist_but_he_has_no_phone()
    {
        //arrange
        $stub = $this->getStub();

        //create user with same email but no phone
        $user = factory(User::class)->make();
        $user->phone = null;
        $user->email = $stub['email'];
        $user->first_name = 'Pinco Pallino';
        $user->save();

        //check that there is no user with the stub's phone number yet
        $userWithPhone = User::where('phone', $stub['phone'])->get();
        $this->assertCount(0, $userWithPhone);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        $new_user = User::where('phone', $stub['phone'])->get()->first();

        //assert
        $this->assertNotEquals(null, $new_user); //user exists
        //assert that the new user has no email because there was already a user with the same email
        $this->assertEquals(null, $new_user->email);
        $this->assertEquals(201, $response->getStatusCode());
    }

    /** @test */
    public function ak_it_returns_422_if_phone_is_invalid_or_missing()
    {
        //invalid phone
        //arrange
        $stub = $this->getStub();
        $stub['phone'] = str_random(Constant::PHONE_LENGTH + 1);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        //assert
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals(
            '{"errors":[{"code":"max","title":"phone","detail":"The phone may not be greater than ' . Constant::PHONE_LENGTH . ' characters."}]}',
            $response->content());

        //missing phone
        //arrange
        $stub = $this->getStub();
        $stub['phone'] = null;

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        //assert
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals(
            '{"errors":[{"code":"required","title":"phone","detail":"The phone field is required."}]}',
            $response->content());
    }

    /** @test */
    public function ak_it_returns_422_if_first_name_is_invalid()
    {
        //arrange
        $stub = $this->getStub();
        $stub['name'] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        //assert
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals(
            '{"errors":[{"code":"max","title":"first_name","detail":"The first name may not be greater than ' . Constant::FIRST_NAME_LENGTH . ' characters."}]}',
            $response->content());
    }

    /** @test */
    public function ak_it_returns_422_if_email_is_invalid()
    {
        //arrange
        $stub = $this->getStub();
        $stub['email'] = str_random(Constant::EMAIL_LENGTH + 1);

        //act
        $response = $this->getAccountKitLoginProtected(
            [
                $stub['phone'],
                $stub['email'],
                $stub['name'],
                $stub['language'],
                $stub['currency']
            ]
        );

        //assert
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals(
            '{"errors":[{"code":"max","title":"email","detail":"The email may not be greater than ' . Constant::EMAIL_LENGTH . ' characters."}]}',
            $response->content());
    }

    /** @test */
    public function ak_verification_returns_422_if_code_not_found_or_not_valid()
    {
        //arrange
        $captainTester = new CaptainTest();
        $this->postJson('v1/captains', $captainTester->getStub());

        $user = User::all()->last();

        $data = JsonHelper::createMetaMessage(['code' => 'random_code']);
        $data = array_merge($data, JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id)));

        //act
        $this->postJson('v1/verify/accountkit/', $data,
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]);

        //assert
        $this->seeJson(['code' => 'authentication_failed']);
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Get mock data
     *
     * @return array
     */
    protected function getStub()
    {
        return [
            'phone' => $this->fake->phoneNumber,
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'language' => Language::inRandomOrder()->first()->language,
            'currency' => Currency::inRandomOrder()->first()->name
        ];
    }

    /**
     * Allow calling of protected function on AccountKitController
     *
     * @param array|null $args
     * @return mixed
     */
    protected function getAccountKitLoginProtected(array $args = null)
    {
        $class = new ReflectionClass('App\Http\Controllers\v1\Auth\UserAccountKitController');
        $method = $class->getMethod('accountKitLogin');
        $method->setAccessible(true);
        $object = new UserAccountKitController();
        return $method->invokeArgs($object, $args);
    }
}