<?php
/**
 * User: Giuseppe Basciu
 * Date: 28/10/2016
 * Time: 10:54
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Authorization;
use App\Models\Country;
use App\Models\Language;
use App\Services\PhotoHelper;
use App\TranslationModels\Language as TranslationLanguage;
use App\Models\Currency;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use JD\Cloudder\Facades\Cloudder;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;

class UserTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $valid_token; //FacebookController tests need a valid facebook token (not expired) to be tested

    /**
     * UserTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->valid_token = 'EAANt3KLp6ZCUBAL7KlCSZBKZCmA9Jmb6i6i0h516zQNo7al5Mxdd4wX28s22LF7ZCdqJyl8WGAKwg8anEZCKglyoGjczlbEhwfmCcZCUA0BHOL8Xkr8YHpPEZAABPSe3DBgxNZB0n2VNErXD4hsu4fHqv2eR5cbuOJM4MIUqM6JxQwZDZD';
    }

    /**
     * Token validity is evaluated first: the test will fail if validation fails.
     *
     * @test
     */
    public function it_returns_404_if_user_doesnt_exist()
    {
        //arrange
        $id = 99999999;
        $token = self::getJwtString($id);

        //act
        $this->getJson('v1/users/' . $id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_403_if_user_exists_but_jwt_sub_claim_is_different()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();
        $user_2 = factory(User::class)->make();
        $user_2->first_name = 'Caio';
        $user_2->save();

        $token = JwtService::getTokenStringFromAccount($user_2);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this user data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $user_1 = factory(User::class)->make();;
        $user_1->first_name = 'Tizio';
        $user_1->save();

        $token = JwtService::getTokenStringFromAccount($user_1); // Retrieves the generated token

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user_1->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_200_if_user_exist_and_jwt_is_valid_and_verified()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();

        $token = JwtService::getTokenStringFromAccount($user_1);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
    }

    //FacebookAuthController

    /** @test */
    public function it_returns_422_if_accessToken_is_not_valid()
    {
        $data = [
            'meta' => [
                'access_token' => 'fake_token',
                'language' => 'it',
                'currency' => 'EUR'
            ]
        ];

        //act
        $this->postJson('v1/auth/facebook', $data);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * For this method to pass, the user associated with the accessToken must not exists yet
     *
     * @test
     */
    public function it_returns_201_and_registers_a_new_user_if_user_doesnt_exists_and_parameters_are_valid()
    {
        //arrange
        $users_count = User::all()->count(); //users count in db

        $data = [
            'meta' => [
                'access_token' => $this->valid_token,
                'language' => 'it',
                'currency' => 'EUR'
            ]
        ];

        //act
        $this->postJson('v1/auth/facebook', $data);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertEquals($users_count + 1, User::all()->count()); //assert that users have increased
        $newUser = User::all()->last();
        $this->assertFalse($newUser->is_mail_activated);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($newUser->email);
        $this->seeEmailFrom("noreply@nausdream.com");
    }

    /** @test */
    public function it_returns_200_and_user_token_if_user_already_exists_and_parameters_are_valid()
    {
        $data = [
            'meta' => [
                'access_token' => $this->valid_token,
                'language' => 'it',
                'currency' => 'EUR'
            ]
        ];

        //act
        $this->postJson('v1/auth/facebook', $data); //create user
        $this->postJson('v1/auth/facebook', $data); //try to create another user: get its token instead


        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_returns_spoken_languages_by_user_if_user_already_exists_and_parameters_are_valid()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();
        $user->languages()->attach([
            $language1->id,
            $language2->id,
        ]);

        //act
        $this->getJson('v1/users/' . $user->id . '?include=languages', ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]);

        //assert
        $this->seeJson([
            'type' => 'languages',
            'id' => (string)$language1->id,
            'attributes' => [
                'language' => $language1->language
            ]
        ]);
        $this->seeJson([
            'type' => 'languages',
            'id' => (string)$language2->id,
            'attributes' => [
                'language' => $language2->language
            ]
        ]);
    }

    /** @test */
    public function it_returns_204_status_code_if_updates_correctly_user_spoken_languages()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();
        $user->languages()->attach([
            $language1->id,
            $language2->id,
        ]);
        do {
            $language3 = Language::inRandomOrder()->first();
        } while ($language3->id == $language1->id || $language3->id == $language2->id);
        do {
            $language4 = Language::inRandomOrder()->first();
        } while ($language4->id == $language1->id || $language4->id == $language2->id);

        //act
        $this->patchJson('v1/users/' . $user->id . '/relationships/languages',
            JsonHelper::createDataMessage([
                    JsonHelper::createData('languages', $language3->id),
                    JsonHelper::createData('languages', $language4->id)]
            ),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $userEdited = User::find($user->id);
        $this->assertResponseStatus(SymfonyResponse::HTTP_NO_CONTENT);
        $updatedLanguage = $userEdited->languages()->where('language_id', $language1->id)->first();
        $this->assertFalse(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language2->id)->first();
        $this->assertFalse(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language3->id)->first();
        $this->assertTrue(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language4->id)->first();
        $this->assertTrue(isset($updatedLanguage));
    }

    /** @test */
    public function it_returns_204_status_code_if_updates_correctly_user_spoken_languages_with_admin()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();
        $user->languages()->attach([
            $language1->id,
            $language2->id,
        ]);
        do {
            $language3 = Language::inRandomOrder()->first();
        } while ($language3->id == $language1->id || $language3->id == $language2->id);
        do {
            $language4 = Language::inRandomOrder()->first();
        } while ($language4->id == $language1->id || $language4->id == $language2->id);

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->patchJson('v1/users/' . $user->id . '/relationships/languages',
            JsonHelper::createDataMessage([
                    JsonHelper::createData('languages', $language3->id),
                    JsonHelper::createData('languages', $language4->id)]
            ),
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $userEdited = User::find($user->id);
        $this->assertResponseStatus(SymfonyResponse::HTTP_NO_CONTENT);
        $updatedLanguage = $userEdited->languages()->where('language_id', $language1->id)->first();
        $this->assertFalse(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language2->id)->first();
        $this->assertFalse(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language3->id)->first();
        $this->assertTrue(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language4->id)->first();
        $this->assertTrue(isset($updatedLanguage));
    }

    /** @test */
    public function it_returns_422_status_code_if_unvalid_spoken_language_is_given_and_do_not_edit_the_language_on_user()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();
        $user->languages()->attach([
            $language1->id,
            $language2->id,
        ]);
        $language = Language::all()->last();

        //act
        $this->patchJson('v1/users/' . $user->id . '/relationships/languages',
            JsonHelper::createDataMessage(
                [JsonHelper::createData('languages', $language->id + 1)]
            ),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $userEdited = User::find($user->id);
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $updatedLanguage = $userEdited->languages()->where('language_id', $language1->id)->first();
        $this->assertTrue(isset($updatedLanguage));
        $updatedLanguage = $userEdited->languages()->where('language_id', $language2->id)->first();
        $this->assertTrue(isset($updatedLanguage));
    }

    /** @test */
    public function it_returns_404_if_user_to_update_spoken_languages_doesnt_exist()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();

        //act
        $this->patchJson('v1/users/' . ($user->id + 1) . '/relationships/languages',
            JsonHelper::createDataMessage([
                    JsonHelper::createData('languages', $language1->id),
                    JsonHelper::createData('languages', $language2->id)]
            ),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired_on_update_spoken_language()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->first_name = 'Tizio';
        $user->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();


        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();

        //act
        $this->patchJson('v1/users/' . $user->id . '/relationships/languages',
            JsonHelper::createDataMessage([
                    JsonHelper::createData('languages', $language1->id),
                    JsonHelper::createData('languages', $language2->id)]
            ),
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_403_if_user_exist_but_jwt_sub_claim_is_different_on_update_spoken_languages()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->first_name = 'Tizio';
        $user->save();
        $user_2 = factory(User::class)->make();
        $user_2->first_name = 'Tizio';
        $user_2->save();

        $token = JwtService::getTokenStringFromAccount($user_2);

        $language1 = Language::inRandomOrder()->first();
        $language2 = Language::inRandomOrder()->first();

        //act
        $this->patchJson('v1/users/' . $user->id . '/relationships/languages',
            JsonHelper::createDataMessage([
                    JsonHelper::createData('languages', $language1->id),
                    JsonHelper::createData('languages', $language2->id)]
            ),
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this user data']);
    }

    /** @test */
    public function it_returns_200_and_user_is_updated_on_db_if_given_user_is_valid()
    {
        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $userTransformer = new UserTransformer();
        $stub = $userTransformer->transform($userEdit);
        $stub['currency'] = $userEdit->currency;
        unset($stub['profile_photo']);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newUser = User::find($user->id);
        $user = $user->fill($userEdit->toArray());
        $user->is_mail_activated = false;
        $user->email = $userEdit->email;
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $user->fill($userEdit->toArray())->toArray();
        $user->currency = $userEdit->currency;
        $this->assertEmpty($this->arrayRecursiveDiff($user->toArray(), $newUser->toArray()));
        $this->assertEquals($newUser->email, $userEdit->email);
        $this->assertFalse($newUser->is_mail_activated);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($userEdit->email);
        $this->seeEmailFrom("noreply@nausdream.com");
    }


    /** @test */
    public function it_returns_404_if_user_to_update_doesnt_exist()
    {
        //arrange
        $user = $this->createUser();
        $user->save();
        $user = User::all()->last();

        $userEdit = $this->createUser();
        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . ($user->id + 1),
            JsonHelper::createDataMessage(JsonHelper::createData('users', ($user->id + 1), $userTransformer->transform($userEdit))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired_on_update()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->first_name = 'Tizio';
        $user->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $userTransformer->transform($user))),
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_403_if_user_exist_but_jwt_sub_claim_is_different_on_update()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();
        $user_2 = factory(User::class)->make();
        $user_2->first_name = 'Caio';
        $user_2->save();

        $token = JwtService::getTokenStringFromAccount($user_2);

        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . $user_1->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user_1->id, $userTransformer->transform($user_1))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user_2)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this user data']);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter()
    {
        /** first_name */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);

        $stub['first_name'] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        /** last_name */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['last_name'] = str_random(Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** docking_place */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['docking_place'] = str_random(Constant::DOCKING_PLACE_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_1()
    {
        /** first_name_skipper */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['first_name_skipper'] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** last_name_skipper */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['last_name_skipper'] = str_random(Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** skippers_have_license */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['skippers_have_license'] = 2;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_2()
    {
        /** first_name_contact */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['first_name_contact'] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** last_name_contact */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['last_name_contact'] = str_random(Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** email */

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['email'] = $this->fake->email . str_random(Constant::EMAIL_LENGTH);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userEdit = $this->createUser();
        $userEdit->save();
        $stub = $this->getUserStub($userEdit);
        $stub['email'] = $this->fake->firstName;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** paypal_account */

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['paypal_account'] = $this->fake->email . str_random(Constant::EMAIL_LENGTH);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userEdit = $this->createUser();
        $userEdit->save();
        $stub = $this->getUserStub($userEdit);
        $stub['paypal_account'] = $this->fake->firstName;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);


        /** captain_type */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['captain_type'] = -1;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_3()
    {
        /** is_skipper */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['is_skipper'] = 2;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** sex */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['sex'] = str_random(2);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_4()
    {
        /** vat_number */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['vat_number'] = str_random(Constant::ENTERPRISE_VAT_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_address */

        //arrange
        $userEdit = $this->createUser();
        $stub['enterprise_address'] = str_random(Constant::ENTERPRISE_ADDRESS_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_city */

        //arrange
        $userEdit = $this->createUser();
        $stub['enterprise_city'] = str_random(Constant::ENTERPRISE_CITY_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_zip_code */

        //arrange
        $userEdit = $this->createUser();
        $stub['enterprise_zip_code'] = str_random(Constant::ENTERPRISE_ZIP_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_country */

        //arrange
        $userEdit = $this->createUser();
        $stub['enterprise_country'] = str_random(Constant::COUNTRY_NAME_LENGTH);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** has_card */

        //arrange
        $userEdit = $this->createUser();
        $stub['has_card'] = 2;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, array_merge($stub, ['has_card' => $userEdit->has_card]))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_5()
    {
        /** default_fee */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['default_fee'] = Constant::MAX_FEE_VALUE + 1;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['default_fee'] = Constant::MIN_FEE_VALUE - 1;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);


        /** is_partner */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, array_merge($stub, ['is_partner' => 2]))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** is_suspended */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, array_merge($stub, ['is_suspended' => 2]))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** newsletter */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['newsletter'] = 2;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    function it_gets_422_if_there_is_an_invalid_parameter_6()
    {
        /** nationality */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['nationality'] = str_random(Constant::NATIONALITY_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** currency */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['currency'] = str_random(Constant::CURRENCY_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** language */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['language'] = str_random(Constant::LANGUAGE_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter_7()
    {
        /** birth_date */

        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['birth_date'] = $this->fake->firstName;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** decription */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['description'] = 1;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_name */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['enterprise_name'] = str_random(Constant::ENTERPRISE_NAME_LENGTH + 1);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** card_price */

        //arrange
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['card_price'] = -1;

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_returns_409_if_id_in_data_attributes_of_the_update_request_is_different_from_the_one_on_route()
    {
        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $userEdit->save();
        $stub = $this->getUserStub($userEdit);
        $stub['email'] = $this->fake->email;
        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $userEdit->id, $userTransformer->transform($userEdit))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /** @test */
    public function it_returns_409_if_type_in_update_request_is_not_users()
    {
        //arrange
        $user = $this->createUser();
        $user->save();

        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('user', $user->id, $userTransformer->transform($userEdit))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }


    /** @test */
    public function it_doesnt_change_is_mail_activated_value_if_the_edited_email_is_the_same_of_the_old_one_and_do_not_send_mail()
    {
        //arrange
        $user = $this->createUser();
        $user->save();

        $userTransformer = new UserTransformer();
        $userEdit = $this->createUser();
        $stub = $this->getUserStub($userEdit);
        $stub['email'] = $user->email;
        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newUser = User::find($user->id);
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(0);
        $this->assertTrue($newUser->is_mail_activated);
    }

    /** @test */
    public function it_returns_also_url_to_profile_picture_in_attributes()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user_1->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user_1)]
        );

        $token = JwtService::getTokenStringFromAccount($user_1);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $device = PhotoHelper::getDeviceByName(null);
        $newUser = User::find($user_1->id);
        $url = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/' . $user_1->id . "/profile",
            [
                'version' => $newUser->photo_version,
                'format' => 'jpg',
                'width' => $device->width,
                'height' => $device->height,
                'crop' => 'limit',
                'angle' => 'exif'
            ]);

        $this->seeJson(['profile_photo' => $url]);
    }

    /** @test */
    public function it_returns_null_to_no_profile_picture_in_attributes_if_user_has_not_profile_picture()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();

        $token = JwtService::getTokenStringFromAccount($user_1);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $url = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'static/avatar', ['format' => 'jpg']);

        $this->seeJson(['profile_photo' => null]);
    }


    /** @test */
    public function it_returns_403_if_user_exist_but_admin_has_not_permission_to_show_user()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot access this user data']);
    }

    /** @test */
    public function it_returns_403_if_user_exist_but_admin_has_not_permission_to_update_user()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();

        $token = JwtService::getTokenStringFromAccount($admin);

        $userEdit = $this->createUser();
        $userTransformer = new UserTransformer();

        //act
        $this->patchJson('v1/users/' . $user_1->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user_1->id, $userTransformer->transform($userEdit))),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this user data']);
    }

    /** @test */
    public function it_returns_200_if_user_exist_and_jwt_is_valid_and_verified_for_admin()
    {
        /** users authorization */

        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);

        /** captains authorization */

        //arrange
        $user_1 = factory(User::class)->states('captain')->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'captains')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->getJson('v1/users/' . $user_1->id, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
    }

    /** @test */
    public function it_returns_200_if_user_exist_and_jwt_is_valid_and_verified_for_admin_to_patch()
    {
        /** users authorization */

        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        $userTransformer = new UserTransformer();
        $userEdit = $this->createUser();
        $stub = $userTransformer->transform($userEdit);
        unset($stub['profile_photo']);

        //act
        $this->patchJson('v1/users/' . $user_1->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user_1->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);

        /** captains authorization */

        //arrange
        $user_1 = factory(User::class)->states('captain')->make();
        $user_1->save();
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'captains')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        $userTransformer = new UserTransformer();
        $userEdit = $this->createUser();
        $stub = $userTransformer->transform($userEdit);
        unset($stub['profile_photo']);

        //act
        $this->patchJson('v1/users/' . $user_1->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user_1->id, $stub)),
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user_1->id]);
    }

    /** @test */
    public function it_returns_200_and_change_just_currency_if_it_is_not_a_captain()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->currency = 'EUR';
        $user->save();

        $userTransformer = new UserTransformer();
        $userEdit = $this->createUser();
        $stub = $userTransformer->transform($userEdit);
        $stub['is_captain'] = false;
        $stub['currency'] = 'USD';
        unset($stub['profile_photo']);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            $this->getHeaders($user)
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user->id]);
        $this->seeJson(['currency' => 'USD']);

    }

    /** @test */
    public function it_returns_200_and_change_currency_of_the_captain_and_of_every_experience_version_and_services_and_prices()
    {
        //arrange
        $user = User::has('boats')->where('is_captain', true)->inRandomOrder()->first();

        $userTransformer = new UserTransformer();
        $stub = $userTransformer->transform($user);
        do {
            $stub['currency'] = Currency::inRandomOrder()->first()->name;
        } while ($user->currency == $stub['currency']);
        unset($stub['profile_photo']);

        //act
        $this->patchJson('v1/users/' . $user->id,
            JsonHelper::createDataMessage(JsonHelper::createData('users', $user->id, $stub)),
            $this->getHeaders($user)
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'users']);
        $this->seeJson(['id' => (string)$user->id]);
        $this->seeJson(['currency' => $stub['currency']]);

        $boats = $user->boats()->get();
        foreach ($boats as $boat) {
            $experiences = $boat->experiences()->get();
            foreach ($experiences as $experience) {
                $experienceVersions = $experience->experienceVersions()->get();
                foreach ($experienceVersions as $experienceVersion) {
                    $this->assertEquals($experienceVersion->currency, $stub['currency']);
                    $fixedAdditionalServices = $experienceVersion->experienceVersionsFixedAdditionalServices()->get();
                    foreach ($fixedAdditionalServices as $fixedAdditionalService) {
                        $this->assertEquals($fixedAdditionalService->currency, $stub['currency']);
                    }
                    $additionalServices = $experienceVersion->additionalServices()->get();
                    foreach ($additionalServices as $additionalService) {
                        $this->assertEquals($additionalService->currency, $stub['currency']);
                    }
                }
                $periods = $experience->periods()->get();
                foreach ($periods as $period) {
                    $this->assertEquals($period->currency, $stub['currency']);
                    $prices = $period->prices()->get();
                    foreach ($prices as $price) {
                        $this->assertEquals($price->currency, $stub['currency']);
                    }
                }
            }
        }

    }

    protected function getJwtString($id)
    {
        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time())// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() + 3600000)// Configures the expiration time of the token (exp claim)
            ->setSubject($id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        return $token;
    }

    public function createUser()
    {
        $user = factory(User::class)->make();

        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->docking_place = $this->fake->city;
        $user->first_name_skipper = $this->fake->firstName;
        $user->last_name_skipper = $this->fake->lastName;
        $user->skippers_have_license = $this->fake->boolean;
        $user->first_name_contact = $this->fake->firstName;
        $user->last_name_contact = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->is_captain = $this->fake->boolean;
        $user->is_skipper = $this->fake->boolean;
        $user->sex = $this->fake->randomElement(array('m', 'f', null));
        $user->birth_date = $this->fake->date();
        $user->nationality = Country::inRandomOrder()->first()->name;
        $user->currency = Currency::inRandomOrder()->first()->name;
        $user->language = TranslationLanguage::inRandomOrder()->first()->language;
        $user->description = $this->fake->text();
        $user->enterprise_name = $this->fake->name;
        $user->vat_number = str_random(Constant::ENTERPRISE_VAT_LENGTH);
        $user->enterprise_address = $this->fake->address;
        $user->enterprise_city = $this->fake->city;
        $user->enterprise_zip_code = $this->fake->postcode;
        $user->enterprise_country = Country::inRandomOrder()->first()->name;
        $user->has_card = $this->fake->boolean;
        $user->default_fee = $this->fake->numberBetween(1, 30);
        $user->photo_version = null;
        $user->is_partner = false;
        $user->is_mail_activated = true;
        $user->is_suspended = false;
        $user->newsletter = $this->fake->boolean;
        $user->fuid = $this->fake->numberBetween(1);
        $user->is_phone_activated = true;
        $user->card_price = $this->fake->numberBetween(0, 200);
        $user->paypal_account = $this->fake->email;

        return $user;
    }

    public function getUserStub($user)
    {
        return [
            'first_name' => $this->fake->firstName,
            'last_name' => $this->fake->lastName,
            'docking_place' => $this->fake->city,
            'first_name_skipper' => $this->fake->firstName,
            'last_name_skipper' => $this->fake->lastName,
            'skippers_have_license' => $this->fake->boolean,
            'first_name_contact' => $this->fake->firstName,
            'last_name_contact' => $this->fake->lastName,
            'email' => $user->email,
            'phone' => $user->phone,
            'is_skipper' => $this->fake->boolean,
            'sex' => $this->fake->randomElement(array('m', 'f', null)),
            'birth_date' => $this->fake->date(),
            'nationality' => Country::inRandomOrder()->first()->name,
            'currency' => Currency::inRandomOrder()->first()->name,
            'language' => TranslationLanguage::inRandomOrder()->first()->language,
            'description' => $this->fake->text(),
            'enterprise_name' => $this->fake->name,
            'vat_number' => str_random(Constant::ENTERPRISE_VAT_LENGTH),
            'enterprise_address' => $this->fake->address,
            'enterprise_city' => $this->fake->city,
            'enterprise_zip_code' => $this->fake->postcode,
            'enterprise_country' => Country::inRandomOrder()->first()->name,
            'has_card' => $this->fake->boolean,
            'newsletter' => $this->fake->boolean,
            'card_price' => $this->fake->numberBetween(0),
            'paypal_account' => $this->fake->email,
        ];
    }
}