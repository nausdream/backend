<?php

/**
 * User: Giuseppe
 * Date: 15/03/2017
 * Time: 10:33
 */
use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ResendTokenTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    protected $user;
    protected $resendRoute;
    protected $resendStub;

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->resendRoute = 'v1/resend/token';
    }

    /** @test */
    public function it_gets_422_if_email_is_not_valid_or_present()
    {
        //arrange
        $this->setThingsUp();
        $this->resendStub['meta']['email'] = $this->fake->firstName;

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        unset($this->resendStub['meta']['email']);

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_404_if_email_does_not_exist_on_database()
    {
        //arrange
        $this->setThingsUp();
        $is_edited = false;
        do {
            $randomEmail = $this->fake->email;
            if ($randomEmail != $this->resendStub['meta']['email']) {
                $this->resendStub['meta']['email'] = $randomEmail;
                $is_edited = true;
            }
        } while ($is_edited == false);

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'email_not_found']);
    }

    /** @test */
    public function it_gets_422_if_email_exist_but_not_activated()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_mail_activated = 0;
        $this->user->save();

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email_not_activated']);
    }

    /** @test */
    public function it_gets_422_if_password_is_already_set()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_set_password = 1;
        $this->user->save();

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_has_already_password']);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_captain_or_partner()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_captain = 0;
        $this->user->is_partner = 0;
        $this->user->save();

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner']);
    }

    /** @test */
    public function it_gets_200_if_parameters_are_ok()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->resendRoute, $this->resendStub);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->resendStub['meta']['email']);
        $this->seeEmailFrom("noreply@nausdream.com");
    }

    protected function setThingsUp()
    {
        $this->user = factory(\App\Models\User::class)->states(['dummy'])->make();
        $this->user->is_mail_activated = 1;
        $this->user->is_set_password = 0;
        $this->user->is_captain = 1;
        $this->user->is_partner = 1;
        $this->user->save();
        $this->resendStub = JsonHelper::createMetaMessage([
            'email' => $this->user->email
        ]);
    }
}