<?php
/**
 * User: Giuseppe Basciu
 * Date: 28/10/2016
 * Time: 10:54
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Authorization;
use App\Models\Country;
use App\Models\Language;
use App\Services\PhotoHelper;
use App\TranslationModels\Language as TranslationLanguage;
use App\Models\Currency;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use JD\Cloudder\Facades\Cloudder;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;

class AdminCreateTokenAccessForSpecificUserTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route = 'v1/admin-token';
    private $admin;
    private $user;


    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->postJson($this->route, $requestBody);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->postJson($this->route, $requestBody, ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $requestBody, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_request_does_not_pass_validation()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        unset($requestBody['meta']['user_id']);

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'user_id']);

        //arrange
        $requestBody = $this->getRequestBody();
        $requestBody['meta']['user_id'] = 'not_an_integer';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'user_id']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_user_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $requestBody['meta']['user_id'] = User::all()->last()->id + 1;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'user_not_found']);
    }

    /** @test */
    public function it_returns_403_if_user_exists_but_account_is_not_admin()
    {
        ///arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /** @test */
    public function it_returns_403_if_user_exists_but_admin_has_not_users_authorization()
    {
        ///arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /** @test */
    public function it_returns_201_if_everything_is_ok()
    {
        ///arrange
        $this->setThingsUp();
        $requestBody = $this->getRequestBody();

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->meta, []);
        $tokenString = $responseContent->meta->token;
        $this->assertNotNull($tokenString);

        $tokenObject = \App\Models\Token::where('token', '=', $tokenString)
            ->first();
        $this->assertNotNull($tokenObject);

        $user = $tokenObject->user;
        $this->assertEquals($this->user->id, $user->id);

        $this->assertFalse($tokenObject->is_mail_token);
    }

    public function setThingsUp()
    {
        //admin
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);
        $this->admin = $admin;

        //user
        $user = factory(User::class)->create();
        $this->user = $user;
    }

    public function getRequestBody()
    {
        return [
            'meta' => [
                'user_id' => $this->user->id
            ]
        ];
    }
}