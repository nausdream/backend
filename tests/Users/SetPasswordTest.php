<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 03/11/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Token;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class SetPasswordTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $user;
    protected $route;
    protected $stub;
    protected $unhashedPassword;

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();

        $token = JwtService::getTokenStringFromAccount($this->user); // Retrieves the generated token

        //act
        $this->postJson($this->getRoute(), $this->stub, ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->getRoute(), $this->stub, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_404_if_user_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $id = User::all()->last()->id + 1;

        //act
        $this->postJson($this->getRoute($id), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'user_not_found']);
    }

    /** @test */
    public function it_gets_422_if_there_is_not_old_password_or_new_password_attribute()
    {
        //arrange
        $this->setThingsUp();
        unset($this->stub['meta']['password']);

        //act
        $this->postJson($this->getRoute(), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->stub['meta']['password'] = str_random(Constant::MIN_PASSWORD_LENGTH - 1);

        //act
        $this->postJson($this->getRoute(), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->stub['meta']['password'] = str_random(Constant::MAX_PASSWORD_LENGTH + 1);

        //act
        $this->postJson($this->getRoute(), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_403_if_user_is_not_captain_or_partner_or_id_do_not_match_the_account_id_or_password_is_already_set()
    {
        //arrange
        $this->setThingsUp();
        $this->user->is_captain = 0;
        $this->user->is_partner = 0;
        $this->user->save();

        //act
        $this->postJson($this->getRoute(), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner_id_match_password_already_set']);

        //arrange
        $this->setThingsUp();
        $id = factory(\App\Models\User::class)->states(['dummy'])->create()->id;

        //act
        $this->postJson($this->getRoute($id), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner_id_match_password_already_set']);

        //arrange
        $this->setThingsUp();
        $this->user->is_set_password = 1;
        $this->user->save();

        //act
        $this->postJson($this->getRoute($id), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'user_not_captain_or_partner_id_match_password_already_set']);
    }

    /** @test */
    public function it_gets_200_if_parameters_are_valid()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->getRoute(), $this->stub, $this->getHeaders($this->user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $userEdited = User::find($this->user->id);
        $this->assertEquals($userEdited->is_set_password, true);
    }

    protected function setThingsUp()
    {
        $this->user = factory(\App\Models\User::class)->states(['dummy'])->make();
        $this->unhashedPassword = $this->fake->password(Constant::MIN_PASSWORD_LENGTH, Constant::MAX_PASSWORD_LENGTH);
        $this->user->is_mail_activated = 1;
        $this->user->is_set_password = 0;
        $this->user->is_captain = 1;
        $this->user->is_partner = 1;
        $this->user->save();
        $this->stub = JsonHelper::createMetaMessage([
            'password' => $this->unhashedPassword,
            ]
        );
    }

    protected function getRoute($id = null)
    {
        if (!isset($id)) {
            $id = $this->user->id;
        }
        return 'v1/users/' . $id . '/password';
    }


}