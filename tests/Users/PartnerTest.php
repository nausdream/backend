<?php

/**
 * User: Giuseppe
 * Date: 27/12/2016
 * Time: 17:12
 */

use App\Models\Administrator;
use App\Models\Authorization;
use App\Models\Country;
use App\TranslationModels\Language;
use App\Models\Currency;
use App\Models\User;
use App\Constant;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\TranslationModels\Language as TranslationLanguage;


class PartnerTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    /** @test */
    public function it_creates_a_partner_given_valid_parameters()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $this->postJson('v1/partners', $this->getStub(), $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_first_name()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();
        $stub['data']['attributes']["first_name"] = null;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["first_name"] = str_random(Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_phone()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();
        $stub['data']['attributes']["phone"] = null;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["phone"] = str_random(Constant::PHONE_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_email()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();
        $stub['data']['attributes']["email"] = null;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["email"] = str_random(Constant::EMAIL_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_invalid_currency()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();
        $stub['data']['attributes']["currency"] = null;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["currency"] = str_random(Constant::CURRENCY_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter()
    {

        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);

        /** last_name */
        $stub = $this->getStub();
        $stub['data']['attributes']["last_name"] = null;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["last_name"] = str_random(Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** sex */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["sex"] = 'c';

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** birth_date */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["birth_date"] = $this->fake->firstName;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** nationality */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["nationality"] = 1;

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_name */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["enterprise_name"] = str_random(Constant::ENTERPRISE_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** vat_number */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["vat_number"] = str_random(Constant::ENTERPRISE_VAT_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_address */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["enterprise_address"] = str_random(Constant::ENTERPRISE_ADDRESS_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_city */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["enterprise_city"] = str_random(Constant::ENTERPRISE_CITY_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_zip_code */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["enterprise_zip_code"] = str_random(Constant::ENTERPRISE_ZIP_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** enterprise_country */
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']["enterprise_country"] = str_random(Constant::COUNTRY_NAME_LENGTH + 1);

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function it_gets_409_if_there_is_invalid_type()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();
        $stub['data']['type'] = 'user';

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

    }


    /** @test */
    public function it_gets_406_if_there_is_already_a_partner_with_same_email_and_phone_active()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));
        $partner = User::where('email', $stub['data']['attributes']['email'])
            ->first();
        $partner->is_mail_activated = true;
        $partner->is_phone_activated = true;
        $partner->save();
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function it_gets_406_if_there_is_already_a_partner_with_same_email_active()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));
        $partner = User::where('email', $stub['data']['attributes']['email'])
            ->first();
        $partner->is_mail_activated = true;
        $partner->save();
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function it_gets_406_if_there_is_already_a_partner_with_same_phone_active()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));
        $partner = User::where('phone', $stub['data']['attributes']['phone'])
            ->first();
        $partner->is_phone_activated = true;
        $partner->save();
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_ACCEPTABLE);

    }

    /** @test */
    public function email_is_sent_to_partner_on_partners_registration()
    {
        //arrange
        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);
        $stub = $this->getStub();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->seeEmailsSent(1);
        $this->seeEmailTo($stub['data']['attributes']['email']);
        $this->seeEmailFrom("noreply@nausdream.com");
    }

    /** @test */
    public function it_gets_403_if_user_is_not_an_administrator()
    {
        //arrange
        $user_1 = $this->createUser();
        $user_1->save();
        $stub = $this->getStub();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot create this data']);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $stub = $this->getStub();

        $admin = factory(Administrator::class)->create();
        $authorization = Authorization::where('name', 'partners')->first();
        $admin->authorizations()->attach($authorization->id);

        //act
        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson('v1/partners', $stub, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_gets_403_if_it_is_an_administrator_but_has_not_permission_for_that_type_of_users()
    {
        /** users */
        //arrange
        $stub = $this->getStub();
        $admin = factory(Administrator::class)->create();

        //act
        $this->postJson('v1/partners', $stub, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot create this data']);
    }

    protected function getStub()
    {
        return [
            'data' => [
                'type' => 'users',
                'attributes' => [
                    'first_name' => $this->fake->firstName,
                    'last_name' => $this->fake->lastName,
                    'sex' => $this->fake->randomElement(['m', 'f']),
                    'birth_date' => $this->fake->date(),
                    'email' => $this->fake->email,
                    'phone' => $this->fake->phoneNumber,
                    'nationality' => Country::inRandomOrder()->first()->name,
                    'currency' => Currency::inRandomOrder()->first()->name,
                    'enterprise_name' => $this->fake->name,
                    'vat_number' => str_random(Constant::ENTERPRISE_VAT_LENGTH),
                    'enterprise_address' => $this->fake->streetAddress,
                    'enterprise_city' => $this->fake->city,
                    'enterprise_zip_code' => $this->fake->postcode,
                    'enterprise_country' => Country::inRandomOrder()->first()->name
                ]
            ]
        ];

    }

    public function createUser()
    {
        $user = factory(User::class)->make();

        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->docking_place = $this->fake->city;
        $user->first_name_skipper = $this->fake->firstName;
        $user->last_name_skipper = $this->fake->lastName;
        $user->skippers_have_license = $this->fake->boolean;
        $user->first_name_contact = $this->fake->firstName;
        $user->last_name_contact = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->is_captain = $this->fake->boolean;
        $user->is_skipper = $this->fake->boolean;
        $user->sex = $this->fake->randomElement(array('m', 'f', null));
        $user->birth_date = $this->fake->date();
        $user->nationality = Country::inRandomOrder()->first()->name;
        $user->currency = Currency::inRandomOrder()->first()->name;
        $user->language = TranslationLanguage::inRandomOrder()->first()->language;
        $user->description = $this->fake->text();
        $user->enterprise_name = $this->fake->name;
        $user->vat_number = str_random(Constant::ENTERPRISE_VAT_LENGTH);
        $user->enterprise_address = $this->fake->address;
        $user->enterprise_city = $this->fake->city;
        $user->enterprise_zip_code = $this->fake->postcode;
        $user->enterprise_country = Country::inRandomOrder()->first()->name;
        $user->has_card = $this->fake->boolean;
        $user->default_fee = $this->fake->numberBetween(1, 30);
        $user->photo_version = null;
        $user->is_partner = $this->fake->boolean;
        $user->is_mail_activated = true;
        $user->is_suspended = false;
        $user->newsletter = $this->fake->boolean;
        $user->fuid = $this->fake->numberBetween(1);
        $user->is_phone_activated = true;
        $user->card_price = $this->fake->numberBetween(0);

        return $user;
    }
}