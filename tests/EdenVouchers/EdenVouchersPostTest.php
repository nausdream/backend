<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class EdenVouchersPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route = 'v1/eden-vouchers';
    private $admin;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getStub(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_of_resource_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['type'] = 'not_eden_voucher';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_422_if_one_of_the_attributes_of_the_request_is_not_valid()
    {
        //village_name
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['village_name'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_name'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_name']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['village_name']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_name'] = str_random(\App\Constant::ENTERPRISE_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'village_name']);

        //assistant_name
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['assistant_name'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'assistant_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['assistant_name'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'assistant_name']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['assistant_name']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'assistant_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['assistant_name'] = str_random(\App\Constant::FIRST_NAME_LENGTH + \App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'assistant_name']);

        //village_phone
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['village_phone'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_phone']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_phone'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_phone']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['village_phone']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_phone']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_phone'] = str_random(\App\Constant::ENTERPRISE_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'village_phone']);

        //village_mail
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['village_mail'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_mail']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_mail'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_mail']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['village_mail']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'village_mail']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_mail'] = $this->fake->email . str_random(\App\Constant::EMAIL_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'village_mail']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['village_mail'] = 'not_an_email';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email']);
        $this->seeJson(['title' => 'village_mail']);

        //voucher_number
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['voucher_number'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'voucher_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['voucher_number'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'voucher_number']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['voucher_number']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'voucher_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['voucher_number'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'voucher_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['voucher_number'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'voucher_number']);

        //contact_name
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['contact_name'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'contact_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['contact_name'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'contact_name']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['contact_name']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'contact_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['contact_name'] = str_random(\App\Constant::FIRST_NAME_LENGTH + \App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'contact_name']);

        //guest_name
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_name'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_name'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_name']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['guest_name']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_name'] = str_random(\App\Constant::FIRST_NAME_LENGTH + \App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'guest_name']);

        //room_number
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['room_number'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'room_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['room_number'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'room_number']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['room_number']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'room_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['room_number'] = str_random(\App\Constant::ROOM_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'room_number']);

        //guests
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['guests'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guests']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guests'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guests']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['guests']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guests']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guests'] = str_random(\App\Constant::GUESTS_FOR_EDEN_VOUCHER_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'guests']);

        //experience_title
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_title']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = str_random(\App\Constant::EXPERIENCE_TITLE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'experience_title']);

        //tour_number
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['tour_number'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'tour_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['tour_number'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'tour_number']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['tour_number']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'tour_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['tour_number'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'tour_number']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['tour_number'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'tour_number']);

        //experience_date
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_date']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_date']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'experience_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = \Carbon\Carbon::yesterday()->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'date_greater_or_equal_today']);
        $this->seeJson(['title' => 'experience_date']);

        //departure_port
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_port']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = str_random(\App\Constant::PORT_DEPARTURE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'departure_port']);

        //departure_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'departure_time']);

        //arrival_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['arrival_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'arrival_time']);

        //guest_language
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_language'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_language']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_language'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_language']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['guest_language']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'guest_language']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['guest_language'] = 'not_a_language';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'guest_language']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_200_if_request_is_ok_and_it_is_from_admin_with_bookings_authorization()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(1);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");
    }

    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);
    }

    protected function getStub()
    {
        return [
            'data'=> [
                'type'=> 'eden-vouchers',
                'attributes'=> [
                    'village_name' => $this->fake->name,
                    'assistant_name' => $this->fake->name,
                    'village_phone' => $this->fake->phoneNumber,
                    'village_mail' => $this->fake->email,
                    'voucher_number' => $this->fake->numberBetween(1, 100),
                    'contact_name' => $this->fake->name,
                    'guest_name' => $this->fake->name,
                    'room_number' => $this->fake->text(\App\Constant::ROOM_LENGTH),
                    'guests' => $this->fake->text(\App\Constant::GUESTS_FOR_EDEN_VOUCHER_LENGTH),
                    'experience_title' => $this->fake->text(30),
                    'tour_number' => $this->fake->numberBetween(1, 5),
                    'experience_date' => \Carbon\Carbon::today()->addDays(10)->format('Y-m-d'),
                    'departure_port' => $this->fake->city,
                    'departure_time' => $this->fake->time(),
                    'arrival_time' => $this->fake->time(),
                    'guest_language' => \App\TranslationModels\Language::inRandomOrder()->first()->language,
                ]
	        ]
        ];
    }

}