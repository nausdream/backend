<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperienceStepsPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $admin;
    private $experience;
    private $route;
    private $requestBody;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/steps';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = User::all()->random();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_last_finished_experience_version_is_not_in_admin_area()
    {
        //arrange
        $this->setThingsUp();
        $lastExperienceVersion = $this->experience->experienceVersions()->get()->last();
        $lastExperienceVersion->departure_lat = 5;
        $lastExperienceVersion->save();
        $smallArea = factory(\App\Models\Area::class)->create(['point_a_lat' => 6, 'point_b_lat' => 7]);
        $this->admin->area()->associate($smallArea);
        $this->admin->save();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_201_and_create_step_message()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertCreated();
    }

    /**
     * @test
     */
    public function it_returns_201_and_edit_step_message_if_it_already_exists_for_the_same_experience_and_step()
    {
        //arrange
        $this->setThingsUp();
        $lastExperienceVersion = $this->experience->experienceVersions()->get()->last();
        $lastExperienceVersion->steps()->save(factory(\App\Models\Step::class)->make(['step' => 1]));
        $this->requestBody['data']['attributes']['step'] = '1';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());

        //assert
        $this->assertCreated();
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_id_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getRequestBody(99999999999999), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_404_if_there_are_no_finished_experience_versions()
    {
        //arrange
        $this->setThingsUp();
        $this->experience->experienceVersions()->delete();
        $this->experience->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class)->states('dummy', 'finished_and_accepted')->make(['is_finished' => false]));

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_is_not_steps()
    {
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_validates_fields()
    {
        // STEP
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = -2;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = 5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = 2.5;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = 'not an int';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = true;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['step'] = '3*1';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        // MESSAGE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['attributes']['message'] = null;
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);





        // RELATIONSHIP TYPE
        //arrange
        $this->setThingsUp();
        $this->requestBody['data']['relationships']['experience']['data']['type'] = 'wrong_type';
        //act
        $this->postJson($this->route, $this->requestBody, $this->getHeaders());
        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    // HELPERS

    protected function setThingsUp()
    {
        factory(\App\Models\Area::class)->states('world')->create();
        factory(\App\Models\Area::class, 4)->create();

        $this->admin = factory(Administrator::class)->create();
        $this->admin->area()->associate(\App\Models\Area::all()->where('name', 'world')->first()->id);
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'experiences')->first()->id);
        $this->admin->save();

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $boats = factory(\App\Models\Boat::class, 2)
            ->create()
            ->each(function ($b) use ($users) {
                $b->user()->associate($users->random());
                $b->boatVersions()->saveMany(factory(\App\Models\BoatVersion::class, 3)->states('dummy')->make());
                $b->boatVersions()->save(factory(\App\Models\BoatVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
                $b->save();
            });
        $experiences = factory(\App\Models\Experience::class, 6)
            ->create()
            ->each(function ($e) use ($boats) {
                $e->boat()->associate($boats->random());
                $e->experienceVersions()->saveMany(factory(\App\Models\ExperienceVersion::class, 3)->states('dummy')->make());
                $e->experienceVersions()->save(factory(\App\Models\ExperienceVersion::class, 1)->states('dummy', 'finished_and_accepted')->make());
                $e->save();
            });

        $this->experience = $experiences->random();
        $this->requestBody = $this->getRequestBody();
    }

    public function getRoute(int $couponId)
    {
        return 'v1/coupons/' . $couponId;
    }

    protected function getHeaders($account = null)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account ?? $this->admin)];
    }

    protected function getRequestBody(int $id = null)
    {
        return [
            "data" => [
                "type" => "steps",
                "attributes" => [
                    "step" => (string) $this->fake->numberBetween(1, 4),
                    "message" => $this->fake->text()
                ],
                "relationships" => [
                    "experience" => [
                        "data" => [
                            "type" => "experiences",
                            "id" => $id ?? $this->experience->id
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function assertCreated(SymfonyResponse $status = null)
    {
        $this->assertResponseStatus($status ?? SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['message' => $this->requestBody['data']['attributes']['message']]);
    }
}