<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class ExperienceGetBoatExperiencesTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $type;
    private $area;

    /**
     * ExperienceGetCaptainExperiencesTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'experiences';
    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $v3inArea = \App\Services\AreaHelper::inArea($this->area, $experienceVersion_3->departure_lat, $experienceVersion_3->departure_lng);
            if ($isFinished && $v3inArea) {
                $visibleExperienceVersions[] = $experienceVersion_3;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
            } elseif ($isFinished && !$v3inArea) {
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            } elseif (!$isFinished) {
                $visibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            }
        }

        $token = JwtService::getTokenStringFromAccount($user); // Retrieves the generated token

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true&per_page=100&language=en', ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $v3inArea = \App\Services\AreaHelper::inArea($this->area, $experienceVersion_3->departure_lat, $experienceVersion_3->departure_lng);
            if ($isFinished && $v3inArea) {
                $visibleExperienceVersions[] = $experienceVersion_3;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
            } elseif ($isFinished && !$v3inArea) {
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            } elseif (!$isFinished) {
                $visibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            }
        }

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true&per_page=100&language=en', ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_list_in_admin_area_else_nothing()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $v3inArea = \App\Services\AreaHelper::inArea($this->area, $experienceVersion_3->departure_lat, $experienceVersion_3->departure_lng);
            if ($v3inArea) {
                $visibleExperienceVersions[] = $experienceVersion_3;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
            } else {
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            }
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true&per_page=100&language=en', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function if_user_OR_admin_without_experiences_permission_it_returns_403()
    {
        // RANDOM USER
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $v3inArea = \App\Services\AreaHelper::inArea($this->area, $experienceVersion_3->departure_lat, $experienceVersion_3->departure_lng);
            if ($isFinished && $v3inArea) {
                $visibleExperienceVersions[] = $experienceVersion_3;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
            } elseif ($isFinished && !$v3inArea) {
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            } elseif (!$isFinished) {
                $visibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            }
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true', $this->getHeaders(new User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);


        // ADMIN WITH NO PERMISSION
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $v3inArea = \App\Services\AreaHelper::inArea($this->area, $experienceVersion_3->departure_lat, $experienceVersion_3->departure_lng);
            if ($isFinished && $v3inArea) {
                $visibleExperienceVersions[] = $experienceVersion_3;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
            } elseif ($isFinished && !$v3inArea) {
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            } elseif (!$isFinished) {
                $visibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
                $notVisibleExperienceVersions[] = $experienceVersion_3;
            }
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function if_captain_it_returns_a_list_of_last_experience_version()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_3 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_3->save();

            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_1;
            $visibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=true&per_page=100', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_last_finished_accepted_experience_version()
    {
        //ADMIN WITH PERMISSIONS
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = -200005;
            $experienceVersion_2->save();

            $experienceVersion_3 = $this->getExperienceVersion($experience, false);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = 5;
            $experienceVersion_3->save();

            $visibleExperienceVersions[] = $experienceVersion_1;
            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=false&per_page=100', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }


        //ADMIN WITHOUT PERMISSIONS
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = -200005;
            $experienceVersion_2->save();

            $experienceVersion_3 = $this->getExperienceVersion($experience, false);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = 5;
            $experienceVersion_3->save();

            $visibleExperienceVersions[] = $experienceVersion_1;
            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=false&per_page=100', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }

        //RANDOM USER
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = -200005;
            $experienceVersion_2->save();

            $experienceVersion_3 = $this->getExperienceVersion($experience, false);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = 5;
            $experienceVersion_3->save();

            $visibleExperienceVersions[] = $experienceVersion_1;
            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=false&per_page=100', $this->getHeaders(new User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }



        //CAPTAIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = -200005;
            $experienceVersion_2->save();

            $experienceVersion_3 = $this->getExperienceVersion($experience, false);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = 5;
            $experienceVersion_3->save();

            $visibleExperienceVersions[] = $experienceVersion_1;
            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=false&per_page=100', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }


        //VISITOR
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $visibleExperienceVersions = [];
        $notVisibleExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience);
            $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = -200005;
            $experienceVersion_2->save();

            $experienceVersion_3 = $this->getExperienceVersion($experience, false);
            $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_3->departure_lat = 5;
            $experienceVersion_3->departure_lng = 5;
            $experienceVersion_3->save();

            $visibleExperienceVersions[] = $experienceVersion_1;
            $notVisibleExperienceVersions[] = $experienceVersion_2;
            $notVisibleExperienceVersions[] = $experienceVersion_3;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?editing=false&per_page=100');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_200_and_empty_list_if_experience_not_found()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        //act
        $this->getJson($this->getRoute($boat) . '?per_page=50');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
    }

    /**
     * ?editing=false
     *
     * @test
     */
    public function it_returns_200_and_empty_list_if_experience_version_not_found()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $notFinishedExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience, false);
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $experienceVersion_2 = $this->getExperienceVersion($experience, false);
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = 5;
            $experienceVersion_2->save();

            $notFinishedExperienceVersions[] = $experienceVersion_1;
            $notFinishedExperienceVersions[] = $experienceVersion_2;
        }

        //act
        $this->getJson($this->getRoute($boat) . '?per_page=50', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['data' => []]);
        $this->seeJson(['total_pages' => 1]);
        foreach ($notFinishedExperienceVersions as $notFinishedExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notFinishedExperienceVersion->arrival_port]);
        }
    }

    /**
     * ?editing=true
     *
     * @test
     */
    public function it_returns_404_if_boat_not_found()
    {
        //arrange
        $admin = $this->getSuperAdmin();
        $lastBoat = Boat::all()->last();
        $boatId = isset($lastBoat) ? $lastBoat->id+1 : 1;

        //act
        $this->getJson('/v1/boats/' . $boatId . '/experiences' . '?editing=true&per_page=100', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        if (isset($account)) {
            return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
        }
        return [];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();
        $this->area = $area;

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRoute(Boat $boat)
    {
        return 'v1/boats/' . $boat->id . '/experiences';
    }
}