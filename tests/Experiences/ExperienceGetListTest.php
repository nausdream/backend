<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperienceGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;
    private $area;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/experiences';
        $this->type = 'experiences';
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        // AUTHENTICATED USER
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route, $this->getHeaders(new User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        // VISITOR
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_last_finished_experience_versions_if_within_admin_area()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $finishedExperienceVersions = [];
        $notLastFinishedExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_2 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_2->status = $isFinished ? Constant::STATUS_ACCEPTED : null;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_2->save();

            if ($isFinished && \App\Services\AreaHelper::inArea($this->area, $experienceVersion_2->departure_lat, $experienceVersion_2->departure_lng)) {
                $finishedExperienceVersions[] = $experienceVersion_2;
                $notLastFinishedExperienceVersions[] = $experienceVersion_1;
            } else {
                $notLastFinishedExperienceVersions[] = $experienceVersion_1;
                $notLastFinishedExperienceVersions[] = $experienceVersion_2;
            }

        }

        //act
        $this->getJson($this->route . '?per_page=50&language=en', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($finishedExperienceVersions as $finishedExperienceVersion) {
            $this->seeJson(['arrival_port' => $finishedExperienceVersion->arrival_port]);
        }
        foreach ($notLastFinishedExperienceVersions as $notLastFinishedExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notLastFinishedExperienceVersion->arrival_port]);
        }
    }


    /**
     * @test
     */
    public function it_returns_a_list_of_all_last_experience_versions_if_within_admin_area_if_editing_true()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);

        $finishedExperienceVersions = [];
        $notLastFinishedExperienceVersions = [];
        for ($i = 0; $i < 20; $i++) {
            $experience = $this->getExperience($boat);

            $experienceVersion_1 = $this->getExperienceVersion($experience);
            $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
            $experienceVersion_1->departure_lat = 5;
            $experienceVersion_1->departure_lng = 5;
            $experienceVersion_1->save();

            $isFinished = $this->fake->boolean;
            $experienceVersion_2 = $this->getExperienceVersion($experience, $isFinished);
            $experienceVersion_2->status = $isFinished ? Constant::STATUS_ACCEPTED : null;
            $experienceVersion_2->departure_lat = 1;
            $experienceVersion_2->departure_lng = $this->fake->numberBetween(-5, 5);
            $experienceVersion_2->save();

            if (\App\Services\AreaHelper::inArea($this->area, $experienceVersion_2->departure_lat, $experienceVersion_2->departure_lng)) {
                $visibleExperienceVersions[] = $experienceVersion_2;
                $notVisibleExperienceVersions[] = $experienceVersion_1;
            }

        }

        //act
        $this->getJson($this->route . '?editing=true&per_page=50&language=en', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        foreach ($visibleExperienceVersions as $visibleExperienceVersion) {
            $this->seeJson(['arrival_port' => $visibleExperienceVersion->arrival_port]);
        }
        foreach ($notVisibleExperienceVersions as $notVisibleExperienceVersion) {
            $this->dontSeeJson(['arrival_port' => $notVisibleExperienceVersion->arrival_port]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_experiences()
    {
        if (\App\Models\Experience::all()->count() == 0){
            //arrange
            $user = $this->getUser();
            $admin = $this->getSuperAdmin();
            $this->setSmallArea($admin);
            $boat = $this->getBoat($user);

            //act
            $this->getJson($this->route, $this->getHeaders($admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_finished_experience_versions()
    {
        if (\App\Models\ExperienceVersion::all()->count() == 0){
            //arrange
            $user = $this->getUser();
            $admin = $this->getSuperAdmin();
            $this->setSmallArea($admin);
            $boat = $this->getBoat($user);

            $notFinishedExperienceVersions = [];
            for ($i = 0; $i < 20; $i++) {
                $experience = $this->getExperience($boat);

                $experienceVersion_1 = $this->getExperienceVersion($experience, false);
                $experienceVersion_1->departure_lat = 5;
                $experienceVersion_1->departure_lng = 5;
                $experienceVersion_1->save();

                $experienceVersion_2 = $this->getExperienceVersion($experience, false);
                $experienceVersion_2->departure_lat = 1;
                $experienceVersion_2->departure_lng = 5;
                $experienceVersion_2->save();

                $notFinishedExperienceVersions[] = $experienceVersion_1;
                $notFinishedExperienceVersions[] = $experienceVersion_2;
            }

            //act
            $this->getJson($this->route . '?per_page=50', $this->getHeaders($admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
            foreach ($notFinishedExperienceVersions as $notFinishedExperienceVersion) {
                $this->dontSeeJson(['arrival_port' => $notFinishedExperienceVersion->arrival_port]);
            }
        }
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_finished_experience_versions_in_admin_area()
    {
        if (\App\Models\ExperienceVersion::all()->count() == 0){
            //arrange
            $user = $this->getUser();
            $admin = $this->getSuperAdmin();
            $this->setSmallArea($admin);
            $boat = $this->getBoat($user);

            $notFinishedOrInAdminAreaExpVersions = [];
            for ($i = 0; $i < 20; $i++) {
                $experience = $this->getExperience($boat);

                $experienceVersion_1 = $this->getExperienceVersion($experience, false);
                $experienceVersion_1->departure_lat = 5;
                $experienceVersion_1->departure_lng = 5;
                $experienceVersion_1->save();

                $experienceVersion_2 = $this->getExperienceVersion($experience);
                $experienceVersion_2->departure_lat = -1;
                $experienceVersion_2->departure_lng = 5;
                $experienceVersion_2->save();

                $notFinishedOrInAdminAreaExpVersions[] = $experienceVersion_1;
                $notFinishedOrInAdminAreaExpVersions[] = $experienceVersion_2;
            }

            //act
            $this->getJson($this->route . '?per_page=50', $this->getHeaders($admin));

            //assert
            $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
            $this->seeJson(['data' => []]);
            $this->seeJson(['total_pages' => 1]);
            foreach ($notFinishedOrInAdminAreaExpVersions as $notFinishedOrInAdminAreaExpVersion) {
                $this->dontSeeJson(['arrival_port' => $notFinishedOrInAdminAreaExpVersion->arrival_port]);
            }
        }
    }

    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();
        $this->area = $area;

        $admin->area()->associate($area);
        $admin->save();
    }
}