<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperienceChangeStatusTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $experience_1;
    public $id;
    public $experienceVersions;
    public $requestBody;
    public $stub;

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_accepted_and_sends_email()
    {
        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_rejected_and_sends_email()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_REJECTED;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_200_and_changes_status_from_pending_to_accepted_conditionally_and_sends_email()
    {
        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED_CONDITIONALLY;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id), $status);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($this->user_1->email);
        $this->seeEmailFrom('noreply@nausdream.com');
    }

    /** @test */
    public function it_returns_200_and_changes_status_from_accepted_to_freezed_and_vice_versa()
    {
        //arrange ACCEPTED->FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $status = Constant::STATUS_FREEZED;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id), $status);


        //arrange FREEZED->ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id), $status);
    }

    /** @test */
    public function it_returns_422_if_status_is_not_in_status_array()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $requestBody = $this->getRequestBody($this->id, null);
        $requestBody['data']['attributes']['status'] = 'pippo';

        //act NEGATIVE NUMBER
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $requestBody = $this->getRequestBody($this->id, null);
        $requestBody['data']['attributes']['status'] = new \App\Models\User();

        //act OTHER OBJECT
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

    }

    /** @test */
    public function it_returns_403_if_changing_from_any_status_to_the_same_status()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED_CONDITIONALLY), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_REJECTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_FREEZED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_null_to_any()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, null);


        //act PENDING
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED_CONDITIONALLY), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_REJECTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_FREEZED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_any_to_pending()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_pending_to_freezed()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_FREEZED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_accepted_to_other_than_freezed()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act PENDING
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //act ACCEPTED_CONDITIONALLY
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED_CONDITIONALLY), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //act REJECTED
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_REJECTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_changing_from_freezed_to_other_than_accepted()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act PENDING
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_PENDING), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //act ACCEPTED_CONDITIONALLY
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED_CONDITIONALLY), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //act REJECTED
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_REJECTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->experienceVersions[2]->departure_lat = -5001;
        $this->experienceVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_finished_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $this->experienceVersions[0]->is_finished = false;
        $this->experienceVersions[0]->save();
        $this->experienceVersions[1]->is_finished = false;
        $this->experienceVersions[1]->save();
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_experiences()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $requestBody = $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED);
        unset($requestBody['data']['type']);
        $requestBody['data']['type'] = 'wrong_type';

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $requestBody = $this->getRequestBody($this->id, Constant::STATUS_ACCEPTED);
        unset($requestBody['data']['id']);
        $requestBody['data']['id'] = (string)($this->experience_1->id - 1);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $empty_experience = $this->getExperience($this->boat);

        //act
        $this->patchJson($this->getRoute($empty_experience->id), $this->getRequestBody($empty_experience->id, Constant::STATUS_ACCEPTED), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_validates_status()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);
        $status = Constant::STATUS_ACCEPTED;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $status), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[1]->id), $status);
    }

    // HELPERS
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        $requestBody = [
            'duration' => $this->fake->numberBetween(1, 10),
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time('h:i:s'),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time('h:i:s'),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
            'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];

        if ($isFinished) {
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $experienceId, $status)
    {
        return [
            'data' => [
                'type' => 'experiences',
                'id' => (string)$experienceId,
                'attributes' => [
                    'status' => \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)
                ]
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/experiences/' . $id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);

        $this->stub = $this->getExperienceVersionStub(true);
        $this->requestBody = $this->getRequestBody($this->experience_1->id, Constant::STATUS_ACCEPTED);
    }

    protected function setStatus(...$status)
    {
        for ($i = 0; $i < count($this->experienceVersions); $i++) {
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\ExperienceVersion $experienceVersion, int $status)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['status' => \App\Services\ValueKeyHelper::getValueByKey($status, Constant::STATUS_NAMES)]);
        $this->assertEquals($status, $experienceVersion->status);
    }

    protected function assertNotEdited(\App\Models\ExperienceVersion $experienceVersion)
    {
        $this->dontSeeJson(['departure_port' => $experienceVersion->departure_port]);
    }

    protected function getExperienceVersionById(int $id)
    {
        return \App\Models\ExperienceVersion::find($id);
    }
}