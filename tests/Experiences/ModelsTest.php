<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 18:42
 */

use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\FixedAdditionalService;
use App\Models\Rule;

class ModelsTest extends \ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /**
     * @test
     */
    public function experience_version_rules_and_fixed_additional_services_relationships_are_correct()
    {

        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        $boat = factory(Boat::class)->create();

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->currency = $user->currency;
        $experience->experienceVersions()->save($experienceVersion);

        $rule = new Rule();
        $rule->name = 'animali';
        $rule->save();
        $fixedAdditionalService = new FixedAdditionalService();
        $fixedAdditionalService->name = 'doccia';
        $fixedAdditionalService->save();

        $experienceVersion->rules()->attach($rule->id);
        $experienceVersion->fixedAdditionalServices()->attach($fixedAdditionalService->id, [
            'price' => 0, 'per_person' => true, 'currency' => \App\Models\Currency::all()->random()->name
        ]);

        $this->assertEquals($rule->name, $experienceVersion->rules()->first()->name);
        $this->assertEquals($fixedAdditionalService->name, $experienceVersion->fixedAdditionalServices()->first()->name);
    }
}