<?php

/**
 * User: Giuseppe
 * Date: 20/01/2017
 * Time: 16:59
 */

use App\Constant;
use App\Models\Administrator;
use App\TranslationModels\User as Translator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceVersion;
use App\Models\Seo;
use App\Services\JwtService;
use App\Services\SeoHelper;
use App\TranslationModels\Language;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencesSlugUrlsGetTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $user_1;
    protected $user_2;
    protected $admin;
    protected $translator;
    protected $boat;
    protected $experience_1;
    protected $id;
    protected $experienceVersions;
    protected $requestBody;
    protected $stub;
    protected $route;

    /** @test */
    public function it_returns_200_and_get_slug_url_admin()
    {
        //arrange
        $this->setThingsUp();
        $title = $this->fake->text(Constant::SEO_TITLE_LENGTH);
        $count = Seo::all()->count();

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['meta' => ['slug_url' => SeoHelper::getSlugUrl($title)]]);
        $this->assertEquals($count, Seo::all()->count());
    }

    /** @test */
    public function it_returns_200_and_get_slug_url_translator()
    {
        //arrange
        $this->setThingsUp();
        $title = $this->fake->text(Constant::SEO_TITLE_LENGTH);

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->translator));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['meta' => ['slug_url' => SeoHelper::getSlugUrl($title)]]);
    }

    /** @test */
    public function it_returns_422_title_is_incorrect_or_unset()
    {
        //arrange
        $this->setThingsUp();
        $title = str_random(Constant::SEO_TITLE_LENGTH + 1);

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $title = '';

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $route = 'v1/slugurls';

        //act
        $this->getJson($route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

    }

    /** @test */
    public function it_returns_403_if_admin_have_not_authorization_for_experiences()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $title = $this->fake->text(Constant::SEO_TITLE_LENGTH);

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /** @test */
    public function it_returns_403_if_user_is_not_admin_or_translator()
    {
        //arrange
        $this->setThingsUp();
        $title = $this->fake->text(Constant::SEO_TITLE_LENGTH);

        //act
        $this->getJson($this->getRoute($title), $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    // HELPERS
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        $requestBody = [
            'duration' => $this->fake->numberBetween(1, 10),
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time('h:i:s'),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time('h:i:s'),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
            'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];

        if ($isFinished) {
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getSeoStub()
    {
        $requestBody = [
            'language' => Language::inRandomOrder()->first()->language,
            'description' => $this->fake->text(Constant::SEO_DESCRIPTION_LENGTH),
            'title' => $this->fake->text(Constant::SEO_TITLE_LENGTH)
        ];
        return $requestBody;
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getTranslator()
    {
        return factory(Translator::class)->create();
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getRoute($title)
    {
        return 'v1/slugurls?title=' . $title;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $experienceId, array $stub)
    {
        return [
            'data' => [
                'type' => 'seos',
                'attributes' => $stub,
                'relationships' => [
                    'experience' => [
                        'data' => [
                            'type' => 'experiences',
                            'id' => (string)$experienceId
                        ]
                    ]
                ]
            ]
        ];
    }


    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->translator = $this->getTranslator();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);

        $this->experienceVersions[0]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[0]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->experienceVersions[1]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[1]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->experienceVersions[2]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[2]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->stub = $this->getSeoStub();
        $this->requestBody = $this->getRequestBody($this->experience_1->id, $this->stub);
    }

    protected function setStatus(int ...$status){
        for ($i = 0; $i < count($this->experienceVersions); $i++){
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\ExperienceVersion $experienceVersion){
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $experienceVersion->departure_port]);
    }

    protected function assertNotEdited(\App\Models\ExperienceVersion $experienceVersion){
        $this->dontSeeJson(['departure_port' => $experienceVersion->departure_port]);
    }

    protected function getExperienceVersionById(int $id){
        return \App\Models\ExperienceVersion::find($id);
    }
}