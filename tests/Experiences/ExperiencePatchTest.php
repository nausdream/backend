<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Experience;
use App\Services\JwtService;
use JD\Cloudder\Facades\Cloudder;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencePatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    /**
     * ExperiencePostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /** @test */
    public function it_returns_200_and_edit_last_unfinished_experience_version()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_200_and_edit_last_accepted_conditionally_experience_version()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_200_and_create_new_experience_version_if_last_is_accepted()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
        $rule_1_id = \App\Models\Rule::all()->first()->id;
        $rule_2_id = \App\Models\Rule::all()->last()->id;
        $experienceVersion_3->rules()->attach([$rule_1_id, $rule_2_id]);

        $additionalServiceData = [
            'name' => $this->fake->name,
            'price' => $this->fake->numberBetween(0, 20),
            'per_person' => $this->fake->boolean(),
            'currency' => \App\Models\Currency::all()->random()->first()->name
        ];
        $additionalService_1 = new \App\Models\AdditionalService($additionalServiceData);
        $experienceVersion_3->additionalServices()->save($additionalService_1);

        $additionalServiceData = [
            'name' => $this->fake->name,
            'price' => $this->fake->numberBetween(0, 20),
            'per_person' => $this->fake->boolean(),
            'currency' => \App\Models\Currency::all()->random()->first()->name
        ];
        $additionalService_2 = new \App\Models\AdditionalService($additionalServiceData);
        $experienceVersion_3->additionalServices()->save($additionalService_2);

        $fixedAdditionalService_1_id = \App\Models\FixedAdditionalService::all()->first()->id;
        $experienceVersion_3->fixedAdditionalServices()->attach($fixedAdditionalService_1_id, ["price" => $this->fake->numberBetween(0, 20), "per_person" => $this->fake->boolean(), "currency" => \App\Models\Currency::all()->random()->first()->name]);
        $fixedAdditionalService_2_id = \App\Models\FixedAdditionalService::all()->first()->id;
        $experienceVersion_3->fixedAdditionalServices()->attach($fixedAdditionalService_2_id, ["price" => $this->fake->numberBetween(0, 20), "per_person" => $this->fake->boolean(), "currency" => \App\Models\Currency::all()->random()->first()->name]);

        $fixedService_1 = \App\Models\ExperienceVersionsFixedAdditionalServices::all()
            ->where('fixed_additional_service_id', $fixedAdditionalService_1_id)
            ->where('experience_version_id', $experienceVersion_3->id)
            ->last();
        $fixedService_2 = \App\Models\ExperienceVersionsFixedAdditionalServices::all()
            ->where('fixed_additional_service_id', $fixedAdditionalService_2_id)
            ->where('experience_version_id', $experienceVersion_3->id)
            ->last();

        $link = 'http://www.w3schools.com/css/img_fjords.jpg';
        $photo = $experienceVersion_3->experienceVersionPhotos()->create([
            'is_cover' => true
        ]);

        // Create public id
        $url = \App\Services\PhotoHelper::getExperiencePhotoPublicId($user->id,
            $boat->id,
            $experience->id,
            $photo->id
        );

        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );

        $photo = $experienceVersion_3->experienceVersionPhotos()->create([
            'is_cover' => false
        ]);

        // Create public id
        $url = \App\Services\PhotoHelper::getExperiencePhotoPublicId($user->id,
            $boat->id,
            $experience->id,
            $photo->id
        );

        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );

        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber + 1, $experience->experienceVersions()->get());

        $experienceVersionNew = $experience->experienceVersions()->get()->last();
        $this->assertNotEquals($experienceVersion_3->id, $experienceVersionNew->id);

        $this->assertNotNull($experienceVersionNew->rules()->where('rule_id', $rule_1_id)->first());
        $this->assertNotNull($experienceVersionNew->rules()->where('rule_id', $rule_2_id)->first());

        $additionalService_1_new = $experienceVersionNew->additionalServices()->where('name', $additionalService_1->name)->first();
        $this->assertNotEquals($additionalService_1->id, $additionalService_1_new->id);
        $this->assertEquals($additionalService_1->name,  $additionalService_1_new->name);
        $this->assertEquals($additionalService_1->price,  $additionalService_1_new->price);
        $this->assertEquals($additionalService_1->per_person,  $additionalService_1_new->per_person);
        $this->assertEquals($additionalService_1->currency,  $additionalService_1_new->currency);

        $additionalService_2_new = $experienceVersionNew->additionalServices()->where('name', $additionalService_2->name)->first();
        $this->assertNotEquals($additionalService_2->id, $additionalService_2_new->id);
        $this->assertEquals($additionalService_2->name,  $additionalService_2_new->name);
        $this->assertEquals($additionalService_2->price,  $additionalService_2_new->price);
        $this->assertEquals($additionalService_2->per_person,  $additionalService_2_new->per_person);
        $this->assertEquals($additionalService_2->currency,  $additionalService_2_new->currency);

        $fixedServiceNew_1 = \App\Models\ExperienceVersionsFixedAdditionalServices::all()
            ->where('fixed_additional_service_id', $fixedAdditionalService_1_id)
            ->where('experience_version_id', $experienceVersionNew->id)
            ->last();

        $this->assertNotEquals($fixedService_1->id, $fixedServiceNew_1->id);
        $this->assertEquals($fixedService_1->price,  $fixedServiceNew_1->price);
        $this->assertEquals($fixedService_1->per_person,  $fixedServiceNew_1->per_person);
        $this->assertEquals($fixedService_1->currency,  $fixedServiceNew_1->currency);

        $fixedServiceNew_2 = \App\Models\ExperienceVersionsFixedAdditionalServices::all()
            ->where('fixed_additional_service_id', $fixedAdditionalService_2_id)
            ->where('experience_version_id', $experienceVersionNew->id)
            ->last();

        $this->assertNotEquals($fixedService_2->id, $fixedServiceNew_2->id);
        $this->assertEquals($fixedService_2->price,  $fixedServiceNew_2->price);
        $this->assertEquals($fixedService_2->per_person,  $fixedServiceNew_2->per_person);
        $this->assertEquals($fixedService_2->currency,  $fixedServiceNew_2->currency);

        $this->assertEquals(2, $experienceVersionNew->experienceVersionPhotos()->get()->count());
        $this->assertTrue($this->url_exists(\App\Services\PhotoHelper::getExperiencePhotoLink(
            $experienceVersionNew->experienceVersionPhotos()->where('is_cover', true)->first(),
            'external',
            null
        )));
        $this->assertTrue($this->url_exists(\App\Services\PhotoHelper::getExperiencePhotoLink(
            $experienceVersionNew->experienceVersionPhotos()->where('is_cover', false)->first(),
            'external',
            null
        )));




    }

    /** @test */
    public function it_doesnt_set_is_searchable_field()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_3->is_searchable = false;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $requestBody['data']['attributes']['is_searchable'] = true;
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->seeJson(['is_searchable' => false]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_last_experience_version_is_pending()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_PENDING;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_last_experience_version_is_rejected()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_REJECTED;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_last_experience_version_is_freezed()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_FREEZED;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $user = $this->getUser();
        $user_2 = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $user = $this->getUser();
        $user_2 = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $user = $this->getUser();
        $user_2 = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_PENDING;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

    }

    /** @test */
    public function it_returns_200_and_edit_last_unfinished_experience_version_admin()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $admin = $this->getSuperAdmin();
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_200_and_create_new_experience_version_if_last_is_accepted_admin()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber + 1, $experience->experienceVersions()->get());
    }


    /** @test */
    public function it_returns_403_if_admin_has_not_experience_auth()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_experience_in_his_area()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->area_id = null;
        $admin->save();
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();
        $admin->area()->associate($area);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);
        $experienceVersion_3->departure_lat = 11;
        $experienceVersion_3->departure_lng = 11;

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_is_admin_and_experience_version_is_rejected_or_accepted_conditionally()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_REJECTED;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience);
        $experienceVersion_3->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_3->save();

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_403_if_user_is_a_visitor()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_404_if_last_experience_version_doesnt_exists()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_404_if_experience_doesnt_exists()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experienceId = 1;
        if (Experience::all()->count() != 0){
            $experienceId = Experience::all()->last()->id + 1;
        }

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experienceId, $stub);

        //act
        $this->patchJson($this->getRoute($experienceId), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
    }

    /** @test */
    public function it_returns_409_if_object_type_is_not_experiences()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $requestBody['data']['type'] = 'wrong_type';
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);
        $experience_2 = $this->getExperience($boat);

        $stub = $this->getExperienceVersionStub(false);
        $requestBody = $this->getRequestBody($experience->id, $stub);
        $requestBody['data']['id'] = (string) $experience_2->id;
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());
    }

    /** @test */
    public function it_validates_fields()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_3 = $this->getExperienceVersion($experience, false);
        $experienceVersionsNumber = $experience->experienceVersions()->get()->count();


        //=====================TYPE
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['type'] = 'not an integer';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['type'] = -10;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['type'] = 1.5;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['type'] = 15;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================DURATION
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['duration'] = 'not an integer';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['duration'] = -10;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['duration'] = 1.5;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================DEPARTURE PORT
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_port'] = str_random(Constant::PORT_DEPARTURE_LENGTH + 1);
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================DEPARTURE TIME
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_time'] = '25:22:33';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_time'] = 'not a time';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================ARRIVAL PORT
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['arrival_port'] = str_random(Constant::PORT_RETURN_LENGTH + 1);
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================ARRIVAL TIME
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['arrival_time'] = '25:22:33';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['arrival_time'] = 'not a time';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================DAY OF WEEK (and other booleans)
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['monday'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['monday'] = 3;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================KIDS/BABIES
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['kids'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['kids'] = -1;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());



        //=====================LAT/LNG
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lat'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lat'] = -50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lat'] = 50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lat'] = true;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lng'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lng'] = -50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lng'] = 50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['departure_lng'] = true;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lat'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lat'] = -50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lat'] = 50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lat'] = true;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lng'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lng'] = -50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lng'] = 50000;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['destination_lng'] = true;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['departure_port' => $stub['departure_port']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());


        //=====================TILE
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['title'] = str_random(Constant::EXPERIENCE_TITLE_LENGTH + 1);
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['title' => $stub['title']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //=====================DESCRIPTION
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['description'] = str_random(Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['description' => $stub['description']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //=====================RULES
        //arrange
        $stub = $this->getExperienceVersionStub(false);
        $stub['rules'] = str_random(Constant::EXPERIENCE_RULES_LENGTH + 1);
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->dontSeeJson(['rules' => $stub['rules']]);
        $this->assertCount($experienceVersionsNumber, $experience->experienceVersions()->get());

        //=====================ROAD_STEAD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['road_stead'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FIXED_MENU
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fixed_menu'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================STARTER_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['starter_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FIRST_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['first_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================SECOND_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['second_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================SIDE_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['side_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================DESSERT_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['dessert_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================BREAD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['bread'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================WINES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['wines'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================CHAMPAGNE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['champagne'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================WEATHER
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['weather'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================LURES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['lures'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FISHING_POLE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_pole'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FRESH_BAG
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fresh_bag'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================ICE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['ice'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================UNDERWATER_BAPTISM
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['underwater_baptism'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================DOWN_PAYMENT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['down_payment'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================DAYS
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = 0;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);





        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = Constant::MAX_EXPERIENCE_DAYS + 1;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FISHING_PARTITION
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_partition'] = 'not a fishing partition';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================FISHING_TYPE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_type'] = 'not a fishing type';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================DEPOSIT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit'] = 0;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================CANCELLATION_MAX_DAYS
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = Constant::CANCELLATION_MIN_DAYS - 1;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = Constant::CANCELLATION_MAX_DAYS + 1;
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = 'not a number';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




        //=====================DEPOSIT_CARD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_card'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //=====================DEPOSIT_CASH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_cash'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //=====================DEPOSIT_CHECK
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_check'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);


        //=====================CAPTAIN_NIGHT_ABOARD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['captain_night_aboard'] = 'not a boolean';
        $requestBody = $this->getRequestBody($experience->id, $stub);

        //act
        $this->patchJson($this->getRoute($experience->id), $requestBody, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);




    }

    // HELPERS
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        $requestBody = [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'duration' => $this->fake->numberBetween(1, 10),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time('h:i:s'),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time('h:i:s'),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
            'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
            'captain_night_aboard' => $this->fake->boolean,
        ];

        if ($isFinished){
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $experienceId, array $stub)
    {
        return [
            'data' => [
                'type' => 'experiences',
                'id' => (string)$experienceId,
                'attributes' => $stub
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/experiences/'.$id;
    }

    protected function url_exists($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        }
        return true;
    }
}