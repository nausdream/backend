<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceVersion;
use App\Models\Seo;
use App\Services\JwtService;
use App\Services\SeoHelper;
use App\TranslationModels\Language;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperienceAdminPutTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $experience_1;
    public $id;
    public $experienceVersions;
    public $requestBody;
    public $stub;

    /** @test */
    public function it_returns_200_and_edit_last_experience_version_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_edit_seo_element_for_experienceVersion_if_there_is_element_with_same_captain_language()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();
        $seo_old = new Seo([
            'title' => SeoHelper::getMetaTitle($this->experienceVersions[1]->title),
            'description' => SeoHelper::getMetaDescription($this->experienceVersions[1]->description),
            'slug_url' => SeoHelper::getSlugUrl($this->experienceVersions[1]->title),
            'language' => $this->experienceVersions[1]->experience()->first()->boat()->first()->user()->first()->language
        ]);
        $this->experienceVersions[2]->seos()->save($seo_old);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $experienceVersion_2 = ExperienceVersion::find($this->experienceVersions[2]->id);
        $seo_new = $experienceVersion_2->seos()->get()->first();
        $this->assertNotNull($seo_new);
        $this->assertEquals($seo_old->id, $seo_new->id);
        $this->assertNotEquals($seo_old->title, $seo_new->title);
        $this->assertNotEquals($seo_old->description, $seo_new->description);
        $this->assertNotEquals($seo_old->slug_url, $seo_new->slug_url);
    }


    /** @test */
    public function it_edit_seo_element_for_experienceVersion_if_there_is_element_with_translator_id_null()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();
        $language_to_not_use = $this->experienceVersions[2]->experience()->first()->boat()->first()->user()->first()->language;
        $language = Language::all()->where('language', '!=', $language_to_not_use)->first();
        $seo_old = new Seo([
            'title' => SeoHelper::getMetaTitle($this->experienceVersions[1]->title),
            'description' => SeoHelper::getMetaDescription($this->experienceVersions[1]->description),
            'slug_url' => SeoHelper::getSlugUrl($this->experienceVersions[1]->title),
            'language' => $language->language
        ]);
        $this->experienceVersions[2]->seos()->save($seo_old);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $experienceVersion_1 = ExperienceVersion::find($this->experienceVersions[2]->id);
        $seo_new = $experienceVersion_1->seos()->get()->first();
        $this->assertNotNull($seo_new);
        $this->assertEquals($seo_old->id, $seo_new->id);
        $this->assertNotEquals($seo_old->title, $seo_new->title);
        $this->assertNotEquals($seo_old->description, $seo_new->description);
        $this->assertNotEquals($seo_old->slug_url, $seo_new->slug_url);
    }

    /** @test */
    public function it_creates_a_new_seo_element_for_experienceVersion_with_number_if_there_is_already_an_equal_slug_url()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();
        $seo_1 = new Seo([
            'title' => SeoHelper::getMetaTitle($this->stub['title']),
            'description' => SeoHelper::getMetaDescription($this->stub['description']),
            'slug_url' => SeoHelper::getSlugUrl($this->stub['title']),
            'language' => $this->experienceVersions[1]->experience()->first()->boat()->first()->user()->first()->language
        ]);
        $this->experienceVersions[1]->seos()->save($seo_1);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $experienceVersion_2 = ExperienceVersion::find($this->experienceVersions[2]->id);
        $seo = $experienceVersion_2->seos()->get()->first();
        $this->assertNotNull($seo);
        $this->assertEquals($seo->slug_url, SeoHelper::getSlugUrl($experienceVersion_2->title, 1));
    }

    /** @test */
    public function it_creates_a_new_seo_element_for_experienceVersion()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();
        $shortTitleLastPart = ' ' . Constant::SEO_SEPARATOR . ' ' . Constant::SEO_DOMAIN_NAME;
        $this->stub['title'] = str_random(Constant::SEO_TITLE_LENGTH - mb_strlen($shortTitleLastPart));

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $experienceVersion_2 = ExperienceVersion::find($this->experienceVersions[2]->id);
        $seo = $experienceVersion_2->seos()->get()->first();
        $this->assertNotNull($seo);
        $this->assertEquals($experienceVersion_2->title, $seo->title);
    }

    /** @test */
    public function it_returns_422_if_there_are_not_2_photos_at_least()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();
        $this->experienceVersions[2]->experienceVersionPhotos()->delete();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
    }

    /** @test */
    public function it_sets_is_searchable_field()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->is_searchable = false;
        $this->experienceVersions[2]->save();

        $this->requestBody = $this->getRequestBody($this->id, $this->stub);
        $this->requestBody['data']['attributes']['is_searchable'] = true;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->seeJson(['is_searchable' => true]);
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->is_searchable = true;
        $this->experienceVersions[2]->save();

        $this->requestBody = $this->getRequestBody($this->id, $this->stub);
        $this->requestBody['data']['attributes']['is_searchable'] = false;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->seeJson(['is_searchable' => false]);
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_doesnt_change_status()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $oldStatus = $this->experienceVersions[2]->status;

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));
        $newStatus = ExperienceVersion::find($this->experienceVersions[2]->id)->status;

        //assert
        $this->assertEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
        $this->assertNotEquals($oldStatus, $newStatus);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experienceVersions[2]->departure_lat = -5001;
        $this->experienceVersions[2]->save();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $this->getRequestBody($this->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_experiences()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $requestBody = $this->getRequestBody($this->id, $this->stub);
        unset($requestBody['data']['type']);
        $requestBody['data']['type'] = 'wrong_type';

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_409_if_resource_id_doesnt_match_endpoint_id()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $requestBody = $this->getRequestBody($this->id, $this->stub);
        unset($requestBody['data']['id']);
        $requestBody['data']['id'] = (string) ($this->experience_1->id-1);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $empty_experience = $this->getExperience($this->boat);

        //act
        $this->patchJson($this->getRoute($empty_experience->id), $this->getRequestBody($empty_experience->id, $this->stub), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    /** @test */
    public function it_validates_fields()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);


        //=====================TYPE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['type'] = 'not an integer';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['type'] = -15;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['type'] = 1.5;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['type'] = 15;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================DURATION
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['duration'] = 'not and integer';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['duration'] = -10;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['duration'] = 1.5;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================DEPARTURE PORT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_port'] = str_random(Constant::PORT_DEPARTURE_LENGTH + 1);
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================DEPARTURE TIME
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_time'] = '25:22:33';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_time'] = 'not a time';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================ARRIVAL PORT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['arrival_port'] = str_random(Constant::PORT_RETURN_LENGTH + 1);
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================ARRIVAL TIME
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['arrival_time'] = '25:22:33';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['arrival_time'] = 'not a time';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================DAY OF WEEK (and other booleans)
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['monday'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['monday'] = 3;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================KIDS/BABIES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['kids'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['kids'] = -1;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));



        //=====================LAT/LNG
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lat'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lat'] = -50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lat'] = 50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lat'] = true;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lng'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lng'] = -50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lng'] = 50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['departure_lng'] = true;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lat'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lat'] = -50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lat'] = 50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lat'] = true;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lat'] = true;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lng'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lng'] = -50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lng'] = 50000;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['destination_lng'] = true;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================TITLE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['title'] = str_random(Constant::EXPERIENCE_TITLE_LENGTH + 1);
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DESCRIPTION
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['description'] = str_random(Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================RULES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['rules'] = str_random(Constant::EXPERIENCE_RULES_LENGTH + 1);
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================ROAD_STEAD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['road_stead'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FIXED_MENU
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fixed_menu'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================STARTER_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['starter_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FIRST_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['first_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================SECOND_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['second_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================SIDE_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['side_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DESSERT_DISH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['dessert_dish'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================BREAD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['bread'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================WINES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['wines'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================CHAMPAGNE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['champagne'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================WEATHER
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['weather'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================LURES
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['lures'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FISHING_POLE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_pole'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FRESH_BAG
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fresh_bag'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================ICE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['ice'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================UNDERWATER_BAPTISM
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['underwater_baptism'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DOWN_PAYMENT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['down_payment'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DAYS
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = 0;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = Constant::MAX_EXPERIENCE_DAYS + 1;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['days'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FISHING_PARTITION
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_partition'] = 'not a fishing partition';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================FISHING_TYPE
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['fishing_type'] = 'not a fishing type';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DEPOSIT
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit'] = 0;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================CANCELLATION_MAX_DAYS
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = Constant::CANCELLATION_MIN_DAYS - 1;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = Constant::CANCELLATION_MAX_DAYS + 1;
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['cancellation_max_days'] = 'not a number';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DEPOSIT_CARD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_card'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DEPOSIT_CASH
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_cash'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));

        //=====================DEPOSIT_CHECK
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['deposit_check'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));


        //=====================CAPTAIN_NIGHT_ABOARD
        //arrange
        $stub = $this->getExperienceVersionStub(true);
        $stub['captain_night_aboard'] = 'not a boolean';
        $requestBody = $this->getRequestBody($this->experience_1->id, $stub);

        //act
        $this->patchJson($this->getRoute($this->experience_1->id), $requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[0]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[1]->id));
        $this->assertNotEdited($this->getExperienceVersionById($this->experienceVersions[2]->id));
    }

    // HELPERS
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        $requestBody = [
            'duration' => $this->fake->numberBetween(1, 10),
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time('h:i:s'),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time('h:i:s'),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
            'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
            'captain_night_aboard' => $this->fake->boolean,
        ];

        if ($isFinished) {
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->name,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $experienceId, array $stub)
    {
        return [
            'data' => [
                'type' => 'experiences',
                'id' => (string)$experienceId,
                'attributes' => $stub
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/experiences/' . $id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);

        $this->experienceVersions[0]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[0]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->experienceVersions[1]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[1]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->experienceVersions[2]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => true]));
        $this->experienceVersions[2]->experienceVersionPhotos()->save(new \App\Models\ExperiencePhoto(['is_cover' => false]));

        $this->stub = $this->getExperienceVersionStub(true);
        $this->requestBody = $this->getRequestBody($this->experience_1->id, $this->stub);
    }

    protected function setStatus(int ...$status){
        for ($i = 0; $i < count($this->experienceVersions); $i++){
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\ExperienceVersion $experienceVersion){
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['departure_port' => $experienceVersion->departure_port]);
    }

    protected function assertNotEdited(\App\Models\ExperienceVersion $experienceVersion){
        $this->dontSeeJson(['departure_port' => $experienceVersion->departure_port]);
    }

    protected function getExperienceVersionById(int $id){
        return \App\Models\ExperienceVersion::find($id);
    }
}