<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencesPricesGetTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $experience_1;
    public $period;
    public $price;
    public $id;
    public $experienceVersions;
    public $requestBody;
    public $stub;

    // ANYONE
    /** @test */
    public function it_returns_422_if_currency_is_set_but_not_valid()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $periodId = \App\Models\Period::all()->last()->id;

        //act
        $this->getJson($this->getRoute($periodId, 'not_a_currency'));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency']);
    }

    /** @test */
    public function it_returns_404_periods_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $periodId = \App\Models\Period::all()->last()->id + 1;

        //act
        $this->getJson($this->getRoute($periodId));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'period_not_found']);
    }

    /** @test */
    public function it_returns_200_and_list_of_prices_if_period_exist_and_have_prices()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->getJson($this->getRoute($this->period->id));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $prices = $this->period->prices()->get();
        foreach ($prices as $price) {
            $this->seeJson(['id' => (string) $price->id]);
            $this->seeJson(['person' => $price->person]);
            $this->seeJson(['price' => $price->price]);
            $this->seeJson(['currency' => $price->currency]);
        }
    }

    /** @test */
    public function it_returns_200_and_list_of_prices_if_period_exist_and_have_prices_with_a_specific_currency()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $currency = \App\Models\Currency::all()->random()->name;

        //act
        $this->getJson($this->getRoute($this->period->id, $currency));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
        $prices = $this->period->prices()->get();
        foreach ($prices as $price) {
            $this->seeJson(['id' => (string) $price->id]);
            $this->seeJson(['person' => $price->person]);
            $this->seeJson(['price' => \App\Services\PriceHelper::convertPrice($price->currency, $currency, $price->price)]);
            $this->seeJson(['currency' => $currency]);
        }
    }

    /** @test */
    public function it_returns_200_and_empty_list_of_prices_if_period_exist_but_have_not_prices()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experience_1->periods()->save(new \App\Models\Period([
            "date_start" => '2015-08-20',
            "date_end" => '2015-09-20',
            "entire_boat" => $this->fake->boolean,
            "price" => $this->fake->numberBetween(0, 1000),
            "min_person" => $this->fake->numberBetween(1, 20),
            "is_default" => false,
            "currency" => \App\Models\Currency::all()->random()->name
        ]));
        $this->period = $this->experience_1->periods()
            ->get()->last();

        //act
        $this->getJson($this->getRoute($this->period->id));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $responseContent = json_decode($this->response->getContent());
        $this->assertEquals($responseContent->data, []);
    }

    //helpers
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    public function getStub()
    {
        return [
            'price' => $this->fake->numberBetween(0, 10000),
            'person' => 80,
            "currency" => \App\Models\Currency::all()->random()->name
        ];
    }

    public function getRequestBody(int $priceId)
    {
        return ["data" =>
            [
                "type" => "prices",
                "id" => (string) $priceId,
                "attributes" => [
                    "price" => $this->fake->numberBetween(0, 10000)
                ]
            ]
        ];
    }

    public function getRoute(int $periodId, $currency = null)
    {
        return 'v1/periods/' . $periodId . '/prices' . (isset($currency) ? '?currency=' . $currency : '');
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experience_1->periods()->save(new \App\Models\Period([
            "date_start" => '2015-08-20',
            "date_end" => '2015-09-20',
            "entire_boat" => $this->fake->boolean,
            "price" => $this->fake->numberBetween(0, 1000),
            "min_person" => $this->fake->numberBetween(1, 20),
            "is_default" => false,
            "currency" => \App\Models\Currency::all()->random()->name
        ]));
        $this->period = $this->experience_1->periods()
            ->get()->first();
        $this->price = new \App\Models\Price($this->getStub());
        $this->period->prices()->save($this->price);

        $this->requestBody = $this->getRequestBody($this->price->id);
    }

    protected function setStatus(int ...$status)
    {
        for ($i = 0; $i < count($this->experienceVersions); $i++) {
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\Period $period)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $price = $period->prices()->get()->last();

        $hasNotChangedPerson = $price->person == $this->price->person;
        $hasNotChangedPrice = $price->price == $this->price->price;
        $this->assertFalse($hasNotChangedPerson && $hasNotChangedPrice);

        $this->seeJson(["id" => (string) $price->id]);
        $this->seeJson(["currency" => (string) $price->currency]);

    }

    protected function assertNotEdited(\App\Models\Period $period)
    {
        $price = \App\Models\Price::find($this->price->id);

        $hasNotChangedPerson = $price->person == $this->price->person;
        $hasNotChangedPrice = $price->price == $this->price->price;
        $this->assertTrue($hasNotChangedPerson && $hasNotChangedPrice);
    }
}