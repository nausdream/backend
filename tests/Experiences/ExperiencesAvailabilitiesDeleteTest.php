<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencesAvailabilitiesDeleteTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $experience_1;
    public $availability;
    public $id;
    public $experienceVersions;
    public $requestBody;
    public $stub;

    // ADMIN
    /** @test */
    public function it_returns_200_and_delete_availability_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->experience_1);

    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experienceVersions[2]->departure_lat = -5001;
        $this->experienceVersions[2]->save();

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->admin->authorizations()->detach();

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);
    }

    /** @test */
    public function it_returns_404_if_there_are_no_finished_experience_versions()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experienceVersions[0]->is_finished = false;
        $this->experienceVersions[0]->save();
        $this->experienceVersions[1]->is_finished = false;
        $this->experienceVersions[1]->save();
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->experience_1);
    }




    // USER
    /** @test */
    public function it_returns_200_and_delete_availability()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = true;
        $this->experienceVersions[2]->save();

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_1));

        //assert
        $this->assertEdited($this->experience_1);
    }

    /** @test */
    public function it_returns_403_if_last_finished_experience_version_is_not_accepted_conditionally()
    {
        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);

        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->deleteJson($this->getRoute($this->availability->id), [], $this->getHeaders($this->user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->experience_1);
    }



    // ANYONE
    /** @test */
    public function it_returns_404_if_availability_doesnt_exist()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $fakeAvailabilityId = !is_null(\App\Models\ExperienceAvailability::all()->last()) ? \App\Models\ExperienceAvailability::all()->last()->id + 1 : 1;
        $this->requestBody['data']['id'] = $fakeAvailabilityId;

        //act
        $this->deleteJson($this->getRoute($fakeAvailabilityId),$this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->experience_1);
    }




    //helpers
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    public function getStub()
    {
        $stub = [];
        $stub[] = [
            'id' => 1,
            'price' => $this->fake->numberBetween(0, 100),
            'per_person' => $this->fake->boolean()
        ];
        $stub[] = [
            'id' => 3,
            'price' => $this->fake->numberBetween(0, 100),
            'per_person' => $this->fake->boolean()
        ];
        $stub[] = [
            'id' => 4,
            'price' => $this->fake->numberBetween(0, 100),
            'per_person' => $this->fake->boolean()
        ];


        return $stub;
    }

    public function getRoute(int $id)
    {
        return 'v1/experience-availabilities/'.$id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experience_1->experienceAvailabilities()->save(new \App\Models\ExperienceAvailability(["date_start" => date('Y-m-d'), "date_end" => date('Y-m-d', strtotime('+2 months'))]));
        $this->availability = $this->experience_1->experienceAvailabilities()
            ->get()->first();
    }

    protected function setStatus(int ...$status)
    {
        for ($i = 0; $i < count($this->experienceVersions); $i++) {
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\Experience $experience)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->assertCount(0, \App\Models\ExperienceAvailability::where('experience_id', $experience->id)->get());
    }

    protected function assertNotEdited(\App\Models\Experience $experience)
    {
        $this->assertCount(1, \App\Models\ExperienceAvailability::where('experience_id', $experience->id)->get());
    }
}