<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencesPricesPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $experience_1;
    public $id;
    public $experienceVersions;
    public $requestBody;
    public $stub;
    public $period;

    // ANYONE
    /** @test */
    public function it_returns_409_if_object_type_in_request_body_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['type']);
        $this->requestBody['data']['type'] = 'wrong type';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_422_if_price_with_same_person_already_exists()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->period->prices()->save(new \App\Models\Price(['person' => 2, 'price' => 53, "currency" => \App\Models\Currency::all()->random()->name]));
        $this->requestBody['data']['attributes']['person'] = 2;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount(1, $this->period->prices()->get());
        $this->assertEquals(53, $this->period->prices()->get()->first()->price);

    }

    /** @test */
    public function it_returns_422_if_period_type_or_id_not_present()
    {
        // TYPE
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['relationships']['period']['data']['type']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //ID
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        unset($this->requestBody['data']['relationships']['period']['data']['id']);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_last_experience_version()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $experience = $this->getExperience($this->boat);
        $this->requestBody['data']['relationships']['period']['data']['id'] = $experience->id;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_404_if_there_is_no_experience()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $experiences = \App\Models\Experience::all();
        $experienceId = $experiences->count() > 0 ? $experiences->last()->id + 1 : 1;
        $this->requestBody['data']['relationships']['period']['data']['id'] = $experienceId;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_validates_fields()
    {
        // PRICE
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->requestBody['data']['attributes']['price'] = 'not a number';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->requestBody['data']['attributes']['price'] = -10;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->requestBody['data']['attributes']['price'] = 5.5;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);





        // PERSON
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['person'] = 'not a number';

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['person'] = -10;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['person'] = 5.5;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);

        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->requestBody['data']['attributes']['person'] = 0;

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->period);
    }



    // ADMIN
    /** @test */
    public function it_returns_201_and_edit_experience_if_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertEdited($this->period);

    }

    /** @test */
    public function it_returns_403_if_last_finished_is_rejected_or_accepted_conditionally()
    {
        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);

        //arrange ACCEPTED_CONDITIONALLY
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_403_if_last_finished_is_not_in_his_area()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experienceVersions[2]->departure_lat = -5001;
        $this->experienceVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_201_it_is_an_admin()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);
        $this->experienceVersions[0]->is_finished = false;
        $this->experienceVersions[0]->save();
        $this->experienceVersions[1]->is_finished = false;
        $this->experienceVersions[1]->save();
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertEdited($this->period);
    }




    // USER
    /** @test */
    public function it_returns_201_and_edit_experience()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertEdited($this->period);
    }

    /** @test */
    public function it_returns_403_if_last_finished_experience_version_is_not_accepted_conditionally()
    {

        //arrange PENDING
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_PENDING);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);


        //arrange ACCEPTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);


        //arrange REJECTED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_REJECTED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);

        //arrange FREEZED
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_FREEZED);

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_1));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);
    }

    /** @test */
    public function it_returns_403_if_user_is_not_boat_owner()
    {
        //arrange
        $this->setThingsUp();
        $this->setStatus(Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED, Constant::STATUS_ACCEPTED_CONDITIONALLY);
        $this->experienceVersions[2]->is_finished = false;
        $this->experienceVersions[2]->save();

        //act
        $this->postJson($this->getRoute(), $this->requestBody, $this->getHeaders($this->user_2));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertNotEdited($this->period);
    }


    //helpers
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    public function getRequestBody(int $periodId = null)
    {
        $body = [];
        $body['data'] = [
            "type" => "prices",
            "attributes" => [
                "person" => $this->fake->numberBetween(1, 50),
                "price" => $this->fake->numberBetween(0, 10000)
            ],
            "relationships" => [
                "period" => [
                    "data" => [
                        "type" => "periods",
                        "id" => $periodId ?? $this->period->id
                    ]
                ]
            ]
        ];
        return $body;
    }

    public function getRoute()
    {
        return 'v1/prices/';
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->experience_1 = $this->getExperience($this->boat);
        $this->id = $this->experience_1->id;
        $this->experienceVersions = [];
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experienceVersions[] = $this->getExperienceVersion($this->experience_1);
        $this->experience_1->periods()->save(new \App\Models\Period([
            "date_start" => '2015-08-20',
            "date_end" => '2015-09-20',
            "entire_boat" => $this->fake->boolean,
            "price" => $this->fake->numberBetween(0, 1000),
            "min_person" => $this->fake->numberBetween(1, 20),
            "is_default" => false,
            "currency" => \App\Models\Currency::all()->random()->name
        ]));
        $this->period = $this->experience_1->periods()
            ->get()->first();
        $this->requestBody = $this->getRequestBody($this->period->id);
    }

    protected function setStatus(int ...$status)
    {
        for ($i = 0; $i < count($this->experienceVersions); $i++) {
            $this->experienceVersions[$i]->status = $status[$i];
            $this->experienceVersions[$i]->save();
        }
    }

    protected function assertEdited(\App\Models\Period $period)
    {
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->assertCount(1, $period->prices()->get());
        $price = $this->period->prices()->get()->last();

        $this->seeJson(["id" => (string) $price->id]);
        $this->seeJson(["currency" => (string) $price->currency]);
    }

    protected function assertNotEdited(\App\Models\Period $period)
    {
        $this->assertCount(0, $period->prices()->get());
    }
}