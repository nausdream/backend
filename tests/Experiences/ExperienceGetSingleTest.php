<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class ExperienceGetSingleTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;

    /**
     * ExperienceGetSingleTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/experiences/';
        $this->type = 'experiences';
    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);
        $token = JwtService::getTokenStringFromAccount($user); // Retrieves the generated token

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();


        //act
        $this->getJson($this->route . $experience->id . '?editing=true', ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_captain_it_returns_200_and_last_experience_version()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_2->departure_port]);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_200_and_last_experience_version_in_admin_area()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_2->departure_port]);
        $this->dontSeeJson(['arrival_port' => $experienceVersion_1->arrival_port]);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_403_if_admin_has_no_experiences_permission_or_last_is_not_in_area()
    {
        // ADMIN NO PERMISSION
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->departure_lat = 5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        // ADMIN NOT IN AREA
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);

        $experienceVersion_1->departure_lat = 5;
        $experienceVersion_1->departure_lng = 5;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        $experienceVersion_2->departure_lat = -5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_user_it_returns_403()
    {
        // ADMIN NO PERMISSION
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders(new \App\Models\User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function if_invalid_currency_return_422()
    {
        // ADMIN NO PERMISSION
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        //act
        $this->getJson($this->route . $experience->id . '?currency=' . 'not_a_currency');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency']);

    }

    /**
     * ?editing=false
     * @test
     */
    public function it_returns_200_and_last_ACCEPTED_experience_version_for_any_user()
    {
        //CAPTAIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);

        //ADMIN NO PERMISSIONS
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);

        //ADMIN NO AREA
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;

        $experienceVersion_1->departure_lat = -5;
        $experienceVersion_1->departure_lng = 5;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->departure_lat = 5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);

        //SUPER ADMIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);

        //USER
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);

        //VISITOR
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);
    }

    /**
     * ?language=it|en
     * ?editing=false
     * @test
     */
    public function it_fetches_correct_language_data_for_visitors()
    {
        //VISITOR
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '?language=en');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);

        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);




        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '?language=it');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $this->seeJson(['departure_port' => $experienceVersion_1->departure_port]);
        $this->dontSeeJson(['departure_port' => $experienceVersion_2->arrival_port]);
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_doesnt_exist()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experiences = \App\Models\Experience::all();
        $experienceId = $experiences->count()>0 ? $experiences->last()->id + 1 : 1;

        //act
        $this->getJson($this->route . $experienceId, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_404_if_correct_experience_version_doesnt_exist()
    {
        // CAPTAIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);


        // ADMIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        //act
        $this->getJson($this->route . $experience->id, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);


        // EVERYONE WITH EDITING=FALSE
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);

        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_1->save();

        //act
        $this->getJson($this->route . $experience->id);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_includes_default_relationships_and_included()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        // Fixed Rules
        $fixed_rule_1 = new \App\Models\Rule(['name' => $this->fake->text(Constant::RULE_LENGTH)]);
        $fixed_rule_1->save();
        $experienceVersion_2->rules()->attach($fixed_rule_1->id);

        // Additional Services
        $additional_service_1 = new \App\Models\AdditionalService(
            [
                'name' => str_random(Constant::ADDITIONAL_SERVICE_LENGTH),
                'price' => $this->fake->numberBetween(0, 1000),
                'per_person' => $this->fake->boolean,
                'currency' => \App\Models\Currency::all()->random()->name
            ]
        );
        $experienceVersion_2->additionalServices()->save($additional_service_1);

        // Fixed Additional Services
        $fixed_additional_service = new \App\Models\FixedAdditionalService(['name' => $this->fake->text(Constant::ADDITIONAL_SERVICE_LENGTH)]);
        $fixed_additional_service->save();
        $experienceVersion_2->fixedAdditionalServices()->attach(
            $fixed_additional_service->id,
            [
                'price' => $this->fake->randomFloat(2, 0, 50),
                'per_person' => $this->fake->boolean,
                'currency' => \App\Models\Currency::all()->random()->name
            ]
        );
        $fixed_additional_service_price = $fixed_additional_service->experienceVersionsFixedAdditionalServices()->get()->first();

        // Photos
        $photo = new \App\Models\ExperiencePhoto(['is_cover' => false]);
        $experienceVersion_2->experienceVersionPhotos()->save($photo);

        // Availabilities
        $availability_1 = new \App\Models\ExperienceAvailability(
            [
                'date_start' => $this->fake->date(),
                'date_end' => $this->fake->date()
            ]);
        $availability_1->experience()->associate($experience);
        $availability_1->save();

        // Feedbacks
        $feedback_1 = new \App\Models\ExperienceFeedback(['score' => $this->fake->numberBetween(0, 5)]);
        $feedback_1->experience()->associate($experience);
        $feedback_1->user()->associate($user);
        $feedback_1->save();

        // Periods
        $period_1 = new \App\Models\Period([
            'date_start' => $this->fake->date(),
            'date_end' => $this->fake->date(),
            'price' => $this->fake->numberBetween(1, 100),
            'min_person' => $this->fake->numberBetween(1, 10),
            'entire_boat' => $this->fake->boolean(),
            'is_default' => $this->fake->boolean(),
            'currency' => \App\Models\Currency::inRandomOrder()->first()->name
        ]);
        $period_1->experience()->associate($experience);
        $period_1->save();

        //act
        $this->getJson($this->route . $experience->id . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["type" => $this->type]);
        $this->seeJson(["departure_port" => $experienceVersion_2->departure_port]);
        $this->seeJson(
            ["rules" => ["data" => [["type" => "rules", "id" => (string) $fixed_rule_1->id]]]]
        );
        $this->seeJson(
            ["additional_services" => ["data" => [["type" => "additional-services", "id" => (string) $additional_service_1->id]]]]
        );
        $this->seeJson(
            ["fixed_additional_service_prices" => ["data" => [["type" => "fixed-additional-service-prices", "id" => (string) $fixed_additional_service_price->id]]]]
        );
        $this->seeJson(
            ["photos" => ["data" => [["type" => "photos", "id" => (string) $photo->id]]]]
        );
        $this->seeJson(
            ["experience_availabilities" => ["data" => [["type" => "experience-availabilities", "id" => (string) $availability_1->id]]]]
        );
        $this->seeJson(
            ["periods" => ["data" => [["type" => "periods", "id" => (string) $period_1->id]]]]
        );
        $this->dontSeeJson(
            ["feedbacks" => ["data" => [["type" => "feedbacks", "id" => (string) $feedback_1->id]]]]
        );
        $this->seeJson([
                'name' => $additional_service_1->name
            ]
        );
        $this->seeJson([
                'price' => $additional_service_1->price
            ]
        );
        $this->seeJson([
                'is_cover' => $photo->is_cover
            ]
        );
        $this->seeJson(
            [
                'date_start' => $availability_1->date_start
            ]
        );
    }

    /**
     * @test
     */
    public function it_includes_default_relationships_and_included_with_changed_currency()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        // Fixed Rules
        $fixed_rule_1 = new \App\Models\Rule(['name' => $this->fake->text(Constant::RULE_LENGTH)]);
        $fixed_rule_1->save();
        $experienceVersion_2->rules()->attach($fixed_rule_1->id);

        // Additional Services
        $additional_service_1 = new \App\Models\AdditionalService(
            [
                'name' => str_random(Constant::ADDITIONAL_SERVICE_LENGTH),
                'price' => $this->fake->numberBetween(0, 1000),
                'per_person' => $this->fake->boolean,
                'currency' => \App\Models\Currency::all()->random()->name
            ]
        );
        $experienceVersion_2->additionalServices()->save($additional_service_1);

        // Fixed Additional Services
        $fixed_additional_service = new \App\Models\FixedAdditionalService(['name' => $this->fake->text(Constant::ADDITIONAL_SERVICE_LENGTH)]);
        $fixed_additional_service->save();
        $experienceVersion_2->fixedAdditionalServices()->attach(
            $fixed_additional_service->id,
            [
                'price' => $this->fake->randomFloat(2, 0, 50),
                'per_person' => $this->fake->boolean,
                'currency' => \App\Models\Currency::all()->random()->name
            ]
        );
        $fixed_additional_service_price = $fixed_additional_service->experienceVersionsFixedAdditionalServices()->get()->first();

        // Photos
        $photo = new \App\Models\ExperiencePhoto(['is_cover' => false]);
        $experienceVersion_2->experienceVersionPhotos()->save($photo);

        // Availabilities
        $availability_1 = new \App\Models\ExperienceAvailability(
            [
                'date_start' => $this->fake->date(),
                'date_end' => $this->fake->date()
            ]);
        $availability_1->experience()->associate($experience);
        $availability_1->save();

        // Feedbacks
        $feedback_1 = new \App\Models\ExperienceFeedback(['score' => $this->fake->numberBetween(0, 5)]);
        $feedback_1->experience()->associate($experience);
        $feedback_1->user()->associate($user);
        $feedback_1->save();

        // Periods
        $period_1 = new \App\Models\Period([
            'date_start' => $this->fake->date(),
            'date_end' => $this->fake->date(),
            'price' => $this->fake->numberBetween(1, 100),
            'min_person' => $this->fake->numberBetween(1, 10),
            'entire_boat' => $this->fake->boolean(),
            'is_default' => $this->fake->boolean(),
            'currency' => \App\Models\Currency::inRandomOrder()->first()->name
        ]);
        $period_1->experience()->associate($experience);
        $period_1->save();

        $currency = \App\Models\Currency::all()->random()->name;


        //act
        $this->getJson($this->route . $experience->id . '?editing=true&currency=' . $currency, $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["type" => $this->type]);
        $this->seeJson(["departure_port" => $experienceVersion_2->departure_port]);
        $this->seeJson(
            ["rules" => ["data" => [["type" => "rules", "id" => (string) $fixed_rule_1->id]]]]
        );
        $this->seeJson(
            ["additional_services" => ["data" => [["type" => "additional-services", "id" => (string) $additional_service_1->id]]]]
        );
        $this->seeJson(
            ["fixed_additional_service_prices" => ["data" => [["type" => "fixed-additional-service-prices", "id" => (string) $fixed_additional_service_price->id]]]]
        );
        $this->seeJson(
            ["photos" => ["data" => [["type" => "photos", "id" => (string) $photo->id]]]]
        );
        $this->seeJson(
            ["experience_availabilities" => ["data" => [["type" => "experience-availabilities", "id" => (string) $availability_1->id]]]]
        );
        $this->seeJson(
            ["periods" => ["data" => [["type" => "periods", "id" => (string) $period_1->id]]]]
        );
        $this->dontSeeJson(
            ["feedbacks" => ["data" => [["type" => "feedbacks", "id" => (string) $feedback_1->id]]]]
        );
        $this->seeJson([
                'name' => $additional_service_1->name
            ]
        );
        $this->seeJson([
                'price' => ceil(\App\Services\PriceHelper::convertPrice($additional_service_1->currency, $currency, $additional_service_1->price))
            ]
        );
        $this->seeJson([
                'is_cover' => $photo->is_cover
            ]
        );
        $this->seeJson(
            [
                'date_start' => $availability_1->date_start
            ]
        );
    }

    /**
     * @test
     */
    public function it_includes_requested_relationships_and_included()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        // Feedbacks
        $feedback_1 = new \App\Models\ExperienceFeedback(['score' => $this->fake->numberBetween(0, 5)]);
        $feedback_1->experience()->associate($experience);
        $feedback_1->user()->associate($user);
        $feedback_1->save();

        //act
        $this->getJson($this->route . $experience->id . '?editing=true' .'&include=feedbacks', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(["type" => $this->type]);
        $this->seeJson(["departure_port" => $experienceVersion_2->departure_port]);
        $this->seeJson(
            ["feedbacks" => ["data" => [["type" => "feedbacks", "id" => (string) $feedback_1->id]]]]
        );
    }

    private function getExperience(Boat $boat){
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)){
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished){
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true){
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)){
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null){
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account){
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin){
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }
}