<?php

/**
 * User: Luca Puddu
 * Date: 31/10/2016
 * Time: 10:37
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencePostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route;

    /**
     * ExperiencePostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/experiences';
    }

    /** @test */
    public function it_returns_201_and_creates_a_new_experience_and_experienceversion()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id), $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();
        $new_experience = $new_experiences->last();
        $new_experienceVersion = $new_experienceVersions->last();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(["id" => (string) $new_experience->id]);
        $this->seeJson(["type" => 'experiences']);
        $this->assertCount($experiences->count() + 1, $new_experiences);
        $this->assertCount($experienceVersions->count() + 1, $new_experienceVersions);
        $this->assertEquals($new_experience->id, $new_experienceVersion->experience_id);
        $this->assertEquals(false, $new_experienceVersion->is_finished);
        $this->assertEquals(null, $new_experienceVersion->status);
    }

    /** @test */
    public function it_returns_404_if_boat_doesnt_exist()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id + 1), $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_201_and_creates_a_new_experience_and_experienceversion_if_user_is_admin()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $admin = $this->getSuperAdmin();

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id), $this->getHeaders($admin));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();
        $new_experience = $new_experiences->last();
        $new_experienceVersion = $new_experienceVersions->last();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(["id" => (string) $new_experience->id]);
        $this->seeJson(["type" => 'experiences']);
        $this->assertCount($experiences->count() + 1, $new_experiences);
        $this->assertCount($experienceVersions->count() + 1, $new_experienceVersions);
        $this->assertEquals($new_experience->id, $new_experienceVersion->experience_id);
        $this->assertEquals(false, $new_experienceVersion->is_finished);
        $this->assertEquals(null, $new_experienceVersion->status);
    }

    /** @test */
    public function it_returns_403_if_admin_has_no_experiences_permission()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id), $this->getHeaders($admin));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_403_if_user_not_boat_owner()
    {
        // OTHER USER
        //arrange
        $user = $this->getUser();
        $user_2 = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id), $this->getHeaders($user_2));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);


        // VISITOR
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        //act
        $this->postJson($this->route, $this->getRequestBody($boat->id));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_422_if_relationship_object_name_is_not_boat()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        $requestBody = $this->getRequestBody($boat->id);
        $boatData = $requestBody['data']['relationships']['boat'];
        unset($requestBody['data']['relationships']['boat']);
        $requestBody['data']['relationships']['wrong_type'] = $boatData;

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_422_if_relationship_object_type_is_not_boats()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        $requestBody = $this->getRequestBody($boat->id);
        $requestBody['data']['relationships']['boat']['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_422_if_relationship_object_id_is_not_int()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        $requestBody = $this->getRequestBody($boat->id);
        $requestBody['data']['relationships']['boat']['data']['id'] = 'Lorem ipsum';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    /** @test */
    public function it_returns_409_if_object_type_is_not_experiences()
    {
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);

        $experiences = Experience::all();
        $experienceVersions = ExperienceVersion::all();

        $requestBody = $this->getRequestBody($boat->id);
        $requestBody['data']['type'] = 'wrong_type';

        //act
        $this->postJson($this->route, $requestBody, $this->getHeaders($user));

        //arrange
        $new_experiences = Experience::all();
        $new_experienceVersions = ExperienceVersion::all();

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
        $this->assertCount($experiences->count(), $new_experiences);
        $this->assertCount($experienceVersions->count(), $new_experienceVersions);
    }

    // HELPERS
    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'title' => str_random(Constant::EXPERIENCE_TITLE_LENGTH),
            'description' => $this->fake->text(Constant::EXPERIENCE_DESCRIPTION_LENGTH),
            'rules' => $this->fake->text(Constant::EXPERIENCE_RULES_LENGTH),
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->numberBetween(1, 100),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
           'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(\App\Models\ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'indication' => $this->fake->text(),
            'rules' => $this->fake->text(),
            'description' => $this->fake->text(),
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $boatId = null){
        $request = ['data' => ['type' => 'experiences']];
        if (isset($boatId)){
            $request['data']['relationships']['boat']['data'] = ['type' => 'boats', 'id' => (string) $boatId];
        }

        return $request;
    }
}