<?php

/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 28/10/2016
 * Time: 17:11
 */
class TestingMailEventListener implements Swift_Events_EventListener
{
    protected $test;

    /**
     * TestingMailEventListener constructor.
     * @param $test
     */
    public function __construct($test)
    {
        $this->test = $test;
    }


    /**
     * Add email to test
     *
     * @param $event
     */
    public function beforeSendPerformed($event)
    {
        $this->test->addEmail($event->getMessage());
    }
}