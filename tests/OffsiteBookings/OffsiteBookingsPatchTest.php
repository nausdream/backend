<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class OffsiteBookingsPatchTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route = 'v1/offsite-bookings/';
    private $offsiteBooking;
    private $user;
    private $admin;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_of_resource_is_not_correct_or_id_in_the_body_does_not_match_the_route_id()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['type'] = 'not_offsite_booking';

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

        //arrange
        $stub = $this->getStub();
        $stub['data']['id'] = $this->offsiteBooking->id + 1;

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_422_if_one_of_the_attributes_of_the_request_is_not_valid()
    {
        //status
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['status'] = '';

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'status']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['status'] = null;

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'status']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['status']);

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'status']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['status'] = 'pending';

        //act
        $this->patchJson($this->getRoute(), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'in']);
        $this->seeJson(['title' => 'status']);
    }

    /**
     * @test
     */
    public function it_returns_404_if_offsite_booking_does_not_exist()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['id'] = \App\Models\OffsiteBooking::all()->last()->id + 1;

        //act
        $this->patchJson($this->getRoute($stub['data']['id']), $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
        $this->seeJson(['code' => 'offsite_booking_not_found']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_offsite_booking_is_already_paid()
    {
        //arrange
        $this->setThingsUp();
        $this->offsiteBooking->status = \App\Constant::OFFSITE_BOOKING_STATUS_PAID;
        $this->offsiteBooking->save();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'offsite_booking_not_pending']);
    }

    /**
     * @test
     */
    public function it_returns_200_if_request_is_ok_and_it_is_from_admin_with_bookings_authorization()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->patchJson($this->getRoute(), $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => 'offsite-bookings']);
        $this->seeEmailsSent(3);
        $this->seeEmailTo($this->offsiteBooking->user->email);
        $this->seeEmailTo($this->offsiteBooking->email_captain);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $this->assertEquals(\App\Constant::OFFSITE_BOOKING_STATUS_NAMES[\App\Constant::OFFSITE_BOOKING_STATUS_PAID], $responseContent->data->attributes->status);
        $offsiteBookingNew = \App\Models\OffsiteBooking::find($this->offsiteBooking->id);
        $this->assertNotEquals($this->offsiteBooking->status, $offsiteBookingNew->status);
        $this->assertEquals(\App\Constant::OFFSITE_BOOKING_STATUS_PAID, $offsiteBookingNew->status);
    }

    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $this->user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->offsiteBooking = factory(\App\Models\OffsiteBooking::class)
            ->make();
        $this->offsiteBooking->user_id = $this->user->id;
        $this->offsiteBooking->save();
    }

    protected function getStub()
    {
        return [
            'data'=> [
                'id' => $this->offsiteBooking->id,
                'type'=> 'offsite-bookings',
                'attributes'=> [
                    'status' => \App\Constant::OFFSITE_BOOKING_STATUS_NAMES[\App\Constant::OFFSITE_BOOKING_STATUS_PAID]
                ]
	        ]
        ];
    }

    protected function getRoute($id = null)
    {
        return $this->route . (isset($id) ? $id : $this->offsiteBooking->id);
    }

}