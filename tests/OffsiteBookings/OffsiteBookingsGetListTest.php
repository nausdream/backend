<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class OffsiteBookingsGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route = 'v1/offsite-bookings';
    private $admin;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route, []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->getJson($this->route, ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->getJson($this->route, ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->getJson($this->route, $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_all_offsite_bookings()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->response->getContent();
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
        $responseContent = json_decode($this->response->getContent());
        $this->assertNotEquals($responseContent->data, []);
    }

    /**
     * @test
     */
    public function it_returns_200_and_empty_list_if_there_are_no_conversations()
    {
        //arrange
        $this->setThingsUp();
        $offsiteBookings = \App\Models\OffsiteBooking::all();
        foreach ($offsiteBookings as $offsiteBooking) {
            $offsiteBooking->delete();
        }

        //act
        $this->getJson($this->route, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['current_page' => 1]);
        $responseContent = json_decode($this->response->getContent());
        $this->assertEquals($responseContent->data, []);
    }


    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $users = factory(\App\Models\User::class, 5)->states('dummy')->create();
        $offsiteBookings = factory(\App\Models\OffsiteBooking::class, 12)
            ->make()
            ->each(function ($e) use ($users) {
                $e->user()->associate($users->random());
                $e->save();
            });
    }
}