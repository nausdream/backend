<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class OffsiteBookingsPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route = 'v1/offsite-bookings';
    private $offsiteBooking;
    private $user;
    private $admin;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getStub(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_of_resource_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['type'] = 'not_offsite_booking';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_422_if_one_of_the_attributes_of_the_request_is_not_valid()
    {
        //first_name_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['first_name_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_guest'] = str_random(\App\Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'first_name_guest']);

        //last_name_guest
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['last_name_guest'] = str_random(\App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'last_name_guest']);

        //address_guest
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['address_guest'] = str_random(\App\Constant::ENTERPRISE_ADDRESS_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'address_guest']);

        //city_guest
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['city_guest'] = str_random(\App\Constant::ENTERPRISE_CITY_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'city_guest']);

        //phone_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['phone_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_guest'] = str_random(\App\Constant::PHONE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'phone_guest']);

        //email_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['email_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['email_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_guest'] = $this->fake->email . str_random(\App\Constant::EMAIL_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'email_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_guest'] = 'not_an_email';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email']);
        $this->seeJson(['title' => 'email_guest']);

        //vat_guest
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['vat_guest'] = str_random(\App\Constant::ENTERPRISE_VAT_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'vat_guest']);

        //experience_date
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'experience_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = \Carbon\Carbon::yesterday()->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'date_greater_or_equal_today']);
        $this->seeJson(['title' => 'experience_date']);

        //arrival_date
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'arrival_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = \Carbon\Carbon::today()->addDays(9)->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'arrival_date_greater_or_equal_to_departure_date']);
        $this->seeJson(['title' => 'arrival_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_date']);

        //experience_title
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_title']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = str_random(\App\Constant::EXPERIENCE_TITLE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'experience_title']);

        //experience_description
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_description'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_description']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_description'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_description']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_description']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_description']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_description'] = str_random(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'experience_description']);

        //boat_type
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['boat_type'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'boat_type']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['boat_type'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'boat_type']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['boat_type']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'boat_type']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['boat_type'] = 'not_a_boat_type';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'boat_type']);

        //departure_port
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_port']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = str_random(\App\Constant::PORT_DEPARTURE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'departure_port']);

        //arrival_port
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['arrival_port']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = str_random(\App\Constant::PORT_DEPARTURE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'arrival_port']);

        //departure_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'departure_time']);

        //arrival_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['arrival_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'arrival_time']);

        //adults
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['adults']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'adults']);

        //kids
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['kids']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'kids']);

        //babies
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['babies']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'babies']);

        //included_services
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['included_services'] = str_random(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'included_services']);

        //excluded_services
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['excluded_services'] = str_random(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'excluded_services']);

        //drinks
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['drinks'] = str_random(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'drinks']);

        //dishes
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['dishes'] = str_random(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'dishes']);

        //price
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['price']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = 9;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'price']);

        //fee
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['fee']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = 0;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = $stub['data']['attributes']['price'] + 1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'fee']);

        //expiration_date
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['expiration_date'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'expiration_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['expiration_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'expiration_date']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['expiration_date']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'expiration_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['expiration_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'expiration_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['expiration_date'] = \Carbon\Carbon::createFromFormat('Y-m-d', $stub['data']['attributes']['experience_date'])->addDays(1)->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'expiration_date_greater_than_experience_date']);
        $this->seeJson(['title' => 'expiration_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = null;
        $stub['data']['attributes']['expiration_date'] = \Carbon\Carbon::yesterday()->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'date_greater_or_equal_today']);
        $this->seeJson(['title' => 'expiration_date']);

        //language_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['language_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = 'not_a_language';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'language_guest']);

        //currency_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['currency_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_guest'] = 'not_a_currency';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency_guest']);

        //language_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['language_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['language_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_captain'] = 'not_a_language';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'language_captain']);

        //currency_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['currency_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = 'not_a_currency';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency_captain']);

        //first_name_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['first_name_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name_captain'] = str_random(\App\Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'first_name_captain']);

        //last_name_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['last_name_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'last_name_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['last_name_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'last_name_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['last_name_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'last_name_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['last_name_captain'] = str_random(\App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'last_name_captain']);

        //email_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['email_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['email_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'email_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_captain'] = $this->fake->email . str_random(\App\Constant::EMAIL_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'email_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['email_captain'] = 'not_an_email';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email']);
        $this->seeJson(['title' => 'email_captain']);


        //phone_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['phone_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'phone_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['phone_captain'] = str_random(\App\Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'phone_captain']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_422_if_guest_email_and_phone_are_of_a_different_user()
    {
        //arrange
        $this->setThingsUp();
        $userEmail = factory(\App\Models\User::class)->states('dummy')->create();
        $userPhone = factory(\App\Models\User::class)->states('dummy')->create();
        $stub = $this->getStub();
        $stub['data']['attributes']['email_guest'] = $userEmail->email;
        $stub['data']['attributes']['phone_guest'] = $userPhone->phone;
        $userEmail->is_mail_activated = 1;
        $userEmail->save();
        $userPhone->is_phone_activated = 1;
        $userPhone->save();

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'email_and_phone_of_different_user']);
        $this->seeJson(['title' => 'email']);
    }

    /**
     * @test
     */
    public function it_returns_201_if_request_is_ok_and_it_is_from_admin_with_bookings_authorization()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeJson(['type' => 'offsite-bookings']);
        $this->seeEmailsSent(1);
        $this->seeEmailTo($stub['data']['attributes']['email_guest']);
        $this->seeEmailFrom("noreply@nausdream.com");
        $responseContent = json_decode($this->response->getContent());
        $this->assertEquals($responseContent->data->attributes->status, \App\Constant::OFFSITE_BOOKING_STATUS_NAMES[\App\Constant::OFFSITE_BOOKING_STATUS_PENDING]);

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = null;
        $stub['data']['attributes']['arrival_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(2);
        $this->seeEmailTo($stub['data']['attributes']['email_guest']);


        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_date'] = '';
        $stub['data']['attributes']['arrival_date'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(3);
        $this->seeEmailTo($stub['data']['attributes']['email_guest']);

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_date']);
        unset($stub['data']['attributes']['arrival_date']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
        $this->seeEmailsSent(4);
        $this->seeEmailTo($stub['data']['attributes']['email_guest']);
    }

    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $this->user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->offsiteBooking = factory(\App\Models\OffsiteBooking::class)
            ->make();
        $this->offsiteBooking->user_id = $this->user->id;
        $this->offsiteBooking->save();
    }

    protected function getStub()
    {
        return [
            'data'=> [
                'type'=> 'offsite-bookings',
                'attributes'=> [
                    'first_name_guest'=> $this->fake->firstName,
                    'last_name_guest'=> $this->fake->randomElement([$this->fake->lastName, null]),
                    'address_guest'=> $this->fake->randomElement([$this->fake->address, null]),
                    'city_guest'=> $this->fake->randomElement([$this->fake->city, null]),
                    'phone_guest'=> $this->fake->phoneNumber,
                    'email_guest'=> $this->fake->email,
                    'vat_guest'=> $this->fake->randomElement([str_random(\App\Constant::ENTERPRISE_VAT_LENGTH), null]),
                    'experience_date'=> \Carbon\Carbon::today()->addDays(10)->format('Y-m-d'),
                    'arrival_date'=> \Carbon\Carbon::today()->addDays(11)->format('Y-m-d'),
                    'experience_title'=> $this->fake->text(\App\Constant::EXPERIENCE_TITLE_LENGTH),
                    'experience_description'=> $this->fake->text(\App\Constant::EXPERIENCE_DESCRIPTION_LENGTH),
                    'boat_type'=> \App\Models\BoatType::inRandomOrder()->first()->name,
                    'departure_port'=> $this->fake->city,
                    'arrival_port'=> $this->fake->city,
                    'departure_time'=> $this->fake->time(),
                    'arrival_time'=> $this->fake->time(),
                    'adults'=> $this->fake->numberBetween(1, 5),
                    'kids'=> $this->fake->numberBetween(0, 2),
                    'babies'=> $this->fake->numberBetween(0, 2),
                    'included_services'=> $this->fake->randomElement([$this->fake->text(\App\Constant::ADDITIONAL_SERVICE_LENGTH), null]),
                    'excluded_services'=> $this->fake->randomElement([$this->fake->text(\App\Constant::ADDITIONAL_SERVICE_LENGTH), null]),
                    'drinks'=> $this->fake->randomElement([$this->fake->text(\App\Constant::ADDITIONAL_SERVICE_LENGTH), null]),
                    'dishes'=> $this->fake->randomElement([$this->fake->text(\App\Constant::ADDITIONAL_SERVICE_LENGTH), null]),
                    'price'=>  $this->fake->numberBetween(100, 200),
                    'fee'=> $this->fake->numberBetween(10, 50),
                    'expiration_date'=> \Carbon\Carbon::today()->addDays(8)->format('Y-m-d'),
                    'language_guest'=> \App\TranslationModels\Language::inRandomOrder()->first()->language,
                    'currency_guest'=> \App\Models\Currency::inRandomOrder()->first()->name,
                    'language_captain'=> \App\TranslationModels\Language::inRandomOrder()->first()->language,
                    'currency_captain'=> \App\Models\Currency::inRandomOrder()->first()->name,
                    'first_name_captain'=> $this->fake->firstName,
                    'last_name_captain'=> $this->fake->lastName,
                    'email_captain'=> $this->fake->email,
                    'phone_captain'=> $this->fake->phoneNumber,
                    'without_payment' => false,
                ]
	        ]
        ];
    }

}