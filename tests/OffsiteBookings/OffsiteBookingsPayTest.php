<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class OffsiteBookingsPayTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $offsiteBooking;
    private $user;

    /** @test */
    public function it_does_not_send_emails_if_offsite_bookings_is_not_in_status_pending()
    {
        //arrange
        $this->setThingsUp();
        $this->offsiteBooking->status = \App\Constant::OFFSITE_BOOKING_STATUS_PAID;
        $this->offsiteBooking->save();

        //act
        $offsiteBookingController = new \App\Http\Controllers\v1\OffsiteBookingsController();
        $offsiteBookingController->payBookingRequest($this->offsiteBooking->id);

        //assert
        $this->seeEmailsSent(0);
    }

    /** @test */
    public function it_sends_3_emails_if_it_is_properly_paid_and_set_status_to_paid()
    {
        //arrange
        $this->setThingsUp();

        //act
        $offsiteBookingController = new \App\Http\Controllers\v1\OffsiteBookingsController();
        $offsiteBookingController->payBookingRequest($this->offsiteBooking->id);

        //assert
        $newOffsiteBooking = \App\Models\OffsiteBooking::find($this->offsiteBooking->id);
        $this->seeEmailsSent(3);
        $this->seeEmailTo($this->user->email);
        $this->seeEmailFrom("noreply@nausdream.com");
        $this->seeEmailTo($this->offsiteBooking->email_captain);
        $this->seeEmailTo('support@nausdream.com');
        $this->assertEquals(\App\Constant::OFFSITE_BOOKING_STATUS_PAID, $newOffsiteBooking->status);

    }

    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);

        // Create data
        $this->user = factory(\App\Models\User::class)->states('dummy')->create();
        $this->offsiteBooking = factory(\App\Models\OffsiteBooking::class)
            ->make();
        $this->offsiteBooking->user_id = $this->user->id;
        $this->offsiteBooking->save();
    }
}