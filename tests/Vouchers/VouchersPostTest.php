<?php

/**
 * User: Giuseppe Basciu
 * Date: 10/07/2017
 * Time: 11:15
 */

use App\Models\Administrator;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class VouchersPostTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;
    use MailTracking;

    private $route = 'v1/vouchers';
    private $admin;

    /** @test */
    public function it_returns_403_if_no_jwt_is_provided()
    {
        //arrange
        $this->setThingsUp();

        //act
        $this->postJson($this->route, $this->getStub(), []);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'no_jwt_provided']);

    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $this->setThingsUp();
        $token = JwtService::getTokenStringFromAccount($this->admin); // Retrieves the generated token

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_tampered_with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $this->setThingsUp();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($this->admin->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'token_expired']);
    }

    /**
     * @test
     */
    public function it_returns_409_if_type_of_resource_is_not_correct()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['type'] = 'not_voucher';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /**
     * @test
     */
    public function it_returns_422_if_one_of_the_attributes_of_the_request_is_not_valid()
    {
        //first_name
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['first_name']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'first_name']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['first_name'] = str_random(\App\Constant::FIRST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'first_name']);

        //last_name
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['last_name'] = str_random(\App\Constant::LAST_NAME_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'last_name']);

        //departure_date
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'departure_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_date'] = \Carbon\Carbon::yesterday()->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'date_greater_or_equal_today']);
        $this->seeJson(['title' => 'departure_date']);

        //arrival_date
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = 'not_a_date';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'arrival_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = \Carbon\Carbon::today()->addDays(9)->format('Y-m-d');

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'arrival_date_greater_or_equal_to_departure_date']);
        $this->seeJson(['title' => 'arrival_date']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_date']);

        //experience_title
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['experience_title']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'experience_title']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['experience_title'] = str_random(\App\Constant::EXPERIENCE_TITLE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'experience_title']);

        //departure_port
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_port']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_port'] = str_random(\App\Constant::PORT_DEPARTURE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'departure_port']);

        //arrival_port
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['arrival_port']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_port']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_port'] = str_random(\App\Constant::PORT_DEPARTURE_LENGTH + 1);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'arrival_port']);

        //departure_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'departure_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'departure_time']);

        //arrival_time
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['arrival_time']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'arrival_time']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['arrival_time'] = 'not_a_time';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'dateformat']);
        $this->seeJson(['title' => 'arrival_time']);

        //adults
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['adults']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'adults']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['adults'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'adults']);

        //kids
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['kids']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'kids']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['kids'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'kids']);

        //babies
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['babies']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'babies']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['babies'] = -1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'babies']);

        //price
        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'price']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = 9;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'price']);

        //fee
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['fee']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = 'not_a_number';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'integer']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = 0;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'min']);
        $this->seeJson(['title' => 'fee']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['fee'] = $stub['data']['attributes']['price'] + 1;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'max']);
        $this->seeJson(['title' => 'fee']);

        //language_guest
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['language_guest']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'language_guest']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['language_guest'] = 'not_a_language';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'language_guest']);

        //currency_captain
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        unset($stub['data']['attributes']['currency_captain']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'required']);
        $this->seeJson(['title' => 'currency_captain']);

        //arrange
        $stub = $this->getStub();
        $stub['data']['attributes']['currency_captain'] = 'not_a_currency';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['code' => 'exists']);
        $this->seeJson(['title' => 'currency_captain']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_user_is_not_admin()
    {
        //arrange
        $this->setThingsUp();
        $user = factory(\App\Models\User::class)->states('dummy')->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }

    /**
     * @test
     */
    public function it_returns_403_if_admin_has_no_bookings_permission()
    {
        //arrange
        $this->setThingsUp();
        $this->admin->authorizations()->detach();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['code' => 'not_authorized']);
    }


    /**
     * @test
     */
    public function it_returns_200_if_request_is_ok_and_it_is_from_admin_with_bookings_authorization()
    {
        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(1);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = null;
        $stub['data']['attributes']['fee'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(2);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");


        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['price'] = '';
        $stub['data']['attributes']['fee'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(3);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        unset($stub['data']['attributes']['price']);
        unset($stub['data']['attributes']['fee']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(4);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_date'] = null;
        $stub['data']['attributes']['arrival_date'] = null;

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(5);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");


        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        $stub['data']['attributes']['departure_date'] = '';
        $stub['data']['attributes']['arrival_date'] = '';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(6);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");

        //arrange
        $this->setThingsUp();
        $stub = $this->getStub();
        unset($stub['data']['attributes']['departure_date']);
        unset($stub['data']['attributes']['arrival_date']);

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($this->admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeEmailsSent(7);
        $this->seeEmailTo('support@nausdream.com');
        $this->seeEmailFrom("noreply@nausdream.com");

    }

    // HELPERS

    protected function setThingsUp()
    {
        $this->admin = factory(Administrator::class)->create();
        $this->admin->authorizations()->attach(\App\Models\Authorization::all()->where('name', 'bookings')->first()->id);
    }

    protected function getStub()
    {
        return [
            'data'=> [
                'type'=> 'vouchers',
                'attributes'=> [
                    'first_name'=> $this->fake->firstName,
                    'last_name'=> $this->fake->randomElement([$this->fake->lastName, null]),
                    'departure_date'=> \Carbon\Carbon::today()->addDays(10)->format('Y-m-d'),
                    'arrival_date'=> \Carbon\Carbon::today()->addDays(12)->format('Y-m-d'),
                    'experience_title'=> $this->fake->text(40),
                    'departure_port'=> $this->fake->city,
                    'arrival_port'=> $this->fake->city,
                    'departure_time'=> $this->fake->time(),
                    'arrival_time'=> $this->fake->time(),
                    'adults'=> $this->fake->numberBetween(1, 5),
                    'kids'=> $this->fake->numberBetween(0, 2),
                    'babies'=> $this->fake->numberBetween(0, 2),
                    'price'=>  $this->fake->numberBetween(100, 200),
                    'fee'=> $this->fake->numberBetween(10, 50),
                    'language_guest'=> \App\TranslationModels\Language::inRandomOrder()->first()->language,
                    'currency_captain'=> \App\Models\Currency::inRandomOrder()->first()->name,
                ]
	        ]
        ];
    }

}