<?php

namespace App\TranslationModels;

use App\Interfaces\Transformable;
use App\Models\AbstractModel;
use App\Services\Transformers\Transformer;
use App\Services\Transformers\TranslatorTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends AbstractModel implements Transformable
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $connection = 'mysql_translation';
    protected $table = 'users';
    protected $hidden = ['id', 'email', 'phone', 'created_at', 'updated_at', 'deleted_at'];


    /**
     * The attributes that should be casted for arrays.
     *
     * @var array
     */

    protected $casts = [
        'is_admin' => 'boolean',
    ];

    public function languages()
    {
        return $this->belongsToMany(\App\TranslationModels\Language::class, 'languages_users', 'user_id', 'language_id');
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer() : Transformer
    {
        return new TranslatorTransformer();
    }
}
