<?php

namespace App\TranslationModels;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SentenceTranslation extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $connection = 'mysql_translation';
    protected $table = 'sentence_translations';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be guarded for arrays.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function sentence()
    {
        return $this->belongsTo(\App\TranslationModels\Sentence::class, 'sentence_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(\App\TranslationModels\Language::class, 'language_id', 'id');
    }
}
