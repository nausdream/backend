<?php

namespace App\TranslationModels;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $connection = 'mysql_translation';
    protected $table = 'languages';
    protected $fillable = ['language'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];


    public function users()
    {
        return $this->belongsToMany(\App\TranslationModels\User::class, 'languages_users', 'language_id', 'user_id');
    }
}
