<?php

namespace App\TranslationModels;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LanguagesUser extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $connection = 'mysql_translation';
    protected $table = 'languages_users';


    public function language()
    {
        return $this->belongsTo(\App\TranslationModels\Language::class, 'language_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\TranslationModels\User::class, 'user_id', 'id');
    }
}
