<?php

namespace App\Mail;

use App\Constant;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class EdenVoucherToSupport extends Mailable
{
    use Queueable, SerializesModels;

    protected $attributes;
    protected $translationAddressPdf;

    /**
     * Create a new message instance.
     *
     * @param array $attributes
     */
    public function __construct($attributes)
    {
        $this->attributes = $attributes;
        $this->translationAddressPdf = 'pdf/eden-voucher.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Generate pdf voucher
        $pdfData = [
            'translationAddress' => $this->translationAddressPdf,
            'language' => $this->attributes['guest_language'],
            'village_name' => $this->attributes['village_name'],
            'assistant_name' => $this->attributes['assistant_name'],
            'village_phone' => $this->attributes['village_phone'],
            'village_mail' => $this->attributes['village_mail'],
            'voucher_number' => $this->attributes['voucher_number'],
            'contact_name' => $this->attributes['contact_name'],
            'guest_name' => $this->attributes['guest_name'],
            'room_number' => $this->attributes['room_number'],
            'guests' => $this->attributes['guests'],
            'experience_title' => $this->attributes['experience_title'],
            'tour_number' => $this->attributes['tour_number'],
            'experience_date' => Carbon::parse($this->attributes['experience_date'])->format('d/m/Y'),
            'departure_port' => $this->attributes['departure_port'],
            'time' => Carbon::createFromFormat('H:i:s', $this->attributes['departure_time'])->format('H:i') . ' - ' . Carbon::createFromFormat('H:i:s', $this->attributes['arrival_time'])->format('H:i'),
            'company_name' => Constant::COMPANY_NAME,
            'company_phone' => Constant::SUPPORT_PHONE,
            'company_mail' => Constant::BOOKING_MAIL,
            'company_site' => Constant::WEB_SITE_ADDRESS_FOR_PDF,
            'company_legal_address' => Constant::COMPANY_ADDRESS,
            'company_operative_address' => Constant::COMPANY_OPERATIVE_ADDRESS,
            'company_city' => Constant::COMPANY_CITY,
            'today' => Carbon::today()->format('d/m/Y'),
        ];
        $pdfName = $this->attributes['voucher_number'] . '_' . $this->attributes['guest_name'] . '.pdf';

        $pdf = PDF::loadView('pdf.eden-voucher', $pdfData);

        \App::setLocale(\Lang::getFallback());
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(Constant::SUPPORT_MAIL, Constant::SUPPORT_NAME)
            ->subject('New Eden Voucher ' . $this->attributes['guest_name'])
            ->view('emails.administrators.eden-voucher')
            ->with([
                'language' => \Lang::getFallback()
            ])
            ->attachData($pdf->output(), $pdfName, [
                'mime' => 'application/pdf',
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
