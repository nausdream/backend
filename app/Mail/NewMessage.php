<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use App\Models\Message;
use App\Services\TokenService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Message
     */
    protected $message;

    /**
     * Create a new message instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Fetch conversation, guest and captain
        $conversation = $this->message->conversation()->first();
        $guest = $conversation->user()->first();
        $captain = $conversation->captain()->first();

        // Check if the message is sent to guest or captain and set the related parameters
        if ($this->message->user_id == $guest->id) {
            $translationAddress = 'emails/captains/new-message.';
            $view = 'emails.captains.new-message';
            $language = $captain->language;
            $email = $captain->email;
            $name = $captain->first_name;
            $senderName = $guest->first_name;
            $token = TokenService::generateTokenByUser($captain);
        } else {
            $translationAddress = 'emails/users/new-message.';
            $view = 'emails.users.new-message';
            $language = $guest->language;
            $email = $guest->email;
            $name = $guest->first_name;
            $senderName = $captain->first_name;
            $token = TokenService::generateTokenByUser($guest);
        }

        // Check if there is a booking and take the right experienceVersion and experience date
        $booking = Booking::where('conversation_id', $conversation->id)->get()->last();
        if (isset($booking)) {
            $experienceVersion = $booking->experienceVersion()->first();
            $experienceDate = Carbon::parse($booking->experience_date)->format('d/m/Y');
        } else {
            $experienceVersion = $conversation->experience()->first()->lastFinishedAndAcceptedExperienceVersion();
            $experienceDate = Carbon::parse($conversation->request_date)->format('d/m/Y');
        }

        // Fetch the seo object
        $seo = $experienceVersion->seos()->where('language', $language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
            if (!isset($seo)) {
                $seo = $experienceVersion->seos()->first();
            }
        }

        // Set up email
        \App::setLocale($language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($email, $name)
            ->subject(trans($translationAddress . 'subject', ['name' => $senderName], null, $language))
            ->view($view)
            ->with([
                'translationAddress' => $translationAddress,
                'language' => $language,
                'id' => $conversation->id,
                'captain_name' => $captain->first_name,
                'guest_name' => $guest->first_name,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'experience_date' => $experienceDate,
                'token' => $token
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
