<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingPaidToGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->translationAddress = 'emails/users/booking-paid.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $guest = $this->booking->user()->first();

        \App::setLocale($guest->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($guest->email, $guest->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $guest->language))
            ->view('emails.users.booking-paid')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $guest->language,
                'title' => $experienceVersion->title,
                'guest_name' => $guest->first_name,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
