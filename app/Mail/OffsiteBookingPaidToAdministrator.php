<?php

namespace App\Mail;

use App\Constant;
use App\Models\OffsiteBooking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OffsiteBookingPaidToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    protected $offsiteBooking;
    protected $translationAddress;

    /**
     * Create a new message instance.
     *
     * @param OffsiteBooking $offsiteBooking
     */
    public function __construct(OffsiteBooking $offsiteBooking)
    {
        $this->offsiteBooking = $offsiteBooking;
        $this->translationAddress = 'emails/captains/booking-paid.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $guest = $this->offsiteBooking->user()->first();

        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject("Offsite Booking " . $this->offsiteBooking->id . " has been paid")
            ->view('emails.administrators.booking-offsite-paid')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => 'en',
                'id' => $this->offsiteBooking->id,
                'captain_name' => $this->offsiteBooking->first_name_captain,
                'captain_phone' => $this->offsiteBooking->phone_captain,
                'captain_email' => $this->offsiteBooking->email_captain,
                'title' => $this->offsiteBooking->experience_title,
                'guest_name' => $guest->first_name,
                'guest_phone' => $guest->phone,
                'guest_email' => $guest->email,
                'experience_date' => isset($this->offsiteBooking->experience_date) ? Carbon::parse($this->offsiteBooking->experience_date)->format('d/m/Y') : null
            ]);
        return $email;
    }
}
