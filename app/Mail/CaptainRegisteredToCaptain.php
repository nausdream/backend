<?php

namespace App\Mail;

use App\Constant;
use App\Services\JwtService;
use Illuminate\Bus\Queueable;
use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TokenService;
use Lang;

class CaptainRegisteredToCaptain extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $captain;
    protected $translationAddress = 'emails/captains/registered.';

    /**
     * CaptainRegisteredToCaptain constructor.
     * @param User $captain
     */
    public function __construct(User $captain)
    {
        $this->captain = $captain;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        \App::setLocale($this->captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($this->captain->email, $this->captain->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $this->captain->language))
            ->view('emails.captains.registered')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $this->captain->language,
                'name' => $this->captain->first_name,
                'token' => TokenService::generateTokenByUser($this->captain)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
