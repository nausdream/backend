<?php

namespace App\Mail;

use App\Constant;
use App\Models\BoatType;
use App\Models\OffsiteBooking;
use App\Services\PriceHelper;
use App\Services\TokenService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OffsiteBookingToPay extends Mailable
{
    use Queueable, SerializesModels;

    protected $offsiteBooking;
    protected $translationAddress = 'emails/users/offsite-booking-pay.';

    /**
     * Create a new message instance.
     *
     * @param OffsiteBooking $offsiteBooking
     * @param $isNewUser
     */
    public function __construct(OffsiteBooking $offsiteBooking)
    {
        $this->offsiteBooking = $offsiteBooking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $guest = $this->offsiteBooking->user()->first();
        \App::setLocale($guest->language);
        $email = $this->from(Constant::NOREPLY_MAIL, Constant::SUPPORT_NAME)
            ->to($guest->email, $guest->first_name)
            ->subject(trans($this->translationAddress . 'subject', [ 'title' => $this->offsiteBooking->experience_title ], null, $guest->language))
            ->view('emails.users.offsite-booking-pay')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $guest->language,
                'id' => $this->offsiteBooking->id,
                'captain_name' => $this->offsiteBooking->first_name_captain,
                'title' => $this->offsiteBooking->experience_title,
                'description' => $this->offsiteBooking->experience_description,
                'departure_port' => $this->offsiteBooking->departure_port,
                'arrival_port' => $this->offsiteBooking->arrival_port,
                'guest_name' => $guest->first_name,
                'experience_date' => isset($this->offsiteBooking->experience_date) ? Carbon::parse($this->offsiteBooking->experience_date)->format('d/m/Y') : null,
                'request_date' => $this->offsiteBooking->created_at->format('d/m/Y'),
                'expiration_date' => Carbon::parse($this->offsiteBooking->expiration_date)->format('d/m/Y'),
                'departure_time' => Carbon::createFromFormat('H:i:s', $this->offsiteBooking->departure_time)->format('H:i'),
                'arrival_time' => Carbon::createFromFormat('H:i:s', $this->offsiteBooking->arrival_time)->format('H:i'),
                'adults' => $this->offsiteBooking->adults,
                'kids' => $this->offsiteBooking->kids,
                'babies' => $this->offsiteBooking->babies,
                'arrival_date' =>  isset($this->offsiteBooking->arrival_date) ? Carbon::parse($this->offsiteBooking->arrival_date)->format('d/m/Y') : null,
                'boat_type' => BoatType::find($this->offsiteBooking->boat_type)->name ?? null,
                'total_seats' => $this->offsiteBooking->adults + $this->offsiteBooking->kids + $this->offsiteBooking->babies,
                'to_pay_now' => $this->offsiteBooking->fee,
                'to_pay_onboard' => $this->offsiteBooking->price - $this->offsiteBooking->fee,
                'total_price' => $this->offsiteBooking->price,
                'to_pay_now_converted' => ceil(PriceHelper::convertPrice($this->offsiteBooking->currency, $guest->currency, $this->offsiteBooking->fee)),
                'to_pay_onboard_converted' => ceil(PriceHelper::convertPrice($this->offsiteBooking->currency, $guest->currency, $this->offsiteBooking->price - $this->offsiteBooking->fee)),
                'total_price_converted' => ceil(PriceHelper::convertPrice($this->offsiteBooking->currency, $guest->currency, $this->offsiteBooking->price)),
                'included_services' => $this->offsiteBooking->included_services,
                'excluded_services' => $this->offsiteBooking->excluded_services,
                'drinks' => $this->offsiteBooking->drinks,
                'dishes' => $this->offsiteBooking->dishes,
                'currency' => $this->offsiteBooking->currency,
                'currency_symbol' => Constant::CURRENCY_SYMBOLS[$this->offsiteBooking->currency],
                'currency_guest' => $guest->currency,
                'currency_guest_symbol' => Constant::CURRENCY_SYMBOLS[$guest->currency],
                'token' => TokenService::generateTokenByUser($guest)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
