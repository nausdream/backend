<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use App\Models\Coupon;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Services\TokenService;
use App\Services\PhotoHelper;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingAnswerToGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $coupon = Coupon::all()->where('name', $this->booking->coupon)->last();
        $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
        $guest = $this->booking->user()->first();
        $totalSeats = $this->booking->adults + $this->booking->kids + $this->booking->babies;
        $seo = $experienceVersion->seos()->where('language', $captain->language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
            if (!isset($seo)) {
                $seo = $experienceVersion->seos()->first();
            }
        }
        $pricedServices = [];
        $freeServices = [];
        $sumPriceServices = 0;
        foreach ($this->booking->fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            if ($fixedServicePrice->per_person) {
                $price = $fixedServicePrice->price * $totalSeats;
            } else {
                $price = $fixedServicePrice->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($fixedServicePrice->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => true
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'fixed' => true
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        foreach ($this->booking->additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $price = $additionalService->price * $totalSeats;
            } else {
                $price = $additionalService->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $additionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => false
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $additionalService->name,
                    'fixed' => false
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        switch ($this->booking->status) {
            case Constant::BOOKING_STATUS_ACCEPTED:
                $view = 'emails.users.booking-accepted';
                $translationAddress = 'emails/users/booking-answer.';
                break;
            case Constant::BOOKING_STATUS_REJECTED:
                $view = 'emails.users.booking-rejected';
                $translationAddress = 'emails/users/booking-rejected.';
                break;
            default:
                $view = 'emails.users.booking-rejected';
                $translationAddress = 'emails/users/booking-rejected.';
        }
        if (isset($coupon)) {
            $coupon_discount = $this->booking->discount;
            $discount_price = $this->booking->price - $this->booking->discount;
        } else {
            $coupon_discount = 0;
            $discount_price = $this->booking->price;
        }
        \App::setLocale($guest->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($guest->email, $guest->first_name)
            ->subject(trans($translationAddress . 'subject', [], null, $captain->language) . ' ' . $experienceVersion->title)
            ->view($view)
            ->with([
                'translationAddress' => $translationAddress,
                'language' => $guest->language,
                'id' => $this->booking->id,
                'captain_name' => $captain->first_name,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'departure_port' => $experienceVersion->departure_port,
                'arrival_port' => $experienceVersion->arrival_port,
                'guest_name' => $guest->first_name,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
                'request_date' => Carbon::parse($this->booking->request_date)->format('d/m/Y'),
                'accept_date' => Carbon::today()->format('d/m/Y'),
                'departure_time' => $experienceVersion->departure_time,
                'arrival_time' => $experienceVersion->arrival_time,
                'adults' => $this->booking->adults,
                'kids' => $this->booking->kids,
                'babies' => $this->booking->babies,
                'total_seats' => $totalSeats,
                'coupon_discount' => $coupon_discount,
                'to_pay_now' => $this->booking->price_to_pay,
                'to_pay_onboard' => $this->booking->price_on_board,
                'total_price' => $discount_price,
                'to_pay_now_converted' => ceil(PriceHelper::convertPrice($this->booking->currency, $guest->currency, $this->booking->price_to_pay)),
                'to_pay_onboard_converted' => ceil(PriceHelper::convertPrice($this->booking->currency, $guest->currency, $this->booking->price_on_board)),
                'total_price_converted' => ceil(PriceHelper::convertPrice($this->booking->currency, $guest->currency, $discount_price)),
                'total_price_without_services' => $this->booking->price - $sumPriceServices,
                'total_price_services' => $sumPriceServices,
                'price_per_person' => ceil(($this->booking->price - $sumPriceServices) / $totalSeats),
                'priced_services' => $pricedServices,
                'free_services' => $freeServices,
                'entire_boat' => $this->booking->entire_boat,
                'currency' => $this->booking->currency,
                'currency_symbol' => Constant::CURRENCY_SYMBOLS[$this->booking->currency],
                'currency_guest' => $guest->currency,
                'currency_guest_symbol' => Constant::CURRENCY_SYMBOLS[$guest->currency],
                'token' => TokenService::generateTokenByUser($guest)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
