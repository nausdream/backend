<?php

namespace App\Mail;

use App\Constant;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaptainRegisteredToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $captain;

    /**
     * CaptainRegisteredToAdministrator constructor.
     * @param User $captain
     */
    public function __construct(User $captain)
    {
        $this->captain = $captain;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('New captain registered')
            ->view('emails.administrators.captains-registered')
            ->with([
                'name' => $this->captain->first_name,
                'phone' => $this->captain->phone,
                'email' => $this->captain->email,
                'docking_place' => $this->captain->docking_place
            ]);


    }
}
