<?php

namespace App\Mail;

use App\Constant;
use App\Models\BoatVersion;
use Illuminate\Bus\Queueable;
use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TokenService;

class AdminChangedBoatStatusToCaptain extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $captain;
    protected $boatVersion;
    protected $status;
    protected $admin_text;

    /**
     * CaptainRegisteredToCaptain constructor.
     * @param User $captain
     * @param BoatVersion $boatVersion
     * @param int $status
     * @param string $admin_text
     */
    public function __construct(User $captain, BoatVersion $boatVersion, int $status, string $admin_text = null)
    {
        $this->captain = $captain;
        $this->boatVersion = $boatVersion;
        $this->status = $status;
        $this->admin_text = $admin_text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        switch ($this->status) {
            case Constant::STATUS_ACCEPTED:
                $view = 'emails.captains.boat-accepted';
                $translationAddress = 'emails/captains/boat-accepted.';
                break;
            case Constant::STATUS_ACCEPTED_CONDITIONALLY:
                $view = 'emails.captains.boat-accepted-conditionally';
                $translationAddress = 'emails/captains/boat-accepted-conditionally.';
                break;
            case Constant::STATUS_REJECTED:
                $view = 'emails.captains.boat-rejected';
                $translationAddress = 'emails/captains/boat-rejected.';
                break;
            default:
                $view = 'emails.captains.boat-accepted';
                $translationAddress = 'emails/captains/boat-accepted.';
                break;
        }

        \App::setLocale($this->captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($this->captain->email, $this->captain->first_name)
            ->subject(trans($translationAddress . 'subject', [], null, $this->captain->language))
            ->view($view)
            ->with([
                'translationAddress' => $translationAddress,
                'language' => $this->captain->language,
                'name' => $this->captain->first_name,
                'id' => $this->boatVersion->boat()->first()->id,
                'boat_name' => $this->boatVersion->name,
                'admin_text' => $this->admin_text,
                'token' => TokenService::generateTokenByUser($this->captain)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
