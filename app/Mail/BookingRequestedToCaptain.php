<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Models\Price;
use App\Services\TokenService;
use App\Services\PhotoHelper;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingRequestedToCaptain extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Booking
     */
    protected $booking;
    protected $translationAddress = 'emails/captains/booking-request.';

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $boat = $experienceVersion->experience()->first()->boat()->first();
        $captain = $boat->user()->first();
        $guest = $this->booking->user()->first();
        $totalSeats = $this->booking->adults + $this->booking->kids + $this->booking->babies;
        $seo = $experienceVersion->seos()->where('language', $captain->language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
            if (!isset($seo)) {
                $seo = $experienceVersion->seos()->first();
            }
        }
        $pricedServices = [];
        $freeServices = [];
        $sumPriceServices = 0;
        foreach ($this->booking->fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            if ($fixedServicePrice->per_person) {
                $price = $fixedServicePrice->price * $totalSeats;
            } else {
                $price = $fixedServicePrice->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($fixedServicePrice->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => true
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'fixed' => true
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        foreach ($this->booking->additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $price = $additionalService->price * $totalSeats;
            } else {
                $price = $additionalService->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $additionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => false
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $additionalService->name,
                    'fixed' => false
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        $nausdream_fee = floor(($this->booking->price / 100) * $boat->fee);
        \App::setLocale($captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($captain->email, $captain->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $captain->language) . ' ' . $experienceVersion->title)
            ->view('emails.captains.booking-request')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $captain->language,
                'id' => $this->booking->id,
                'captain_name' => $captain->first_name,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'departure_port' => $experienceVersion->departure_port,
                'arrival_port' => $experienceVersion->arrival_port,
                'guest_name' => $guest->first_name,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
                'request_date' => Carbon::parse($this->booking->request_date)->format('d/m/Y'),
                'departure_time' => $experienceVersion->departure_time,
                'arrival_time' => $experienceVersion->arrival_time,
                'adults' => $this->booking->adults,
                'kids' => $this->booking->kids,
                'babies' => $this->booking->babies,
                'total_seats' => $totalSeats,
                'fee' => $boat->fee,
                'nausdream_fee' => $nausdream_fee,
                'total_without_fee' => $this->booking->price - $nausdream_fee,
                'total_price' => $this->booking->price,
                'total_price_without_services' => $this->booking->price - $sumPriceServices,
                'total_price_services' => $sumPriceServices,
                'price_per_person' => ceil(($this->booking->price - $sumPriceServices) / $totalSeats),
                'priced_services' => $pricedServices,
                'free_services' => $freeServices,
                'entire_boat' => $this->booking->entire_boat,
                'currency' => $this->booking->currency,
                'currency_symbol' => Constant::CURRENCY_SYMBOLS[$this->booking->currency],
                'token' => TokenService::generateTokenByUser($captain)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
