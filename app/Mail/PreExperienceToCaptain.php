<?php

namespace App\Mail;

use App\Constant;
use App\Models\BoatVersion;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PreExperienceToCaptain extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->translationAddress = 'emails/captains/pre-experience.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        //$boatVersion = $experienceVersion->experience()->first()->boat()->first()->lastFinishedAndAcceptedBoatVersion();
        $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
        $guest = $this->booking->user()->first();

        \App::setLocale($captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($captain->email, $captain->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $captain->language))
            ->view('emails.captains.pre-experience-offsite')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $captain->language,
                'title' => $experienceVersion->title,
                //'boat_name' => $boatName,
                'captain_name' => $captain->first_name,
                'guest_name' => $guest->first_name,
                'guest_email' => $guest->email,
                'guest_phone' => $guest->phone,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
