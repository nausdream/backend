<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use App\Models\Coupon;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Services\PhotoHelper;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingRequestedToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Booking
     */
    protected $booking;
    protected $translationAddress = 'emails/captains/booking-request.';

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $coupon = Coupon::all()->where('name', $this->booking->coupon)->last();
        $boat = $experienceVersion->experience()->first()->boat()->first();
        $captain = $boat->user()->first();
        $guest = $this->booking->user()->first();
        $totalSeats = $this->booking->adults + $this->booking->kids + $this->booking->babies;
        $seo = $experienceVersion->seos()->where('language', $captain->language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
            if (!isset($seo)) {
                $seo = $experienceVersion->seos()->first();
            }
        }
        $pricedServices = [];
        $freeServices = [];
        $sumPriceServices = 0;
        foreach ($this->booking->fixedAdditionalServices as $fixedAdditionalService) {
            $fixedServicePrice = ExperienceVersionsFixedAdditionalServices::all()
                ->where('fixed_additional_service_id', $fixedAdditionalService->id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();
            if ($fixedServicePrice->per_person) {
                $price = $fixedServicePrice->price * $totalSeats;
            } else {
                $price = $fixedServicePrice->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($fixedServicePrice->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => true
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $fixedAdditionalService->name,
                    'fixed' => true
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        foreach ($this->booking->additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $price = $additionalService->price * $totalSeats;
            } else {
                $price = $additionalService->price;
            }
            if ($price != 0) {
                $convertedPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $this->booking->currency, $price));
                $pricedServices[] = [
                    'name' => $additionalService->name,
                    'price' => $convertedPrice,
                    'fixed' => false
                ];
            } else {
                $convertedPrice = 0;
                $freeServices[] = [
                    'name' => $additionalService->name,
                    'fixed' => false
                ];
            }
            $sumPriceServices += $convertedPrice;
        }
        $nausdream_fee = floor(($this->booking->price / 100) * $boat->fee);
        $price = $this->booking->price;
        if (isset($coupon)) {
            if ($coupon->is_percentual) {
                $coupon_discount = ceil (($price / 100) * $coupon->amount);
                $discount_price = $price - $coupon_discount;
            } else {
                $coupon_discount = ceil(PriceHelper::convertPrice($coupon->currency, $this->booking->currency, $coupon->amount));
                $discount_price = $price - $coupon_discount;
            }
        } else {
            $coupon_discount = 0;
            $discount_price = $price;
        }
        \App::setLocale('en');
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('New booking request for ' . $experienceVersion->title)
            ->view('emails.administrators.booking-request')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => 'en',
                'id' => $this->booking->id,
                'captain_name' => $captain->first_name . ' ' . $captain->last_name,
                'captain_phone' => $captain->phone,
                'captain_email' => $captain->email,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'departure_port' => $experienceVersion->departure_port,
                'arrival_port' => $experienceVersion->arrival_port,
                'guest_name' => $guest->first_name . ' ' . $guest->last_name,
                'guest_phone' => $guest->phone,
                'guest_email' => $guest->email,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
                'request_date' => Carbon::parse($this->booking->request_date)->format('d/m/Y'),
                'departure_time' => $experienceVersion->departure_time,
                'arrival_time' => $experienceVersion->arrival_time,
                'adults' => $this->booking->adults,
                'kids' => $this->booking->kids,
                'babies' => $this->booking->babies,
                'total_seats' => $totalSeats,
                'fee' => $boat->fee,
                'nausdream_fee' => $nausdream_fee,
                'discount' => $coupon_discount,
                'total_without_fee' => $this->booking->price - $nausdream_fee,
                'total_nausdream' => $nausdream_fee - $coupon_discount,
                'total_price' => $this->booking->price,
                'total_price_without_services' => $this->booking->price - $sumPriceServices,
                'total_price_services' => $sumPriceServices,
                'price_per_person' => ceil(($this->booking->price - $sumPriceServices) / $totalSeats),
                'priced_services' => $pricedServices,
                'free_services' => $freeServices,
                'entire_boat' => $this->booking->entire_boat,
                'currency' => $this->booking->currency,
                'currency_symbol' => Constant::CURRENCY_SYMBOLS[$this->booking->currency],
            ]);
        \App::setLocale('en');
        return $email;
    }
}
