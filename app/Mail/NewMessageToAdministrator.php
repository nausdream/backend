<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessageToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Message
     */
    protected $message;

    /**
     * Create a new message instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Fetch conversation, guest and captain
        $conversation = $this->message->conversation()->first();
        $guest = $conversation->user()->first();
        $captain = $conversation->captain()->first();

        // Check if the message is sent to guest or captain and set the related parameters
        if ($this->message->user_id == $guest->id) {
            $senderName = $guest->first_name;
            $is_captain = false;
        } else {
            $senderName = $captain->first_name;
            $is_captain = true;
        }

        // Check if there is a booking and take the right experienceVersion and experience date
        $booking = Booking::where('conversation_id', $conversation->id)->get()->last();
        if (isset($booking)) {
            $experienceVersion = $booking->experienceVersion()->first();
            $experienceDate = Carbon::parse($booking->experience_date)->format('d/m/Y');
        } else {
            $experienceVersion = $conversation->experience()->first()->lastFinishedAndAcceptedExperienceVersion();
            $experienceDate = Carbon::parse($conversation->request_date)->format('d/m/Y');
        }

        // Fetch the seo object
        $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->first();
        }

        // Set up email
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('New booking message for ' . $experienceVersion->title . ' from ' . $senderName)
            ->view('emails.administrators.new-message')
            ->with([
                'captain_name' => $captain->first_name . ' ' . $captain->last_name,
                'captain_phone' => $captain->phone,
                'captain_email' => $captain->email,
                'guest_name' => $guest->first_name . ' ' . $guest->last_name,
                'guest_phone' => $guest->phone,
                'guest_email' => $guest->email,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'experience_date' => $experienceDate,
                'is_captain' => $is_captain,
                'text' => (string) $this->message->message
            ]);
        return $email;
    }
}
