<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingPaidToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->translationAddress = 'emails/captains/booking-paid.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
        $guest = $this->booking->user()->first();
        $seo = $experienceVersion->seos()->where('language', $captain->language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
            if (!isset($seo)) {
                $seo = $experienceVersion->seos()->first();
            }
        }

        \App::setLocale($captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject(trans($this->translationAddress . 'subject', [], null, 'en'))
            ->view('emails.administrators.booking-paid')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => 'en',
                'id' => $this->booking->id,
                'captain_name' => $captain->first_name,
                'captain_phone' => $captain->phone,
                'captain_email' => $captain->email,
                'title' => $experienceVersion->title,
                'slug_url' => $seo->slug_url,
                'guest_name' => $guest->first_name,
                'guest_phone' => $guest->phone,
                'guest_email' => $guest->email,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y')
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
