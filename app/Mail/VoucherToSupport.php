<?php

namespace App\Mail;

use App\Constant;
use App\Models\OffsiteBooking;
use Carbon\Carbon;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VoucherToSupport extends Mailable
{
    use Queueable, SerializesModels;

    protected $attributes;
    protected $translationAddressPdf;

    /**
     * Create a new message instance.
     *
     * @param array $attributes
     */
    public function __construct($attributes)
    {
        $this->attributes = $attributes;
        $this->translationAddressPdf = 'pdf/voucher.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Generate pdf voucher
        $pdfData = [
            'translationAddress' => $this->translationAddressPdf,
            'language' => $this->attributes['language_guest'],
            'id' => null,
            'company_name' => Constant::COMPANY_NAME,
            'company_phone' => Constant::SUPPORT_PHONE,
            'company_mail' => Constant::BOOKING_MAIL,
            'company_site' => Constant::WEB_SITE_ADDRESS_FOR_PDF,
            'company_legal_address' => Constant::COMPANY_ADDRESS,
            'company_operative_address' => Constant::COMPANY_OPERATIVE_ADDRESS,
            'company_city' => Constant::COMPANY_CITY,
            'today' => Carbon::today()->format('d/m/Y'),
            'guest_name' => $this->attributes['first_name'] . ' ' . $this->attributes['last_name'],
            'experience_title' => $this->attributes['experience_title'],
            'boarding' => (isset($this->attributes['departure_date']) ? (Carbon::createFromFormat('Y-m-d', $this->attributes['departure_date'])->format('d/m/Y') . ' - ') : '') . Carbon::createFromFormat('H:i:s', $this->attributes['departure_time'])->format('H:i'),
            'landing' => (isset($this->attributes['arrival_date']) ? (Carbon::createFromFormat('Y-m-d', $this->attributes['arrival_date'])->format('d/m/Y') . ' - ') : '') . Carbon::createFromFormat('H:i:s', $this->attributes['arrival_time'])->format('H:i'),
            'departure_port' => $this->attributes['departure_port'],
            'arrival_port' => $this->attributes['arrival_port'],
            'adults' => $this->attributes['adults'],
            'kids' => $this->attributes['kids'],
            'babies' => $this->attributes['babies'],
            'total' => $this->attributes['price'],
            'fee' => $this->attributes['fee'],
            'to_pay_on_board' => $this->attributes['price'] - $this->attributes['fee'],
            'currency_symbol' => Constant::CURRENCY_SYMBOLS_FOR_PDF[$this->attributes['currency_captain']]
        ];
        $pdfName = $this->attributes['first_name'] . '_' . $this->attributes['last_name'] . '_Nausdream.pdf';

        $pdf = PDF::loadView('pdf.voucher', $pdfData);

        \App::setLocale(\Lang::getFallback());
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(Constant::SUPPORT_MAIL, Constant::SUPPORT_NAME)
            ->subject('New Voucher ' . $this->attributes['first_name'])
            ->view('emails.administrators.voucher')
            ->with([
                'language' => \Lang::getFallback()
            ])
            ->attachData($pdf->output(), $pdfName, [
                'mime' => 'application/pdf',
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
