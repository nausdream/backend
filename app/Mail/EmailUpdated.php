<?php

namespace App\Mail;

use App\Constant;
use App\Models\User;
use App\Services\TokenService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUpdated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;
    protected $translationAddress = 'emails/users/email-updated.';

    /**
     * EmailUpdated constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        \App::setLocale($this->user->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($this->user->email, $this->user->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $this->user->language))
            ->view('emails.users.email-updated')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $this->user->language,
                'name' => $this->user->first_name,
                'token' => TokenService::generateTokenByUser($this->user)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
