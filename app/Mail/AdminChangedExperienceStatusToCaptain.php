<?php

namespace App\Mail;

use App\Constant;
use App\Models\ExperienceVersion;
use Illuminate\Bus\Queueable;
use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\TokenService;

class AdminChangedExperienceStatusToCaptain extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $experienceVersion;
    protected $status;

    /**
     * AdminChangedExperienceStatusToCaptain constructor.
     * @param ExperienceVersion $experienceVersion
     * @param int $status
     */
    public function __construct(ExperienceVersion $experienceVersion, $status)
    {
        $this->experienceVersion = $experienceVersion;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        switch ($this->status) {
            case Constant::STATUS_ACCEPTED:
                $view = 'emails.captains.experience-accepted';
                $translationAddress = 'emails/captains/experience-accepted.';
                break;
            case Constant::STATUS_ACCEPTED_CONDITIONALLY:
                $view = 'emails.captains.experience-accepted-conditionally';
                $translationAddress = 'emails/captains/experience-accepted-conditionally.';
                break;
            case Constant::STATUS_REJECTED:
                $view = 'emails.captains.experience-rejected';
                $translationAddress = 'emails/captains/experience-rejected.';
                break;
            case Constant::STATUS_FREEZED:
                $view = 'emails.captains.experience-freezed';
                $translationAddress = 'emails/captains/experience-freezed.';
                break;
            default:
                $view = 'emails.captains.experience-accepted';
                $translationAddress = 'emails/captains/experience-accepted.';
                break;
        }

        $experience = $this->experienceVersion->experience()->first();
        $boat = $experience->boat()->first();
        $boatVersion = $boat->lastFinishedAndAcceptedBoatVersion();
        $captain = $boat->user()->first();
        $steps = $this->experienceVersion->steps()->get();
        $steps_array = [];
        foreach ($steps as $step) {
            $steps_array[$step->step] = $step->message;
        }

        \App::setLocale($captain->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($captain->email, $captain->first_name)
            ->subject(trans($translationAddress . 'subject', [], null, $captain->language))
            ->view($view)
            ->with([
                'translationAddress' => $translationAddress,
                'language' => $captain->language,
                'id' => $experience->id,
                'name' => $captain->first_name,
                'title' => $this->experienceVersion->title,
                'boat_name' => $boatVersion->name,
                'step_1' => isset($steps_array[1]) ? $steps_array[1] : null,
                'step_2' => isset($steps_array[2]) ? $steps_array[2] : null,
                'step_3' => isset($steps_array[3]) ? $steps_array[3] : null,
                'step_4' => isset($steps_array[4]) ? $steps_array[4] : null,
                'token' => TokenService::generateTokenByUser($captain)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
