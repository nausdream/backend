<?php

namespace App\Mail;

use App\Constant;
use App\Models\User;
use App\Services\TokenService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PartnerRegistered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $partner;

    /**
     * PartnerRegistered constructor.
     *
     * @param User $partner
     */
    public function __construct(User $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->partner->language)) {
            $language = $this->partner->language;
        } else {
            $language = \Lang::getFallback();
        }
        \App::setLocale($language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($this->partner->email, $this->partner->first_name)
            ->subject(trans('emails/partners/registered.subject'))
            ->view('emails.partners.registered')
            ->with([
                'name' => $this->partner->name,
                'token' => TokenService::generateTokenByUser($this->partner)
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
