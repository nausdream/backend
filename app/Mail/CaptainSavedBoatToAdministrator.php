<?php

namespace App\Mail;

use App\Constant;
use App\Models\BoatVersion;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaptainSavedBoatToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $boatVersion;

    /**
     * CaptainSavedBoatToAdministrator constructor.
     * @param BoatVersion $boatVersion
     */
    public function __construct(BoatVersion $boatVersion)
    {
        $this->boatVersion = $boatVersion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('New boat saved')
            ->view('emails.administrators.boat-saved')
            ->with([
                'name' => $this->boatVersion->name,
                'first_name' => $this->boatVersion->boat()->first()->user()->first()->first_name,
                'last_name' => $this->boatVersion->boat()->first()->user()->first()->last_name,
                'docking_place' => $this->boatVersion->boat()->first()->user()->first()->docking_place
            ]);
    }
}
