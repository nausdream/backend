<?php

namespace App\Mail;

use App\Constant;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactToSupport extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $text;

    /**
     * Create a new message instance.
     *
     * @param $email
     * @param $text
     */
    public function __construct($email, $text)
    {
        $this->email = $email;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('Contact from user for event on boat')
            ->text('emails.contacts',['text' => $this->text, 'email' => $this->email]);
    }
}
