<?php

namespace App\Mail;

use App\Constant;
use App\Models\ExperienceVersion;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaptainSavedExperienceToAdministrator extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ExperienceVersion
     */
    protected $experienceVersion;

    /**
     * CaptainSavedExperienceToAdministrator constructor.
     * @param ExperienceVersion $experienceVersion
     */
    public function __construct(ExperienceVersion $experienceVersion)
    {
        $this->experienceVersion = $experienceVersion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to(env('SUPPORT_EMAIL'), Constant::SUPPORT_NAME)
            ->subject('New experience saved')
            ->view('emails.administrators.experience-saved')
            ->with([
                'title' => $this->experienceVersion->title,
                'first_name' => $this->experienceVersion->experience()->first()->boat()->first()->user()->first()->first_name,
                'last_name' => $this->experienceVersion->experience()->first()->boat()->first()->user()->first()->last_name,
                'boat_name' => $this->experienceVersion->experience()->first()->boat()->first()->lastFinishedAndAcceptedBoatVersion()->name,
                'departure_port' => $this->experienceVersion->experience()->first()->boat()->first()->user()->first()->docking_place
            ]);
    }
}
