<?php

namespace App\Mail;

use App\Constant;
use App\Models\OffsiteBooking;
use PDF;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use View;

class PreExperienceOffsiteToGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $offsiteBooking;
    protected $translationAddress;
    protected $translationAddressPdf;

    /**
     * Create a new message instance.
     *
     * @param OffsiteBooking $offsiteBooking
     */
    public function __construct(OffsiteBooking $offsiteBooking)
    {
        $this->offsiteBooking = $offsiteBooking;
        $this->translationAddress = 'emails/users/pre-experience.';
        $this->translationAddressPdf = 'pdf/voucher.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $guest = $this->offsiteBooking->user()->first();

        // Generate pdf voucher
        $pdfData = [
            'translationAddress' => $this->translationAddressPdf,
            'language' => $guest->language,
            'company_name' => Constant::COMPANY_NAME,
            'company_phone' => Constant::SUPPORT_PHONE,
            'company_mail' => Constant::BOOKING_MAIL,
            'company_site' => Constant::WEB_SITE_ADDRESS_FOR_PDF,
            'company_legal_address' => Constant::COMPANY_ADDRESS,
            'company_operative_address' => Constant::COMPANY_OPERATIVE_ADDRESS,
            'company_city' => Constant::COMPANY_CITY,
            'id' => $this->offsiteBooking->id,
            'today' => Carbon::today()->format('d/m/Y'),
            'guest_name' => $guest->first_name . ' ' . $guest->last_name,
            'experience_title' => $this->offsiteBooking->experience_title,
            'boarding' => (isset($this->offsiteBooking->experience_date) ? (Carbon::createFromFormat('Y-m-d', $this->offsiteBooking->experience_date)->format('d/m/Y') . ' - ') : '') . Carbon::createFromFormat('H:i:s',$this->offsiteBooking->departure_time)->format('H:i'),
            'landing' => (isset($this->offsiteBooking->arrival_date) ? (Carbon::createFromFormat('Y-m-d', $this->offsiteBooking->arrival_date)->format('d/m/Y') . ' - ') : '') . Carbon::createFromFormat('H:i:s', $this->offsiteBooking->arrival_time)->format('H:i'),
            'departure_port' => $this->offsiteBooking->departure_port,
            'arrival_port' => $this->offsiteBooking->arrival_port,
            'adults' => $this->offsiteBooking->adults,
            'kids' => $this->offsiteBooking->kids,
            'babies' => $this->offsiteBooking->babies,
            'total' => $this->offsiteBooking->price,
            'fee' => $this->offsiteBooking->fee,
            'to_pay_on_board' => $this->offsiteBooking->price - $this->offsiteBooking->fee,
            'currency_symbol' => Constant::CURRENCY_SYMBOLS_FOR_PDF[$this->offsiteBooking->currency]
        ];
        $pdfName = $guest->first_name . '_' . $guest->last_name . '_Nausdream_' . $this->offsiteBooking->id . '.pdf';

        $pdf = PDF::loadView('pdf.voucher', $pdfData);

        \App::setLocale($guest->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($guest->email, $guest->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $guest->language))
            ->view('emails.users.pre-experience')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $guest->language,
                'captain_name' => $this->offsiteBooking->first_name_captain,
                'captain_email' => $this->offsiteBooking->email_captain,
                'captain_phone' => $this->offsiteBooking->phone_captain,
                'guest_name' => $guest->first_name,
                'experience_date' => isset($this->offsiteBooking->experience_date) ? Carbon::createFromFormat('Y-m-d', $this->offsiteBooking->experience_date)->format('d/m/Y') : null,
            ])
            ->attachData($pdf->output(), $pdfName, [
                'mime' => 'application/pdf',
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
