<?php

namespace App\Mail;

use App\Constant;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class PreExperienceToGuest extends Mailable
{
    use Queueable, SerializesModels;

    protected $booking;
    protected $translationAddress;
    protected $translationAddressPdf;

    /**
     * Create a new message instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->translationAddress = 'emails/users/pre-experience.';
        $this->translationAddressPdf = 'pdf/voucher.';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $experienceVersion = $this->booking->experienceVersion()->first();
        $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
        $guest = $this->booking->user()->first();

        // Generate pdf voucher
        $pdfData = [
            'translationAddress' => $this->translationAddressPdf,
            'language' => $guest->language,
            'company_name' => Constant::COMPANY_NAME,
            'company_phone' => Constant::SUPPORT_PHONE,
            'company_mail' => Constant::BOOKING_MAIL,
            'company_site' => Constant::WEB_SITE_ADDRESS_FOR_PDF,
            'company_legal_address' => Constant::COMPANY_ADDRESS,
            'company_operative_address' => Constant::COMPANY_OPERATIVE_ADDRESS,
            'company_city' => Constant::COMPANY_CITY,
            'id' => $this->booking->id,
            'today' => Carbon::today()->format('d/m/Y'),
            'guest_name' => $guest->first_name . ' ' . $guest->last_name,
            'experience_title' => $experienceVersion->title,
            'boarding' => (isset($this->booking->experience_date) ? (Carbon::createFromFormat('Y-m-d', $this->booking->experience_date)->format('d/m/Y') . ' - ') : '') . Carbon::createFromFormat('H:i:s',$experienceVersion->departure_time)->format('H:i'),
            'landing' => Carbon::createFromFormat('H:i:s', $experienceVersion->arrival_time)->format('H:i'),
            'departure_port' => $experienceVersion->departure_port,
            'arrival_port' => $experienceVersion->arrival_port,
            'adults' => $this->booking->adults,
            'kids' => $this->booking->kids,
            'babies' =>$this->booking->babies,
            'total' => $this->booking->price,
            'fee' => $this->booking->price_to_pay,
            'to_pay_on_board' => $this->booking->price_on_board,
            'currency_symbol' => Constant::CURRENCY_SYMBOLS_FOR_PDF[$this->booking->currency]
        ];
        $pdfName = $guest->first_name . '_' . $guest->last_name . '_Nausdream_' . $this->booking->id . '.pdf';

        $pdf = PDF::loadView('pdf.voucher', $pdfData);

        \App::setLocale($guest->language);
        $email = $this->from('noreply@nausdream.com', Constant::SUPPORT_NAME)
            ->to($guest->email, $guest->first_name)
            ->subject(trans($this->translationAddress . 'subject', [], null, $guest->language))
            ->view('emails.users.pre-experience')
            ->with([
                'translationAddress' => $this->translationAddress,
                'language' => $guest->language,
                'captain_name' => $captain->first_name,
                'captain_email' => $captain->email,
                'captain_phone' => $captain->phone,
                'guest_name' => $guest->first_name,
                'experience_date' => Carbon::parse($this->booking->experience_date)->format('d/m/Y'),
            ])
            ->attachData($pdf->output(), $pdfName, [
                'mime' => 'application/pdf',
            ]);
        \App::setLocale(\Lang::getFallback());
        return $email;
    }
}
