<?php

/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 11:17
 */

namespace App\Sms;

use App\Models\Booking;
use App\Models\Conversation;
use App\Models\User;


class AlertToCheckEmailToUser implements Sms
{
    protected $user;
    private $translationAddress = 'sms/users/alert-email.';

    /**
     * AlertToCheckEmailToUser constructor.
     *
     * @param \App\Models\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get phone of the user
     */
    public function getPhone()
    {
        return $this->user->phone;
    }

    public function getText()
    {
        return trans($this->translationAddress . 'text', [], null, $this->user->language);
    }
}