<?php

/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 11:17
 */

namespace App\Sms;

use App\Models\Booking;
use Carbon\Carbon;


class BookingPaidToCaptain implements Sms
{
    protected $booking;
    private $translationAddress = 'sms/captains/booking-paid.';

    /**
     * BookingRequestedToCaptain constructor.
     *
     * @param \App\Models\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get phone of the captain
     */
    public function getPhone()
    {
        $captain = $this->booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first();
        return $captain->phone;
    }

    public function getText()
    {
        $experience_date = Carbon::createFromFormat('Y-m-d', $this->booking->experience_date)->format('d/m/Y');
        $hosts = $this->booking->adults + $this->booking->kids + $this->booking->babies;
        $captain = $this->booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first();
        return trans($this->translationAddress . 'text', ['date' =>$experience_date, 'hosts' => $hosts], null, $captain->language);
    }
}