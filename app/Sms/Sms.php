<?php
/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 13:16
 */

namespace App\Sms;

interface Sms
{
    public function getPhone();
    public function getText();
}