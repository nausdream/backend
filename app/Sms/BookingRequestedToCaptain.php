<?php

/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 11:17
 */

namespace App\Sms;

use App\Models\Booking;


class BookingRequestedToCaptain implements Sms
{
    protected $booking;
    private $translationAddress = 'sms/captains/booking-request.';

    /**
     * BookingRequestedToCaptain constructor.
     *
     * @param \App\Models\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get phone of the captain
     */
    public function getPhone()
    {
        $captain = $this->booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first();
        return $captain->phone;
    }

    public function getText()
    {
        $captain = $this->booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first();
        return trans($this->translationAddress . 'text', [], null, $captain->language);
    }
}