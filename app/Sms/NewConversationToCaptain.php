<?php

/**
 * User: Giuseppe
 * Date: 17/05/2017
 * Time: 11:17
 */

namespace App\Sms;

use App\Models\Conversation;


class NewConversationToCaptain implements Sms
{
    protected $conversation;
    private $translationAddress = 'sms/captains/new-conversation.';

    /**
     * BookingRequestedToCaptain constructor.
     *
     * @param \App\Models\Conversation $conversation
     */
    public function __construct(Conversation $conversation)
    {
        $this->conversation = $conversation;
    }

    /**
     * Get phone of the captain
     */
    public function getPhone()
    {
        $captain = $this->conversation->captain()->first();
        return $captain->phone;
    }

    public function getText()
    {
        $captain = $this->conversation->captain()->first();
        return trans($this->translationAddress . 'text', [], null, $captain->language);
    }
}