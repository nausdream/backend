<?php
/**
 * User: Giuseppe
 * Date: 13/01/2017
 * Time: 15:35
 */

namespace App\Services;


use App\Constant;

class SeoHelper
{
    /**
     * Get meta title
     *
     * @param string $title
     * @return string
     */
    public static function getMetaTitle(string $title)
    {
        // Get last part of final title if input title will be short enough to have it

        $shortTitleLastPart = ' ' . Constant::SEO_SEPARATOR . ' ' . Constant::SEO_DOMAIN_NAME;

        // Check if input title is short enough to add shortTitleLastPart

        if (mb_strlen($title) <= (Constant::SEO_TITLE_LENGTH - mb_strlen($shortTitleLastPart))) {
            return $title;
        }

        // Return cutted input title if it's longer than SEO_TITLE_LENGTH

        return mb_substr($title, 0, Constant::SEO_TITLE_LENGTH);
    }

    /**
     * Get meta title for public site
     *
     * @param string $title
     * @return string
     */
    public static function getMetaTitleForPublicSite(string $title)
    {
        // Get last part of final title if input title will be short enough to have it

        $shortTitleLastPart = ' ' . Constant::SEO_SEPARATOR . ' ' . Constant::SEO_DOMAIN_NAME;

        // Check if input title is short enough to add shortTitleLastPart

        if (mb_strlen($title) <= (Constant::SEO_TITLE_LENGTH - mb_strlen($shortTitleLastPart))) {
            return $title . $shortTitleLastPart;
        }

        // Return cutted input title if it's longer than SEO_TITLE_LENGTH

        return mb_substr($title, 0, Constant::SEO_TITLE_LENGTH);
    }

    /**
     * Get meta description
     *
     * @param string $description
     * @return string
     */
    public static function getMetaDescription(string $description)
    {
        // Return input description cutted to SEO_DESCRIPTION_LENGTH length

        return mb_substr($description, 0, Constant::SEO_DESCRIPTION_LENGTH);
    }

    /**
     * Get slug url
     *
     * @param string $title
     * @param int|null $number
     * @return string
     */
    public static function getSlugUrl(string $title, int $number = null) {

        // Cut title to a shorter string before replace words

        $cutted_title = substr($title, 0, Constant::SEO_SLUG_URL_LENGTH);

        // Remove word with less then 3 characters from input title

        $replaced = preg_replace('/\b[A-z]{1,2}\b\s*/', '', $cutted_title);

        // Return slug url with random number at the end

        if (isset($number)) {
            return str_slug($replaced . ' ' . $number);
        }

        // Return slug url

        return str_slug($replaced);
    }
}