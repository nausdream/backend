<?php
/**
 * User: Giuseppe
 * Date: 25/01/2017
 * Time: 16:26
 */

namespace App\Services;

use App\Models\Area;

class AreaHelper
{
    /**
     * Check if latitude and longitude are inside an area
     *
     * @param Area $area
     * @param $lat
     * @param $lng
     * @return boolean
     */
    public static function inArea(Area $area, $lat, $lng)
    {
        return ($area->point_a_lat <= $lat) && ($lat <= $area->point_b_lat)
            && ($area->point_a_lng <= $lng) && ($lng <= $area->point_b_lng);
    }
}