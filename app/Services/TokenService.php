<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 02/11/2016
 * Time: 13:03
 */

namespace App\Services;

use App\Constant;
use App\Models\User;
use App\Models\Token as UserToken;
use Illuminate\Support\Facades\DB;

class TokenService
{
    /**
     * Generate a mail token for a specific user, admin or translator
     *
     * @param \App\Models\User $user
     * @param $is_mail_token
     * @return mixed|string
     */
    public static function generateTokenByUser(User $user, $is_mail_token = true)
    {
        // Check the type of the user

        $token = new UserToken();

        // Create a random string token until it is unique

        do {
            try {
                $token->token = str_random(Constant::EMAIL_TOKEN_LENGTH);
                $token->is_mail_token = $is_mail_token;
                $user->tokens()->save($token);
                $it_fails = false;
            } catch (\Exception $e) {
                $it_fails = true;
            }
        } while ($it_fails);

        return $token->token;
    }
}