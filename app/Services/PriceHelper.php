<?php
/**
 * User: Giuseppe
 * Date: 19/12/2016
 * Time: 18:38
 */

namespace App\Services;


use App\Models\CurrencyChange;

class PriceHelper
{
    public static function estimatePrice($seats, $services_price, $currency, $period)
    {
        // Initialize price and currency

        $price = 0;

        // Calculate price for entire boat if it is true

        if ($period->entire_boat) {
            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
            if (isset($priceObject)) {
                $priceTemp = $priceObject->price;
                $currencyIn = $priceObject->currency;
            } else {
                $priceTemp = $period->price;
                $currencyIn = $period->currency;
            }
            $priceTemp = PriceHelper::convertPrice($currencyIn, $currency, $priceTemp);
        } else {
            $priceObject = $period->prices()->where('person', 1)->first();
            $priceTemp = PriceHelper::convertPrice($priceObject->currency, $currency, $priceObject->price * $seats);
        }

        $price += $priceTemp;

        return ceil($price + $services_price);
    }

    /**
     * Given an input currency and an amount return the final price
     *
     * @param string $currency_in
     * @param string $currency_out
     * @param int $amount
     * @return int
     */
    public static function convertPrice(string $currency_in, string $currency_out, int $amount)
    {
        if ($currency_in == $currency_out) {
            return $amount;
        }

        $currencyChange = CurrencyChange::all()->where('currency_in', $currency_in)->where('currency_out', $currency_out)->last();

        return $amount * $currencyChange->change;
    }
}