<?php
/**
 * User: Giuseppe
 * Date: 20/02/2017
 * Time: 11:31
 */

namespace App\Services;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\PreExperienceToCaptain;
use App\Mail\PreExperienceToGuest;
use App\Models\Booking;
use Carbon\Carbon;

class PreExperienceMailSender
{
    public static function run()
    {
        // Get all bookings that need that the pre experience mail is sent
        $bookings = Booking::all()
            ->where('status', Constant::BOOKING_STATUS_PAID)
            ->where('experience_date', '<=', Carbon::now()->addDays(3)->toDateString())
            ->where('is_pre_experience_mail_sent', false);

        // Send email to every fetched booking
        foreach ($bookings as $booking) {
            dispatch((new SendMail(new PreExperienceToGuest($booking)))->onQueue(env('SQS_MAIL')));
            dispatch((new SendMail(new PreExperienceToCaptain($booking)))->onQueue(env('SQS_MAIL')));

            $booking->is_pre_experience_mail_sent = true;
            $booking->save();
        }
    }
}