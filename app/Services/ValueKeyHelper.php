<?php
/**
 * User: Luca Puddu
 * Date: 19/12/2016
 * Time: 12:56
 */

namespace App\Services;


use App\Constant;

class ValueKeyHelper
{

    /**
     * Return value by passing key and array
     *
     * @param int|null $key
     * @param array $array
     * @return null
     */
    public static function getValueByKey(int $key = null, $array = [])
    {
        if (!isset($key)){
            return null;
        }
        if (array_key_exists($key, $array)){
            return $array[$key];
        }
        return null;
    }

    /**
     * Return key by passing value and array
     *
     * @param string|null $value
     * @param array $array
     * @return null
     */
    public static function getKeyByValue(string $value = null, $array = [])
    {
        if (!isset($value)){
            return null;
        }
        if (in_array($value, $array)){
            return array_search($value, $array);
        }
        return null;
    }
}