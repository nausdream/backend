<?php
/**
 * User: Luca Puddu
 * Date: 07/12/2016
 * Time: 12:35
 */

namespace App\Services;


use App\Constant;

class LanguageHelper
{
    /**
     * Return priorities array with user language on top
     *
     * @param string|null $language
     * @return array
     */
    public static function getPriorities(string $language = null)
    {
        $priorities = Constant::LANGUAGE_ORDER;

        if (isset($language)) {
            $key = array_search($language, $priorities);

            //if language was found in array, put user language on top of priorities
            if ($key != false){
                unset($priorities[$key]);
                array_unshift($priorities, $language);
            }
        }

        return $priorities;
    }

    /**
     * Return elected language
     *
     * @param string|null $language
     * @return array
     */
    public static function getFirst(string $language = null)
    {
        $priorities = Constant::LANGUAGE_ORDER;

        if (isset($language)) {
            $key = array_search($language, $priorities);

            //if language was found in array, put user language on top of priorities
            if ($key != false){
                return $language;
            }
        }

        return $priorities[0];
    }

}