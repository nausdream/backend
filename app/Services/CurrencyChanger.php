<?php
/**
 * User: Giuseppe
 * Date: 20/02/2017
 * Time: 11:31
 */

namespace App\Services;

use App\Models\CurrencyChange;
use App\Models\Currency;

class CurrencyChanger
{
    public static function run()
    {
        $currencies = Currency::all();
        foreach ($currencies as $currency) {
            $currenciesToChange = Currency::all();
            foreach ($currenciesToChange as $currencyToChange) {
                if ($currency->name != $currencyToChange->name) {
                    $currencyChangeValue = self::getCurrencyChangeLive($currency->name, $currencyToChange->name);

                    $currencyObject = CurrencyChange::all()->where('currency_in', $currency->name)->where('currency_out', $currencyToChange->name)->last();
                    if (isset($currencyObject)) {
                        $currencyObject->change = $currencyChangeValue;
                    } else {
                        $currencyObject = new CurrencyChange([
                            'currency_in' => $currency->name,
                            'currency_out' => $currencyToChange->name,
                            'change' => $currencyChangeValue
                        ]);
                    }
                    $currencyObject->save();
                }
            }
        }
    }

    /**
     * Return the currency change value
     *
     * @param $currencyIn
     * @param $currencyOut
     * @return int
     */
    public static function getCurrencyChangeLive($currencyIn, $currencyOut)
    {
        if ($currencyIn != $currencyOut) {
            $url = "https://finance.google.com/finance/converter?a=1&from=" . $currencyIn . "&to=" . $currencyOut;
            $ch = curl_init();
            $timeout = 0;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $rawdata = curl_exec($ch);
            curl_close($ch);
            $matches = array();
            preg_match_all("|<span class=bld>(.*)</span>|U", $rawdata, $matches);
            $result = explode(" ", $matches[1][0]);
            return $result[0];
        } else {
            return 1;
        }
    }
}