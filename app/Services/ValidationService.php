<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 26/10/2016
 * Time: 16:16
 */

namespace App\Services;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as IlluminateResponse;


class ValidationService
{
    use ValidatesRequests;
    /**
     * Verify that specified params are not null
     *
     * @param Request $request
     * @param array $attributes
     * @return array
     */
    public static function paramsCannotBeNull(Request $request, array $attributes)
    {
        $arrayError = [];

        foreach ($attributes as $attribute) {
            if (!$request->input($attribute)) {
                array_push($arrayError, JsonHelper::createError(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY, $attribute . ' cannot be null'));
            }
        }

        return $arrayError;
    }
}