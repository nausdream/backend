<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 21/10/2016
 * Time: 00:56
 */

namespace App\Services;

use GuzzleHttp\Exception\RequestException;
use App\Services\ResponseHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class RequestService
{

    /**
     * Send an Ajax GET request with attached data to a given URL
     *
     * @param $url
     * @param $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Psr\Http\Message\ResponseInterface|\Symfony\Component\HttpFoundation\Response
     */
    public static function get($url, $data){
        $client = new \GuzzleHttp\Client();

        try {
            $res = $client->request('GET', $url, [
                'query' => $data
            ]);
        }
        catch (RequestException $e){
            //TODO: manage different res codes OR extract to an interface and switch implementation on AccountKitLogin
            return ResponseHelper::responseError(
                'Error message exception: ' . $e->getMessage(),
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'authentication_failed'
            );
        }
        catch (\Exception $e){
            return ResponseHelper::responseError(
                'Error message exception: ' . $e->getMessage(),
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'authentication_failed'
            );
        }

        return $res;
    }
}