<?php
/**
 * Created by PhpStorm.
 * User: Luca
 * Date: 21/10/2016
 * Time: 13:39
 */
namespace App\Services;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;
use App\Models\User;
use App\TranslationModels\User as Translator;
use App\Models\Administrator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class JwtService
{
    const type = 'token';

    /**
     * Return true if signature is correct
     *
     * @param $jwt
     * @return bool
     */
    public static function isSigned($jwt)
    {
        if (self::isAValidJwt($jwt)) {
            $signer = new Sha256();
            $token = (new Parser())->parse($jwt);

            return $token->verify($signer, env('JWT_SECRET'));
        }
        return false;
    }

    /**
     * Return true if all claims (iss, aud, exp, nbf, iat) are met
     *
     * @param $jwt
     * @return bool
     */
    public static function isValid($jwt)
    {
        if (self::isAValidJwt($jwt)) {
            $token = (new Parser())->parse($jwt);

            $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
            $data->setIssuer(env('APP_URL'));

            return $token->validate($data);
        }
        return false;
    }

    /**
     * Get JWT from Eloquent User model
     *
     * @param $user
     * @return array|null
     */
    public static function getTokenDataFromUser($user)
    {
        return self::getJWT(self::getTokenStringFromAccount($user));
    }

    /**
     * Get JWT string from Eloquent User model
     *
     * @param $user
     * @return null|string
     * @internal param bool|null $isAdmin
     */
    public static function getTokenStringFromAccount($user)
    {
        if (!isset($user)) {
            return null;
        }

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time())// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() + 3600000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id) // (sub claim)
        ;


        if ($user instanceof User) {
            $token->set('utyp', 'usr');
        }
        if ($user instanceof Administrator) {
            $token->set('utyp', 'adm');
        }
        if ($user instanceof Translator) {
            $token->set('utyp', 'trs');
        }

        $token = $token->sign($signer, env('JWT_SECRET'))->getToken(); // Retrieves the generated token

        return (string)$token;
    }

    /**
     * Return token in a key-value array
     *
     * @param $jwt
     * @return array
     */
    public static function getJWT($jwt)
    {
        return JsonHelper::createData(self::type, $jwt);
    }

    /**
     * Get user_id (sub claim) from jwt string
     *
     * @param $jwt
     * @return int
     */
    public static function userIdFromJWT($jwt)
    {
        if (! self::isAValidJwt($jwt)){
            return null;
        }

        $jwt = (new Parser())->parse($jwt);

        return $jwt->getClaim('sub');
    }

    /**
     * Return true if the id provided corresponds with the sub claim of the provided JWT
     *
     * @param string $jwt
     * @param $user_id
     * @return bool
     */
    public static function requestedUserIsInJwt(string $jwt, $user_id)
    {
        if (! self::isAValidJwt($jwt)){
            return false;
        }
        $jwt = (new Parser())->parse($jwt);

        return $jwt->getClaim('sub') == $user_id;
    }

    /**
     * Return true if JWT is correctly generated
     *
     * @param $jwt
     * @return bool
     */
    public static function isAValidJwt($jwt)
    {
        try {
            (new Parser())->parse($jwt);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string $jwt
     * @return int
     */
    public static function getSubFromTokenString(string $jwt)
    {
        return (int)self::getClaimFromTokenString('sub', $jwt);
    }

    /**
     * @param string $claim
     * @param string $jwt
     * @return mixed
     */
    public static function getClaimFromTokenString(string $claim, string $jwt){
        if (! self::isAValidJwt($jwt)){
            return null;
        }

        $jwt = (new Parser())->parse($jwt);

        return $jwt->getClaim($claim);
    }

    /**
     * Get account from jwt string
     *
     * @param $jwt
     * @return mixed
     */
    public static function getAccountFromJWT($jwt)
    {
        if (! self::isAValidJwt($jwt)){
            return null;
        }

        $jwt = (new Parser())->parse($jwt);

        try {
            $id = $jwt->getClaim('sub');
        } catch (\OutOfBoundsException $e){
            $id = null;
        }

        try {
            $type = $jwt->getClaim('utyp');
        } catch (\OutOfBoundsException $e){
            $type = null;
        }

        if ($type == 'usr') {
            return User::find($id);
        }
        if ($type == 'adm') {
            return Administrator::find($id);
        }
        if ($type == 'trs') {
            return Translator::find($id);
        }
        return null;

    }

    /**
     * Check jwt complete validity
     *
     * @param $jwt
     * @return mixed
     */
    public static function jwtChecker($jwt)
    {
        if (!isset($jwt)) {
            return ResponseHelper::responseError(
                'No JWT provided',
                SymfonyResponse::HTTP_FORBIDDEN,
                'no_jwt_provided'
            );
        }
        //verify and validate
        if (self::isSigned($jwt)) {
            if (self::isValid($jwt)) {
                return true;
            } else {
                return ResponseHelper::responseError(
                    'Token expired or no permission to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'token_expired'
                );
            }
        } else {
            return ResponseHelper::responseError(
                'Token was tampered with',
                SymfonyResponse::HTTP_FORBIDDEN,
                'token_tampered_with'
            );
        }
    }
}