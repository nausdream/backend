<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 26/10/2016
 * Time: 13:22
 */

namespace App\Services;


class JsonHelper
{
    /**
     * Create a data response
     *
     * @param string $type
     * @param $id
     * @param $attributes
     * @param $relationships
     * @return array
     */
    public static function createData(string $type, $id, $attributes = [], $relationships = [])
    {
        $data = [
            'type' => $type,
            'id' => (string)$id
        ];
        if (!empty ($attributes)) {
            $data['attributes'] = $attributes;
        }
        if (!empty ($relationships)) {
            $data['relationships'] = $relationships;
        }

        return $data;
    }


    /**
     * Create a relationships fragment
     *
     * @param array $relationships
     * @return array
     */
    public static function createRelationships($relationships = [])
    {
        $relationshipFragment = [];
        $keys = [];
        foreach ($relationships as $relationship) {
            if  (!in_array($relationship['type'], $keys)) {
                array_push($keys, $relationship['type']);
            }
        }
        foreach ($keys as $key) {
            foreach ($relationships as $relationship) {
                if ($relationship['type'] == $key) {
                    $relationshipFragment[$key]['data'][] = self::createData(
                        $relationship['type'],
                        $relationship['id']
                    );
                }
            }
        }
        return $relationshipFragment;
    }

    /**
     * Create an error response without title
     *
     * @param int $code
     * @param string|null $detail
     * @param string|null $title
     * @return array
     */
    public static function createError($code, string $detail = null, $title = null)
    {
        return [
            'code' => (string)$code,
            'detail' => $detail,
            'title' => $title
        ];
    }

    /**
     * Create an error response with title
     *
     * @param $code
     * @param string $title
     * @param string $detail
     * @return array
     */
    public static function createErrorWithTitle($code, string $title, string $detail)
    {
        return [
            'code' => (string)$code,
            'title' => $title,
            'detail' => $detail
        ];
    }

    /**
     * Create the final error message
     *
     * @param $messages
     * @return array
     */
    public static function createErrorMessage($messages)
    {
        return ['errors' => $messages];
    }

    /**
     * Create the final data message
     *
     * @param $data
     * @return array
     */
    public static function createDataMessage($data)
    {
        return ['data' => $data];
    }

    /**
     * Create the final meta message
     *
     * @param $meta
     * @return array
     */
    public static function createMetaMessage($meta)
    {
        return ['meta' => $meta];
    }
}