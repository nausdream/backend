<?php
/**
 * User: Luca Puddu
 * Date: 03/11/2016
 * Time: 17:53
 */

namespace App\Services;

use App\Constant;
use App\Models\User;
use App\Models\Administrator;
use App\TranslationModels\User as Translator;
use App\Interfaces\Transformable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ResponseHelper
{
    /**
     * Return response error
     *
     * @param $message
     * @param int $responseCode
     * @param null $errorCode
     * @param null $title
     * @return \Illuminate\Http\Response
     */
    public static function responseError($message, int $responseCode, $errorCode = null, $title = null)
    {
        if (is_string($message)) {
            return response(JsonHelper::createErrorMessage([JsonHelper::createError($errorCode == null ? $responseCode : $errorCode, $message, $title)]), $responseCode);
        }
        return response(JsonHelper::createErrorMessage($message), $responseCode);
    }

    /**
     * Return response no content
     *
     * @return \Illuminate\Http\Response
     */
    public static function responseNoContent()
    {
        return response('', SymfonyResponse::HTTP_NO_CONTENT);
    }


    /**
     * Return response for login
     *
     * @param $account
     * @return \Illuminate\Http\Response
     */
    public static function responseForLogin($account)
    {
        return self::responseAccount($account, SymfonyResponse::HTTP_OK);
    }

    /**
     * Return response for registering new user
     *
     * @param $user
     * @return \Illuminate\Http\Response
     */
    public static function responseForRegister($user)
    {
        return self::responseAccount($user, SymfonyResponse::HTTP_CREATED);
    }

    /**
     * @param $account
     * @param $code
     * @return \Illuminate\Http\Response
     */
    protected static function responseAccount(Transformable $account, $code)
    {
        $tokenString = JwtService::getTokenStringFromAccount($account);
        $csrf_token = str_random(Constant::CSRF_TOKEN_LENGTH);
        $accountTransformer = $account->getTransformer();
        if ($code == SymfonyResponse::HTTP_CREATED) {
            $response = JsonHelper::createDataMessage($accountTransformer->transform($account));
        } else {
            $response = JsonHelper::createDataMessage(JsonHelper::createData($accountTransformer->getType(), $accountTransformer->getId($account)));
        }
        $response = array_merge($response, JsonHelper::createMetaMessage(['csrf_token' => $csrf_token, 'access_token' => $tokenString]));

        // Assign different cookie name to prevent cookies conflict on different app users, admin and translator
        $cookie_name = 'csrf_token_user';
        $minute = 0;
        if ($account instanceof User) {
            $cookie_name = 'csrf_token_user';
            $minute = Constant::USER_SESSION_EXPIRATION;
        }
        if ($account instanceof Administrator) {
            $cookie_name = 'csrf_token_admin';
            $minute = 0;
        }
        if ($account instanceof Translator) {
            $cookie_name = 'csrf_token_trans';
            $minute = 0;
        }

        return response($response, $code)
            ->cookie($cookie_name, $csrf_token, $minute);
    }

    /**
     * Return response for update
     *
     * @param $type
     * @param $resources
     * @param null $attributes
     * @return Response
     */
    public static function responseUpdate($type, $resources, $attributes = null)
    {
        if (is_array($resources)) {
            $response = [];
            foreach ($resources as $resource) {
                array_push($response, JsonHelper::createData($type, $resource->id, $resource));
            }
            return response(JsonHelper::createDataMessage($response), SymfonyResponse::HTTP_OK);
        }
        return response(JsonHelper::createDataMessage(JsonHelper::createData($type, $resources->id, isset($attributes) ? $attributes : $resources)), SymfonyResponse::HTTP_OK);
    }

    // TODO: Create common function for ResponseGet and ResponsePost

    /**
     * Return response to get a single resource
     *
     * @param $type
     * @param $resource
     * @param array $attributes
     * @param array $included
     * @param array $relationships
     * @param null $id
     * @param array $mappings
     * @return Response
     */
    public static function responseGet($type, $resource, $attributes = [], $included = [], $relationships = [], $id = null, array $mappings = [])
    {
        $relationships =
            !empty($relationships) ?
            array_merge($relationships, JsonHelper::createRelationships($included)) :
            JsonHelper::createRelationships($included);

        // If a key from the relationship related object name (eg. 'users') is found in the mappings array,
        // change the name on the relationship array to the one specified in the mappings array (eg. ['users' => 'captain'])
        if (!empty($mappings)){
            foreach ($relationships as $key => $data){
                if (! array_key_exists($key, $mappings)){
                    continue;
                }
                $relationships[$mappings[$key]] = $relationships[$key];
                unset($relationships[$key]);
            }
        }

        $response = JsonHelper::createDataMessage(
            JsonHelper::createData(
                $type,
                isset($id) ? (string) ((int) $id) : $resource->id,
                !empty($attributes) ? $attributes : $resource,
                $relationships
            )
        );
        if (!empty($included)) {
            $response['included'] = $included;
        }
        return response($response, SymfonyResponse::HTTP_OK);
    }

    /**
     * Return response to get a resource listing
     *
     * @param $type
     * @param $resources
     * @return Response
     */
    public static function responseIndex(string $type, $resources)
    {
        $response =  ['data' => []];
        foreach ($resources as $resource) {
            $response['data'][] = JsonHelper::createData($type, $resource['id'], $resource);
        }
        return response($response, SymfonyResponse::HTTP_OK);
    }

    /**
     * Return response to post to a single resource
     *
     * @param $type
     * @param $resource
     * @param array $attributes
     * @param array $included
     * @param array $relationships
     * @param null $id
     * @param array $mappings
     * @return Response
     */
    public static function responsePost($type, $resource, $attributes = [], $included = [], $relationships = [], $id = null, array $mappings = [])
    {
        $response = self::responseGet($type, $resource, $attributes, $included, $relationships, $id, $mappings);
        $response->setStatusCode(SymfonyResponse::HTTP_CREATED);
        return $response;
    }
}