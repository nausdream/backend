<?php
/**
 * User: Giuseppe
 * Date: 05/06/2017
 * Time: 00:11
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;

class LogServiceProvider
{
    /**
     * Get a Log service instance
     *
     * @return \Monolog\Logger
     */
    public static function getLogService()
    {
        $monolog = Log::getMonolog();
        $syslog = new \Monolog\Handler\SyslogHandler('papertrail');
        $formatter = new \Monolog\Formatter\LineFormatter(env('APP_URL') . ' %channel%.%level_name%: %message% %extra%');
        $syslog->setFormatter($formatter);
        $monolog->pushHandler($syslog);
        return $monolog;
    }
}