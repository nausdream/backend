<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 14:18
 */

namespace App\Services;


use App\Constant;
use App\Services\Transformers\Transformer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Paginator
{
    /**
     * @param $collection
     * @param Transformer $transformer
     * @param int $perPage
     * @param Request $request
     * @param string $parameterQuery
     * @return array
     */
    public static function getPaginatedData(LengthAwarePaginator $collection, Transformer $transformer, int $perPage = Constant::DEF_ITEMS_PER_PAGE, Request $request, string $parameterQuery = '')
    {
        $total = ceil($collection->total()/$perPage) > 0 ? ceil($collection->total()/$perPage) : 1;

        $prevPage = $collection->currentPage() > 1 ? $collection->currentPage() - 1 : null;
        $prevUrl = isset($prevPage) ? env('APP_URL') . '/' . $request->path() . '?page=' . $prevPage . $parameterQuery : null;

        $nextPage = $collection->currentPage() < $total ? $collection->currentPage() + 1 : null;
        $nextUrl = isset($nextPage) ? env('APP_URL') . '/' . $request->path() . '?page=' . $nextPage . $parameterQuery : null;

        $paginatedResponse = [
            'meta' => [
                'current_page' => $collection->currentPage(),
                'total_pages' => $total,
                'total_elements' => $collection->total()
            ],
            'data' => [],
            'links' => [
                'self' => env('APP_URL') . '/' . $request->path() . '?page=' . $collection->currentPage() . $parameterQuery,
                'first' => env('APP_URL') . '/' . $request->path() . '?page=1' . $parameterQuery,
                'prev' => $prevUrl,
                'next' => $nextUrl,
                'last' => env('APP_URL') . '/' . $request->path() . '?page=' . $total . $parameterQuery
            ]
        ];

        $sortedCollection = $collection->values()->all();
        $data = [];
        foreach ($sortedCollection as $item) {
            $data[] = JsonHelper::createData($transformer->getType(), $transformer->getId($item), $transformer->transform($item));
        }

        $paginatedResponse['data'] = $data;

        return $paginatedResponse;
    }
}