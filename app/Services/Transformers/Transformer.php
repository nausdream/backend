<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:23
 */

namespace App\Services\Transformers;

abstract class Transformer
{
    private $type;
    protected $language;

    /**
     * Transformer constructor.
     * @param string $language
     */
    public function __construct(string $language = null)
    {
        $this->language = $language;
    }

    /**
     * Abstract class to implement in every transformer
     *
     * @param $model
     * @return mixed
     */
    public abstract function transform($model);


    /**
     * Transform the array of models in an array with transformed models
     * @deprecated
     *
     * @param array $model
     * @return array|null
     */
    public function transformCollection(array $model)
    {
        if (!isset($model)) {
            return null;
        }
        return array_map([$this, 'transform'], $model);
    }

    /**
     * Get the type of the resource used by transformer
     *
     * @return mixed
     */
    public abstract function getType();

    /**
     * Get id of the current model used by transformer
     *
     * @param $model
     * @return mixed
     */
    public function getId($model)
    {
        return $model->id;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language = null)
    {
        $this->language = $language;
    }
}