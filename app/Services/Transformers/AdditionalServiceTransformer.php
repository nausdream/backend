<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Services\LanguageHelper;
use App\Services\PriceHelper;

class AdditionalServiceTransformer extends Transformer
{
    private $type = 'additional-services';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($additionalService, $isAdmin = false, $currency = null)
    {
        if ($isAdmin) {
            $additionalService->setVisibilityAll();
        }

        $attributes = $additionalService->toArray();
        if (isset($currency)) {
            $attributes['price'] = Ceil(PriceHelper::convertPrice($attributes['currency'], $currency, $attributes['price']));
            $attributes['currency'] = $currency;
        }

        return $attributes;
    }
}