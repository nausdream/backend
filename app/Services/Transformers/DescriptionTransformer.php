<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class DescriptionTransformer extends Transformer
{
    private $type = 'descriptions';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($description, $isAdmin = false)
    {
        if ($isAdmin) {
            $description->setVisibilityAll();
        }

        return $description->toArray();
    }
}