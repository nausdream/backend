<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Services\PriceHelper;

class BookingTransformer extends Transformer
{
    private $type = 'bookings';
    private $servicesPrice = 0;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($booking, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $booking->setVisibilityAll();
        }

        $currencyGuest = $booking->user()->first()->currency;
        $currencyCaptain = $booking->currency;

        $bookingAttributes = $booking->toArray();
        $seats = $bookingAttributes['adults'] + $bookingAttributes['kids'] + $bookingAttributes['babies'];
        $bookingAttributes['status'] = Constant::BOOKING_STATUS_NAMES[$bookingAttributes['status']];
        $bookingAttributes['price_per_person'] = ceil(($bookingAttributes['price']) / $seats);
		$bookingAttributes['price_per_person_without_services'] = ceil(($bookingAttributes['price'] - $this->servicesPrice) / $seats);
		$bookingAttributes['price_minus_discount'] = $bookingAttributes['price'] - $bookingAttributes['discount'];
		$bookingAttributes['services_price'] = $this->servicesPrice;
		$bookingAttributes['price_to_pay_guest'] = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest,  $bookingAttributes['price_to_pay']));
		$bookingAttributes['price_on_board_guest'] = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest, $bookingAttributes['price_on_board']));
		$bookingAttributes['price_minus_discount_guest'] = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest, $bookingAttributes['price_minus_discount']));

        return $bookingAttributes;
    }

    public function setServicesPrice($servicesPrice)
    {
        $this->servicesPrice = $servicesPrice;
    }
}