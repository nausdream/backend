<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class ExperienceTypeTransformer extends Transformer
{
    private $type = 'experience_types';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($experienceType, $isAdmin = false)
    {
        if ($isAdmin) {
            $experienceType->setVisibilityAll();
        }

        return $experienceType->toArray();
    }
}