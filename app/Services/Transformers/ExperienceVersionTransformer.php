<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\FishingPartition;
use App\Models\FishingType;
use App\Services\LanguageHelper;
use App\Services\StatusHelper;
use App\Services\ValueKeyHelper;

class ExperienceVersionTransformer extends Transformer
{
    private $type = 'experiences';
    private $editing = false;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($experienceVersion, $isAdmin = false)
    {
        // Fetch experience and captain
        $experience = $experienceVersion->experience()->first();
        $captain = $experience->boat()->first()->user()->first();

        // If in editing mode, use user language
        if ($this->editing) {
            $language = $captain
                ->language;
            $this->setLanguage($language);
        }

        if ($isAdmin) {
            $experienceVersion->setVisibilityAll();
        }

        $experienceVersionArray = $experienceVersion->toArray();
        $this->setAdditionalFields($experienceVersion, $experienceVersionArray);

        // Transform status
        $experienceVersionArray['status'] = ValueKeyHelper::getValueByKey(
            $experienceVersion->status,
            Constant::STATUS_NAMES
        );
        $experienceVersionArray['type'] = ExperienceType::find($experienceVersionArray['type'])->name ?? null;
        $experienceVersionArray['fishing_type'] = FishingType::find($experienceVersionArray['fishing_type'])->name ?? null;
        $experienceVersionArray['fishing_partition'] = FishingPartition::find($experienceVersionArray['fishing_partition'])->name ?? null;
        $experienceVersionArray['captain_first_name'] = $captain->first_name;

        // Removes boat_id from boatVersion array
        unset($experienceVersionArray['experience_id']);

        return $experienceVersionArray;
    }

    public function getId($experienceVersion)
    {
        return $experienceVersion->experience()->first()->id;
    }

    private function setAdditionalFields(ExperienceVersion $experienceVersion, array &$attributes)
    {
        $priorities = LanguageHelper::getPriorities();

        // SEO
        $seos = $experienceVersion->seos()->get();

        if ($this->editing) {
            // SEO
            // Try fetching rules in user language
            $seo = $seos
                ->where('language', $this->language)
                ->first();
        }
        else {
            if (isset($this->language)) {
                $key = array_search($this->language, $priorities);

                //if language was found in array, put user language on top of priorities
                if ($key != false) {
                    unset($priorities[$key]);
                    array_unshift($priorities, $this->language);
                }
            }
        }

        // If there isn't any, fetch in priorities order
        foreach ($priorities as $language) {

            if (!isset($seo)) {
                $seo = $seos->where('language', $language)->first();
            }
        }

        // Set attributes on experienceVersion
        $attributes['slug_url'] = isset($seo) ? $seo->slug_url : null;
        $attributes['seo_title'] = isset($seo) ? $seo->title : null;
        $attributes['seo_description'] = isset($seo) ? $seo->description : null;
    }

    public function setEditingMode(bool $isEditing)
    {
        $this->editing = $isEditing;
    }
}