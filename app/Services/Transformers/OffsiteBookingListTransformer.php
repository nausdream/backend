<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\FishingPartition;
use App\Models\FishingType;
use App\Services\LanguageHelper;
use App\Services\StatusHelper;
use App\Services\ValueKeyHelper;

class OffsiteBookingListTransformer extends Transformer
{
    private $type = 'offsite-bookings';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($offsiteBooking, $isAdmin = false)
    {
        // Fetch experience and captain
        $guest = $offsiteBooking->user()->first();

        if ($isAdmin) {
            $offsiteBooking->setVisibilityAll();
        }

        $offsiteBookingArray = [];

        // Transform status
        $offsiteBookingArray['status'] = ValueKeyHelper::getValueByKey(
            $offsiteBooking->status,
            Constant::OFFSITE_BOOKING_STATUS_NAMES
        );
        $offsiteBookingArray['experience_date'] = $offsiteBooking->experience_date;
        $offsiteBookingArray['experience_title'] = $offsiteBooking->experience_title;
        $offsiteBookingArray['first_name_captain'] = $offsiteBooking->first_name_captain;
        $offsiteBookingArray['last_name_captain'] = $offsiteBooking->last_name_captain;
        $offsiteBookingArray['first_name_guest'] = $guest->first_name;
        $offsiteBookingArray['last_name_guest'] = $guest->last_name;

        return $offsiteBookingArray;
    }
}