<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Services\PhotoHelper;
use JD\Cloudder\Facades\Cloudder;

class PhotoTransformer extends Transformer
{
    private $type = 'photos';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($photo, $isAdmin = false, $device = null)
    {
        if ($isAdmin) {
            $photo->setVisibilityAll();
        }

        $url = PhotoHelper::getExperiencePhotoLink($photo, $device);

        return [
            'url' => $url,
            'is_cover' => $photo->is_cover
        ];
    }
}