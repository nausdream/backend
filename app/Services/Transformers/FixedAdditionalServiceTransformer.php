<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class FixedAdditionalServiceTransformer extends Transformer
{
    private $type = 'fixed-additional-services';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($additionalService, $isAdmin = false)
    {
        if ($isAdmin) {
            $additionalService->setVisibilityAll();
        }

        return $additionalService->toArray();
    }
}