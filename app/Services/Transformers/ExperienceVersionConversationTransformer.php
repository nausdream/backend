<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\FishingPartition;
use App\Models\FishingType;
use App\Services\LanguageHelper;
use App\Services\PhotoHelper;
use App\Services\StatusHelper;

class ExperienceVersionConversationTransformer extends Transformer
{
    private $type = 'experiences';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($experienceVersion, $isAdmin = false)
    {
        if ($isAdmin) {
            $experienceVersion->setVisibilityAll();
        }

        $experienceVersionArray = [];
        $experienceVersionArray['type'] = isset($experienceVersion->type) ? ExperienceType::find($experienceVersion->type)->name : null;
        $experienceVersionArray['cover_photo'] = PhotoHelper::getExperienceCoverPhotoData($experienceVersion);

        return $experienceVersionArray;
    }

    public function getId($experienceVersion)
    {
        return $experienceVersion->experience()->first()->id;
    }
}