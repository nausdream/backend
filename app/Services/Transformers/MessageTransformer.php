<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class MessageTransformer extends Transformer
{
    private $type = 'messages';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($message, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $message->setVisibilityAll();
        }
        $attributes = [ 'message' => $message->message ];
        $attributes['created_at'] = $message->created_at->format('Y-m-d H:i:s');

        return $attributes;
    }
}