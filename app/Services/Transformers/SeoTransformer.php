<?php
/**
 * User: Giuseppe
 * Date: 24/01/2017
 * Time: 11:36
 */

namespace App\Services\Transformers;


use App\Services\SeoHelper;

class SeoTransformer extends Transformer
{
    private $type = 'seos';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($seo, $isAdmin = false)
    {
        if ($isAdmin) {
            $seo->setVisibilityAll();
        }

        $attributes = $seo->toArray();

        if (!$isAdmin) {
            $attributes['title'] = SeoHelper::getMetaTitleForPublicSite($attributes['title']);
        }

        return $attributes;
    }
}