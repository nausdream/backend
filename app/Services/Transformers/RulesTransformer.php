<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class RulesTransformer extends Transformer
{
    private $type = 'rules';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($rule, $isAdmin = false)
    {
        if ($isAdmin) {
            $rule->setVisibilityAll();
        }

        return $rule->toArray();
    }
}