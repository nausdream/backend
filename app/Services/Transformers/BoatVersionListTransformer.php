<?php
/**
 * User: Giuseppe Basciu
 * Date: 02/02/2017
 * Time: 16:34
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\BoatMaterial;
use App\Models\BoatType;
use App\Models\BoatVersion;
use App\Models\MotorType;
use App\Services\LanguageHelper;
use App\Services\PhotoHelper;
use App\Services\ValueKeyHelper;
use Carbon\Carbon;

class BoatVersionListTransformer extends Transformer
{
    private $type = 'boats';
    private $editing = false;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($boatVersion, $isAdmin = false, string $deviceName = null)
    {
        if ($isAdmin) {
            $boatVersion->setVisibilityAll();
        }

        $boatVersionArray = $boatVersion->toArray();

        // Transform status
        $boatVersionArray['status'] = ValueKeyHelper::getValueByKey($boatVersion->status, Constant::STATUS_NAMES);
        $boatVersionArray['type'] = BoatType::find($boatVersionArray['type'])->name ?? null;
        $boatVersionArray['material'] = BoatMaterial::find($boatVersionArray['material'])->name ?? null;
        $boatVersionArray['motor_type'] = MotorType::find($boatVersionArray['motor_type'])->name ?? null;

        // Add photos
        $boatVersionArray['external_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'external', $deviceName);
        $boatVersionArray['internal_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'internal', $deviceName);
        $boatVersionArray['sea_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'sea', $deviceName);

        // Add created_at date
        $boatVersionArray['created_at'] = $boatVersion->created_at->format('Y-m-d');

        // Add captain first name and last name
        $captain = $boatVersion->boat()->first()->user()->first();
        $boatVersionArray['captain_first_name'] = $captain->first_name;
        $boatVersionArray['captain_last_name'] = $captain->last_name;

        // Removes boat_id from boatVersion array
        unset($boatVersionArray['boat_id']);

        // Add boat attributes

        $boat = $boatVersion->boat()->first();
        $boatVersionArray['is_luxury'] = $boat->is_luxury;
        $boatVersionArray['fee'] = $boat->fee;

        return $boatVersionArray;
    }

    public function getId($boatVersion)
    {
        return $boatVersion->boat()->first()->id;
    }

    public function setEditingMode(bool $isEditing)
    {
        $this->editing = $isEditing;
    }
}