<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 15:20
 */

namespace App\Services\Transformers;


class TranslatorTransformer extends Transformer
{
    private $type = 'translators';

    /**
     * Get correct attribute for translator
     *
     * @param $translator
     * @return array
     */
    public function transform($translator)
    {
        return $translator->toArray();

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}