<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class FeedbackTransformer extends Transformer
{
    private $type = 'availabilities';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($availability, $isAdmin = false)
    {
        if ($isAdmin) {
            $availability->setVisibilityAll();
        }

        return $availability->toArray();
    }
}