<?php
/**
 * User: Giuseppe
 * Date: 26/12/2016
 * Time: 16:17
 */

namespace App\Services\Transformers;

use App\Services\PhotoHelper;

class UserListTransformer extends Transformer
{
    private $type = 'users';

    /**
     * Get correct attribute for user
     *
     * @param $user
     * @param string $deviceName
     * @return array
     */
    public function transform($user, string $deviceName = null)
    {
        $userArray = [];

        $userArray["first_name"] = $user->first_name;
		$userArray["last_name"] = $user->last_name;
		$userArray["email"] = $user->email;
		$userArray["created_at"] = $user->created_at->toDateString();
		$userArray["enterprise_city"] = $user->enterprise_city;
		$userArray["enterprise_zip_code"] = $user->enterprise_zip_code;
        $userArray["is_captain"] = $user->is_captain;
        $userArray["is_partner"] = $user->is_partner;
        $userArray['profile_photo'] = PhotoHelper::getProfilePhotoLink($user, $deviceName);

        return $userArray;

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}