<?php
/**
 * User: Giuseppe Basciu
 * Date: 11/07/2017
 * Time: 10:49
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\BoatType;
use App\Services\ValueKeyHelper;

class OffsiteBookingsTransformer extends Transformer
{
    private $type = 'offsite-bookings';

    public function transform($offsiteBooking, bool $isAdmin = false)
    {
        // Get relationship

        $guest = $offsiteBooking->user()->first();

        // Get attributes

        $attributes = $offsiteBooking->toArray();

        $attributes['first_name_guest'] = $guest->first_name;
        $attributes['last_name_guest'] = $guest->last_name;
        $attributes['address_guest'] = $guest->enterprise_address;
        $attributes['city_guest'] = $guest->enterprise_city;
        $attributes['phone_guest'] = $guest->phone;
        $attributes['email_guest'] = $guest->email;
        $attributes['vat_guest'] = $guest->vat_number;
        $attributes['language_guest'] = $guest->language;
        $attributes['currency_guest'] = $guest->currency;
        $attributes['status'] = ValueKeyHelper::getValueByKey($offsiteBooking->status, Constant::OFFSITE_BOOKING_STATUS_NAMES);
        $attributes['created_at'] = $offsiteBooking->created_at->format('Y-m-d');
        $attributes['language_captain'] = $offsiteBooking->language;
        $attributes['currency_captain'] = $offsiteBooking->currency;
        $attributes['boat_type'] = BoatType::find($offsiteBooking->boat_type)->name ?? null;

        return $attributes;
    }

    /**
     * Get the type of the resource used by transformer
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}