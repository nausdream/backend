<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\Administrator;
use App\Models\Booking;
use Carbon\Carbon;

class BookingListTransformer extends Transformer
{
    private $type = 'bookings';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($booking)
    {
        $experienceVersion = $booking->experienceVersion()->first();
        $captain = $experienceVersion->experience()->first()->boat()->first()->user()->first();
        $guest = $booking->user()->first();

        $bookingArray = [];
        $bookingArray['experience_date'] = $booking->experience_date;
        $bookingArray['experience_title'] = $experienceVersion->title;
        $bookingArray['first_name_captain'] = $captain->first_name;
        $bookingArray['last_name_captain'] = $captain->last_name;
        $bookingArray['first_name_guest'] = $guest->first_name;
        $bookingArray['last_name_guest'] = $guest->last_name;
        $bookingArray['status'] = Constant::BOOKING_STATUS_NAMES[$booking->status];

        return $bookingArray;
    }
}