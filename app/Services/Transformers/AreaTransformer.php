<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class AreaTransformer extends Transformer
{
    private $type = 'areas';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($area, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $area->setVisibilityAll();
        }

        return $area->toArray();
    }
}