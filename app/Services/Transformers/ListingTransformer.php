<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\FishingPartition;
use App\Models\FishingType;
use App\Services\LanguageHelper;
use App\Services\ValueKeyHelper;

class ListingTransformer extends Transformer
{
    private $type = 'listings';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($listing, $isAdmin = false)
    {
        $array = $listing->toArray();
        $array['type'] = ExperienceType::find($array['type'])->name ?? null;
        return $array;
    }
}