<?php
/**
 * User: Giuseppe
 * Date: 08/11/2016
 * Time: 11:37
 */

namespace App\Services\Transformers;


class LanguageTransformer extends Transformer
{
    private $type = 'languages';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($language)
    {
        return $language->toArray();
    }
}