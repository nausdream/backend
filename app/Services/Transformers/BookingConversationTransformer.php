<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Services\ValueKeyHelper;

class BookingConversationTransformer extends Transformer
{
    private $type = 'bookings';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($booking, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $booking->setVisibilityAll();
        }
        $attributes = $booking->toArray();
        $attributes['status'] = ValueKeyHelper::getValueByKey($booking->status, Constant::BOOKING_STATUS_NAMES);

        // Hide fields
        unset($attributes['feedback']);
        unset($attributes['is_feedback_approved']);
        unset($attributes['partner_fee']);

        return $attributes;
    }
}