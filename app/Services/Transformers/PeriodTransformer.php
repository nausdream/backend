<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Services\PriceHelper;

class PeriodTransformer extends Transformer
{
    private $type = 'periods';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($period, $isAdmin = false, $currency = null)
    {
        if ($isAdmin) {
            $period->setVisibilityAll();
        }

        $attributes = $period->toArray();
        if (isset($currency)) {
            $attributes['price'] = PriceHelper::convertPrice($attributes['currency'], $currency, $attributes['price']);
            $attributes['currency'] = $currency;
        }

        return $attributes;
    }
}