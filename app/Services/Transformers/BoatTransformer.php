<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class BoatTransformer extends Transformer
{
    private $type = 'boats';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($boat, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $boat->setVisibilityAll();
        }

        return $boat->toArray();
    }
}