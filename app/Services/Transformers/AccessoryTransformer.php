<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class AccessoryTransformer extends Transformer
{
    private $type = 'accessories';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($accessory, $isAdmin = false)
    {
        if ($isAdmin) {
            $accessory->setVisibilityAll();
        }

        return $accessory->toArray();
    }
}