<?php
/**
 * User: Giuseppe
 * Date: 02/12/2016
 * Time: 12:15
 */

namespace App\Services\Transformers;


class PageTransformer extends Transformer
{
    private $type = 'pages';

    /**
     * Get correct attribute for page
     *
     * @param $page
     * @return array
     */
    public function transform($page)
    {
        $pageArray = $page->toArray();

        $pageArray['created_at'] = $page->created_at->toDateString();

        return $pageArray;
    }

    /**
     * Get type of resource
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}