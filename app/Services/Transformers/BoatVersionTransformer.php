<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\BoatMaterial;
use App\Models\BoatType;
use App\Models\BoatVersion;
use App\Models\MotorType;
use App\Services\LanguageHelper;
use App\Services\PhotoHelper;
use App\Services\ValueKeyHelper;

class BoatVersionTransformer extends Transformer
{
    private $type = 'boats';
    private $editing = false;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($boatVersion, $isAdmin = false, string $deviceName = null)
    {
        if ($isAdmin) {
            $boatVersion->setVisibilityAll();
        }

        $boatVersionArray = $boatVersion->toArray();

        // Transform status
        $boatVersionArray['status'] = ValueKeyHelper::getValueByKey($boatVersion->status, Constant::STATUS_NAMES);
        $boatVersionArray['type'] = BoatType::find($boatVersionArray['type'])->name ?? null;
        $boatVersionArray['material'] = BoatMaterial::find($boatVersionArray['material'])->name ?? null;
        $boatVersionArray['motor_type'] = MotorType::find($boatVersionArray['motor_type'])->name ?? null;

        // Add photos
        $boatVersionArray['external_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'external', $deviceName);
        $boatVersionArray['internal_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'internal', $deviceName);
        $boatVersionArray['sea_photo'] = PhotoHelper::getBoatPhotoLink($boatVersion, 'sea', $deviceName);

        // Removes boat_id from boatVersion array
        unset($boatVersionArray['boat_id']);

        // Add boat attributes

        $boat = $boatVersion->boat()->first();
        $boatVersionArray['is_luxury'] = $boat->is_luxury;
        $boatVersionArray['fee'] = $boat->fee;
        $boatVersionArray['created_at'] = $boat->created_at->format('Y-m-d H:i:s');

        return $boatVersionArray;
    }

    public function getId($boatVersion)
    {
        return $boatVersion->boat()->first()->id;
    }

    public function setEditingMode(bool $isEditing)
    {
        $this->editing = $isEditing;
    }
}