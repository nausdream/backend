<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 14:07
 */

namespace App\Services\Transformers;


class AdministratorTransformer extends Transformer
{
    private $type = 'administrators';

    /**
     * Get correct attribute for user
     *
     * @param $user
     * @param bool $isAdmin
     * @return array
     */
    public function transform($user, $isAdmin = false)
    {
        // Check if the user is an admin for visibility

        if ($isAdmin) {
            $user->setVisibilityAll();
        }

        return $user->toArray();

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}