<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class FixedRulesTransformer extends Transformer
{
    private $type = 'fixed-rules';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($rule, $isAdmin = false)
    {
        if ($isAdmin) {
            $rule->setVisibilityAll();
        }

        return $rule->toArray();
    }
}