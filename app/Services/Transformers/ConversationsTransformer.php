<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

use App\Constant;
use App\Models\Administrator;
use App\Models\User;
use App\Services\PhotoHelper;
use App\Services\ValueKeyHelper;

class ConversationsTransformer extends Transformer
{
    private $type = 'conversations';
    private $mode = 'single';
    private $user;

    public function transform($conversation, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $conversation->setVisibilityAll();
        }

        // Fetch relationships object
        $guest = $conversation->user()->first();
        $captain = $conversation->captain()->first();

        $attributes = [];

        /** TODO: show last names just when it is an admin */

        $attributes['profile_image_guest'] = PhotoHelper::getProfilePhotoLink($conversation->user()->first());
        $attributes['first_name_guest'] = $guest->first_name;
        $attributes['last_name_guest'] = $guest->last_name;
        $attributes['profile_image_captain'] = PhotoHelper::getProfilePhotoLink($conversation->captain()->first());
        $attributes['first_name_captain'] = $captain->first_name;
        $attributes['last_name_captain'] = $captain->last_name;
        $attributes['total_messages'] = $conversation->messages()->get()->count();
        $attributes['unread_messages_guest'] = $conversation->messages()->where('user_id', '!=', $guest->id)->where('read', false)->get()->count();
        $attributes['unread_messages_captain'] = $conversation->messages()->where('user_id', '!=', $captain->id)->where('read', false)->get()->count();


        // Set the role of who is requiring the conversation
        if ($this->user instanceof User) {
            if ($this->user->id == $guest->id) {
                $attributes['role'] = 'guest';
            }
            if ($this->user->id == $captain->id) {
                $attributes['role'] = 'captain';
            }
        }
        if ($this->user instanceof Administrator) {
            $attributes['role'] = 'admin';
        }

        // Set different attributes for single and list view
        if ($this->mode == 'list') {
            $lastMessage = $conversation->messages()->orderBy('created_at', 'desc')->first();
            $attributes['last_message_text'] = isset($lastMessage) ? $lastMessage->message : null;
            $attributes['last_message_date_time'] = $conversation->updated_at->format('Y-m-d H:i:s');
            $attributes['alert'] = $conversation->alert;
            $attributes['status'] = ValueKeyHelper::getValueByKey($conversation->bookings()->get()->count() > 0 ? $conversation->bookings()->get()->last()->status : null, Constant::BOOKING_STATUS_NAMES) ?? Constant::CONVERSATION_STATUS_NEW;
        }

        if ($this->mode == 'single') {
            $attributes['created_at'] = $conversation->created_at->format('Y-m-d');
            $attributes['request_date'] = $conversation->request_date;
            $attributes['adults'] = $conversation->adults;
            $attributes['kids'] = $conversation->kids;
            $attributes['babies'] = $conversation->babies;
        }

        return $attributes;
    }

    /**
     * Get the type of the resource used by transformer
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @param $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}