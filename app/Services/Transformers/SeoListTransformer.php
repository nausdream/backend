<?php
/**
 * User: Giuseppe
 * Date: 24/01/2017
 * Time: 11:36
 */

namespace App\Services\Transformers;


use App\Models\Seo;
use App\Services\SeoHelper;

class SeoListTransformer extends Transformer
{
    private $type = 'seos';
    private $languageRequest;
    private $translated;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }


    /**
     * @param $seo
     * @param $isAdmin
     * @return array
     */
    public function transform($seo, $isAdmin = false)
    {
        if ($isAdmin) {
            $seo->setVisibilityAll();
        }

        $attributes = $seo->toArray();

        if (isset($translated)) {
            $attributes['translated'] = $this->translated;
        } else {
            // Check if exist translation
            $seoTranslated = Seo::all()->where('experience_version_id', $seo->experience_version_id)->where('language', $this->languageRequest)->first();

            if (isset($seoTranslated)) {
                $attributes['translated'] = true;
            } else {
                $attributes['translated'] = false;
            }
        }

        return $attributes;
    }

    /**
     * @param $language
     */
    public function setRequestLanguage($languageRequest)
    {
       $this->languageRequest = $languageRequest;
    }

    /**
     * @param $translated
     */
    public function setTranslated($translated)
    {
        $this->translated = $translated;
    }
}