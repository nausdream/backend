<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class PriceTransformer extends Transformer
{
    private $type = 'prices';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($price, $isAdmin = false)
    {
        if ($isAdmin) {
            $price->setVisibilityAll();
        }

        return $price->toArray();
    }
}