<?php
/**
 * User: Giuseppe
 * Date: 08/11/2016
 * Time: 11:34
 */

namespace App\Services\Transformers;

use App\Models\CaptainType;
use App\Services\PhotoHelper;

class UserTransformer extends Transformer
{
    private $type = 'users';

    /**
     * Get correct attribute for user
     *
     * @param $user
     * @param bool $isAdmin
     * @param string $deviceName
     * @return array
     */
    public function transform($user, $isAdmin = false, string $deviceName = null)
    {
        // Check if the user is an admin for visibility

        if ($isAdmin) {
            $user->setVisibilityAll();
        }

        $userArray = $user->toArray();

        // Set user profile photo link
        $userArray['captain_type'] = CaptainType::find($userArray['captain_type'])->name ?? null;
        $userArray['profile_photo'] = PhotoHelper::getProfilePhotoLink($user, $deviceName);

        return $userArray;

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}