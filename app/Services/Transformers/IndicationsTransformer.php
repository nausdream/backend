<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class IndicationsTransformer extends Transformer
{
    private $type = 'indications';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function transform($indication, $isAdmin = false)
    {
        if ($isAdmin) {
            $indication->setVisibilityAll();
        }

        return $indication->toArray();
    }
}