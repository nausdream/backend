<?php
/**
 * User: Luca Puddu
 * Date: 08/11/2016
 * Time: 10:32
 */

namespace App\Services\Transformers;

class CouponsTransformer extends Transformer
{
    private $type = 'coupons';
    private $canSeeDeleted = false;

    public function transform($coupon, bool $isAdmin = false)
    {
        if ($isAdmin) {
            $coupon->setVisibilityAll();
        }

        if ($this->canSeeDeleted) {
            $coupon->setHidden([
                'id', 'area_id', 'experience_id',
                'created_at', 'updated_at'
            ]);
        }

        $attributes = $coupon->toArray();

        if (isset($coupon->deleted_at)) {
            $couponNameExploded = explode('_', $coupon->name);
            if (count($couponNameExploded) > 2) {
                array_pop($couponNameExploded);
                array_pop($couponNameExploded);
            }
            $attributes['name'] = implode('_', $couponNameExploded);
        }


        return $attributes;
    }

    /**
     * Get the type of the resource used by transformer
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function setSeeDeleted(bool $canSeeDeleted = true)
    {
        $this->canSeeDeleted = $canSeeDeleted;
    }
}