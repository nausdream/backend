<?php
/**
 * User: Giuseppe
 * Date: 29/11/2016
 * Time: 22:58
 */

namespace App\Services;

use App\Models\Device;
use App\Models\BoatVersion;
use App\Models\ExperiencePhoto;
use App\Models\ExperienceVersion;
use App\Models\User;
use JD\Cloudder\Facades\Cloudder;

class PhotoHelper
{
    /**
     * Get profile photo link by user profile
     *
     * @param User $user
     * @param string $deviceName
     * @return string
     */
    public static function getProfilePhotoLink(User $user, string $deviceName = null)
    {
        $device = self::getDeviceByName($deviceName);
        $photoVersion = $user->photo_version;
        if (!is_null($photoVersion)) {
            $publicId = self::getProfilePhotoPublicId($user->id);
            return Cloudder::secureShow($publicId, [
                'version' => $photoVersion,
                'format' => 'jpg',
                'width' => $device->width,
                'height' => $device->height,
                'crop' => 'limit',
                'angle' => 'exif'
            ]);
        }
        $fuid = $user->fuid;
        if (!is_null($fuid)) {
            return self::getFacebookProfilePhoto($fuid);
        }
        return self::getNoProfilePhoto();
    }

    /**
     * Get profile photo public id
     *
     * @param $id
     * @return string
     */
    public static function getProfilePhotoPublicId($id)
    {
        return env('CLOUDINARY_BASE_FOLDER') . 'users/' . $id . '/profile';
    }

    /**
     * Get facebook profile photo
     *
     * @param $fuid
     * @return string
     */
    public static function getFacebookProfilePhoto($fuid)
    {
        return 'https://graph.facebook.com/' . $fuid . '/picture?type=large';
    }

    /**
     * Get link to avatar profile photo
     *
     * @return mixed
     */
    public static function getNoProfilePhoto()
    {
        return null;
    }

    /**
     * Get photo link by boat and photo type
     *
     * @param BoatVersion $boatVersion
     * @param string $type
     * @param string $deviceName
     * @return mixed
     */
    public static function getBoatPhotoLink(BoatVersion $boatVersion, string $type, string $deviceName = null)
    {
        $device = self::getDeviceByName($deviceName);
        switch ($type) {
            case 'external':
                $photoVersion = $boatVersion->version_external_photo;
                break;
            case 'internal':
                $photoVersion = $boatVersion->version_internal_photo;
                break;
            case 'sea':
                $photoVersion = $boatVersion->version_sea_photo;
                break;
            default:
                $photoVersion = $boatVersion->version_external_photo;
        }
        if (!is_null($photoVersion)) {
            $publicId = self::getBoatPhotoPublicId($boatVersion->boat->user->id, $boatVersion->boat->id, $type);
            return Cloudder::secureShow($publicId, [
                'version' => $photoVersion,
                'format' => 'jpg',
                'width' => $device->width,
                'height' => $device->height,
                'crop' => 'limit',
                'angle' => 'exif'
            ]);
        }
        return null;
    }

    /**
     * Get boat photo public id
     *
     * @param $userId
     * @param $boatId
     * @param string $type
     * @return string
     */
    public static function getBoatPhotoPublicId($userId, $boatId, string $type)
    {
        return env('CLOUDINARY_BASE_FOLDER')
        . 'users/'
        . $userId . "/"
        . $boatId . "/"
        . $type;
    }

    /**
     * Get photo link by photo
     *
     * @param ExperiencePhoto $photo
     * @param string $deviceName
     * @return mixed
     */
    public static function getExperiencePhotoLink(ExperiencePhoto $photo, string $deviceName = null)
    {
        $device = self::getDeviceByName($deviceName);
        $publicId = self::getExperiencePhotoPublicId($photo->experienceVersion->experience->boat->user->id,
            $photo->experienceVersion->experience->boat->id,
            $photo->experienceVersion->experience->id,
            $photo->id);
        return Cloudder::secureShow($publicId, [
            'format' => 'jpg',
            'width' => $device->width,
            'height' => $device->height,
            'crop' => 'limit',
            'angle' => 'exif'
        ]);
    }

    /**
     * Get boat photo public id
     *
     * @param $userId
     * @param $boatId
     * @param $experienceId
     * @param $photoId
     * @return string
     */
    public static function getExperiencePhotoPublicId($userId, $boatId, $experienceId, $photoId)
    {
        return env('CLOUDINARY_BASE_FOLDER') . 'users/'
        . $userId . "/"
        . $boatId . "/"
        . $experienceId . "/"
        . $photoId;
    }

    /**
     * Get arrays of photo link by experienceVersion
     *
     * @param ExperienceVersion $experienceVersion
     * @param string $deviceName
     * @return array
     */
    public static function getExperiencePhotosData(ExperienceVersion $experienceVersion, string $deviceName = null)
    {
        $array = [];
        $photos = $experienceVersion->experienceVersionPhotos()->orderBy('is_cover', 'desc')->get();
        $userId = $experienceVersion->experience->boat->user->id;
        $boatId = $experienceVersion->experience->boat->id;
        $experienceId = $experienceVersion->experience->id;
        $device = self::getDeviceByName($deviceName);
        foreach ($photos as $photo) {
            $publicId = $publicId = self::getExperiencePhotoPublicId($userId, $boatId, $experienceId, $photo->id);
            $array[] = JsonHelper::createData('photos', $photo->id, [
                    'link' => Cloudder::secureShow($publicId, [
                        'format' => 'jpg',
                        'width' => $device->width,
                        'height' => $device->height,
                        'crop' => 'limit',
                        'angle' => 'exif'
                    ]),
                    'is_cover' => $photo->is_cover
                ]
            );
        }
        return $array;
    }

    /**
     * Get cover photo link by experienceVersion
     *
     * @param ExperienceVersion $experienceVersion
     * @param string $deviceName
     * @return string
     */
    public static function getExperienceCoverPhotoData(ExperienceVersion $experienceVersion, string $deviceName = null)
    {
        $photo = $experienceVersion->experienceVersionPhotos()->where('is_cover', true)->get()->last();
        if (isset($photo)) {
            $userId = $experienceVersion->experience->boat->user->id;
            $boatId = $experienceVersion->experience->boat->id;
            $experienceId = $experienceVersion->experience->id;
            $device = self::getDeviceByName($deviceName);
            $publicId = $publicId = self::getExperiencePhotoPublicId($userId, $boatId, $experienceId, $photo->id);
            $link = Cloudder::secureShow($publicId, [
                'format' => 'jpg',
                'width' => $device->width,
                'height' => $device->height,
                'crop' => 'limit',
                'angle' => 'exif'
            ]);
            return $link;
        }
        return null;
    }

    /**
     * Get device model by device name
     *
     * @param string $deviceName
     * @return Device
     */
    public static function getDeviceByName(string $deviceName = null)
    {
        if (isset($deviceName) && !is_null($deviceName)) {
            $device = Device::where('name', $deviceName)->get()->last();
            if (isset($device)) {
                return $device;
            }
        }
        return Device::where('name', env('DEFAULT_DEVICE'))->get()->last();
    }
}