<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 12:51
 */

namespace App\Interfaces;
use App\Services\Transformers\Transformer;

interface Transformable
{
    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer() : Transformer;
}