<?php

namespace App\Console\Commands;

use App\Http\Controllers\v1\SeosController;
use App\Models\AdditionalService;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceAvailability;
use App\Models\Period;
use App\Models\Price;
use App\Models\User;
use App\Services\PhotoHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Storage;
use JD\Cloudder\Facades\Cloudder;

class MigrateOldSiteInvalidContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:content-invalid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transfer contents from old site to server db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $stampUsers = [];
        $oldBoats = 0;
        $oldExperiences = 0;
        $errors = [];
        $results = [];
        $log = [];

        try {
            $conn = 'mysql_old_site';

            // Get possible spoken languages ids
            $languages = [];
            $languageNames = ['it', 'en', 'fr', 'es', 'de', 'ru', 'zh', 'hi', 'ar', 'pt'];
            foreach ($languageNames as $languageName) {
                $languages[$languageName] = \App\Models\Language::all()->where('language', $languageName)->first();
            }

            // Set conversion array for accessories
            $accessoriesName = [
                'cucina' => 'kitchen',
                'radio' => 'radio',
                'ecoscandaglio' => 'echo-sounder',
                'attrezzatura_sportiva' => 'sports-gear',
                'tendalino' => 'awning',
                'doccia_esterna' => 'outdoor-shower',
                'microonde' => 'microwave',
                'tender' => 'tender',
                'tv' => 'tv',
                'frigo' => 'fridge',
                'passerella' => 'gangway',
                'prese' => 'wall-socket',
                'utensili_cucina' => 'kitchen-tools',
                'estintore' => 'fire-extinguisher',
                'epirb' => 'epirb',
                'vhf' => 'vhf',
                'riscaldamento' => 'heating',
                'dotazioni_sicurezza' => 'security-equipment',
                'freezer' => 'freezer',
                'generator' => 'power-unit',
                'fire_saving' => 'fire-prevention-system'
            ];

            // Set conversion array for experience type name
            $experienceTypesName = [
                'escursioni' => 'trips-and-tours',
                'aperitivi' => 'aperitif',
                'cene' => 'dinners',
                'pesca' => 'fishing',
                'sport' => 'other',
                'notti' => 'nights',
            ];

            // Set conversion array for experience type
            $experienceTypes = [
                1 => 'trips-and-tours',
                2 => 'aperitif',
                3 => 'fishing',
                4 => 'dinners',
                5 => 'nights',
                6 => 'other',
                7 => 'romantic'
            ];

            // Helper model
            $breakfast = \App\Models\FixedAdditionalService::all()->where('name', 'breakfast')->first();

            // Fetch not already transferred captain, boats and experiences
            $ids = [
                1157, 435, 439, 1181, 274, 1180, 1173, 340, 810, 684, 353, 1079, 194, 1151, 1078, 1059, 1108, 1124,
                1081, 121, 1055, 649, 212, 25, 22, 1082, 589, 1058, 483, 1054, 388, 26, 1260, 395, 815, 377, 1150, 1223,
                80, 1090, 152, 762, 811, 637, 399, 213, 93, 863, 536, 846, 1153, 1018, 1076, 941, 164, 591, 172, 828, 646,
                604, 171, 603, 770, 387, 501, 776, 332, 411, 1172, 425, 407, 51, 1154, 190, 1066, 53, 774, 23, 819, 1214,
                799, 1043, 825, 831, 1155, 428, 862, 170, 113, 24, 1086, 418, 30, 47, 1229, 383, 786, 757, 42, 1071
            ];
            $users = DB::connection($conn)->table('UTENTI')
                ->whereIn('id', $ids)
                ->get();

            $results['old_users'] = $users->count();

            array_push($log, $users->count() . ' users fetched.');

            //================================== USERS ====================================
            foreach ($users as $user) {

                $stampUser = [
                    'user_id' => $user->id,
                    'first_name' => $user->nome,
                    'last_name' => $user->cognome,
                    'email' => $user->email,
                    'attivato' => $user->attivato,
                    'bloccato' => $user->bloccato,
                    'eliminato' => $user->eliminato,
                    'boats' => []
                ];

                // Check if captain, skip element if not
                $boats = DB::connection($conn)->select('select * from BARCHE where id_utente = ?', [$user->id]);

                array_push($log, 'UID ' . $user->id . ', ' . $user->nome . ' ' . $user->cognome);
                $this->writeLog($log);

                if (empty($boats)) {
                    continue;
                }

                $is_captain = true;
                $azienda = $user->azienda;
                switch ($azienda) {
                    case 0:
                        $captain_type = 'private';
                        break;
                    case 1:
                        $captain_type = 'enterprise';
                        break;
                    case 2:
                        $captain_type = 'association';
                        break;
                    default:
                        $captain_type = 'private';
                }
                if (isset($user->citta)) {
                    $docking_place = $user->citta;
                } else {
                    $docking_place = null;
                }

                // If user already exists, only add non existent boats and experiences
                $checkMail = $user->email;
                if (!isset($checkMail)) $checkMail = 'ooooooooooooooo';
                $checkPhone = $user->telefono;
                if (!isset($checkPhone)) $checkPhone = 'ooooooooooooooo';

                $userCollision = DB::table('users')
                    ->where('email', $checkMail)
                    ->orWhere('phone', $checkPhone)->get()->first();

                $collides = isset($userCollision);
                if ($collides) {
                    $userCollision = User::find($userCollision->id);
                    $newUser = $userCollision;

                    array_push($log, 'User already present. Name: ' . $userCollision->first_name . ' ' . $userCollision->last_name
                        . ' Phone: ' . $userCollision->phone
                        . ' Email: ' . $userCollision->email
                        . ' Id: ' . $userCollision->id);
                    $this->writeLog($log);
                }

                if (!$collides) {
                    // Set new user fields
                    $langOld = substr($user->lang, 0, 2);
                    $newUser = new User([
                        'first_name' => $user->nome,
                        'last_name' => $user->cognome,
                        'birth_date' => $user->data_nascita == '0000-00-00' ? null : $user->data_nascita,
                        'captain_type' => $captain_type,
                        'sex' => $user->sesso,
                        'language' => (isset($langOld) && $langOld != '') ? $langOld : \Lang::getFallback(),
                        'description' => $user->descrizione,
                        'enterprise_name' => $user->nome_societa,
                        'enterprise_address' => $user->sede_legale,
                        'vat_number' => $user->partita_iva,
                        'card_price' => $user->card_price,
                        'docking_place' => $docking_place,
                        'skippers_have_license' => isset($user->patente),
                        'newsletter' => false,
                        'has_card' => $user->association_card
                    ]);
                    $newUser->is_captain = $is_captain;
                    $newUser->is_phone_activated = false;
                    $newUser->email = $user->email;
                    $newUser->phone = $user->telefono;
                    $newUser->default_fee = env('DEFAULT_FEE');
                    $newUser->is_mail_activated = $user->mail_active;
                    $newUser->is_partner = false;
                    $newUser->is_suspended = false;
                    $newUser->is_set_password = false;
                    $newUser->currency = isset($user->cur) ? $user->cur : 'EUR';
                    try {
                        $newUser->save();
                    } catch (\Exception $e) {
                        array_push($errors, $e);
                        $newUser->birth_date = null;
                        $newUser->save();
                    }

                    // Spoken languages
                    if ($user->italiano) {
                        $newUser->languages()->attach($languages['it']->id);
                    }
                    if ($user->inglese) {
                        $newUser->languages()->attach($languages['en']->id);
                    }
                    if ($user->francese) {
                        $newUser->languages()->attach($languages['fr']->id);
                    }
                    if ($user->spagnolo) {
                        $newUser->languages()->attach($languages['es']->id);
                    }
                    if ($user->tedesco) {
                        $newUser->languages()->attach($languages['de']->id);
                    }
                    if ($user->russo) {
                        $newUser->languages()->attach($languages['ru']->id);
                    }
                    if ($user->cinese) {
                        $newUser->languages()->attach($languages['zh']->id);
                    }
                    if ($user->hindi) {
                        $newUser->languages()->attach($languages['hi']->id);
                    }
                    if ($user->arabo) {
                        $newUser->languages()->attach($languages['ar']->id);
                    }
                    if ($user->portoghese) {
                        $newUser->languages()->attach($languages['pt']->id);
                    }

                    // Set profile pitcure
                    if (isset($user->foto_version)) {
                        array_push($log, 'Uploading user photo.');
                        $this->writeLog($log);


                        $link = 'https://res.cloudinary.com/naustrip/image/upload/v' . $user->foto_version . '/user_images/' . $user->id . '/foto_profilo.jpg';
                        $url = PhotoHelper::getProfilePhotoPublicId($newUser->id);

                        Cloudder::upload(
                            $link,
                            $url,
                            ["format" => "jpg"]
                        );
                        $arrayResult = Cloudder::getResult();

                        // Update database value

                        $newUser->photo_version = $arrayResult['version'];
                        $newUser->save();
                    }
                }

                //================================== BOATS ====================================
                array_push($log, 'Analyzing '. count($boats) .' boats...');

                foreach ($boats as $boat) {
                    $oldBoats += 1;
                    $stampBoat = ['boat_id' => $boat->id, 'name' => $boat->nome, 'eliminata' => $boat->eliminata, 'bloccata' => $boat->bloccata, 'experiences' => []];
                    $boatPhotos = DB::connection($conn)->select('select * from FOTO_BARCHE where eliminata = 0 and id_barca = ? order by foto_copertina desc', [$boat->id]);

                    array_push($log, 'Boat ID: ' . $boat->id);
                    $this->writeLog($log);

                    // Determine if this boat should be added to user.
                    // Rules: boat should be added if a boat with the same name is not there yet
                    $newBoat = true;

                    if ($collides) {
                        array_push($log, 'Checking user\'s boats for collisions...');
                        $this->writeLog($log);

                        $userCollisionBoats = $userCollision->boats()->get();

                        foreach ($userCollisionBoats as $userCollisionBoat) {

                            $lastBv = $userCollisionBoat->lastFinishedAndAcceptedBoatVersion();
                            if (!isset($lastBv) || $boat->nome == "") {
                                continue;
                            }

                            if ($lastBv->name == $boat->nome) {
                                $newBoat = false;
                                $oldBoat = $userCollisionBoat;

                                array_push($log, $boat->nome . ' already exists.');
                                $this->writeLog($log);

                                break;
                            }
                        }
                    }

                    // Add new boat
                    if ($newBoat) {
                        // Set new boat fields
                        $newBoat = new \App\Models\Boat([
                            'is_luxury' => false,
                            'fee' => env('DEFAULT_FEE'),
                            'user_id' => $newUser->id
                        ]);
                        $newBoat->save();

                        // Set new boatVersion fields
                        switch ($boat->tipo) {
                            case 1:
                                $boat_type = 'sailboat';
                                break;
                            case 2:
                                $boat_type = 'rubberboat';
                                break;
                            case 3:
                                $boat_type = 'motorboat';
                                break;
                            case 4:
                                $boat_type = 'catamaran';
                                break;
                            case 5:
                                $boat_type = 'motorboat';
                                break;
                            default:
                                $boat_type = 'sailboat';
                        }
                        switch ($boat->tipo_motore) {
                            case 1:
                                $motor_type = 'inboard';
                                break;
                            case 2:
                                $motor_type = 'outboard';
                                break;
                            case 3:
                                $motor_type = 'sterndrive';
                                break;
                            default:
                                $motor_type = null;
                        }
                        $newBoatVersion = new \App\Models\BoatVersion([
                            'production_site' => $boat->marca,
                            'model' => $boat->modello,
                            'name' => $boat->nome,
                            'construction_year' => $boat->anno,
                            'restoration_year' => null,
                            'type' => $boat_type,
                            'material' => null,
                            'length' => $boat->lunghezza,
                            'has_insurance' => isset($boat->assicurazione) && $boat->assicurazione != '0000-00-00' ? true : false,
                            'motors_quantity' => $boat->numero_motori,
                            'motor_type' => $motor_type,
                            'docking_place' => $boat->citta,
                            'lat' => $boat->lat,
                            'lng' => $boat->lng,
                            'seats' => $boat->n_posti,
                            'berths' => isset($boat->posti_letto) ? $boat->posti_letto : 0,
                            'cabins' => isset($boat->cabine_ospiti) ? $boat->cabine_ospiti : 0,
                            'toilets' => isset($boat->bagni) ? $boat->bagni : 0,
                            'is_finished' => true,
                            'description' => $boat->descrizione,
                            'rules' => $boat->regole,
                            'indications' => $boat->indicazioni,
                            'events' => $boat->eventi == 1
                        ]);
                        $newBoatVersion->boat_id = $newBoat->id;
                        $newBoatVersion->status = $boat->attivata ? \App\Constant::STATUS_ACCEPTED : \App\Constant::STATUS_PENDING;
                        $newBoatVersion->save();

                        // Boat accessories
                        $accessory_ids = [];
                        foreach ($accessoriesName as $key => $accessoryName) {
                            if ($boat->{$key}) {
                                $accessory_ids[] = \App\Models\Accessory::all()->where('name', $accessoryName)->first()->id;
                            }
                        }
                        $newBoatVersion->accessories()->attach($accessory_ids);
                        $newBoatVersion->save();

                        // Boat experience types
                        $experienceType_ids = [];
                        foreach ($experienceTypesName as $key => $experienceTypeName) {
                            if ($boat->{$key}) {
                                $experienceType_ids[] = \App\Models\ExperienceType::all()->where('name', $experienceTypeName)->first()->id;
                            }
                        }
                        $newBoat->experienceTypes()->attach($experienceType_ids);
                        $newBoat->save();

                        // Boat photos
                        $i = 0;
                        $linkPhotoExternal = null;
                        if (!empty($boatPhotos) && isset($boatPhotos[$i])) {
                            do {
                                $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                $i++;
                            } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                            if (@is_array(getimagesize($link))) {
                                $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'external');
                                $linkPhotoExternal = $link;
                                Cloudder::upload(
                                    $link,
                                    $url,
                                    ["format" => "jpg"]
                                );
                                $arrayResult = Cloudder::getResult();
                                $newBoatVersion->version_external_photo = $arrayResult['version'];

                                if (isset($boatPhotos[$i])) {
                                    do {
                                        $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                        $i++;
                                    } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                                    if (@is_array(getimagesize($link))) {
                                        $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'internal');
                                        Cloudder::upload(
                                            $link,
                                            $url,
                                            ["format" => "jpg"]
                                        );
                                        $arrayResult = Cloudder::getResult();
                                        $newBoatVersion->version_internal_photo = $arrayResult['version'];

                                        if (isset($boatPhotos[$i])) {
                                            do {
                                                $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[$i]->id . '.jpg';
                                                $i++;
                                            } while (!@is_array(getimagesize($link)) && isset($boatPhotos[$i]));
                                            if (@is_array(getimagesize($link))) {
                                                $url = PhotoHelper::getBoatPhotoPublicId($newUser->id, $newBoat->id, 'sea');
                                                Cloudder::upload(
                                                    $link,
                                                    $url,
                                                    ["format" => "jpg"]
                                                );
                                                $arrayResult = Cloudder::getResult();
                                                $newBoatVersion->version_sea_photo = $arrayResult['version'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $newBoatVersion->save();
                    }

                    //================================== EXPERIENCES ====================================
                    $experiences = DB::connection($conn)->select('select * from TRIP where id_barca = ?', [$boat->id]);

                    array_push($log, 'Analyzing '. count($experiences) .' experiences...');
                    $this->writeLog($log);
                    foreach ($experiences as $experience) {

                        $oldExperiences += 1;
                        $experiencePhotos = DB::connection($conn)->select('select * from FOTO_TRIP where eliminata = 0 AND id_trip = ? ORDER BY foto_copertina DESC, id ASC', [$experience->id]);
                        $useBoatPhoto = false;
                        if (!$experiencePhotos) {
                            $useBoatPhoto = true;
                            array_push($log, 'Using boat photo...');
                            $this->writeLog($log);

                        }

                        // Determine if this experience should be added to user.
                        // Rules: experience should be added if boat is new OR experience with the same title for the same boat is not there yet
                        $newExperience = true;

                        if (!$newBoat) {
                            array_push($log, 'Checking boat\'s experiences for collisions');
                            $this->writeLog($log);

                            // Take all boat experiences and check if there's one with the same name
                            $oldBoatExperiences = $oldBoat->experiences()->get();

                            foreach ($oldBoatExperiences as $oldBoatExperience) {
                                $lastEv = $oldBoatExperience->lastFinishedAndAcceptedExperienceVersion();
                                if (!isset($lastEv) || $experience->nome == "") {
                                    continue;
                                }

                                if ($lastEv->title == $experience->nome) {
                                    array_push($log, $experience->nome . ' already exists.');
                                    $this->writeLog($log);

                                    $newExperience = false;
                                    break;
                                }
                            }

                            // We add the experience to an already existing boat
                            if ($newExperience) $newBoat = $oldBoat;
                        }

                        array_push(
                            $stampBoat['experiences'],
                            [
                                'experience_id' => $experience->id,
                                'title' => $experience->nome,
                                'eliminato' => $experience->eliminato,
                                'bloccato' => $experience->bloccato,
                                'photos' => count($experiencePhotos)
                            ]
                        );

                        // Add new experience
                        if ($newExperience) {
                            array_push($log, 'Saving new experience...');
                            $this->writeLog($log);

                            // Set new experience fields
                            $newExperience = new \App\Models\Experience();
                            $newExperience->boat_id = $newBoat->id;
                            $newExperience->save();

                            if (isset($experience->citta_rientro) && $experience->citta_rientro != '') {
                                $citta_rientro = $experience->citta_rientro;
                            } else {
                                $citta_rientro = $experience->citta;
                            }

                            if (isset($experience->lat_rientro) && $experience->lat_rientro != '') {
                                $lat_rientro = $experience->lat_rientro;
                                $lng_rientro = $experience->lng_rientro;
                            } else {
                                $lat_rientro = $experience->lat;
                                $lng_rientro = $experience->lng;
                            }

                            $experienceVersion = new \App\Models\ExperienceVersion([
                                'type' => $experienceTypes[$experience->tipo_uscita],
                                'departure_port' => $experience->citta,
                                'departure_time' => $experience->ora_partenza,
                                'arrival_port' => $citta_rientro,
                                'arrival_time' => $experience->ora_fine,
                                'monday' => $experience->lun,
                                'tuesday' => $experience->mar,
                                'wednesday' => $experience->mer,
                                'thursday' => $experience->gio,
                                'friday' => $experience->ven,
                                'saturday' => $experience->sab,
                                'sunday' => $experience->dom,
                                'is_fixed_price' => true,
                                'seats' => $experience->posti,
                                'kids' => $experience->posti,
                                'babies' => $experience->posti,
                                'departure_lat' => $experience->lat,
                                'departure_lng' => $experience->lng,
                                'description' => mb_substr($experience->descrizione, 0, \App\Constant::EXPERIENCE_DESCRIPTION_LENGTH),
                                'title' => mb_substr($experience->nome, 0, \App\Constant::EXPERIENCE_TITLE_LENGTH),
                                'destination_lat' => $lat_rientro,
                                'destination_lng' => $lng_rientro,
                                'days' => 1,
                                'road_stead' => false,
                                'down_payment' => isset($user->caparra) && $user->caparra,
                                'cancellation_max_days' => env('DEFAULT_CANCELLATION'),
                                'deposit' => 0,
                                'currency' => $experience->cur,
                            ]);
                            $experienceVersion->experience_id = $newExperience->id;
                            $experienceVersion->is_finished = true;
                            $experienceVersion->status = \App\Constant::STATUS_ACCEPTED;
                            $experienceVersion->is_searchable = false;

                            $experienceVersion->save();

                            array_push($log, 'Experience version saved.');
                            $this->writeLog($log);

                            // Experience seo

                            $seoController = new SeosController();
                            $seoController->createOrUpdateSeo(
                                $experienceVersion->title,
                                $experienceVersion->description,
                                (isset($newUser->language) && $newUser->language != '') ? $newUser->language : \Lang::getFallback(),
                                $experienceVersion->id
                            );
                            array_push($log, 'Seo saved.');
                            $this->writeLog($log);

                            // Fixed additional services
                            if ($experience->colazione) {
                                $experienceVersion->fixedAdditionalServices()->attach($breakfast->id, ["price" => 0, "per_person" => false, "currency" => $experienceVersion->currency]);
                            }
                            array_push($log, 'Fixed additional services saved.');
                            $this->writeLog($log);

                            // Additional services

                            for ($j = 1; $j <= 5; $j++) {
                                if (isset($experience->{'nome_servizio_' . $j}) && $experience->{'nome_servizio_' . $j} != '') {
                                    $price = isset($experience->{'prezzo_servizio_' . $j}) ? $experience->{'prezzo_servizio_' . $j} : 0;
                                    $additionalServiceData = ['name' => $experience->{'nome_servizio_' . $j}, 'price' => $price, 'per_person' => false, 'currency' => $experienceVersion->currency];
                                    $additionalService = new AdditionalService($additionalServiceData);
                                    $experienceVersion->additionalServices()->save($additionalService);
                                }
                            }
                            array_push($log, 'Additional services saved.');
                            $this->writeLog($log);

                            // Period
                            $periodData = [
                                'currency' => $experienceVersion->currency,
                                'date_start' => null,
                                'date_end' => null,
                                'entire_boat' => true,
                                'price' => 0,
                                'min_person' => $experience->n_posti_min
                            ];

                            $period = new Period($periodData);
                            $newExperience->periods()->save($period);
                            array_push($log, 'Period saved.');
                            $this->writeLog($log);

                            // Prices

                            $prices = DB::connection($conn)->select('select * from PREZZI_TRIP where eliminato = 0 AND mezza = 0 AND id_trip = ? ORDER BY n_persone DESC', [$experience->id]);
                            $counterPrice = 0;
                            foreach ($prices as $price) {
                                if ($counterPrice == 0) {
                                    if ($price->n_persone == $experienceVersion->seats) {
                                        $period->price = $price->prezzo;
                                        $period->save();
                                    } else {
                                        $tempPrice = ceil(($price->prezzo / $price->n_persone) * $experienceVersion->seats);
                                        $period->price = $tempPrice;
                                        $period->save();
                                        $newPrice = new Price(['person' => $price->n_persone, 'price' => $price->prezzo, 'currency' => $experienceVersion->currency]);
                                        $period->prices()->save($newPrice);
                                    }
                                } else {
                                    $newPrice = new Price(['person' => $price->n_persone, 'price' => $price->prezzo, 'currency' => $experienceVersion->currency]);
                                    $period->prices()->save($newPrice);
                                }
                                // Add price to period
                                $counterPrice++;
                            }
                            array_push($log, 'Prices saved.');
                            $this->writeLog($log);

                            // Availability
                            $availability = ['date_start' => '2017-03-24', 'date_end' => '2018-03-31'];
                            $availability = new ExperienceAvailability($availability);
                            $newExperience->experienceAvailabilities()->save($availability);
                            array_push($log, 'Availability saved.');
                            $this->writeLog($log);

                            // Photos
                            if (!$useBoatPhoto) {
                                array_push($log, 'Experience has photos...');
                                $this->writeLog($log);

                                $k = 0;
                                $photoCounter = 0;
                                $is_first = true;
                                while (isset($experiencePhotos[$k]) && $photoCounter <= \App\Constant::MAX_PHOTO_NUMBER) {
                                    do {
                                        $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/trip/' . $experience->id . '/' . $experiencePhotos[$k]->id . '.jpg';
                                        $k++;
                                    } while (!@is_array(getimagesize($link)) && isset($experiencePhotos[$k]));
                                    if (@is_array(getimagesize($link))) {
                                        array_push($log, 'Saving photo...');
                                        $this->writeLog($log);

                                        $photo = $experienceVersion->experienceVersionPhotos()->create([
                                            'is_cover' => false
                                        ]);
                                        if ($is_first) {
                                            $photo->is_cover = true;
                                            $is_first = false;
                                        }

                                        $photo->save();

                                        // Create public id
                                        $url = PhotoHelper::getExperiencePhotoPublicId($newUser->id,
                                            $newBoat->id,
                                            $newExperience->id,
                                            $photo->id
                                        );

                                        Cloudder::upload(
                                            $link,
                                            $url,
                                            ["format" => "jpg"]
                                        );

                                        $photoCounter++;
                                    }
                                }

                                if ($photoCounter == 0 && isset($linkPhotoExternal)) {
                                    $photo = $experienceVersion->experienceVersionPhotos()->create([
                                        'is_cover' => true
                                    ]);

                                    $photo->save();

                                    // Create public id
                                    $url = PhotoHelper::getExperiencePhotoPublicId($newUser->id,
                                        $newBoat->id,
                                        $newExperience->id,
                                        $photo->id
                                    );

                                    Cloudder::upload(
                                        $linkPhotoExternal,
                                        $url,
                                        ["format" => "jpg"]
                                    );
                                }
                            }
                            else {
                                array_push($log, 'Using boat photos...');
                                $this->writeLog($log);

                                if (!$boatPhotos) {
                                    array_push($log, 'Boat has no photos.');
                                    $this->writeLog($log);
                                }
                                else {
                                    $link = 'https://res.cloudinary.com/naustrip/image/upload/user_images/' . $user->id . '/' . $boat->id . '/' . $boatPhotos[0]->id . '.jpg';
                                    if (@is_array(getimagesize($link))) {
                                        $photo = $experienceVersion->experienceVersionPhotos()->create([
                                            'is_cover' => true
                                        ]);
                                        $photo->save();

                                        // Create public id
                                        $url = PhotoHelper::getExperiencePhotoPublicId($newUser->id,
                                            $newBoat->id,
                                            $newExperience->id,
                                            $photo->id
                                        );

                                        Cloudder::upload(
                                            $link,
                                            $url,
                                            ["format" => "jpg"]
                                        );
                                    }
                                }
                            }
                        }
                    }

                    array_push($stampUser['boats'], $stampBoat);

                    array_push($log, 'Experiences saved.');
                    $this->writeLog($log);
                }
                array_push($stampUsers, $stampUser);
            }

            $results['new_users'] = User::all()->count();
            $results['old_boats'] = $oldBoats;
            $results['new_boats'] = Boat::all()->count();
            $results['old_experiences'] = $oldExperiences;
            $results['new_experiences'] = Experience::all()->count();

        } catch (\Exception $e) {
            DB::rollBack();
            array_push($errors, $e);
        }

        $stampUsers = json_encode($stampUsers);
        Storage::put('data.json', $stampUsers);

        $errors = json_encode($errors);
        Storage::put('errors.json', $errors);

        $results = json_encode($results);
        Storage::put('results.json', $results);

        $this->writeLog($log);

        DB::commit();
    }

    private function writeLog(array $log, string $name = 'log'){
        $logStamp = json_encode($log);
        Storage::put($name . '.json', $logStamp);
    }
}
