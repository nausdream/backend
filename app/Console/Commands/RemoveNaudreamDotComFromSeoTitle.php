<?php

namespace App\Console\Commands;

use App\Constant;
use App\Models\Seo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RemoveNaudreamDotComFromSeoTitle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:nausdreamcom';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove " - Nausdream.com" from all meta title of seo element when present';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        try {
            // Get all seo
            $seos = Seo::all();

            // Get last part of short meta title and length
            $shortTitleLastPart = ' ' . Constant::SEO_SEPARATOR . ' ' . Constant::SEO_DOMAIN_NAME;
            $lengthShortTitleLastPart = mb_strlen($shortTitleLastPart);

            foreach ($seos as $seo) {
                $length = mb_strlen($seo->title);
                if (mb_substr($seo->title, $length - $lengthShortTitleLastPart) == $shortTitleLastPart) {
                    $seo->title = mb_substr($seo->title, 0, $length - $lengthShortTitleLastPart);
                    $seo->save();
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd('Error: ' . $e->getMessage());
        }
        DB::commit();
    }
}
