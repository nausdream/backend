<?php

namespace App\Console\Commands;

use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateTranslations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update translation pages, sentences and translations on server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection(env('DB_CONNECTION_TRANSLATION'))->beginTransaction();

        try {
            $conn = env('DB_STAGING_CONNECTION_TRANSLATION');

            // Get all languages from server db
            $languagesStaging = DB::connection($conn)->select('select * from languages');
            $languagesStagingIds = [];

            foreach ($languagesStaging as $languageStaging) {

                // Check if language exists, create otherwise
                $language = Language::where('language', $languageStaging->language)->first();

                if (!isset($language)) {
                    $language = new Language(['name' => $languageStaging->language, 'rtl' => $languageStaging->rtl]);
                    $language->save();
                }

                // Create association between id of server db and staging db to check translations later
                $languagesStagingIds[$language->id] = $languageStaging->id;
            }

            $languages = Language::all();

            // Fetch all pages from staging
            $stagingPages = DB::connection($conn)->select('select * from pages');
            $stagingPageNames = [];

            foreach ($stagingPages as $stagingPage) {

                // Check if page exists, create otherwise
                $page = Page::where('name', $stagingPage->name)->first();

                if (!isset($page)) {
                    $page = new Page(['name' => $stagingPage->name]);
                    $page->save();
                }

                // Store page name for delete the page that are not present anymore
                $stagingPageNames[] = $stagingPage->name;

                // Fetch all sentences of the current page
                $stagingSentences = DB::connection($conn)
                    ->select('select * from sentences where page_id = ?', [$stagingPage->id]);

                foreach ($stagingSentences as $stagingSentence) {

                    // Check if sentence exists, create otherwise
                    $sentence = Sentence::where('name', $stagingSentence->name)->where('page_id', $page->id)->first();

                    if (is_null($stagingSentence->deleted_at)) {
                        if (!isset($sentence)) {
                            $sentence = new Sentence(['name' => $stagingSentence->name]);
                            $sentence->page_id = $page->id;
                            $sentence->save();
                        }

                        // Insert translations
                        foreach ($languages as $language) {

                            // Fetch sentence translation
                            $sentenceTranslationStaging = DB::connection($conn)
                                ->select('select * from sentence_translations where sentence_id = ? and language_id = ?',
                                    [$stagingSentence->id, $languagesStagingIds[$language->id]]);

                            if (isset($sentenceTranslationStaging) && !empty($sentenceTranslationStaging)) {
                                $sentenceTranslationStaging = $sentenceTranslationStaging[0];

                                // Check if already exist and update, create otherwise
                                $sentenceTranslation = SentenceTranslation::where('sentence_id', $sentence->id)
                                    ->where('language_id', $language->id)
                                    ->first();

                                if (isset($sentenceTranslation)) {
                                    $sentenceTranslation->text = $sentenceTranslationStaging->text;
                                    $sentenceTranslation->save();
                                } else {
                                    $sentenceTranslation = new SentenceTranslation(['text' => $sentenceTranslationStaging->text]);
                                    $sentenceTranslation->language()->associate($language);
                                    $sentence->sentenceTranslations()->save($sentenceTranslation);
                                }
                            }
                        }
                    } else {
                        if (isset($sentence)) {
                            // Delete sentence translations
                            $sentenceTranslations = $sentence->sentenceTranslations();
                            foreach ($sentenceTranslations as $sentenceTranslation) {
                                $sentenceTranslation->delete();
                            }

                            // Delete sentence
                            $sentence->delete();
                        }
                    }
                }

            }

            // Fetch all pages that does not exist anymore
            $pagesToDelete = Page::all()->filter(function ($value, $key) use ($stagingPageNames) {
                if (!in_array($value->name, $stagingPageNames)){
                    return true;
                }
                return false;
            });

            // Delete al pages not used anymore
            foreach ($pagesToDelete as $page) {
                // Delete sentences

                $sentences = $page->sentences()->withTrashed()->get();
                foreach ($sentences as $sentence) {

                    // Delete sentence translations

                    $sentenceTranslations = $sentence->sentenceTranslations()->withTrashed()->get();
                    foreach ($sentenceTranslations as $sentenceTranslation) {
                        $sentenceTranslation->forceDelete();
                    }

                    // Delete sentence

                    $sentence->forceDelete();
                }

                // Delete page

                $page->forceDelete();
            }


        } catch (\Exception $e) {
            DB::connection(env('DB_CONNECTION_TRANSLATION'))->rollBack();
            dd('Error: ' . $e->getMessage());
        }

        DB::connection(env('DB_CONNECTION_TRANSLATION'))->commit();
    }
}
