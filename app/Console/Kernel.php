<?php

namespace App\Console;

use Artisan;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateTranslations::class,
        Commands\MigrateOldSiteContent::class,
        Commands\MigrateOldSiteInvalidContent::class,
        Commands\RemoveNaudreamDotComFromSeoTitle::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Change currency for search api
        $schedule->call(function () {
            \App\Services\CurrencyChanger::run();
        })->cron('0 */3 * * *')->timezone('Europe/Rome');

        // Send email for pre experience
        $schedule->call(function () {
            \App\Services\PreExperienceMailSender::run();
        })->cron('0 */6 * * *')->timezone('Europe/Rome');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
