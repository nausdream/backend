<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 17/10/2016
 * Time: 12:38
 */

namespace App;

/**
 * A list of constants for the app.
 *
 * @author Giuseppe Basciu <giuseppe@nausdream.com>
 */
class Constant
{
    // Database fields
    const FIRST_NAME_LENGTH = 150;
    const LAST_NAME_LENGTH = 150;
    const EMAIL_LENGTH = 150;
    const PHONE_LENGTH = 150;
    const NATIONALITY_LENGTH = 200;
    const CURRENCY_LENGTH = 3;
    const LANGUAGE_LENGTH = 20;
    const ENTERPRISE_NAME_LENGTH = 150;
    const ENTERPRISE_VAT_LENGTH = 100;
    const ENTERPRISE_ADDRESS_LENGTH = 150;
    const ENTERPRISE_CITY_LENGTH = 150;
    const ENTERPRISE_ZIP_LENGTH = 20;
    const PRODUCTION_SITE_LENGTH = 150;
    const BOAT_MODEL_LENGTH = 100;
    const BOAT_NAME_LENGTH = 100;
    const DOCKING_PLACE_LENGTH = 150;
    const PORT_DEPARTURE_LENGTH = 100;
    const PORT_RETURN_LENGTH = 100;
    const FILENAME_LENGTH = 150;
    const EMAIL_TOKEN_LENGTH = 64;
    const COUPON_NAME_LENGTH = 50;
    const COUPON_NAME_LENGTH_DB = 100;
    const EVENT_PLACE_LENGTH = 200;
    const PAGE_NAME_LENGTH = 250;
    const EXPERIENCE_TITLE_LENGTH = 300;
    const SENTENCE_NAME_LENGTH = 200;
    const FUID_LENGTH = 100;
    const EXPERIENCE_TYPE_LENGTH = 100;
    const FISHING_TYPE_LENGTH = 100;
    const CAPTAIN_TYPE_LENGTH = 100;
    const FISHING_PARTITION_LENGTH = 100;
    const DEPOSIT_METHOD_LENGTH = 100;
    const ACCESSORY_LENGTH = 100;
    const RULE_LENGTH = 100;
    const ADDITIONAL_SERVICE_LENGTH = 100;
    const AREA_NAME_LENGTH = 100;
    const MAX_PASSWORD_LENGTH = 128;
    const MIN_PASSWORD_LENGTH = 8;
    const CAPTAIN_TYPES = [
        0, // Private
        1, // Enterprise
        2, // Association
    ];
    const SEX_TYPES = [
        'm', // Male
        'f', // Female
    ];

    // Room length
    const ROOM_LENGTH = 20;

    // Guests of eden voucher string legnth
    const GUESTS_FOR_EDEN_VOUCHER_LENGTH = 40;

    // Lat - lng
    const MAX_LATITUDE = 90;
    const MAX_LONGITUDE = 180;

    // Countries
    const COUNTRY_NAME_LENGTH = 200;

    // Fees
    const MAX_FEE_VALUE = 99;
    const MIN_FEE_VALUE = 0;

    // Security string legth
    const CSRF_TOKEN_LENGTH = 16;

    // Pagination
    const BOATS_PER_PAGE = 6;
    const MAX_BOATS_PER_PAGE = 50;
    const EXPERIENCES_PER_PAGE = 50;
    const MAX_EXPERIENCES_PER_PAGE = 50;
    const DEF_ITEMS_PER_PAGE = 6;
    const PAGES_PER_PAGE = 6;
    const MAX_PAGES_PER_PAGE = 50;
    const COUPONS_PER_PAGE = 6;
    const MAX_COUPONS_PER_PAGE = 50;
    const CONVERSATIONS_PER_PAGE = 6;
    const MAX_CONVERSATIONS_PER_PAGE = 50;
    const BOOKINGS_PER_PAGE = 6;
    const MAX_BOOKINGS_PER_PAGE = 50;
    const USERS_PER_PAGE = 6;
    const MAX_USERS_PER_PAGE = 50;
    const LISTINGS_PER_PAGE = 6;
    const MAX_LISTINGS_PER_PAGE = 50;
    const SEOS_PER_PAGE = 6;
    const MAX_SEOS_PER_PAGE = 50;

    // Status
    const STATUS_PENDING = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_ACCEPTED_CONDITIONALLY = 2;
    const STATUS_REJECTED = 3;
    const STATUS_FREEZED = 4;
    const STATUS_NAMES = [
        0 => 'pending',
        1 => 'accepted',
        2 => 'accepted_conditionally',
        3 => 'rejected',
        4 => 'freezed'
    ];

    const BOOKING_STATUS_PENDING = 0;
    const BOOKING_STATUS_ACCEPTED = 1;
    const BOOKING_STATUS_REJECTED = 2;
    const BOOKING_STATUS_PAID = 3;
    const BOOKING_STATUS_CANCELLED = 4;
    const BOOKING_STATUS_NAMES = [
        0 => 'pending',
        1 => 'accepted',
        2 => 'rejected',
        3 => 'paid',
        4 => 'cancelled'
    ];

    const OFFSITE_BOOKING_STATUS_PENDING = 0;
    const OFFSITE_BOOKING_STATUS_PAID = 1;
    const OFFSITE_BOOKING_STATUS_NAMES = [
        0 => 'pending',
        1 => 'paid',
    ];

    const CONVERSATION_STATUS_NEW = 'new';

    // Photos
    const MAX_PHOTO_SIZE = 6291456; // in bytes
    const MAX_PHOTO_NUMBER = 6;

    // Language priorities
    const LANGUAGE_ORDER = ['it', 'en'];

    // Boats
    const BOAT_DESCRIPTION_LENGTH = 2000;
    const BOAT_RULES_LENGTH = 1000;
    const BOAT_INDICATIONS_LENGTH = 1000;

    // Experiences
    const EXPERIENCE_DESCRIPTION_LENGTH = 2000;
    const EXPERIENCE_RULES_LENGTH = 1000;
    const MAX_EXPERIENCE_DAYS = 30;
    const CANCELLATION_MIN_DAYS = 3;
    const CANCELLATION_MAX_DAYS = 30;

    // Seo
    const SEO_TITLE_LENGTH = 60;
    const SEO_DESCRIPTION_LENGTH = 160;
    const SEO_SLUG_URL_LENGTH = 200;
    const SEO_DOMAIN_NAME = 'Nausdream.com';
    const SEO_SEPARATOR = '-';
    const SEO_MAX_SLUG_URL_NUMBER = 9999;

    // S3
    const FILE_NAME_S3 = 256;

    // Cookies
    const USER_SESSION_EXPIRATION = 10512000;

    // Distance calculation
    const KM_MULTIPLICATOR = 6371;

    // Support name
    const SUPPORT_NAME = 'Nausdream Support';
    const SUPPORT_MAIL = 'support@nausdream.com';
    const NOREPLY_MAIL = 'noreply@nausdream.com';
    const COMPANY_NAME = 'Nausdream S.r.l.';
    const SUPPORT_PHONE = '+39 070 85 50 052';
    const BOOKING_MAIL = 'booking@nausdream.com';
    const WEB_SITE_ADDRESS_FOR_PDF = 'www.nausdream.com';
    const COMPANY_ADDRESS = 'Vico II Is Arenas, 4 - 09126 - Cagliari';
    const COMPANY_OPERATIVE_ADDRESS = 'Hub/Spoke - Via Roma 235, 09123, Cagliari';
    const COMPANY_CITY = 'Cagliari';

    // Currency Symbol
    const CURRENCY_SYMBOLS = [
        "ALL" => "Lek",
        "AFN" => "؋",
        "ARS" => "$",
        "AWG" => "ƒ",
        "AUD" => "$",
        "AZN" => "ман",
        "BSD" => "$",
        "BBD" => "$",
        "BYN" => "Br",
        "BZD" => "BZ$",
        "BMD" => "$",
        "BOB" => "\$b",
        "BAM" => "KM",
        "BWP" => "P",
        "BGN" => "лв",
        "BRL" => "R$",
        "BND" => "$",
        "KHR" => "៛",
        "CAD" => "$",
        "KYD" => "$",
        "CLP" => "$",
        "CNY" => "¥",
        "COP" => "$",
        "CRC" => "₡",
        "HRK" => "kn",
        "CUP" => "₱",
        "CZK" => "Kč",
        "DKK" => "kr",
        "DOP" => "RD$",
        "XCD" => "$",
        "EGP" => "£",
        "SVC" => "$",
        "EUR" => "€",
        "FKP" => "£",
        "FJD" => "$",
        "GHS" => "¢",
        "GIP" => "£",
        "GTQ" => "Q",
        "GGP" => "£",
        "GYD" => "$",
        "HNL" => "L",
        "HKD" => "$",
        "HUF" => "Ft",
        "ISK" => "kr",
        "INR" => "",
        "IDR" => "Rp",
        "IRR" => "﷼",
        "IMP" => "£",
        "ILS" => "₪",
        "JMD" => "J$",
        "JPY" => "¥",
        "JEP" => "£",
        "KZT" => "лв",
        "KPW" => "₩",
        "KRW" => "₩",
        "KGS" => "лв",
        "LAK" => "₭",
        "LBP" => "£",
        "LRD" => "$",
        "MKD" => "ден",
        "MYR" => "RM",
        "MUR" => "₨",
        "MXN" => "$",
        "MNT" => "₮",
        "MZN" => "MT",
        "NAD" => "$",
        "NPR" => "₨",
        "ANG" => "ƒ",
        "NZD" => "$",
        "NIO" => "C$",
        "NGN" => "₦",
        "NOK" => "kr",
        "OMR" => "﷼",
        "PKR" => "₨",
        "PAB" => "B/.",
        "PYG" => "Gs",
        "PEN" => "S/.",
        "PHP" => "₱",
        "PLN" => "zł",
        "QAR" => "﷼",
        "RON" => "lei",
        "RUB" => "₽",
        "SHP" => "£",
        "SAR" => "﷼",
        "RSD" => "Дин.",
        "SCR" => "₨",
        "SGD" => "$",
        "SBD" => "$",
        "SOS" => "S",
        "ZAR" => "R",
        "LKR" => "₨",
        "SEK" => "kr",
        "CHF" => "CHF",
        "SRD" => "$",
        "SYP" => "£",
        "TWD" => "NT$",
        "THB" => "฿",
        "TTD" => "TT$",
        "TRY" => "",
        "TVD" => "$",
        "UAH" => "₴",
        "GBP" => "£",
        "USD" => "$",
        "UYU" => "\$U",
        "UZS" => "лв",
        "VEF" => "Bs",
        "VND" => "₫",
        "YER" => "﷼",
        "ZWD" => "Z$"
    ];

    const CURRENCY_SYMBOLS_FOR_PDF = [
        "ALL" => "Lek",
        "AFN" => "؋",
        "ARS" => "$",
        "AWG" => "ƒ",
        "AUD" => "$",
        "AZN" => "ман",
        "BSD" => "$",
        "BBD" => "$",
        "BYN" => "Br",
        "BZD" => "BZ$",
        "BMD" => "$",
        "BOB" => "\$b",
        "BAM" => "KM",
        "BWP" => "P",
        "BGN" => "лв",
        "BRL" => "R$",
        "BND" => "$",
        "KHR" => "៛",
        "CAD" => "$",
        "KYD" => "$",
        "CLP" => "$",
        "CNY" => "¥",
        "COP" => "$",
        "CRC" => "₡",
        "HRK" => "kn",
        "CUP" => "₱",
        "CZK" => "Kč",
        "DKK" => "kr",
        "DOP" => "RD$",
        "XCD" => "$",
        "EGP" => "£",
        "SVC" => "$",
        "EUR" => "€",
        "FKP" => "£",
        "FJD" => "$",
        "GHS" => "¢",
        "GIP" => "£",
        "GTQ" => "Q",
        "GGP" => "£",
        "GYD" => "$",
        "HNL" => "L",
        "HKD" => "$",
        "HUF" => "Ft",
        "ISK" => "kr",
        "INR" => "",
        "IDR" => "Rp",
        "IRR" => "﷼",
        "IMP" => "£",
        "ILS" => "₪",
        "JMD" => "J$",
        "JPY" => "¥",
        "JEP" => "£",
        "KZT" => "лв",
        "KPW" => "₩",
        "KRW" => "₩",
        "KGS" => "лв",
        "LAK" => "₭",
        "LBP" => "£",
        "LRD" => "$",
        "MKD" => "ден",
        "MYR" => "RM",
        "MUR" => "₨",
        "MXN" => "$",
        "MNT" => "₮",
        "MZN" => "MT",
        "NAD" => "$",
        "NPR" => "₨",
        "ANG" => "ƒ",
        "NZD" => "$",
        "NIO" => "C$",
        "NGN" => "₦",
        "NOK" => "kr",
        "OMR" => "﷼",
        "PKR" => "₨",
        "PAB" => "B/.",
        "PYG" => "Gs",
        "PEN" => "S/.",
        "PHP" => "₱",
        "PLN" => "zł",
        "QAR" => "﷼",
        "RON" => "lei",
        "RUB" => "₽",
        "SHP" => "£",
        "SAR" => "﷼",
        "RSD" => "Дин.",
        "SCR" => "₨",
        "SGD" => "$",
        "SBD" => "$",
        "SOS" => "S",
        "ZAR" => "R",
        "LKR" => "₨",
        "SEK" => "kr",
        "CHF" => "CHF",
        "SRD" => "$",
        "SYP" => "£",
        "TWD" => "NT$",
        "THB" => "THB",
        "TTD" => "TT$",
        "TRY" => "",
        "TVD" => "$",
        "UAH" => "₴",
        "GBP" => "£",
        "USD" => "$",
        "UYU" => "\$U",
        "UZS" => "лв",
        "VEF" => "Bs",
        "VND" => "₫",
        "YER" => "﷼",
        "ZWD" => "Z$"
    ];
}
