<?php

namespace App\Listeners;

use App\Constant;
use App\Events\ExperienceVersionPutted;
use App\Http\Controllers\v1\SeosController;
use App\Models\Seo;
use App\Services\SeoHelper;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrUpdateSeo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExperienceVersionPutted  $event
     * @return void
     */
    public function handle(ExperienceVersionPutted $event)
    {
        // Get experienceVersion

        $experienceVersion = $event->experienceVersion;

        // Create or update Seo

        $seoController = new SeosController();
        $seoController->createOrUpdateSeo(
            $experienceVersion->title,
            $experienceVersion->description,
            $experienceVersion->experience()->first()->boat()->first()->user()->first()->language,
            $experienceVersion->id
        );
    }
}
