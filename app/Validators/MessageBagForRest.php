<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 27/10/2016
 * Time: 11:59
 */

namespace App\Validators;

use Illuminate\Support\MessageBag;

class MessageBagForRest extends MessageBag
{
    /**
     * Add a message to the bag for rest api.
     *
     * @param  string $key
     * @param  string $message
     * @return $this
     */
    public function add($key, $message)
    {
        if ($this->isUnique($key, $message)) {
            array_push($this->messages, $message);
        }

        return $this;
    }

    /**
     * Determine if the message bag has any messages.
     *
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->messages) == 0 ? true : false;
    }
}