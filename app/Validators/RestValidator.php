<?php
/**
 * Created by PhpStorm.
 * User: Giuseppe
 * Date: 26/10/2016
 * Time: 18:04
 */

namespace App\Validators;

use Illuminate\Validation\Validator;

class RestValidator extends Validator
{
    /**
     * The message bag instance.
     *
     * @var MessageBagForRest
     */
    protected $messages;

    /**
     * Determine if the data passes the validation rules.
     *
     * @return bool
     */

    public function passes()
    {
        $this->messages = new MessageBagForRest();

        // We'll spin through each rule, validating the attributes attached to that
        // rule. Any error messages will be added to the containers with each of
        // the other error messages, returning true if we don't have messages.
        foreach ($this->rules as $attribute => $rules) {
            $attribute = str_replace('\.', '->', $attribute);

            foreach ($rules as $rule) {
                $this->validateAttribute($attribute, $rule);

                if ($this->shouldStopValidating($attribute)) {
                    break;
                }
            }
        }

        // Here we will spin through all of the "after" hooks on this validator and
        // fire them off. This gives the callbacks a chance to perform all kinds
        // of other validation that needs to get wrapped up in this operation.
        foreach ($this->after as $after) {
            call_user_func($after);
        }

        return $this->messages->isEmpty();
    }


    /**
     * Get the message container for the validator.
     *
     * @return MessageBagForRest
     */
    public function messages()
    {
        if (!$this->messages) {
            $this->passes();
        }

        return $this->messages;
    }

    /**
     * An alternative more semantic shortcut to the message container.
     *
     * @return MessageBagForRest
     */
    public function errors()
    {
        return $this->messages();
    }

    /**
     * Get the messages for the instance.
     *
     * @return MessageBagForRest
     */
    public function getMessageBag()
    {
        return $this->messages();
    }

    /**
     * Add an error message to the validator's collection of messages.
     *
     * @param  string $attribute
     * @param  string $rule
     * @param  array $parameters
     * @return void
     */
    protected function addError($attribute, $rule, $parameters)
    {
        $message = $this->getMessage($attribute, $rule);

        $message = $this->doReplacements($message, $attribute, $rule, $parameters);

        $customMessage = new MessageBagForRest();

        $customMessage->merge(['code' => strtolower($rule)]);
        $customMessage->merge(['title' => $attribute]);
        $customMessage->merge(['detail' => $message]);

        $this->messages->add($attribute, $customMessage);//dd($this->messages);
    }
}