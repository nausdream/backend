<?php

namespace App\Jobs;

use App\Models\ExperiencePhoto;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetExperienceCover implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $photoId;

    /**
     * Create a new job instance.
     * @param $photoId
     */
    public function __construct($photoId)
    {
        $this->photoId = $photoId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $photo = ExperiencePhoto::find($this->photoId);
        $coverPhoto = $photo->experienceVersion()->get()->last()
            ->experienceVersionPhotos()->where('is_cover', true)->where('id', '!=', $this->photoId)->get()->last();
        if (isset($coverPhoto)) {
            $coverPhoto->is_cover = false;
            $coverPhoto->save();
        }
        $photo->is_cover = true;
        $photo->save();
    }
}
