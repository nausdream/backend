<?php

namespace App\Jobs;

use App\Models\User;
use App\Services\CurrencyChanger;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaptainChangesCurrency implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $currencyIn;
    protected $currencyOut;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $currencyIn
     * @param $currencyOut
     */
    public function __construct(User $user, $currencyIn, $currencyOut)
    {
        $this->user = $user;
        $this->currencyIn = $currencyIn;
        $this->currencyOut = $currencyOut;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Get all user boats
        $boats = $this->user->boats()->get();
        foreach ($boats as $boat) {

            // Get all boat experiences
            $experiences = $boat->experiences()->get();
            foreach ($experiences as $experience) {

                // Get all experience experienceVersions
                $experienceVersions = $experience->experienceVersions()->get();
                foreach ($experienceVersions as $experienceVersion) {

                    // If set change deposit
                    if (isset($experienceVersion->deposit)) {
                        $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($experienceVersion->currency, $this->currencyOut);
                        $experienceVersion->deposit = ceil($experienceVersion->deposit * $currencyChangeValue);
                    }
                    $experienceVersion->currency = $this->currencyOut;
                    $experienceVersion->save();

                    // Get fixed additional services
                    $fixedAdditionalServices = $experienceVersion->experienceVersionsFixedAdditionalServices()->get();
                    foreach ($fixedAdditionalServices as $fixedAdditionalService) {

                        // If set change price of fixed additional services
                        if (isset($fixedAdditionalService->price)) {
                            $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($fixedAdditionalService->currency, $this->currencyOut);
                            $fixedAdditionalService->price = ceil($fixedAdditionalService->price * $currencyChangeValue);
                        }
                        $fixedAdditionalService->currency = $this->currencyOut;
                        $fixedAdditionalService->save();
                    }

                    // Get additional services
                    $additionalServices = $experienceVersion->additionalServices()->get();
                    foreach ($additionalServices as $additionalService) {

                        // If set change price of additional services
                        if (isset($additionalService->price)) {
                            $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($additionalService->currency, $this->currencyOut);
                            $additionalService->price = ceil($additionalService->price * $currencyChangeValue);
                        }
                        $additionalService->currency = $this->currencyOut;
                        $additionalService->save();
                    }
                }

                // Get all experience periods
                $periods = $experience->periods()->get();
                foreach ($periods as $period) {

                    // If set change price of period
                    if (isset($period->price)) {
                        $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($period->currency, $this->currencyOut);
                        $period->price = ceil($period->price * $currencyChangeValue);
                    }
                    $period->currency = $this->currencyOut;
                    $period->save();

                    // Get all period prices
                    $prices = $period->prices()->get();
                    foreach ($prices as $price) {

                        // If set change price of price
                        if (isset($price->price)) {
                            $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($price->currency, $this->currencyOut);
                            $price->price = ceil($price->price * $currencyChangeValue);
                        }
                        $price->currency = $this->currencyOut;
                        $price->save();
                    }
                }
            }
        }
    }
}
