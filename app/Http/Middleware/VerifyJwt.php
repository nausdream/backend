<?php

namespace App\Http\Middleware;

use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Closure;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class VerifyJwt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //get JWT token
        $result = JwtService::jwtChecker($request->bearerToken());
        if ($result instanceof Response) {
            return $result;
        }
        return $next($request);
    }
}
