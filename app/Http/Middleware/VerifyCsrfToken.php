<?php
/**
 * This middleware protects server and clients from Cross Site Request Forgery.
 *
 * The client needs to send a random csrf_token (generated and sent by the server along with the user token on registration)
 * in both a cookie and a request parameter. This way, the server doesn't need to store it, and can just check if the two
 * match.
 *
 * The only way to circumvent this is by exploiting XSS vulnerability on our side.
 *
 * Read more at: https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet#Double_Submit_Cookie
 */
namespace App\Http\Middleware;

use App\Services\ResponseHelper;
use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') == 'testing') {
            return $next($request);
        }
        if ($request->cookie('csrf_token_admin') != null && $request->cookie('csrf_token_admin') != '' && $request->input('meta')['csrf_token'] == $request->cookie('csrf_token_admin')) {
            return $next($request);
        }
        if ($request->cookie('csrf_token_user') != null && $request->cookie('csrf_token_user') != '' && $request->input('meta')['csrf_token'] == $request->cookie('csrf_token_user')) {
            return $next($request);
        }
        if ($request->cookie('csrf_token_trans') != null && $request->cookie('csrf_token_trans') != '' && $request->input('meta')['csrf_token'] == $request->cookie('csrf_token_trans')) {
            return $next($request);
        }
        return ResponseHelper::responseError(
            'Csrf token is not valid',
            SymfonyResponse::HTTP_FORBIDDEN,
            'invalid_csrf_token');
    }


}
