<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceType;
use App\Models\ExperienceTypesFixedAdditionalServices;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AccessoryTransformer;
use App\Services\Transformers\FixedAdditionalServiceTransformer;
use App\Services\Transformers\RulesTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class FixedAdditionalServicesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->type = 'fixed-additional-services';
        $this->transformer = new FixedAdditionalServiceTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Check for experience type and experience not type input presence

        $experienceTypeName = $request->input('type');
        $experienceNotTypeName = $request->input('not_type');

        if (isset($experienceTypeName)) {
            $experienceType = ExperienceType::all()->where('name', $experienceTypeName)->last();

            if (!isset($experienceType)) {
                return ResponseHelper::responseError(
                    'Experience type does not exist.',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_type_not_found'
                    );
            }

            $fixedAdditionalServices = $experienceType->fixedAdditionalServices()->get();
        } else {
            if (isset($experienceNotTypeName)) {
                $experienceType = ExperienceType::all()->where('name', $experienceNotTypeName)->last();

                if (!isset($experienceType)) {
                    return ResponseHelper::responseError(
                        'Experience type does not exist.',
                        SymfonyResponse::HTTP_NOT_FOUND,
                        'experience_type_not_found'
                    );
                }

                $fixedAdditionalServices = \App\Models\FixedAdditionalService::all();

                $collection = collect();

                foreach ($fixedAdditionalServices as $fixedAdditionalService) {
                    $temp = $experienceType->fixedAdditionalServices()->where('fixed_additional_services.id', $fixedAdditionalService->id)->get()->last();

                    if (!isset($temp)) {
                        $collection->push($fixedAdditionalService);
                    }
                }

                $fixedAdditionalServices = $collection;
            } else {
                $fixedAdditionalServices = \App\Models\FixedAdditionalService::all();
            }
        }

        return ResponseHelper::responseIndex($this->type, $fixedAdditionalServices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getFieldsToValidate($accessory)
    {
        // If a key is not found, assign null
        return [
            'resource_type' => $accessory['type'] ?? null,
            'resource_id' => (int) ($accessory['id'] ?? null)
        ];
    }
}
