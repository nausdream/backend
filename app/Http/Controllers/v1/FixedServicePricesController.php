<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Models\FixedAdditionalService;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AccessoryTransformer;
use App\Services\Transformers\FixedAdditionalServicePriceTransformer;
use App\Services\Transformers\FixedAdditionalServiceTransformer;
use App\Services\Transformers\RulesTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class FixedServicePricesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'fixed-additional-service-prices';
        $this->transformer = new FixedAdditionalServicePriceTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $price = $request->input('data.attributes.price');
        $perPerson = $request->input('data.attributes.per_person');

        // Validate type & id
        $rules = [
            'type' => 'in:'.$this->type,
            'id' => 'in:'.$id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validate other data
        $rules = [
            'price' => 'numeric|min:0',
            'per_person' => 'boolean'
        ];

        $data = [
            'price' => $price,
            'per_person' => $perPerson
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get FixedAdditionalServicePrice from db
        $fixedAdditionalServicePrice = ExperienceVersionsFixedAdditionalServices::find($id);
        if (!isset($fixedAdditionalServicePrice)){
            return ResponseHelper::responseError(
                'Price not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'price_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersion = $fixedAdditionalServicePrice->experienceVersion()->first();
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found');
        }

        if (Gate::forUser($account)->denies('patch-fixed-additional-service-price', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized');
        }

        $fixedAdditionalServicePrice->price = $price;
        $fixedAdditionalServicePrice->per_person = $perPerson;
        $fixedAdditionalServicePrice->save();

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($experienceVersion->experience()->first()->id, $fixedAdditionalServicePrice->fixed_additional_service_id);

        // Set attributes to show
        $attributes = $this->transformer->transform($fixedAdditionalServicePrice);

        return ResponseHelper::responseGet(
            $this->type,
            $fixedAdditionalServicePrice,
            $attributes,
            [],
            $relationships,
            $fixedAdditionalServicePrice->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $fixedAdditionalServicePrice = ExperienceVersionsFixedAdditionalServices::find($id);

        if (!isset($fixedAdditionalServicePrice)){
            return ResponseHelper::responseError(
                'Fixed additional service price not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'fixed_service_price_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersion = ExperienceVersion::find($fixedAdditionalServicePrice->experience_version_id);
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('delete-fixed-additional-service-price', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        ExperienceVersionsFixedAdditionalServices::destroy($id);

        return response('');
    }

    protected function getFieldsToValidate($accessory)
    {
        // If a key is not found, assign null
        return [
            'resource_type' => $accessory['type'] ?? null,
            'resource_id' => (int)($accessory['id'] ?? null)
        ];
    }

    /**
     * Get relationships array
     *
     * @param $experienceId
     * @param $serviceId
     * @return array
     */
    private function getRelationships($experienceId, $serviceId)
    {
        return [
          "service" => [
              "data" => [
                  "type" => "fixed-additional-services",
                  "id" => (string) $serviceId
              ]
          ],
          "experience" => [
              "data" => [
                  "type" => "experiences",
                  "id" => (string) $experienceId
              ]
          ]
        ];
    }

    /**
     * Get array of the resources and values in post request
     *
     * @param Request $request
     * @return array
     */
    protected function getPostAttributesArray($request)
    {
        $arrayResources = [];
        $data = $request->input('data');

        foreach ($data as $resource) {
            $arrayResources[] = [
                'type' => $resource['type'],
                'price' => $resource['attributes']['price'],
                'per_person' => $resource['attributes']['per_person'],
                'service_type' => $resource['relationships']['service']['data']['type'],
                'service_id' => $resource['relationships']['service']['data']['id'],
            ];
        }

        return $arrayResources;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function experienceFixedAdditionalServices(Request $request, $id)
    {
        // Check experience existence

        $experience = Experience::find($id);
        if (!isset($experience)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('post-fixed-additional-service-price', $experienceVersion)) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Get array of elements
        try {
            $arrayResourceAttributes = $this->getPostAttributesArray($request);
        } catch (\Exception $e) {
            return ResponseHelper::responseError(
                'Invalid request',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'invalid_request_format');
        }

        $arrayResources = [];
        $arrayServiceIds = [];
        foreach ($arrayResourceAttributes as $resourceAttribute) {

            // Validate type
            $rules = [
                'type' => 'in:' . $this->type
            ];

            $objectType = [
                'type' => $resourceAttribute['type']
            ];

            $validator = \Validator::make($objectType, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
            }

            // Validate other data and relationships
            $rules = [
                'price' => 'numeric|min:0',
                'per_person' => 'boolean',
                'service_type' => 'in:fixed-additional-services',
                'service_id' => 'numeric',
            ];

            $data = [
                'price' => $resourceAttribute['price'],
                'per_person' => $resourceAttribute['per_person'],
                'service_type' => $resourceAttribute['service_type'],
                'service_id' => $resourceAttribute['service_id'],
            ];

            $validator = \Validator::make($data, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $service = FixedAdditionalService::find($resourceAttribute['service_id']);

            if (!isset($service)) {
                return ResponseHelper::responseError(
                    'Fixed additional service not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'fixed_service_not_found'
                    );
            }

            if (!in_array($service->id, $arrayServiceIds)) {
                $arrayServiceIds[] = $service->id;
                $arrayResources[] = $data;
            }
        }

        // Set correct currency
        $currency = $experience->boat()->first()->user()->first()->currency;

        // Attach fixed additional services and prices

        $experienceVersion->fixedAdditionalServices()->detach();

        $data = [];

        foreach ($arrayResources as $resource) {
            $experienceVersion->fixedAdditionalServices()->attach($resource['service_id'], ["price" => $resource['price'], "per_person" => $resource['per_person'], "currency" => $currency]);

            // Fetch relationships to include on data
            $relationships = $this->getRelationships($experience->id, $resource['service_id']);

            $fixedAdditionalServicePrice = $experienceVersion->fixedAdditionalServices()->where("fixed_additional_service_id", $resource['service_id'])->withPivot('id', 'price', 'per_person')->get()->first();
            $fixedAdditionalServicePrice = ExperienceVersionsFixedAdditionalServices::find($fixedAdditionalServicePrice->pivot->id);

            // Set attributes to show
            $attributes = $this->transformer->transform($fixedAdditionalServicePrice);
            $data[] = JsonHelper::createData($this->type, $fixedAdditionalServicePrice->id, $attributes, $relationships);
        }
        $experienceVersion->save();

        return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_OK);
    }
}
