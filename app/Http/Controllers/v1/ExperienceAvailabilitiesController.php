<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\AdditionalService;
use App\Models\AdditionalServiceTranslation;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceAvailability;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\LanguageHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\AccessoryTransformer;
use App\Services\Transformers\AvailabilityTransformer;
use App\Services\Transformers\FixedAdditionalServiceTransformer;
use App\Services\Transformers\RulesTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class ExperienceAvailabilitiesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'experience-availabilities';
        $this->transformer = new AvailabilityTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');
        $relationshipType = $request->input('data.relationships.experience.data.type');
        $relationshipId = $request->input('data.relationships.experience.data.id');

        // Validate type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $type
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j',
            'relationship_type' => 'in:experiences',
            'relationship_id' => 'numeric|min:1'
        ];

        $objectType = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'relationship_type' => $relationshipType,
            'relationship_id' => $relationshipId
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($relationshipId);
        // Check if experience exists
        if (!isset($experience)) {
            //experience id not found
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0) {
            //experience id not found
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // MANUAL DATE VALIDATION
        // date_start is ahead of date_end
        $wrongOrder = $dateEnd <= $dateStart;
        if ($wrongOrder) {
            return ResponseHelper::responseError(
                'date_start can not be more recent than or equal to date_end',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_start_recent',
                'date_start'
            );
        }

        // Dates periods overlap with the one in the database
        $experienceAvailabilities = $experience->experienceAvailabilities();

        $dateStartOverlaps = $experienceAvailabilities
                ->where('date_start', '<=', $dateStart)
                ->where('date_end', '>=', $dateStart)
                ->get()->count() > 0;

        $dateEndOverlaps = $experienceAvailabilities
                ->where('date_start', '<=', $dateEnd)
                ->where('date_end', '>=', $dateEnd)
                ->get()->count() > 0;

        if ($dateStartOverlaps || $dateEndOverlaps) {
            return ResponseHelper::responseError(
                'This period overlaps with previous ones',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'periods_overlapping',
                'date_start');
        }

        $availability = ['date_start' => $dateStart, 'date_end' => $dateEnd];

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found');
        }

        if (Gate::forUser($account)->denies('post-experience-availability', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Add availability to experience
        $availability = new ExperienceAvailability($availability);
        $experience->experienceAvailabilities()->save($availability);

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($relationshipId);

        // Set attributes to show
        $attributes = $this->transformer->transform($availability);

        return ResponseHelper::responsePost(
            $this->type,
            $availability,
            $attributes,
            [],
            $relationships,
            $availability->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');

        // Validate type & id
        $rules = [
            'type' => 'in:'.$this->type,
            'id' => 'in:'.$id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get Availability from db
        $availability = ExperienceAvailability::find($id);
        if (!isset($availability)){
            return ResponseHelper::responseError(
                'Availability not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'availability_not_found'
            );
        }

        // Validate other data
        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j'
        ];

        $data = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($availability->experience_id);
        if (!isset($experience)){
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // MANUAL DATE VALIDATION
        // date_start is ahead of date_end
        $wrongOrder = $dateEnd <= $dateStart;
        if ($wrongOrder) {
            return ResponseHelper::responseError(
                'date_start can not be more recent than or equal to date_end',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_start_recent',
                'date_start'
            );
        }

        // Dates periods overlap with the one in the database
        $experienceAvailabilities = $experience->experienceAvailabilities();

        $dateStartOverlaps = $experienceAvailabilities
                ->where('date_start', '<=', $dateStart)
                ->where('date_end', '>=', $dateStart)
                ->where('id', '!=', $id)
                ->get()->count() > 0;

        $dateEndOverlaps = $experienceAvailabilities
                ->where('date_start', '<=', $dateEnd)
                ->where('date_end', '>=', $dateEnd)
                ->where('id', '!=', $id)
                ->get()->count() > 0;

        if ($dateStartOverlaps || $dateEndOverlaps) {
            return ResponseHelper::responseError(
                'This period overlaps with previous ones',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'periods_overlapping',
                'date_start'
            );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersions = $experience->experienceVersions();

        // Set additional filters for different account types
        if ($account instanceof Administrator) {
            $experienceVersions = $experienceVersions
                ->where('is_finished', true);
        }
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('patch-experience-availability', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $availability->date_start = $dateStart;
        $availability->date_end = $dateEnd;
        $availability->save();

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($experience->id);

        // Set attributes to show
        $attributes = $this->transformer->transform($availability);

        return ResponseHelper::responseGet(
            $this->type,
            $availability,
            $attributes,
            [],
            $relationships,
            $availability->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $availability = ExperienceAvailability::find($id);

        if (!isset($availability)){
            return ResponseHelper::responseError(
                'Availability not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'availability_not_found'
            );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experience = Experience::find($availability->experience_id);
        if (!isset($experience)){
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $experienceVersions = $experience->experienceVersions();

        // Set additional filters for different account types
        if ($account instanceof Administrator) {
            $experienceVersions = $experienceVersions
                ->where('is_finished', true);
        }
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('delete-experience-availability', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        ExperienceAvailability::destroy($id);

        return response('');
    }

    /**
     * Get relationships array
     *
     * @param $experienceId
     * @return array
     */
    private function getRelationships($experienceId)
    {
        return [
            "experience" => [
                "data" => [
                    "type" => "experiences",
                    "id" => (string) $experienceId
                ]
            ]
        ];
    }
}
