<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\AdditionalService;
use App\Models\Administrator;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AdditionalServiceTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TranslationModels\User as Translator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class AdditionalServicesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'additional-services';
        $this->transformer = new AdditionalServiceTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $name = $request->input('data.attributes.name');
        $perPerson = $request->input('data.attributes.per_person');
        $price = $request->input('data.attributes.price');
        $relationshipType = $request->input('data.relationships.experience.data.type');
        $relationshipId = $request->input('data.relationships.experience.data.id');

        // Validate type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $type
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'name' => 'string|min:1|max:' . Constant::ADDITIONAL_SERVICE_LENGTH,
            'per_person' => 'boolean',
            'price' => 'integer|numeric|min:0',
            'relationship_type' => 'in:experiences',
            'relationship_id' => 'numeric|min:1'
        ];

        $objectType = [
            'name' => $name,
            'per_person' => $perPerson,
            'price' => $price,
            'relationship_type' => $relationshipType,
            'relationship_id' => $relationshipId
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($relationshipId);
        // Check if experience exists
        if (!isset($experience)) {
            //experience id not found
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0) {
            //experience id not found
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Set correct currency
        $captain = $experience->boat()->first()->user()->first();
        $currency = $captain->currency;

        $additionalServiceData = ['name' => $name, 'price' => $price, 'per_person' => $perPerson, 'currency' => $currency];

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('post-additional-service', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized');
        }

        // Add additional service to experience
        $additionalService = new AdditionalService($additionalServiceData);
        $experienceVersion->additionalServices()->save($additionalService);

        // Set attributes to show
        $attributes = $this->transformer->transform($additionalService);

        return ResponseHelper::responsePost(
            $this->type,
            $additionalService,
            $attributes,
            [],
            [],
            $additionalService->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $name = $request->input('data.attributes.name');
        $price = $request->input('data.attributes.price');
        $perPerson = $request->input('data.attributes.per_person');

        // Validate type & id
        $rules = [
            'type' => 'in:' . $this->type,
            'id' => 'in:' . $id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Differentiate Translators from others
        $isTranslator = $account instanceof Translator;
        if ($isTranslator) {
            //TODO
        } else {
            // Validate other data
            $rules = [
                'price' => 'integer|numeric|min:0',
                'per_person' => 'boolean',
                'name' => 'string|max:' . Constant::ADDITIONAL_SERVICE_LENGTH
            ];

            $data = [
                'price' => $price,
                'per_person' => $perPerson,
                'name' => $name
            ];
        }

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get FixedAdditionalServicePrice from db
        $additionalService = AdditionalService::find($id);
        if (!isset($additionalService)) {
            return ResponseHelper::responseError(
                'Additional service not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'additional_service_not_found');
        }

        $experienceVersion = $additionalService->experienceVersion()->first();
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('patch-additional-service', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Edit additional service
        $additionalService->fill($data);
        $additionalService->save();

        // Set attributes to show
        $attributes = $this->transformer->transform($additionalService);

        return ResponseHelper::responseGet(
            $this->type,
            $additionalService,
            $attributes,
            [],
            [],
            $additionalService->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $additionalService = AdditionalService::find($id);

        if (!isset($additionalService)){
            return ResponseHelper::responseError('Fixed additional service price not found', SymfonyResponse::HTTP_NOT_FOUND);
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersion = ExperienceVersion::find($additionalService->experience_version_id);
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('delete-additional-service', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Delete translations and service
        $additionalService->delete();

        return response('');
    }

    protected function getFieldsToValidate($accessory)
    {
        // If a key is not found, assign null
        return [
            'resource_type' => $accessory['type'] ?? null,
            'resource_id' => (int)($accessory['id'] ?? null)
        ];
    }
}
