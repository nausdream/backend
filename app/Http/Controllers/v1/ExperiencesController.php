<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Events\ExperienceVersionPutted;
use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Listeners\CreateOrUpdateSeo;
use App\Mail\AdminChangedExperienceStatusToCaptain;
use App\Mail\CaptainSavedExperienceToAdministrator;
use App\Models\AdditionalService;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\PhotoHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\AdditionalServiceTransformer;
use App\Services\Transformers\AvailabilityTransformer;
use App\Services\Transformers\ExperienceVersionListTransformer;
use App\Services\Transformers\ExperienceVersionTransformer;
use App\Services\Transformers\FeedbackTransformer;
use App\Services\Transformers\FixedAdditionalServicePriceTransformer;
use App\Services\Transformers\PeriodTransformer;
use App\Services\Transformers\PhotoTransformer;
use App\Services\ValueKeyHelper;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use JD\Cloudder\Facades\Cloudder;
use Mail;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ExperiencesController extends Controller
{
    protected $type = 'experiences';
    protected $transformer;

    /**
     * ExperiencesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getCaptainExperiences', 'getBoatExperiences']);
        $this->middleware('jwt')->except(['show', 'getCaptainExperiences', 'getBoatExperiences']);

        $this->transformer = new ExperienceVersionTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('view-experiences-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $area = $account->area;

        // Get empty area if admin has null area
        if (!isset($area)) {
            $area = new Area([
                'point_a_lat' => 0,
                'point_a_lng' => 0,
                'point_b_lat' => 0,
                'point_b_lng' => 0
            ]);
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_EXPERIENCES_PER_PAGE) : Constant::EXPERIENCES_PER_PAGE;
        $page = $request->page;

        // Set editing parameter
        if ($request->editing == 'true') {
            // Get experiences from admin area
            $experiences = ExperienceVersion::all()
                ->sortByDesc('id')
                ->unique('experience_id')
                ->where('departure_lat', '>=', $area->point_a_lat)
                ->where('departure_lat', '<=', $area->point_b_lat)
                ->where('departure_lng', '>=', $area->point_a_lng)
                ->where('departure_lng', '<=', $area->point_b_lng);
        } else {
            // Get experiences from admin area
            $experiences = ExperienceVersion::all()
                ->sortByDesc('id')
                ->unique('experience_id')
                ->where('is_finished', true)
                ->where('departure_lat', '>=', $area->point_a_lat)
                ->where('departure_lat', '<=', $area->point_b_lat)
                ->where('departure_lng', '>=', $area->point_a_lng)
                ->where('departure_lng', '<=', $area->point_b_lng);
        }



        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        // Set language to transformer
        $this->transformer->setLanguage($language);

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($experiences->count() / $perPage)) ?
            min($page, ceil($experiences->count() / $perPage)) :
            1;

        $paginatedExperiences = new LengthAwarePaginator(
            $experiences->slice($perPage * ($currentPage - 1), $perPage),
            $experiences->count(),
            $perPage,
            $currentPage
        );

        $experiences = $paginatedExperiences;
        $experienceVersionListTransformer = new ExperienceVersionListTransformer();

        return response(Paginator::getPaginatedData($experiences, $experienceVersionListTransformer, $perPage, $request));
    }

    /**
     * Display a listing of a captain's experiences.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getCaptainExperiences(Request $request, int $id)
    {
        $user = User::find($id);

        //check if user exist
        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt);
        } else {
            $account = new User();
        }
        if (!isset($account)) {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $result = JwtService::jwtChecker($jwt);
            if ($result instanceof Response) {
                return $result;
            }
            $editing = true;
        } else {
            $editing = false;
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOATS_PER_PAGE) : Constant::BOATS_PER_PAGE;
        $page = $request->page;

        $area = $account->area;

        // Get empty area if admin has null area
        if (!isset($area)) {
            $area = new Area([
                'point_a_lat' => 0,
                'point_a_lng' => 0,
                'point_b_lat' => 0,
                'point_b_lng' => 0
            ]);
        }

        // Get user experienceVersions
        $userExperiences = ExperienceVersion::whereHas('experience', function ($q1) use ($id) {
            $q1->whereHas('boat', function ($q2) use ($id) {
                $q2->where('user_id', '=', $id);
            });
        })->get();

        // Set transformer editing mode
        $experienceVersionListTransformer = new ExperienceVersionListTransformer();
        $experienceVersionListTransformer->setEditingMode($editing);

        // Discriminate by account type setting additional filters
        if ($editing) {
            if ($account instanceof Administrator && Gate::forUser($account)->allows('view-editing-experiences', $user)) {
                $userExperiences = $userExperiences
                    ->sortByDesc('id')->unique('experience_id')
                    ->where('departure_lat', '>=', $area->point_a_lat)
                    ->where('departure_lat', '<=', $area->point_b_lat)
                    ->where('departure_lng', '>=', $area->point_a_lng)
                    ->where('departure_lng', '<=', $area->point_b_lng);
            } elseif (($account instanceof User && Gate::forUser($account)->allows('view-editing-experiences', $user))) {
                $userExperiences = $userExperiences
                    ->sortByDesc('id')
                    ->unique('experience_id');
            } else {
                return ResponseHelper::responseError(
                    'Not authorized to make this request',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }
        } else {
            $userExperiences = $userExperiences
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->sortByDesc('id')
                ->unique('experience_id');
        }

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        // Set language to transformer
        $experienceVersionListTransformer->setLanguage($language);

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($userExperiences->count() / $perPage)) ?
            min($page, ceil($userExperiences->count() / $perPage)) :
            1;

        $paginatedBoats = new LengthAwarePaginator(
            $userExperiences->slice($perPage * ($currentPage - 1), $perPage),
            $userExperiences->count(),
            $perPage,
            $currentPage
        );

        $userExperiences = $paginatedBoats;

        return response(Paginator::getPaginatedData($userExperiences, $experienceVersionListTransformer, $perPage, $request));
    }

    /**
     * Display a listing of a boat's experiences.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getBoatExperiences(Request $request, int $id)
    {
        $boat = Boat::find($id);

        //check if boat exist
        if (!isset($boat)) {
            return ResponseHelper::responseError(
                'Boat does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        $user = $boat->user()->first();
        //check if user exist
        if (!isset($user)) {
            return ResponseHelper::responseError(
                'This boat has no captain associated with it',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_has_no_captain'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt);
        } else {
            $account = new User();
        }
        if (!isset($account)) {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $result = JwtService::jwtChecker($jwt);
            if ($result instanceof Response) {
                return $result;
            }
            $editing = true;
        } else {
            $editing = false;
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOATS_PER_PAGE) : Constant::BOATS_PER_PAGE;
        $page = $request->page;

        $area = $account->area;

        // Get empty area if admin has null area
        if (!isset($area)) {
            $area = new Area([
                'point_a_lat' => 0,
                'point_a_lng' => 0,
                'point_b_lat' => 0,
                'point_b_lng' => 0
            ]);
        }

        // Get user experienceVersions
        $userExperiences = ExperienceVersion::whereHas('experience', function ($query) use ($id) {
            $query->where('boat_id', '=', $id);
        })->get();

        // Set transformer editing mode
        $experienceVersionListTransformer = new ExperienceVersionListTransformer();
        $experienceVersionListTransformer->setEditingMode($editing);

        // Discriminate by account type setting additional filters
        if ($editing) {
            if ($account instanceof Administrator && Gate::forUser($account)->allows('view-editing-experiences', $user)) {
                $userExperiences = $userExperiences
                    ->sortByDesc('id')->unique('experience_id')
                    ->where('departure_lat', '>=', $area->point_a_lat)
                    ->where('departure_lat', '<=', $area->point_b_lat)
                    ->where('departure_lng', '>=', $area->point_a_lng)
                    ->where('departure_lng', '<=', $area->point_b_lng);
            } elseif (($account instanceof User && Gate::forUser($account)->allows('view-editing-experiences', $user))) {
                $userExperiences = $userExperiences
                    ->sortByDesc('id')
                    ->unique('experience_id');
            } else {
                return ResponseHelper::responseError(
                    'Not authorized to make this request',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }
        } else {
            $userExperiences = $userExperiences
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->sortByDesc('id')
                ->unique('experience_id');
        }

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        // Set language to transformer
        $experienceVersionListTransformer->setLanguage($language);

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($userExperiences->count() / $perPage)) ?
            min($page, ceil($userExperiences->count() / $perPage)) :
            1;

        $paginatedBoats = new LengthAwarePaginator(
            $userExperiences->slice($perPage * ($currentPage - 1), $perPage),
            $userExperiences->count(),
            $perPage,
            $currentPage
        );

        $userExperiences = $paginatedBoats;

        return response(Paginator::getPaginatedData($userExperiences, $experienceVersionListTransformer, $perPage, $request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt);
        } else {
            $account = new User();
        }
        if (!isset($account)) {
            $account = new User();
        }

        // Parse request body
        $objectType = $request->input('data.type');
        $relationship = $request->input('data.relationships.boat');
        $relationshipId = $request->input('data.relationships.boat.data.id');
        $relationshipType = $request->input('data.relationships.boat.data.type');

        // Validation for object type
        $rules = [
            'type' => 'in:experiences'
        ];

        $objectType = [
            'type' => $objectType
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validation for relationship
        $rules = [
            'boat' => 'required',
            'boat_id' => 'integer',
            'boat_type' => 'in:boats'
        ];

        $requestFields = [
            'boat' => $relationship,
            'boat_id' => $relationshipId,
            'boat_type' => $relationshipType
        ];

        $validator = \Validator::make($requestFields, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $boat = Boat::find($relationshipId);

        if (!isset($boat)){
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        if (Gate::forUser($account)->denies('create-experience', $boat)){
            return ResponseHelper::responseError(
                'Not authorized to create experience',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Create and save new experience
        $experience = factory(Experience::class)->create();

        // Create and save new experience version
        $experienceVersion = factory(ExperienceVersion::class)->make();
        $experienceVersion->currency = $boat->user()->first()->currency;

        // Relationships
        $experience->experienceVersions()->save($experienceVersion);
        $boat->experiences()->save($experience);

        $data = JsonHelper::createData($this->type, $experience->id);
        return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // Validation
        $input = [
            'currency' => $request->input('currency')
        ];

        $rules = [
            'currency' => 'nullable|exists:currencies,name'
        ];

        $validator = \Validator::make($input, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }


        $experience = Experience::find($id);

        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check if there are experienceversions
        $experienceVersions = $experience->experienceVersions();
        if ($experienceVersions->get()->count() == 0) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $result = JwtService::jwtChecker($jwt);
            if ($result instanceof Response) {
                return $result;
            }
            $editing = true;
        } else {
            $editing = false;
        }

        // Behave differently according to editing parameter
        if ($editing) {

            $experienceVersion = $experienceVersions->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-experience-editing', $experienceVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

        } else {
            $experienceVersion = $experienceVersions
                ->where('is_finished', true)
                ->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-experience', $experienceVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

            $experienceVersion = $experienceVersions
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
            }
        }

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        $this->transformer->setEditingMode($editing);

        // If editing mode, get user language
        if ($editing){
            $language = $experienceVersion
                ->experience()->first()
                ->boat()->first()
                ->user()->first()
                ->language;
        }

        // Set language to transformer
        $this->transformer->setLanguage($language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($experienceVersion, $experience, $request->input('include'));

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($experienceVersion, $experience, $request->input('include'), $language, $input['currency'], $request->input('device'));

        $experienceVersionAttributes = $this->transformer->transform($experienceVersion);

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $experienceVersion,
            $experienceVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $experience->id,
            [
                'experience-availabilities' => 'experience_availabilities',
                'additional-services' => 'additional_services',
                'fixed-additional-service-prices' => 'fixed_additional_service_prices'
            ]

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experience = Experience::find($id);

        // Parse request body
        $objectType = $request->input('data.type');
        $requestBodyId = $request->input('data.id');

        // Check if request body id and endpoint id match
        if ($requestBodyId != $id) {
            return ResponseHelper::responseError(
                'Request id does not match endpoint id',
                SymfonyResponse::HTTP_CONFLICT,
                'id_not_match'
            );
        }

        // Validation for object type
        $rules = [
            'type' => 'in:experiences'
        ];

        $objectType = [
            'type' => $objectType
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check if experience exists
        if (!isset($experience)) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0){
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? $account = new User();
        } else {
            $account = new User();
        }

        $isPut = isset($request->data['attributes']['is_finished']) && (boolean) $request->data['attributes']['is_finished'];
        $isPatch = !$isPut;
        $isChangingStatus = isset($request->data['attributes']['status']);

        // Set behaviour for different request type
        if ($isChangingStatus){
            return $this->changeStatus($request, $account, $experienceVersions, $experience);
        }
        if ($isPut){
            return $this->put($request, $account, $experienceVersions, $experience);
        }
        if ($isPatch){
            return $this->patch($request, $account, $experienceVersions, $experience);
        }
    }

    /**
     * Set is_finished to true and status to PENDING
     *
     * @param Request $request
     * @param $account
     * @param $experienceVersions
     * @param $experience
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function put(Request $request, $account, $experienceVersions, $experience)
    {
        $isAdmin = $account instanceof Administrator;

        // Validation
        $rules = [
            'type' => 'nullable|exists:experience_types,name',
            'title' => 'nullable|string|max:' . Constant::EXPERIENCE_TITLE_LENGTH,
            'description' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'rules' => 'nullable|string|max:' . Constant::EXPERIENCE_RULES_LENGTH,
            'duration' => 'nullable|integer|min:1',
            'departure_port' => 'nullable|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'departure_time' => 'nullable|date_format:H:i:s',
            'arrival_port' => 'nullable|string|max:' . Constant::PORT_RETURN_LENGTH,
            'arrival_time' => 'nullable|date_format:H:i:s',
            'monday' => 'nullable|boolean',
            'tuesday' => 'nullable|boolean',
            'wednesday' => 'nullable|boolean',
            'thursday' => 'nullable|boolean',
            'friday' => 'nullable|boolean',
            'saturday' => 'nullable|boolean',
            'sunday' => 'nullable|boolean',
            'is_fixed_price' => 'nullable|boolean',
            'seats' => 'nullable|integer|min:1',
            'kids' => 'nullable|integer|min:0',
            'babies' => 'nullable|integer|min:0',
            'departure_lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'departure_lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'destination_lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'destination_lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'is_searchable' => 'boolean|nullable',
            'road_stead' => 'boolean|nullable',
            'fixed_menu' => 'boolean|nullable',
            'starter_dish' => 'boolean|nullable',
            'first_dish' => 'boolean|nullable',
            'second_dish' => 'boolean|nullable',
            'side_dish' => 'boolean|nullable',
            'dessert_dish' => 'boolean|nullable',
            'bread' => 'boolean|nullable',
            'wines' => 'boolean|nullable',
            'champagne' => 'boolean|nullable',
            'weather' => 'boolean|nullable',
            'lures' => 'boolean|nullable',
            'fishing_pole' => 'boolean|nullable',
            'fresh_bag' => 'boolean|nullable',
            'ice' => 'boolean|nullable',
            'underwater_baptism' => 'boolean|nullable',
            'down_payment' => 'boolean|nullable',
            'days' => 'nullable|integer|min:1|max:' . Constant::MAX_EXPERIENCE_DAYS,
            'fishing_partition' => 'nullable|exists:fishing_partitions,name',
            'fishing_type' => 'nullable|exists:fishing_types,name',
            'deposit' => 'nullable|integer|min:1',
            'cancellation_max_days' => 'nullable|integer|min:' . Constant::CANCELLATION_MIN_DAYS . '|max:' . Constant::CANCELLATION_MAX_DAYS,
            'deposit_card' => 'boolean|nullable',
            'deposit_check' => 'boolean|nullable',
            'deposit_cash' => 'boolean|nullable',
            'captain_night_aboard' => 'boolean|nullable'
        ];

        // Get fields from request
        $attributes = $this->getExperienceVersionFields($request);

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if there are 2 photos at least
        if ($experienceVersion->experienceVersionPhotos()->get()->count() < 2){
            return ResponseHelper::responseError(
                'Experience need two photos at least',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'two_photos_at_least');
        }

        if (Gate::forUser($account)->denies('put-experience', $experienceVersion)){
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        $experienceVersion->fill($attributes);

        if ($isAdmin) {
            if ($request->input('data.attributes.is_searchable') == false) {
                $experienceVersion->is_searchable = false;
            } else {
                $experienceVersion->is_searchable = true;
            }
        }

        // Set kids and babies they are not set
        if (!isset($experienceVersion->kids)) {
            $experienceVersion->kids = 0;
        }
        if (!isset($experienceVersion->babies)) {
            $experienceVersion->babies = 0;
        }

        // Change status, finish editing and send email
        $experienceVersion->is_finished = true;
        $experienceVersion->status = Constant::STATUS_PENDING;

        if ($account instanceof User){
            dispatch((new SendMail(new CaptainSavedExperienceToAdministrator($experienceVersion)))->onQueue(env('SQS_MAIL')));
        }
        // Set admin_id
        if ($isAdmin){
            $experienceVersion->admin_id = $account->id;
        }
        $experienceVersion->save();

        $seoController = new SeosController();
        $seoController->createOrUpdateSeo(
            $experienceVersion->title,
            $experienceVersion->description,
            $experience->boat()->first()->user()->first()->language,
            $experienceVersion->id
        );

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($experienceVersion, $experience, $request->input('include'));

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($experienceVersion, $experience, $request->input('include'), $account->language);

        $experienceVersionAttributes = $this->transformer->transform($experienceVersion);

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $experienceVersion,
            $experienceVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $experience->id,
            [
                'experience-availabilities' => 'experience_availabilities',
                'additional-services' => 'additional_services',
                'fixed-additional-service-prices' => 'fixed_additional_service_prices'
            ]
        );

    }

    /**
     * Edit experienceVersion fields
     *
     * @param Request $request
     * @param $account
     * @param $experienceVersions
     * @param $experience
     * @return \Illuminate\Http\Response
     */
    public function patch(Request $request, $account, $experienceVersions, $experience)
    {
        // Validation
        $rules = [
            'type' => 'nullable|string|exists:experience_types,name',
            'title' => 'nullable|string|max:' . Constant::EXPERIENCE_TITLE_LENGTH,
            'description' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'rules' => 'nullable|string|max:' . Constant::EXPERIENCE_RULES_LENGTH,
            'duration' => 'nullable|integer|min:1',
            'departure_port' => 'nullable|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'departure_time' => 'nullable|date_format:H:i:s',
            'arrival_port' => 'nullable|string|max:' . Constant::PORT_RETURN_LENGTH,
            'arrival_time' => 'nullable|date_format:H:i:s',
            'monday' => 'nullable|boolean',
            'tuesday' => 'nullable|boolean',
            'wednesday' => 'nullable|boolean',
            'thursday' => 'nullable|boolean',
            'friday' => 'nullable|boolean',
            'saturday' => 'nullable|boolean',
            'sunday' => 'nullable|boolean',
            'is_fixed_price' => 'nullable|boolean',
            'seats' => 'nullable|integer',
            'kids' => 'nullable|integer|min:0',
            'babies' => 'nullable|integer|min:0',
            'departure_lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'departure_lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'destination_lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'destination_lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'road_stead' => 'boolean|nullable',
            'fixed_menu' => 'boolean|nullable',
            'starter_dish' => 'boolean|nullable',
            'first_dish' => 'boolean|nullable',
            'second_dish' => 'boolean|nullable',
            'side_dish' => 'boolean|nullable',
            'dessert_dish' => 'boolean|nullable',
            'bread' => 'boolean|nullable',
            'wines' => 'boolean|nullable',
            'champagne' => 'boolean|nullable',
            'weather' => 'boolean|nullable',
            'lures' => 'boolean|nullable',
            'fishing_pole' => 'boolean|nullable',
            'fresh_bag' => 'boolean|nullable',
            'ice' => 'boolean|nullable',
            'underwater_baptism' => 'boolean|nullable',
            'down_payment' => 'boolean|nullable',
            'days' => 'nullable|integer|min:1|max:' . Constant::MAX_EXPERIENCE_DAYS,
            'fishing_partition' => 'nullable|exists:fishing_partitions,name',
            'fishing_type' => 'nullable|exists:fishing_types,name',
            'deposit' => 'nullable|integer|min:1',
            'cancellation_max_days' => 'nullable|integer|min:' . Constant::CANCELLATION_MIN_DAYS . '|max:' . Constant::CANCELLATION_MAX_DAYS,
            'deposit_card' => 'boolean|nullable',
            'deposit_check' => 'boolean|nullable',
            'deposit_cash' => 'boolean|nullable',
            'captain_night_aboard' => 'boolean|nullable'
        ];

        // Get fields from request
        $attributes = $this->getExperienceVersionFields($request);

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions
            ->get()
            ->last();

        // Check if experience version exists
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('patch-experience', $experienceVersion)){
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Create new experienceVersion
        if ($experienceVersion->status == Constant::STATUS_ACCEPTED){
            $oldExperienceVersion = $experienceVersion;
            $experienceVersion = factory(ExperienceVersion::class)->make();
            $experience->experienceVersions()->save($experienceVersion);

            // Link relationship of old experience version to new experience version

            // Fixed rules
            $experienceRules = $oldExperienceVersion->rules()->get();
            $ids = [];
            foreach ($experienceRules as $experienceRule) {
                $ids[] = $experienceRule->id;
            }
            $experienceVersion->rules()->attach($ids);

            // Additional services
            $experienceAdditionalServices = $oldExperienceVersion->additionalServices()->get();
            foreach ($experienceAdditionalServices as $experienceAdditionalService) {
                $additionalServiceData = [
                    'name' => $experienceAdditionalService->name,
                    'price' => $experienceAdditionalService->price,
                    'per_person' => $experienceAdditionalService->per_person,
                    'currency' => $experienceAdditionalService->currency
                ];
                $additionalService = new AdditionalService($additionalServiceData);
                $experienceVersion->additionalServices()->save($additionalService);
            }

            // Fixed additional service prices
            $experienceFixedAdditionalServicePrices = $oldExperienceVersion->experienceVersionsFixedAdditionalServices()->get();
            foreach ($experienceFixedAdditionalServicePrices as $experienceFixedAdditionalServicePrice) {
                $experienceVersion->fixedAdditionalServices()->attach($experienceFixedAdditionalServicePrice->fixed_additional_service_id, ["price" => $experienceFixedAdditionalServicePrice->price, "per_person" => $experienceFixedAdditionalServicePrice->per_person, "currency" => $experienceFixedAdditionalServicePrice->currency]);
            }

            // Photos
            $boat = $experience->boat()->first();
            $captain = $boat->user()->first();
            $experiencePhotos = $oldExperienceVersion->experienceVersionPhotos()->get();
            foreach ($experiencePhotos as $experiencePhoto) {
                $link = env('CLOUDINARY_SECURE_URL') . '/' .
                    PhotoHelper::getExperiencePhotoPublicId(
                        $captain->id,
                        $boat->id,
                        $experience->id,
                        $experiencePhoto->id
                    );

                $is_cover = $experiencePhoto->is_cover;
                $photo = $experienceVersion->experienceVersionPhotos()->create([
                    'is_cover' => $is_cover
                ]);

                // Create public id
                $url = PhotoHelper::getExperiencePhotoPublicId(
                    $captain->id,
                    $boat->id,
                    $experience->id,
                    $photo->id
                );

                Cloudder::upload(
                    $link,
                    $url,
                    ["format" => "jpg"]
                );
            }
        }

        $experienceVersion->fill($attributes);
        $experienceVersion->save();

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($experienceVersion, $experience, $request->input('include'));

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($experienceVersion, $experience, $request->input('include'), $account->language);

        $experienceVersionAttributes = $this->transformer->transform($experienceVersion);

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $experienceVersion,
            $experienceVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $experience->id,
            [
                'experience-availabilities' => 'experience_availabilities',
                'additional-services' => 'additional_services',
                'fixed-additional-service-prices' => 'fixed_additional_service_prices'
            ]
        );
    }

    /**
     * Set experienceVersion status
     *
     * @param Request $request
     * @param $account
     * @param $experienceVersions
     * @param $experience
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $account, $experienceVersions, $experience)
    {
        // Validation
        $rules = [
            'status' => 'string|in:' . implode(',', Constant::STATUS_NAMES)
        ];

        // Get fields from request
        $attributes = [
            'status' => $request->input('data.attributes.status')
        ];

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $status = ValueKeyHelper::getKeyByValue($attributes['status'], Constant::STATUS_NAMES);

        $experienceVersion = $experienceVersions
            ->where('is_finished', true)
            ->get()
            ->last();

        // Check if there are finished experienceVersions
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if admin is changing from/to an allowed status
        if (Gate::forUser($account)->denies('experience-change-status', [$experienceVersion, $status])){
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        $experienceVersion->status = $status;
        $experienceVersion->admin_id = $account->id;
        $experienceVersion->save();

        // Sends email to captain
        if ($experienceVersion->status != Constant::STATUS_FREEZED) {
            dispatch((new SendMail(new AdminChangedExperienceStatusToCaptain($experienceVersion, $status)))->onQueue(env('SQS_MAIL')));
        }

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($experienceVersion, $experience, $request->input('include'));

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($experienceVersion, $experience, $request->input('include'), $account->language);

        $experienceVersionAttributes = $this->transformer->transform($experienceVersion);

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $experienceVersion,
            $experienceVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $experience->id,
            [
                'experience-availabilities' => 'experience_availabilities',
                'additional-services' => 'additional_services',
                'fixed-additional-service-prices' => 'fixed_additional_service_prices'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the arrays of the relationships elements for the specified experienceVersion
     *
     * @param $experienceVersion
     * @param $experience
     * @param string $includesRequestString
     * @return array
     */
    protected function getRelationships($experienceVersion, $experience, $includesRequestString = '')
    {
        $includesRequest = explode(',', $includesRequestString);
        $relationships = [];

        // ALWAYS INCLUDED
        // Fixed rules
        $experienceRules = $experienceVersion->rules()->get();
        if ($experienceRules->count() > 0) {
            foreach ($experienceRules as $experienceRule) {
                $relationships['rules']['data'][] = JsonHelper::createData('rules', $experienceRule->id);
            }
        }

        // Additional services
        $experienceAdditionalServices = $experienceVersion->additionalServices()->get();
        if ($experienceAdditionalServices->count() > 0) {
            foreach ($experienceAdditionalServices as $experienceAdditionalService) {
                $relationships['additional_services']['data'][] = JsonHelper::createData('additional_services', $experienceAdditionalService->id);
            }
        }

        // Fixed additional service prices
        $experienceFixedAdditionalServicePrices = $experienceVersion->experienceVersionsFixedAdditionalServices()->get();
        if ($experienceFixedAdditionalServicePrices->count() > 0) {
            foreach ($experienceFixedAdditionalServicePrices as $experienceFixedAdditionalServicePrice) {
                $relationships['fixed_additional_service_prices']['data'][] = JsonHelper::createData('fixed-additional-service-prices', $experienceFixedAdditionalServicePrice->id);
            }
        }

        // Photos
        $experiencePhotos = $experienceVersion->experienceVersionPhotos()->get();
        if ($experiencePhotos->count() > 0) {
            foreach ($experiencePhotos as $experiencePhoto) {
                $relationships['photos']['data'][] = JsonHelper::createData('photos', $experiencePhoto->id);
            }
        }

        // Availabilities
        $experienceAvailabilities = $experience->experienceAvailabilities()->get();
        if ($experienceAvailabilities->count() > 0) {
            foreach ($experienceAvailabilities as $experienceAvailability) {
                $relationships['experience_availabilities']['data'][] = JsonHelper::createData('availabilities', $experienceAvailability->id);
            }
        }

        // Peridos
        $experiencePeriods = $experience->periods()->get();
        if ($experiencePeriods->count() > 0) {
            foreach ($experiencePeriods as $experiencePeriod) {
                $relationships['periods']['data'][] = JsonHelper::createData('periods', $experiencePeriod->id);
            }
        }

        //INCLUDED ON REQUEST
        // Feedbacks
        if (in_array('feedbacks', $includesRequest)) {
            $experienceFeedbacks = $experience->experienceFeedbacks()->get();
            if ($experienceFeedbacks->count() > 0) {
                foreach ($experienceFeedbacks as $experienceFeedback) {
                    $relationships['feedbacks']['data'][] = JsonHelper::createData('feedbacks', $experienceFeedback->id);
                }
            }
        }

        return $relationships;
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $experienceVersion
     * @param $experience
     * @param string $includesRequestString
     * @param string $language
     * @param string $currency
     * @param string $device
     * @return array
     */
    protected function getIncludedResources($experienceVersion, $experience, $includesRequestString = '', string $language = null, string $currency = null, string $device = null)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];

        // ALWAYS INCLUDED
        // Additional services
        $experienceAdditionalServices = $experienceVersion->additionalServices()->get();
        if ($experienceAdditionalServices->count() > 0) {
            $additionalServiceTransformer = new AdditionalServiceTransformer();
            $additionalServiceTransformer->setLanguage($language);
            foreach ($experienceAdditionalServices as $experienceAdditionalService) {
                $includes[] = JsonHelper::createData(
                    'additional-services',
                    $experienceAdditionalService->id,
                    $additionalServiceTransformer->transform($experienceAdditionalService, false, $currency)
                );
            }
        }

        // Photos
        $experiencePhotos = $experienceVersion->experienceVersionPhotos()->get();
        if ($experiencePhotos->count() > 0) {
            $photosTransformer = new PhotoTransformer();
            foreach ($experiencePhotos as $experiencePhoto) {
                $includes[] = JsonHelper::createData(
                    'photos',
                    $experiencePhoto->id,
                    $photosTransformer->transform($experiencePhoto, false, $device)
                );
            }
        }

        // Availabilities
        $experienceAvailabilities = $experience->experienceAvailabilities()->orderBy('date_start', 'asc')->get();
        if ($experienceAvailabilities->count() > 0) {
            $availabilitiesTransformer = new AvailabilityTransformer();
            foreach ($experienceAvailabilities as $experienceAvailability) {
                $includes[] = JsonHelper::createData(
                    'experience-availabilities',
                    $experienceAvailability->id,
                    $availabilitiesTransformer->transform($experienceAvailability)
                );
            }
        }

        // Fixed additional service prices
        $experienceFixedAdditionalServicePrices = $experienceVersion->experienceVersionsFixedAdditionalServices()->get();
        if ($experienceFixedAdditionalServicePrices->count() > 0) {
            $fixedAdditionalServicePricesTransformer = new FixedAdditionalServicePriceTransformer();
            foreach ($experienceFixedAdditionalServicePrices as $experienceFixedAdditionalServicePrice) {
                $includes[] = JsonHelper::createData(
                    'fixed-additional-service-prices',
                    $experienceFixedAdditionalServicePrice->id,
                    $fixedAdditionalServicePricesTransformer->transform($experienceFixedAdditionalServicePrice, false, $currency),
                    ['fixed_additional_service' => JsonHelper::createData(
                        'fixed-additional-services',
                        $experienceFixedAdditionalServicePrice->fixedAdditionalService()->first()->id)

                    ]
                );
            }
        }

        // Periods
        $experiencePeriods = $experienceVersion->experience()->first()->periods()->get();
        if ($experiencePeriods->count() > 0) {
            $periodsTransformer = new PeriodTransformer();
            foreach ($experiencePeriods as $experiencePeriod) {
                $includes[] = JsonHelper::createData(
                    'periods',
                    $experiencePeriod->id,
                    $periodsTransformer->transform($experiencePeriod, false, $currency)
                );
            }
        }

        //INCLUDED ON REQUEST
        if (in_array('feedbacks', $includesRequest)) {
            $experienceFeedbacks = $experience->experienceFeedbacks()->get();
            $feedbackTransformer = new FeedbackTransformer();
            if ($experienceFeedbacks->count() > 0) {
                foreach ($experienceFeedbacks as $experienceFeedback) {
                    $includes[] = JsonHelper::createData(
                        'feedbacks',
                        $experienceFeedback->id,
                        $feedbackTransformer->transform($experienceFeedback)
                    );
                }
            }
        }

        return $includes;
    }

    /**
     * Retrieve ExperienceVersion fields
     *
     * @param Request $request
     * @return array
     */
    protected function getExperienceVersionFields(Request $request)
    {
        return [
            'type' => $request->input('data.attributes.type'),
            'title' => $request->input('data.attributes.title'),
            'description' => $request->input('data.attributes.description'),
            'rules' => $request->input('data.attributes.rules'),
            'duration' => $request->input('data.attributes.duration'),
            'departure_port' => $request->input('data.attributes.departure_port'),
            'departure_time' => $request->input('data.attributes.departure_time'),
            'arrival_port' => $request->input('data.attributes.arrival_port'),
            'arrival_time' => $request->input('data.attributes.arrival_time'),
            'monday' => $request->input('data.attributes.monday'),
            'tuesday' => $request->input('data.attributes.tuesday'),
            'wednesday' => $request->input('data.attributes.wednesday'),
            'thursday' => $request->input('data.attributes.thursday'),
            'friday' => $request->input('data.attributes.friday'),
            'saturday' => $request->input('data.attributes.saturday'),
            'sunday' => $request->input('data.attributes.sunday'),
            'is_fixed_price' => $request->input('data.attributes.is_fixed_price'),
            'seats' => $request->input('data.attributes.seats'),
            'kids' => $request->input('data.attributes.kids'),
            'babies' => $request->input('data.attributes.babies'),
            'departure_lat' => $request->input('data.attributes.departure_lat'),
            'departure_lng' => $request->input('data.attributes.departure_lng'),
            'destination_lat' => $request->input('data.attributes.destination_lat'),
            'destination_lng' => $request->input('data.attributes.destination_lng'),
            'road_stead' => $request->input('data.attributes.road_stead'),
            'fixed_menu' => $request->input('data.attributes.fixed_menu'),
            'starter_dish' => $request->input('data.attributes.starter_dish'),
            'first_dish' => $request->input('data.attributes.first_dish'),
            'second_dish' => $request->input('data.attributes.second_dish'),
            'side_dish' => $request->input('data.attributes.side_dish'),
            'dessert_dish' => $request->input('data.attributes.dessert_dish'),
            'bread' => $request->input('data.attributes.bread'),
            'wines' => $request->input('data.attributes.wines'),
            'champagne' => $request->input('data.attributes.champagne'),
            'weather' => $request->input('data.attributes.weather'),
            'lures' => $request->input('data.attributes.lures'),
            'fishing_pole' => $request->input('data.attributes.fishing_pole'),
            'fresh_bag' => $request->input('data.attributes.fresh_bag'),
            'ice' => $request->input('data.attributes.ice'),
            'underwater_baptism' => $request->input('data.attributes.underwater_baptism'),
            'down_payment' => $request->input('data.attributes.down_payment'),
            'days' => $request->input('data.attributes.days'),
            'fishing_partition' => $request->input('data.attributes.fishing_partition'),
            'fishing_type' => $request->input('data.attributes.fishing_type'),
            'deposit' => $request->input('data.attributes.deposit'),
            'cancellation_max_days' => $request->input('data.attributes.cancellation_max_days'),
            'deposit_card' => $request->input('data.attributes.deposit_card'),
            'deposit_check' => $request->input('data.attributes.deposit_check'),
            'deposit_cash' => $request->input('data.attributes.deposit_cash'),
            'captain_night_aboard' => $request->input('data.attributes.captain_night_aboard')
        ];
    }
}
