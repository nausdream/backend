<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\VoucherToSupport;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class VouchersController extends Controller
{
    protected $type = 'vouchers';

    /**
     * VouchersController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check type

        $type = [ 'type' => $request->input('data.type') ];

        $rules = [
            'type' => 'in:' . $this->type
        ];

        $valid = \Validator::make($type, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get request attributes

        $attributes = $this->getPostAttributes($request);

        // Validate offsite booking attributes

        $rules = [
            'first_name' => 'required|string|max:' . Constant::FIRST_NAME_LENGTH,
            'last_name' => 'nullable|string|max:' . Constant::LAST_NAME_LENGTH,
            'departure_date' => 'nullable|date_format:Y-m-d',
            'arrival_date' => 'nullable|date_format:Y-m-d',
            'experience_title' => 'required|string|max:' . Constant::EXPERIENCE_TITLE_LENGTH,
            'departure_port' => 'required|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'arrival_port' => 'required|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'departure_time' => 'required|date_format:H:i:s',
            'arrival_time' => 'required|date_format:H:i:s',
            'adults' => 'required|integer|min:0',
            'kids' => 'required|integer|min:0',
            'babies' => 'required|integer|min:0',
            'price' => 'nullable|integer|min:10',
            'fee' => 'nullable|integer|min:1|max:' . $attributes['price'],
            'language_guest' => 'required|exists:mysql_translation.languages,language',
            'currency_captain' => 'required|exists:currencies,name',
        ];

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check that fee is set if price is set

        if ((isset($attributes['price']) && $attributes['price'] != '') && (!isset($attributes['fee']) || $attributes['fee'] == '')) {
            return ResponseHelper::responseError(
                'The field fee must be set',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'required',
                'fee'
            );
        }

        // Check that departure date is not less than today

        if ((isset($attributes['departure_date']) && $attributes['departure_date'] != '') && Carbon::today()->gt(Carbon::createFromFormat('Y-m-d', $attributes['departure_date']))) {
            return ResponseHelper::responseError(
                'The experience date must be greater than or equal today',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_greater_or_equal_today',
                'departure_date'
            );
        }

        // Check that arrival date is set if departure_date is

        if ((isset($attributes['departure_date']) && $attributes['departure_date'] != '') && !isset($attributes['arrival_date'])) {
            return ResponseHelper::responseError(
                'The field arrival_date must be set',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'required',
                'arrival_date'
            );
        }

        // Check that arrival date is greater or equal to departure_date

        if ((isset($attributes['departure_date']) && $attributes['departure_date'] != '') && Carbon::createFromFormat('Y-m-d', $attributes['departure_date'])->gt(Carbon::createFromFormat('Y-m-d', $attributes['arrival_date']))) {
            return ResponseHelper::responseError(
                'The arrival date has to be greater or equal to departure date',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'arrival_date_greater_or_equal_to_departure_date',
                'arrival_date'
            );
        }

        // Check if number of total seats is equal 0

        $totalSeats = $attributes['adults'] +
            $attributes['kids'] +
            $attributes['babies'];

        if ($totalSeats <= 0) {
            return ResponseHelper::responseError(
                'The number of total seats has to be bigger than 0',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'min',
                'adults'
            );
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorizations

        if (Gate::forUser($account)->allows('post-vouchers')) {

                // Send voucher to support

                if (!isset($attributes['price']) || $attributes['price'] == '') {
                    $attributes['fee'] = 0;
                    $attributes['price'] = 0;
                }


                if (!isset($attributes['departure_date']) || $attributes['departure_date'] == '') {
                    $attributes['departure_date'] = null;
                    $attributes['arrival_date'] = null;
                }

                dispatch((new SendMail(new VoucherToSupport($attributes)))->onQueue(env('SQS_MAIL')));

                return response('', SymfonyResponse::HTTP_OK);
        }

        return ResponseHelper::responseError(
            'Not authorized to create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Retrieve post voucher fields
     *
     * @param Request $request
     * @return array
     */
    protected function getPostAttributes(Request $request)
    {
        // Fetch attributes for voucher

        $attributes = [
            'first_name' => $request->input('data.attributes.first_name'),
            'last_name' => $request->input('data.attributes.last_name'),
            'departure_date' => $request->input('data.attributes.departure_date'),
            'arrival_date' => $request->input('data.attributes.arrival_date'),
            'experience_title' => $request->input('data.attributes.experience_title'),
            'departure_port' => $request->input('data.attributes.departure_port'),
            'arrival_port' => $request->input('data.attributes.arrival_port'),
            'departure_time' => $request->input('data.attributes.departure_time'),
            'arrival_time' => $request->input('data.attributes.arrival_time'),
            'adults' => $request->input('data.attributes.adults'),
            'kids' => $request->input('data.attributes.kids'),
            'babies' => $request->input('data.attributes.babies'),
            'price' => $request->input('data.attributes.price'),
            'fee' => $request->input('data.attributes.fee'),
            'language_guest' => $request->input('data.attributes.language_guest'),
            'currency_captain' => $request->input('data.attributes.currency_captain'),
        ];

        return $attributes;
    }
}
