<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Jobs\SendSms;
use App\Mail\NewMessage;
use App\Mail\NewMessageToAdministrator;
use App\Models\Area;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\Experience;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\ConversationsTransformer;
use App\Services\Transformers\MessageTransformer;
use App\Sms\AlertToCheckEmailToUser;
use App\Sms\NewConversationToCaptain;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class MessagesController extends Controller
{
    protected $type = 'messages';
    protected $transformer;

    /**
     * MessagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show']);
        $this->middleware('jwt');

        $this->transformer = new MessageTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $message = $request->input('data.attributes.message');
        $conversationType = $request->input('data.relationships.conversation.data.type');
        $conversationId = $request->input('data.relationships.conversation.data.id');

        // Validate data
        $rules = [
            'type' => 'required|in:messages',
        ];
        $data = [
            'type' => $type
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'message' => 'required|string|min:1',
            'conversationType' => 'sometimes|required|in:conversations',
        ];

        $messageData = [
            'message' => $message
        ];
        $relationshipsData = [
            'conversationType' => $conversationType
        ];

        $validator = \Validator::make($messageData + $relationshipsData, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Validate conversation id
        $rules = [
            'conversation_id' => 'required|numeric|integer|exists:conversations,id',
        ];
        $data = [
            'conversation_id' => $conversationId
        ];
        $validator = \Validator::make($data, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError(
                'Conversation with ID of ' . $conversationId . ' not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'conversation_not_found'
            );
        }

        // Get Jwt
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt) ?? new User();

        // Get conversation
        $conversation = Conversation::find($conversationId);

        // Check if user is authorized
        if (Gate::forUser($account)->denies('create-messages', $conversation)) {
            return ResponseHelper::responseError(
                'Not authorized to create messages',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Replace message forbidden texts
        $originalMessage = $messageData['message'];
        $message = $messageData['message'];
        $replacement = "***";
        // Email
        $pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
        $message = preg_replace($pattern, $replacement, $message);
        // Url
        $pattern = "#(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))#iS";
        $message = preg_replace($pattern, $replacement, $message);
        // Telephone
        $pattern = "/[0-9]{6,1000}/";
        $message = preg_replace($pattern, $replacement, $message);

        // Sets alert field
        if ($originalMessage != $message){
            $conversation->alert = true;
            $conversation->save();
        }

        // Create new message
        $message = factory(Message::class)->make(['message' => $message]);
        $message->user()->associate($account);
        $message->conversation()->associate($conversation);
        $message->save();
        $conversation->touch();

        // Send emails
        dispatch((new SendMail(new NewMessage($message)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendMail(new NewMessageToAdministrator($message)))->onQueue(env('SQS_MAIL')));

        // Check if it is the first message and if there is to send an sms to captain and/or user
        if ($conversation->messages()->count() == 1 && $account->id == $conversation->user_id) {
            dispatch((new SendSms(new NewConversationToCaptain($conversation)))->onQueue(env('SQS_SMS')));
            if (!$account->is_mail_activated && $account->is_phone_activated) {
                dispatch((new SendSms(new AlertToCheckEmailToUser($account)))->onQueue(env('SQS_SMS')));
            }
        }

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $relationships = $this->getRelationships($conversation, '', $account->language);

        // Fetch message attributes from transformer
        $messageAttributes = $this->transformer->transform($message);

        return ResponseHelper::responsePost(
            $this->transformer->getType(),
            $message,
            $messageAttributes,
            [],
            $relationships,
            null
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $requestId = $request->input('data.id');
        $type = $request->input('data.type');
        $messageText = $request->input('data.attributes.message');

        // Validate data
        $rules = [
            'type' => 'required|in:messages',
            'id' => 'in:' . $id
        ];
        $data = [
            'type' => $type,
            'id' => $requestId
        ];
        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $message = Message::find($id);
        if (!isset($message)) {
            return ResponseHelper::responseError(
                'Message does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'message_not_found'
            );
        }

        $rules = [
            'message' => 'required|string|min:1',
        ];

        $messageData = [
            'message' => $messageText
        ];

        $validator = \Validator::make($messageData, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get Jwt and account from it
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check if user is authorized
        if (Gate::forUser($account)->denies('edit-messages')) {
            return ResponseHelper::responseError(
                'Not authorized to edit messages',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Edit message
        $message->fill($messageData);
        $message->save();

        return ResponseHelper::responseUpdate(
            $this->transformer->getType(),
            $message
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $conversation
     * @param string $includesRequestString
     * @param string $language
     * @return array
     */
    protected function getRelationships($conversation, $includesRequestString = '', string $language = null)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];

        // INCLUDED BY DEFAULT
        // Conversation
        $includes['conversation']['data'] = [JsonHelper::createData(
            'conversations',
            $conversation->id
        )];

        return $includes;
    }
}
