<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class ExperienceTypesController extends Controller
{
    protected $type;

    /**
     * ExperienceTypesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index']);
        $this->middleware('jwt')->except(['index']);
        $this->type = 'experience-types';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $experienceTypes = \App\Models\ExperienceType::all();

        return ResponseHelper::responseIndex($this->type, $experienceTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $boat = Boat::find($id);

        $requestData = $request->input('data');

        // Prepare array of IDs to attach as relationships
        $ids = [];

        // Validate relationships
        for ($i = 0; $i < count($requestData); $i++) {
            // Parse request body
            $objectType = $request->input('data.'.$i.'.type');

            // Validate type
            $rules = [
                'type' => 'in:'.$this->type,
            ];

            $objectType = [
                'type' => $objectType,
            ];

            $validator = \Validator::make($objectType, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
            }

            // Validate id
            $requestBodyId = $request->input('data.'.$i.'.id');

            $rules = [
                'id' => 'required|exists:experience_types,id'
            ];

            $objectType = [
                'id' => $requestBodyId
            ];

            $validator = \Validator::make($objectType, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $ids[] = $requestBodyId;
        }

        // Check if boat exists
        if (!isset($boat)) {
            //boat id not found
            return ResponseHelper::responseError('Boat not found', SymfonyResponse::HTTP_NOT_FOUND, 'boat_not_found');
        }

        // Check if boatversion exists
        $boatVersions = $boat->boatVersions();

        if ($boatVersions->get()->count() <= 0){
            //boat id not found
            return ResponseHelper::responseError('Boat not found', SymfonyResponse::HTTP_NOT_FOUND, 'boat_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        if ($account instanceof Administrator){
            $boatVersions = $boatVersions
                ->where('is_finished', true);
        }
        $boatVersion = $boatVersions->get()->last();

        // Check if boat version exists
        if (!isset($boatVersion)){
            return ResponseHelper::responseError('Boat not found', SymfonyResponse::HTTP_NOT_FOUND, 'boat_not_found');
        }

        if (Gate::forUser($account)->denies('put-boat', $boatVersion)){
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        $boat->experienceTypes()->detach();
        $boat->experienceTypes()->attach($ids);
        $boat->save();

        return response('', SymfonyResponse::HTTP_NO_CONTENT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getFieldsToValidate($accessory)
    {
        // If a key is not found, assign null
        return [
            'resource_type' => $accessory['type'] ?? null,
            'resource_id' => (int) ($accessory['id'] ?? null)
        ];
    }
}
