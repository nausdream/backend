<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\AdditionalService;
use App\Models\Booking;
use App\Models\Coupon;
use App\Models\Experience;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Models\User;
use App\Services\AreaHelper;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\PriceHelper;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use App\Services\ResponseHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class EstimationsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEstimations(Request $request)
    {
        // Fetch parameters
        $parameters = $this->getParameters($request);

        // Validation
        $rules = [
            'experience_id' => 'required',
            'hosts' => 'nullable|integer|min:1',
            'request_date' => 'required|date_format:Y-m-d',
            'currency' => 'required|exists:currencies,name',
        ];

        $validator = \Validator::make($parameters, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for experience existence
        $experience = Experience::find($parameters['experience_id']);
        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check that last experience version of that is not freezed or rejected
        $lastFinishedExperienceVersion = $experience->lastFinishedExperienceVersion();
        if (!isset($lastFinishedExperienceVersion)) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $status = $lastFinishedExperienceVersion->status;
        if ($status == Constant::STATUS_REJECTED || $status == Constant::STATUS_FREEZED) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get last finished and accepted experience version
        $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check for fixed-additional-service-prices existence for related experienceVersion
        if (isset($parameters['fixed_additional_service_prices']) && $parameters['fixed_additional_service_prices'] != '') {
            $fixed_additional_service_prices_ids = explode(',', $parameters['fixed_additional_service_prices']);
        } else {
            $fixed_additional_service_prices_ids = [];
        }

        $fixedServices = [];
        foreach ($fixed_additional_service_prices_ids as $fixed_additional_service_prices_id) {

            // Validation fixed additional services
            $fixedService = ExperienceVersionsFixedAdditionalServices::all()
                ->where('id', $fixed_additional_service_prices_id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();

            // Check for existence
            if (!isset($fixedService)) {
                return ResponseHelper::responseError(
                    'Fixed service does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'fixed_service_not_found'
                );
            }

            // Save model and id for later edits
            $fixedServices[] = $fixedService;
        }

        // Check for additional-services existence for related experienceVersion

        if (isset($parameters['additional_services']) && $parameters['additional_services'] != '') {
            $additional_service_ids = explode(',', $parameters['additional_services']);
        } else {
            $additional_service_ids = [];
        }

        $additionalServices = [];
        foreach ($additional_service_ids as $additional_service_id) {

            // Validation additional services
            $additionalService = AdditionalService::all()
                ->where('id', $additional_service_id)
                ->where('experience_version_id', $experienceVersion->id)
                ->last();

            // Check for existence
            if (!isset($additionalService)) {
                return ResponseHelper::responseError(
                    'Additional service does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'additional_service_not_found'
                );
            }

            // Save model and id for later edits
            $additionalServices[] = $additionalService;
        }

        // Check that request date is inside a period of experience
        $experienceDate = $parameters['request_date'];
        $availability = $experience->experienceAvailabilities()
            ->where('date_start', '<=', $experienceDate)
            ->where('date_end', '>=', $experienceDate)
            ->get()->last();

        if (!isset($availability)) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        // Check that request date is on a day of the week of availability
        $requestDayOfWeek = Carbon::createFromFormat('Y-m-d', $experienceDate)->dayOfWeek;

        if ($requestDayOfWeek == Carbon::MONDAY && !$experienceVersion->monday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::TUESDAY && !$experienceVersion->tuesday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::WEDNESDAY && !$experienceVersion->wednesday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::THURSDAY && !$experienceVersion->thursday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::FRIDAY && !$experienceVersion->friday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::SATURDAY && !$experienceVersion->saturday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::SUNDAY && !$experienceVersion->sunday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        // Check if there is a period valid for request date
        $period = $experience->periods()
            ->where('date_start', null)
            ->where('date_end', null)
            ->get()->last();
        if (!isset($period)) {
            $period = $experience->periods()
                ->where('date_start', '<=', $experienceDate)
                ->where('date_end', '>=', $experienceDate)
                ->get()->last();
            if (!isset($period)) {
                $period = $experience->periods()
                    ->where('is_default', true)
                    ->get()->last();
                if (!isset($period)) {
                    return ResponseHelper::responseError(
                        'The request date is not available',
                        SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                        'experience_date_not_available',
                        'experience_date'
                    );
                }
            }
        }

        // Set entire boat attribute
        $entire_boat = $period->entire_boat;

        // Validate number of seats
        $min = $period->min_person;

        $rules = [
            'hosts' => 'nullable|numeric|min:' . $min . '|max:' . $experienceVersion->seats,
        ];

        $validator = \Validator::make($parameters, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Set seats
        if (isset($parameters['hosts'])) {
            $seats = $parameters['hosts'];
        } else {
            $seats = $experienceVersion->seats;
        }

        // Fetch account
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Validation coupon

        $coupon = null;
        if (isset($parameters['coupon'])) {

            // Check for coupon existence, not consumed and not expired
            $coupon = Coupon::all()
                ->where('name', $parameters['coupon'])
                ->last();

            if (!isset($coupon) || $coupon->is_consumed ||
                ($coupon->expiration_date != null &&
                    !Carbon::today()->lte(Carbon::createFromFormat('Y-m-d', $coupon->expiration_date)))
            ) {
                return ResponseHelper::responseError(
                    'Coupon does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'coupon_not_found',
                    'coupon'
                );
            }

            // Check if coupon is associated with this account and not consumed
            $couponUsers = $coupon->couponsUsers()->get();

            if (!$couponUsers->isEmpty()) {

                if (!isset($account) || !($account instanceof User)) {
                    return ResponseHelper::responseError(
                        'You need to be logged in to use this coupon',
                        SymfonyResponse::HTTP_FORBIDDEN,
                        'not_authorized_coupon_login',
                        'coupon'
                    );
                }

                $couponUser = $couponUsers->where('user_id', $account->id)->first();

                if (!isset($couponUser)) {
                    return ResponseHelper::responseError(
                        'You cannot use this coupon',
                        SymfonyResponse::HTTP_FORBIDDEN,
                        'not_authorized_coupon',
                        'coupon'
                    );
                } else {
                    if ($couponUser->is_coupon_consumed) {
                        return ResponseHelper::responseError(
                            'Coupon does not exist',
                            SymfonyResponse::HTTP_NOT_FOUND,
                            'coupon_not_found',
                            'coupon'
                        );
                    }
                }
            }

            // Check if coupon is associated with this experience
            if (isset($coupon->experience_id) && $coupon->experience_id != $experience->id) {
                return ResponseHelper::responseError(
                    'You cannot use this coupon for this experience',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized_coupon_experience',
                    'coupon'
                );
            }

            // Check if experience of coupon is in the area
            $area = $coupon->area()->first();

            if (isset($area) && !AreaHelper::inArea($area, $experienceVersion->departure_lat, $experienceVersion->departure_lng)) {
                return ResponseHelper::responseError(
                    'You cannot use this coupon for this area',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized_coupon_area',
                    'coupon'
                );
            }
        }

        // Calculate prices
        $services_price = 0;
        $discount = 0;
        $price_minus_discount = 0;

        // Fetch currency of the captain and of the exit user
        $currencyCaptain = $experience->boat()->first()->user()->first()->currency;
        $currencyGuest = $parameters['currency'];

        // Add price for each requested fixed service

        $fixed_additional_service_prices = [];
        foreach ($fixedServices as $fixedService) {
            if ($fixedService->per_person) {
                $fixedServicePrice = $fixedService->price * $seats;
            } else {
                $fixedServicePrice = $fixedService->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($fixedService->currency, $currencyCaptain, $fixedServicePrice));
            $fixed_additional_service_prices[] = [
                'id' => $fixedService->id,
                'name' => $fixedService->fixedAdditionalService()->first()->name,
                'price' => $tempPrice,
                'per_person' => $fixedService->fixedAdditionalService()->first()->per_person,
            ];
            $services_price += $tempPrice;
        }

        // Add price for each requested additional service

        $additional_services = [];
        foreach ($additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $additionalServicePrice = $additionalService->price * $seats;
            } else {
                $additionalServicePrice = $additionalService->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $currencyCaptain, $additionalServicePrice));
            $additional_services[] = [
                'id' => $additionalService->id,
                'name' => $additionalService->name,
                'price' => $tempPrice,
                'per_person' => $additionalService->per_person,
            ];
            $services_price += $tempPrice;
        }

        // Estimate price without discount
        $estimatedPrice = PriceHelper::estimatePrice(
            $seats,
            $services_price,
            $currencyCaptain,
            $period
        );

        // Fetch boat and calculate fee
        $boat = $experience->boat()->first();
        $nausdream_fee = floor(($estimatedPrice / 100) * $boat->fee);

        // Calculate eventual discount
        if (isset($coupon)) {
            if ($coupon->is_percentual) {
                $discount = ceil(($estimatedPrice / 100) * $coupon->amount);
                $price_minus_discount = $estimatedPrice - $discount;
            } else {
                $discount = ceil(PriceHelper::convertPrice($coupon->currency, $currencyCaptain, $coupon->amount));
                $price_minus_discount = $estimatedPrice - $discount;
            }
        } else {
            $price_minus_discount = $estimatedPrice;
        }

        // Calculate each price
        $price_to_pay = $nausdream_fee - $discount;
        $price_on_board = $estimatedPrice - $nausdream_fee;
        $price_per_person = ceil($estimatedPrice / $seats);
        $price_per_person_without_services = ceil(($estimatedPrice - $services_price) / $seats);
        $price_to_pay_guest = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest, $price_to_pay));
        $price_on_board_guest = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest, $price_on_board));
        $price_minus_discount_guest = ceil(PriceHelper::convertPrice($currencyCaptain, $currencyGuest, $price_minus_discount));

        // Create meta
        $meta = JsonHelper::createMetaMessage([
            'price_per_person' => $price_per_person,
            'price_per_person_without_services' => $price_per_person_without_services,
            'services_price' => $services_price,
            'discount' => $discount,
            'price_to_pay' => $price_to_pay,
            'price_on_board' => $price_on_board,
            'price_minus_discount' => $price_minus_discount,
            'price_to_pay_guest' => $price_to_pay_guest,
            'price_on_board_guest' => $price_on_board_guest,
            'price_minus_discount_guest' => $price_minus_discount_guest,
            'entire_boat' => $entire_boat,
            'captain_currency' => $currencyCaptain,
            'hosts' => $seats
        ]);

        // Create eventual includes array
        $includes = [
            'included' => $this->getIncludedResources($fixed_additional_service_prices, $additional_services)
        ];

        // Prepare response content
        if (!empty($includes)) {
            $response = array_merge($meta, $includes);
        } else {
            $response = $meta;
        }

        // Return response
        return response($response, SymfonyResponse::HTTP_OK);
    }

    /**
     * Get the arrays of the included elements
     *
     * @param $fixedAdditionalServicePrices
     * @param $additionalServices
     * @return array
     */
    protected function getIncludedResources($fixedAdditionalServicePrices, $additionalServices)
    {
        $includes = [];

        // Fixed additional services
        foreach ($fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $includes[] = JsonHelper::createData(
                'fixed-additional-service-prices',
                $fixedAdditionalServicePrice['id'],
                [
                    'name' => $fixedAdditionalServicePrice['name'],
                    'price' => $fixedAdditionalServicePrice['price'],
                    'per_person' => $fixedAdditionalServicePrice['per_person']
                ]
            );
        }

        // Additional services

        foreach ($additionalServices as $additionalService) {
            $includes[] = JsonHelper::createData(
                'additional-services',
                $additionalService['id'],
                [
                    'name' => $additionalService['name'],
                    'price' => $additionalService['price'],
                    'per_person' => $additionalService['per_person']
                ]
            );
        }

        return $includes;
    }

    /**
     * Get parameters of the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function getParameters(Request $request)
    {
        return [
            'experience_id' => $request->experience_id,
            'hosts' => $request->hosts,
            'request_date' => $request->request_date,
            'fixed_additional_service_prices' => $request->fixed_additional_service_prices,
            'additional_services' => $request->additional_services,
            'coupon' => $request->coupon,
            'currency' => $request->currency
        ];
    }
}
