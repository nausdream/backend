<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Mail\AdminChangedBoatStatusToCaptain;
use App\Mail\CaptainSavedBoatToAdministrator;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatMaterial;
use App\Models\BoatType;
use App\Models\BoatVersion;
use App\Models\DefaultFee;
use App\Models\Experience;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\PhotoHelper;
use App\Services\ResponseHelper;
use App\Services\StatusHelper;
use App\Services\Transformers\AdditionalServiceTransformer;
use App\Services\Transformers\AvailabilityTransformer;
use App\Services\Transformers\BoatVersionListTransformer;
use App\Services\Transformers\BoatVersionTransformer;
use App\Services\Transformers\ExperienceVersionTransformer;
use App\Services\Transformers\FeedbackTransformer;
use App\Services\Transformers\FixedAdditionalServicePriceTransformer;
use App\Services\Transformers\PhotoTransformer;
use App\Services\ValueKeyHelper;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use JD\Cloudder\Facades\Cloudder;
use Mail;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoatsController extends Controller
{
    protected $type = 'boats';
    protected $transformer;
    protected $listTransformer;

    /**
     * BoatsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getCaptainBoats', 'getBoatByExperience']);
        $this->middleware('jwt')->except(['show', 'getCaptainBoats', 'getBoatByExperience']);

        $this->transformer = new BoatVersionTransformer();
        $this->listTransformer = new BoatVersionListTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('view-boats-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $area = $account->area;

        // Get empty area if admin has null area
        if (!isset($area)) {
            $area = new Area([
                'point_a_lat' => 0,
                'point_a_lng' => 0,
                'point_b_lat' => 0,
                'point_b_lng' => 0
            ]);
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOATS_PER_PAGE) : Constant::BOATS_PER_PAGE;
        $page = $request->page;

        // Get experiences from admin area
        $boats = BoatVersion::all()
            ->where('is_finished', true)
            ->sortByDesc('id')
            ->unique('boat_id')
            ->where('lat', '>=', $area->point_a_lat)
            ->where('lat', '<=', $area->point_b_lat)
            ->where('lng', '>=', $area->point_a_lng)
            ->where('lng', '<=', $area->point_b_lng);

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        // Set language to transformer
        $this->transformer->setLanguage($language);

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($boats->count() / $perPage)) ?
            min($page, ceil($boats->count() / $perPage)) :
            1;

        $paginatedBoats = new LengthAwarePaginator(
            $boats->slice($perPage * ($currentPage - 1), $perPage),
            $boats->count(),
            $perPage,
            $currentPage
        );

        $boats = $paginatedBoats;

        return response(Paginator::getPaginatedData($boats, $this->listTransformer, $perPage, $request));
    }

    /**
     * Display a listing of a captain's experiences.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getCaptainBoats(Request $request, int $id)
    {
        $user = User::find($id);

        //check if user exist
        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $result = JwtService::jwtChecker($jwt);
            if ($result instanceof Response) {
                return $result;
            }
            $editing = true;
        } else {
            $editing = false;
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOATS_PER_PAGE) : Constant::BOATS_PER_PAGE;
        $page = $request->page;

        $area = $account->area;

        // Get empty area if admin has null area
        if (!isset($area)) {
            $area = new Area([
                'point_a_lat' => 0,
                'point_a_lng' => 0,
                'point_b_lat' => 0,
                'point_b_lng' => 0
            ]);
        }

        // Get user boatVersions
        $userBoats = BoatVersion::whereHas('boat', function ($query) use ($id) {
            $query->where('user_id', '=', $id);
        })->get();

        // Set transformer editing mode
        $this->transformer->setEditingMode($editing);

        // Discriminate by account type setting additional filters
        if ($editing) {
            if ($account instanceof Administrator && Gate::forUser($account)->allows('view-editing-boats', $user)) {
                $userBoats = $userBoats
                    ->where('is_finished', true)
                    ->sortByDesc('id')->unique('boat_id')
                    ->where('lat', '>=', $area->point_a_lat)
                    ->where('lat', '<=', $area->point_b_lat)
                    ->where('lng', '>=', $area->point_a_lng)
                    ->where('lng', '<=', $area->point_b_lng);
            } elseif (($account instanceof User && Gate::forUser($account)->allows('view-editing-boats', $user))) {
                $userBoats = $userBoats
                    ->sortByDesc('id')
                    ->unique('boat_id');
            } else {
                return ResponseHelper::responseError(
                    'Not authorized to make this request',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }
        } else {
            // If not in editing mode, get only finished and accepted boatVersions
            $userBoats = $userBoats
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->sortByDesc('id')
                ->unique('boat_id');
        }

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        // Set language to transformer
        $this->transformer->setLanguage($language);

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($userBoats->count() / $perPage)) ?
            min($page, ceil($userBoats->count() / $perPage)) :
            1;

        $paginatedBoats = new LengthAwarePaginator(
            $userBoats->slice($perPage * ($currentPage - 1), $perPage),
            $userBoats->count(),
            $perPage,
            $currentPage
        );

        $userBoats = $paginatedBoats;

        return response(Paginator::getPaginatedData($userBoats, $this->listTransformer, $perPage, $request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt);
        } else {
            $account = new User();
        }
        if (!isset($account)) {
            $account = new User();
        }

        // Parse request body
        $objectType = $request->input('data.type');
        $relationshipId = $request->input('data.relationships.user.data.id');
        $relationshipType = $request->input('data.relationships.user.data.type');

        // Validation for object type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $objectType
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validation for relationship
        $rules = [
            'user_id' => 'integer|numeric',
            'user_type' => 'in:users'
        ];

        $requestFields = [
            'user_id' => $relationshipId,
            'user_type' => $relationshipType
        ];

        $validator = \Validator::make($requestFields, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::find($relationshipId);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        if (Gate::forUser($account)->denies('create-boat', $user)) {
            return ResponseHelper::responseError(
                'Not authorized to create boat',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Create and save new boat
        $boat = factory(Boat::class)->create();

        // Create and save new boat version
        $boatVersion = factory(BoatVersion::class)->make();

        // Relationships
        $boat->boatVersions()->save($boatVersion);
        $user->boats()->save($boat);

        $data = JsonHelper::createData($this->type, $boat->id);
        return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $boat = Boat::find($id);

        if (!isset($boat)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Check if there are boatversions
        $boatVersions = $boat->boatVersions();
        if ($boatVersions->get()->count() == 0) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $result = JwtService::jwtChecker($jwt);
            if ($result instanceof Response) {
                return $result;
            }
            $editing = true;
        } else {
            $editing = false;
        }

        // Behave differently according to editing parameter
        if ($editing) {
            // Additional filter for admin
            if ($account instanceof Administrator) {
                // Filter by finished boatversion
                $finishedBoatVersions = $boatVersions
                    ->where('is_finished', true);

                $boatVersions = $finishedBoatVersions;
            }

            $boatVersion = $boatVersions->get()->last();

            if (!isset($boatVersion)) {
                return ResponseHelper::responseError(
                    'Boat not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'boat_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-boat-editing', $boatVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

        } else {
            $boatVersion = $boatVersions
                ->where('is_finished', true)
                ->get()->last();

            if (!isset($boatVersion)) {
                return ResponseHelper::responseError(
                    'Boat not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'boat_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-boat', $boatVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

            $boatVersion = $boatVersions
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->get()->last();

            if (!isset($boatVersion)) {
                return ResponseHelper::responseError(
                    'Boat not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'boat_not_found');
            }
        }

        // If language is not set or empty, get account language
        $language = isset($request->language) && !empty($request->language) ? $request->language : $account->language;
        $this->transformer->setEditingMode($editing);

        // If editing mode, get user language
        if ($editing) {
            $language = $boatVersion
                ->boat()->first()
                ->user()->first()
                ->language;
        }

        // Set language to transformer
        $this->transformer->setLanguage($language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($boatVersion, $boat);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($boatVersion, $boat, $request->input('include'), $language);

        $boatVersionAttributes = $this->transformer->transform($boatVersion);

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $boatVersion,
            $boatVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $boat->id
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $boat = Boat::find($id);

        // Parse request body
        $objectType = $request->input('data.type');
        $requestBodyId = $request->input('data.id');

        // Check if request body id and endpoint id match
        if ($requestBodyId != $id) {
            return ResponseHelper::responseError(
                'Request id does not match endpoint id',
                SymfonyResponse::HTTP_CONFLICT,
                'id_not_match'
            );
        }

        // Validation for object type
        $rules = [
            'type' => 'in:boats'
        ];

        $objectType = [
            'type' => $objectType
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check if boat exists
        if (!isset($boat)) {
            //boat id not found
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Check if boatversion exists
        $boatVersions = $boat->boatVersions();

        if ($boatVersions->get()->count() <= 0) {
            //boat id not found
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? $account = new User();
        } else {
            $account = new User();
        }

        $isPut = isset($request->data['attributes']['is_finished']) && (boolean)$request->data['attributes']['is_finished'];
        $isPatch = !$isPut;
        $isChangingStatus = array_key_exists('status', $request->data['attributes']);

        // Set behaviour for different request type
        if ($isChangingStatus) {
            return $this->changeStatus($request, $account, $boatVersions, $boat);
        }
        if ($isPut) {
            return $this->put($request, $account, $boatVersions, $boat);
        }
        if ($isPatch) {
            return $this->patch($request, $account, $boatVersions, $boat);
        }
    }

    /**
     * Set is_finished to true and status to PENDING
     *
     * @param Request $request
     * @param $account
     * @param $boatVersions
     * @param $boat
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function put(Request $request, $account, $boatVersions, $boat)
    {
        $isAdmin = $account instanceof Administrator;

        // Validation
        $rules = [
            'production_site' => 'max:' . Constant::PRODUCTION_SITE_LENGTH,
            'model' => 'max:' . Constant::BOAT_MODEL_LENGTH,
            'name' => 'max:' . Constant::BOAT_NAME_LENGTH,
            'description' => 'nullable|string|max:' . Constant::BOAT_DESCRIPTION_LENGTH,
            'rules' => 'max:' . Constant::BOAT_RULES_LENGTH,
            'indications' => 'nullable|string|min:2|max:' . Constant::BOAT_INDICATIONS_LENGTH,
            'construction_year' => 'nullable|integer|min:0|max:' . date('Y'),
            'restoration_year' => 'nullable|integer|min:0|max:' . date('Y'),
            'type' => 'exists:boat_types,name|nullable',
            'material' => 'exists:boat_materials,name|nullable',
            'length' => 'integer|nullable|min:1|max:65535',
            'has_insurance' => 'boolean|nullable',
            'motors_quantity' => 'integer|min:1|nullable',
            'motor_type' => 'exists:motor_types,name|nullable',
            'docking_place' => 'max:' . Constant::DOCKING_PLACE_LENGTH,
            'lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'seats' => 'integer|min:0|nullable',
            'berths' => 'integer|min:0|nullable',
            'cabins' => 'integer|min:0|nullable',
            'toilets' => 'integer|min:0|nullable',
            'events' => 'boolean'
        ];


        // Get fields from request
        $attributes = $this->getBoatVersionFields($request);

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Validate attribute that only admin can set

        if ($isAdmin) {
            $boatAttributes['fee'] = $request->input('data.attributes.fee');
            $boatAttributes['is_luxury'] = $request->input('data.attributes.is_luxury');

            $rules = [
                'fee' => 'integer|numeric|max:'.Constant::MAX_FEE_VALUE.'|min:'.Constant::MIN_FEE_VALUE,
                'is_luxury' => 'boolean',
            ];

            $validator = \Validator::make($boatAttributes, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        // Set additional filters for different account types
        if ($isAdmin) {
            $boatVersions = $boatVersions
                ->where('is_finished', true);
        }
        $boatVersion = $boatVersions->get()->last();

        // Check if boat version exists
        if (!isset($boatVersion)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Check if there are all 3 photos at least
        if (
            !isset($boatVersion->version_external_photo) ||
            !isset($boatVersion->version_internal_photo) ||
            !isset($boatVersion->version_sea_photo)
        ) {
            return ResponseHelper::responseError(
                'Boat photos are not set',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'boat_photos_not_set'
            );
        }

        if (Gate::forUser($account)->denies('put-boat', $boatVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
                );
        }

        $boatVersion->fill($attributes);

        // Change status, finish editing and send email
        if ($account instanceof User) {

            // Set fee if it's not already set
            if (!isset($boat->fee)){
                $boat->fee = DefaultFee::all()
                    ->where('point_a_lat', '<=', $boatVersion->lat)
                    ->where('point_b_lat', '>=', $boatVersion->lat)
                    ->where('point_a_lng', '<=', $boatVersion->lng)
                    ->where('point_b_lng', '>=', $boatVersion->lng)
                    ->first()->fee;
            }

            $boatVersion->is_finished = true;
            $boatVersion->status = Constant::STATUS_PENDING;

            dispatch((new SendMail(new CaptainSavedBoatToAdministrator($boatVersion)))->onQueue(env('SQS_MAIL')));
        }
        // Set admin_id on boatVersion, fee and is_luxury on boat
        if ($isAdmin) {
            $boatVersion->admin_id = $account->id;
            $boat->fee = $boatAttributes['fee'];
            $boat->is_luxury = $boatAttributes['is_luxury'];
        }
        $boatVersion->save();
        $boat->save();

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($boatVersion, $boat);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($boatVersion, $boat, $request->input('include'), $account->language);

        $boatVersionAttributes = $this->transformer->transform($boatVersion, false, $request->input('device'));

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $boatVersion,
            $boatVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $boat->id
        );

    }

    /**
     * Edit boatVersion fields
     *
     * @param Request $request
     * @param $account
     * @param $boatVersions
     * @param $boat
     * @return \Illuminate\Http\Response
     */
    public function patch(Request $request, $account, $boatVersions, $boat)
    {
        // Validation
        $rules = [
            'production_site' => 'max:' . Constant::PRODUCTION_SITE_LENGTH,
            'model' => 'max:' . Constant::BOAT_MODEL_LENGTH,
            'name' => 'max:' . Constant::BOAT_NAME_LENGTH,
            'description' => 'max:' . Constant::BOAT_DESCRIPTION_LENGTH,
            'rules' => 'max:' . Constant::BOAT_RULES_LENGTH,
            'indications' => 'max:' . Constant::BOAT_INDICATIONS_LENGTH,
            'construction_year' => 'nullable|integer|min:0|max:' . date('Y'),
            'restoration_year' => 'nullable|integer|min:0|max:' . date('Y'),
            'type' => 'exists:boat_types,name|nullable',
            'material' => 'exists:boat_materials,name|nullable',
            'length' => 'integer|nullable|min:1|max:32767',
            'has_insurance' => 'boolean|nullable',
            'motors_quantity' => 'integer|min:1|nullable',
            'motor_type' => 'exists:motor_types,name|nullable',
            'docking_place' => 'max:' . Constant::DOCKING_PLACE_LENGTH,
            'lat' => 'nullable|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'lng' => 'nullable|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'seats' => 'integer|min:0|nullable',
            'berths' => 'integer|min:0|nullable',
            'cabins' => 'integer|min:0|nullable',
            'toilets' => 'integer|min:0|nullable',
            'events' => 'boolean'
        ];

        // Get fields from request
        $attributes = $this->getBoatVersionFields($request);

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Set additional filters for different account types
        if ($account instanceof User) {
            $boatVersion = $boatVersions
                ->get()
                ->last();

            // Check if boat version exists
            if (!isset($boatVersion)) {
                return ResponseHelper::responseError(
                    'Boat not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'boat_not_found'
                );
            }
        } else {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        if (Gate::forUser($account)->denies('patch-boat', $boatVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Create new boatVersion
        if ($boatVersion->status == Constant::STATUS_ACCEPTED) {
            $oldBoatVersion = $boatVersion;
            $boatVersion = factory(BoatVersion::class)->make();
            $boat->boatVersions()->save($boatVersion);

            // Link relationship of old boat version to new boat version

            // Accessories
            $accessories = $oldBoatVersion->accessories()->get();
            $accessoryIds = [];
            foreach ($accessories as $accessory) {
                $accessoryIds[] = $accessory->id;
            }

            $boatVersion->accessories()->attach($accessoryIds);

            // Photos
            $captain = $boat->user()->first();

            if (isset($oldBoatVersion->version_external_photo)) {
                $version = $this->getNewPhotoVersion($boat, $captain, 'external', $oldBoatVersion->version_external_photo);
                $boatVersion->version_external_photo = $version;
            }

            if (isset($oldBoatVersion->version_internal_photo)) {
                $version = $this->getNewPhotoVersion($boat, $captain, 'internal', $oldBoatVersion->version_internal_photo);
                $boatVersion->version_internal_photo = $version;
            }

            if (isset($oldBoatVersion->version_sea_photo)) {
                $version = $this->getNewPhotoVersion($boat, $captain, 'sea', $oldBoatVersion->version_sea_photo);
                $boatVersion->version_sea_photo = $version;
            }

            $boatVersion->save();

        }

        $boatVersion->fill($attributes);
        $boatVersion->save();

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($boatVersion, $boat);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($boatVersion, $boat, $request->input('include'), $account->language);

        $boatVersionAttributes = $this->transformer->transform($boatVersion, false, $request->input('device'));

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $boatVersion,
            $boatVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $boat->id
        );
    }

    /**
     * Edit boatVersion fields
     *
     * @param Request $request
     * @param $account
     * @param $boatVersions
     * @param $boat
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $account, $boatVersions, $boat)
    {
        // Validation
        $status_names = Constant::STATUS_NAMES;
        unset($status_names[4]);
        $rules = [
            'status' => 'string|in:' . implode(',', $status_names),
            'admin_text' => 'string|nullable'
        ];

        // Get fields from request
        $attributes = [
            'status' => $request->input('data.attributes.status'),
            'admin_text' => $request->input('data.attributes.admin_text')
        ];

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $status = ValueKeyHelper::getKeyByValue($attributes['status'], Constant::STATUS_NAMES);

        // If setting ACCEPTED_CONDITIONALLY status, add admin_text validation rules
        if ($status == Constant::STATUS_ACCEPTED_CONDITIONALLY || $status == Constant::STATUS_REJECTED){
            $rules = [];
            $rules['admin_text'] = 'string|required';
        }

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $boatVersion = $boatVersions
            ->where('is_finished', true)
            ->get()
            ->last();

        // Check if there are finished boatVersions
        if (!isset($boatVersion)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Check if admin is changing from/to an allowed status
        if (Gate::forUser($account)->denies('boat-change-status', [$boatVersion, $status])) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $boatVersion->status = $status;
        $boatVersion->admin_id = $account->id;
        $boatVersion->admin_text = $attributes['admin_text'];
        $boatVersion->save();

        // Sends email to captain
        dispatch((new SendMail(new AdminChangedBoatStatusToCaptain(
            $boat->user()->first(),
            $boatVersion,
            $status,
            $boatVersion->admin_text)))->onQueue(env('SQS_MAIL')));

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Fetch relationships to include on data
        $relationshipsResources = $this->getRelationships($boatVersion, $boat);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($boatVersion, $boat, $request->input('include'), $account->language);

        $boatVersionAttributes = $this->transformer->transform($boatVersion, false, $request->input('device'));

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $boatVersion,
            $boatVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $boat->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the boat of the specified experience
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getBoatByExperience(Request $request, $id)
    {
        $experience = Experience::find($id);

        // Check if experience exists

        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get boat of the experience

        $boat = $experience->boat()->first();
        $boatVersion = $boat->lastFinishedBoatVersion();

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Fetch for relationships to include on data

        $relationshipsResources = $this->getRelationships($boatVersion, $boat);

        // Fetch for data to includes to response

        $includedResources = $this->getIncludedResources($boatVersion, $boat, $request->input('include'));

        if ($account instanceof Administrator) {
            $is_admin = true;
        } else {
            $is_admin = false;
        }

        $boatVersionAttributes = $this->transformer->transform($boatVersion, $is_admin, $request->input('device'));

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $boatVersion,
            $boatVersionAttributes,
            $includedResources,
            $relationshipsResources,
            $boat->id
        );

    }

    /**
     * Get the arrays of the relationships elements for the specified experienceVersion
     *
     * @param $boatVersion
     * @param $boat
     * @return array
     */
    protected function getRelationships($boatVersion, $boat)
    {
        $relationships = [];

        // ALWAYS INCLUDED
        // Accessories
        $accessories = $boatVersion->accessories()->get();
        if ($accessories->count() > 0) {
            foreach ($accessories as $accessory) {
                $relationships['accessories']['data'][] = JsonHelper::createData('accessories', $accessory->id);
            }
        }

        // Experience types
        $boatExperienceTypes = $boat->experienceTypes()->get();
        if ($boatExperienceTypes->count() > 0) {
            foreach ($boatExperienceTypes as $boatExperienceType) {
                $relationships['experience_types']['data'][] = JsonHelper::createData('experience-types', $boatExperienceType->id);
            }
        }

        return $relationships;
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $boatVersion
     * @param $boat
     * @param string $includesRequestString
     * @param string $language
     * @return array
     */
    protected function getIncludedResources($boatVersion, $boat, $includesRequestString = '', string $language = null)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];

        // ALWAYS INCLUDED
        // Availabilities
        $boatAvailabilities = $boat->boatAvailabilities()->get();
        if ($boatAvailabilities->count() > 0) {
            $availabilitiesTransformer = new AvailabilityTransformer();
            foreach ($boatAvailabilities as $boatAvailability) {
                $includes[] = JsonHelper::createData(
                    'availabilities',
                    $boatAvailability->id,
                    $availabilitiesTransformer->transform($boatAvailability)
                );
            }
        }

        //INCLUDED ON REQUEST
        if (in_array('feedbacks', $includesRequest)) {
            $boatFeedbacks = $boat->boatFeedbacks()->get();
            $feedbackTransformer = new FeedbackTransformer();
            if ($boatFeedbacks->count() > 0) {
                foreach ($boatFeedbacks as $boatFeedback) {
                    $includes[] = JsonHelper::createData(
                        'feedbacks',
                        $boatFeedback->id,
                        $feedbackTransformer->transform($boatFeedback)
                    );
                }
            }
        }

        return $includes;
    }

    /**
     * Retrieve BoatVersion fields
     *
     * @param Request $request
     * @return array
     */
    protected function getBoatVersionFields(Request $request)
    {
        return [
            'production_site' => $request->input('data.attributes.production_site'),
            'model' => $request->input('data.attributes.model'),
            'name' => $request->input('data.attributes.name'),
            'description' => $request->input('data.attributes.description'),
            'indications' => $request->input('data.attributes.indications'),
            'rules' => $request->input('data.attributes.rules'),
            'construction_year' => $request->input('data.attributes.construction_year'),
            'restoration_year' => $request->input('data.attributes.restoration_year'),
            'type' => $request->input('data.attributes.type'),
            'material' => $request->input('data.attributes.material'),
            'length' => $request->input('data.attributes.length'),
            'has_insurance' => $request->input('data.attributes.has_insurance'),
            'motors_quantity' => $request->input('data.attributes.motors_quantity'),
            'motor_type' => $request->input('data.attributes.motor_type'),
            'docking_place' => $request->input('data.attributes.docking_place'),
            'lat' => $request->input('data.attributes.lat'),
            'lng' => $request->input('data.attributes.lng'),
            'seats' => $request->input('data.attributes.seats'),
            'berths' => $request->input('data.attributes.berths'),
            'cabins' => $request->input('data.attributes.cabins'),
            'toilets' => $request->input('data.attributes.toilets'),
            'events' => $request->input('data.attributes.events'),
        ];
    }

    /**
     * @param $boat
     * @param $captain
     * @param $photoType
     * @param $photoVersion
     * @return mixed
     */
    public function getNewPhotoVersion($boat, $captain, $photoType, $photoVersion)
    {
        $publicId = PhotoHelper::getBoatPhotoPublicId($captain->id, $boat->id, $photoType);

        $link = Cloudder::secureShow($publicId, [
            'version' => $photoVersion,
            'format' => 'jpg',
            'width' => null,
            'height' => null,
            'angle' => 'exif'
        ]);

        $url = PhotoHelper::getBoatPhotoPublicId($captain->id, $boat->id, $photoType);

        Cloudder::upload(
            $link,
            $url,
            ["format" => "jpg"]
        );
        $arrayResult = Cloudder::getResult();
        return $arrayResult['version'];
    }
}
