<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AccessoryTransformer;
use App\Services\Transformers\RulesTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class FixedRulesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index']);
        $this->middleware('jwt')->except(['index']);
        $this->type = 'fixed-rules';
        $this->transformer = new RulesTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fixedRules = \App\Models\Rule::all();

        return ResponseHelper::responseIndex($this->type, $fixedRules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $experience = Experience::find($id);

        $requestData = $request->input('data');

        // Prepare array of IDs to attach as relationships
        $ids = [];

        // Validate relationships
        for ($i = 0; $i < count($requestData); $i++) {
            // Parse request body
            $objectType = $request->input('data.'.$i.'.type');

            // Validate type
            $rules = [
                'type' => 'in:fixed-rules',
            ];

            $objectType = [
                'type' => $objectType,
            ];

            $validator = \Validator::make($objectType, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
            }

            // Validate id
            $requestBodyId = $request->input('data.'.$i.'.id');

            $rules = [
                'id' => 'exists:rules,id'
            ];

            $objectType = [
                'id' => $requestBodyId
            ];

            $validator = \Validator::make($objectType, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $ids[] = $requestBodyId;
        }

        // Check if experience exists
        if (!isset($experience)) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0){
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('put-experience', $experienceVersion)){
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        $experienceVersion->rules()->detach();
        $experienceVersion->rules()->attach($ids);
        $experienceVersion->save();

        return response('', SymfonyResponse::HTTP_NO_CONTENT);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function getFieldsToValidate($accessory)
    {
        // If a key is not found, assign null
        return [
            'resource_type' => $accessory['type'] ?? null,
            'resource_id' => (int) ($accessory['id'] ?? null)
        ];
    }
}
