<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Experience;
use App\Models\ExperienceVersion;
use App\Models\Seo;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\SeoHelper;
use App\Services\Transformers\SeoListTransformer;
use App\Services\Transformers\SeoTransformer;
use App\TranslationModels\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TranslationModels\User as Translator;
use App\Services\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Support\Facades\DB;
use Gate;

class SeosController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * SeosController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['getSeoByExperience', 'getSlugUrlByTitle', 'getSeoBySlugUrl', 'show', 'index']);
        $this->middleware('jwt')->except('getSeoBySlugUrl');
        $this->type = 'seos';
        $this->transformer = new SeoTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Fetch parameters
        $parameters = $this->getIndexParameters($request);

        // Validation
        $rules = [
            'language' => 'required|exists:mysql_translation.languages,language',
            'translated' => 'nullable|boolean'
        ];

        $validator = \Validator::make($parameters, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Verify authorization

        $language = $parameters['language'];
        $translated = $parameters['translated'];

        if (Gate::forUser($account)->allows('get-seo-list', $language)) {

            // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
            // between per_page and MAX_SEOS_PER_PAGE. If per_page is not set, uses default SEOS_PER_PAGE
            $perPage = $request->per_page;
            $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_SEOS_PER_PAGE) : Constant::SEOS_PER_PAGE;
            $page = $request->page;

            $experienceVersions = ExperienceVersion::all()
                ->sortByDesc('id')
                ->unique('experience_id')
                ->where('is_finished', true);

            $seos = collect();

            foreach ($experienceVersions as $experienceVersion) {

                // Check if fallback language seo exist
                $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();

                // Check if captain language seo exist
                if(!isset($seo)) {
                    $seo = $experienceVersion->seos()->where('language', $experienceVersion->experience()->first()->boat()->first()->user()->first()->language)->first();
                }

                // Check if any seo exist
                if(!isset($seo)) {
                    $seo = $experienceVersion->seos()->first();
                }

                if (isset($seo)) {

                    // Check if is set translated parameter
                    if (isset($translated)) {
                        // Check if exist translation
                        $seoTranslated = Seo::all()->where('experience_version_id', $experienceVersion->id)->where('language', $language)->first();

                        if (($translated && isset($seoTranslated)) || (!$translated && !isset($seoTranslated))) {
                            $seos->push($seo);
                        }
                    } else {
                        $seos->push($seo);
                    }
                }


            }

            // Set page to fetch and if value is out of range, fetch page 1
            $currentPage = (
                isset($page) &&
                (int)$page > 0 &&
                (int)$page <= ceil($seos->count() / $perPage)) ?
                min($page, ceil($seos->count() / $perPage)) :
                1;

            $paginatedUsers = new LengthAwarePaginator(
                $seos->slice($perPage * ($currentPage - 1), $perPage),
                $seos->count(),
                $perPage,
                $currentPage
            );

            $users = $paginatedUsers;
            $transformerForPaginatedSeos = new SeoListTransformer();
            $transformerForPaginatedSeos->setRequestLanguage($language);
            $transformerForPaginatedSeos->setTranslated($translated);

            $parameterQuery = $this->getQuery($language, $translated);

            return response(Paginator::getPaginatedData($users, $transformerForPaginatedSeos, $perPage, $request, $parameterQuery), SymfonyResponse::HTTP_OK);
        }

        return ResponseHelper::responseError(
            'You cannot see this list',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get attributes request

        $attributes = $this->getPostRequestAttributes($request);

        // Validate type

        $rules = [
            'type' => 'in:seos',
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Write common validation rules

        $rules = [
            'title' => 'required|string|min:2|max:' . Constant::SEO_TITLE_LENGTH,
            'description' => 'required|string|min:2|max:' . Constant::SEO_DESCRIPTION_LENGTH,
            'relationship_type' => 'in:experiences'
        ];

        // Validate

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for relationship existence

        $experience = Experience::find($attributes['relationship_id']);

        if (!isset($experience)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Fetch experience version

        $experienceVersion = $experience->experienceVersions()->where('is_finished', true)->get()->last();

        // Check type of account performing operation

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if ($account instanceof Translator) {

            // Add validation rule for language
            $rules = [
                'language' => 'exists:mysql_translation.languages,language'
            ];

            // Validate

            $valid = \Validator::make($attributes, $rules);

            if (!$valid->passes()) {
                return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Fetch language object

            $language = Language::all()->where('language', $attributes['language'])->last();

            // Check for permission

            if (Gate::forUser($account)->allows('post-seo-translation', [$experienceVersion, $language])) {

                // Write Seo object

                $seo = $this->createOrUpdateSeo(
                    $attributes['title'],
                    $attributes['description'],
                    $attributes['language'],
                    $experienceVersion->id,
                    $account->id
                );

                // Fetch relationships to include on data
                $relationshipData = $this->getRelationshipData($experience->id);

                return ResponseHelper::responsePost(
                    $this->type,
                    $seo,
                    $this->transformer->transform($seo, true),
                    [],
                    $relationshipData);
            }

        } else {

            // Check for permission

            if (Gate::forUser($account)->allows('post-seo', $experienceVersion)) {

                // Write Seo object

                $seo = $this->createOrUpdateSeo(
                    $attributes['title'],
                    $attributes['description'],
                    $experienceVersion->experience()->first()->boat()->first()->user()->first()->language,
                    $experienceVersion->id
                );

                // Fetch relationships to include on data
                $relationshipData = $this->getRelationshipData($experience->id);

                return ResponseHelper::responsePost(
                    $this->type,
                    $seo,
                    $this->transformer->transform($seo, true),
                    [],
                    $relationshipData);
            }
        }

        return ResponseHelper::responseError(
            'You cannot create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // Check for seo existence

        $seo = Seo::find($id);

        if (!isset($seo)) {
            return ResponseHelper::responseError(
                'Seo not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'seo_not_found'
            );
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorization

        if (Gate::forUser($account)->allows('show-seo', $seo)) {

            // Fetch relationships to include on data
            $relationshipData = $this->getRelationshipData($seo->experienceVersion()->first()->experience()->first()->id);

            // Return response
            return ResponseHelper::responseGet(
                $this->type,
                $seo,
                $this->transformer->transform($seo, true),
                [],
                $relationshipData);
        }

        return ResponseHelper::responseError(
            'You cannot see this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get seo with experience id by slug url
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSeoBySlugUrl(Request $request)
    {
        // Get attribute

        $attributes['slug_url'] = $request->input('slug_url');

        // Validation

        $rules = [
            'slug_url' => 'required|string|min:2'
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for existence

        $seo = Seo::all()->where('slug_url', $attributes['slug_url'])->first();

        if (!isset($seo)) {
            return ResponseHelper::responseError(
                'This resource does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'slug_url_does_not_exist');
        }

        if (env('REDIRECT_SLUG_URL') == false) {

            // Get response

            $experienceId = $seo->experienceVersion()->first()->experience()->first()->id;

            // Create relationship data array to include to data
            $relationshipData = $this->getRelationshipData($experienceId);

            return ResponseHelper::responseGet(
                $this->type,
                $seo,
                $this->transformer->transform($seo),
                [],
                $relationshipData);

        } else {

            return response(['redirect_slug_url' => $seo->redirect_slug_url]);

        }
    }

    /**
     * Get slug url from title
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSlugUrlByTitle(Request $request)
    {
        // Fetch attribute

        $attributes['title'] = $request->input('title');

        // Validation

        $rules = [
            'title' => 'required|string|min:2|max:' . Constant::SEO_TITLE_LENGTH,
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-slug-url-by-title')) {

            // Create a fake seo element just to create slug url value and then rollback editings in db

            DB::beginTransaction();
            $seo = $this->createOrUpdateSeo(
                $attributes['title'],
                str_random(Constant::SEO_DESCRIPTION_LENGTH),
                \Lang::getFallback(),
                ExperienceVersion::all()->first()->id);
            $slug_url = $seo->slug_url;
            DB::rollBack();

            // Return slug_url value

            return response(['meta' => ['slug_url' => $slug_url]]);
        }

        return ResponseHelper::responseError(
            'You are not authorized.',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get seo element for experience
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getSeoByExperience(Request $request, $id)
    {
        // Check for experience existence

        $experience = Experience::find($id);

        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $experienceVersion = $experience->experienceVersions()->where('is_finished', true)->get()->last();

        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'This experience version does not still have a seo element',
                SymfonyResponse::HTTP_NOT_FOUND,
                'seo_still_not_created'
            );
        }

        $userLanguage = $experienceVersion->experience()->first()->boat()->first()->user()->first()->language;

        // Check for account type

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Translator case

        if ($account instanceof Translator) {

            // Fetch language on request

            $inputLanguage = $request->input('language');

            $language = null;

            if (isset($inputLanguage)) {
                $attributes['language'] = $inputLanguage;

                // Add validation rule for language

                $rules = [
                    'language' => 'exists:mysql_translation.languages,language'
                ];

                // Validate

                $valid = \Validator::make($attributes, $rules);

                if (!$valid->passes()) {
                    return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
                }

                // Fetch language object

                $language = Language::all()->where('language', $inputLanguage)->last();

            }

            // Check for permission

            if (Gate::forUser($account)->allows('get-seo-translation', [$experienceVersion, $language])) {

                // If there is input language fetch seo in requested language otherwise fetch language with priority

                if (isset($language)) {
                    $seo = Seo::all()
                        ->where('experience_version_id', $experienceVersion->id)
                        ->where('language', $inputLanguage)
                        ->last();
                } else {
                    $seo = $this->getSeoWithPriority($experienceVersion->id, $userLanguage);
                }

                if (isset($seo)) {
                    // Create relationship data array to include to data
                    $relationshipData = $this->getRelationshipData($experience->id);

                    return ResponseHelper::responseGet(
                        $this->type,
                        $seo,
                        $this->transformer->transform($seo, true),
                        [],
                        $relationshipData);
                }

                return ResponseHelper::responseNoContent();
            }

        }

        // Admin case

        if ($account instanceof Administrator) {

            // Check for permission

            if (Gate::forUser($account)->allows('get-seo', $experienceVersion)) {

                // Get Seo object for user language

                $seo = $this->getSeoWithPriority($experienceVersion->id, $userLanguage);

                if (isset($seo)) {
                    // Create relationship data array to include to data
                    $relationshipData = $this->getRelationshipData($experience->id);

                    return ResponseHelper::responseGet(
                        $this->type,
                        $seo,
                        $this->transformer->transform($seo, true),
                        [],
                        $relationshipData);
                }

                return ResponseHelper::responseNoContent();
            }
        }

        return ResponseHelper::responseError(
            'You cannot view this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get array of the attributes and values in post request
     *
     * @param Request $request
     * @return array
     */
    protected function getPostRequestAttributes($request)
    {
        return [
            'type' => $request->input('data.type'),
            'language' => $request->input('data.attributes.language'),
            'description' => $request->input('data.attributes.description'),
            'title' => $request->input('data.attributes.title'),
            'relationship_id' => $request->input('data.relationships.experience.data.id'),
            'relationship_type' => $request->input('data.relationships.experience.data.type')
        ];
    }

    /**
     * Create or update a seo element
     *
     * @param $title
     * @param $description
     * @param $language
     * @param $experienceVersionId
     * @param null $translator_id
     * @return Seo
     */
    public function createOrUpdateSeo($title, $description, $language, $experienceVersionId, $translator_id = null)
    {
        // Create Seo attributes

        $metaTitle = SeoHelper::getMetaTitle($title);
        $metaDescription = SeoHelper::getMetaDescription($description);

        // Create or fetch Seo object and set title, description, language, experience_version_id and translator_id

        $seo = Seo::all()->where('experience_version_id', $experienceVersionId)->where('language', $language)->last();
        if (!isset($seo)) {
            if (is_null($translator_id)) {
                $seo = Seo::all()->where('experience_version_id', $experienceVersionId)->where('translator_id', null)->last();
            }
            if (!isset($seo)) {
                $seo = new Seo();
            }
        }

        $seo->title = $metaTitle;
        $seo->description = $metaDescription;
        $seo->language = $language;
        $seo->experience_version_id = $experienceVersionId;
        $seo->translator_id = $translator_id;

        // Set number to null for the first attempt: if it fails will be created a number to add at
        // the end of the slug url

        $i = null;

        do {
            try {
                $seo->slug_url = SeoHelper::getSlugUrl($title, $i);
                $seo->save();
                $it_fails = false;
            } catch (\Exception $e) {
                if (!isset($i)) {
                    $i = 0;
                }
                $i++;
                $it_fails = true;
            }
        } while ($it_fails);

        return $seo;
    }

    /**
     * Get Seo with major priority
     *
     * @param $experienceVersionId
     * @param $userLanguage
     * @return mixed
     */
    protected function getSeoWithPriority($experienceVersionId, $userLanguage)
    {
        // Try to fetch Seo with user language

        $seo = Seo::all()
            ->where('experience_version_id', $experienceVersionId)
            ->where('language', $userLanguage)
            ->last();

        // Try to fetch Seo with translato_id null

        if (!isset($seo)) {
            $seo = Seo::all()
                ->where('experience_version_id', $experienceVersionId)
                ->where('translator_id', null)
                ->last();
        }

        // Try to fetch Seo with language priority order

        if (!isset($seo)) {
            $priorities = Constant::LANGUAGE_ORDER;
            foreach ($priorities as $priority) {
                $seo = Seo::all()
                    ->where('experience_version_id', $experienceVersionId)
                    ->where('language', $priority)
                    ->last();
                if (isset($seo)) {
                    break;
                }
            }
        }

        return $seo;
    }

    /**
     * Get relationship data array
     *
     * @param $experienceId
     * @return array
     */
    public function getRelationshipData($experienceId) {
        return [
            'experience' => JsonHelper::createDataMessage(
                JsonHelper::createData('experiences', $experienceId)
            )
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function getIndexParameters(Request $request)
    {
        return [
            'language' => $request->language,
            'translated' => $request->translated,
        ];
    }

    /**
     * Get parameter query
     *
     * @param $language
     * @param $translated
     * @return string
     */
    protected function getQuery($language, $translated)
    {
        $query = '&language=' . $language;

        if (isset($translated)) {
            $query = $query . '&translated=' . (int) $translated;
        }

        return $query;
    }
}
