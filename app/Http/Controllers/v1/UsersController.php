<?php
namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\CaptainChangesCurrency;
use App\Jobs\SendMail;
use App\Mail\CaptainRegisteredToCaptain;
use App\Mail\EmailUpdated;
use App\Mail\PartnerRegistered;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Booking;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\User;
use App\Services\CurrencyChanger;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\TokenService;
use App\Services\Transformers\LanguageSpokenTransformer;
use App\Services\Transformers\UserListTransformer;
use App\Services\Transformers\UserTransformer;
use App\Services\ValidationService;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\Paginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class UsersController extends Controller
{
    protected $type = 'users';

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getCaptainByBoat', 'resendToken', 'getUserNotifications']);
        $this->middleware('jwt')->except('resendToken');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Verify it is an administrator

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_USERS_PER_PAGE. If per_page is not set, uses default USERS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_USERS_PER_PAGE) : Constant::USERS_PER_PAGE;
        $page = $request->page;

        // Get requested types of user
        $requestedTypes = $request->input('type');
        $arrayRequestedTypes = explode(',', $requestedTypes);

        if (in_array('users', $arrayRequestedTypes) || empty($arrayRequestedTypes[0])) {
             if (Gate::forUser($account)->denies('get-user-list')) {
                 return ResponseHelper::responseError(
                     'You cannot access this data',
                     SymfonyResponse::HTTP_FORBIDDEN,
                     'not_authorized'
                 );
             }
        }

        if (in_array('captains', $arrayRequestedTypes)) {
            if (Gate::forUser($account)->denies('get-captain-list')) {
                return ResponseHelper::responseError(
                    'You cannot access this data',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }
        }

        if (in_array('partners', $arrayRequestedTypes)) {
            if (Gate::forUser($account)->denies('get-partner-list')) {
                return ResponseHelper::responseError(
                    'You cannot access this data',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }
        }

        // Filter user by name, phone or email with filter if it is set

        $filter = $request->input('filter');
        if (isset($filter) && $filter != '') {
            $users = User::search($filter)->get();
        } else {
            $users = User::all();
        }

        if (!empty($arrayRequestedTypes[0])) {
            $finalCollection = collect([]);
            if (in_array('users', $arrayRequestedTypes)) {
                $guests = $users->where('is_captain', false)->where('is_partner', false);
                $finalCollection = $finalCollection->union($guests);
            }

            if (in_array('captains', $arrayRequestedTypes)) {
                $captains = $users->where('is_captain', true);
                $finalCollection = $finalCollection->union($captains);
            }

            if (in_array('partners', $arrayRequestedTypes)) {
                $partners = $users->where('is_partner', true);
                $finalCollection = $finalCollection->union($partners);
            }
            $users = $finalCollection;
        }

        // Order by id desc
        $users = $users->sortByDesc('id');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($users->count() / $perPage)) ?
            min($page, ceil($users->count() / $perPage)) :
            1;

        $paginatedUsers = new LengthAwarePaginator(
            $users->slice($perPage * ($currentPage - 1), $perPage),
            $users->count(),
            $perPage,
            $currentPage
        );

        $users = $paginatedUsers;
        $transformerForPaginatedUsers = new UserListTransformer();

        return response(Paginator::getPaginatedData($users, $transformerForPaginatedUsers, $perPage, $request), SymfonyResponse::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::find($id);

        // Check if user exists

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('show-user', $user)) {

            // User found in db and request is authorized

            $transformer = new UserTransformer();

            // Fetch for relationships to include on data

            $relationshipsResources = $this->getRelationships($user);

            // Fetch for data to includes to response

            $includedResources = $this->getIncludedResources($user, $request->input('include'));

            if ($account instanceof Administrator) {
                $is_admin = true;
            } else {
                $is_admin = false;
            }

            return ResponseHelper::responseGet('users', $user, $transformer->transform($user, $is_admin, $request->input('device')), $includedResources, $relationshipsResources);
        }

        return ResponseHelper::responseError(
            'You cannot access this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get the captain of the specified boat
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getCaptainByBoat(Request $request, $id)
    {
        $boat = Boat::find($id);

        // Check if boat exists

        if (!isset($boat)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Get captain of the boat

        $captain = $boat->user()->first();

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);
        $boatVersion = $boat->lastFinishedBoatVersion();

        if (Gate::forUser($account)->allows('show-boat-editing', $boatVersion)) {

            // User found in db and request is authorized

            $transformer = new UserTransformer();

            // Fetch for relationships to include on data

            $relationshipsResources = $this->getRelationships($captain);

            // Fetch for data to includes to response

            $includedResources = $this->getIncludedResources($captain, $request->input('include'));

            if ($account instanceof Administrator) {
                $is_admin = true;
            } else {
                $is_admin = false;
            }

            return ResponseHelper::responseGet('users', $captain, $transformer->transform($captain, $is_admin, $request->input('device')), $includedResources, $relationshipsResources);
        }

        return ResponseHelper::responseError(
            'You cannot access this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idAndTypeRequest = $this->getIdAndTypeRequest($request);
        $attributes = $this->getRequestAttributes($request);

        // Validate type and id

        $rules = [
            'id' => 'in:' . $id,
            'type' => 'in:users',
        ];

        $valid = \Validator::make($idAndTypeRequest, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check if user exists

        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found');
        }

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);
        if (Gate::forUser($account)->allows('update-user', $user)) {

            // Check input validity

            $rules = [
                'first_name' => 'string|nullable|max:' . Constant::FIRST_NAME_LENGTH,
                'last_name' => 'string|nullable|max:' . Constant::LAST_NAME_LENGTH,
                'docking_place' => 'string|nullable|max:' . Constant::DOCKING_PLACE_LENGTH,
                'first_name_skipper' => 'string|nullable|max:' . Constant::FIRST_NAME_LENGTH,
                'last_name_skipper' => 'string|nullable|max:' . Constant::LAST_NAME_LENGTH,
                'skippers_have_license' => 'boolean|nullable',
                'first_name_contact' => 'string|nullable|max:' . Constant::FIRST_NAME_LENGTH,
                'last_name_contact' => 'string|nullable|max:' . Constant::LAST_NAME_LENGTH,
                'email' => 'email|nullable|max:' . Constant::EMAIL_LENGTH,
                'captain_type' => 'exists:captain_types,name|nullable',
                'is_skipper' => 'boolean|nullable',
                'sex' => 'size:1|nullable|in:' . implode(',', Constant::SEX_TYPES),
                'birth_date' => 'date|nullable',
                'nationality' => 'nullable|exists:countries,name',
                'currency' => 'required|exists:currencies,name',
                'language' => 'required|exists:mysql_translation.languages,language',
                'description' => 'string|nullable',
                'enterprise_name' => 'string|nullable|max:' . Constant::ENTERPRISE_NAME_LENGTH,
                'vat_number' => 'string|nullable|max:' . Constant::ENTERPRISE_VAT_LENGTH,
                'enterprise_address' => 'string|nullable|max:' . Constant::ENTERPRISE_ADDRESS_LENGTH,
                'enterprise_city' => 'string|nullable|max:' . Constant::ENTERPRISE_CITY_LENGTH,
                'enterprise_zip_code' => 'string|nullable|max:' . Constant::ENTERPRISE_ZIP_LENGTH,
                'enterprise_country' => 'nullable|exists:countries,name',
                'has_card' => 'boolean|nullable',
                'default_fee' => 'integer|nullable|min:' . Constant::MIN_FEE_VALUE . '|max:' . Constant::MAX_FEE_VALUE,
                'is_partner' => 'boolean|nullable',
                'is_suspended' => 'boolean|nullable',
                'newsletter' => 'boolean|nullable',
                'card_price' => 'integer|nullable|min:0',
                'paypal_account' => 'email|nullable|max:' . Constant::EMAIL_LENGTH,
            ];

            $valid = \Validator::make($attributes, $rules);

            if (!$valid->passes()) {
                return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $email = strtolower($attributes['email']);

            $email_has_changed = false;
            if (isset($email) && !is_null($email) && $email != $user->email) {
                $userEmail = User::where('email', $email)
                    ->where('is_mail_activated', 1)
                    ->where('id', '!=', $id)
                    ->first();

                if (isset($userEmail)) {
                    return ResponseHelper::responseError(
                        'The email is already used by another account',
                        SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                        'email_already_used',
                        'email'
                    );
                }
                $user->email = $email;
                $user->is_mail_activated = false;
                $email_has_changed = true;
            }

            $old_card_price = $user->card_price;
            $user->fill($attributes);

            $user->save();

            if (isset($email) && !is_null($email) && $email_has_changed) {
                dispatch((new SendMail(new EmailUpdated($user)))->onQueue(env('SQS_MAIL')));
            }

            $currencyIn = $user->currency;
            $currencyOut = $attributes['currency'];

            if (isset($currencyOut) && $currencyOut != $currencyIn) {
                if ($user->is_captain) {
                    $currencyChangeValue = CurrencyChanger::getCurrencyChangeLive($currencyIn, $currencyOut);
                    $user->currency = $currencyOut;
                    if (isset($user->card_price) && $old_card_price == $user->card_price) {
                        $user->card_price = ceil($user->cardPrice * $currencyChangeValue);
                    }
                    dispatch((new CaptainChangesCurrency($user, $currencyIn, $currencyOut)));
                } else {
                    $user->currency = $currencyOut;
                }
                $user->save();
            }

            $transformer = new UserTransformer();

            if ($account instanceof Administrator) {
                $is_admin = true;
            } else {
                $is_admin = false;
            }

            return ResponseHelper::responseGet('users', $user, $transformer->transform($user, $is_admin, $request->input('meta.device')));
        }

        return ResponseHelper::responseError(
            'You cannot update this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change password
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, $id)
    {
        // Check user existence

        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Fetch request attribute

        $input = $request->input('meta');

        // Set validation rules

        $rules = [
            'old_password' => 'required',
            'new_password' => 'required|string|min:' . Constant::MIN_PASSWORD_LENGTH . '|max:' . Constant::MAX_PASSWORD_LENGTH,
        ];

        // Check rules on input

        $valid = \Validator::make($input, $rules);

        // Check validation rule

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('update-password', $user)) {

            $old_password = $input['old_password'];
            $new_password = $input['new_password'];

            $credentials = ['email' => $account->email, 'password' => $old_password, 'is_mail_activated' => 1];

            // Check for right password

            if (Auth::once($credentials)) {
                // Set new password

                $account->password = \Hash::make($new_password);
                $account->save();

                // Return response
                $transformer = new UserTransformer();

                return ResponseHelper::responseGet('users', $account, $transformer->transform($account));
            }

            return ResponseHelper::responseError(
                'Incorrect password',
                SymfonyResponse::HTTP_FORBIDDEN,
                'incorrect_password',
                'old_password'
            );
        }

        return ResponseHelper::responseError(
            'User must be a captain or partner or user id do not match account',
            SymfonyResponse::HTTP_FORBIDDEN,
            'user_not_captain_or_partner_id_match'
        );
    }

    /**
     * Change password
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function setPassword(Request $request, $id)
    {
        // Check user existence

        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Fetch request attribute

        $input = $request->input('meta');

        // Set validation rules

        $rules = [
            'password' => 'required|string|min:' . Constant::MIN_PASSWORD_LENGTH . '|max:' . Constant::MAX_PASSWORD_LENGTH,
        ];

        // Check rules on input

        $valid = \Validator::make($input, $rules);

        // Check validation rule

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('set-password', $user)) {

            // Set password

            $password = $input['password'];

            $account->password = \Hash::make($password);
            $account->is_set_password = 1;
            $account->save();

            // Return response
            $transformer = new UserTransformer();

            return ResponseHelper::responseGet('users', $account, $transformer->transform($account));
        }

        return ResponseHelper::responseError(
            'User must be a captain or partner, or id do not match account or password is already set',
            SymfonyResponse::HTTP_FORBIDDEN,
            'user_not_captain_or_partner_id_match_password_already_set'
        );
    }

    /**
     * Update spoken languages for a specific user
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function languages(Request $request, $id)
    {
        // Check if user exists

        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError('User not found', SymfonyResponse::HTTP_NOT_FOUND, 'user_not_found');
        }

        // Check if the requested user is equal to the sub claim of the jwt

        $jwt = $request->bearerToken();

        if (Gate::forUser(JwtService::getAccountFromJWT($jwt))->allows('update-user-languages', $user)) {

            // Check input validity

            $languages = $request->input('data');

            $rules = [
                'id' => 'required|exists:languages,id',
                'type' => 'in:languages'
            ];

            foreach ($languages as $language) {
                $valid = \Validator::make($language, $rules);

                if (!$valid->passes()) {
                    return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
                }
            }

            $arrayLanguages = [];

            foreach ($languages as $language) {
                $arrayLanguages[] = $language['id'];
            }

            $user->languages()->sync($arrayLanguages);

            return response(null, SymfonyResponse::HTTP_NO_CONTENT);

        }

        return ResponseHelper::responseError(
            'You cannot update this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );


    }

    /**
     * Get user notifications
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getUserNotifications(Request $request, $id)
    {
        // Check for user existence
        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Check for authorization
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-user-notifications', $user)) {

            // Get number of unread messages
            $conversionIds = [];

            // Converations as user
            $conversations = Conversation::all()->where('user_id', $user->id);
            foreach ($conversations as $conversation)
            {
                $conversionIds[] = $conversation->id;
            }

            // Conversation as captain
            $conversations = Conversation::all()->where('captain_id', $user->id);
            foreach ($conversations as $conversation)
            {
                $conversionIds[] = $conversation->id;
            }

            $unreadMessages = Message::all()->whereIn('conversation_id', $conversionIds)
                ->where('user_id', '!=', $user->id)
                ->where('read', 0)
                ->count();

            // Get number of bookings

            // Get number of user bookings
            $bookingsAsGuest = $user->bookings()
                ->where('experience_date', '>=', Carbon::tomorrow()->format('Y-m-d'))
                ->get()
                ->filter(function ($value, $key) {
                    return $value->status == Constant::BOOKING_STATUS_ACCEPTED ||
                        $value->status == Constant::BOOKING_STATUS_REJECTED;
                })
                ->count();

            // Get number of captain bookings

            $bookingAsCaptainIds =  DB::table('bookings')
                ->where('bookings.experience_date', '>=', Carbon::tomorrow()->format('Y-m-d'))
                ->join('experience_versions', 'bookings.experience_version_id', '=', 'experience_versions.id')
                ->join('experiences', 'experience_versions.experience_id', '=', 'experiences.id')
                ->join('boats', 'experiences.boat_id', '=', 'boats.id')
                ->join('users', function ($join) use ($user) {
                    $join->on('boats.user_id', '=', 'users.id')
                        ->where('users.id', '=', $user->id);
                })
                ->select('bookings.id')
                ->get();

            $bookingIds = [];
            foreach ($bookingAsCaptainIds as $bookingCollectionId) {
                $bookingIds[] = $bookingCollectionId->id;
            }

            // Get bookings sorted by Ids

            $bookingsAsCaptain = Booking::all()
                ->whereIn('id', $bookingIds)
                ->filter(function ($value, $key) {
                    if ($value->status == Constant::BOOKING_STATUS_PENDING || $value->status == Constant::BOOKING_STATUS_PAID) {
                        return true;
                    }
                    return false;
                })
                ->sortByDesc('id')
                ->sortByDesc('updated_at')
                ->count();



            // Return response

            return response(JsonHelper::createMetaMessage([
                'is_mail_activated' => $user->is_mail_activated,
                'is_phone_activated' => $user->is_phone_activated,
                'messages' => $unreadMessages,
                'bookings' => $bookingsAsGuest + $bookingsAsCaptain]),
                SymfonyResponse::HTTP_OK
            );
        }

        return ResponseHelper::responseError(
            'You cannot access this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }


    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $user
     * @param $includesRequestString
     * @return array
     */
    protected function getIncludedResources($user, $includesRequestString)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];
        if (!empty($includesRequest)) {
            if (in_array('languages', $includesRequest)) {
                $languageTransformer = new LanguageSpokenTransformer();
                $languageCollection = $user->languages()->get();
                foreach ($languageCollection as $language) {
                    $includes[] = JsonHelper::createData('languages', $language->id, $languageTransformer->transform($language));
                }
            }
        }
        return $includes;
    }

    /**
     * Get the arrays of the relationships elements for the specified user
     *
     * @param $user
     * @return array
     */
    protected function getRelationships($user)
    {
        $relationships = [];
        return $relationships;
    }

    /**
     * Get array of the attributes and values in request
     *
     * @param Request $request
     * @return array
     */
    protected function getRequestAttributes($request)
    {
        return [
            'first_name' => $request->input('data.attributes.first_name'),
            'last_name' => $request->input('data.attributes.last_name'),
            'docking_place' => $request->input('data.attributes.docking_place'),
            'first_name_skipper' => $request->input('data.attributes.first_name_skipper'),
            'last_name_skipper' => $request->input('data.attributes.last_name_skipper'),
            'skippers_have_license' => $request->input('data.attributes.skippers_have_license'),
            'first_name_contact' => $request->input('data.attributes.first_name_contact'),
            'last_name_contact' => $request->input('data.attributes.last_name_contact'),
            'email' => $request->input('data.attributes.email'),
            'captain_type' => $request->input('data.attributes.captain_type'),
            'is_skipper' => $request->input('data.attributes.is_skipper'),
            'sex' => $request->input('data.attributes.sex'),
            'birth_date' => $request->input('data.attributes.birth_date'),
            'nationality' => $request->input('data.attributes.nationality'),
            'currency' => $request->input('data.attributes.currency'),
            'language' => $request->input('data.attributes.language'),
            'description' => $request->input('data.attributes.description'),
            'enterprise_name' => $request->input('data.attributes.enterprise_name'),
            'vat_number' => $request->input('data.attributes.vat_number'),
            'enterprise_address' => $request->input('data.attributes.enterprise_address'),
            'enterprise_city' => $request->input('data.attributes.enterprise_city'),
            'enterprise_zip_code' => $request->input('data.attributes.enterprise_zip_code'),
            'enterprise_country' => $request->input('data.attributes.enterprise_country'),
            'has_card' => $request->input('data.attributes.has_card'),
            'default_fee' => $request->input('data.attributes.default_fee'),
            'is_partner' => $request->input('data.attributes.is_partner'),
            'is_suspended' => $request->input('data.attributes.is_suspended'),
            'newsletter' => $request->input('data.attributes.newsletter'),
            'card_price' => $request->input('data.attributes.card_price'),
            'paypal_account' => $request->input('data.attributes.paypal_account'),
        ];
    }

    /**
     * Get array of the attributes and values in request
     *
     * @param Request $request
     * @return array
     */
    protected function getIdAndTypeRequest($request)
    {
        return [
            'type' => $request->input('data.type'),
            'id' => $request->input('data.id'),
        ];
    }

    /**
     * Resend access token for captain or partner who has not already set password
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function resendToken(Request $request)
    {
        // Get input request attribute

        $input = $request->input('meta');

        // Validate fields

        $rules = [
            'email' => 'required|email',
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $input['email'];

        // Check for email existence

        $user = User::all()->where('email', $email)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'email_not_found',
                'email'
            );
        }

        // Check for email activated

        $user = User::all()->where('email', $email)->where('is_mail_activated', true)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email is not activated',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'email_not_activated',
                'email'
            );
        }

        // Check that password is not already set

        if ($user->is_set_password) {
            return ResponseHelper::responseError(
                'Password is already set, log in or restore your password if you forget it',
                SymfonyResponse::HTTP_FORBIDDEN,
                'user_has_already_password',
                'email'
            );
        }

        // Check that user is captain and/or partner to restore password

        if (Gate::forUser($user)->denies('resend-token')) {
            return ResponseHelper::responseError(
                'User must be a captain or partner',
                SymfonyResponse::HTTP_FORBIDDEN,
                'user_not_captain_or_partner',
                'email'
            );
        }

        // Send mail with token link

        if ($user->is_captain) {
            dispatch((new SendMail(new CaptainRegisteredToCaptain($user)))->onQueue(env('SQS_MAIL')));
        } else {
            dispatch((new SendMail(new PartnerRegistered($user)))->onQueue(env('SQS_MAIL')));
        }

        return response('', SymfonyResponse::HTTP_OK);
    }


    /**
     * Get token to access as a specific user
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTokenForAccess(Request $request)
    {
        // Validate fields

        $rules = [
            'user_id' => 'integer|required',
        ];

        $valid = \Validator::make($request->input('meta'), $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get user id
        $idUser = $request->input('meta.user_id');

        // Check for user existence
        $user = User::find($idUser);

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Get account by jwt
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check that account is authorized to make this request
        if (Gate::forUser($account)->allows('create-token')) {

            // Create token that is not for mail
            $token = TokenService::generateTokenByUser($user, false);
            return response(JsonHelper::createMetaMessage(['token' => $token]), SymfonyResponse::HTTP_CREATED);
        }

        return ResponseHelper::responseError(
            'You cannot create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

}
