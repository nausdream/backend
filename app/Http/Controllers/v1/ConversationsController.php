<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Experience;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\ResponseHelper;
use App\Services\Transformers\BookingConversationTransformer;
use App\Services\Transformers\ConversationsTransformer;
use App\Services\Transformers\ExperienceVersionConversationTransformer;
use App\Services\Transformers\MessageConversationTransformer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class ConversationsController extends Controller
{
    protected $type = 'conversations';
    protected $transformer;

    /**
     * ConversationsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getUserConversations', 'getExperienceConversations']);
        $this->middleware('jwt');

        $this->transformer = new ConversationsTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-conversations-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_CONVERSATIONS_PER_PAGE) : Constant::CONVERSATIONS_PER_PAGE;
        $page = $request->page;

        // Get conversations sorted by updated_at first, and then by id
        $conversations = Conversation::all()
            ->sortByDesc('id')
            ->sortByDesc('updated_at');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($conversations->count() / $perPage)) ?
            min($page, ceil($conversations->count() / $perPage)) :
            1;

        $paginatedConversations = new LengthAwarePaginator(
            $conversations->slice($perPage * ($currentPage - 1), $perPage),
            $conversations->count(),
            $perPage,
            $currentPage
        );

        $conversations = $paginatedConversations;

        // Set transformer mode to "list", in order to fetch correct attributes
        $this->transformer->setMode('list');
        // Set user to transformer in order to know if the conversation has unread messages for that user
        $this->transformer->setUser($account);

        return response(Paginator::getPaginatedData($conversations, $this->transformer, $perPage, $request));
    }

    /**
     * Display all conversations of a user
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getUserConversations(Request $request, $id)
    {
        // Check for user existence
        $user = User::find($id);
        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-user-conversations', $id)) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_CONVERSATIONS_PER_PAGE) : Constant::CONVERSATIONS_PER_PAGE;
        $page = $request->page;

        // Get user conversations
        $conversations = Conversation::where('user_id', $id)->orWhere('captain_id', $id)->get()
            ->sortByDesc('id')
            ->sortByDesc('updated_at');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($conversations->count() / $perPage)) ?
            min($page, ceil($conversations->count() / $perPage)) :
            1;

        $paginatedConversations = new LengthAwarePaginator(
            $conversations->slice($perPage * ($currentPage - 1), $perPage),
            $conversations->count(),
            $perPage,
            $currentPage
        );

        $conversations = $paginatedConversations;

        // Set transformer mode to "list", in order to fetch correct attributes
        $this->transformer->setMode('list');
        // Set user to transformer in order to know if the conversation has unread messages for that user
        $this->transformer->setUser($account);

        return response(Paginator::getPaginatedData($conversations, $this->transformer, $perPage, $request));
    }

    /**
     * Display all conversations of an experience
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getExperienceConversations(Request $request, $id)
    {
        // Check for user existence
        $experience = Experience::find($id);
        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-experience-conversations', $id)) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_CONVERSATIONS_PER_PAGE) : Constant::CONVERSATIONS_PER_PAGE;
        $page = $request->page;

        // Get user conversations
        $conversations = $experience->conversations()->get()
            ->sortByDesc('id')
            ->sortByDesc('updated_at');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($conversations->count() / $perPage)) ?
            min($page, ceil($conversations->count() / $perPage)) :
            1;

        $paginatedConversations = new LengthAwarePaginator(
            $conversations->slice($perPage * ($currentPage - 1), $perPage),
            $conversations->count(),
            $perPage,
            $currentPage
        );

        $conversations = $paginatedConversations;

        // Set transformer mode to "list", in order to fetch correct attributes
        $this->transformer->setMode('list');

        // Set transformer user to set "role"
        $this->transformer->setUser($account);

        return response(Paginator::getPaginatedData($conversations, $this->transformer, $perPage, $request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $requestDate = $request->input('data.attributes.request_date');
        $adults = $request->input('data.attributes.adults');
        $kids = $request->input('data.attributes.kids');
        $babies = $request->input('data.attributes.babies');
        $experienceType = $request->input('data.relationships.experience.data.type');
        $experienceId = $request->input('data.relationships.experience.data.id');

        // Validate data
        $rules = [
            'type' => 'required|in:' . $this->type,
        ];
        $data = [
            'type' => $type
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'request_date' => 'nullable|date_format:Y-m-d|after:-1 day',
            'adults' => 'nullable|integer|numeric|min:0',
            'kids' => 'nullable|integer|numeric|min:0',
            'babies' => 'nullable|integer|numeric|min:0',
            'experienceType' => 'required|in:experiences'
        ];

        $conversationData = [
            'request_date' => $requestDate,
            'adults' => $adults,
            'kids' => $kids,
            'babies' => $babies
        ];
        $relationshipsData = [
            'experienceType' => $experienceType
        ];

        $validator = \Validator::make($conversationData + $relationshipsData, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Validate experience id
        $rules = [
            'experience_id' => 'required|numeric|integer|exists:experiences,id',
        ];
        $data = [
            'experience_id' => $experienceId
        ];
        $validator = \Validator::make($data, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError(
                'Experience with ID of ' . $experienceId . ' not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Fetch experience from db
        $experience = Experience::find($experienceId);

        // Check if there is at least a finished experienceVersion
        if ($experience->experienceVersions()->get()->where('is_finished', true)->count() <= 0) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check if user is authorized
        if (Gate::forUser($account)->denies('create-conversations', $experience)) {
            return ResponseHelper::responseError(
                'Not authorized to create conversations',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }


        // Return 422 if there is already a conversation from the same user for the same experience and request date
        $conversation = $account->userConversations()->get()
            ->where('experience_id', $experience->id)
            ->where('request_date', $requestDate)
            ->first();
        if (isset($conversation)) {
            $meta = JsonHelper::createMetaMessage(['conversation_id' => $conversation->id]);
            $errors = JsonHelper::createErrorMessage([
                JsonHelper::createErrorWithTitle(
                    'conversation_already_exists',
                    'conversation',
                    'The conversation with these attributes already exists'
                )]
            );
            return response($meta + $errors, SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (isset($requestDate)) {
            // Return 422 if request_date is out of experience availabilities range
            $availabilitiesRequestDate = $experience->experienceAvailabilities()->get()
                ->where('date_start', '<=', $requestDate)->where('date_end', '>=', $requestDate)->first();
            if ((!isset($availabilitiesRequestDate))) {
                return ResponseHelper::responseError(
                    'This Experience is not available on this date',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'experience_not_available',
                    'request_date'
                );
            }
        }

        // Create new conversation
        $conversation = factory(Conversation::class)->make($conversationData);
        $conversation->experience()->associate($experience);
        $conversation->user()->associate($account);
        $conversation->captain()->associate($experience->boat()->first()->user()->first());
        $conversation->save();

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($conversation, $request->input('include'), $account->language);

        // Set transformer user to set "role"
        $this->transformer->setUser($account);

        // Fetch conversation attributes from transformer
        $conversationAttributes = $this->transformer->transform($conversation);

        // Define included-to-relationships mappings
        $mappings = ['experiences' => 'experience'];

        return ResponseHelper::responsePost(
            $this->transformer->getType(),
            $conversation,
            $conversationAttributes,
            $includedResources,
            [],
            $conversation->id,
            $mappings
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        $conversation = Conversation::find($id);

        if (!isset($conversation)) {
            return ResponseHelper::responseError(
                'Conversation not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'conversation_not_found'
            );
        }

        if (Gate::forUser($account)->denies('get-conversations-single', $conversation)) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Sets read=true on other user messages
        if ($account instanceof User) {
            $otherUserMessages = $conversation->messages()->where('user_id', '!=', $account->id)->get();
            $otherUserMessages->each(function ($message) {
                $message->read = true;
                $message->save();
            });
        }

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($conversation, $request->input('include'), $account->language);

        // Set transformer user to set "role"
        $this->transformer->setUser($account);

        // Fetch conversation attributes from transformer
        $conversationAttributes = $this->transformer->transform($conversation);

        // Define included-to-relationships mappings
        $mappings = ['experiences' => 'experience', 'bookings' => 'booking'];

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $conversation,
            $conversationAttributes,
            $includedResources,
            [],
            $conversation->id,
            $mappings
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $conversation
     * @param string $includesRequestString
     * @param string $language
     * @return array
     */
    protected function getIncludedResources($conversation, $includesRequestString = '', string $language = null)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];

        // INCLUDED BY DEFAULT
        // Booking
        $booking = $conversation->bookings()->get()->last();
        if (isset($booking)) {
            $bookingTransformer = new BookingConversationTransformer();
            $includes[] = JsonHelper::createData(
                $bookingTransformer->getType(),
                $booking->id,
                $bookingTransformer->transform($booking)
            );
        }

        // Experience (version)
        $experience = $conversation->experience()->first();
        if (isset($experience)) {
            $experienceVersions = $experience->experienceVersions()->get();
            if ($experienceVersions->count() > 0) {
                // Get correct experienceVersion
                if (
                    isset($booking) &&
                    $booking->status != Constant::BOOKING_STATUS_CANCELLED &&
                    $booking->status != Constant::BOOKING_STATUS_REJECTED
                ) {
                    // Get booking experienceVersion
                    $experienceVersion = $booking->experienceVersion()->first();
                } else {
                    // Get last finished experienceVersion
                    $experienceVersion = $experience->lastFinishedExperienceVersion();
                }
                $experienceVersionTransformer = new ExperienceVersionConversationTransformer();
                $experienceVersionTransformer->setLanguage($language);
                $includes[] = JsonHelper::createData(
                    $experienceVersionTransformer->getType(),
                    $experience->id,
                    $experienceVersionTransformer->transform($experienceVersion)
                );
            }
        }

        // Messages
        $messages = $conversation->messages()->get()->sortBy('id');
        if ($messages->count() > 0) {
            $messageTransformer = new MessageConversationTransformer();
            foreach ($messages as $message) {
                $includes[] = JsonHelper::createData(
                    $messageTransformer->getType(),
                    $message->id,
                    $messageTransformer->transform($message)
                );
            }
        }

        return $includes;
    }
}
