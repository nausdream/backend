<?php

namespace App\Http\Controllers\v1;

use App\Jobs\SendMail;
use App\Mail\CaptainRegisteredToAdministrator;
use App\Mail\CaptainRegisteredToCaptain;
use App\Models\User;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use ClassPreloader\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Console\IlluminateCaster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\JsonHelper;
use App\Validators\RestValidator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationServiceProvider;
use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Constant;


class CaptainsController extends Controller
{
    protected $type = 'captains';

    /**
     * CaptainsController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->getRequestAttributes($request);

        // Check type

        $rules = [
            'type' => 'in:users'
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check other attributes

        $rules = [
            'first_name' => 'required|max:' . Constant::FIRST_NAME_LENGTH,
            'phone' => 'required|max:' . Constant::PHONE_LENGTH,
            'docking_place' => 'required|max:' . Constant::DOCKING_PLACE_LENGTH,
            'email' => 'required|email|max:' . Constant::EMAIL_LENGTH,
            'language' => 'required|exists:mysql_translation.languages,language',
            'currency' => 'required|exists:currencies,name',
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check if mail and phone already exist users table

        $captainEmail = User::where('email',  $input['email'])
            ->where('is_mail_activated', 1)
            ->first();

        $captainPhone = User::where('phone', $input['phone'])
            ->where('is_phone_activated', 1)
            ->first();

        // Mail and phone don't exist in users table

        if (!isset($captainPhone) && !isset($captainEmail)) {

            $captain = factory(User::class)->make();

            $captain->first_name = $input['first_name'];
            $captain->phone = $input['phone'];
            $captain->docking_place = $input['docking_place'];
            $captain->email = $input['email'];
            $captain->language = $input['language'];
            $captain->currency = $input['currency'];

            $captain->is_captain = true;

            $captain->save();
            $this->sendMailForCreation($captain);

            $userTransformer = new UserTransformer();

            return response(JsonHelper::createDataMessage(JsonHelper::createData('users', $captain->id, $userTransformer->transform($captain))), SymfonyResponse::HTTP_CREATED);
        }

        // Mail and phone exist in users table
        if (isset($captainEmail) && isset($captainPhone)) {
            if (!$captainEmail->is_captain) {
                $captainEmail->is_captain = true;
                $captainEmail->save();
                $this->sendMailForCreation($captainEmail);
                return ResponseHelper::responseGet('users', $captainEmail);
            }
            return ResponseHelper::responseError(
                "Already a captain",
                SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                'already_a_captain',
                'email'
            );
        }

        // Mail exists in users table

        if (isset($captainEmail)) {
            if (!$captainEmail->is_captain) {
                $captain = $captainEmail;

                if (!$captain->phone) {
                    $captain->phone = $input['phone'];
                }
                return $this->changeCaptainValues($request, $captain);
            }
            return ResponseHelper::responseError(
                "Already a captain",
                SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                'already_a_captain',
                'email'
            );
        }

        // Phone exists in users table

        if (isset($captainPhone)) {
            if (!$captainPhone->is_captain) {
                $captain = $captainEmail;

                if (!$captain->email) {
                    $captain->email = $input['email'];
                }
                return $this->changeCaptainValues($request, $captain);
            }
            return ResponseHelper::responseError(
                "Already a captain", SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                'already_a_captain',
                'phone');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change captain values when inserted
     *
     * @param $input
     * @param User $captain
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function changeCaptainValues($input, User $captain)
    {
        if (!$captain->first_name) {
            $captain->first_name = $input['first_name'];
        }
        if (!$captain->docking_place) {
            $captain->docking_place = $input['docking_place'];
        }
        if (!$captain->language) {
            $captain->language = $input['language'];
        }
        if (!$captain->currency) {
            $captain->currency = $input['currency'];
        }
        $captain->is_captain = true;

        $captain->save();
        $this->sendMailForCreation($captain);

        unset($input['type']);

        return ResponseHelper::responseUpdate('users', $captain, $input);
    }

    /**
     * Send a mail for captain creation to general admin and to captain
     *
     * @param User $captain
     */
    protected function sendMailForCreation(User $captain)
    {
        dispatch((new SendMail(new CaptainRegisteredToCaptain($captain)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendMail(new CaptainRegisteredToAdministrator($captain)))->onQueue(env('SQS_MAIL')));
    }

    /**
     * Get array of the attributes and values in request
     *
     * @param Request $request
     * @return array
     */
    protected function getRequestAttributes($request)
    {
        return [
            'type' => $request->input('data.type'),

            'first_name' => $request->input('data.attributes.first_name'),
            'phone' => $request->input('data.attributes.phone'),
            'docking_place' => $request->input('data.attributes.docking_place'),
            'email' => strtolower($request->input('data.attributes.email')),
            'language' => $request->input('data.attributes.language'),
            'currency' => $request->input('data.attributes.currency'),
        ];
    }
}
