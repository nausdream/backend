<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Events\BookingRequestCreated;
use App\Jobs\SendMail;
use App\Jobs\SendSms;
use App\Mail\BookingAnswerToAdministrator;
use App\Mail\BookingAnswerToGuest;
use App\Mail\BookingPaidToAdministrator;
use App\Mail\BookingPaidToCaptain;
use App\Mail\BookingPaidToGuest;
use App\Mail\BookingRequestedToAdministrator;
use App\Mail\BookingRequestedToCaptain;
use App\Mail\PreExperienceToCaptain;
use App\Mail\PreExperienceToGuest;
use App\Models\AdditionalService;
use App\Models\Booking;
use App\Models\Conversation;
use App\Models\Coupon;
use App\Models\Experience;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\ExperienceVersionsFixedAdditionalServices;
use App\Models\Price;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\PriceHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\BookingListTransformer;
use App\Services\Transformers\BookingTransformer;
use App\Sms\AlertToCheckEmailToUser;
use App\Sms\BookingPaidToCaptain as BookingPaidToCaptainSms;
use App\Sms\BookingRequestedToCaptain as BookingRequestedToCaptainSms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\AreaHelper;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Gate;
use App\Http\Controllers\Controller;

class BookingsController extends Controller
{
    protected $type = 'bookings';

    /**
     * BookingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getUserBookings', 'getCaptainBookings', 'payBookingRequest']);
        $this->middleware('jwt')->except(['payBookingRequest']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get request attributes

        $attributes = $this->getPostAttributes($request);

        // Validate attributes

        $rules = [
            'experience_date' => 'required|date_format:Y-m-d',
            'adults' => 'required|integer|min:0',
            'kids' => 'required|integer|min:0',
            'babies' => 'required|integer|min:0'
        ];

        $validator = \Validator::make($attributes['attributes'], $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!isset($attributes['experience']['id']) && !isset($attributes['conversation']['id'])) {
            return ResponseHelper::responseError(
                'Required an experience or a conversation to connect at the booking',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_or_conversation_required'
            );
        }

        // If experience set, get the right experienceVersion for experience in relationship
        if (isset($attributes['experience']['id'])) {

            // Validation experience relationship
            $rules = [
                'type' => 'required|in:experiences'
            ];

            $validator = \Validator::make($attributes['experience'], $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Check for experience existence

            $experience = Experience::find($attributes['experience']['id']);
            if (!isset($experience)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Check that last experience version of that it is not freezed or rejected

            $lastFinishedExperienceVersion = $experience->lastFinishedExperienceVersion();
            if (!isset($lastFinishedExperienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            $status = $lastFinishedExperienceVersion->status;
            if ($status == Constant::STATUS_REJECTED || $status == Constant::STATUS_FREEZED) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Get last finished and accepted experience version

            $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }
        }

        // If experience is not set get right experienceVersion for conversation in relationship
        if (!isset($experienceVersion) && isset($attributes['conversation']['id'])) {

            // Validation experience relationship
            $rules = [
                'type' => 'required|in:conversations'
            ];

            $validator = \Validator::make($attributes['conversation'], $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Check fot conversation existence

            $conversation = Conversation::find($attributes['conversation']['id']);
            if (!isset($conversation)) {
                return ResponseHelper::responseError(
                    'Conversation does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'conversation_not_found'
                );
            }

            // Fetch experience for conversation

            $experience = $conversation->experience()->first();

            // Check that last experience version os that it is accepted

            $lastFinishedExperienceVersion = $experience->lastFinishedExperienceVersion();
            if (!isset($lastFinishedExperienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }
            $status = $lastFinishedExperienceVersion->status;
            if ($status == Constant::STATUS_REJECTED || $status == Constant::STATUS_FREEZED) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found');
            }

            // Get last finished and accepted experience version

            $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();
            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }
        }

        // Check that experience version is set

        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check for fixed-additional-service-prices existence for related experienceVersion

        $fixedServices = [];
        $fixedServiceIds = [];
        foreach ($attributes['fixed-additional-service-prices'] as $fixedAdditionalServicePrice) {

            // Validation fixed additional services
            $rules = [
                'type' => 'required|in:fixed-additional-service-prices'
            ];

            $validator = \Validator::make($fixedAdditionalServicePrice, $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $fixedService = ExperienceVersionsFixedAdditionalServices::all()
                ->where('id', $fixedAdditionalServicePrice['id'])
                ->where('experience_version_id', $experienceVersion->id)
                ->last();

            // Check for existence

            if (!isset($fixedService)) {
                return ResponseHelper::responseError(
                    'Fixed service does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'fixed_service_not_found'
                );
            }

            // Save model and id for later edits

            $fixedServices[] = $fixedService;
            $fixedServiceIds[] = $fixedService->fixed_additional_service_id;
        }

        // Check for additional-services existence for related experienceVersion

        $additionalServices = [];
        $additionalServiceIds = [];
        foreach ($attributes['additional-services'] as $additionalServiceFromAttributes) {

            // Validation additional services
            $rules = [
                'type' => 'required|in:additional-services'
            ];

            $validator = \Validator::make($additionalServiceFromAttributes, $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            $additionalService = AdditionalService::all()
                ->where('id', $additionalServiceFromAttributes['id'])
                ->where('experience_version_id', $experienceVersion->id)
                ->last();

            // Check for existence

            if (!isset($additionalService)) {
                return ResponseHelper::responseError(
                    'Additional service does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'additional_service_not_found'
                );
            }

            // Save model and id for later edits

            $additionalServices[] = $additionalService;
            $additionalServiceIds[] = $additionalServiceFromAttributes['id'];
        }

        // Check that request date is inside a period of experience

        $experienceDate = $attributes['attributes']['experience_date'];
        $availability = $experience->experienceAvailabilities()
            ->where('date_start', '<=', $experienceDate)
            ->where('date_end', '>=', $experienceDate)
            ->get()->last();

        if (!isset($availability)) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        // Check that request date is on a day of the week of availability

        $requestDayOfWeek = Carbon::createFromFormat('Y-m-d', $experienceDate)->dayOfWeek;

        if ($requestDayOfWeek == Carbon::MONDAY && !$experienceVersion->monday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::TUESDAY && !$experienceVersion->tuesday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::WEDNESDAY && !$experienceVersion->wednesday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::THURSDAY && !$experienceVersion->thursday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::FRIDAY && !$experienceVersion->friday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::SATURDAY && !$experienceVersion->saturday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        if ($requestDayOfWeek == Carbon::SUNDAY && !$experienceVersion->sunday) {
            return ResponseHelper::responseError(
                'The request date is not available',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'experience_date_not_available',
                'experience_date'
            );
        }

        // Check if there is a period valid for request date and entire boat if requested

        $period = $experience->periods()
            ->where('date_start', null)
            ->where('date_end', null)
            ->get()->last();
        if (!isset($period)) {
            $period = $experience->periods()
                ->where('date_start', '<=', $experienceDate)
                ->where('date_end', '>=', $experienceDate)
                ->get()->last();
            if (!isset($period)) {
                $period = $experience->periods()
                    ->where('is_default', true)
                    ->get()->last();
                if (!isset($period)) {
                    return ResponseHelper::responseError(
                        'The request date is not available',
                        SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                        'experience_date_not_available',
                        'experience_date'
                    );
                }
            }
        }

        // Validate number of seats

        $min = $period->min_person;

        $rules = [
            'kids' => 'numeric|max:' . $experienceVersion->kids,
            'babies' => 'numeric|max:' . $experienceVersion->babies,
            'seats' => 'numeric|min:' . $min . '|max:' . $experienceVersion->seats
        ];

        $attributes['attributes']['seats'] = $attributes['attributes']['adults'] +
            $attributes['attributes']['kids'] +
            $attributes['attributes']['babies'];

        $validator = \Validator::make($attributes['attributes'], $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $seats = $attributes['attributes']['seats'];

        // Fetch account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check if already exists a booking with this date and for this experience version and user
        // that it is not rejected or cancelled

        $experienceVersions = $experience->experienceVersions()->get();
        $experienceVersionIds = [];

        foreach ($experienceVersions as $experienceVersionOfExperience) {
            $experienceVersionIds[] = $experienceVersionOfExperience->id;
        }

        $booking = Booking::all()
            ->whereIn('experience_version_id', $experienceVersionIds)
            ->where('user_id', $account->id)
            ->where('experience_date', $experienceDate)
            ->where('status', '!=', Constant::BOOKING_STATUS_CANCELLED)
            ->where('status', '!=', Constant::BOOKING_STATUS_REJECTED)
            ->last();

        if (isset($booking)) {
            return ResponseHelper::responseError(
                'Booking request already exist for this date',
                SymfonyResponse::HTTP_FORBIDDEN,
                'booking_already_exists',
                'experience_date'
            );
        }

        // Check for authorization

        if (Gate::forUser($account)->denies('create-booking', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to create a booking',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized');
        }

        // Calculate price
        $currencyCaptain = $experienceVersion->experience()->first()->boat()->first()->user()->first()->currency;

        // Add price for each requested fixed service
        $services_price = 0;

        $fixed_additional_service_prices = [];
        foreach ($fixedServices as $fixedService) {
            if ($fixedService->per_person) {
                $fixedServicePrice = $fixedService->price * $seats;
            } else {
                $fixedServicePrice = $fixedService->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($fixedService->currency, $currencyCaptain, $fixedServicePrice));
            $fixed_additional_service_prices[] = [
                'id' => $fixedService->id,
                'name' => $fixedService->fixedAdditionalService()->first()->name,
                'price' => $tempPrice,
                'per_person' => $fixedService->fixedAdditionalService()->first()->per_person,
            ];
            $services_price += $tempPrice;
        }

        // Add price for each requested additional service

        $additional_services = [];
        foreach ($additionalServices as $additionalService) {
            if ($additionalService->per_person) {
                $additionalServicePrice = $additionalService->price * $seats;
            } else {
                $additionalServicePrice = $additionalService->price;
            }
            $tempPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $currencyCaptain, $additionalServicePrice));
            $additional_services[] = [
                'id' => $additionalService->id,
                'name' => $additionalService->name,
                'price' => $tempPrice,
                'per_person' => $additionalService->per_person,
            ];
            $services_price += $tempPrice;
        }


        $estimatedPrice = PriceHelper::estimatePrice(
            $seats,
            $services_price,
            $currencyCaptain,
            $period
        );

        // TODO: find a way to mantain the value of coupon valid until the coupon is registered on booking

        DB::beginTransaction();

        try {

            // Check for coupon validity

            $coupon = null;
            if (isset($attributes['attributes']['coupon'])) {

                // Check for coupon existence, not consumed and not expired

                $coupon = Coupon::all()
                    ->where('name', $attributes['attributes']['coupon'])
                    ->last();

                if (!isset($coupon) || $coupon->is_consumed ||
                    ($coupon->expiration_date != null &&
                        !Carbon::today()->lte(Carbon::createFromFormat('Y-m-d', $coupon->expiration_date)))
                ) {
                    return ResponseHelper::responseError(
                        'Coupon does not exist',
                        SymfonyResponse::HTTP_NOT_FOUND,
                        'coupon_not_found',
                        'coupon'
                    );
                }

                // Check if coupon is associated with this account and not consumed

                $couponUsers = $coupon->couponsUsers()->get();

                if (!$couponUsers->isEmpty()) {
                    $couponUser = $couponUsers->where('user_id', $account->id)->first();

                    if (!isset($couponUser)) {
                        return ResponseHelper::responseError(
                            'You cannot use this coupon',
                            SymfonyResponse::HTTP_FORBIDDEN,
                            'not_authorized_coupon',
                            'coupon'
                        );
                    } else {
                        if ($couponUser->is_coupon_consumed) {
                            return ResponseHelper::responseError(
                                'Coupon does not exist',
                                SymfonyResponse::HTTP_NOT_FOUND,
                                'coupon_not_found',
                                'coupon'
                            );
                        }
                    }
                }

                // Check if coupon is associated with this experience

                if (isset($coupon->experience_id) && $coupon->experience_id != $experience->id) {
                    return ResponseHelper::responseError(
                        'You cannot use this coupon for this experience',
                        SymfonyResponse::HTTP_FORBIDDEN,
                        'not_authorized_coupon_experience',
                        'coupon'
                    );
                }

                // Check if experience of coupon is in the area

                $area = $coupon->area()->first();

                if (isset($area) && !AreaHelper::inArea($area, $experienceVersion->departure_lat, $experienceVersion->departure_lng)) {
                    return ResponseHelper::responseError(
                        'You cannot use this coupon for this area',
                        SymfonyResponse::HTTP_FORBIDDEN,
                        'not_authorized_coupon_area',
                        'coupon'
                    );
                }
            }

            // Initialice
            $discount = 0;

            // Fetch boat for fee

            $boat = $experience->boat()->first();

            // Calculate other price
            $nausdream_fee = floor(($estimatedPrice / 100) * $boat->fee);

            // Calculate eventual discount
            if (isset($coupon)) {
                if ($coupon->is_percentual) {
                    $discount = ceil(($estimatedPrice / 100) * $coupon->amount);
                } else {
                    $discount = ceil(PriceHelper::convertPrice($coupon->currency, $currencyCaptain, $coupon->amount));
                }
            }

            // Calculate each price
            $price_to_pay = $nausdream_fee - $discount;
            $price_on_board = $estimatedPrice - $nausdream_fee;

            // Create booking

            $booking = new Booking([
                'experience_date' => $experienceDate,
                'request_date' => Carbon::now()->format('Y-m-d'),
                'status' => Constant::BOOKING_STATUS_PENDING,
                'price' => $estimatedPrice,
                'adults' => $attributes['attributes']['adults'],
                'kids' => $attributes['attributes']['kids'],
                'babies' => $attributes['attributes']['babies'],
                'coupon' => $attributes['attributes']['coupon'],
                'currency' => $currencyCaptain,
                'entire_boat' => $period->entire_boat,
                'fee' => $boat->fee,
                'discount' => $discount,
                'price_on_board' => $price_on_board,
                'price_to_pay' => $price_to_pay,
            ]);

            $booking->user_id = $account->id;
            $booking->experience_version_id = $experienceVersion->id;
            $booking->save();

            if (isset($coupon) && $coupon->is_consumable) {
                if (isset($couponUser)) {
                    $couponUser->is_coupon_consumed = true;
                    $couponUser->save();
                } else {
                    $coupon->is_consumed = true;
                    $coupon->save();
                }
            }

            // Associate fixed additional services

            $booking->fixedAdditionalServices()->attach($fixedServiceIds);

            // Associate additional services

            $booking->additionalServices()->attach($additionalServiceIds);

        } catch (\Exception $e) {
            DB::rollBack();
            return ResponseHelper::responseError(
                'It is not possible to register the booking',
                SymfonyResponse::HTTP_FORBIDDEN,
                'booking_not_possible'
            );
        }

        DB::commit();

        // Send mails
        dispatch((new SendMail(new BookingRequestedToCaptain($booking)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendMail(new BookingRequestedToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

        // Send sms to captain
        dispatch((new SendSms(new BookingRequestedToCaptainSms($booking)))->onQueue(env('SQS_SMS')));

        // Send sms to alert user if he still has to activate email
        if (!$account->is_mail_activated && $account->is_phone_activated) {
            dispatch((new SendSms(new AlertToCheckEmailToUser($account)))->onQueue(env('SQS_SMS')));
        }

        // If there is a conversation for the request date and experience bind booking to this conversation

        if (!isset($conversation)) {
            $conversation = Conversation::all()
                ->where('user_id', $account->id)
                ->where('experience_id', $experience->id)
                ->where('request_date', $experienceDate)
                ->last();
        }

        if (isset($conversation)) {
            $booking->conversation_id = $conversation->id;
            $booking->save();
        }

        // Create relationship data

        $relationshipData = $this->getRelationshipData($booking);

        // Get included resources
        $includedResources = $this->getIncludedResources($fixed_additional_service_prices, $additional_services);

        // Get transformer

        $transformer = new BookingTransformer();
        $transformer->setServicesPrice($services_price);

        // Return response

        return ResponseHelper::responsePost(
            $this->type,
            $booking,
            $transformer->transform($booking),
            $includedResources,
            $relationshipData,
            null,
            [
                'additional-services' => 'additional_services',
                'fixed-additional-service-prices' => 'fixed_additional_service_prices'
            ]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // Check for booking existence

        $booking = Booking::find($id);

        if (!isset($booking)) {
            return ResponseHelper::responseError(
                'Booking not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'booking_not_found'
            );
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-booking', $booking)) {

            // Get relationships
            $relationshipData = $this->getRelationshipData($booking);

            // Get included resources
            // Set seats to calculate price of services and initialize services price
            $seats = $booking->adults + $booking->babies + $booking->kids;
            $services_price = 0;

            // Get fixed additional service prices array

            $fixedServices = $booking->fixedAdditionalServices()->get();
            $experienceVersion = $booking->experienceVersion()->first();
            $fixed_additional_service_prices = [];
            foreach ($fixedServices as $fixedService) {
                $fixedServicePriceObject = ExperienceVersionsFixedAdditionalServices::all()
                    ->where('fixed_additional_service_id', $fixedService->id)
                    ->where('experience_version_id', $experienceVersion->id)
                    ->last();
                if ($fixedService->per_person) {
                    $fixedServicePrice = $fixedServicePriceObject->price * $seats;
                } else {
                    $fixedServicePrice = $fixedServicePriceObject->price;
                }
                $tempPrice = ceil(PriceHelper::convertPrice($fixedServicePriceObject->currency, $booking->currency, $fixedServicePrice));
                $fixed_additional_service_prices[] = [
                    'id' => $fixedServicePriceObject->id,
                    'name' => $fixedService->name,
                    'price' => $tempPrice,
                    'per_person' => $fixedServicePriceObject->per_person,
                ];
                $services_price += $tempPrice;
            }

            // Add price for each requested additional service

            $additionalServices = $booking->additionalServices()->get();
            $additional_services = [];
            foreach ($additionalServices as $additionalService) {
                if ($additionalService->per_person) {
                    $additionalServicePrice = $additionalService->price * $seats;
                } else {
                    $additionalServicePrice = $additionalService->price;
                }
                $tempPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $booking->currency, $additionalServicePrice));
                $additional_services[] = [
                    'id' => $additionalService->id,
                    'name' => $additionalService->name,
                    'price' => $tempPrice,
                    'per_person' => $additionalService->per_person,
                ];
                $services_price += $tempPrice;
            }

            $includedResources = $this->getIncludedResources($fixed_additional_service_prices, $additional_services);

            // Return response

            $transformer = new BookingTransformer();
            $transformer->setServicesPrice($services_price);

            return ResponseHelper::responseGet(
                $this->type,
                $booking,
                $transformer->transform($booking),
                $includedResources,
                $relationshipData,
                null,
                [
                    'additional-services' => 'additional_services',
                    'fixed-additional-service-prices' => 'fixed_additional_service_prices'
                ]
            );
        }

        return ResponseHelper::responseError(
            'You cannot access this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Check for booking existence
        $booking = Booking::find($id);

        if (!isset($booking)) {
            return ResponseHelper::responseError(
                'Booking not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'booking_not_found'
            );
        }

        // Fetch parameters
        $attributes = $this->getPatchAttributes($request);

        // Validation of parameters
        $rules = [
            'status' => 'required|in:' . Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_ACCEPTED] . ',' . Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_REJECTED],
        ];

        $validator = \Validator::make($attributes, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check that booking is in pending status
        if ($booking->status != Constant::BOOKING_STATUS_PENDING) {
            return ResponseHelper::responseError(
                'Booking is not pending status',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'booking_not_pending'
            );
        }

        // Check for authorization
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('answer-booking', $booking)) {

            // Get eventual coupon
            $coupon = Coupon::where('name', $booking->coupon)->first();

            // Get included resources
            // Set seats to calculate price of services and initialize services price
            $seats = $booking->adults + $booking->babies + $booking->kids;
            $services_price = 0;

            // Get fixed additional service prices array

            $fixedServices = $booking->fixedAdditionalServices()->get();
            $experienceVersion = $booking->experienceVersion()->first();
            $fixed_additional_service_prices = [];
            foreach ($fixedServices as $fixedService) {
                $fixedServicePriceObject = ExperienceVersionsFixedAdditionalServices::all()
                    ->where('fixed_additional_service_id', $fixedService->id)
                    ->where('experience_version_id', $experienceVersion->id)
                    ->last();
                if ($fixedService->per_person) {
                    $fixedServicePrice = $fixedServicePriceObject->price * $seats;
                } else {
                    $fixedServicePrice = $fixedServicePriceObject->price;
                }
                $tempPrice = ceil(PriceHelper::convertPrice($fixedServicePriceObject->currency, $booking->currency, $fixedServicePrice));
                $fixed_additional_service_prices[] = [
                    'id' => $fixedServicePriceObject->id,
                    'name' => $fixedService->name,
                    'price' => $tempPrice,
                    'per_person' => $fixedServicePriceObject->per_person,
                ];
                $services_price += $tempPrice;
            }

            // Add price for each requested additional service

            $additionalServices = $booking->additionalServices()->get();
            $additional_services = [];
            foreach ($additionalServices as $additionalService) {
                if ($additionalService->per_person) {
                    $additionalServicePrice = $additionalService->price * $seats;
                } else {
                    $additionalServicePrice = $additionalService->price;
                }
                $tempPrice = ceil(PriceHelper::convertPrice($additionalService->currency, $booking->currency, $additionalServicePrice));
                $additional_services[] = [
                    'id' => $additionalService->id,
                    'name' => $additionalService->name,
                    'price' => $tempPrice,
                    'per_person' => $additionalService->per_person,
                ];
                $services_price += $tempPrice;
            }

            // If accepted

            if ($attributes['status'] == Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_ACCEPTED]) {

                // Validation of price

                $rules = [
                    'price' => 'required|integer|min:10',
                ];

                $validator = \Validator::make($attributes, $rules);
                if (!$validator->passes()) {
                    return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
                }

                // Check that price is at least greater than 1 to services_price
                $price = $attributes['price'];

                if ($services_price >= $price) {
                    return ResponseHelper::responseError(
                        'Price has to be at least greater than 1 unit than total services price',
                        SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                        'booking_price_lte_services_price',
                        'price'
                    );
                }

                // Set the new price, fee, discount, price_on_board and price_to_pay definitively

                // Initialice
                $discount = 0;

                // Fetch boat for fee

                $boat = $booking->experienceVersion()->first()->experience()->first()->boat()->first();
                $currencyCaptain = $boat->user()->first()->currency;

                // Calculate other price
                $nausdream_fee = floor(($price / 100) * $boat->fee);

                // Calculate eventual discount
                if (isset($coupon)) {
                    if ($coupon->is_percentual) {
                        $discount = ceil(($price / 100) * $coupon->amount);
                    } else {
                        $discount = ceil(PriceHelper::convertPrice($coupon->currency, $currencyCaptain, $coupon->amount));
                    }
                }

                // Calculate each price
                $price_to_pay = $nausdream_fee - $discount;
                $price_on_board = $price - $nausdream_fee;

                // Set prices
                $booking->price = $price;
                $booking->fee = $boat->fee;
                $booking->price_on_board = $price_on_board;
                $booking->price_to_pay = $price_to_pay;
                $booking->discount = $discount;
                $booking->currency = $currencyCaptain;

                // Set status
                $booking->status = Constant::BOOKING_STATUS_ACCEPTED;

                // Set acceptance_date
                $booking->acceptance_date = Carbon::now()->format('Y-m-d');

                // Save booking
                $booking->save();
            }

            // If rejected
            if ($attributes['status'] == Constant::BOOKING_STATUS_NAMES[Constant::BOOKING_STATUS_REJECTED]) {

                // If there is a coupon and it was consumable restore to not consumed status
                if (isset($coupon) && $coupon->is_consumable) {

                    $couponUser = $coupon->couponsUsers()->where('user_id', $booking->user_id)->first();

                    if (isset($couponUser)) {
                        $couponUser->is_coupon_consumed = false;
                        $couponUser->save();
                    } else {
                        $coupon->is_consumed = false;
                        $coupon->save();
                    }
                }

                // Set status
                $booking->status = Constant::BOOKING_STATUS_REJECTED;

                // Save booking
                $booking->save();
            }

            // Send emails
            dispatch((new SendMail(new BookingAnswerToGuest($booking)))->onQueue(env('SQS_MAIL')));
            dispatch((new SendMail(new BookingAnswerToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

            // Get relationships
            $relationshipData = $this->getRelationshipData($booking);

            // Get included
            $includedResources = $this->getIncludedResources($fixed_additional_service_prices, $additional_services);

            // Return response
            $transformer = new BookingTransformer();
            $transformer->setServicesPrice($services_price);

            return ResponseHelper::responseGet(
                $this->type,
                $booking,
                $transformer->transform($booking),
                $includedResources,
                $relationshipData,
                null,
                [
                    'additional-services' => 'additional_services',
                    'fixed-additional-service-prices' => 'fixed_additional_service_prices'
                ]
            );
        }

        // Return response for no authorization
        return ResponseHelper::responseError(
            'Not authorized to answer the booking',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the user bookings.
     *
     * @param  \Illuminate\Http\Request Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getUserBookings(Request $request, $id)
    {
        $user = User::find($id);

        // Check if user exists

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-user-bookings', $user)) {

            // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
            // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
            $perPage = $request->per_page;
            $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOOKINGS_PER_PAGE) : Constant::BOOKINGS_PER_PAGE;
            $page = $request->page;

            // Get user bookings sorted by updated_at first, and then by id
            $bookings = $user->bookings()
                ->orderBy('id', 'desc')
                ->orderBy('updated_at', 'desc')->get();

            // Set page to fetch and if value is out of range, fetch page 1
            $currentPage = (
                isset($page) &&
                (int)$page > 0 &&
                (int)$page <= ceil($bookings->count() / $perPage)) ?
                min($page, ceil($bookings->count() / $perPage)) :
                1;

            $paginatedBookings = new LengthAwarePaginator(
                $bookings->slice($perPage * ($currentPage - 1), $perPage),
                $bookings->count(),
                $perPage,
                $currentPage
            );

            $transformer = new BookingListTransformer();

            return response(Paginator::getPaginatedData($paginatedBookings, $transformer, $perPage, $request));
        }

        return ResponseHelper::responseError(
            'You cannot access this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get the captain bookings.
     *
     * @param  \Illuminate\Http\Request Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getCaptainBookings(Request $request, $id)
    {
        $captain = User::find($id);

        // Check if user exists

        if (!isset($captain)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-captain-bookings', $captain)) {

            // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
            // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
            $perPage = $request->per_page;
            $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOOKINGS_PER_PAGE) : Constant::BOOKINGS_PER_PAGE;
            $page = $request->page;

            // Get captain bookings ids
            $bookingCollectionIds = DB::table('bookings')
                ->join('experience_versions', 'bookings.experience_version_id', '=', 'experience_versions.id')
                ->join('experiences', 'experience_versions.experience_id', '=', 'experiences.id')
                ->join('boats', 'experiences.boat_id', '=', 'boats.id')
                ->join('users', function ($join) use ($captain) {
                    $join->on('boats.user_id', '=', 'users.id')
                        ->where('users.id', '=', $captain->id);
                })
                ->select('bookings.id')
                ->get();

            $bookingIds = [];
            foreach ($bookingCollectionIds as $bookingCollectionId) {
                $bookingIds[] = $bookingCollectionId->id;
            }

            // Get bookings sorted by Ids

            $bookings = Booking::all()
                ->whereIn('id', $bookingIds)
                ->sortByDesc('id')
                ->sortByDesc('updated_at');

            // Set page to fetch and if value is out of range, fetch page 1
            $currentPage = (
                isset($page) &&
                (int)$page > 0 &&
                (int)$page <= ceil($bookings->count() / $perPage)) ?
                min($page, ceil($bookings->count() / $perPage)) :
                1;

            $paginatedBookings = new LengthAwarePaginator(
                $bookings->slice($perPage * ($currentPage - 1), $perPage),
                $bookings->count(),
                $perPage,
                $currentPage
            );

            $transformer = new BookingListTransformer();

            return response(Paginator::getPaginatedData($paginatedBookings, $transformer, $perPage, $request));
        }

        return ResponseHelper::responseError(
            'You cannot access this user data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Set as paid the booking and send emails confirmation
     *
     * @param $id
     */
    public function payBookingRequest($id)
    {
        // Check for booking existence
        $booking = Booking::find($id);
        if (isset($booking)) {

            // Check that booking is in accepted status
            if ($booking->status == Constant::BOOKING_STATUS_ACCEPTED) {

                // Set new status
                $booking->status = Constant::BOOKING_STATUS_PAID;
                $booking->save();

                // Send emails
                dispatch((new SendMail(new BookingPaidToGuest($booking)))->onQueue(env('SQS_MAIL')));
                dispatch((new SendMail(new BookingPaidToCaptain($booking)))->onQueue(env('SQS_MAIL')));
                dispatch((new SendMail(new BookingPaidToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

                // Send sms
                dispatch((new SendSms(new BookingPaidToCaptainSms($booking)))->onQueue(env('SQS_SMS')));

                // Check if it is already 3 days less to experience date
                if ($booking->experience_date <= Carbon::now()->addDays(3)->toDateString()) {

                    //
                    dispatch((new SendMail(new PreExperienceToGuest($booking)))->onQueue(env('SQS_MAIL')));
                    dispatch((new SendMail(new PreExperienceToCaptain($booking)))->onQueue(env('SQS_MAIL')));
                    $booking->is_pre_experience_mail_sent = true;
                    $booking->save();
                }
            }
        }
    }


    /**
     * Retrieve post booking fields
     *
     * @param Request $request
     * @return array
     */
    protected function getPostAttributes(Request $request)
    {
        // Fetch main attributes
        $attributes = [
            'attributes' => [
                'experience_date' => $request->input('data.attributes.experience_date'),
                'adults' => $request->input('data.attributes.adults'),
                'kids' => $request->input('data.attributes.kids'),
                'babies' => $request->input('data.attributes.babies'),
                'coupon' => $request->input('data.attributes.coupon'),
            ]
        ];

        // Fetch experience relationship data
        $attributes['experience'] = [
            'id' => $request->input('data.relationships.experience.data.id'),
            'type' => $request->input('data.relationships.experience.data.type'),
        ];

        // Fetch conversation relationship data
        $attributes['conversation'] = [
            'id' => $request->input('data.relationships.conversation.data.id'),
            'type' => $request->input('data.relationships.conversation.data.type'),
        ];

        // Fetch fixed additional service price relationship
        $fixedAdditionalServicePrices = $request->input('data.relationships.fixed_additional_service_prices.data');
        if (isset($fixedAdditionalServicePrices)) {
            foreach ($fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
                $attributes['fixed-additional-service-prices'][] = [
                    'id' => $fixedAdditionalServicePrice['id'],
                    'type' => $fixedAdditionalServicePrice['type']
                ];
            }
        } else {
            $attributes['fixed-additional-service-prices'] = [];
        }


        // Fetch additional service price relationships
        $additionalServices = $request->input('data.relationships.additional_services.data');
        if (isset($additionalServices)) {
            foreach ($additionalServices as $additionalService) {
                $attributes['additional-services'][] = [
                    'id' => $additionalService['id'],
                    'type' => $additionalService['type']
                ];
            }
        } else {
            $attributes['additional-services'] = [];
        }

        return $attributes;
    }

    /**
     * Retrieve patch booking fields
     *
     * @param Request $request
     * @return array
     */
    protected function getPatchAttributes(Request $request)
    {
        // Fetch main attributes

        $attributes = [
            'price' => $request->input('data.attributes.price'),
            'status' => $request->input('data.attributes.status')
        ];

        return $attributes;
    }

    /**
     * Get relationship data array
     *
     * @param Booking $booking
     * @return array
     */
    public function getRelationshipData(Booking $booking)
    {

        $relationshipData = [];

        // Experience relationship
        $experienceVersion = $booking->experienceVersion()->first();
        $experience = $experienceVersion->experience()->first();
        $relationshipData['experience'] = JsonHelper::createDataMessage(
            JsonHelper::createData('experiences', $experience->id)
        );

        // Captain relationship

        $captainId = $experience->boat()->first()->user()->first()->id;
        $relationshipData['captain'] = JsonHelper::createDataMessage(
            JsonHelper::createData('users', $captainId)
        );

        // User relationship

        $userId = $booking->user_id;
        $relationshipData['guest'] = JsonHelper::createDataMessage(
            JsonHelper::createData('users', $userId)
        );

        // Conversation relationship

        $conversation = $booking->conversation()->first();
        if (isset($conversation)) {
            $relationshipData['conversation'] = JsonHelper::createDataMessage(
                JsonHelper::createData('conversations', $conversation->id)
            );
        }

        return $relationshipData;
    }

    /**
     * Get the arrays of the included elements
     *
     * @param $fixedAdditionalServicePrices
     * @param $additionalServices
     * @return array
     */
    protected function getIncludedResources($fixedAdditionalServicePrices, $additionalServices)
    {
        $includes = [];

        // Fixed additional services
        foreach ($fixedAdditionalServicePrices as $fixedAdditionalServicePrice) {
            $includes[] = JsonHelper::createData(
                'fixed-additional-service-prices',
                $fixedAdditionalServicePrice['id'],
                [
                    'name' => $fixedAdditionalServicePrice['name'],
                    'price' => $fixedAdditionalServicePrice['price'],
                    'per_person' => $fixedAdditionalServicePrice['per_person']
                ]
            );
        }

        // Additional services

        foreach ($additionalServices as $additionalService) {
            $includes[] = JsonHelper::createData(
                'additional-services',
                $additionalService['id'],
                [
                    'name' => $additionalService['name'],
                    'price' => $additionalService['price'],
                    'per_person' => $additionalService['per_person']
                ]
            );
        }

        return $includes;
    }
}
