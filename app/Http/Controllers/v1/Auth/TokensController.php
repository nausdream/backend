<?php

namespace App\Http\Controllers\v1\Auth;

use App\Models\Token;
use App\Services\ResponseHelper;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Constant;
use App\Services\JwtService;
use App\Services\JsonHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class TokensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function check(Request $request)
    {
        $input = $request->input('meta');

        // Set validation rules

        $rules = [
            'token' => 'required|string|size:' . Constant::EMAIL_TOKEN_LENGTH
        ];

        // Check rules on input

        $valid = \Validator::make($input, $rules);

        // Check validation rule

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get token object

        $token = $input['token'];

        $tokenObject = Token::where('token', '=', $token)
            ->first();

        // Check for existence

        if (!isset($tokenObject)) {
            return ResponseHelper::responseError('Token not found', SymfonyResponse::HTTP_NOT_FOUND, 'token_not_found');
        }

        $user = $tokenObject->user;

        // Set email as verified if is_mail_token
        if ($tokenObject->is_mail_token) {
            $user->is_mail_activated = true;
            $user->save();
        }

        // Delete token
        $tokenObject->delete();

        // Set response
        return ResponseHelper::responseForLogin($user);
    }


}
