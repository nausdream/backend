<?php

namespace App\Http\Controllers\v1\Auth;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\RequestService;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class AccountKitController extends Controller
{
    protected $me_endpoint_base_url;
    protected $token_exchange_base_url;
    protected $app_access_token;
    protected $grant_type = 'authorization_code';

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        $this->me_endpoint_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/me';
        $this->token_exchange_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/access_token';
        $this->app_access_token = join('|', ['AA', env('FB_APP_ID'), env('FB_AK_APP_SECRET')]);
    }

    /**
     * Get user id and token from user or register it on the database
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        /*
         * 1. Get Fb response code from Api consumer
         * 2. Send authorization code to Fb /token endpoint and get an access token
         * 3. Send said token to Fb /me endpoint and retrieve user email/phone
         * 4. Create token from user data (fetch user from DB or create it as a new one)
         */

        // 1. Get Fb response code from Api consumer
        $code = $request->input('meta')['code'];

        // 2. Send authorization code to Fb /token endpoint and get an access token
        $request_body = [
            'grant_type' => $this->grant_type,
            'code' => $code,
            'access_token' => $this->app_access_token,
        ];

        $res = RequestService::get($this->token_exchange_base_url, $request_body);

        if (!isset($res) || $res->getStatusCode() != SymfonyResponse::HTTP_OK) {
            return $res;
        }
        $data = json_decode((string)$res->getBody());
        $user_access_token = $data->{'access_token'};


        // 3. Send said token to Fb /me endpoint and retrieve user email/phone
        $request_body = [
            'access_token' => $user_access_token,
        ];

        $res = RequestService::get($this->me_endpoint_base_url, $request_body);

        if (! isset($res) || $res->getStatusCode() != SymfonyResponse::HTTP_OK){
            return null;
        }

        $data = json_decode((string)$res->getBody());
        $phone = $data->{'phone'}->{'number'};

        return $phone;
    }
}