<?php

namespace App\Http\Controllers\v1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\ResponseHelper;
use Gate;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Get user id and token with email and password
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        // Get input request attribute

        $input = $request->input('meta');

        // Validate fields

        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $input['email'];
        $password = $input['password'];

        // Check for email existence

        $user = User::all()->where('email', $email)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'email_not_found',
                'email'
            );
        }

        // Check for email activated

        $user = User::all()->where('email', $email)->where('is_mail_activated', true)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email is not activated',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'email_not_activated',
                'email'
            );
        }

        // Check that user is captain and/or partner to login with password

        if (Gate::forUser($user)->denies('password-login')) {
            return ResponseHelper::responseError(
                'User must be a captain or partner',
                SymfonyResponse::HTTP_FORBIDDEN,
                'user_not_captain_or_partner',
                'email'
            );
        }

        // Attempt authentication

        $credentials = ['email' => $email, 'password' => $password, 'is_mail_activated' => 1];

        if (Auth::once($credentials)) {
            return ResponseHelper::responseForLogin($user);
        }

        return ResponseHelper::responseError(
            'Password provided is incorrect',
            SymfonyResponse::HTTP_FORBIDDEN,
            'incorrect_password'
        );
    }
}
