<?php

namespace App\Http\Controllers\v1\Auth;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Mail\RestorePassword;
use App\Models\Token;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Jobs\SendMail;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * ResetPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['restore', 'reset']);
        $this->middleware('jwt')->except(['restore', 'reset']);
    }

    /**
     * Send restore password email
     *
     * @param Request $request
     * @return Response
     */
    public function restore(Request $request)
    {
        // Get input request attribute

        $input = $request->input('meta');

        // Validate fields

        $rules = [
            'email' => 'required|email',
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $email = $input['email'];

        // Check for email existence

        $user = User::all()->where('email', $email)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email does not exist',
                SymfonyResponse::HTTP_NOT_FOUND,
                'email_not_found',
                'email'
            );
        }

        // Check for email activated

        $user = User::all()->where('email', $email)->where('is_mail_activated', true)->last();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Email is not activated',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'email_not_activated',
                'email'
                );
        }

        // Check that user is captain and/or partner to restore password

        if (Gate::forUser($user)->denies('reset-password')) {
            return ResponseHelper::responseError(
                'User must be a captain or partner',
                SymfonyResponse::HTTP_FORBIDDEN,
                'user_not_captain_or_partner',
                'email'
            );
        }

        dispatch((new SendMail(new RestorePassword($user)))->onQueue(env('SQS_MAIL')));

        return response('', SymfonyResponse::HTTP_OK);
    }


    /**
     * Reset password with token
     *
     * @param Request $request
     * @return Response
     */
    public function reset(Request $request)
    {
        $input = $request->input('meta');

        // Set validation rules

        $rules = [
            'password' => 'required|string|min:' . Constant::MIN_PASSWORD_LENGTH . '|max:' . Constant::MAX_PASSWORD_LENGTH,
            'token' => 'required|string|size:' . Constant::EMAIL_TOKEN_LENGTH
        ];

        // Check rules on input

        $valid = \Validator::make($input, $rules);

        // Check validation rule

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for token existence

        $token = $input['token'];
        $tokenObject = Token::where('token', '=', $token)
            ->first();

        if (!isset($tokenObject)) {
            return ResponseHelper::responseError('Token not found', SymfonyResponse::HTTP_NOT_FOUND, 'token_not_found');
        }

        // Get user by token string

        $password = $input['password'];

        $user = $tokenObject->user;

        // Delete token

        $tokenObject->delete();

        // Check that user is captain and/or partner to restore password

        if (Gate::forUser($user)->denies('reset-password')) {
            return ResponseHelper::responseError(
                'User must be a captain or partner',
                SymfonyResponse::HTTP_FORBIDDEN,
                'user_not_captain_or_partner'
            );
        }

        // Set new password

        $user->password = \Hash::make($password);
        $user->is_set_password = 1;
        $user->save();

        $userTransformer = new UserTransformer();
        return ResponseHelper::responseUpdate('users', $user, $userTransformer->transform($user));
    }
}
