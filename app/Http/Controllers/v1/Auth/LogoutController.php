<?php

namespace App\Http\Controllers\v1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Gate;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class LogoutController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * LogoutController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
    }

    /**
     * Get user id and token with email and password
     *
     * @param Request $request
     *
     * @return Response
     */
    public function logout(Request $request)
    {
        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check that user is captain and/or partner to login with password

        if (Gate::forUser($account)->denies('logout')) {
            return ResponseHelper::responseError(
                'You are not logged in',
                SymfonyResponse::HTTP_FORBIDDEN,
                'no_account_relation_with_jwt');
        }

        \Cookie::forget('csrf_token');

        return response('', SymfonyResponse::HTTP_OK);
    }
}
