<?php

namespace App\Http\Controllers\v1\Auth;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Jobs\SendMail;
use App\Mail\EmailUpdated;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Facebook as FB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class FacebookAuthController extends Controller
{
    /**
     * Receive scopes and access token from the frontend and return JWT token
     *
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        //sets data
        $accessToken = $request->input('meta.access_token');
        $scopes = "id,first_name,last_name,email";

        $fb = new FB\Facebook([
            'app_id' => env('FB_APP_ID'),
            'app_secret' => env('FB_APP_SECRET'),
            'default_graph_version' => 'v2.8',
        ]);

        try {
            $response = $fb->get('/me?fields=' . $scopes, $accessToken);
        } catch (FB\Exceptions\FacebookResponseException $e) {
            $errors = JsonHelper::createErrorMessage(
                [JsonHelper::createError(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, $e->getMessage())]
            );
            return response($errors, SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        } catch (FB\Exceptions\FacebookSDKException $e) {
            $errors = JsonHelper::createErrorMessage(
                [JsonHelper::createError(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, $e->getMessage())]
            );
            return response($errors, SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = $response->getGraphUser();

        if (!isset($user)) {
            return ResponseHelper::responseError(
                'Facebook user not found',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'facebook_user_not_found');
        }

        return $this->facebookLogin($request, $user);
    }

    /**
     * Register or Login user with Facebook, return token already in json data format
     *
     * @param Request $request
     * @param $user
     * @return Response
     */
    private function facebookLogin(Request $request, FB\GraphNodes\GraphUser $user)
    {
        $rules = [
            'first_name' => 'max:' . Constant::FIRST_NAME_LENGTH,
            'last_name' => 'max:' . Constant::LAST_NAME_LENGTH,
            'fuid' => 'required|max:' . Constant::FUID_LENGTH,
            'email' => 'max:' . Constant::EMAIL_LENGTH,
            'language' => 'required|exists:mysql_translation.languages,language',
            'currency' => 'required|exists:currencies,name'
        ];

        $fields = [
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'fuid' => $user->getId(),
            'email' => $user->getEmail(),
            'language' => $request->input('meta.language'),
            'currency' => $request->input('meta.currency')
        ];
        $validator = \Validator::make($fields, $rules);

        if (!$validator->passes()) {
            return response(JsonHelper::createErrorMessage($validator->errors()), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user_fuid = User::where('fuid', $user->getId())->first();

        if (!isset($user_fuid)) { //user with fuid not present in DB

            // Check if user from fb provide email
            $emailFromFb = $user->getEmail();

            if (isset($emailFromFb) && $emailFromFb!= '' && filter_var($emailFromFb, FILTER_VALIDATE_EMAIL)) {
                $user_email = User::where('email', $emailFromFb)->first();

                if (!isset($user_email)) { //user with email not in DB, register user
                    $new_user = factory(User::class)->make();
                    $new_user->first_name = $user->getFirstName();
                    $new_user->last_name = $user->getLastName();
                    $new_user->fuid = $user->getId();
                    $new_user->email = $user->getEmail();
                    $new_user->language = $request->input('meta.language');
                    $new_user->currency = $request->input('meta.currency');

                    $new_user->save();

                    dispatch((new SendMail(new EmailUpdated($new_user)))->onQueue(env('SQS_MAIL')));

                    return ResponseHelper::responseForRegister($new_user);

                } else { //user with email already in db, set other fields if not already set
                    $user_email->fuid = $user->getId();

                    if (!isset($user_email->first_name)) {
                        $user_email->first_name = $user->getFirstName();
                    }
                    if (!isset($user_email->last_name)) {
                        $user_email->last_name = $user->getLastName();
                    }
                    if (!isset($user_email->language)) {
                        $user_email->language = $request->input('meta.language');
                    }
                    if (!isset($user_email->currency)) {
                        $user_email->currency = $request->input('meta.currency');
                    }

                    $user_email->save();

                    return ResponseHelper::responseForLogin($user_email);
                }
            } else {
                return ResponseHelper::responseError(
                    'You need to provide the email address in order to continue login with facebook',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'email_not_provided_from_fb');
            }
        }

        return ResponseHelper::responseForLogin($user_fuid);
    }
}