<?php

namespace App\Http\Controllers\v1\Auth;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Services\JsonHelper;
use App\Services\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\TranslationModels\User as Translator;

class TranslatorAccountKitController extends Controller
{
    protected $me_endpoint_base_url;
    protected $token_exchange_base_url;
    protected $app_access_token;
    protected $grant_type = 'authorization_code';

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except('login');
        $this->me_endpoint_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/me';
        $this->token_exchange_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/access_token';
        $this->app_access_token = join('|', ['AA', env('FB_APP_ID'), env('FB_APP_SECRET')]);
    }

    /**
     * Get translator id and token
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $accountKitController = new AccountKitController();

        $phone = $accountKitController->login($request);

        if ($phone instanceof Response){
            return $phone;
        }

        //4. Create token from user data (fetch user from DB or create it as a new one)
        return $this->accountKitLogin($phone);
    }

    /**
     * Login user with Facebook AccountKit and return JWT
     *
     * @param $phone
     * @return Response
     */
    protected function accountKitLogin($phone)
    {
        //validation
        $rules = [
            'phone' => 'required|max:' . Constant::PHONE_LENGTH
        ];

        $fields = [
            'phone' => $phone
        ];
        $validator = \Validator::make($fields, $rules);

        if (!$validator->passes()) {
            return response(JsonHelper::createErrorMessage($validator->errors()), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        //fetch user with given phone
        $translator = Translator::where('phone', $phone)->first();

        // Check if admin exists
        if (!isset($translator)) {
            return ResponseHelper::responseError('Not a translator', SymfonyResponse::HTTP_FORBIDDEN, 'not_a_translator');
        }

        return ResponseHelper::responseForLogin($translator);
    }
}