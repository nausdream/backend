<?php

namespace App\Http\Controllers\v1\Auth;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\Administrator;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\RequestService;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class UserAccountKitController extends Controller
{
    protected $me_endpoint_base_url;
    protected $token_exchange_base_url;
    protected $app_access_token;
    protected $grant_type = 'authorization_code';

    /**
     * AccountKitController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except('login');
        $this->me_endpoint_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/me';
        $this->token_exchange_base_url = env('FB_GRAPH_URL') . env('FB_AK_API_VERSION') . '/access_token';
        $this->app_access_token = join('|', ['AA', env('FB_APP_ID'), env('FB_APP_SECRET')]);
    }

    /**
     * Get user id and token from user or register it on the database
     *
     * @param Request $request
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $accountKitController = new AccountKitController();

        $phone = $accountKitController->login($request);

        if ($phone instanceof Response){
            return $phone;
        }

        $name = $request->input('meta.name');
        $email = $request->input('meta.email');
        $language = $request->input('meta.language');
        $currency = $request->input('meta.currency');

        //4. Create token from user data (fetch user from DB or create it as a new one)
        return $this->accountKitLogin($phone, $email, $name, $language, $currency);
    }

    /**
     * Login user with Facebook AccountKit and return JWT
     *
     * @param $phone
     * @param $email
     * @param $name
     * @param $language
     * @param $currency
     * @return Response
     */
    protected function accountKitLogin($phone, $email, $name, $language, $currency)
    {
        //validation
        $rules = [
            'first_name' => 'max:' . Constant::FIRST_NAME_LENGTH,
            'phone' => 'required|max:' . Constant::PHONE_LENGTH,
            'email' => 'max:' . Constant::EMAIL_LENGTH,
            'language' => 'required|exists:mysql_translation.languages,language',
            'currency' => 'required|exists:currencies,name'
        ];

        $fields = [
            'first_name' => $name,
            'phone' => $phone,
            'email' => $email,
            'language' => $language,
            'currency' => $currency
        ];
        $validator = \Validator::make($fields, $rules);

        if (!$validator->passes()) {
            return response(JsonHelper::createErrorMessage($validator->errors()), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        //fetch user with given phone
        $user = User::where('phone', $phone)->first();

        if (!isset($user)) { //phone number not in database, register new user

            $new_user = factory(\App\Models\User::class)->make();

            if (isset($email)) {
                $user_db = User::where('email', $email)->first(); //check if a user with the same email exists

                if (!isset($user_db)) { //email not in DB, register new user email too
                    $new_user->email = $email;
                }
            }
            $new_user->phone = $phone;
            $new_user->first_name = $name;
            $new_user->currency = $currency;
            $new_user->language = $language;
            $new_user->is_phone_activated = true;
            //TODO: photo

            $new_user->save();

            return ResponseHelper::responseForRegister($new_user);
        }

        //user already in db with phone, setting other field if null
        $user->is_phone_activated = true;
        if ($user->email == null) {
            $user->email = $email;
        }
        if ($user->first_name == null) {
            $user->first_name = $name;
        }
        //language and currency are set even if not null because they could be the default values set by the model factory
        $user->language = $language;
        $user->currency = $currency;

        $user->save();

        return ResponseHelper::responseForLogin($user);
    }

    /**
     * Verify telephone number when changed
     *
     * @param Request $request
     *
     * @return Response
     */
    public function verify(Request $request)
    {
        // Check for user id validity and user authorization

        // Check if user exists
        $id = $request->input('data')['id'];

        $user = User::find($id);

        if (!isset($user)) {
            return ResponseHelper::responseError('User not found', SymfonyResponse::HTTP_NOT_FOUND, 'user_not_found');
        }

        // Check if the requested user is equal to the sub claim of the jwt

        $jwt = $request->bearerToken();

        // Check if user is authorized to update telephone
        if (Gate::forUser(JwtService::getAccountFromJwt($jwt))->denies('update-user-telephone', $user)) {
            return ResponseHelper::responseError(
                'You cannot update this user data',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized');
        }

        // Get phone number from AccountKit
        $accountKitController = new AccountKitController();
        $phone = $accountKitController->login($request);

        if ($phone instanceof Response){
            return $phone;
        }

        $userPhone = User::where('phone', $phone)
            ->where('is_phone_activated', 1)
            ->where('id', '!=', $id)
            ->first();

        if (isset($userPhone)) {
            return ResponseHelper::responseError(
                'The phone is already used by another account',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'phone_already_used',
                'phone'
            );
        }

        $user->phone = $phone;
        $user->is_phone_activated = true;
        $user->save();

        $transformer = new UserTransformer();

        return ResponseHelper::responseGet('users', $user, $transformer->transform($user));
    }
}