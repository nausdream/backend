<?php
namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\EmailUpdated;
use App\Models\Administrator;
use App\Models\Boat;
use App\Services\Transformers\TranslatorTransformer;
use App\TranslationModels\User as Translator;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\LanguageTransformer;
use App\Services\Transformers\UserListTransformer;
use App\Services\Transformers\UserTransformer;
use App\Services\ValidationService;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\Paginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class TranslatorsController extends Controller
{
    protected $type = 'translators';

    /**
     * UsersController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $translator = Translator::find($id);

        // Check if translator exists

        if (!isset($translator)) {
            return ResponseHelper::responseError(
                'Translator not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'translator_not_found');
        }

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('show-translator', $translator)) {

            // User found in db and request is authorized

            $transformer = new TranslatorTransformer();

            // Fetch for data to includes to response

            $includedResources = $this->getIncludedResources($translator);

            return ResponseHelper::responseGet('translators', $translator, $transformer->transform($translator, $request->input('device')), $includedResources);
        }

        return ResponseHelper::responseError(
            'You cannot access this translator data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get the arrays of the included elements for the specified translator
     *
     * @param $translator
     * @return array
     */
    protected function getIncludedResources($translator)
    {
        $includes = [];
        $languageTransformer = new LanguageTransformer();
        $languageCollection = $translator->languages()->get();
        foreach ($languageCollection as $language) {
            $includes[] = JsonHelper::createData('languages', $language->id, $languageTransformer->transform($language));
        }
        return $includes;
    }
}
