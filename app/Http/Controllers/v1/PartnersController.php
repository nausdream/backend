<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\PartnerRegistered;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gate;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class PartnersController extends Controller
{
    protected $type = 'partners';

    /**
     * CaptainsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show']);
        $this->middleware('jwt');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $this->getRequestAttributes($request);

        // Check type

        $rules = [
            'type' => 'in:users'
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check other attributes

        $rules = [
            'first_name' => 'string|max:' . Constant::FIRST_NAME_LENGTH,
            'last_name' => 'string|max:' . Constant::LAST_NAME_LENGTH,
            'sex' => 'nullable|size:1|in:' . implode(',', Constant::SEX_TYPES),
            'birth_date' => 'nullable|date',
            'email' => 'required|email|max:' . Constant::EMAIL_LENGTH,
            'phone' => 'string|max:' . Constant::PHONE_LENGTH,
            'nationality' => 'nullable|exists:countries,name',
            'currency' => 'required|exists:currencies,name',
            'enterprise_name' => 'string|max:' . Constant::ENTERPRISE_NAME_LENGTH,
            'vat_number' => 'string|max:' . Constant::ENTERPRISE_VAT_LENGTH,
            'enterprise_address' => 'string|max:' . Constant::ENTERPRISE_ADDRESS_LENGTH,
            'enterprise_city' => 'string|max:' . Constant::ENTERPRISE_CITY_LENGTH,
            'enterprise_zip_code' => 'string|max:' . Constant::ENTERPRISE_ZIP_LENGTH,
            'enterprise_country' => 'required|exists:countries,name',
        ];

        $valid = \Validator::make($input, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('post-partner')) {

            // Check if mail and phone already exist users table

            $partnerEmail = User::where('email', $input['email'])
                ->where('is_mail_activated', 1)
                ->first();

            $partnerPhone = User::where('phone', $input['phone'])
                ->where('is_phone_activated', 1)
                ->first();

            // Mail and phone don't exist in users table

            if (!isset($partnerPhone) && !isset($partnerEmail)) {

                $partner = factory(User::class)->make();

                $partner->first_name = $input['first_name'];
                $partner->last_name = $input['last_name'];
                $partner->sex = $input['sex'];
                $partner->birth_date = $input['birth_date'];
                $partner->email = $input['email'];
                $partner->phone = $input['phone'];
                $partner->nationality = $input['nationality'];
                $partner->currency = $input['currency'];
                $partner->enterprise_name = $input['enterprise_name'];
                $partner->vat_number = $input['vat_number'];
                $partner->enterprise_address = $input['enterprise_address'];
                $partner->enterprise_city = $input['enterprise_city'];
                $partner->enterprise_zip_code = $input['enterprise_zip_code'];
                $partner->enterprise_country = $input['enterprise_country'];

                $partner->is_partner = true;

                $partner->save();
                $this->sendMailForCreation($partner);

                $userTransformer = new UserTransformer();

                return response(JsonHelper::createDataMessage(JsonHelper::createData('users', $partner->id, $userTransformer->transform($partner))), SymfonyResponse::HTTP_CREATED);
            }

            // Mail and phone exist in users table

            if (isset($partnerEmail) && isset($partnerPhone)) {
                if (!$partnerEmail->is_partner) {
                    $partnerEmail->is_partner = true;
                    $partnerEmail->save();
                    $this->sendMailForCreation($partnerEmail);
                    return ResponseHelper::responseGet('users', $partnerEmail);
                }
                return ResponseHelper::responseError(
                    "Already a partner",
                    SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                    'already_a_partner',
                    'email'
                );
            }

            // Mail exists in users table

            if (isset($partnerEmail)) {
                if (!$partnerEmail->is_partner) {
                    $partner = $partnerEmail;

                    if (!$partner->phone) {
                        $partner->phone = $input['phone'];
                    }
                    return $this->changePartnerValues($input, $partner);
                }
                return ResponseHelper::responseError(
                    "Already a partner",
                    SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                    'already_a_partner',
                    'email'
                );
            }

            // Phone exists in users table

            if (isset($partnerPhone)) {
                if (!$partnerPhone->is_partner) {
                    $partner = $partnerEmail;

                    if (!$partner->email) {
                        $partner->email = $input['email'];
                    }
                    return $this->changePartnerValues($request, $partner);
                }
                return ResponseHelper::responseError(
                    "Already a partner",
                    SymfonyResponse::HTTP_NOT_ACCEPTABLE,
                    'already_a_partner',
                    'phone'
                );
            }
        }

        return ResponseHelper::responseError(
            'You cannot create this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Change captain values when inserted
     *
     * @param $input
     * @param User $partner
     * @return \Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function changePartnerValues($input, User $partner)
    {
        if (!$partner->first_name) {
            $partner->first_name = $input['first_name'];
        }
        if (!$partner->last_name) {
            $partner->last_name = $input['last_name'];
        }
        if (!$partner->sex) {
            $partner->sex = $input['sex'];
        }
        if (!$partner->birth_date) {
            $partner->birth_date = $input['birth_date'];
        }
        if (!$partner->email) {
            $partner->email = $input['email'];
        }
        if (!$partner->phone) {
            $partner->phone = $input['phone'];
        }
        if (!$partner->nationality) {
            $partner->nationality = $input['nationality'];
        }
        if (!$partner->currency) {
            $partner->currency = $input['currency'];
        }
        if (!$partner->enterprise_name) {
            $partner->enterprise_name = $input['enterprise_name'];
        }
        if (!$partner->vat_number) {
            $partner->vat_number = $input['vat_number'];
        }
        if (!$partner->enterprise_address) {
            $partner->enterprise_address = $input['enterprise_address'];
        }
        if (!$partner->enterprise_city) {
            $partner->enterprise_city = $input['enterprise_city'];
        }
        if (!$partner->enterprise_zip_code) {
            $partner->enterprise_zip_code = $input['enterprise_zip_code'];
        }
        if (!$partner->enterprise_country) {
            $partner->enterprise_country = $input['enterprise_country'];
        }

        $partner->is_partner = true;

        $partner->save();
        $this->sendMailForCreation($partner);

        unset($input['type']);

        return ResponseHelper::responseUpdate('users', $partner, $input);
    }

    /**
     * Send a mail for captain creation to general admin and to captain
     *
     * @param User $partner
     */
    protected function sendMailForCreation(User $partner)
    {
        dispatch((new SendMail(new PartnerRegistered($partner)))->onQueue(env('SQS_MAIL')));
    }

    /**
     * Get array of the attributes and values in request
     *
     * @param Request $request
     * @return array
     */
    protected function getRequestAttributes($request)
    {
        return [
            'type' => $request->input('data.type'),

            'first_name' => $request->input('data.attributes.first_name'),
            'last_name' => $request->input('data.attributes.last_name'),
            'sex' => $request->input('data.attributes.sex'),
            'birth_date' => $request->input('data.attributes.birth_date'),
            'email' => strtolower($request->input('data.attributes.email')),
            'phone' => $request->input('data.attributes.phone'),
            'nationality' => $request->input('data.attributes.nationality'),
            'currency' => $request->input('data.attributes.currency'),
            'enterprise_name' => $request->input('data.attributes.enterprise_name'),
            'vat_number' => $request->input('data.attributes.vat_number'),
            'enterprise_address' => $request->input('data.attributes.enterprise_address'),
            'enterprise_city' => $request->input('data.attributes.enterprise_city'),
            'enterprise_zip_code' => $request->input('data.attributes.enterprise_zip_code'),
            'enterprise_country' => $request->input('data.attributes.enterprise_country'),
        ];
    }
}
