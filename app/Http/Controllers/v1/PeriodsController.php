<?php

namespace App\Http\Controllers\v1;

use App\Models\Administrator;
use App\Models\Experience;
use App\Models\Period;
use App\Models\User;
use App\Services\JwtService;
use App\Services\PriceHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\PeriodTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class PeriodsController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['getPrices']);
        $this->middleware('jwt')->except(['getPrices']);
        $this->type = 'periods';
        $this->transformer = new PeriodTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');
        $entireBoat = $request->input('data.attributes.entire_boat');
        $price = $request->input('data.attributes.price');
        $minPerson = $request->input('data.attributes.min_person');
        $relationshipType = $request->input('data.relationships.experience.data.type');
        $relationshipId = $request->input('data.relationships.experience.data.id');

        // Validate type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $type
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j',
            'entire_boat' => 'boolean',
            'price' => 'integer|numeric|min:0',
            'min_person' => 'integer|numeric|min:1',
            'relationship_type' => 'in:experiences',
            'relationship_id' => 'integer|numeric|min:1'
        ];

        $objectType = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'entire_boat' => $entireBoat,
            'price' => $price,
            'min_person' => $minPerson,
            'relationship_type' => $relationshipType,
            'relationship_id' => $relationshipId
        ];

        // If dateStart and dateEnd are both null, they are accepted
        if (!isset($dateStart) && !isset($dateEnd)) {
            $rules['date_start'] = $rules['date_end'] = '';
        }

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($relationshipId);
        // Check if experience exists
        if (!isset($experience)) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // MANUAL DATE VALIDATION
        $periods = $experience->periods();

        if (isset($dateStart)) {
            // date_start is ahead of date_end
            $wrongOrder = $dateEnd <= $dateStart;
            if ($wrongOrder) {
                return ResponseHelper::responseError(
                    'date_start can not be more recent than or equal to date_end',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'date_start_recent',
                    'date_start');
            }

            // Dates periods overlap with the one in the database
            $dateStartOverlaps = $periods
                    ->where('date_start', '<=', $dateStart)
                    ->where('date_end', '>=', $dateStart)
                    ->get()->count() > 0;

            $dateEndOverlaps = $periods
                    ->where('date_start', '<=', $dateEnd)
                    ->where('date_end', '>=', $dateEnd)
                    ->get()->count() > 0;

            if ($dateStartOverlaps || $dateEndOverlaps) {
                return ResponseHelper::responseError(
                    'This period overlaps with previous ones',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'periods_overlapping',
                    'date_start'
                );
            }
        } else {
            $datesOverlaps = $periods
                    ->where('date_start', null)
                    ->where('date_end', null)
                    ->get()->count() > 0;

            if ($datesOverlaps) {
                return ResponseHelper::responseError(
                    'There is already a fixed price for the entire year',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'fixed_price_entire_year'
                );
            }
        }

        $periodData = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'entire_boat' => $entireBoat,
            'price' => $price,
            'min_person' => $minPerson
        ];

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set correct currency
        $currency = $experience->boat()->first()->user()->first()->currency;
        $periodData['currency'] = $currency;

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('post-period', $experienceVersion)) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Add availability to experience
        $period = new Period($periodData);
        $experience->periods()->save($period);

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($relationshipId);

        // Set attributes to show
        $attributes = $this->transformer->transform($period);

        return ResponseHelper::responsePost(
            $this->type,
            $period,
            $attributes,
            [],
            $relationships,
            $period->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');
        $entireBoat = $request->input('data.attributes.entire_boat');
        $price = $request->input('data.attributes.price');
        $minPerson = $request->input('data.attributes.min_person');
        $isDefault = $request->input('data.attributes.is_default');

        // Validate type & id
        $rules = [
            'type' => 'in:' . $this->type,
            'id' => 'in:' . $id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get Period from db
        $period = Period::find($id);
        if (!isset($period)) {
            return ResponseHelper::responseError(
                'Period not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'period_not_found'
            );
        }

        // Validate other data
        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j',
            'entire_boat' => 'boolean',
            'price' => 'integer|numeric|min:0',
            'min_person' => 'integer|numeric|min:1',
            'is_default' => 'boolean'
        ];

        $data = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'entire_boat' => $entireBoat,
            'price' => $price,
            'min_person' => $minPerson,
            'is_default' => $isDefault
        ];

        // If dateStart or dateEnd are set, they must both be dates
        if (!isset($dateStart) && !isset($dateEnd)) {
            $rules['date_start'] = $rules['date_end'] = '';
        }

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($period->experience_id);
        if (!isset($experience)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // MANUAL DATE VALIDATION
        $periods = $experience->periods()->where('id', '!=', $id);

        if (isset($dateStart)) {
            // date_start is ahead of date_end
            $wrongOrder = $dateEnd <= $dateStart;
            if ($wrongOrder) {
                return ResponseHelper::responseError(
                    'date_start can not be more recent than or equal to date_end',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'date_start_recent',
                    'date_start'
                );
            }

            // Dates periods overlap with the one in the database
            $dateStartOverlaps = $periods
                    ->where('date_start', '<=', $dateStart)
                    ->where('date_end', '>=', $dateStart)
                    ->get()->count() > 0;

            $dateEndOverlaps = $periods
                    ->where('date_start', '<=', $dateEnd)
                    ->where('date_end', '>=', $dateEnd)
                    ->get()->count() > 0;

            if ($dateStartOverlaps || $dateEndOverlaps) {
                return ResponseHelper::responseError(
                    'This period overlaps with previous ones',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'periods_overlapping',
                    'date_start'
                );
            }
        } else {
            $datesOverlaps = $periods
                    ->where('date_start', null)
                    ->where('date_end', null)
                    ->get()->count() > 0;

            if ($datesOverlaps) {
                return ResponseHelper::responseError(
                    'There is already a fixed price for the entire year',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'fixed_price_entire_year');
            }
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersions = $experience->experienceVersions();

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('patch-period', $experienceVersion)) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Clear defaults if is_default is set to true
        if ($isDefault) {
            $defaultPeriod = $experience->periods()->where('is_default', true)->get()->first();
            if (isset($defaultPeriod)) {
                $defaultPeriod->is_default = false;
                $defaultPeriod->save();
            }
        }

        // Update record in database
        $period->fill($data);
        $period->save();

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($experience->id);

        // Set attributes to show
        $attributes = $this->transformer->transform($period);

        return ResponseHelper::responseGet(
            $this->type,
            $period,
            $attributes,
            [],
            $relationships,
            $period->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $period = Period::find($id);

        if (!isset($period)) {
            return ResponseHelper::responseError(
                'Period not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'period_not_found'
            );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experience = Experience::find($period->experience_id);
        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $experienceVersions = $experience->experienceVersions();

        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        if (Gate::forUser($account)->denies('delete-period', $experienceVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $period->prices()->delete();
        $period->delete();

        return response('');
    }

    /**
     * Get all the prices for a specified period.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getPrices(Request $request, $id)
    {
        // Validation

        $input = [
            'currency' => $request->input('currency')
        ];

        $rules = [
            'currency' => 'nullable|exists:currencies,name'
        ];

        $validator = \Validator::make($input, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for period existence

        $period = Period::find($id);

        if (!isset($period)) {
            return ResponseHelper::responseError(
                'Period not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'period_not_found');
        }

        // Fetch all prices

        $prices = $period->prices()->get();
        if (isset($input['currency'])) {
            $collectionPrice = collect();
            foreach ($prices as $price) {
                $price->price = PriceHelper::convertPrice($price->currency, $input['currency'], $price->price);
                $price->currency = $input['currency'];
                $collectionPrice->push($price);
            }
            $prices = $collectionPrice;
        }
        return ResponseHelper::responseIndex('prices', $prices);
    }

    /**
     * Get relationships array
     *
     * @param $experienceId
     * @return array
     */
    private function getRelationships($experienceId)
    {
        return [
            "experience" => [
                "data" => [
                    "type" => "experiences",
                    "id" => (string)$experienceId
                ]
            ]
        ];
    }
}
