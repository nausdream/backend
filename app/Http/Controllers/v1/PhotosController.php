<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SetExperienceCover;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\ExperiencePhoto;
use App\Services\JsonHelper;
use App\Services\PhotoHelper;
use App\Services\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use JD\Cloudder\Facades\Cloudder;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;
use Gate;

class PhotosController extends Controller
{
    /**
     * PhotosController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['getExperiencePhotos']);
        $this->middleware('jwt')->except(['getExperiencePhotos']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->getRequestAttributesForPost($request);

        $rules = [
            'type' => 'in:photos'
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validation rules

        $rules = [
            'link' => 'string',
            'relationship_type' => 'in:users,boats,experiences'
        ];

        // Specific validation for relationship type
        $relationshipType = $attributes['relationship_type'];

        if ($relationshipType == 'boats') {
            $rules['photo_type'] = 'in:external,internal,sea';
        }

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check if link is an image

        $link = $attributes['link'];
        if (!$this->isImage($link)) {
            return ResponseHelper::responseError(
                'Link provided is not an image',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'link_not_image'
            );
        }

        // Check file size


        $size = $this->getLinkSize($link);

        if ($size > Constant::MAX_PHOTO_SIZE) {
            return ResponseHelper::responseError(
                'File too large',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'file_too_large');
        }

        // Permission

        $jwt = $request->bearerToken();
        $relationshipId = $attributes['relationship_id'];
        $account = JwtService::getAccountFromJWT($jwt);

        // User profile photo

        if ($relationshipType == 'users') {

            // Check for user existence

            $user = User::find($relationshipId);
            if (!isset($user)) {
                return ResponseHelper::responseError(
                    'User does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'user_not_found'
                );
            }

            if (Gate::forUser($account)->allows('update-profile-photo', $user)) {
                // Create public id
                $url = PhotoHelper::getProfilePhotoPublicId($user->id);
            }
        }

        // Boat photo

        if ($relationshipType == 'boats') {

            // Check for boat existence
            $boat = Boat::find($relationshipId);
            if (!isset($boat)) {
                return ResponseHelper::responseError(
                    'Boat does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'boat_not_found'
                );
            }

            // Account is a user
            if ($account instanceof User) {
                $boatVersions = $boat->boatVersions();
            }

            // Account is an administrator
            if ($account instanceof Administrator) {
                $boatVersions = $boat->boatVersions()->where('is_finished', true);
            }

            $boatVersion = $boatVersions->get()->last();

            // Check valid boatVersion existence
            if (isset($boatVersion)) {
                if (Gate::forUser($account)->allows('update-boat-photo', $boatVersion)) {
                    // Create public id
                    $url = PhotoHelper::getBoatPhotoPublicId($boatVersion->boat->user->id, $boatVersion->boat->id, $attributes['photo_type']);
                }
            }
        }

        // Experience photo

        if ($relationshipType == 'experiences') {

            // Check for experience existence
            $experience = Experience::find($relationshipId);
            if (!isset($experience)) {
                return ResponseHelper::responseError(
                    'Experience does not exist',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            $experienceVersions = $experience->experienceVersions();

            $experienceVersion = $experienceVersions->get()->last();

            // Account is a user
            if ($account instanceof User) {
                $experienceVersion = $experience->experienceVersions()->get()->last();
                if (isset($experienceVersion) && $experienceVersion->is_finished == true && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY) {
                    return ResponseHelper::responseError(
                        'You cannot update this data',
                        SymfonyResponse::HTTP_FORBIDDEN,
                        'not_authorized'
                    );
                }
            }

            // Check valid experienceVersion existence
            if (isset($experienceVersion)) {
                if (Gate::forUser($account)->allows('post-experience-photo', $experienceVersion)) {

                    // Check for photo maximum number
                    $count = $experienceVersion->experienceVersionPhotos()->count();
                    if ($count >= Constant::MAX_PHOTO_NUMBER) {
                        return ResponseHelper::responseError(
                            'Maximum photo number reached for this experience',
                            SymfonyResponse::HTTP_FORBIDDEN,
                            'max_photo_number'
                        );
                    }

                    // Check if there is already a photo with cover
                    $coverPhoto = $experienceVersion->experienceVersionPhotos()->where('is_cover', true)->get()->last();
                    DB::beginTransaction();
                    $photo = $experienceVersion->experienceVersionPhotos()->create([
                        'is_cover' => false
                    ]);
                    if (!isset($coverPhoto)) {
                        $setCoverPhotoJob = new SetExperienceCover($photo->id);
                        $is_cover = true;
                    } else {
                        $is_cover = false;
                    }

                    // Create public id
                    $url = PhotoHelper::getExperiencePhotoPublicId($experienceVersion->experience->boat->user->id,
                        $experienceVersion->experience->boat->id,
                        $experienceVersion->experience->id,
                        $photo->id
                    );
                }
            }

        }

        // Execute upload

        if (isset($url)) {
            try {
                Cloudder::upload(
                    $link,
                    $url,
                    ["format" => "jpg"]
                );
                $arrayResult = Cloudder::getResult();

                // Update database value

                if ($relationshipType == 'users') {
                    $user->photo_version = $arrayResult['version'];
                    $user->save();
                    return response(JsonHelper::createData('photos', $arrayResult['version'], ['link' => $arrayResult['secure_url']]), SymfonyResponse::HTTP_CREATED);
                }
                if ($relationshipType == 'boats') {
                    if ($attributes['photo_type'] == "external") {
                        $boatVersion->version_external_photo = $arrayResult['version'];
                    }
                    if ($attributes['photo_type'] == "internal") {
                        $boatVersion->version_internal_photo = $arrayResult['version'];
                    }
                    if ($attributes['photo_type'] == "sea") {
                        $boatVersion->version_sea_photo = $arrayResult['version'];
                    }
                    $boatVersion->save();
                    return response(JsonHelper::createData('photos', $arrayResult['version'], ['link' => $arrayResult['secure_url'], 'type' => $attributes['photo_type']]), SymfonyResponse::HTTP_CREATED);
                }
                if ($relationshipType == 'experiences') {
                    if (isset($setCoverPhotoJob)) {
                        dispatch($setCoverPhotoJob);
                    }
                    DB::commit();
                    return response(JsonHelper::createData('photos', $photo->id, ['link' => $arrayResult['secure_url'], 'is_cover' => $is_cover]), SymfonyResponse::HTTP_CREATED);
                }
            } catch (\Exception $e) {
                // Error on upload on cloudinary
                if ($relationshipType == 'experiences') {
                    DB::rollBack();
                }
                return ResponseHelper::responseError(
                    'Error uploading photo',
                    SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR,
                    'error_uploading_photo'
                );
            }
        }

        return ResponseHelper::responseError(
            'You cannot update this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $this->getRequestAttributesForPatch($request);

        // Validation id and type

        $rules = [
            'id' => 'in:' . $id,
            'type' => 'in:photos'
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validation rules

        $rules = [
            'is_cover' => 'accepted',
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check that photo exists

        $photo = ExperiencePhoto::find($id);

        if (!isset($photo)) {
            return ResponseHelper::responseError(
                'Photo not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'photo_not_found'
            );
        }

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        $experienceVersion = $photo->experienceVersion()->get()->last();

        // Check for experience version existence
        if (isset($experienceVersion)) {
            if (Gate::forUser($account)->allows('patch-experience-photo', $experienceVersion)) {
                $setCoverPhotoJob = new SetExperienceCover($id);
            }
        }

        // Dispatch job on default queue

        if (isset($setCoverPhotoJob)) {
            dispatch($setCoverPhotoJob);
            return response("", SymfonyResponse::HTTP_ACCEPTED);
        }

        return ResponseHelper::responseError(
            'You cannot update this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Check that photo exists

        $photo = ExperiencePhoto::find($id);

        if (!isset($photo)) {
            return ResponseHelper::responseError(
                'Photo not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'photo_not_found'
            );
        }

        // Permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Account is a user
        $experienceVersion = $photo->experienceVersion()->get()->last();


        // Check for experience version existence
        if (isset($experienceVersion)) {
            if (Gate::forUser($account)->allows('delete-experience-photo', $experienceVersion)) {
                $is_cover = $photo->is_cover;

                // Fetch public id for the photo

                $publicId = PhotoHelper::getExperiencePhotoPublicId($experienceVersion->experience->boat->user->id,
                    $experienceVersion->experience->boat->id,
                    $experienceVersion->experience->id,
                    $photo->id);

                // Delete cloudinary resource

                Cloudder::destroyImage($publicId);
                Cloudder::delete($publicId);

                // Delete photo from db

                $photo->delete();

                if ($is_cover) {
                    $photoToCover = $experienceVersion->experienceVersionPhotos()->get()->first();
                    if (isset($photoToCover)) {
                        // Dispatch job on default queue
                        $setCoverPhotoJob = new SetExperienceCover($photoToCover->id);
                        dispatch($setCoverPhotoJob);
                    }
                }
                return response("", SymfonyResponse::HTTP_OK);
            }
        }

        return ResponseHelper::responseError(
            'You cannot delete this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get the experience photo
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getExperiencePhotos(Request $request, $id)
    {
        // Check for experience existence

        $experience = Experience::find($id);

        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Check if there are experienceversions
        $experienceVersions = $experience->experienceVersions();
        if ($experienceVersions->get()->count() == 0) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set editing parameter
        if ($request->editing == 'true') {
            $editing = true;
        } else {
            $editing = false;
        }

        // Behave differently according to editing parameter
        if ($editing) {

            $experienceVersion = $experienceVersions->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-experience-editing', $experienceVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

        } else {
            $experienceVersion = $experienceVersions
                ->where('is_finished', true)
                ->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }

            // Check if user is authorized
            if (Gate::forUser($account)->denies('show-experience', $experienceVersion)) {
                return ResponseHelper::responseError(
                    'Not authorized to access this resource',
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'not_authorized'
                );
            }

            $experienceVersion = $experienceVersions
                ->where('is_finished', true)
                ->where('status', Constant::STATUS_ACCEPTED)
                ->get()->last();

            if (!isset($experienceVersion)) {
                return ResponseHelper::responseError(
                    'Experience not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'experience_not_found'
                );
            }
        }

        // Fetch experienceVersion photos
        $photosData = PhotoHelper::getExperiencePhotosData($experienceVersion, $request->device);

        return response(JsonHelper::createDataMessage($photosData));
    }


    /**
     * Get array of the attributes and values in request for post
     *
     * @param Request $request
     * @return array
     */
    protected function getRequestAttributesForPost($request)
    {
        $attributes = [
            'type' => $request->input('data.type'),
            'link' => $request->input('data.attributes.link'),
        ];

        $user = $request->input('data.relationships.user');
        if (isset($user)) {
            $attributes['relationship_id'] = $request->input('data.relationships.user.data.id');
            $attributes['relationship_type'] = $request->input('data.relationships.user.data.type');
        }

        $boat = $request->input('data.relationships.boat');
        if (isset($boat)) {
            $attributes['relationship_id'] = $request->input('data.relationships.boat.data.id');
            $attributes['relationship_type'] = $request->input('data.relationships.boat.data.type');
            $attributes['photo_type'] = $request->input('data.attributes.photo_type');
        }

        $experience = $request->input('data.relationships.experience');
        if (isset($experience)) {
            $attributes['relationship_id'] = $request->input('data.relationships.experience.data.id');
            $attributes['relationship_type'] = $request->input('data.relationships.experience.data.type');
        }

        return $attributes;
    }

    /**
     * Get array of the attributes and values in request for patch
     *
     * @param Request $request
     * @return array
     */
    protected function getRequestAttributesForPatch($request)
    {
        $attributes = [
            'id' => $request->input('data.id'),
            'type' => $request->input('data.type'),
            'is_cover' => $request->input('data.attributes.is_cover'),
        ];
        return $attributes;
    }

    /**
     * Check if an url is a valid image
     *
     * @param $url
     * @return boolean
     */
    protected function isImage($url)
    {
        if(@is_array(getimagesize($url))){
            return true;
        }
        return false;
    }

    /**
     * Get link size
     *
     * @param $url
     * @return int
     */
    protected function getLinkSize($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true); // make it a HEAD request
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $head = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);

        return $size;
    }
}
