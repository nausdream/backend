<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\Experience;
use App\Models\Step;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AccessoryTransformer;
use App\Services\Transformers\FixedAdditionalServiceTransformer;
use App\Services\Transformers\RulesTransformer;
use App\Services\Transformers\StepTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class StepsController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'steps';
        $this->transformer = new StepTransformer();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Parse request body
        $objectType = $request->input('data.type');
        $stepNumber = $request->input('data.attributes.step');
        $message = $request->input('data.attributes.message');
        $relationshipId = $request->input('data.relationships.experience.data.id');
        $relationshipType = $request->input('data.relationships.experience.data.type');

        // Validation for object type
        $rules = [
            'type' => 'in:steps'
        ];

        $objectType = [
            'type' => $objectType
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validation for other fields
        $rules = [
            'experience_id' => 'integer|numeric',
            'experience_type' => 'in:experiences',
            'step' => 'numeric|integer|min:1|max:4',
            'message' => 'string'
        ];

        $requestFields = [
            'experience_id' => $relationshipId,
            'experience_type' => $relationshipType,
            'step' => $stepNumber,
            'message' => $message
        ];

        $validator = \Validator::make($requestFields, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $experience = Experience::find($relationshipId);
        if (!isset($experience)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get last finished experience version
        $experienceVersion = $experience->lastFinishedExperienceVersion();
        if (!isset($experienceVersion)){
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('create-step-message', $experienceVersion)){
            return ResponseHelper::responseError(
                'Not authorized to create experience',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if there's already a step message for the same step and the same experience.
        // If there isn't any, create a new step
        $step = $experienceVersion->steps()->where('step', $stepNumber)->get()->first();
        if(!isset($step)){
            $step = new Step();
        }

        $step->step = $stepNumber;
        $step->message = $message;
        $experienceVersion->steps()->save($step);

        // Get relationships body
        $relationships = $request->input('data.relationships');
        $relationships['experience'] = $request->input('data.relationships.experience');

        return ResponseHelper::responsePost($this->type, $step, [], [], $relationships, null);
    }
}
