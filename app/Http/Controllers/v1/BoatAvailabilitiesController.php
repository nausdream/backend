<?php

namespace App\Http\Controllers\v1;
use App\Models\Administrator;
use App\Models\Boat;
use App\Models\BoatAvailability;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\AvailabilityTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class BoatAvailabilitiesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'boat-availabilities';
        $this->transformer = new AvailabilityTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');
        $relationshipType = $request->input('data.relationships.boat.data.type');
        $relationshipId = $request->input('data.relationships.boat.data.id');

        // Validate type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $type
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j',
            'relationship_type' => 'in:boats',
            'relationship_id' => 'numeric|min:1'
        ];

        $objectType = [
            'date_start' => $dateStart,
            'date_end' => $dateEnd,
            'relationship_type' => $relationshipType,
            'relationship_id' => $relationshipId
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $boat = Boat::find($relationshipId);
        // Check if boat exists
        if (!isset($boat)) {
            //boat id not found
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // Check if boatversion exists
        $boatVersions = $boat->boatVersions();

        if ($boatVersions->get()->count() <= 0) {
            //boat id not found
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        // MANUAL DATE VALIDATION
        // date_start is ahead of date_end
        $wrongOrder = $dateEnd <= $dateStart;
        if ($wrongOrder) {
            return ResponseHelper::responseError(
                'date_start can not be more recent than or equal to date_end',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_start_recent',
                'date_start'
                );
        }

        // Dates periods overlap with the one in the database
        $boatAvailabilities = $boat->boatAvailabilities();

        $dateStartOverlaps = $boatAvailabilities
                ->where('date_start', '<=', $dateStart)
                ->where('date_end', '>=', $dateStart)
                ->get()->count() > 0;

        $dateEndOverlaps = $boatAvailabilities
                ->where('date_start', '<=', $dateEnd)
                ->where('date_end', '>=', $dateEnd)
                ->get()->count() > 0;

        if ($dateStartOverlaps || $dateEndOverlaps) {
            return ResponseHelper::responseError(
                'This period overlaps with previous ones',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'periods_overlapping',
                'date_start'
            );
        }

        $availability = ['date_start' => $dateStart, 'date_end' => $dateEnd];

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set additional filters for different account types
        if ($account instanceof Administrator) {
            $boatVersions = $boatVersions
                ->where('is_finished', true);
        }
        $boatVersion = $boatVersions->get()->last();

        // Check if boat version exists
        if (!isset($boatVersion)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
                );
        }

        if (Gate::forUser($account)->denies('post-boat-availability', $boatVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
                );
        }

        // Add availability to boat
        $availability = new BoatAvailability($availability);
        $boat->boatAvailabilities()->save($availability);

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($relationshipId);

        // Set attributes to show
        $attributes = $this->transformer->transform($availability);

        return ResponseHelper::responsePost(
            $this->type,
            $availability,
            $attributes,
            [],
            $relationships,
            $availability->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $dateStart = $request->input('data.attributes.date_start');
        $dateEnd = $request->input('data.attributes.date_end');

        // Validate type & id
        $rules = [
            'type' => 'in:'.$this->type,
            'id' => 'in:'.$id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get Availability from db
        $availability = BoatAvailability::find($id);
        if (!isset($availability)){
            return ResponseHelper::responseError(
                'Availability not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'availability_not_found'
            );
        }

        // Validate other data
        $rules = [
            'date_start' => 'date_format:Y-m-j',
            'date_end' => 'date_format:Y-m-j'
        ];

        $data = [
            'date_start' => $dateStart,
            'per_person' => $dateEnd
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $boat = Boat::find($availability->boat_id);
        if (!isset($boat)){
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
                );
        }

        // MANUAL DATE VALIDATION
        // date_start is ahead of date_end
        $wrongOrder = $dateEnd <= $dateStart;
        if ($wrongOrder) {
            return ResponseHelper::responseError(
                'date_start can not be more recent than or equal to date_end',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_start_recent',
                'date_start');
        }

        // Dates periods overlap with the one in the database
        $boatAvailabilities = $boat->boatAvailabilities();

        $dateStartOverlaps = $boatAvailabilities
                ->where('date_start', '<=', $dateStart)
                ->where('date_end', '>=', $dateStart)
                ->where('id', '!=', $id)
                ->get()->count() > 0;

        $dateEndOverlaps = $boatAvailabilities
                ->where('date_start', '<=', $dateEnd)
                ->where('date_end', '>=', $dateEnd)
                ->where('id', '!=', $id)
                ->get()->count() > 0;

        if ($dateStartOverlaps || $dateEndOverlaps) {
            return ResponseHelper::responseError(
                'This period overlaps with previous ones',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'periods_overlapping',
                'date_start');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $boatVersions = $boat->boatVersions();

        // Set additional filters for different account types
        if ($account instanceof Administrator) {
            $boatVersions = $boatVersions
                ->where('is_finished', true);
        }
        $boatVersion = $boatVersions->get()->last();

        // Check if boat version exists
        if (!isset($boatVersion)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        if (Gate::forUser($account)->denies('patch-boat-availability', $boatVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $availability->date_start = $dateStart;
        $availability->date_end = $dateEnd;
        $availability->save();

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($boat->id);

        // Set attributes to show
        $attributes = $this->transformer->transform($availability);

        return ResponseHelper::responseGet(
            $this->type,
            $availability,
            $attributes,
            [],
            $relationships,
            $availability->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $availability = BoatAvailability::find($id);

        if (!isset($availability)){
            return ResponseHelper::responseError(
                'Availability not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'availability_not_found'
                );
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $boat = Boat::find($availability->boat_id);
        if (!isset($boat)){
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found'
            );
        }

        $boatVersions = $boat->boatVersions();

        // Set additional filters for different account types
        if ($account instanceof Administrator) {
            $boatVersions = $boatVersions
                ->where('is_finished', true);
        }
        $boatVersion = $boatVersions->get()->last();

        // Check if boat version exists
        if (!isset($boatVersion)) {
            return ResponseHelper::responseError(
                'Boat not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'boat_not_found');
        }

        if (Gate::forUser($account)->denies('delete-boat-availability', $boatVersion)) {
            return ResponseHelper::responseError(
                'Not authorized to edit this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
                );
        }

        BoatAvailability::destroy($id);

        return response('');
    }

    /**
     * Get relationships array
     *
     * @param $boatId
     * @return array
     */
    private function getRelationships($boatId)
    {
        return [
            "boat" => [
                "data" => [
                    "type" => "boats",
                    "id" => (string) $boatId
                ]
            ]
        ];
    }
}
