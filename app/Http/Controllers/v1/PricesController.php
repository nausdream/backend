<?php

namespace App\Http\Controllers\v1;

use App\Models\Administrator;
use App\Models\Experience;
use App\Models\Period;
use App\Models\Price;
use App\Models\User;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\PeriodTransformer;
use App\Services\Transformers\PriceTransformer;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class PricesController extends Controller
{
    protected $type;
    protected $transformer;

    /**
     * FixedRulesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
        $this->type = 'prices';
        $this->transformer = new PriceTransformer();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $person = $request->input('data.attributes.person');
        $price = $request->input('data.attributes.price');
        $relationshipType = $request->input('data.relationships.period.data.type');
        $relationshipId = $request->input('data.relationships.period.data.id');

        // Validate type
        $rules = [
            'type' => 'in:' . $this->type
        ];

        $objectType = [
            'type' => $type
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'person' => 'numeric|integer|min:1',
            'price' => 'numeric|integer|min:0',
            'relationship_type' => 'in:periods',
            'relationship_id' => 'integer|numeric|min:1'
        ];

        $objectType = [
            'person' => $person,
            'price' => $price,
            'relationship_type' => $relationshipType,
            'relationship_id' => $relationshipId
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $period = Period::find($relationshipId);
        // Check if period exists
        if (!isset($period)) {
            // Period id not found
            return ResponseHelper::responseError('Period not found', SymfonyResponse::HTTP_NOT_FOUND);
        }

        // User can't add a price if a price for the same number of people exist
        if ($period->prices()->where('person', $person)->get()->count() > 0){
            return ResponseHelper::responseError(
                'There is already a price for the same amount of people',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'already_a_price',
                'person'
            );
        }

        $experience = $period->experience()->first();

        // Check if experienceversion exists
        $experienceVersions = $experience->experienceVersions();

        if ($experienceVersions->get()->count() <= 0) {
            //experience id not found
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Set correct currency
        $currency = $experience->boat()->first()->user()->first()->currency;

        // Set additional filters for different account types
        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('post-price', $experienceVersion)) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Add price to period
        $price = new Price(['person' => $person, 'price' => $price, 'currency' => $currency]);
        $period->prices()->save($price);

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($relationshipId);

        // Set attributes to show
        $attributes = $this->transformer->transform($price);

        return ResponseHelper::responsePost(
            $this->type,
            $price,
            $attributes,
            [],
            $relationships,
            $price->id
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $type = $request->input('data.type');
        $request_id = $request->input('data.id');
        $price = $request->input('data.attributes.price');

        // Validate type & id
        $rules = [
            'type' => 'in:' . $this->type,
            'id' => 'in:' . $id
        ];

        $objectType = [
            'type' => $type,
            'id' => $request_id
        ];

        $validator = \Validator::make($objectType, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validate other data
        $rules = [
            'price' => 'integer|numeric|min:0'
        ];

        $data = [
            'price' => $price
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        $price = Price::find($id);
        // Check if price exists
        if (!isset($price)){
            return ResponseHelper::responseError('Price not found', SymfonyResponse::HTTP_NOT_FOUND, 'price_not_found');
        }

        $period = $price->period()->first();
        // Check if period exists
        if (!isset($period)){
            return ResponseHelper::responseError(
                'Period not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'period_not_found'
            );
        }

        $experience = $price->period()->first()->experience()->first();
        if (!isset($experience)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $experienceVersions = $experience->experienceVersions();

        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('patch-price', $experienceVersion)) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        // Update record in database
        $price->fill($data);
        $price->save();

        // Fetch relationships to include on data
        $relationships = $this->getRelationships($period->id);

        // Set attributes to show
        $attributes = $this->transformer->transform($price);

        return ResponseHelper::responseGet(
            $this->type,
            $price,
            $attributes,
            [],
            $relationships,
            $price->id
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $price = Price::find($id);

        if (!isset($price)) {
            return ResponseHelper::responseError('Price not found', SymfonyResponse::HTTP_NOT_FOUND, 'price_not_found');
        }

        // Get account of who's making the request
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        $period = $price->period()->first();
        // Check if period exists
        if (!isset($period)){
            return ResponseHelper::responseError(
                'Period not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'period_not_found'
            );
        }

        $experience = Experience::find($period->experience_id);
        if (!isset($experience)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        $experienceVersions = $experience->experienceVersions();

        $experienceVersion = $experienceVersions->get()->last();

        // Check if experience version exists
        if (!isset($experienceVersion)) {
            return ResponseHelper::responseError('Experience not found', SymfonyResponse::HTTP_NOT_FOUND, 'experience_not_found');
        }

        if (Gate::forUser($account)->denies('delete-price', [$experienceVersion, $price])) {
            return ResponseHelper::responseError('Not authorized to edit this resource', SymfonyResponse::HTTP_FORBIDDEN, 'not_authorized');
        }

        $price->delete();

        return response('');
    }

    /**
     * Get relationships array
     *
     * @param $periodId
     * @return array
     */
    private function getRelationships(int $periodId)
    {
        return [
            "period" => [
                "data" => [
                    "type" => "periods",
                    "id" => (string)$periodId
                ]
            ]
        ];
    }
}
