<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Gate;

class SignedUrlsController extends Controller
{

    /**
     * PhotosController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt');
        $this->middleware('csrf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new instance of signed url on s3.
     *
     * @param  \Illuminate\Http\Request $putRequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $putRequest)
    {
        // Validate type

        $attributes = [];
        $attributes['type'] = $putRequest->input('data.type');

        $rules = [
            'type' => 'in:signed-urls'
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Authorization

        $jwt = $putRequest->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('post-signed-urls')) {

            try {
                $s3 = \Storage::disk('s3');
                $client = $s3->getDriver()->getAdapter()->getClient();
                $expiry = "+10 minutes";

                $key = str_random(Constant::FILE_NAME_S3);

                $getCommand = $client->getCommand('GetObject', [
                    'Bucket' => env('BUCKET_NAME'),
                    'Key' => $key
                ]);
                $getRequest = $client->createPresignedRequest($getCommand, $expiry);

                $putCommand = $client->getCommand('PutObject', [
                    'Bucket' => env('BUCKET_NAME'),
                    'Key' => $key
                ]);
                $putRequest = $client->createPresignedRequest($putCommand, $expiry);
            } catch (\Exception $e) {
                return ResponseHelper::responseError(
                    $e,
                    SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR,
                    'error_dialing_with_s3'
                );
            }

            return response(JsonHelper::createDataMessage([
                'type' => 'signed-urls',
                'attributes' => [
                    'get_url' => (string)$getRequest->getUri(),
                    'put_url' => (string)$putRequest->getUri()
                ]
            ]), SymfonyResponse::HTTP_CREATED);
        }

        return ResponseHelper::responseError(
            'You cannot create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
