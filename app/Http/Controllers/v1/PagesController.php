<?php

namespace App\Http\Controllers\v1;

use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\PageTransformer;
use App\TranslationModels\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TranslationModels\Page;
use Gate;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Constant;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\Paginator;
use App\TranslationModels\Sentence;

class PagesController extends Controller
{
    protected $type = 'pages';
    protected $transformer;

    /**
     * PagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'getPageSentences']);
        $this->middleware('jwt');

        $this->transformer = new PageTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Verify it is a translator

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-page-list')) {

            // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
            // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
            $perPage = $request->per_page;
            $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_PAGES_PER_PAGE) : Constant::PAGES_PER_PAGE;
            $page = $request->page;

            // Get all pages

            $pages = Page::all()->sortByDesc('id');

            // Set page to fetch and if value is out of range, fetch page 1
            $currentPage = (
                isset($page) &&
                (int)$page > 0 &&
                (int)$page <= ceil($pages->count() / $perPage)) ?
                min($page, ceil($pages->count() / $perPage)) :
                1;

            $paginatedPages = new LengthAwarePaginator(
                $pages->slice($perPage * ($currentPage - 1), $perPage),
                $pages->count(),
                $perPage,
                $currentPage
            );

            $pages = $paginatedPages;

            return response(Paginator::getPaginatedData($pages, $this->transformer, $perPage, $request), SymfonyResponse::HTTP_OK);

        }

        return ResponseHelper::responseError(
            'You cannot access this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get attributes request

        $attributes = $this->getPostRequestAttributes($request);

        // Validate type

        $rules = [
            'type' => 'in:pages',
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Validate attributes

        $rules = [
            'name' => 'required|alpha_dash|string|min:2|max:' . Constant::PAGE_NAME_LENGTH,
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('post-page')) {

            // Create page

            $page = new Page(['name' => $attributes['name']]);

            try {
                $page->save();
            } catch (\Exception $e) {
                return ResponseHelper::responseError(
                    'Page with this name already exists',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'page_already_exists',
                    'name'
                );
            }

            return ResponseHelper::responsePost($this->type, $page, $this->transformer->transform($page));
        }

        return ResponseHelper::responseError(
            'You cannot create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Check for existence

        $page = Page::find($id);

        if (!isset($page)) {
            return ResponseHelper::responseError(
                'Page not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'page_not_found'
            );
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('delete-page')) {

            // Delete sentences

            $sentences = $page->sentences()->withTrashed()->get();
            foreach ($sentences as $sentence) {

                // Delete sentence translations

                $sentenceTranslations = $sentence->sentenceTranslations()->withTrashed()->get();
                foreach ($sentenceTranslations as $sentenceTranslation) {
                    $sentenceTranslation->forceDelete();
                }

                // Delete sentence

                $sentence->forceDelete();
            }

            // Delete page

            $page->forceDelete();

            return response("", SymfonyResponse::HTTP_OK);

        }

        return ResponseHelper::responseError(
            'You cannot delete this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Get sentence translation of a page with a specific language
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getPageSentences(Request $request, $id)
    {
        // Check for page existence

        $page = Page::find($id);

        if (!isset($page)) {
            return ResponseHelper::responseError(
                'Page not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'page_not_found'
            );
        }

        // Check for valid language request

        $languageName = $request->language;

        $rules = [
            'language' => 'required|exists:mysql_translation.languages,language',
        ];

        $valid = \Validator::make(['language' => $languageName], $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Fetch language object

        $language = Language::all()->where('language', $languageName)->last();

        // Check for permission

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('get-page-translation-list', $language)) {

            $data = [];

            // Get all sentences for this page

            $sentences = $page->sentences()->orderBy('id')->get();

            // Get all sentenceTranslation fot the provided page and language

            foreach ($sentences as $sentence) {

                $sentenceTranslation = $sentence->sentenceTranslations()->where('language_id', $language->id)->get()->first();

                if (isset($sentenceTranslation)) {
                    $data[] = JsonHelper::createData('sentences', $sentence->id, [
                        'name' => $sentence->name,
                        'text' => $sentenceTranslation->text,
                        'language' => $language->language
                    ]);
                }
            }

            return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_OK);
        }

        // Return sentences translation

        return ResponseHelper::responseError(
            'You cannot access this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Get array of the attributes and values in post request
     *
     * @param Request $request
     * @return array
     */
    protected function getPostRequestAttributes($request)
    {
        return [
            'type' => $request->input('data.type'),
            'name' => $request->input('data.attributes.name'),
        ];
    }
}
