<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\Boat;
use App\Models\BoatType;
use App\Models\BoatVersion;
use App\Models\Experience;
use App\Models\ExperienceType;
use App\Models\ExperienceVersion;
use App\Models\Listing;
use App\Models\Rule;
use App\Services\PriceHelper;
use App\Services\Transformers\ListingTransformer;
use Carbon\Carbon;
use App\Services\Paginator;
use App\Services\ResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class ListingsController extends Controller
{
    protected $transformer;

    /**
     * ExperiencesController constructor.
     */
    public function __construct()
    {
        $this->transformer = new ListingTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Fetch parameters
        $parameters = $this->getIndexParameters($request);

        // Validation
        $rules = [
            'lat' => 'required|numeric|between:-' . Constant::MAX_LATITUDE . ',' . Constant::MAX_LATITUDE,
            'lng' => 'required|numeric|between:-' . Constant::MAX_LONGITUDE . ',' . Constant::MAX_LONGITUDE,
            'distance' => 'required|numeric|min:0.1',
            'experience_types' => 'nullable|string',
            'entire_boat' => 'required|boolean',
            'boat_types' => 'nullable|string',
            'adults' => 'nullable|integer|min:0',
            'babies' => 'required|integer|min:0',
            'kids' => 'required|integer|min:0',
            'min_price' => 'required|integer|min:1',
            'max_price' => 'nullable|integer|min:2',
            'road_stead' => 'nullable|boolean',
            'min_length' => 'required|integer|min:1',
            'max_length' => 'nullable|integer|min:2',
            'rules' => 'nullable|string',
            'currency' => 'required|exists:currencies,name',
            'language' => 'required|exists:mysql_translation.languages,language',
            'device' => 'required|exists:devices,name'
        ];

        $validator = \Validator::make($parameters, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Validation selected experience types

        if (isset($parameters['experience_types']) && $parameters['experience_types'] != '') {
            $experience_types = explode(',', $parameters['experience_types']);
        } else {
            $experience_types = [];
        }

        foreach ($experience_types as $experience_type) {
            $arrayValue = [];

            $rules = [
                'experience_types' => 'exists:experience_types,name',
            ];

            $arrayValue['experience_types'] = $experience_type;

            $validator = \Validator::make($arrayValue, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        // Validation selected boat types

        if (isset($parameters['boat_types']) && $parameters['boat_types'] != '') {
            $boat_types = explode(',', $parameters['boat_types']);
        } else {
            $boat_types = [];
        }

        foreach ($boat_types as $boat_type) {
            $arrayValue = [];

            $rules = [
                'boat_types' => 'exists:boat_types,name',
            ];

            $arrayValue['boat_types'] = $boat_type;

            $validator = \Validator::make($arrayValue, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        // Validation selected rules

        if (isset($parameters['rules']) && $parameters['rules'] != '') {
            $rulesParameter = explode(',', $parameters['rules']);
        } else {
            $rulesParameter = [];
        }

        foreach ($rulesParameter as $rule) {
            $arrayValue = [];

            $rules = [
                'rules' => 'exists:rules,name',
            ];

            $arrayValue['rules'] = $rule;

            $validator = \Validator::make($arrayValue, $rules);

            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        // Additional validation

        if (isset($parameters['max_price']) && $parameters['min_price'] >= $parameters['max_price']) {
            return ResponseHelper::responseError('Max price must be greater then min price', SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, 'max_price_greater', 'max_price');
        }

        if (isset($parameters['max_length']) && $parameters['min_length'] >= $parameters['max_length']) {
            return ResponseHelper::responseError('Max length must be greater then min length', SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, 'max_length_greater', 'max_length');
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_BOATS_PER_PAGE. If per_page is not set, uses default BOATS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_LISTINGS_PER_PAGE) : Constant::LISTINGS_PER_PAGE;
        $page = $request->page;

        // If language is not set or empty, get account language
        // Set language to transformer
        $this->transformer->setLanguage($parameters['language']);

        // Filter boats

        $rejectedBoatVersions = BoatVersion::all()
            ->where('is_finished', true)
            ->where('status', Constant::STATUS_REJECTED)
            ->sortByDesc('id')
            ->unique('boat_id');

        $rejectedBoatIds = [];
        foreach ($rejectedBoatVersions as $rejectedBoatVersion) {
            $rejectedBoatIds[] = $rejectedBoatVersion->boat_id;
        }

        $min_length = $parameters['min_length'];
        $acceptedBoatVersions = BoatVersion::all()
            ->where('is_finished', true)
            ->where('status', Constant::STATUS_ACCEPTED)
            ->sortByDesc('id')
            ->unique('boat_id')
            ->filter(function ($value, $key) use ($rejectedBoatIds, $min_length) {
                return !in_array($value->boat_id, $rejectedBoatIds) && $value->length >= $min_length;
            });

        // Filter boat type
        $boatTypeIds = [];
        foreach ($boat_types as $boat_type) {
            $boatTypeIds[] = BoatType::all()->where('name', $boat_type)->first()->id;
        }
        if (!empty($boatTypeIds)) {
            $acceptedBoatVersions = $acceptedBoatVersions->filter(function ($boatVersion, $key) use ($boatTypeIds) {
                return in_array($boatVersion->type, $boatTypeIds);
            });
        }

        // Filter max length
        if (isset($parameters['max_length'])) {
            $acceptedBoatVersions = $acceptedBoatVersions->where('length', '<=', $parameters['max_length']);
        }

        $acceptedBoatIds = [];
        foreach ($acceptedBoatVersions as $acceptedBoatVersion) {
            $acceptedBoatIds[] = $acceptedBoatVersion->boat_id;
        }

        // Filter experience
        $experiences = Experience::all()
            ->whereIn('boat_id', $acceptedBoatIds);

        $experienceIds = [];
        foreach ($experiences as $experience) {
            $experienceIds[] = $experience->id;
        }

        // Rejected experience versions

        $rejectedExperienceVersions = ExperienceVersion::all()
            ->where('is_finished', true)
            ->where('status', Constant::STATUS_REJECTED)
            ->sortByDesc('id')
            ->unique('experience_id')
            ->whereIn('experience_id', $experienceIds);

        $rejectedExperienceIds = [];
        foreach ($rejectedExperienceVersions as $rejectedExperienceVersion) {
            $rejectedExperienceIds[] = $rejectedExperienceVersion->experience_id;
        }

        // Freezed experience versions

        $finishedExperienceVersions = ExperienceVersion::all()
            ->where('is_finished', true)
            ->sortByDesc('id')
            ->unique('experience_id')
            ->whereIn('experience_id', $experienceIds);

        $freezedExperienceIds = [];
        foreach ($finishedExperienceVersions as $finishedExperienceVersion) {
            if ($finishedExperienceVersion->status == Constant::STATUS_FREEZED) {
                $freezedExperienceIds[] = $finishedExperienceVersion->experience_id;
            }
        }

        // Set seats to filter experience for number of seats and periods (if the number of adults is set).
        if (isset($parameters['adults'])) {
            $seats = $parameters['kids'] + $parameters['babies'] + $parameters['adults'];
        }

        // Set distance and lat
        $lat = $parameters['lat'];
        $lng = $parameters['lng'];
        $distance = $parameters['distance'];

        $acceptedExperienceVersions = ExperienceVersion::all()
            ->where('is_finished', true)
            ->where('status', Constant::STATUS_ACCEPTED)
            ->sortByDesc('id')
            ->unique('experience_id')
            ->whereIn('experience_id', $experienceIds)
            ->where('kids', '>=', $parameters['kids'])
            ->where('babies', '>=', $parameters['babies']);

        if (isset($seats)) {
            $acceptedExperienceVersions = $acceptedExperienceVersions
                ->where('seats', '>=', $seats);
        }

        $acceptedExperienceVersions = $acceptedExperienceVersions
            ->filter(function ($value, $key) use ($rejectedExperienceIds, $freezedExperienceIds, $lat, $lng, $distance) {
                if (in_array($value->experience_id, $rejectedExperienceIds) || in_array($value->experience_id, $freezedExperienceIds) || !$value->is_searchable) {
                    return false;
                }
                $distance_from_point = ( Constant::KM_MULTIPLICATOR * acos( cos( deg2rad($lat) ) * cos( deg2rad( $value->departure_lat ) ) * cos( deg2rad( $value->departure_lng ) - deg2rad($lng) ) + sin( deg2rad($lat) ) * sin( deg2rad( $value->departure_lat ) ) ) );
                if ($distance_from_point <= $distance) {
                    return true;
                }
                return false;
            });

        // Experience Types
        $experienceTypeIds = [];
        foreach ($experience_types as $experience_type) {
            $experienceTypeIds[$experience_type] = ExperienceType::all()->where('name', $experience_type)->first()->id;
        }
        if (!empty($experienceTypeIds)) {
            $acceptedExperienceVersions = $acceptedExperienceVersions->filter(function ($value, $key) use ($experienceTypeIds) {
                return in_array($value->type, $experienceTypeIds);
            });
        }

        // Rules
        $ruleIds = [];
        foreach ($rulesParameter as $rule) {
            $ruleIds[] = Rule::all()->where('name', $rule)->first()->id;
        }
        if (!empty($ruleIds)) {
            $acceptedExperienceVersions = $acceptedExperienceVersions->filter(function ($value, $key) use ($ruleIds) {
                $rules = $value->rules()->whereIn('rule_id', $ruleIds);
                return count($ruleIds) == $rules->count();
            });
        }

        // Road stead or port
        $road_stead = $parameters['road_stead'];
        if (isset($road_stead)) {
            $dinners = ExperienceType::all()->where('name', 'dinners')->first()->id;
            $nights = ExperienceType::all()->where('name', 'nights')->first()->id;
            $acceptedExperienceVersions = $acceptedExperienceVersions->filter(function ($value, $key)
            use ($road_stead, $experienceTypeIds, $dinners, $nights) {
                if ($value->type == $dinners || $value->type == $nights) {
                    if ($value->road_stead == $road_stead) {
                        return true;
                    }
                }
                return false;
            });
        }

        // Set date to search price
        $tomorrow = Carbon::tomorrow()->toDateString();

        // Set min price, max price and currency
        $minPrice = $parameters['min_price'];
        $maxPrice = $parameters['max_price'];
        $currency = $parameters['currency'];

        // Set entireBoatFilter
        $entireBoat = $parameters['entire_boat'];

        $listings = collect();
        foreach ($acceptedExperienceVersions as $acceptedExperienceVersion) {
            $periods = $acceptedExperienceVersion->experience()->first()->periods()->where('date_start', null);
            if (isset($seats)) {
                $period = $periods->where('min_person', '<=', $seats)->first();
            } else {
                $period = $periods->first();
            }
            if (!isset($period)) {
                $periods = $acceptedExperienceVersion->experience()->first()->periods()->where('date_start', '>=', $tomorrow)->where('date_end', '<=', $tomorrow);
                if (isset($seats)) {
                    $period = $periods->where('min_person', '<=', $seats)->first();
                } else {
                    $period = $periods->first();
                }
            }
            if (!isset($period)) {
                $periods = $acceptedExperienceVersion->experience()->first()->periods()->where('is_default', true);
                if (isset($seats)) {
                    $period = $periods->where('min_person', '<=', $seats)->first();
                } else {
                    $period = $periods->first();
                }
            }

            if (isset($period)) {
                if ($entireBoat == true) {
                    if ($period->entire_boat == true) {
                        if (isset($seats)) {
                            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
                            if (isset($priceObject)) {
                                $price = $priceObject->price;
                                $currencyIn = $priceObject->currency;
                            } else {
                                $price = $period->price;
                                $currencyIn = $period->currency;
                            }
                            $finalSeats = $seats;
                        } else {
                            $finalSeats = $acceptedExperienceVersion->seats;
                            $price = $period->price;
                            $currencyIn = $period->currency;
                        }
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    }
                } else {
                    if ($period->entire_boat == true) {
                        if (isset($seats)) {
                            $priceObject = $period->prices()->where('person', '>=', $seats)->orderBy('person', 'asc')->first();
                            if (isset($priceObject)) {
                                $price = $priceObject->price;
                                $currencyIn = $priceObject->currency;
                            } else {
                                $price = $period->price;
                                $currencyIn = $period->currency;
                            }
                            $finalSeats = $seats;
                        } else {
                            $finalSeats = $acceptedExperienceVersion->seats;
                            $price = $period->price;
                            $currencyIn = $period->currency;
                        }
                        $price = ceil(PriceHelper::convertPrice($currencyIn, $currency, $price / $finalSeats));
                    } else {
                        $priceObject = $period->prices()->where('person', 1)->first();
                        if (isset($priceObject)) {
                            $price = ceil(PriceHelper::convertPrice($priceObject->currency, $currency, $priceObject->price));
                        }
                    }
                }
                if (isset($price) && $price >= $minPrice) {
                    if (isset($maxPrice)) {
                        if ($price <= $maxPrice) {
                            $listing = new Listing($acceptedExperienceVersion, $price, $parameters['language'], $parameters['device']);
                            $listings->push($listing);
                        }
                    } else {
                        $listing = new Listing($acceptedExperienceVersion, $price, $parameters['language'], $parameters['device']);
                        $listings->push($listing);
                    }
                }
                unset($price);
            }
        }

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($listings->count() / $perPage)) ?
            min($page, ceil($listings->count() / $perPage)) :
            1;

        $paginatedListings = new LengthAwarePaginator(
            $listings->slice($perPage * ($currentPage - 1), $perPage),
            $listings->count(),
            $perPage,
            $currentPage
        );

        $listings = $paginatedListings;
        $parameterQuery = $this->getQuery($parameters['lat'], $parameters['lng'], $parameters['distance'], $parameters['experience_types'], $parameters['entire_boat'], $parameters['boat_types'], $parameters['adults'], $parameters['babies'], $parameters['kids'], $parameters['min_price'], $parameters['max_price'], $parameters['road_stead'], $parameters['min_length'], $parameters['max_length'], $parameters['rules'], $parameters['currency'], $parameters['language'], $parameters['device']);

        return response(Paginator::getPaginatedData($listings, $this->transformer, $perPage, $request, $parameterQuery));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function getIndexParameters(Request $request)
    {
        return [
            'lat' => $request->lat,
            'lng' => $request->lng,
            'distance' => $request->distance,
            'experience_types' => $request->experience_types,
            'entire_boat' => $request->entire_boat,
            'boat_types' => $request->boat_types,
            'adults' => $request->adults,
            'babies' => $request->babies,
            'kids' => $request->kids,
            'min_price' => $request->min_price,
            'max_price' => $request->max_price,
            'road_stead' => $request->road_stead,
            'min_length' => $request->min_length,
            'max_length' => $request->max_length,
            'rules' => $request->rules,
            'currency' => $request->currency,
            'language' => $request->language,
            'device' => $request->device
        ];
    }

    /**
     * Get parameter query
     *
     * @param $lat
     * @param $lng
     * @param $distance
     * @param null $experience_types
     * @param null $entire_boat
     * @param null $boat_types
     * @param null $adults
     * @param $babies
     * @param $kids
     * @param $min_price
     * @param null $max_price
     * @param null $road_stead
     * @param $min_length
     * @param null $max_length
     * @param null $rules
     * @param $currency
     * @param $language
     * @param $device
     * @return string
     */
    protected function getQuery($lat, $lng, $distance, $experience_types = null, $entire_boat = null, $boat_types = null, $adults = null, $babies, $kids, $min_price, $max_price = null, $road_stead = null, $min_length, $max_length = null, $rules = null, $currency, $language, $device)
    {
        $query = '&lat=' . $lat . '&lng=' . $lng . '&distance=' . $distance . '&babies=' . $babies . '&kids=' . $kids . '&min_price=' . $min_price . '&min_length=' . $min_length . '&currency=' . $currency . '&language=' . $language . '&device=' . $device . '&entire_boat=' . (int) $entire_boat;

        if (isset($experience_types)) {
            $query = $query . '&experience_types=' . $experience_types;
        }

        if (isset($boat_types)) {
            $query = $query . '&boat_types=' . $boat_types;
        }

        if (isset($adults)) {
            $query = $query . '&adults=' . $adults;
        }

        if (isset($max_price)) {
            $query = $query . '&max_price=' . $max_price;
        }

        if (isset($road_stead)) {
            $query = $query . '&road_stead=' . (int) $road_stead;
        }

        if (isset($max_length)) {
            $query = $query . '&max_length=' . $max_length;
        }

        if (isset($rules)) {
            $query = $query . '&rules=' . $rules;
        }

        return $query;
    }
}
