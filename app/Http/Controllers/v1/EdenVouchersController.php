<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\EdenVoucherToSupport;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class EdenVouchersController extends Controller
{
    protected $type = 'eden-vouchers';

    /**
     * EdenVouchersController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check type

        $type = [ 'type' => $request->input('data.type') ];

        $rules = [
            'type' => 'in:eden-vouchers'
        ];

        $valid = \Validator::make($type, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get request attributes

        $attributes = $this->getPostAttributes($request);

        // Validate offsite booking attributes

        $rules = [
            'village_name' => 'required|string|max:' . Constant::ENTERPRISE_NAME_LENGTH,
            'assistant_name' => 'required|string|max:' . (Constant::FIRST_NAME_LENGTH + Constant::LAST_NAME_LENGTH),
            'village_phone' => 'required|string|max:' . Constant::PHONE_LENGTH,
            'village_mail' => 'required|email|max:' . Constant::EMAIL_LENGTH,
            'voucher_number' => 'required|integer|min:1',
            'contact_name' => 'required|string|max:' . (Constant::FIRST_NAME_LENGTH + Constant::LAST_NAME_LENGTH),
            'guest_name' => 'required|string|max:' . (Constant::FIRST_NAME_LENGTH + Constant::LAST_NAME_LENGTH),
            'room_number' => 'required|string|max:' . Constant::ROOM_LENGTH,
            'guests' => 'required|string|min:1|max:' . Constant::GUESTS_FOR_EDEN_VOUCHER_LENGTH,
            'experience_title' => 'required|string|max:' . Constant::EXPERIENCE_TITLE_LENGTH,
            'tour_number' => 'required|integer|min:1',
            'experience_date' => 'required|date_format:Y-m-d',
            'departure_port' => 'required|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'departure_time' => 'required|date_format:H:i:s',
            'arrival_time' => 'required|date_format:H:i:s',
            'guest_language' => 'required|exists:mysql_translation.languages,language',
        ];

        $validator = \Validator::make($attributes, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check that experience date is not less than today

        if (Carbon::today()->gt(Carbon::createFromFormat('Y-m-d', $attributes['experience_date']))) {
            return ResponseHelper::responseError(
                'The experience date must be greater than or equal today',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_greater_or_equal_today',
                'experience_date'
            );
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorizations

        if (Gate::forUser($account)->allows('post-eden-vouchers')) {

            // Send voucher to support

            dispatch((new SendMail(new EdenVoucherToSupport($attributes)))->onQueue(env('SQS_MAIL')));

            return response('', SymfonyResponse::HTTP_OK);

        }

        return ResponseHelper::responseError(
            'Not authorized to create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Retrieve post offsite booking fields
     *
     * @param Request $request
     * @return array
     */
    protected function getPostAttributes(Request $request)
    {
        // Fetch attributes for offsite booking and for user relationship

        $attributes = [
            'village_name' => $request->input('data.attributes.village_name'),
			'assistant_name' => $request->input('data.attributes.assistant_name'),
			'village_phone' => $request->input('data.attributes.village_phone'),
			'village_mail' => $request->input('data.attributes.village_mail'),
			'voucher_number' => $request->input('data.attributes.voucher_number'),
			'contact_name' => $request->input('data.attributes.contact_name'),
			'guest_name' => $request->input('data.attributes.guest_name'),
			'room_number' => $request->input('data.attributes.room_number'),
			'guests' => $request->input('data.attributes.guests'),
			'experience_title' => $request->input('data.attributes.experience_title'),
			'tour_number' => $request->input('data.attributes.tour_number'),
			'experience_date' => $request->input('data.attributes.experience_date'),
			'departure_port' => $request->input('data.attributes.departure_port'),
			'departure_time' => $request->input('data.attributes.departure_time'),
			'arrival_time' => $request->input('data.attributes.arrival_time'),
			'guest_language' => $request->input('data.attributes.guest_language')
        ];

        return $attributes;
    }
}
