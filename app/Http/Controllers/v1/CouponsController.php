<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Coupon;
use App\Models\Experience;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\PhotoHelper;
use App\Services\ResponseHelper;
use App\Services\Transformers\AreaTransformer;
use App\Services\Transformers\CouponsTransformer;
use App\Services\Transformers\ExperienceVersionTransformer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;


class CouponsController extends Controller
{
    protected $type = 'coupons';
    protected $transformer;

    /**
     * CouponsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'getExperienceCoupons', 'getAreaCoupons', 'getUserCoupons']);
        $this->middleware('jwt');

        $this->transformer = new CouponsTransformer();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-coupons-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_COUPONS_PER_PAGE. If per_page is not set, uses default COUPONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_COUPONS_PER_PAGE) : Constant::COUPONS_PER_PAGE;
        $page = $request->page;

        // Get coupons
        $coupons = Coupon::all()
            ->sortByDesc('created_at');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($coupons->count() / $perPage)) ?
            min($page, ceil($coupons->count() / $perPage)) :
            1;

        $paginatedCoupons = new LengthAwarePaginator(
            $coupons->slice($perPage * ($currentPage - 1), $perPage),
            $coupons->count(),
            $perPage,
            $currentPage
        );

        $coupons = $paginatedCoupons;

        return response(Paginator::getPaginatedData($coupons, $this->transformer, $perPage, $request));
    }

    /**
     * Display all coupons of an experience
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getExperienceCoupons(Request $request, $id)
    {
        // Check for experience existence
        $experience = Experience::find($id);
        if (!isset($experience)) {
            return ResponseHelper::responseError(
                'Experience not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-coupons-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Allows deleted_at attribute to be visible
        $this->transformer->setSeeDeleted();

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_COUPONS_PER_PAGE. If per_page is not set, uses default COUPONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_COUPONS_PER_PAGE) : Constant::COUPONS_PER_PAGE;
        $page = $request->page;

        // Get coupons
        $coupons = Coupon::withTrashed()->where('experience_id', $experience->id)->get()->sortByDesc('id');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($coupons->count() / $perPage)) ?
            min($page, ceil($coupons->count() / $perPage)) :
            1;

        $paginatedCoupons = new LengthAwarePaginator(
            $coupons->slice($perPage * ($currentPage - 1), $perPage),
            $coupons->count(),
            $perPage,
            $currentPage
        );

        $coupons = $paginatedCoupons;

        return response(Paginator::getPaginatedData($coupons, $this->transformer, $perPage, $request));
    }

    /**
     * Display all coupons of an area
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getAreaCoupons(Request $request, $id)
    {
        // Check for area existence
        $area = Area::find($id);
        if (!isset($area)) {
            return ResponseHelper::responseError(
                'Area not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'area_not_found'
            );
        }

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-coupons-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Allows deleted_at attribute to be visible
        $this->transformer->setSeeDeleted();

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_COUPONS_PER_PAGE. If per_page is not set, uses default COUPONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_COUPONS_PER_PAGE) : Constant::COUPONS_PER_PAGE;
        $page = $request->page;

        // Get area coupons
        $coupons = Coupon::withTrashed()->where('area_id', $area->id)->get()->sortByDesc('id');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($coupons->count() / $perPage)) ?
            min($page, ceil($coupons->count() / $perPage)) :
            1;

        $paginatedCoupons = new LengthAwarePaginator(
            $coupons->slice($perPage * ($currentPage - 1), $perPage),
            $coupons->count(),
            $perPage,
            $currentPage
        );

        $coupons = $paginatedCoupons;

        return response(Paginator::getPaginatedData($coupons, $this->transformer, $perPage, $request));
    }

    /**
     * Display all coupons of a user
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function getUserCoupons(Request $request, $id)
    {
        // Check for user existence
        $user = User::find($id);
        if (!isset($user)) {
            return ResponseHelper::responseError(
                'User not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'user_not_found'
            );
        }

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-coupons-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Allows deleted_at attribute to be visible
        $this->transformer->setSeeDeleted();

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_COUPONS_PER_PAGE. If per_page is not set, uses default COUPONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_COUPONS_PER_PAGE) : Constant::COUPONS_PER_PAGE;
        $page = $request->page;

        // Get user coupons
        $coupons = Coupon::withTrashed()->whereHas('users', function ($q) use ($user) {
            $q->where('user_id', $user->id);
        })->get()->sortByDesc('id');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($coupons->count() / $perPage)) ?
            min($page, ceil($coupons->count() / $perPage)) :
            1;

        $paginatedCoupons = new LengthAwarePaginator(
            $coupons->slice($perPage * ($currentPage - 1), $perPage),
            $coupons->count(),
            $perPage,
            $currentPage
        );

        $coupons = $paginatedCoupons;

        return response(Paginator::getPaginatedData($coupons, $this->transformer, $perPage, $request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Parse request body
        $type = $request->input('data.type');
        $name = $request->input('data.attributes.name');
        $isPercentual = $request->input('data.attributes.is_percentual');
        $amount = $request->input('data.attributes.amount');
        $expirationDate = $request->input('data.attributes.expiration_date');
        $isConsumable = $request->input('data.attributes.is_consumable');
        $currency = $request->input('data.attributes.currency');
        $experienceType = $request->input('data.relationships.experience.data.type');
        $experienceId = $request->input('data.relationships.experience.data.id');
        $areaType = $request->input('data.relationships.area.data.type');
        $areaId = $request->input('data.relationships.area.data.id');
        $users = $request->input('data.relationships.users.data');

        // Validate data
        $rules = [
            'type' => 'required|in:coupons',
        ];
        $data = [
            'type' => $type
        ];

        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $rules = [
            'name' => 'required|string|between:2,'.Constant::COUPON_NAME_LENGTH.'|unique:coupons,name',
            'is_percentual' => 'boolean',
            'amount' => 'integer|numeric|min:1',
            'expiration_date' => 'date_format:Y-m-j|nullable|after:today',
            'is_consumable' => 'boolean',
            'currency' => '',
            'experienceType' => 'sometimes|required|in:experiences',
            'areaType' => 'sometimes|required|in:areas'
        ];

        $couponData = [
            'name' => $name,
            'is_percentual' => $isPercentual,
            'amount' => $amount,
            'expiration_date' => $expirationDate,
            'is_consumable' => $isConsumable,
            'currency' => $currency
        ];
        $relationshipsData = [
            'experienceType' => $experienceType,
            'areaType' => $areaType
        ];

        $validator = \Validator::make($couponData+$relationshipsData, $rules);
        $validator->sometimes('amount', 'max:100', function ($input) use ($isPercentual) {
            return $isPercentual == true;
        });
        $validator->sometimes('currency', 'exists:currencies,name', function ($input) use ($isPercentual){
            return $isPercentual == false;
        });

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Validate experience id
        $rules = [
            'experience_id' => 'sometimes|required|numeric|integer|exists:experiences,id',
        ];
        $data = [
            'experience_id' => $experienceId
        ];
        $validator = \Validator::make($data, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError(
                'Experience with ID of '.$experienceId.' not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'experience_not_found'
            );
        }

        // Validate area id
        $rules = [
            'area_id' => 'sometimes|required|numeric|integer|exists:areas,id',
        ];
        $data = [
            'area_id' => $areaId
        ];
        $validator = \Validator::make($data, $rules);
        if (!$validator->passes()) {
            return ResponseHelper::responseError(
                'Area with ID of '.$areaId.' not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'area_not_found'
            );
        }

        // Validate users array
        foreach ($users as $user) {
            $type = key_exists('type', $user) ? $user['type'] : null;
            $id = key_exists('id', $user) ? $user['id'] : null;

            // Validate type
            $rules = [
                'type' => 'required|in:users',
            ];

            $data = [
                'type' => $type
            ];

            $validator = \Validator::make($data, $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Validate id
            $rules = [
                'id' => 'required|exists:users,id',
            ];

            $data = [
                'id' => $id
            ];
            $validator = \Validator::make($data, $rules);
            if (!$validator->passes()) {
                return ResponseHelper::responseError(
                    'User with ID of '.$id.' not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'user_not_found'
                );
            }
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Check if user is authorized
        if (Gate::forUser($account)->denies('create-coupons')) {
            return ResponseHelper::responseError(
                'Not authorized to create coupons',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Create new coupon
        $coupon = new Coupon($couponData);
        if ($isConsumable){
            $coupon->is_consumed = false;
        }

        // Set relationships on newly created coupon
        if (isset($experienceId)){
            $coupon->experience()->associate(Experience::find($experienceId));
        }
        if (isset($areaId)){
            $coupon->area()->associate(Area::find($areaId));
        }

        $coupon->save();

        foreach ($users as $user){
            $coupon->users()->attach($user['id'], ['is_coupon_consumed' => false]);
        }

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($coupon, $request->input('include'), $account->language);

        // Fetch coupon attributes from transformer
        $couponAttributes = $this->transformer->transform($coupon);

        // Define included-to-relationships mappings
        $mappings = ['experiences' => 'experience', 'areas' => 'area'];

        return ResponseHelper::responsePost(
            $this->transformer->getType(),
            $coupon,
            $couponAttributes,
            $includedResources,
            [],
            $coupon->id,
            $mappings
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-coupons-single')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        $coupon = Coupon::find($id);

        if (!isset($coupon)) {
            return ResponseHelper::responseError(
                'Coupon not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'coupon_not_found'
            );
        }

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($coupon, $request->input('include'), $account->language);

        // Fetch coupon attributes from transformer
        $couponAttributes = $this->transformer->transform($coupon);

        // Define included-to-relationships mappings
        $mappings = ['experiences' => 'experience', 'areas' => 'area'];

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $coupon,
            $couponAttributes,
            $includedResources,
            [],
            $coupon->id,
            $mappings
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Parse request body
        $requestId = $request->input('data.id');
        $type = $request->input('data.type');
        $name = $request->input('data.attributes.name');
        $isPercentual = $request->input('data.attributes.is_percentual');
        $amount = $request->input('data.attributes.amount');
        $expirationDate = $request->input('data.attributes.expiration_date');
        $isConsumable = $request->input('data.attributes.is_consumable');
        $currency = $request->input('data.attributes.currency');

        // Validate data
        $rules = [
            'type' => 'required|in:coupons',
            'id' => 'in:'.$id
        ];
        $data = [
            'type' => $type,
            'id' => $requestId
        ];
        $validator = \Validator::make($data, $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        $coupon = Coupon::find($id);
        if (!isset($coupon)){
            return ResponseHelper::responseError(
                'Coupon not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'coupon_not_found');
        }

        $rules = [
            'name' => 'required|string|between:2,'.Constant::COUPON_NAME_LENGTH.'|unique:coupons,name',
            'is_percentual' => 'boolean',
            'amount' => 'integer|numeric|min:1',
            'expiration_date' => 'date_format:Y-m-j|nullable|after:today',
            'is_consumable' => 'boolean',
            'currency' => ''
        ];

        $couponData = [
            'name' => $name,
            'is_percentual' => $isPercentual,
            'amount' => $amount,
            'expiration_date' => $expirationDate,
            'is_consumable' => $isConsumable,
            'currency' => $currency
        ];

        $validator = \Validator::make($couponData, $rules);
        $validator->sometimes('amount', 'max:100', function ($input) use ($isPercentual) {
            return $isPercentual == true;
        });
        $validator->sometimes('currency', 'exists:currencies,name', function ($input) use ($isPercentual){
            return $isPercentual == false;
        });

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Check if user is authorized
        if (Gate::forUser($account)->denies('edit-coupons')) {
            return ResponseHelper::responseError(
                'Not authorized to edit coupons',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Edit coupon
        $coupon->fill($couponData);
        $coupon->save();

        // Set language to transformer
        $this->transformer->setLanguage($account->language);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($coupon, $request->input('include'), $account->language);

        // Fetch coupon attributes from transformer
        $couponAttributes = $this->transformer->transform($coupon);

        // Define included-to-relationships mappings
        $mappings = ['experiences' => 'experience', 'areas' => 'area'];

        return ResponseHelper::responseGet(
            $this->transformer->getType(),
            $coupon,
            $couponAttributes,
            $includedResources,
            [],
            $coupon->id,
            $mappings
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $coupon = Coupon::find($id);
        if (!isset($coupon)){
            return ResponseHelper::responseError(
                'Coupon not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'coupon_not_found'
            );
        }

        // Get Jwt if present. If not or user not found, create new account object
        $jwt = $request->bearerToken();
        if (isset($jwt)) {
            $account = JwtService::getAccountFromJWT($jwt) ?? new User();
        } else {
            $account = new User();
        }

        // Check if user is authorized
        if (Gate::forUser($account)->denies('delete-coupons')) {
            return ResponseHelper::responseError(
                'Not authorized to edit coupons',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Delete pivot table records
        $coupon->users()->detach();

        // Changes name
        $i = 1;
        $oldName = $coupon->name;
        do {
            try {
                $coupon->name = $oldName . '_deleted_' . $i;
                $coupon->save();
                $itFails = false;
            } catch (\Exception $e) {
                $itFails = true;
                $i++;
            }
        } while ($itFails);

        // Delete coupon
        $coupon->delete();

        return response('', SymfonyResponse::HTTP_OK);
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $coupon
     * @param string $includesRequestString
     * @param string $language
     * @return array
     */
    protected function getIncludedResources($coupon, $includesRequestString = '', string $language = null)
    {
        $includesRequest = explode(',', $includesRequestString);
        $includes = [];

        // INCLUDED IF PRESENT
        // Area
        $area = $coupon->area()->first();
        if (isset($area)) {
            $areaTransformer = new AreaTransformer();
            $includes[] = JsonHelper::createData(
                'areas',
                $area->id,
                $areaTransformer->transform($area)
            );
        }

        // Experience
        $experience = $coupon->experience()->first();
        if (isset($experience)) {
            $experienceVersion = $experience->experienceVersions()->where('is_finished', true)->get()->last();
            if (isset($experienceVersion)){
                $experienceVersionTransformer = new ExperienceVersionTransformer();
                $experienceVersionTransformer->setLanguage($language);
                $includes[] = JsonHelper::createData(
                    'experiences',
                    $experience->id,
                    $experienceVersionTransformer->transform($experienceVersion)
                );
            }
        }

        // Users
        $users = $coupon->users()->get();
        if ($users->count() > 0) {
            foreach ($users as $user) {
                $attributes = [];
                $attributes['first_name'] = $user->first_name;
                $attributes['last_name'] = $user->last_name;
                $attributes['profile_image'] = PhotoHelper::getProfilePhotoLink($user);
                $attributes['is_coupon_consumed'] = $coupon->couponsUsers()->where('user_id', $user->id)->first()->is_coupon_consumed;
                $includes[] = JsonHelper::createData(
                    'users',
                    $user->id,
                    $attributes
                );
            }
        }

        return $includes;
    }
}
