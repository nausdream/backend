<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\BookingPaidToAdministrator;
use App\Mail\BookingPaidToCaptain;
use App\Mail\BookingPaidToGuest;
use App\Mail\PreExperienceToCaptain;
use App\Mail\PreExperienceToGuest;
use App\Models\Booking;
use App\Services\LogServiceProvider;
use Carbon\Carbon;
use Fahim\PaypalIPN\PaypalIPNListener;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;

/**
 * Class PaymentController
 * @package App\Http\Controllers\v1
 */
class PaymentController extends Controller
{
    /**
     * Check if payment for booking has been executed
     */
    public function postPayment()
    {
        $logger = LogServiceProvider::getLogService();
        try {
            $ipn = new PaypalIPNListener();
            if (env('APP_ENV') == 'production') {
                $ipn->use_sandbox = false;
            } else {
                $ipn->use_sandbox = true;
            }

            $verified = $ipn->processIpn();

            $report = $ipn->getTextReport();

            if ($verified) {
                if ($_POST['address_status'] == 'confirmed') {
                    // Get booking id from post request
                    $bookingId = $_POST['custom'];

                    // Set the booking as paid
                    $bookingController = new BookingsController();
                    $bookingController->payBookingRequest($bookingId);
                }
            } else {
                $logger->error('payment not verified for booking id: ' . $_POST['custom']);
            }
        } catch (\Exception $e) {
            $logger->error($e->getMessage() . ' for booking id: ' . $_POST['custom']);
        }
    }

    /**
     * Check if payment for offsite booking has been executed
     */
    public function postPaymentOffsite()
    {
        $logger = LogServiceProvider::getLogService();
        try {
            $ipn = new PaypalIPNListener();
            if (env('APP_ENV') == 'production') {
                $ipn->use_sandbox = false;
            } else {
                $ipn->use_sandbox = true;
            }

            $verified = $ipn->processIpn();

            $report = $ipn->getTextReport();

            if ($verified) {
                if ($_POST['address_status'] == 'confirmed') {
                    // Get booking id from post request
                    $offSiteBookingId = $_POST['custom'];

                    // Set the booking as paid
                    $offsiteBookingController = new OffsiteBookingsController();
                    $offsiteBookingController->payBookingRequest($offSiteBookingId);
                }
            } else {
                $logger->error('payment not verified for offsite booking id: ' . $_POST['custom']);
            }
        } catch (\Exception $e) {
            $logger->error($e->getMessage() . ' for offsite booking id: ' . $_POST['custom']);
        }
    }
}
