<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Http\Controllers\Controller;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\TranslationModels\Language;
use App\TranslationModels\Page;
use App\TranslationModels\Sentence;
use App\TranslationModels\SentenceTranslation;
use Illuminate\Http\Request;
use Gate;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class SentencesController extends Controller
{

    /**
     * SentencesController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf');
        $this->middleware('jwt');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get attributes request

        $attributes = $this->getPostRequestAttributes($request);

        // Validate type

        $rules = [
            'type' => 'in:sentences',
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check for id presence

        if (isset($attributes['id']) && !is_null($attributes['id'])) {

            // Check for sentence existence

            $sentence = Sentence::find($attributes['id']);

            if (!isset($sentence)) {
                return ResponseHelper::responseError(
                    'Sentence not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'sentence_not_found'
                );
            }

            // Validate request input

            $rules = [
                'text' => 'string',
                'language' => 'required|exists:mysql_translation.languages,language'
            ];

            $valid = \Validator::make($attributes, $rules);

            if (!$valid->passes()) {
                return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Fetch language object

            $language = Language::all()->where('language', $attributes['language'])->last();

            // Check for permission

            $jwt = $request->bearerToken();
            $account = JwtService::getAccountFromJWT($jwt);

            if (Gate::forUser($account)->allows('post-sentence-translation', $language)) {

                $sentenceTranslation = SentenceTranslation::all()
                    ->where('language_id', $language->id)
                    ->where('sentence_id', $sentence->id)
                    ->last();

                if (!isset($sentenceTranslation)) {
                    $sentenceTranslation = new SentenceTranslation(['text' => $attributes['text']]);
                    $sentenceTranslation->language()->associate($language);
                    $sentence->sentenceTranslations()->save($sentenceTranslation);
                } else {
                    $sentenceTranslation->text = $attributes['text'];
                    $sentenceTranslation->save();
                }

                $data = JsonHelper::createData('sentences', $sentence->id, [
                    'name' => $sentence->name,
                    'text' => $sentenceTranslation->text,
                    'language' => $language->language
                ]);

                return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_CREATED);
            }

        } else {

            // Validate request input

            $rules = [
                'text' => 'string',
                'name' => 'required|alpha_dash|string|min:2|max:' . Constant::SENTENCE_NAME_LENGTH,
                'relationship_type' => 'in:pages'
            ];

            $valid = \Validator::make($attributes, $rules);

            if (!$valid->passes()) {
                return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Check for permission

            $jwt = $request->bearerToken();
            $account = JwtService::getAccountFromJWT($jwt);

            if (Gate::forUser($account)->allows('post-sentence')) {

                // Check for page existence

                $page = Page::find($attributes['page']);

                if (!isset($page)) {
                    return ResponseHelper::responseError(
                        'Page does not exist',
                        SymfonyResponse::HTTP_NOT_FOUND,
                        'page_not_found'
                    );
                }

                // Check for no sentence duplication

                $sentenceForName = Sentence::all()->where('name', $attributes['name'])->where('page_id', $page->id)->last();

                if (isset($sentenceForName)) {
                    return ResponseHelper::responseError(
                        'Sentence with this name already exists',
                        SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                        'sentence_already_exists'
                    );
                }

                // Create new sentence

                $sentence = new Sentence(['name' => $attributes['name']]);
                $page->sentences()->save($sentence);

                // Create sentence translation

                $sentenceTranslation = new SentenceTranslation(['text' => $attributes['text']]);
                $language = Language::all()->where('language', \Lang::getFallback())->first();
                $sentenceTranslation->language()->associate($language);
                $sentence->sentenceTranslations()->save($sentenceTranslation);

                // Return response

                $data = JsonHelper::createData('sentences', $sentence->id, [
                    'name' => $sentence->name,
                    'text' => $sentenceTranslation->text,
                    'language' => $language->language
                ]);

                return response(JsonHelper::createDataMessage($data), SymfonyResponse::HTTP_CREATED);
            }
        }

        return ResponseHelper::responseError(
            'You cannot access this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Check for existence

        $sentence = Sentence::find($id);

        if (!isset($sentence)) {
            return ResponseHelper::responseError(
                'Sentence not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'sentence_not_found');
        }

        // Check for authorization

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->allows('delete-sentence')) {

            // Delete sentence translations

            $sentenceTranslations = $sentence->sentenceTranslations()->get();
            foreach ($sentenceTranslations as $sentenceTranslation) {
                $sentenceTranslation->delete();
            }

            // Delete sentence

            $sentence->delete();

            return response("", SymfonyResponse::HTTP_OK);

        }

        return ResponseHelper::responseError(
            'You cannot delete this data',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }


    /**
     * Get array of the attributes and values in post request
     *
     * @param Request $request
     * @return array
     */
    protected function getPostRequestAttributes($request)
    {
        return [
            'type' => $request->input('data.type'),
            'id' => $request->input('data.id'),
            'text' => $request->input('data.attributes.text'),
            'language' => $request->input('data.attributes.language'),
            'name' => $request->input('data.attributes.name'),
            'page' => $request->input('data.relationships.page.data.id'),
            'relationship_type' => $request->input('data.relationships.page.data.type')
        ];
    }
}
