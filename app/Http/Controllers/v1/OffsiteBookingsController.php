<?php

namespace App\Http\Controllers\v1;

use App\Constant;
use App\Jobs\SendMail;
use App\Mail\OffsiteBookingPaidToAdministrator;
use App\Mail\OffsiteBookingToPay;
use App\Mail\PreExperienceOffsiteToCaptain;
use App\Mail\PreExperienceOffsiteToGuest;
use App\Mail\VoucherToSupport;
use App\Models\OffsiteBooking;
use App\Models\User;
use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\Paginator;
use App\Services\ResponseHelper;
use App\Services\Transformers\OffsiteBookingListTransformer;
use App\Services\Transformers\OffsiteBookingsTransformer;
use App\Services\Transformers\OffsiteBookingTransformer;
use App\Services\Transformers\UserTransformer;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class OffsiteBookingsController extends Controller
{
    protected $type = 'offsite-bookings';

    /**
     * OffsiteBookingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('csrf')->except(['index', 'show', 'payBookingRequest']);
        $this->middleware('jwt')->except(['payBookingRequest']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorizations

        if (Gate::forUser($account)->denies('get-offsite-bookings-list')) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Check if per_page is set and is int (casting string to int gives 0), then take the smallest
        // between per_page and MAX_CONVERSATIONS_PER_PAGE. If per_page is not set, uses default CONVERSATIONS_PER_PAGE
        $perPage = $request->per_page;
        $perPage = (isset($perPage) && (int)$perPage > 0) ? min($perPage, Constant::MAX_BOOKINGS_PER_PAGE) : Constant::BOOKINGS_PER_PAGE;
        $page = $request->page;

        // Get conversations sorted by updated_at first, and then by id
        $offsiteBookings = OffsiteBooking::all()
            ->sortByDesc('id')
            ->sortByDesc('created_at');

        // Set page to fetch and if value is out of range, fetch page 1
        $currentPage = (
            isset($page) &&
            (int)$page > 0 &&
            (int)$page <= ceil($offsiteBookings->count() / $perPage)) ?
            min($page, ceil($offsiteBookings->count() / $perPage)) :
            1;

        $paginatedOffsiteBookings = new LengthAwarePaginator(
            $offsiteBookings->slice($perPage * ($currentPage - 1), $perPage),
            $offsiteBookings->count(),
            $perPage,
            $currentPage
        );

        $offsiteBookings = $paginatedOffsiteBookings;

        $transformer = new OffsiteBookingListTransformer();

        return response(Paginator::getPaginatedData($offsiteBookings, $transformer, $perPage, $request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check type

        $type = [ 'type' => $request->input('data.type') ];

        $rules = [
            'type' => 'in:offsite-bookings'
        ];

        $valid = \Validator::make($type, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Get request attributes

        $attributes = $this->getPostAttributes($request);

        // Validate offsite booking attributes

        $rules = [
            'first_name_guest' => 'required|string|max:' . Constant::FIRST_NAME_LENGTH,
            'last_name_guest' => 'nullable|string|max:' . Constant::LAST_NAME_LENGTH,
            'address_guest' => 'string|nullable|max:' . Constant::ENTERPRISE_ADDRESS_LENGTH,
            'city_guest' => 'string|nullable|max:' . Constant::ENTERPRISE_CITY_LENGTH,
            'phone_guest' => 'required|string|max:' . Constant::PHONE_LENGTH,
            'email_guest' => 'required|email|max:' . Constant::EMAIL_LENGTH,
            'vat_guest' => 'string|nullable|max:' . Constant::ENTERPRISE_VAT_LENGTH,
            'experience_date' => 'nullable|date_format:Y-m-d',
            'arrival_date' => 'nullable|date_format:Y-m-d',
            'experience_title' => 'required|string|max:' . Constant::EXPERIENCE_TITLE_LENGTH,
            'experience_description' => 'required|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'boat_type' => 'exists:boat_types,name|required',
            'departure_port' => 'required|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'arrival_port' => 'required|string|max:' . Constant::PORT_DEPARTURE_LENGTH,
            'departure_time' => 'required|date_format:H:i:s',
            'arrival_time' => 'required|date_format:H:i:s',
            'adults' => 'required|integer|min:0',
            'kids' => 'required|integer|min:0',
            'babies' => 'required|integer|min:0',
            'included_services' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'excluded_services' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'drinks' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'dishes' => 'nullable|string|max:' . Constant::EXPERIENCE_DESCRIPTION_LENGTH,
            'expiration_date' => 'required|date_format:Y-m-d',
            'language_guest' => 'required|exists:mysql_translation.languages,language',
            'currency_guest' => 'required|exists:currencies,name',
            'language_captain' => 'required|exists:mysql_translation.languages,language',
            'currency_captain' => 'required|exists:currencies,name',
            'first_name_captain' => 'required|string|max:' . Constant::FIRST_NAME_LENGTH,
            'last_name_captain' => 'required|string|max:' . Constant::LAST_NAME_LENGTH,
            'email_captain' => 'required|email|max:' . Constant::EMAIL_LENGTH,
            'phone_captain' => 'required|string|max:' . Constant::PHONE_LENGTH,
            'price' => 'required|integer|min:10',
            'fee' => 'required|integer|min:1|max:' . $attributes['offsite_booking']['price'],
        ];

        $validator = \Validator::make($request->input('data.attributes'), $rules);

        if (!$validator->passes()) {
            return ResponseHelper::responseError($validator->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check that experience date is not less than today

        if ((isset($attributes['offsite_booking']['experience_date']) && $attributes['offsite_booking']['experience_date'] != '') && Carbon::today()->gt(Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['experience_date']))) {
            return ResponseHelper::responseError(
                'The experience date must be greater than or equal today',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_greater_or_equal_today',
                'experience_date'
            );
        }

        // Check if number of total seats is equal 0

        $totalSeats = $attributes['offsite_booking']['adults'] +
            $attributes['offsite_booking']['kids'] +
            $attributes['offsite_booking']['babies'];

        if ($totalSeats <= 0) {
            return ResponseHelper::responseError(
                'The number of total seats has to be bigger than 0',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'min',
                'adults'
            );
        }

        // Check that expiration date is not greather than experience date if experience date is set

        if ((isset($attributes['offsite_booking']['experience_date']) && $attributes['offsite_booking']['experience_date'] != '') && Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['expiration_date'])->gt(Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['experience_date']))) {
            return ResponseHelper::responseError(
                'The expiration date must be lower than or equal to experience date',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'expiration_date_greater_than_experience_date',
                'expiration_date'
            );
        }

        // Check that expiration date is not greather or equale than today if experience date is not set

        if ((!isset($attributes['offsite_booking']['experience_date']) || $attributes['offsite_booking']['experience_date'] == '') && Carbon::today()->gt(Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['expiration_date']))) {
            return ResponseHelper::responseError(
                'The expiration date must be lower than or equal to today',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'date_greater_or_equal_today',
                'expiration_date'
            );
        }

        // Check that arrival date is set if departure_date is

        if ((isset($attributes['offsite_booking']['experience_date']) && $attributes['offsite_booking']['experience_date'] != '') && !isset($attributes['offsite_booking']['arrival_date'])) {
            return ResponseHelper::responseError(
                'The field arrival_date must be set',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'required',
                'arrival_date'
            );
        }

        // Check that arrival date is greater or equal to departure_date

        if ((isset($attributes['offsite_booking']['experience_date']) && $attributes['offsite_booking']['experience_date'] != '') && Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['experience_date'])->gt(Carbon::createFromFormat('Y-m-d', $attributes['offsite_booking']['arrival_date']))) {
            return ResponseHelper::responseError(
                'The arrival date has to be greater or equal to departure date',
                SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                'arrival_date_greater_or_equal_to_departure_date',
                'arrival_date'
            );
        }


        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorizations

        if (Gate::forUser($account)->allows('post-offsite-bookings')) {

            // Check if mail and phone already exist users table

            $guestEmail = User::where('email', $attributes['guest']['email'])
                ->where('is_mail_activated', 1)
                ->first();

            $guestPhone = User::where('phone', $attributes['guest']['phone'])
                ->where('is_phone_activated', 1)
                ->first();


            // Mail and phone exist in users table but they are different users

            if (isset($guestEmail) && isset($guestPhone) && $guestEmail->id != $guestPhone->id) {
                return ResponseHelper::responseError(
                    "Email and phone are of a different user",
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'email_and_phone_of_different_user',
                    'email'
                );
            }

            // Mail and phone don't exist in users table

            if (!isset($guestPhone) && !isset($guestEmail)) {
                $guest = factory(User::class)->make();
                $guest->email = $attributes['guest']['email'];
                $guest->phone = $attributes['guest']['phone'];
            }

            // Mail exists in users table

            if (isset($guestEmail) && !isset($guest)) {
                $guest = $guestEmail;
                $guest->phone = $attributes['guest']['phone'];
            }

            // Phone exists in users table

            if (isset($guestPhone) && !isset($guest)) {
                $guest = $guestPhone;
                $guest->email = $attributes['guest']['email'];
            }

            // Set user attributes

            if (!$guest->first_name) {
                $guest->first_name = $attributes['guest']['first_name'];
            }
            if (!$guest->last_name) {
                $guest->last_name = $attributes['guest']['last_name'];
            }
            if (!$guest->enterprise_address) {
                $guest->enterprise_address = $attributes['guest']['enterprise_address'];
            }
            if (!$guest->enterprise_city) {
                $guest->enterprise_city = $attributes['guest']['enterprise_city'];
            }
            if (!$guest->vat_number) {
                $guest->vat_number = $attributes['guest']['vat_number'];
            }
            if (!$guest->language) {
                $guest->language = $attributes['guest']['language'];
            }
            if (!$guest->currency) {
                $guest->currency = $attributes['guest']['currency'];
            }

            $guest->save();

            // Set null departure and arrival date if they are not set

            if (!isset($attributes['offsite_booking']['experience_date']) || $attributes['offsite_booking']['experience_date'] == '') {
                $attributes['offsite_booking']['experience_date'] = null;
                $attributes['offsite_booking']['arrival_date'] = null;
            }

            // Create new offsite booking

            $offsiteBooking = new OffsiteBooking();
            $offsiteBooking->fill($attributes['offsite_booking']);
            $offsiteBooking->status = Constant::OFFSITE_BOOKING_STATUS_PENDING;

            $offsiteBooking->user_id = $guest->id;

            $offsiteBooking->save();

            // Send payment mail

            dispatch((new SendMail(new OffsiteBookingToPay($offsiteBooking)))->onQueue(env('SQS_MAIL')));

            // Get single resource transformer
            $transformer = new OffsiteBookingsTransformer();

            // Get attributes
            $offsiteBookingAttributes = $transformer->transform($offsiteBooking);

            // Parse included in request string and fetch data
            $includedResources = $this->getIncludedResources($offsiteBooking);

            // Define included-to-relationships mappings
            $mappings = ['users' => 'guest'];

            return ResponseHelper::responsePost(
                $this->type,
                $offsiteBooking,
                $offsiteBookingAttributes,
                $includedResources,
                [],
                $offsiteBooking->id,
                $mappings
            );

        }

        return ResponseHelper::responseError(
            'Not authorized to create this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // Check for offsite booking existence

        $offsiteBooking = OffsiteBooking::find($id);

        if (!isset($offsiteBooking)) {
            return ResponseHelper::responseError(
                'Offsite booking not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'offsite_booking_not_found'
            );
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        if (Gate::forUser($account)->denies('get-offsite-bookings-single', $offsiteBooking)) {
            return ResponseHelper::responseError(
                'Not authorized to access this resource',
                SymfonyResponse::HTTP_FORBIDDEN,
                'not_authorized'
            );
        }

        // Get single resource transformer
        $transformer = new OffsiteBookingsTransformer();

        // Get attributes
        $offsiteBookingAttributes = $transformer->transform($offsiteBooking);

        // Parse included in request string and fetch data
        $includedResources = $this->getIncludedResources($offsiteBooking);

        // Define included-to-relationships mappings
        $mappings = ['users' => 'guest'];

        return ResponseHelper::responseGet(
            $this->type,
            $offsiteBooking,
            $offsiteBookingAttributes,
            $includedResources,
            [],
            $offsiteBooking->id,
            $mappings
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Fetch data from request

        $idAndTypeRequest = $this->getIdAndTypeRequest($request);
        $attributes = $this->getPatchAttributes($request);

        // Check type

        $type = [ 'type' => $request->input('data.type') ];

        $rules = [
            'id' => 'in:' . $id,
            'type' => 'in:offsite-bookings'
        ];

        $valid = \Validator::make($idAndTypeRequest, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_CONFLICT);
        }

        // Check attributes

        $rules = [
            'status' => 'required|in:' . Constant::OFFSITE_BOOKING_STATUS_NAMES[Constant::OFFSITE_BOOKING_STATUS_PAID]
        ];

        $valid = \Validator::make($attributes, $rules);

        if (!$valid->passes()) {
            return ResponseHelper::responseError($valid->errors(), SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check for existence

        $offsiteBooking = OffsiteBooking::find($id);

        if (!isset($offsiteBooking)) {
            return ResponseHelper::responseError(
                'Offsite booking not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'offsite_booking_not_found'
            );
        }

        // Get account

        $jwt = $request->bearerToken();
        $account = JwtService::getAccountFromJWT($jwt);

        // Check for authorization

        if (Gate::forUser($account)->allows('patch-offsite-bookings')) {

            // Check offsite bookins status

            if ($offsiteBooking->status != Constant::OFFSITE_BOOKING_STATUS_PENDING) {
                return ResponseHelper::responseError(
                    'Offsite booking has to be pending to change to paid status',
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'offsite_booking_not_pending'
                );
            }

            // Set booking request as paid

            $this->payBookingRequest($id);

            // Get updated offsite booking

            $offsiteBooking = OffsiteBooking::find($id);


            // Get single resource transformer
            $transformer = new OffsiteBookingsTransformer();

            // Get attributes
            $offsiteBookingAttributes = $transformer->transform($offsiteBooking);

            // Parse included in request string and fetch data
            $includedResources = $this->getIncludedResources($offsiteBooking);

            // Define included-to-relationships mappings
            $mappings = ['users' => 'guest'];

            return ResponseHelper::responseGet(
                $this->type,
                $offsiteBooking,
                $offsiteBookingAttributes,
                $includedResources,
                [],
                $offsiteBooking->id,
                $mappings
            );
        }

        return ResponseHelper::responseError(
            'Not authorized to update this resource',
            SymfonyResponse::HTTP_FORBIDDEN,
            'not_authorized'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Set as paid the offsite booking and send emails confirmation
     *
     * @param $id
     */

    public function payBookingRequest($id)
    {
        // Check for booking existence
        $offsiteBooking = OffsiteBooking::find($id);
        if (isset($offsiteBooking)) {

            // Check that offsiteBooking is in accepted status
            if ($offsiteBooking->status == Constant::OFFSITE_BOOKING_STATUS_PENDING) {

                // Set new status
                $offsiteBooking->status = Constant::OFFSITE_BOOKING_STATUS_PAID;
                $offsiteBooking->save();

                // Send emails
                dispatch((new SendMail(new PreExperienceOffsiteToGuest($offsiteBooking)))->onQueue(env('SQS_MAIL')));
                dispatch((new SendMail(new PreExperienceOffsiteToCaptain($offsiteBooking)))->onQueue(env('SQS_MAIL')));
                dispatch((new SendMail(new OffsiteBookingPaidToAdministrator($offsiteBooking)))->onQueue(env('SQS_MAIL')));
            }
        }
    }

    /**
     * Get the arrays of the included elements for the specified user
     *
     * @param $offsiteBooking
     * @return array
     */
    protected function getIncludedResources($offsiteBooking)
    {
        $includes = [];

        // User
        $user = $offsiteBooking->user()->first();
        $userTransformer = new UserTransformer();
        $includes[] = JsonHelper::createData(
            $userTransformer->getType(),
            $user->id,
            $userTransformer->transform($user)
        );

        return $includes;
    }

    /**
     * Retrieve post offsite booking fields
     *
     * @param Request $request
     * @return array
     */
    protected function getPostAttributes(Request $request)
    {
        // Fetch attributes for offsite booking and for user relationship

        $attributes = [
            'offsite_booking' => [
                'experience_date' => $request->input('data.attributes.experience_date'),
                'arrival_date' => $request->input('data.attributes.arrival_date'),
                'experience_title' => $request->input('data.attributes.experience_title'),
                'experience_description' => $request->input('data.attributes.experience_description'),
                'boat_type' => $request->input('data.attributes.boat_type'),
                'departure_port' => $request->input('data.attributes.departure_port'),
                'arrival_port' => $request->input('data.attributes.arrival_port'),
                'departure_time' => $request->input('data.attributes.departure_time'),
                'arrival_time' => $request->input('data.attributes.arrival_time'),
                'adults' => $request->input('data.attributes.adults'),
                'kids' => $request->input('data.attributes.kids'),
                'babies' => $request->input('data.attributes.babies'),
                'included_services' => $request->input('data.attributes.included_services'),
                'excluded_services' => $request->input('data.attributes.excluded_services'),
                'drinks' => $request->input('data.attributes.drinks'),
                'dishes' => $request->input('data.attributes.dishes'),
                'price' => $request->input('data.attributes.price'),
                'fee' => $request->input('data.attributes.fee'),
                'expiration_date' => $request->input('data.attributes.expiration_date'),
                'language' => $request->input('data.attributes.language_captain'),
                'currency' => $request->input('data.attributes.currency_captain'),
                'first_name_captain' => $request->input('data.attributes.first_name_captain'),
                'last_name_captain' => $request->input('data.attributes.last_name_captain'),
                'email_captain' => $request->input('data.attributes.email_captain'),
                'phone_captain' => $request->input('data.attributes.phone_captain'),
            ],
            'guest' => [
                'first_name' => $request->input('data.attributes.first_name_guest'),
                'last_name' => $request->input('data.attributes.last_name_guest'),
                'enterprise_address' => $request->input('data.attributes.address_guest'),
                'enterprise_city' => $request->input('data.attributes.city_guest'),
                'phone' => $request->input('data.attributes.phone_guest'),
                'email' => $request->input('data.attributes.email_guest'),
                'vat_number' => $request->input('data.attributes.vat_guest'),
                'language' => $request->input('data.attributes.language_guest'),
                'currency' => $request->input('data.attributes.currency_guest'),
            ]
        ];

        return $attributes;
    }

    protected function getPatchAttributes($request)
    {
        return [
            'status' => $request->input('data.attributes.status'),
        ];
    }

    /**
     * Get array of the attributes and values in request
     *
     * @param Request $request
     * @return array
     */
    protected function getIdAndTypeRequest($request)
    {
        return [
            'type' => $request->input('data.type'),
            'id' => $request->input('data.id'),
        ];
    }

}
