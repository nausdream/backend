<?php

namespace App\Http\Controllers\v1;

use App\Services\JsonHelper;
use App\Services\JwtService;
use App\Services\ResponseHelper;
use App\Services\Transformers\PageTransformer;
use App\TranslationModels\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TranslationModels\Page;
use Gate;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Constant;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Services\Paginator;
use App\TranslationModels\Sentence;

class TranslationsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

    /**
     * Get sentence translation of a page with a specific language
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPageTranslation(Request $request)
    {
        // Fetch language object
        $languageName = $request->language;

        $language = Language::all()->where('language', $languageName)->last();
        $fallabackLanguage = Language::all()->where('language', \Lang::getFallback())->last();

        // Check for language existence
        if (!isset($language)) {
            return ResponseHelper::responseError(
                'Language not found',
                SymfonyResponse::HTTP_NOT_FOUND,
                'language_not_found'
            );
        }

        // Check for pages existence

        $pages = $request->page;

        if (isset($pages) && $pages != '') {
            $pages = explode(',', $pages);
        } else {
            $pages = [];
        }

        $data = [];

        foreach ($pages as $pageName) {

            $page = Page::all()->where('name', $pageName)->first();

            if (!isset($page)) {
                return ResponseHelper::responseError(
                    'Page not found',
                    SymfonyResponse::HTTP_NOT_FOUND,
                    'page_not_found',
                    'page'
                );
            }

            $data[$page->name] = [];

            // Get all sentences for this page

            $sentences = $page->sentences()->orderBy('id')->get();

            // Get all sentenceTranslation fot the provided page and language

            foreach ($sentences as $sentence) {

                // Get sentence translation for request language, if not present set fallback language translation
                $sentenceTranslation = $sentence->sentenceTranslations()->where('language_id', $language->id)->get()->first();

                if (isset($sentenceTranslation)) {
                    $data[$page->name][$sentence->name] = $sentenceTranslation->text;
                } else {
                    $data[$page->name][$sentence->name] = $sentence->sentenceTranslations()
                        ->where('language_id', $fallabackLanguage->id)->get()->first()->text;
                }
            }

        }

        return response($data, SymfonyResponse::HTTP_OK);

    }

    /**
     * Get email translations
     *
     * @param string $pageName
     * @param string $languageName
     * @return array
     */

    public static function getTranslations(string $pageName, string $languageName)
    {
        // Fetch page and language objects

        $page = Page::all()->where('name', $pageName)->first();
        $language = Language::all()->where('language', $languageName)->last();
        $fallbackLanguage = Language::all()->where('language', \Lang::getFallback())->last();

        $data = [];

        // Get all sentences for this page

        if (isset($page)) {

            $sentences = $page->sentences()->orderBy('id')->get();

            // Get all sentenceTranslation fot the provided page and language

            foreach ($sentences as $sentence) {

                $sentenceTranslation = $sentence->sentenceTranslations()->where('language_id', $language->id)->get()->first();

                if (!isset($sentenceTranslation)) {
                    $sentenceTranslation = $sentence->sentenceTranslations()->where('language_id', $fallbackLanguage->id)->get()->first();
                }

                if (isset($sentenceTranslation->text)) {
                    $text = $sentenceTranslation->text;
                } else {
                    $text = '';
                }

                $data[$sentence->name] = $text;
            }

        }

        return $data;
    }
}
