<?php

namespace App\Events;

use App\Models\ExperienceVersion;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExperienceVersionPutted
{
    use InteractsWithSockets, SerializesModels;

    public $experienceVersion;

    /**
     * Create a new event instance.
     *
     * @param ExperienceVersion $experienceVersion
     */
    public function __construct(ExperienceVersion $experienceVersion)
    {
        $this->experienceVersion = $experienceVersion;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
