<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OffsiteBooking extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'offsite_bookings';

    protected $hidden = [
        'id',
        'language',
        'currency',
        'user_id',
        'updated_at',
        'deleted_at'
    ];

    protected $guarded = [
        'status',
        'updated_at',
        'created_at',
        'deleted_at',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * Set the boat type.
     *
     * @param  string  $value
     * @return void
     */
    public function setBoatTypeAttribute($value)
    {
        $this->attributes['boat_type'] = BoatType::all()->where('name', $value)->last()->id ?? null;
    }
}
