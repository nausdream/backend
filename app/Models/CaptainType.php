<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class CaptainType extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'captain_types';
    protected $fillable = ['name'];

}
