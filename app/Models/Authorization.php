<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Authorization extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $fillable = ['name'];
    protected $table = 'authorizations';


    public function administrators()
    {
        return $this->belongsToMany(\App\Models\Administrator::class, 'administrators_authorizations', 'authorization_id', 'administrator_id');
    }

    public function administratorsAuthorizations()
    {
        return $this->hasMany(\App\Models\AdministratorsAuthorization::class, 'authorization_id', 'id');
    }


}
