<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class AccessoriesBoatVersion extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'accessories_boat_versions';

    public function boatVersions()
    {
        return $this->belongsTo(\App\Models\BoatVersion::class, 'boat_version_id', 'id');
    }

    public function accessories()
    {
        return $this->belongsTo(\App\Models\Accessory::class, 'accessory_id', 'id');
    }

}
