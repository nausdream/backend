<?php namespace App\Models;


use App\Constant;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experiences';


    public function boat()
    {
        return $this->belongsTo(\App\Models\Boat::class, 'boat_id', 'id');
    }

    public function conversations()
    {
        return $this->hasMany(\App\Models\Conversation::class, 'experience_id', 'id');
    }

    public function coupons()
    {
        return $this->hasMany(\App\Models\Coupon::class, 'experience_id', 'id');
    }

    public function experienceAvailabilities()
    {
        return $this->hasMany(\App\Models\ExperienceAvailability::class, 'experience_id', 'id');
    }

    public function experienceFeedbacks()
    {
        return $this->hasMany(\App\Models\ExperienceFeedback::class, 'experience_id', 'id');
    }

    public function experienceVersions()
    {
        return $this->hasMany(\App\Models\ExperienceVersion::class, 'experience_id', 'id');
    }

    public function lastExperienceVersion()
    {
        return $this->hasMany(\App\Models\ExperienceVersion::class, 'experience_id', 'id')->get()->last();
    }

    public function lastFinishedExperienceVersion()
    {
        return $this->hasMany(\App\Models\ExperienceVersion::class, 'experience_id', 'id')->where('is_finished', true)->get()->last();
    }

    public function lastFinishedAndAcceptedExperienceVersion()
    {
        return $this->hasMany(\App\Models\ExperienceVersion::class, 'experience_id', 'id')->where('is_finished', true)->where('status', Constant::STATUS_ACCEPTED)->get()->last();
    }

    public function periods()
    {
        return $this->hasMany(\App\Models\Period::class, 'experience_id', 'id');
    }

}
