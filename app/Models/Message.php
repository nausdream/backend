<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'messages';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['conversation'];
    protected $visible = ['message', 'read'];
    protected $casts = ['read' => 'boolean'];
    protected $fillable = ['message'];


    public function conversation()
    {
        return $this->belongsTo(\App\Models\Conversation::class, 'conversation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
