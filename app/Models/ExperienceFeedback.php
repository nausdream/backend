<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceFeedback extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_feedbacks';
    protected $fillable = ['score'];
    protected $visible = ['score', 'user_id'];


    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'experience_feedback_id', 'id');
    }


}
