<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 18:24
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class FixedAdditionalService extends AbstractModel {
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'fixed_additional_services';
    protected $visible = ['name'];
    protected $fillable = ['name'];

    public function experienceVersions()
    {
        return $this->belongsToMany(\App\Models\ExperienceVersion::class, 'experience_versions_fixed_additional_services', 'fixed_additional_service_id', 'experience_version_id');
    }

    public function experienceVersionsFixedAdditionalServices()
    {
        return $this->hasMany(\App\Models\ExperienceVersionsFixedAdditionalServices::class, 'fixed_additional_service_id', 'id');
    }

    public function experienceTypes()
    {
        return $this->belongsToMany(\App\Models\ExperienceType::class, 'experience_types_fixed_additional_services', 'fixed_additional_service_id', 'experience_type_id');
    }

    public function eexperienceTypesFixedAdditionalServices()
    {
        return $this->hasMany(\App\Models\ExperienceTypesFixedAdditionalServices::class, 'fixed_additional_service_id', 'id');
    }

    public function bookings()
    {
        return $this->belongsToMany(\App\Models\Booking::class, 'bookings_fixed_additional_services', 'fixed_additional_service_id', 'booking_id');
    }
}