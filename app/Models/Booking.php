<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'bookings';
    protected $visible = [
        'experience_date',
        'request_date',
        'acceptance_date',
        'status',
        'price',
        'adults',
        'kids',
        'babies',
        'currency',
        'coupon',
        'entire_boat',
        'fee',
        'discount',
        'price_on_board',
        'price_to_pay',
    ];

    protected $fillable = [
        'experience_date',
        'request_date',
        'status',
        'price',
        'adults',
        'kids',
        'babies',
        'currency',
        'coupon',
        'entire_boat',
        'fee',
        'discount',
        'price_on_board',
        'price_to_pay',
    ];

    protected $casts = [
        'entire_boat' => 'boolean',
        'is_pre_experience_mail_sent' => 'boolean'
    ];


    public function boatFeedback()
    {
        return $this->belongsTo(\App\Models\BoatFeedback::class, 'boat_feedback_id', 'id');
    }

    public function captainFeedback()
    {
        return $this->belongsTo(\App\Models\CaptainFeedback::class, 'captain_feedback_id', 'id');
    }

    public function contactCustomer()
    {
        return $this->belongsTo(\App\Models\ContactCustomer::class, 'contact_customer_id', 'id');
    }

    public function conversation()
    {
        return $this->belongsTo(\App\Models\Conversation::class, 'conversation_id', 'id');
    }

    public function experienceFeedback()
    {
        return $this->belongsTo(\App\Models\ExperienceFeedback::class, 'experience_feedback_id', 'id');
    }

    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }

    public function userFeedback()
    {
        return $this->belongsTo(\App\Models\UserFeedback::class, 'user_feedback_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function additionalServices()
    {
        return $this->belongsToMany(\App\Models\AdditionalService::class, 'bookings_additional_services', 'booking_id', 'additional_service_id');
    }

    public function bookingsAdditionalServices()
    {
        return $this->hasMany(\App\Models\BookingsAdditionalService::class, 'booking_id', 'id');
    }

    public function customers()
    {
        return $this->hasMany(\App\Models\Customer::class, 'booking_id', 'id');
    }

    public function fixedAdditionalServices()
    {
        return $this->belongsToMany(\App\Models\FixedAdditionalService::class, 'bookings_fixed_additional_services', 'booking_id', 'fixed_additional_service_id');
    }
}
