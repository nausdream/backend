<?php namespace App\Models;


use App\Interfaces\Transformable;
use App\Services\Transformers\Transformer;
use App\Services\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Laravel\Scout\Searchable;

class User extends AbstractModel implements Transformable, Authenticatable
{
    use AuthenticableTrait;
    use SoftDeletes;
    use Searchable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id','password', 'created_at', 'updated_at', 'deleted_at', 'photo_version', 'pivot'];

    /**
     * The attributes that should be guarded for arrays.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
        'is_captain',
        'default_fee',
        'photo_version',
        'is_partner',
        'is_mail_activated',
        'is_suspended',
        'fuid',
        'is_phone_activated',
        'email',
        'phone',
        'is_set_password',
        'currency'
    ];

    /**
     * The attributes that should be casted for arrays.
     *
     * @var array
     */

    protected $casts = [
        'is_captain' => 'boolean',
        'is_partner' => 'boolean',
        'is_mail_activated' => 'boolean',
        'is_suspended' => 'boolean',
        'newsletter' => 'boolean',
        'is_phone_activated' => 'boolean',
        'is_set_password' => 'boolean',
        'has_card' => 'boolean',
        'is_skipper' => 'boolean',
        'skippers_have_license' => 'boolean'
    ];


    public function experienceVersions()
    {
        return $this->belongsToMany(\App\Models\ExperienceVersion::class, 'calls', 'user_id', 'experience_version_id');
    }

    public function languages()
    {
        return $this->belongsToMany(\App\Models\Language::class, 'languages_users', 'user_id', 'language_id');
    }

    public function conversations()
    {
        return $this->belongsToMany(\App\Models\Conversation::class, 'messages', 'user_id', 'conversation_id');
    }

    public function boatFeedbacks()
    {
        return $this->hasMany(\App\Models\BoatFeedback::class, 'user_id', 'id');
    }

    public function boats()
    {
        return $this->hasMany(\App\Models\Boat::class, 'user_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'user_id', 'id');
    }

    public function calls()
    {
        return $this->hasMany(\App\Models\Call::class, 'user_id', 'id');
    }

    public function captainFeedbacks()
    {
        return $this->hasMany(\App\Models\CaptainFeedback::class, 'captain_id', 'id');
    }

    public function userFeedbacksForCaptains()
    {
        return $this->hasMany(\App\Models\CaptainFeedback::class, 'user_id', 'id');
    }

    public function captainConversations()
    {
        return $this->hasMany(\App\Models\Conversation::class, 'captain_id', 'id');
    }

    public function userConversations()
    {
        return $this->hasMany(\App\Models\Conversation::class, 'user_id', 'id');
    }

    public function coupons()
    {
        return $this->belongsToMany(\App\Models\Coupon::class, 'coupons_users', 'user_id', 'coupon_id');
    }

    public function couponsUsers()
    {
        return $this->hasMany(\App\Models\CouponsUser::class, 'user_id', 'id');
    }

    public function emailVerifications()
    {
        return $this->hasMany(\App\Models\EmailVerification::class, 'user_id', 'id');
    }

    public function events()
    {
        return $this->hasMany(\App\Models\Event::class, 'user_id', 'id');
    }

    public function experienceFeedbacks()
    {
        return $this->hasMany(\App\Models\ExperienceFeedback::class, 'user_id', 'id');
    }

    public function languagesUsers()
    {
        return $this->hasMany(\App\Models\LanguagesUser::class, 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(\App\Models\Message::class, 'user_id', 'id');
    }

    public function tokens()
    {
        return $this->hasMany(\App\Models\Token::class, 'user_id', 'id');
    }

    public function captainFeedbacksForUser()
    {
        return $this->hasMany(\App\Models\UserFeedback::class, 'captain_id', 'id');
    }

    public function userFeedbacks()
    {
        return $this->hasMany(\App\Models\UserFeedback::class, 'user_id', 'id');
    }

    public function enterpriseCountry()
    {
        return $this->belongsTo(\App\Models\Country::class, 'enterprise_country', 'id');
    }

    /**
     * Set visibility for administrator
     */

    public function setVisibilityAll()
    {
        parent::setVisibilityAll();
        $this->hidden[] = "password";
        $this->hidden[] = "photo_version";
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer():Transformer
    {
        return new UserTransformer();
    }

    /**
     * Set the captain type.
     *
     * @param  string  $value
     * @return void
     */
    public function setCaptainTypeAttribute($value)
    {
        $this->attributes['captain_type'] = CaptainType::all()->where('name', $value)->last()->id ?? null;
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return env('ALGOLIA_INDEX_USERS');
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'name' => (isset($this->first_name) ? $this->first_name . ' ' : '') . (isset($this->last_name) ? $this->last_name : ''),
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }
}
