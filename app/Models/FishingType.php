<?php
/**
 * User: Giuseppe
 * Date: 09/02/2017
 * Time: 19:33
 */

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class FishingType extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'fishing_types';
    protected $visible = ['name'];
    protected $fillable = ['name'];

}