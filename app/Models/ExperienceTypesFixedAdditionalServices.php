<?php
/**
 * User: Giuseppe Basciu
 * Date: 08/02/2017
 * Time: 18:40
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceTypesFixedAdditionalServices extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_types_fixed_additional_services';

    public function experienceType()
    {
        return $this->belongsTo(\App\Models\ExperienceType::class, 'experience_type_id', 'id');
    }

    public function fixedAdditionalService()
    {
        return $this->belongsTo(\App\Models\FixedAdditionalService::class, 'fixed_additional_service_id', 'id');
    }
}