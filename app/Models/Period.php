<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Period extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'periods';
    protected $fillable = ['date_start', 'date_end', 'price', 'min_person', 'entire_boat', 'is_default', 'currency'];
    protected $visible = ['date_start', 'date_end', 'price', 'min_person', 'entire_boat', 'is_default', 'currency'];

    protected $casts = [
        'is_default' => 'boolean',
        'entire_boat' => 'boolean',
    ];

    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany(\App\Models\Price::class, 'period_id', 'id');
    }


}
