<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'accessories';
    protected $visible = ['name'];
    protected $fillable = ['name'];

    public function boatVersions()
    {
        return $this->belongsToMany(\App\Models\BoatVersion::class, 'accessories_boat_versions', 'accessory_id', 'boat_version_id');
    }

    public function accessoriesBoatVersions()
    {
        return $this->hasMany(\App\Models\AccessoriesBoatVersion::class, 'accessory_id', 'id');
    }

}
