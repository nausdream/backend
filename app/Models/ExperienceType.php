<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceType extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_types';
    protected $visible = ['name'];
    protected $fillable = ['name'];

    public function boats()
    {
        return $this->belongsToMany(\App\Models\Boat::class, 'boats_experience_types', 'experience_type_id', 'boat_id');
    }

    public function boatsExperienceTypes()
    {
        return $this->hasMany(\App\Models\BoatsExperienceType::class, 'experience_type_id', 'id');
    }

    public function fixedAdditionalServices()
    {
        return $this->belongsToMany(\App\Models\FixedAdditionalService::class, 'experience_types_fixed_additional_services', 'experience_type_id', 'fixed_additional_service_id');
    }

    public function fixedAdditionalServicesExperienceTypes()
    {
        return $this->hasMany(\App\Models\ExperienceTypesFixedAdditionalServices::class, 'experience_type_id', 'id');
    }

}
