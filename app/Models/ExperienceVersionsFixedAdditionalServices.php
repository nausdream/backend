<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 18:40
 */

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceVersionsFixedAdditionalServices extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_versions_fixed_additional_services';
    protected $fillable = ['price', 'per_person', 'currency'];
    protected $visible = ['price', 'per_person', 'currency'];
    protected $casts = ['per_person' => 'boolean'];


    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }

    public function fixedAdditionalService()
    {
        return $this->belongsTo(\App\Models\FixedAdditionalService::class, 'fixed_additional_service_id', 'id');
    }
}