<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class BookingsAdditionalService extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'bookings_additional_services';


    public function additionalService()
    {
        return $this->belongsTo(\App\Models\AdditionalService::class, 'additional_service_id', 'id');
    }

    public function booking()
    {
        return $this->belongsTo(\App\Models\Booking::class, 'booking_id', 'id');
    }


}
