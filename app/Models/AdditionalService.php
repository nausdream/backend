<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalService extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'additional_services';
    protected $fillable = ['name', 'price', 'per_person', 'currency'];
    protected $visible = ['name', 'price', 'per_person', 'currency'];
    protected $casts = ['per_person' => 'boolean'];

    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }

    public function bookings()
    {
        return $this->belongsToMany(\App\Models\Booking::class, 'bookings_additional_services', 'additional_service_id', 'booking_id');
    }

    public function bookingsAdditionalServices()
    {
        return $this->hasMany(\App\Models\BookingsAdditionalService::class, 'additional_service_id', 'id');
    }


}
