<?php
/**
 * User: Giuseppe
 * Date: 08/11/2016
 * Time: 12:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AbstractModel extends Model
{
    protected $hidden;

    public function setVisibilityAll()
    {
        $this->hidden = ['id'];
    }
}