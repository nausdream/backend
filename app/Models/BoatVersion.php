<?php namespace App\Models;


use App\Constant;
use App\Interfaces\Transformable;
use App\Services\Transformers\Transformer;
use App\Services\Transformers\BoatVersionTransformer;
use App\Services\ValueKeyHelper;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoatVersion extends AbstractModel implements Transformable
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boat_versions';
    protected $guarded = ['id', 'status', 'boat_id', 'admin_id', 'created_at', 'updated_at', 'deleted_at'];
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
        'version_external_photo',
        'version_internal_photo',
        'version_sea_photo'
    ];
    protected $casts = [
        'has_insurance' => 'boolean',
        'is_finished' => 'boolean',
        'events' => 'boolean'
    ];


    public function administrator()
    {
        return $this->belongsTo(\App\Models\Administrator::class, 'admin_id', 'id');
    }

    public function boat()
    {
        return $this->belongsTo(\App\Models\Boat::class, 'boat_id', 'id');
    }

    public function accessories()
    {
        return $this->belongsToMany(\App\Models\Accessory::class, 'accessories_boat_versions', 'boat_version_id', 'accessory_id');
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer():Transformer
    {
        return new BoatVersionTransformer();
    }

    /**
     * Set the boat type.
     *
     * @param  string  $value
     * @return void
     */
    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = BoatType::all()->where('name', $value)->last()->id ?? null;
    }

    /**
     * Set the boat material.
     *
     * @param  string  $value
     * @return void
     */
    public function setMaterialAttribute($value)
    {
        $this->attributes['material'] = BoatMaterial::all()->where('name', $value)->last()->id ?? null;
    }

    /**
     * Set the motor type.
     *
     * @param  string  $value
     * @return void
     */
    public function setMotorTypeAttribute($value)
    {
        $this->attributes['motor_type'] = MotorType::all()->where('name', $value)->last()->id ?? null;
    }
}
