<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceAvailability extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_availabilities';
    protected $fillable = ['date_start', 'date_end'];
    protected $visible = ['date_start', 'date_end'];


    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }


}
