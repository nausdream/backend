<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends AbstractModel
{
    protected $table = 'devices';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be guarded for arrays.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['name', 'width', 'height'];

}
