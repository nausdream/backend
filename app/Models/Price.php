<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'prices';
    protected $fillable = ['price', 'person', 'currency'];
    protected $visible = ['person', 'price', 'currency'];

    public function period()
    {
        return $this->belongsTo(\App\Models\Period::class, 'period_id', 'id');
    }


}
