<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Token extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'tokens';

    /**
     * The attributes that should be casted for arrays.
     *
     * @var array
     */

    protected $casts = [
        'is_mail_token' => 'boolean'
    ];


    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
