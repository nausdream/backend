<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class BoatsExperienceType extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boats_experience_types';

    public function boats()
    {
        return $this->belongsTo(\App\Models\Boat::class, 'boat_id', 'id');
    }

    public function experienceTypes()
    {
        return $this->belongsTo(\App\Models\ExperienceType::class, 'experience_type_id', 'id');
    }

}
