<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class BoatFeedback extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boat_feedbacks';
    protected $fillable = ['score'];
    protected $visible = ['score', 'user_id'];


    public function boat()
    {
        return $this->belongsTo(\App\Models\Boat::class, 'boat_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'boat_feedback_id', 'id');
    }


}
