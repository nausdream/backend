<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Migration extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'migrations';


}
