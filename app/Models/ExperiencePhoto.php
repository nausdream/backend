<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ExperiencePhoto extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that could be fillable.
     *
     * @var array
     */
    protected $fillable = ['is_cover'];

    /**
     * The attributes that should be casted for arrays.
     *
     * @var array
     */

    protected $casts = [
        'is_cover' => 'boolean',
    ];

    /**
     * Generated
     */

    protected $table = 'experience_photos';

    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }


}
