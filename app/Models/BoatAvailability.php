<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class BoatAvailability extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boat_availabilities';
    protected $fillable = ['date_start', 'date_end'];
    protected $visible = ['date_start', 'date_end'];


    public function boat()
    {
        return $this->belongsTo(\App\Models\Boat::class, 'boat_id', 'id');
    }


}
