<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Step extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'steps';

    protected $fillable = ['step', 'message'];


    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }


}
