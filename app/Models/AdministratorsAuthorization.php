<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class AdministratorsAuthorization extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'administrators_authorizations';


    public function administrator()
    {
        return $this->belongsTo(\App\Models\Administrator::class, 'administrator_id', 'id');
    }

    public function authorization()
    {
        return $this->belongsTo(\App\Models\Authorization::class, 'authorization_id', 'id');
    }


}
