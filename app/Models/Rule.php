<?php
/**
 * User: Luca Puddu
 * Date: 21/11/2016
 * Time: 18:22
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Rule extends AbstractModel {
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'rules';
    protected $visible = ['name'];
    protected $fillable = ['name'];

    public function experienceVersions()
    {
        return $this->belongsToMany(\App\Models\ExperienceVersion::class, 'experience_versions_rules', 'rule_id', 'experience_version_id');
    }

    public function experienceVersionsRules()
    {
        return $this->hasMany(\App\Models\ExperienceVersionRule::class, 'rule_id', 'id');
    }
}