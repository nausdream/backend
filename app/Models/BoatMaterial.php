<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class BoatMaterial extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boat_materials';

    protected $fillable = ['name'];

}
