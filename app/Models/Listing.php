<?php
/**
 * User: Giuseppe
 * Date: 30/03/2017
 * Time: 12:08
 */

namespace App\Models;

use App\Services\PhotoHelper;

class Listing
{
    public $id;
    private $type;
    private $title;
    private $departure_port;
    private $price;
    private $photo;
    private $slug_url;

    /**
     * Listing constructor.
     *
     * @param ExperienceVersion $experienceVersion
     * @param int $price
     * @param string $language
     * @param string|null $device
     */
    public function __construct(ExperienceVersion $experienceVersion, int $price, string $language, string $device = null)
    {
        $this->id = $experienceVersion->experience_id;
        $this->type = $experienceVersion->type;
        $this->title = $experienceVersion->title;
        $this->departure_port = $experienceVersion->departure_port;
        $this->lat = $experienceVersion->departure_lat;
        $this->lng = $experienceVersion->departure_lng;
        $this->price = $price;
        $this->photo = PhotoHelper::getExperienceCoverPhotoData($experienceVersion, $device);
        $seo = $experienceVersion->seos()->where('language', $language)->first();
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', \Lang::getFallback())->first();
        }
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->where('language', $experienceVersion->experience()->first()->boat()->first()->user()->first()->language)->first();
        }
        if (!isset($seo)) {
            $seo = $experienceVersion->seos()->first();
        }
        $this->slug_url = $seo->slug_url;
    }

    /**
     * To array method.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'type' => $this->type,
            'title' => $this->title,
            'departure_port' => $this->departure_port,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'price' => $this->price,
            'photo' => $this->photo,
            'slug_url' => $this->slug_url
        ];
    }
}