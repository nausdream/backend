<?php namespace App\Models;


use App\Interfaces\Transformable;
use App\Services\Transformers\AdministratorTransformer;
use App\Services\Transformers\Transformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Administrator extends AbstractModel implements Transformable
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'administrators';
    protected $fillable = ['first_name', 'last_name', 'email', 'phone'];


    public function authorizations()
    {
        return $this->belongsToMany(\App\Models\Authorization::class, 'administrators_authorizations', 'administrator_id', 'authorization_id');
    }

    public function administratorsAuthorizations()
    {
        return $this->hasMany(\App\Models\AdministratorsAuthorization::class, 'administrator_id', 'id');
    }

    public function boatVersions()
    {
        return $this->hasMany(\App\Models\BoatVersion::class, 'admin_id', 'id');
    }

    public function area()
    {
        return $this->belongsTo(\App\Models\Area::class, 'area_id', 'id');
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer() : Transformer
    {
        return new AdministratorTransformer();
    }
}
