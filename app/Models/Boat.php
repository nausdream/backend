<?php namespace App\Models;


use App\Constant;
use App\Interfaces\Transformable;
use App\Services\Transformers\Transformer;
use App\Services\Transformers\BoatTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boat extends AbstractModel implements Transformable
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'boats';

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'is_luxury' => 'boolean',
        'events' => 'boolean'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function boatAvailabilities()
    {
        return $this->hasMany(\App\Models\BoatAvailability::class, 'boat_id', 'id');
    }

    public function boatFeedbacks()
    {
        return $this->hasMany(\App\Models\BoatFeedback::class, 'boat_id', 'id');
    }

    public function boatVersions()
    {
        return $this->hasMany(\App\Models\BoatVersion::class, 'boat_id', 'id');
    }

    public function experiences()
    {
        return $this->hasMany(\App\Models\Experience::class, 'boat_id', 'id');
    }

    public function experienceTypes()
    {
        return $this->belongsToMany(\App\Models\ExperienceType::class, 'boats_experience_types', 'boat_id', 'experience_type_id');
    }

    public function lastFinishedBoatVersion()
    {
        return $this->hasMany(\App\Models\BoatVersion::class, 'boat_id', 'id')->where('is_finished', true)->get()->last();
    }

    public function lastFinishedAndAcceptedBoatVersion()
    {
        return $this->hasMany(\App\Models\BoatVersion::class, 'boat_id', 'id')->where('is_finished', true)->where('status', Constant::STATUS_ACCEPTED)->get()->last();
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer():Transformer
    {
        return new BoatTransformer();
    }
}
