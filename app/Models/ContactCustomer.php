<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class ContactCustomer extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'contact_customers';


    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'contact_customer_id', 'id');
    }


}
