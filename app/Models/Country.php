<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name'];

    /**
     * Generated
     */

    protected $table = 'countries';


    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'enterprise_country', 'id');
    }

}
