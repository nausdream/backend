<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'coupons';
    protected $hidden = [
        'id', 'area_id', 'experience_id',
        'created_at', 'updated_at', 'deleted_at',
        'experience', 'area'
    ];
    protected $casts = ['is_percentual' => 'boolean', 'is_consumed' => 'boolean', 'is_consumable' => 'boolean'];
    protected $fillable = ['name', 'is_percentual', 'amount', 'expiration_date', 'currency', 'is_consumable'];


    public function area()
    {
        return $this->belongsTo(\App\Models\Area::class, 'area_id', 'id');
    }

    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'coupons_users', 'coupon_id', 'user_id');
    }

    public function couponsUsers()
    {
        return $this->hasMany(\App\Models\CouponsUser::class, 'coupon_id', 'id');
    }
}
