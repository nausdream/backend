<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class UserFeedback extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'user_feedbacks';


    public function captain()
    {
        return $this->belongsTo(\App\Models\User::class, 'captain_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'user_feedback_id', 'id');
    }


}
