<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'areas';
    protected $fillable = ['name', 'point_a_lat', 'point_a_lng', 'point_b_lat', 'point_b_lng'];
    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];


    public function coupons()
    {
        return $this->hasMany(\App\Models\Coupon::class, 'area_id', 'id');
    }

    public function administrators()
    {
        return $this->hasMany(\App\Models\Administrator::class, 'area_id', 'id');
    }
}
