<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $visible = ['language'];

    protected $fillable = ['language'];

    /**
     * Generated
     */

    protected $table = 'languages';


    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'languages_users', 'language_id', 'user_id');
    }

    public function languagesUsers()
    {
        return $this->hasMany(\App\Models\LanguagesUser::class, 'language_id', 'id');
    }


}
