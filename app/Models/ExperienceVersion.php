<?php namespace App\Models;


use App\Interfaces\Transformable;
use App\Services\Transformers\ExperienceVersionTransformer;
use App\Services\Transformers\Transformer;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExperienceVersion extends AbstractModel implements Transformable
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'experience_versions';
    protected $guarded = ['id', 'status', 'is_finished', 'experience_id', 'created_at', 'updated_at', 'deleted_at', 'is_searchable'];
    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'experience_id'];

    protected $casts = [
        'monday' => 'boolean',
        'tuesday' => 'boolean',
        'wednesday' => 'boolean',
        'thursday' => 'boolean',
        'friday' => 'boolean',
        'saturday' => 'boolean',
        'sunday' => 'boolean',
        'is_fixed_price' => 'boolean',
        'is_searchable' => 'boolean',
        'is_finished' => 'boolean',
        'road_stead' => 'boolean',
        'fixed_menu' => 'boolean',
        'starter_dish' => 'boolean',
        'first_dish' => 'boolean',
        'second_dish' => 'boolean',
        'side_dish' => 'boolean',
        'dessert_dish' => 'boolean',
        'bread' => 'boolean',
        'wines' => 'boolean',
        'champagne' => 'boolean',
        'weather' => 'boolean',
        'lures' => 'boolean',
        'fishing_pole' => 'boolean',
        'fresh_bag' => 'boolean',
        'ice' => 'boolean',
        'underwater_baptism' => 'boolean',
        'down_payment' => 'boolean',
        'deposit_card' => 'boolean',
        'deposit_check' => 'boolean',
        'deposit_cash' => 'boolean',
        'captain_night_aboard' => 'boolean'
    ];


    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'calls', 'experience_version_id', 'user_id');
    }

    public function additionalServices()
    {
        return $this->hasMany(\App\Models\AdditionalService::class, 'experience_version_id', 'id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'experience_version_id', 'id');
    }

    public function calls()
    {
        return $this->hasMany(\App\Models\Call::class, 'experience_version_id', 'id');
    }

    public function steps()
    {
        return $this->hasMany(\App\Models\Step::class, 'experience_version_id', 'id');
    }

    public function experienceVersionPhotos()
    {
        return $this->hasMany(\App\Models\ExperiencePhoto::class, 'experience_version_id', 'id');
    }

    public function rules(){
        return $this->belongsToMany(\App\Models\Rule::class, 'experience_versions_rules', 'experience_version_id', 'rule_id');
    }

    public function fixedAdditionalServices()
    {
        return $this->belongsToMany(\App\Models\FixedAdditionalService::class, 'experience_versions_fixed_additional_services', 'experience_version_id', 'fixed_additional_service_id');
    }

    public function experienceVersionsFixedAdditionalServices()
    {
        return $this->hasMany(\App\Models\ExperienceVersionsFixedAdditionalServices::class, 'experience_version_id', 'id');
    }

    public function seos()
    {
        return $this->hasMany(\App\Models\Seo::class, 'experience_version_id', 'id');
    }

    /**
     * Return model transformer
     *
     * @return mixed
     */
    public function getTransformer() : Transformer
    {
        return new ExperienceVersionTransformer();
    }

    /**
     * Set the experience type.
     *
     * @param  string  $value
     * @return void
     */
    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = ExperienceType::all()->where('name', $value)->last()->id ?? null;;
    }

    /**
     * Set the experience fishing partition.
     *
     * @param  string  $value
     * @return void
     */
    public function setFishingPartitionAttribute($value)
    {
        $this->attributes['fishing_partition'] = FishingPartition::all()->where('name', $value)->last()->id ?? null;;
    }

    /**
     * Set the experience fishing type.
     *
     * @param  string  $value
     * @return void
     */
    public function setFishingTypeAttribute($value)
    {
        $this->attributes['fishing_type'] = FishingType::all()->where('name', $value)->last()->id ?? null;;
    }
}
