<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class LanguagesUser extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'languages_users';


    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class, 'language_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
