<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Call extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'calls';


    public function experienceVersion()
    {
        return $this->belongsTo(\App\Models\ExperienceVersion::class, 'experience_version_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }


}
