<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends AbstractModel
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Generated
     */

    protected $table = 'conversations';
    protected $casts = ['alert' => 'boolean'];


    public function captain()
    {
        return $this->belongsTo(\App\Models\User::class, 'captain_id', 'id');
    }

    public function experience()
    {
        return $this->belongsTo(\App\Models\Experience::class, 'experience_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'messages', 'conversation_id', 'user_id');
    }

    public function bookings()
    {
        return $this->hasMany(\App\Models\Booking::class, 'conversation_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(\App\Models\Message::class, 'conversation_id', 'id');
    }


}
