<?php

namespace App\Providers;

use App\Constant;
use App\Http\Controllers\v1\TranslationsController;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\Booking;
use App\Models\Conversation;
use App\Models\ExperienceVersion;
use App\Models\Seo;
use App\Models\User;
use App\Services\AreaHelper;
use App\TranslationModels\User as Translator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('get-user-list', function ($account) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('get-captain-list', function ($account) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('get-partner-list', function ($account) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('post-partner', function ($account) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('show-user', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                if ($user->is_captain) {
                    $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
                if ($user->is_partner) {
                    $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
            }
            return false;
        });

        Gate::define('update-user', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                if ($user->is_captain) {
                    $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
                if ($user->is_partner) {
                    $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
            }
            return false;
        });

        Gate::define('get-user-notifications', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            return false;
        });

        Gate::define('show-translator', function ($account, $translator) {
            if ($account instanceof Translator) {
                return $account->id === $translator->id;
            }
            return false;
        });

        Gate::define('password-login', function ($account) {
            if ($account instanceof User) {
                if ($account->is_captain || $account->is_partner) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('logout', function ($account) {
            if ($account instanceof User || $account instanceof Administrator || $account instanceof Translator) {
                return true;
            }
            return false;
        });

        Gate::define('reset-password', function ($account) {
            if ($account instanceof User) {
                if ($account->is_captain || $account->is_partner) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('resend-token', function ($account) {
            if ($account instanceof User) {
                if ($account->is_captain || $account->is_partner) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('update-password', function ($account, $user) {
            if ($account instanceof User) {
                if (($account->is_captain || $account->is_partner) && $account->id == $user->id) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('set-password', function ($account, $user) {
            if ($account instanceof User) {
                if (($account->is_captain || $account->is_partner)
                    && $account->id == $user->id
                    && !$account->is_set_password
                ) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('update-user-languages', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                if ($user->is_captain) {
                    $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
                if ($user->is_partner) {
                    $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
            }
            return false;
        });

        Gate::define('update-user-telephone', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                if ($user->is_captain) {
                    $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
                if ($user->is_partner) {
                    $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
            }
            return false;
        });

        Gate::define('update-profile-photo', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id === $user->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
                if ($user->is_captain) {
                    $adminAuthorization = $account->authorizations()->where('name', 'captains')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
                if ($user->is_partner) {
                    $adminAuthorization = $account->authorizations()->where('name', 'partners')->get()->last();
                    if (isset($adminAuthorization)) {
                        return true;
                    }
                }
            }
            return false;
        });

        Gate::define('update-boat-photo', function ($account, $boatVersion) {
            if ($account instanceof User) {
                if ($boatVersion->is_finished == true && $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY) {
                    return false;
                }
                return ($account->id === $boatVersion->boat()->first()->user()->first()->id);
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                if (!isset($adminAuthorization)) {
                    return false;
                }

                return $this->checkAdminArea($account, $boatVersion);
            }
            return false;
        });

        Gate::define('post-signed-urls', function ($account) {
            if ($account instanceof User || $account instanceof Administrator) {
                return true;
            }
            return false;
        });

        Gate::define('get-page-list', function ($account) {
            if ($account instanceof Translator) {
                return true;
            }
            return false;
        });

        Gate::define('delete-page', function ($account) {
            if ($account instanceof Translator) {
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('post-page', function ($account) {
            if ($account instanceof Translator) {
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('get-page-translation-list', function ($account, $language) {
            if ($account instanceof Translator) {
                if ($language->language == \Lang::getFallback()) {
                    return true;
                }
                $languageAccount = $account->languages()->where('language', $language->language)->get()->first();
                if (isset($languageAccount) && !is_null($languageAccount)) {
                    return true;
                }
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });


        // BOATS
        Gate::define('show-boat-editing', function ($account, $boatVersion) {
            if ($account instanceof User) {
                return ($account->id === $boatVersion->boat()->first()->user()->first()->id);
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorizations = $account->authorizations()->where('name', 'boats')->get()->first();
                if (!isset($adminAuthorizations)) {
                    return false;
                }

                // Check if admin area includes boat
                return $this->checkAdminArea($account, $boatVersion);
            }
            return false;
        });

        Gate::define('show-boat', function ($account, $boatVersion) {
            return $boatVersion->status != Constant::STATUS_REJECTED;
        });

        Gate::define('create-boat', function ($account, User $user) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            if ($account instanceof User) {
                return $account->id === $user->id && $user->is_captain;
            }
            return false;
        });

        Gate::define('patch-boat', function ($account, BoatVersion $boatVersion) {
            if ($account instanceof User) {
                // Check if user is this boat's captain
                $isOwner = $account->id === $boatVersion->boat()->first()->user()->first()->id;

                return $isOwner &&
                    !$boatVersion->is_finished ||
                    $boatVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY ||
                    $boatVersion->status == Constant::STATUS_ACCEPTED;
            }
            return false;
        });

        Gate::define('put-boat', function ($account, BoatVersion $boatVersion) {
            if (!isset($boatVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $boatVersion)
                    && $boatVersion->status != Constant::STATUS_REJECTED
                    && $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $boatVersion->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$boatVersion->is_finished ||
                        $boatVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('boat-change-status', function ($account, $boatVersion, $status) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                $hasPower = isset($adminAuthorization);
                if (!$hasPower) return false;

                // Check if experienceVersion status is not rejected or accepted_conditionally
                $isNotRejectedOrAcceptedConditionally =
                    $boatVersion->status != Constant::STATUS_REJECTED ||
                    $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
                if (!$isNotRejectedOrAcceptedConditionally) return false;

                // Check if experienceVersion is in admin area
                $isInAdminArea = $this->checkAdminArea($account, $boatVersion);
                if (!$isInAdminArea) return false;

                /* Check if admin can set from a status to another. Admin can set:
                 *
                 * PENDING->(ACCEPTED || REJECTED || REJECTED CONDITIONALLY)
                 *
                 */

                // isThisStatusSettable is true if changing from pending to any allowed status..
                $isThisStatusSettable =
                    $boatVersion->status == Constant::STATUS_PENDING &&
                    (
                        $status == Constant::STATUS_ACCEPTED ||
                        $status == Constant::STATUS_REJECTED ||
                        $status == Constant::STATUS_ACCEPTED_CONDITIONALLY
                    );

                if (!$isThisStatusSettable) return false;

                // Return true only if it's passed all checks
                return true;
            }
            return false;
        });

        Gate::define('edit-boat-translatable-field', function ($account, $boatVersion) {
            if ($account instanceof User || $account instanceof Administrator) {
                return Gate::forUser($account)->allows('edit-boat', $boatVersion);
            }
            if ($account instanceof Translator) {
                //TODO
            }
            return false;
        });

        Gate::define('edit-boat', function ($account, $boatVersion) {
            if ($account instanceof User) {
                return ($account->id === $boatVersion->boat()->first()->user()->first()->id);
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                if (!isset($adminAuthorization)) {
                    return false;
                }

                return $this->checkAdminArea($account, $boatVersion);

            }
            return false;
        });

        Gate::define('view-boats-list', function ($account) {
            if ($account instanceof User) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('view-all-captain-boats', function ($account, $captain) {
            if ($account instanceof User) {
                return $account->id === $captain->id;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('view-editing-boats', function ($account, User $boatsUser) {
            if ($account instanceof User) {
                return $account->id === $boatsUser->id;
            } elseif ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->first();
                if (isset($adminAuthorization)) {
                    return true;
                }

            }
            return false;
        });

        Gate::define('post-boat-availability', function ($account, $boatVersion) {
            if (!isset($boatVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin boat authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $boatVersion)
                    && $boatVersion->status != Constant::STATUS_REJECTED
                    && $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this boatVersion's captain
                $isOwner = $account->id === $boatVersion->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$boatVersion->is_finished ||
                        $boatVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-boat-availability', function ($account, $boatVersion) {
            if (!isset($boatVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin boat authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $boatVersion)
                    && $boatVersion->status != Constant::STATUS_REJECTED
                    && $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this boatVersion's captain
                $isOwner = $account->id === $boatVersion->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$boatVersion->is_finished ||
                        $boatVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-boat-availability', function ($account, $boatVersion) {
            if (!isset($boatVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin boat authorization
                $adminAuthorization = $account->authorizations()->where('name', 'boats')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $boatVersion)
                    && $boatVersion->status != Constant::STATUS_REJECTED
                    && $boatVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this boatVersion's captain
                $isOwner = $account->id === $boatVersion->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$boatVersion->is_finished ||
                        $boatVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });


        // EXPERIENCES
        Gate::define('show-experience-editing', function ($account, $experienceVersion) {
            if ($account instanceof User) {
                return ($account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id);
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();
                if (!isset($adminAuthorization)) {
                    return false;
                }

                return $this->checkAdminArea($account, $experienceVersion);
            }
            return false;
        });

        Gate::define('show-experience', function ($account, $experienceVersion) {
            return $experienceVersion->status != Constant::STATUS_REJECTED &&
                $experienceVersion->status != Constant::STATUS_FREEZED;
        });

        Gate::define('view-experiences-list', function ($account) {
            if ($account instanceof User) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->last();
                if (!isset($adminAuthorization)) {
                    return false;
                }
                return true;
            }
            return false;
        });

        Gate::define('experience-change-status', function ($account, $experienceVersion, $status) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->last();
                $hasPower = isset($adminAuthorization);
                if (!$hasPower) return false;

                // Check if experienceVersion status is not rejected or accepted_conditionally
                $isNotRejectedOrAcceptedConditionally =
                    $experienceVersion->status != Constant::STATUS_REJECTED ||
                    $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
                if (!$isNotRejectedOrAcceptedConditionally) return false;

                // Check if experienceVersion is in admin area
                $isInAdminArea = $this->checkAdminArea($account, $experienceVersion);
                if (!$isInAdminArea) return false;

                /* Check if admin can set from a status to another. Admin can set:
                 *
                 * PENDING->(ACCEPTED || REJECTED || REJECTED CONDITIONALLY)
                 * ACCEPTED->FREEZED
                 * FREEZED->ACCEPTED
                 *
                 */

                // isThisStatusSettable is true if changing from pending to any allowed..
                $isThisStatusSettable =
                    $experienceVersion->status == Constant::STATUS_PENDING &&
                    (
                        $status == Constant::STATUS_ACCEPTED ||
                        $status == Constant::STATUS_REJECTED ||
                        $status == Constant::STATUS_ACCEPTED_CONDITIONALLY
                    );

                // ...OR from accepted to freezed
                $isThisStatusSettable = $isThisStatusSettable ||
                    $experienceVersion->status == Constant::STATUS_ACCEPTED && $status == Constant::STATUS_FREEZED;

                // ...OR from freezed to accepted
                $isThisStatusSettable = $isThisStatusSettable ||
                    $experienceVersion->status == Constant::STATUS_FREEZED && $status == Constant::STATUS_ACCEPTED;
                if (!$isThisStatusSettable) return false;

                // Return true only if it's passed all checks
                return true;
            }
            return false;
        });

        Gate::define('view-editing-experiences', function ($account, User $experiencesUser) {
            if ($account instanceof User) {
                return $account->id === $experiencesUser->id;
            } elseif ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();
                if (isset($adminAuthorization)) {
                    return true;
                }

            }
            return false;
        });

        Gate::define('create-experience', function ($account, Boat $boat) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();
                if (isset($adminAuthorization)) {
                    return true;
                }
            } elseif ($account instanceof User) {
                return $account->id === $boat->user()->first()->id;
            }
            return false;
        });

        Gate::define('put-experience', function ($account, ExperienceVersion $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('show-seo', function ($account, Seo $seo) {
            if (!isset($seo)) {
                return false;
            }
            if ($account instanceof Translator) {
                return true;
            }
            return false;
        });

        Gate::define('get-seo-list', function ($account, $language) {
            if (!isset($language)) {
                return false;
            }
            if ($account instanceof Translator) {
                if ($language == \Lang::getFallback()) {
                    return true;
                }

                $languageAccount = $account->languages()->where('language', $language)->get()->first();
                if (isset($languageAccount) && !is_null($languageAccount)) {
                    return true;
                }

                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });


        Gate::define('post-seo', function ($account, ExperienceVersion $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status == Constant::STATUS_PENDING;
            }
            return false;
        });

        Gate::define('post-seo-translation', function ($account, ExperienceVersion $experienceVersion, $language) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Translator) {
                if ($experienceVersion->status != Constant::STATUS_ACCEPTED) {
                    return false;
                }
                $languageAccount = $account->languages()->where('language', $language->language)->get()->first();
                if (isset($languageAccount) && !is_null($languageAccount)) {
                    return true;
                }
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('get-seo', function ($account, ExperienceVersion $experienceVersion) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();
                if (!isset($adminAuthorization)) {
                    return false;
                }

                return $this->checkAdminArea($account, $experienceVersion);
            }
            return false;
        });

        Gate::define('get-seo-translation', function ($account, ExperienceVersion $experienceVersion, $language) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Translator) {
                if ($experienceVersion->status != Constant::STATUS_ACCEPTED) {
                    return false;
                }

                if (!isset($language)) {
                    return true;
                }

                $languageAccount = $account->languages()->where('language', $language->language)->get()->first();
                if (isset($languageAccount) && !is_null($languageAccount)) {
                    return true;
                }
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('get-slug-url-by-title', function ($account) {
            if ($account instanceof Translator) {
                return true;
            }
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();
                if (!isset($adminAuthorization)) {
                    return false;
                }
                return true;
            }
            return false;
        });

        Gate::define('patch-experience', function ($account, ExperienceVersion $experienceVersion) {
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;

                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                    $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY ||
                    $experienceVersion->status == Constant::STATUS_ACCEPTED);
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && ($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-experience-photo', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-experience-photo', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-experience-photo', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-fixed-additional-service-price', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-fixed-additional-service-price', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-fixed-additional-service-price', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-experience-availability', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-experience-availability', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-experience-availability', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-period', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-period', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-period', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-price', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('patch-price', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-price', function ($account, $experienceVersion, $price) {
            $isNotForOnePerson = $price->person != 1;
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && $experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY
                    && $isNotForOnePerson;
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY) &&
                    $isNotForOnePerson;
            }
            return false;
        });

        Gate::define('patch-additional-service', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('post-additional-service', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('delete-additional-service', function ($account, $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion)
                    && (($experienceVersion->status != Constant::STATUS_REJECTED
                    && $experienceVersion->status != Constant::STATUS_ACCEPTED_CONDITIONALLY));
            }
            if ($account instanceof User) {
                // Check if user is this experienceVersion's captain
                $isOwner = $account->id === $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
                return $isOwner &&
                    (!$experienceVersion->is_finished ||
                        $experienceVersion->status == Constant::STATUS_ACCEPTED_CONDITIONALLY);
            }
            return false;
        });

        Gate::define('create-step-message', function ($account, ExperienceVersion $experienceVersion) {
            if (!isset($experienceVersion)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin experience authorization
                $adminAuthorization = $account->authorizations()->where('name', 'experiences')->get()->first();

                return isset($adminAuthorization)
                    && $this->checkAdminArea($account, $experienceVersion);
            }
        });


        // COUPONS
        Gate::define('get-coupons-list', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin coupons authorization
                $adminAuthorization = $account->authorizations()->where('name', 'coupons')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('get-coupons-single', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin coupons authorization
                $adminAuthorization = $account->authorizations()->where('name', 'coupons')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('create-coupons', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin coupons authorization
                $adminAuthorization = $account->authorizations()->where('name', 'coupons')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('edit-coupons', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin coupons authorization
                $adminAuthorization = $account->authorizations()->where('name', 'coupons')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('delete-coupons', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin coupons authorization
                $adminAuthorization = $account->authorizations()->where('name', 'coupons')->get()->first();

                return isset($adminAuthorization);
            }
        });

        // OFFSITE-BOOKINGS

        Gate::define('get-offsite-bookings-list', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('get-offsite-bookings-single', function ($account, $offsiteBooking) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            if ($account instanceof User) {
                return $account->id == $offsiteBooking->user()->first()->id;
            }
            return false;
        });

        Gate::define('post-offsite-bookings', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        Gate::define('patch-offsite-bookings', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        // VOUCHERS

        Gate::define('post-vouchers', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        // EDEN VOUCHEERS

        Gate::define('post-eden-vouchers', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin bookings authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        // CONVERSATIONS
        Gate::define('get-conversations-list', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'conversations')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('get-conversations-single', function ($account, Conversation $conversation) {
            if (!isset($conversation)) {
                return false;
            }
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'conversations')->get()->first();

                return isset($adminAuthorization);
            }
            if ($account instanceof User) {
                return
                    $account->id === $conversation->user()->first()->id ||
                    $account->id === $conversation->captain()->first()->id;
            }
        });

        Gate::define('get-user-conversations', function ($account, int $userId) {
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'conversations')->get()->first();

                return isset($adminAuthorization);
            }
            if ($account instanceof User) {
                return $account->id === $userId;
            }
        });

        Gate::define('get-experience-conversations', function ($account, int $userId) {
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'conversations')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('edit-messages', function ($account) {
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'conversations')->get()->first();

                return isset($adminAuthorization);
            }
        });

        Gate::define('create-conversations', function ($account, $experience) {
            if ($account instanceof User) {

                // Check last finished experience version

                $lastFinishedExperienceVersion = $experience->lastFinishedExperienceVersion();
                $status = $lastFinishedExperienceVersion->status;
                if ($status == Constant::STATUS_REJECTED || $status == Constant::STATUS_FREEZED) {
                    return false;
                }

                // Get last finished and accepted experience version

                $experienceVersion = $experience->lastFinishedAndAcceptedExperienceVersion();

                // If last experienceVersion is accepted
                return isset($experienceVersion) &&
                    $account->id !== $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
            }
            return false;
        });

        Gate::define('create-booking', function ($account, $experienceVersion) {
            if ($account instanceof User) {
                return $account->id !== $experienceVersion->experience()->first()->boat()->first()->user()->first()->id;
            }
            return false;
        });

        Gate::define('get-booking', function ($account, $booking) {
            if ($account instanceof User) {
                return $account->id == $booking->user()->first()->id ||
                    $account->id == $booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first()->id;
            }
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        Gate::define('answer-booking', function ($account, $booking) {
            if ($account instanceof User) {
                return $account->id == $booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first()->id;
            }
            return false;
        });

        Gate::define('get-user-bookings', function ($account, $user) {
            if ($account instanceof User) {
                return $account->id == $user->id;
            }
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        Gate::define('get-captain-bookings', function ($account, $captain) {
            if ($account instanceof User) {
                return $account->id == $captain->id;
            }
            if ($account instanceof Administrator) {
                // Get admin conversations authorization
                $adminAuthorization = $account->authorizations()->where('name', 'bookings')->get()->first();

                return isset($adminAuthorization);
            }
            return false;
        });

        Gate::define('create-messages', function ($account, $conversation) {
            if ($account instanceof User) {
                return
                    $account->id === $conversation->user()->first()->id ||
                    $account->id === $conversation->captain()->first()->id;
            }
        });


        // SENTENCES
        Gate::define('post-sentence', function ($account) {
            if ($account instanceof Translator) {
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('delete-sentence', function ($account) {
            if ($account instanceof Translator) {
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });

        Gate::define('post-sentence-translation', function ($account, $language) {
            if ($account instanceof Translator) {
                $languageAccount = $account->languages()->where('language', $language->language)->get()->first();
                if (isset($languageAccount) && !is_null($languageAccount)) {
                    return true;
                }
                if ($account->is_admin) {
                    return true;
                }
            }
            return false;
        });


        Gate::define('create-token', function ($account) {
            if ($account instanceof Administrator) {
                // Check if admin has correct authorization
                $adminAuthorization = $account->authorizations()->where('name', 'users')->get()->last();
                if (isset($adminAuthorization)) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * Check if a model are inside an area
     *
     * @param $account
     * @param $model
     * @return boolean
     */
    function checkAdminArea($account, $model)
    {
        $area = $account->area;
        if (!isset($area) || is_null($area)) {
            return false;
        }

        // Check if admin area includes boatVersion or experienceVersion.
        // If model area is not set, return true
        if ($model instanceof ExperienceVersion) {
            $lat = $model->departure_lat;
            $lng = $model->departure_lng;
        } else {
            $lat = $model->lat;
            $lng = $model->lng;
        }

        return !isset($lat) || !isset($lng) || AreaHelper::inArea($area, $lat, $lng);

    }
}
