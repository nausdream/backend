<?php

/**
 * User: Giuseppe
 * Date: 17/11/2016
 * Time: 16:11
 */

use App\Jobs\SendMail;
use App\Jobs\SendSms;
use App\Mail\CaptainRegisteredToAdministrator;
use App\Mail\CaptainRegisteredToCaptain;
use App\Models\User;
use App\Http\Controllers\v1\TranslationsController;

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Services\JwtService;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;



class SendMailTrapTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    public $user_1;
    public $user_2;
    public $admin;
    public $boat;
    public $id;
    public $boatVersions;
    public $requestBody;
    public $stub;
    public $coupon;


    /** @test */
    public function it_sends_emails()
    {
        //$this->setThingsUp();
        //arrange
        //$captain = User::all()->where('is_captain', true)->first();

        //act
        //dispatch((new SendMail(new CaptainRegisteredToCaptain($captain)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new CaptainRegisteredToAdministrator($captain)))->onQueue(env('SQS_MAIL')));

        //dispatch((new SendMail(new \App\Mail\CaptainSavedBoatToAdministrator($this->boatVersions[2])))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedBoatStatusToCaptain($captain, $this->boatVersions[2], Constant::STATUS_ACCEPTED)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedBoatStatusToCaptain($captain, $this->boatVersions[2], Constant::STATUS_ACCEPTED_CONDITIONALLY, $this->fake->text)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedBoatStatusToCaptain($captain, $this->boatVersions[2], Constant::STATUS_REJECTED, $this->fake->text)))->onQueue(env('SQS_MAIL')));

        //dispatch((new SendMail(new \App\Mail\RestorePassword($captain)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\EmailUpdated($captain)))->onQueue(env('SQS_MAIL')));

        /*
        $experienceVersion = $captain->boats()->first()->experiences()->first()->experienceVersions()->first();
        $step_1 = new \App\Models\Step(['step' => 1, 'message' => $this->fake->text()]);
        $step_2 = new \App\Models\Step(['step' => 3, 'message' => $this->fake->text()]);
        $experienceVersion->steps()->save($step_1);
        $experienceVersion->steps()->save($step_2);
        */

        //dispatch((new SendMail(new \App\Mail\CaptainSavedExperienceToAdministrator($experienceVersion)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedExperienceStatusToCaptain($experienceVersion, Constant::STATUS_ACCEPTED)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedExperienceStatusToCaptain($experienceVersion, Constant::STATUS_ACCEPTED_CONDITIONALLY)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedExperienceStatusToCaptain($experienceVersion, Constant::STATUS_REJECTED)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\AdminChangedExperienceStatusToCaptain($experienceVersion, Constant::STATUS_FREEZED)))->onQueue(env('SQS_MAIL')));

        //$booking = \App\Models\Booking::all()->where('status', Constant::BOOKING_STATUS_PENDING)->random();
        //dispatch((new SendMail(new \App\Mail\BookingRequestedToCaptain($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\BookingRequestedToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

        //$booking = \App\Models\Booking::all()->where('status', Constant::BOOKING_STATUS_ACCEPTED)->random();
        //$booking->coupon = $this->coupon->name;
        //$booking->save();
        //dispatch((new SendMail(new \App\Mail\BookingAnswerToGuest($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\BookingAnswerToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

        //$booking = \App\Models\Booking::all()->where('status', Constant::BOOKING_STATUS_REJECTED)->random();
        //dispatch((new SendMail(new \App\Mail\BookingAnswerToGuest($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\BookingAnswerToAdministrator($booking)))->onQueue(env('SQS_MAIL')));

        //$booking = \App\Models\Booking::all()->where('status', Constant::BOOKING_STATUS_PAID)->random();
        //dispatch((new SendMail(new \App\Mail\BookingPaidToGuest($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\BookingPaidToCaptain($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\BookingPaidToAdministrator($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\PreExperienceToGuest($booking)))->onQueue(env('SQS_MAIL')));
        //dispatch((new SendMail(new \App\Mail\PreExperienceToCaptain($booking)))->onQueue(env('SQS_MAIL')));

        //\App\Services\PreExperienceMailSender::run();
        //New message for captain
        /*
        do {
            $message = \App\Models\Message::all()->random();
            $conversation = $message->conversation()->first();
        } while ($message->user_id != $conversation->user_id);
        dispatch((new SendMail(new \App\Mail\NewMessage($message)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendMail(new \App\Mail\NewMessageToAdministrator($message)))->onQueue(env('SQS_MAIL')));

        //New message for guest
        do {
            $message = \App\Models\Message::all()->random();
            $conversation = $message->conversation()->first();
        } while ($message->user_id != $conversation->captain_id);
        dispatch((new SendMail(new \App\Mail\NewMessage($message)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendMail(new \App\Mail\NewMessageToAdministrator($message)))->onQueue(env('SQS_MAIL')));
        */

        // Sms notifications
        /*
        $user = factory(\App\Models\User::class)->states('dummy')->create();
        $user->phone = '+17873632790';
        $user->save();
        */
        //dispatch((new SendSms(new App\Sms\AlertToCheckEmailToUser($user)))->onQueue(env('SQS_MAIL')));
        /*$booking = \App\Models\Booking::all()->random();
        $captain = $booking->experienceVersion()->first()->experience()->first()->boat()->first()->user()->first();
        $captain->phone = '+17873632790';
        $captain->save();
        dispatch((new SendSms(new App\Sms\BookingPaidToCaptain($booking)))->onQueue(env('SQS_MAIL')));
        dispatch((new SendSms(new App\Sms\BookingRequestedToCaptain($booking)))->onQueue(env('SQS_MAIL'))); */

        /*
        $conversation = \App\Models\Conversation::all()->random();
        $captain = $conversation->captain()->first();
        $captain->phone = '+17873632790';
        $captain->save();
        dispatch((new SendSms(new App\Sms\NewConversationToCaptain($conversation)))->onQueue(env('SQS_MAIL')));
        */

        /*
         * Contacts email
         */
        //dispatch((new SendMail(new App\Mail\ContactToSupport('giuseppe.basciu@gmail.com', $this->fake->text())))->onQueue(env('SQS_MAIL')));


        //assert
        $this->assertTrue(true);
    }

    // HELPERS
    private function getBoatVersion(\App\Models\Boat $boat = null, bool $isFinished = true)
    {
        $boatVersion = factory(\App\Models\BoatVersion::class)->states('dummy')->create(['is_finished' => $isFinished]);
        if (isset($boat)) {
            $boat->boatVersions()->save($boatVersion);
        }

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boatVersion;
    }

    private function getBoatVersionStub(bool $isFinished)
    {
        $requestBody = [
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'description' => str_random(Constant::BOAT_DESCRIPTION_LENGTH),
            'rules' => str_random(Constant::BOAT_RULES_LENGTH),
            'indications' => str_random(Constant::BOAT_INDICATIONS_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->name,
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->name,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->name,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'events' => $this->fake->boolean()
        ];

        if ($isFinished){
            $requestBody['is_finished'] = true;
        }

        return $requestBody;
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;
        return [
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(\App\Models\User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->docking_place = $this->fake->city;
        $user->save();
        return $user;
    }

    protected function getBoat(\App\Models\User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'boats')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function getHeaders($account)
    {
        return ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($account)];
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    protected function getRequestBody(int $boatId, array $stub)
    {
        return [
            'data' => [
                'type' => 'boats',
                'id' => (string)$boatId,
                'attributes' => $stub
            ]
        ];
    }

    protected function getRoute(int $id)
    {
        return 'v1/boats/' . $id;
    }

    protected function setThingsUp()
    {
        $this->user_1 = $this->getUser();
        $this->user_2 = $this->getUser();
        $this->admin = $this->getSuperAdmin();
        $this->boat = $this->getBoat($this->user_1);
        $this->id = $this->boat->id;
        $this->boatVersions = [];
        $this->boatVersions[] = $this->getBoatVersion($this->boat);
        $this->boatVersions[] = $this->getBoatVersion($this->boat);
        $this->boatVersions[] = $this->getBoatVersion($this->boat);

        $this->boatVersions[0]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[0]->save();

        $this->boatVersions[1]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[1]->save();

        $this->boatVersions[2]->version_external_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->version_internal_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->version_sea_photo = $this->fake->imageUrl();
        $this->boatVersions[2]->save();

        $this->stub = $this->getBoatVersionStub(true);
        $this->requestBody = $this->getRequestBody($this->boat->id, $this->stub);
        $this->coupon = $this->getCoupon();
    }

    protected function setStatus(int ...$status)
    {
        for ($i = 0; $i < count($this->boatVersions); $i++){
            $this->boatVersions[$i]->status = $status[$i];
            $this->boatVersions[$i]->save();
        }
    }

    protected function getCoupon()
    {
        return factory(\App\Models\Coupon::class)->states('dummy')->create();
    }

    protected function assertEdited(\App\Models\BoatVersion $boatVersion){
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['model' => $boatVersion->model]);
    }

    protected function assertNotEdited(\App\Models\BoatVersion $boatVersion){
        $this->dontSeeJson(['model' => $boatVersion->model]);
    }

    protected function getBoatVersionById(int $id){
        return \App\Models\BoatVersion::find($id);
    }

    protected function assertValidationError(){
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[0]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[1]->id));
        $this->assertNotEdited($this->getBoatVersionById($this->boatVersions[2]->id));
    }

}