<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Get allowed origins

$allowed_origins = explode('|', env('ALLOWED_ORIGINS'));

// Get request origin

$origin = request()->header('Origin');

//

if ($origin == env('PAYPAL_ORIGIN')) {
    header('Access-Control-Allow-Origin: ' . $origin);
    header('Access-Control-Allow-Methods: POST, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Requested-With');
    header('Access-Control-Allow-Credentials: true');
} else {
    if (in_array($origin, $allowed_origins)) {
        header('Access-Control-Allow-Origin: ' . $origin);
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, PATCH');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Requested-With');
        header('Access-Control-Allow-Credentials: true');
    }
}

Route::group(['middleware' => ['api', 'throttle:' . env('THROTTLE')], 'prefix' => 'v1'], function () {

    // Captains api
    Route::resource('/captains', 'v1\CaptainsController');

    // Partners api
    Route::resource('/partners', 'v1\PartnersController');

    // Users api
    Route::resource('/users', 'v1\UsersController');
    Route::patch('/users/{id}/password', 'v1\UsersController@changePassword');
    Route::post('/users/{id}/password', 'v1\UsersController@setPassword');
    Route::patch('/users/{id}/relationships/languages', 'v1\UsersController@languages');
    Route::get('/users/{id}/notifications', 'v1\UsersController@getUserNotifications');

    // Boats
    Route::resource('/boats', 'v1\BoatsController');
    Route::get('/captains/{id}/boats', 'v1\BoatsController@getCaptainBoats');
    Route::post('/captains/{id}/boats', 'v1\BoatsController@storeCaptainBoat');

    // Boats Relationships
    Route::resource('/indications', 'v1\IndicationsController');
    Route::resource('/rules', 'v1\RulesController');
    Route::resource('/accessories', 'v1\AccessoriesController');
    Route::resource('/experience-types', 'v1\ExperienceTypesController');
    Route::resource('/boat-availabilities', 'v1\BoatAvailabilitiesController');
    Route::get('/boats/{id}/captain', 'v1\UsersController@getCaptainByBoat');
    Route::patch('/boats/{id}/relationships/accessories', 'v1\AccessoriesController@update');
    Route::patch('/boats/{id}/relationships/experience-types', 'v1\ExperienceTypesController@update');

    // Photos api
    Route::resource('/signed-urls', 'v1\SignedUrlsController');
    Route::resource('/photos', 'v1\PhotosController');
    Route::get('/experiences/{id}/photos', 'v1\PhotosController@getExperiencePhotos');

    // Experiences
    Route::resource('/experiences', 'v1\ExperiencesController');
    Route::get('/captains/{id}/experiences', 'v1\ExperiencesController@getCaptainExperiences');
    Route::post('/captains/{id}/experiences', 'v1\ExperiencesController@storeCaptainExperience');
    Route::get('/boats/{id}/experiences', 'v1\ExperiencesController@getBoatExperiences');

    // Listings api
    Route::resource('/listings', 'v1\ListingsController');

    // Experiences Relationships
    Route::patch('/experiences/{id}/relationships/fixed-rules', 'v1\FixedRulesController@update');
    Route::resource('/additional-services', 'v1\AdditionalServicesController');
    Route::resource('/experience-availabilities', 'v1\ExperienceAvailabilitiesController');
    Route::resource('/periods', 'v1\PeriodsController');
    Route::resource('/prices', 'v1\PricesController');
    Route::get('/experiences/{id}/boat', 'v1\BoatsController@getBoatByExperience');
    Route::get('/periods/{id}/prices', 'v1\PeriodsController@getPrices');
    Route::resource('/fixed-additional-service-prices', 'v1\FixedServicePricesController');
    Route::patch('/experiences/{id}/fixed-additional-service-prices', 'v1\FixedServicePricesController@experienceFixedAdditionalServices');
    Route::post('/steps', 'v1\StepsController@store');
    Route::resource('/seos', 'v1\SeosController');
    Route::get('/experiences/{id}/seo', 'v1\SeosController@getSeoByExperience');
    Route::get('/slugurls', 'v1\SeosController@getSlugUrlByTitle');
    Route::get('/slugurls/seo', 'v1\SeosController@getSeoBySlugUrl');

    // Fixed list
    Route::get('/boat-types', 'v1\BoatTypesController@index');
    Route::get('/boat-materials', 'v1\BoatMaterialsController@index');
    Route::get('/motor-types', 'v1\MotorTypesController@index');
    Route::get('/fixed-rules', 'v1\FixedRulesController@index');
    Route::get('/fixed-additional-services', 'v1\FixedAdditionalServicesController@index');
    Route::get('/fishing-partitions', 'v1\FishingPartitionsController@index');
    Route::get('/fishing-types', 'v1\FishingTypesController@index');
    Route::get('/currencies', 'v1\CurrenciesController@index');
    Route::get('/spoken-languages', 'v1\SpokenLanguagesController@index');
    Route::get('/languages', 'v1\LanguagesController@index');
    Route::get('/devices', 'v1\DevicesController@index');
    Route::get('/countries', 'v1\CountriesController@index');
    Route::get('/captain-types', 'v1\CaptainTypesController@index');

    // Authentication api
    Route::post('/auth/password', 'v1\Auth\LoginController@login');
    Route::post('/auth/restore', 'v1\Auth\ResetPasswordController@restore');
    Route::post('/auth/reset', 'v1\Auth\ResetPasswordController@reset');
    Route::post('/auth/token', 'v1\Auth\TokensController@check');
    Route::post('/auth/accountkit', 'v1\Auth\UserAccountKitController@login');
    Route::post('/verify/accountkit', 'v1\Auth\UserAccountKitController@verify');
    Route::post('/auth/facebook', 'v1\Auth\FacebookAuthController@login');
    Route::post('/auth/admin', 'v1\Auth\AdminAccountKitController@login');
    Route::post('/auth/translator', 'v1\Auth\TranslatorAccountKitController@login');
    Route::post('/auth/logout', 'v1\Auth\LogoutController@logout');
    Route::post('/resend/token', 'v1\UsersController@resendToken');
    Route::post('/admin-token', 'v1\UsersController@getTokenForAccess');

    // Coupons
    Route::resource('/coupons', 'v1\CouponsController');
    Route::get('/experiences/{id}/coupons', 'v1\CouponsController@getExperienceCoupons');
    Route::get('/areas/{id}/coupons', 'v1\CouponsController@getAreaCoupons');
    Route::get('/users/{id}/coupons', 'v1\CouponsController@getUserCoupons');

    // Conversations
    Route::resource('/conversations', 'v1\ConversationsController');
    Route::resource('/users/{id}/conversations', 'v1\ConversationsController@getUserConversations');
    Route::resource('/experiences/{id}/conversations', 'v1\ConversationsController@getExperienceConversations');
    Route::resource('/messages', 'v1\MessagesController');

    // Bookings
    Route::resource('/bookings', 'v1\BookingsController');
    Route::get('/users/{id}/bookings', 'v1\BookingsController@getUserBookings');
    Route::get('/captains/{id}/bookings', 'v1\BookingsController@getCaptainBookings');
    Route::get('/estimations', 'v1\EstimationsController@getEstimations');

    // Translators api
    Route::resource('/translators', 'v1\TranslatorsController');

    // Pages api
    Route::resource('/pages', 'v1\PagesController');
    Route::resource('/pages/{id}/sentences', 'v1\PagesController@getPageSentences');

    // Sentences api
    Route::resource('/sentences', 'v1\SentencesController');

    // Translations api
    Route::resource('/translations', 'v1\TranslationsController@getPageTranslation');

    // Contact api
    Route::post('/contacts', 'v1\ContactsController@store');

    // Offsite bookings api
    Route::resource('/offsite-bookings', 'v1\OffsiteBookingsController');

    // Vouchers api
    Route::resource('/vouchers', 'v1\VouchersController');

    // Eden vouchers api
    Route::post('/eden-vouchers', 'v1\EdenVouchersController@store');
});

// Notify urls
Route::post('ipnlistener', 'v1\PaymentController@postPayment');
Route::post('ipnlistener-offsite', 'v1\PaymentController@postPaymentOffsite');

Route::group(['prefix' => 'v1'], function () {
    Route::post('/ipnlistener', 'v1\PaymentController@postPayment');
    Route::post('/ipnlistener-offsite', 'v1\PaymentController@postPaymentOffsite');
});



