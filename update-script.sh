#!/bin/bash
php artisan migrate:rollback
git checkout staging
git pull origin staging
composer install
composer dump-autoload
php artisan migrate:refresh --seed