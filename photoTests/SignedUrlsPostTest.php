<?php
/**
 * User: Giuseppe
 * Date: 07/02/2017
 * Time: 12:34
 */

use App\Models\User;
use App\Services\JwtService;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class SignedUrlsPostTest extends ApiTester
{
    protected $connectionsToTransact = ['mysql', 'mysql_translation'];
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    protected $route;
    protected $type;

    /**
     * SignedUrlsPostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->type = 'signed-urls';
        $this->route = 'v1/signed-urls';
    }

    /** @test */
    public function it_gets_201_and_presigned_url_if_it_is_user_or_admin()
    {
        //arrange
        $user = factory(User::class)->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertEquals(SymfonyResponse::HTTP_CREATED, $this->response->getStatusCode());
        $jsonResponse = json_decode($this->response->content('data.attributes.get_url'));
        $this->assertNotNull(filter_var($jsonResponse->data->attributes->get_url, FILTER_VALIDATE_URL));
        $jsonResponse = json_decode($this->response->content('data.attributes.put_url'));
        $this->assertNotNull(filter_var($jsonResponse->data->attributes->put_url, FILTER_VALIDATE_URL));

        //arrange
        $user = factory(\App\Models\Administrator::class)->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertEquals(SymfonyResponse::HTTP_CREATED, $this->response->getStatusCode());
        $jsonResponse = json_decode($this->response->content('data.attributes.get_url'));
        $this->assertNotNull(filter_var($jsonResponse->data->attributes->get_url, FILTER_VALIDATE_URL));
        $jsonResponse = json_decode($this->response->content('data.attributes.put_url'));
        $this->assertNotNull(filter_var($jsonResponse->data->attributes->put_url, FILTER_VALIDATE_URL));
    }

    /** @test */
    public function it_gets_409_if_type_is_wrong()
    {
        //arrange
        $user = factory(User::class)->create();
        $stub = $this->getStub();
        $stub['data']['type'] = 'pippo';

        //act
        $this->postJson($this->route, $stub, $this->getHeaders($user));

        //assert
        $this->assertEquals(SymfonyResponse::HTTP_CONFLICT, $this->response->getStatusCode());

    }

    /** @test */
    public function it_gets_403_if_account_is_not_user_or_admin()
    {
        //arrange
        $user = factory(\App\TranslationModels\User::class)->create();

        //act
        $this->postJson($this->route, $this->getStub(), $this->getHeaders($user));

        //assert
        $this->assertEquals(SymfonyResponse::HTTP_FORBIDDEN, $this->response->getStatusCode());
    }

    /** @test */
    public function it_returns_403_if_jwt_signature_is_incorrect()
    {
        //arrange
        $user = factory(User::class)->create();;

        $token = JwtService::getTokenStringFromAccount($user); // Retrieves the generated token

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token . 'jgjhgjhgjh']);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token was tampered with']);

    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $user = factory(User::class)->create();;

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        //act
        $this->postJson($this->route, $this->getStub(), ['Authorization' => 'Bearer ' . $token]);

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    protected function getStub()
    {
        return ['data' => [
            'type' => $this->type
        ]];
    }
}