<?php

/**
 * User: Giuseppe
 * Date: 21/12/2016
 * Time: 16:12
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Authorization;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperiencePhoto;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Models\Experience;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;
use JD\Cloudder\Facades\Cloudder;

class DeletePhotoTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    /** @test */
    public function it_gets_200_if_the_parameters_are_valid_for_delete_experience_photo()
    {
        /** Delete second of two photos */
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id, [],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertNotEquals($lastPhoto->id, $photo->id);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $photo->id);
        $this->assertFalse($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);

        /** Delete first of two photos */
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->first();

        //act
        $this->deleteJson('v1/photos/' . $photo->id, [],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $firstPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->first();
        $this->assertNotEquals($firstPhoto->id, $photo->id);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $photo->id);
        $this->assertTrue($firstPhoto->is_cover);
        $this->assertFalse($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);

        /** Delete photo of one photo */
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->first();

        //act
        $this->deleteJson('v1/photos/' . $photo->id, [],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $this->assertEquals(0, $newExperienceVersion->experienceVersionPhotos()->count());
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $photo->id);
        $this->assertFalse($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }


    /** @test */
    public function it_returns_404_if_photo_of_the_route_does_not_exist()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . ($photo->id + 1),
            [],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired_on_delete()
    {
        //arrange
        $userTest = new UserTest();
        $user = $userTest->createUser();
        $user->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id,
            [],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_403_if_experience_version_exists_but_jwt_sub_claim_is_different_for_delete_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceFirst = Experience::all()->first();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];
        $boat->experiences()->save($experience);

        $user_1 = $experienceFirst->boat->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'is_cover' => '0'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id,
            [],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user_1)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }


    /** @test */
    public function it_returns_200_if_admin_has_permissions_to_delete_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = true;
        $experienceVersion->currency = 'USD';
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat;
        $experienceVersion->departure_lng = $area->point_a_lng;
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id, [],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert

        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $photo->id);
        $this->assertEquals(0, $newExperienceVersion->experienceVersionPhotos()->count());
        $this->assertFalse($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_delete_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experienceVersion->status = Constant::STATUS_ACCEPTED;

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->save();

        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion->is_finished = true;
        $experienceVersion->save();

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id,
            [],
            ['Authorization' => 'Bearer ' . $token]
        );


        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot delete this data']);
    }

    /** @test */
    public function it_returns_403_admin_has_not_permission_for_delete_experience_picture_in_that_area()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat - 1;
        $experienceVersion->departure_lng = $area->point_a_lng - 1;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion->is_finished = true;
        $experienceVersion->save();

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        //act
        $this->deleteJson('v1/photos/' . $photo->id,
            [],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot delete this data']);
    }

    protected function url_exists($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        }
        return true;
    }
}