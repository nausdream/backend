<?php

/**
 * User: Giuseppe
 * Date: 01/12/2016
 * Time: 12:40
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Services\JwtService;
use App\Services\PhotoHelper;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class PhotoGetListTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private $route;
    private $type;

    /**
     * PhotoGetListTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->route = 'v1/experiences/';
        $this->type = 'photos';
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_captain_it_returns_200_and_last_experience_version_photos()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //act
        $this->getJson($this->route . $experience->id . '/photos' . '?editing=true', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_2->id);
        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_200_and_last_finished_experience_version_in_admin_area()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($admin)]
        );

        //act
        $this->getJson($this->route . $experience->id . '/photos' . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_2->id);
        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_admin_it_returns_403_if_admin_has_no_experiences_permission_or_last_finished_is_not_in_area()
    {
        // ADMIN NO PERMISSION
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);
        $experienceVersion_2->departure_lat = 5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion_2->is_finished = true;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos' . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);

        // ADMIN NOT IN AREA
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);

        $experienceVersion_1->departure_lat = 5;
        $experienceVersion_1->departure_lng = 5;

        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);

        $experienceVersion_2->departure_lat = -5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos' . '?editing=true', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=true
     * @test
     */
    public function if_user_it_returns_403()
    {
        // ADMIN NO PERMISSION
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion_2->is_finished = true;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos' . '?editing=true', $this->getHeaders(new User()));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /**
     * ?editing=false
     * @test
     */
    public function it_returns_200_and_last_ACCEPTED_experience_version_for_any_user()
    {
        //CAPTAIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);

        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();

        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

        //ADMIN NO PERMISSIONS
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $admin->authorizations()->detach();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();

        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

        //ADMIN NO AREA
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $this->setSmallArea($admin);
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();

        $experienceVersion_1->departure_lat = -5;
        $experienceVersion_1->departure_lng = 5;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->departure_lat = 5;
        $experienceVersion_2->departure_lng = 5;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

        //SUPER ADMIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

        //USER
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }

        //VISITOR
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceRelationship = $this->getExperienceRelationship($experience);

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $experienceVersion_1->is_finished = true;
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED;
        $experienceVersion_1->save();
        $experienceVersion_2 = $this->getExperienceVersion($experience);
        $experienceVersion_2->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_2->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_OK);
        $this->seeJson(['type' => $this->type]);
        $experienceVersionNew = ExperienceVersion::find($experienceVersion_1->id);

        $photos = $experienceVersionNew->experienceVersionPhotos()->get();
        foreach ($photos as $photo) {
            $this->seeJson(['id' => (string)$photo->id]);
            $this->seeJson(['attributes' => [
                'link' => PhotoHelper::getExperiencePhotoLink($photo),
                'is_cover' => $photo->is_cover
            ]]);
        }
    }

    /**
     * @test
     */
    public function it_returns_404_if_experience_doesnt_exist()
    {
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experiences = \App\Models\Experience::all();
        $experienceId = $experiences->count() > 0 ? $experiences->last()->id + 1 : 1;

        //act
        $this->getJson($this->route . $experienceId . '/photos', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function it_returns_404_if_correct_experience_version_doesnt_exist()
    {
        // CAPTAIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($user));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);


        // ADMIN
        //arrange
        $user = $this->getUser();
        $admin = $this->getSuperAdmin();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);
        $experienceVersion_1 = $this->getExperienceVersion($experience, false);
        $experienceVersion_2 = $this->getExperienceVersion($experience, false);

        //act
        $this->getJson($this->route . $experience->id . '/photos', $this->getHeaders($admin));

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);

        // EVERYONE WITH EDITING=FALSE
        //arrange
        $user = $this->getUser();
        $boat = $this->getBoat($user);
        $experience = $this->getExperience($boat);

        $experienceVersion_1 = $this->getExperienceVersion($experience);
        $experienceVersion_1->status = Constant::STATUS_ACCEPTED_CONDITIONALLY;
        $experienceVersion_1->save();

        //act
        $this->getJson($this->route . $experience->id . '/photos');

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    private function getExperience(Boat $boat)
    {
        $experience = factory(\App\Models\Experience::class)->create();

        if (isset($boat)) {
            $experience->boat()->associate($boat);
            $experience->save();
        }

        return $experience;
    }

    private function getExperienceVersionStub(bool $isFinished)
    {
        return [
            'departure_port' => $this->fake->text(Constant::PORT_DEPARTURE_LENGTH),
            'departure_time' => $this->fake->time(),
            'arrival_port' => $this->fake->text(Constant::PORT_RETURN_LENGTH),
            'arrival_time' => $this->fake->time(),
            'monday' => $this->fake->boolean,
            'tuesday' => $this->fake->boolean,
            'wednesday' => $this->fake->boolean,
            'thursday' => $this->fake->boolean,
            'friday' => $this->fake->boolean,
            'saturday' => $this->fake->boolean,
            'sunday' => $this->fake->boolean,
            'is_fixed_price' => $this->fake->boolean,
            'seats' => $this->fake->randomNumber(2),
            'is_searchable' => true,
            'is_finished' => $isFinished,
            'departure_lat' => $this->fake->latitude,
            'departure_lng' => $this->fake->longitude,
            'destination_lat' => $this->fake->latitude,
            'destination_lng' => $this->fake->longitude,
            'road_stead' => $this->fake->boolean,
            'fixed_menu' => $this->fake->boolean,
            'starter_dish' => $this->fake->boolean,
            'first_dish' => $this->fake->boolean,
            'second_dish' => $this->fake->boolean,
            'side_dish' => $this->fake->boolean,
            'dessert_dish' => $this->fake->boolean,
            'bread' => $this->fake->boolean,
            'wines' => $this->fake->boolean,
            'champagne' => $this->fake->boolean,
            'weather' => $this->fake->boolean,
            'lures' => $this->fake->boolean,
            'fishing_pole' => $this->fake->boolean,
            'fresh_bag' => $this->fake->boolean,
            'ice' => $this->fake->boolean,
            'underwater_baptism' => $this->fake->boolean,
            'down_payment' => $this->fake->boolean,
            'days' => $this->fake->numberBetween(1, Constant::MAX_EXPERIENCE_DAYS),
            'fishing_partition' => \App\Models\FishingPartition::inRandomOrder()->first()->name,
            'fishing_type' => \App\Models\FishingType::inRandomOrder()->first()->name,
            'deposit' => $this->fake->randomElement([$this->fake->numberBetween(0,1000), null]),
            'cancellation_max_days' => random_int(Constant::CANCELLATION_MIN_DAYS, Constant::CANCELLATION_MAX_DAYS),
            'deposit_card' => $this->fake->boolean,
            'deposit_check' => $this->fake->boolean,
            'deposit_cash' => $this->fake->boolean,
        ];
    }

    private function getExperienceVersion(\App\Models\Experience $experience = null, bool $isFinished = true)
    {
        $experienceVersion = factory(ExperienceVersion::class)->create($this->getExperienceVersionStub($isFinished));
        if (isset($experience)) {
            $experience->experienceVersions()->save($experienceVersion);
        }
        return $experienceVersion;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

    private function getBoatVersionRelatedFields()
    {
        $maxExperienceTypeValue = App\Models\ExperienceType::all()->last()->id;

        return [
            'indication' => $this->fake->text(),
            'rules' => $this->fake->text(),
            'description' => $this->fake->text(),
            'accessories' => [
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
                random_int(1, 25),
            ],
            'experience_types' => [
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
                random_int(1, $maxExperienceTypeValue),
            ],
        ];
    }

    protected function getUser()
    {
        $user = factory(User::class)->make();
        $user->first_name = $this->fake->firstName;
        $user->last_name = $this->fake->lastName;
        $user->email = $this->fake->email;
        $user->phone = $this->fake->phoneNumber;
        $user->save();
        return $user;
    }

    protected function getBoat(User $user = null)
    {
        $boat = factory(Boat::class)->create();
        if (isset($user)) {
            $boat->user()->associate($user);
            $boat->save();
        }
        $boatVersion = factory(BoatVersion::class)->create($this->getBoatVersionStub());
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->is_finished = true;

        // Relationships
        $accessories = array_unique($this->getBoatVersionRelatedFields()['accessories']);
        $boatVersion->accessories()->attach($accessories);
        $experienceTypes = array_unique($this->getBoatVersionRelatedFields()['experience_types']);
        $boat->experienceTypes()->attach($experienceTypes);
        $boat->boatVersions()->save($boatVersion);

        return $boat;
    }

    public function getSuperAdmin()
    {
        $area_1 = new Area(['name' => $this->fake->country,
            'point_a_lat' => -5000,
            'point_a_lng' => -5000,
            'point_b_lat' => 5000,
            'point_b_lng' => 5000
        ]);
        $area_1->save();

        $admin = factory(Administrator::class)->create();
        $admin->authorizations()->attach(\App\Models\Authorization::where('name', 'experiences')->get()->first()->id);
        $admin->area()->associate($area_1);
        $admin->save();

        return $admin;
    }

    protected function setSmallArea(Administrator &$admin)
    {
        $area = new Area(['name' => $this->fake->country,
            'point_a_lat' => 0,
            'point_a_lng' => 0,
            'point_b_lat' => 10,
            'point_b_lng' => 10
        ]);
        $area->save();

        $admin->area()->associate($area);
        $admin->save();
    }

    /**
     * @param $experience
     * @return array
     */
    private function getExperienceRelationship($experience):array
    {
        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];
        return $experienceRelationship;
    }
}
