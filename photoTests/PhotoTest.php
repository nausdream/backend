<?php

/**
 * User: Giuseppe
 * Date: 17/11/2016
 * Time: 16:11
 */

use App\Constant;
use App\Models\Administrator;
use App\Models\Area;
use App\Models\Authorization;
use App\Models\Boat;
use App\Models\BoatVersion;
use App\Models\ExperiencePhoto;
use App\Models\ExperienceVersion;
use App\Models\User;
use App\Models\Experience;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use App\Services\JwtService;
use JD\Cloudder\Facades\Cloudder;


class PhotoTest extends ApiTester
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    // POST tests

    /** @test */
    public function it_gets_201_if_the_parameters_are_valid_for_user_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();

        $photo_version = $user->photo_version;
        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newUser = User::find($user->id);
        $this->assertNotEquals($photo_version, $newUser->photo_version);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/' . $user->id . "/profile");
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_gets_201_if_the_parameters_are_valid_for_boat_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();
        $stub = $this->getBoatVersionStub();
        $boatVersion = factory(BoatVersion::class)->make($stub);
        $boatVersion->boat_id = $boat->id;
        $boatVersion->is_finished = false;
        $boatVersion->save();
        $photo_version = $boatVersion->version_external_photo;

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newBoatVersion = BoatVersion::find($boatVersion->id);
        $this->assertNotEquals($photo_version, $newBoatVersion->version_external_photo);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER')
            . 'users/'
            . $boat->user->id . "/"
            . $boat->id . "/"
            . 'external');
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_gets_201_if_the_parameters_are_valid_for_experience_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);
        $count = $experienceVersion->experienceVersionPhotos()->get()->count();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertNotEquals($count, $newExperienceVersion->experienceVersionPhotos()->get()->count());
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $lastPhoto->id);
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_set_photo_cover_if_experience_has_not_photo_cover_if_the_parameters_are_valid_for_experience_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertTrue($lastPhoto->is_cover);
    }

    /** @test */
    public function it_gets_403_if_the_parameters_are_valid_for_experience_photo_but_has_already_maximum_number_of_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert

        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Maximum photo number reached for this experience']);
    }

    /** @test */
    public function it_returns_404_if_user_of_the_request_does_not_exist()
    {
        //arrange
        $userTest = new UserTest();
        $user = $userTest->createUser();
        $user->save();
        $lastUser = User::all()->last();

        $userRelationship = ['user' => ['data' => $this->createData('users', $lastUser->id + 1)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_404_if_boat_of_the_request_does_not_exist()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $boat->save();

        $boat = Boat::all()->last();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id + 1)]];

        $user = $boat->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_422_if_experience_of_the_request_does_not_exist()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experience = Experience::all()->last();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id + 1)]];

        $user = $experience->boat->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'is_cover' => '0'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_403_if_jwt_is_expired()
    {
        //arrange
        $userTest = new UserTest();
        $user = $userTest->createUser();
        $user->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_403_if_user_exists_but_jwt_sub_claim_is_different_for_update_user_profile_picture()
    {
        //arrange
        $user_1 = factory(User::class)->make();
        $user_1->first_name = 'Tizio';
        $user_1->save();
        $user_2 = factory(User::class)->make();
        $user_2->first_name = 'Caio';
        $user_2->save();

        $token = JwtService::getTokenStringFromAccount($user_2);

        $userRelationship = ['user' => ['data' => $this->createData('users', $user_1->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_201_if_update_user_profile_from_admin_account_has_valid_parameters_and_admin_has_permission()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->save();
        $photo_version = $user->photo_verion;
        $admin = factory(Administrator::class)->make();
        $admin->save();

        $authorization = Authorization::where('name', 'users')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $newUser = User::find($user->id);
        $this->assertNotEquals($photo_version, $newUser->photo_version);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/' . $user->id . "/profile");
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_returns_201_if_update_captain_profile_from_admin_account_has_valid_parameters_and_admin_has_permission()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->is_captain = true;
        $user->save();
        $photo_version = $user->photo_verion;
        $admin = factory(Administrator::class)->make();
        $admin->save();

        $authorization = Authorization::where('name', 'captains')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $newUser = User::find($user->id);
        $this->assertNotEquals($photo_version, $newUser->photo_version);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/' . $user->id . "/profile");
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_returns_201_if_admin_has_permissions_to_update_boat_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();
        $stub = $this->getBoatVersionStub();
        $area = factory(Area::class)->create();
        $boatVersion = factory(BoatVersion::class)->make($stub);
        $boatVersion->boat_id = $boat->id;
        $boatVersion->is_finished = true;
        $boatVersion->lat = $area->point_a_lat;
        $boatVersion->lng = $area->point_a_lng;
        $boatVersion->save();
        $photo_version = $boatVersion->version_external_photo;

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'boats')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $newBoatVersion = BoatVersion::find($boatVersion->id);
        $this->assertNotEquals($photo_version, $newBoatVersion->version_external_photo);
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER')
            . 'users/'
            . $boat->user->id . "/"
            . $boat->id . "/"
            . 'external');
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }

    /** @test */
    public function it_returns_201_if_admin_has_permissions_to_post_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->currency = 'USD';

        $experienceVersion->is_finished = true;
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat;
        $experienceVersion->departure_lng = $area->point_a_lng;
        $experience->experienceVersions()->save($experienceVersion);
        $count = $experienceVersion->experienceVersionPhotos()->get()->count();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertNotEquals($count, $newExperienceVersion->experienceVersionPhotos()->get()->count());
        $expectedUrl = Cloudder::secureShow(env('CLOUDINARY_BASE_FOLDER') . 'users/'
            . $newExperienceVersion->experience->boat->user->id . "/"
            . $newExperienceVersion->experience->boat->id . "/"
            . $newExperienceVersion->experience->id . "/"
            . $lastPhoto->id);
        $this->assertTrue($this->url_exists($expectedUrl));
        $this->assertResponseStatus(SymfonyResponse::HTTP_CREATED);
    }


    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_update_user_profile_picture()
    {
        //arrange
        $user = factory(User::class)->make();
        $user->save();
        $admin = factory(Administrator::class)->make();
        $admin->save();

        $token = JwtService::getTokenStringFromAccount($admin);

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_boat_exists_but_jwt_sub_claim_is_different_for_update_boat_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $boat->save();
        $boatId = $boat->id;

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();

        $boatFirst = Boat::find($boatId);

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $user = $boatFirst->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_update_boat_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();
        $stub = $this->getBoatVersionStub();
        $boatVersion = factory(BoatVersion::class)->make($stub);
        $boatVersion->boat_id = $boat->id;
        $boatVersion->is_finished = true;
        $boatVersion->save();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->save();

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_update_boat_picture_in_that_area()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();
        $stub = $this->getBoatVersionStub();
        $area = factory(Area::class)->create();
        $boatVersion = factory(BoatVersion::class)->make($stub);
        $boatVersion->boat_id = $boat->id;
        $boatVersion->is_finished = true;
        $boatVersion->lat = $area->point_a_lat - 1;
        $boatVersion->lng = $area->point_a_lng - 1;
        $boatVersion->save();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'boats')->first();
        $admin->authorizations()->attach($authorization->id);

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_post_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = true;
        $experienceVersion->currency = 'USD';

        $experienceVersion->status = Constant::STATUS_ACCEPTED;

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->save();

        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_admin_has_not_permission_for_post_experience_picture_in_that_area()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = true;
        $experienceVersion->currency = 'USD';
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat - 1;
        $experienceVersion->departure_lng = $area->point_a_lng - 1;
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_boat_exists_but_has_not_boat_versions()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_boat_exists_but_has_not_valid_boat_version()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);
        $boat->save();
        $stub = $this->getBoatVersionStub();
        $boatVersion = factory(BoatVersion::class)->make($stub);
        $boatVersion->boat_id = $boat->id;
        $boatVersion->is_finished = true;
        $boatVersion->status = Constant::STATUS_ACCEPTED;
        $boatVersion->save();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'photo_type' => 'external'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_experience_exists_but_jwt_sub_claim_is_different_for_post_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceFirst = Experience::all()->first();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $user = $experienceFirst->boat->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'is_cover' => '0'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }

    /** @test */
    public function it_returns_403_if_experience_exists_but_has_not_experience_versions()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_if_experience_exists_but_has_not_valid_experience_version()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = true;
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->currency = 'USD';
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_409_if_type_in_update_request_is_not_photos()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photo', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }

    /** @test */
    public function it_gets_422_if_there_is_an_invalid_parameter()
    {
        /** link */

        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => null], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'https://res.cloudinary.com/nausdream-development/image/upload/v1479897321/test/prova.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['detail' => 'File too large']);


        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();

        $userRelationship = ['user' => ['data' => $this->createData('users', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.sad.com/asdasdasd'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->seeJson(['detail' => 'Link provided is not an image']);

        /** photo_type */

        //arrange
        $user = $userTest->createUser();
        $user->save();
        factory(Boat::class)->create(['user_id' => $user->id]);
        $boat = Boat::all()->random();

        $boatRelationship = ['boat' => ['data' => $this->createData('boats', $boat->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'type' => 'pippo'], $boatRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** relationship_type */

        //arrange
        $user = $userTest->createUser();
        $user->save();

        $userRelationship = ['user' => ['data' => $this->createData('pippo', $user->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $userRelationship);

        //act
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    // PATCH tests

    /** @test */
    public function it_gets_202_if_the_parameters_are_valid_for_update_experience_photo()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );
        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );


        //assert
        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertTrue($lastPhoto->is_cover);
        $otherPhoto = $newExperienceVersion->experienceVersionPhotos()->where('is_cover', true)->where('id', '!=', $lastPhoto->id)->get()->last();
        $this->assertNull($otherPhoto);
        $this->assertResponseStatus(SymfonyResponse::HTTP_ACCEPTED);
    }


    /** @test */
    public function it_returns_404_if_photo_of_the_route_does_not_exist()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id + 1, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . ($photo->id + 1),
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_NOT_FOUND);
    }

    /** @test */
    public function it_returns_422_if_there_are_invalid_parameters_on_patch()
    {
        /** is_cover */

        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id, ['is_cover' => false]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY);

        /** id matching */

        //arrange

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id + 1, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);

        /** type */

        //arrange

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('pippo', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_CONFLICT);
    }


    /** @test */
    public function it_returns_403_if_jwt_is_expired_on_patch()
    {
        //arrange
        $userTest = new UserTest();
        $user = $userTest->createUser();
        $user->save();

        $signer = new Sha256();

        $token = (new Builder())
            ->setIssuer(env('APP_URL'))// Configures the issuer (iss claim)
            ->setIssuedAt(time() - 1000000)// Configures the time that the token was issue (iat claim)
            ->setNotBefore(time() - 500000)// Configures the time that the token can be used (nbf claim)
            ->setExpiration(time() - 150000)// Configures the expiration time of the token (exp claim)
            ->setSubject($user->id)// (sub claim)
            ->sign($signer, env('JWT_SECRET'))
            ->getToken();

        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'Token expired or no permission to access this resource']);
    }

    /** @test */
    public function it_returns_403_if_experience_version_exists_but_jwt_sub_claim_is_different_for_patch_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceFirst = Experience::all()->first();

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];
        $boat->experiences()->save($experience);

        $user_1 = $experienceFirst->boat->user;

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg', 'is_cover' => '0'], $experienceRelationship);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user_1)]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
    }


    /** @test */
    public function it_returns_202_if_admin_has_permissions_to_patch_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = true;
        $experienceVersion->currency = 'USD';

        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat;
        $experienceVersion->departure_lng = $area->point_a_lng;
        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();

        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $newExperienceVersion = ExperienceVersion::find($experienceVersion->id);
        $lastPhoto = $newExperienceVersion->experienceVersionPhotos()->get()->last();
        $this->assertTrue($lastPhoto->is_cover);
        $otherPhoto = $newExperienceVersion->experienceVersionPhotos()->where('is_cover', true)->where('id', '!=', $lastPhoto->id)->get()->last();
        $this->assertNull($otherPhoto);
        $this->assertResponseStatus(SymfonyResponse::HTTP_ACCEPTED);
    }

    /** @test */
    public function it_returns_403_if_admin_has_not_permission_for_patch_experience_picture()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->currency = 'USD';

        $experienceVersion->status = Constant::STATUS_ACCEPTED;

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->save();

        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion->is_finished = true;
        $experienceVersion->save();

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();
        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );


        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    /** @test */
    public function it_returns_403_admin_has_not_permission_for_patch_experience_picture_in_that_area()
    {
        //arrange
        $userTest = new UserTest();

        $user = $userTest->createUser();
        $user->save();
        $boat = factory(Boat::class)->create(['user_id' => $user->id]);

        $experience = new Experience();
        $boat->experiences()->save($experience);

        $area = factory(Area::class)->create();
        $experienceVersion = new ExperienceVersion();
        $experienceVersion->type = 1;
        $experienceVersion->duration = 5;
        $experienceVersion->is_finished = false;
        $experienceVersion->status = Constant::STATUS_ACCEPTED;
        $experienceVersion->departure_lat = $area->point_a_lat - 1;
        $experienceVersion->departure_lng = $area->point_a_lng - 1;
        $experienceVersion->currency = 'USD';

        $experience->experienceVersions()->save($experienceVersion);

        $experienceRelationship = ['experience' => ['data' => $this->createData('experiences', $experience->id)]];

        $stub = $this->createData('photos', null, ['link' => 'http://www.w3schools.com/css/img_fjords.jpg'], $experienceRelationship);

        $admin = factory(Administrator::class)->make();
        $admin->area_id = $area->id;
        $admin->save();
        $authorization = Authorization::where('name', 'experiences')->first();
        $admin->authorizations()->attach($authorization->id);


        $token = JwtService::getTokenStringFromAccount($admin);

        $this->postJson('v1/photos/',
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . JwtService::getTokenStringFromAccount($user)]
        );

        $experienceVersion->is_finished = true;
        $experienceVersion->save();

        $photo = $experienceVersion->experienceVersionPhotos()->get()->last();
        $stub = $this->createData('photos', $photo->id, ['is_cover' => true]);

        //act
        $this->patchJson('v1/photos/' . $photo->id,
            ['data' => $stub],
            ['Authorization' => 'Bearer ' . $token]
        );

        //assert
        $this->assertResponseStatus(SymfonyResponse::HTTP_FORBIDDEN);
        $this->seeJson(['detail' => 'You cannot update this data']);
    }

    protected function url_exists($url)
    {
        $file = $url;
        $file_headers = @get_headers($file);
        if (!$file_headers || $file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        }
        return true;
    }

    private function getBoatVersionStub()
    {
        return [
            'model' => str_random(Constant::BOAT_MODEL_LENGTH),
            'type' => \App\Models\BoatType::inRandomOrder()->first()->id,
            'docking_place' => str_random(Constant::DOCKING_PLACE_LENGTH),
            'seats' => $this->fake->numberBetween(1, 500),
            'berths' => $this->fake->numberBetween(0, 500),
            'cabins' => $this->fake->numberBetween(0, 500),
            'toilets' => $this->fake->numberBetween(0, 500),
            'production_site' => str_random(Constant::PRODUCTION_SITE_LENGTH),
            'name' => str_random(Constant::BOAT_NAME_LENGTH),
            'construction_year' => $this->fake->numberBetween(0, date('Y')),
            'restoration_year' => $this->fake->numberBetween(0, date('Y')),
            'material' => \App\Models\BoatMaterial::inRandomOrder()->first()->id,
            'length' => $this->fake->numberBetween(1, 500),
            'has_insurance' => $this->fake->boolean,
            'motors_quantity' => $this->fake->numberBetween(1, 10),
            'motor_type' => \App\Models\MotorType::inRandomOrder()->first()->id,
            'lat' => $this->fake->latitude,
            'lng' => $this->fake->longitude,
            'events' => $this->fake->boolean()
        ];
    }

}